Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data


Partial Class ParentLogin_PasswordRecovery
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim str_query As String
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim transaction As SqlTransaction
        Dim bPasswordUpdate As Boolean = False
        Dim status As Integer
        Dim strParUserName As String
        Dim USR_ID As String


        If Page.IsValid = True Then
            Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                transaction = conn.BeginTransaction("SampleTransaction")
                Try
                    Dim pass As String = Encr_decrData.Encrypt("gems2010")
                    str_query = "exec UPDPARENT_PASSRECO '" & Session("username") & "', '" & txtemailid.Text & "'"
                    Dim stat As Integer
                    stat = SqlHelper.ExecuteNonQuery(transaction, CommandType.Text, str_query)
                    transaction.Commit()
                    lblError.Text = "Updated Succesfully"
                    bPasswordUpdate = True
                Catch ex As Exception
                    transaction.Rollback()
                    lblError.Text = "Record could not be Saved"
                    UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                End Try
            End Using
        End If
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        If Session("username") Is Nothing Then
            Response.Redirect("~\ParentLogin\Login.aspx")
        End If
        Page.Title = "GEMS EDUCATION"
        If Page.IsPostBack = False Then
            Dim pParms(3) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@OLU_NAME", Session("username"))
            Using reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "GetPARENT_PASSRECO", pParms)
                While reader.Read
                    txtemailid.Text = Convert.ToString(reader("OLU_EMAIL"))
                End While
            End Using

            If Session("STU_NAME") = "" Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            End If
        End If
    End Sub
End Class


