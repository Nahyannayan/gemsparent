﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Partial Class UpdateInfo_UpdateVisaDetails
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    

        If Session("username") Is Nothing Then
            Session("Active_tab") = "Home"
            Session("Site_Path") = ""
            Response.Redirect("~\login.aspx")
        ElseIf Session("bPasswdChanged") = "False" Then
            Response.Redirect("~\UpdateInfo\ForcePasswordChange.aspx", False)
        ElseIf Session("bUpdateContactDetails") = "False" Then
            Response.Redirect(Session("ForceUpdate_stud"), False)
        End If

        If Page.IsPostBack = False Then

            Try
                lbChildName.Text = Session("STU_NAME")

                Call Student_M_Details(Session("STU_ID"))
               


                UtilityObj.beforeLoopingControls(Me.Page)
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try
        End If
    End Sub
    Sub Student_M_Details(ByVal STU_ID As String)

        Try
            Dim conn As String = ConnectionManger.GetOASISConnectionString
            Dim param(2) As SqlParameter
            param(0) = New SqlParameter("@STU_ID", STU_ID)

            Using readerStudent_Detail As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "OPL.GETPROF_STUDENT_VISA_PASS", param)
                If readerStudent_Detail.HasRows = True Then
                    While readerStudent_Detail.Read

                        'handle the null value returned from the reader incase  convert.tostring
                        txtPNo.Text = Convert.ToString(readerStudent_Detail("STU_PASPRTNO"))
                        txtPIssPlace.Text = Convert.ToString(readerStudent_Detail("STU_PASPRTISSPLACE"))
                        txtVNo.Text = Convert.ToString(readerStudent_Detail("STU_VISANO"))
                        txtVIssPlace.Text = Convert.ToString(readerStudent_Detail("STU_VISAISSPLACE"))
                        txtVIssAuth.Text = Convert.ToString(readerStudent_Detail("STU_VISAISSAUTH"))
                        'Setting date
                        txtPIssDate.Text = readerStudent_Detail("STU_PASPRTISSDATE")


                        txtPExpDate.Text = readerStudent_Detail("STU_PASPRTEXPDATE")


                        txtVIssDate.Text = readerStudent_Detail("STU_VISAISSDATE")


                        txtVExpDate.Text = readerStudent_Detail("STU_VISAEXPDATE")
                        txtEMIRATES_ID.Text = readerStudent_Detail("STU_EMIRATES_ID")
                        txtPremisesID.Text = readerStudent_Detail("stu_PremisesID")
                        txtEMIRATES_IDExp_date.Text = readerStudent_Detail("STU_EMIRATES_ID_EXPDATE")


                        If Convert.ToBoolean(readerStudent_Detail("BSU_bHide_PremisesID")) = False Then
                            trPremises.Visible = True
                        Else
                            trPremises.Visible = False
                        End If

                    End While
                End If
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click


        If validate_dates() <> "" Then
            lblValidate_msg.Text = validate_dates()
            Exit Sub
        End If
        Dim status As Integer
        Dim transaction As SqlTransaction
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try

                status = callTrans_Student_M(Session("STU_ID"), transaction)
                If status <> 0 Then
                    Throw New ArgumentException(UtilityObj.getErrorMessage(status))
                End If
                UtilityObj.InsertAuditdetails(transaction, "edit", "STUDENT_M", Session("STU_ID"), Session("STU_ID"), "STU_ID=" + Session("STU_ID").ToString)
                transaction.Commit()
                Dim msg As String = String.Empty
                Dim lblmsg As Label = CType(Page.Master.FindControl("lblSiteMessage"), Label)
                lblmsg.CssClass = "divvalid"

                lblmsg.Text = "Record Updated Successfully"


            Catch myex As ArgumentException
                transaction.Rollback()
                Dim lblmsg As Label = CType(Page.Master.FindControl("lblSiteMessage"), Label)
                lblmsg.CssClass = "diverror"
                lblmsg.Text = myex.Message
            Catch ex As Exception
                transaction.Rollback()
                Dim lblmsg As Label = CType(Page.Master.FindControl("lblSiteMessage"), Label)
                lblmsg.CssClass = "diverror"
                lblmsg.Text = "Record could not be Updated"
            End Try
        End Using
    End Sub
    Private Function validate_dates() As String
        Dim str_error As String = String.Empty
        lblValidate_msg.Text = ""

        '-----------------------------------Passport Issue date date-----------------------------
        If txtPIssDate.Text.Trim <> "" Then
            Dim strPIssDate As String = txtPIssDate.Text.Trim
            Dim str_PIssDate As String = DateFunctions.checkdate(strPIssDate)
            If str_PIssDate <> "" Then
                str_error = str_error & "<b style='padding-left:10px;padding-top:-1px;margin-top:-1px;margin-bottom:4px;display:block;'>&#149;&nbsp;&nbsp;Passport issue date format is invalid</b>"
            Else
                txtPIssDate.Text = strPIssDate
            End If
        End If
        '-----------------------------------Passport expire date date-----------------------------
        If txtPExpDate.Text.Trim <> "" Then
            Dim strPExpDate As String = txtPExpDate.Text.Trim
            Dim str_PExpDate As String = DateFunctions.checkdate(strPExpDate)
            If str_PExpDate <> "" Then
                str_error = str_error & "<b style='padding-left:10px;padding-top:-1px;margin-top:-1px;margin-bottom:4px;display:block;'>&#149;&nbsp;&nbsp;Passport expiry date format is invalid</b>"
            Else
                txtPExpDate.Text = strPExpDate
            End If
        End If

        '-----------------------------------Visa Issue date date-----------------------------
        If txtVIssDate.Text.Trim <> "" Then
            Dim strVIssDate As String = txtVIssDate.Text.Trim
            Dim str_VIssDate As String = DateFunctions.checkdate(strVIssDate)
            If str_VIssDate <> "" Then
                str_error = str_error & "<b style='padding-left:10px;padding-top:-1px;margin-top:-1px;margin-bottom:4px;display:block;'>&#149;&nbsp;&nbsp;Visa issue date format is invalid</b>"
            Else
                txtVIssDate.Text = strVIssDate
            End If
        End If
        '-----------------------------------Visa expiry date date-----------------------------
        If txtVExpDate.Text.Trim <> "" Then
            Dim strVExpDate As String = txtVExpDate.Text.Trim
            Dim str_VExpDate As String = DateFunctions.checkdate(strVExpDate)
            If str_VExpDate <> "" Then
                str_error = str_error & "<b style='padding-left:10px;padding-top:-1px;margin-top:-1px;margin-bottom:4px;display:block;'>&#149;&nbsp;&nbsp;Visa expiry date format is invalid</b>"
            Else
                txtVExpDate.Text = strVExpDate
            End If
        End If
        '-----------------------------------Emirate Exp date-----------------------------
        If txtEMIRATES_IDExp_date.Text.Trim <> "" Then
            Dim strfEmirExpDate As String = txtEMIRATES_IDExp_date.Text.Trim
            Dim str_EmirExperr As String = DateFunctions.checkdate(strfEmirExpDate)
            If str_EmirExperr <> "" Then
                str_error = str_error & "<b style='padding-left:10px;padding-top:-1px;margin-top:-1px;margin-bottom:4px;display:block;'>&#149;&nbsp;&nbsp;Emirates ID expiry date format is invalid</b>"
            Else
                txtEMIRATES_IDExp_date.Text = strfEmirExpDate
            End If
        End If
        Return str_error
    End Function
    Function callTrans_Student_M(ByVal STU_ID As String, ByVal trans As SqlTransaction) As Integer
        Try
            Dim status As Integer
            Dim STU_PASPRTNO As String = txtPNo.Text
            Dim STU_PASPRTISSPLACE As String = txtPIssPlace.Text
            Dim STU_PASPRTISSDATE As String = txtPIssDate.Text
            Dim STU_PASPRTEXPDATE As String = txtPExpDate.Text
            Dim STU_VISANO As String = txtVNo.Text
            Dim STU_VISAISSPLACE As String = txtVIssPlace.Text
            Dim STU_VISAISSDATE As String = txtVIssDate.Text
            Dim STU_VISAEXPDATE As String = txtVExpDate.Text
            Dim STU_VISAISSAUTH As String = txtVIssAuth.Text
            Dim STU_EMIRATES_ID As String = TXTEMIRATES_ID.TEXT
            Dim stu_PremisesID As String = txtPremisesID.Text
            Dim STU_EMIRATES_ID_EXPDATE As String = txtEMIRATES_IDExp_date.Text

            status = studClass.UpdateSTUDENT_M_PassportVisa(STU_ID, STU_PASPRTNO, STU_PASPRTISSPLACE, _
                     STU_PASPRTISSDATE, STU_PASPRTEXPDATE, STU_VISANO, STU_VISAISSPLACE, STU_VISAISSDATE, _
                     STU_VISAEXPDATE, STU_VISAISSAUTH, STU_EMIRATES_ID, stu_PremisesID, STU_EMIRATES_ID_EXPDATE, trans)

            Return status
        Catch ex As Exception
            Return 1000
        End Try
    End Function

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Call Student_M_Details(Session("STU_ID"))
    End Sub
End Class
