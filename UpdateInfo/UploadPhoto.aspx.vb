Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO
Imports System.Web.Configuration
Imports System.Drawing

Partial Class UpdateInfo_UploadPhoto
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("username") Is Nothing Then
            Session("Active_tab") = "Home"
            Session("Site_Path") = ""
            Response.Redirect("~\General\Home.aspx")
        ElseIf Session("bPasswdChanged") = "False" Then
            Response.Redirect("~\UpdateInfo\ForcePasswordChange.aspx", False)
        ElseIf Session("bUpdateContactDetails") = "False" Then
            Response.Redirect(Session("ForceUpdate_stud"), False)
        End If


        ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnSave)

        'AddHandler btnSave.Click, AddressOf btnSave_Click
        Try
            If Page.IsPostBack = False Then
                lbChildName.Text = Session("STU_NAME")
                ViewState("EMPPHOTOFILEPATHoldPath") = ""
                ViewState("EMPPHOTOFILEPATH") = ""
                'FUUploadPhoto.Attributes.Add("onBlur", "javascript:UploadPhoto();")
                Dim CurBsUnit As String = Session("STU_BSU_ID")

                Call Student_M_Details(Session("STU_ID"))

                Dim isParUploadEnabled As Boolean = False

                isParUploadEnabled = GetParentPhotoUploadEnabled()

                If (isParUploadEnabled) Then
                    tblFather.Visible = True
                    tblMother.Visible = True
                    tblGuardian.Visible = True
                Else
                    tblFather.Visible = False
                    tblMother.Visible = False
                    tblGuardian.Visible = False
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)
    End Sub
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.EnablePartialRendering = False
    End Sub
    Sub Student_M_Details(ByVal Stud_No As String)

        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim stuId As String = String.Empty
            stuId = Session("STU_ID")
            ''  Dim str_Sql As String = "SELECT STU_PHOTOPATH  FROM STUDENT_M WHERE STU_ID= " & stuId & ""
            Dim param(1) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@STU_ID", Session("STU_ID"))
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "Get_STUDENT_PARENT_PHOTOS", param)
            Dim strPath As String = Convert.ToString(ds.Tables(0).Rows(0)("STU_PHOTOPATH"))
            Dim strFatherPhoto = Convert.ToString(ds.Tables(0).Rows(0)("STS_FEMIRATESID_PHOTO"))
            Dim strMotherPhoto = Convert.ToString(ds.Tables(0).Rows(0)("STS_MEMIRATESID_PHOTO"))
            Dim strGuardianPhoto = Convert.ToString(ds.Tables(0).Rows(0)("STS_GEMIRATESID_PHOTO"))
            Dim connPath As String = WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ConnectionString
            Dim strImagePath As String = String.Empty

            Dim strfImagePath As String = String.Empty
            Dim strmImagePath As String = String.Empty
            Dim strgImagePath As String = String.Empty

            If strPath <> "" Then
                strImagePath = connPath & strPath
                imgParentImage.ImageUrl = "~\UpdateInfo\GetStudentPhoto.aspx?id=" & Encr_decrData.Encrypt(Session("STU_ID"))
            Else
                imgParentImage.ImageUrl = "~\UpdateInfo\GetStudentPhoto.aspx?id=" & Encr_decrData.Encrypt(Session("STU_ID"))
            End If


            If strFatherPhoto <> "" Then
                strfImagePath = connPath & strFatherPhoto
                imgFatherPhoto.ImageUrl = strfImagePath
            Else
                imgParentImage.ImageUrl = "~\UpdateInfo\GetStudentPhoto.aspx?id=" & Encr_decrData.Encrypt(Session("STU_ID"))
            End If


            If strMotherPhoto <> "" Then
                strmImagePath = connPath & strMotherPhoto
                imgMother.ImageUrl = strmImagePath
            Else
                imgParentImage.ImageUrl = "~\UpdateInfo\GetStudentPhoto.aspx?id=" & Encr_decrData.Encrypt(Session("STU_ID"))
            End If


            If strGuardianPhoto <> "" Then
                strgImagePath = connPath & strGuardianPhoto
                imgGuardian.ImageUrl = strgImagePath
            Else
                imgParentImage.ImageUrl = "~\UpdateInfo\GetStudentPhoto.aspx?id=" & Encr_decrData.Encrypt(Session("STU_ID"))
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Function callTrans_Student_M(ByVal trans As SqlTransaction) As Integer
        Try
            Dim status As Integer
            Dim STU_ID As String = Session("STU_ID")
            Dim STU_BSU_ID As String = Session("STU_BSU_ID")

            Dim PHOTO_PATH As String = String.Empty

            Dim ConFigPath As String = WebConfigurationManager.ConnectionStrings("EmpFilepath").ConnectionString

            If ViewState("EMPPHOTOFILEPATH") <> "" Then

                If Not Directory.Exists(ConFigPath & "\" & Session("STU_BSU_ID") & "\" & Session("STU_ID") & "\") Then
                    Directory.CreateDirectory(ConFigPath & "\" & Session("STU_BSU_ID") & "\" & Session("STU_ID") & "\")
                Else
                    Dim dold As New DirectoryInfo(ConFigPath & "\" & Session("STU_BSU_ID") & "\" & Session("STU_ID") & "\")

                    Dim fiold() As System.IO.FileInfo
                    fiold = dold.GetFiles("*.*", SearchOption.TopDirectoryOnly)
                    If fiold.Length > 0 Then '' If Having Attachments
                        For Each f As System.IO.FileInfo In fiold
                            f.Delete()
                        Next
                    End If




                    Dim d As New DirectoryInfo(ConFigPath & "\" & Session("STU_BSU_ID") & "\" & Session("STU_ID") & "\Temp\")

                    Dim fi() As System.IO.FileInfo
                    fi = d.GetFiles("*.*", SearchOption.TopDirectoryOnly)
                    If fi.Length > 0 Then '' If Having Attachments
                        For Each f As System.IO.FileInfo In fi

                            f.MoveTo(ConFigPath + ViewState("EMPPHOTOFILEPATH"))
                            PHOTO_PATH = ViewState("EMPPHOTOFILEPATH")


                        Next
                    End If

                    ViewState("EMPPHOTOFILEPATHoldPath") = ""
                    ViewState("EMPPHOTOFILEPATH") = ""
                End If
            End If





            If PHOTO_PATH <> "" Then
                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim param(3) As SqlClient.SqlParameter
                param(0) = New SqlClient.SqlParameter("@STU_ID", Session("STU_ID"))
                param(1) = New SqlClient.SqlParameter("@PHOTO_PATH", PHOTO_PATH)
                param(2) = New SqlClient.SqlParameter("@User", Session("username"))
                param(3) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
                param(3).Direction = ParameterDirection.ReturnValue
                'Dim cmd As New SqlCommand
                'cmd.CommandText = "saveStudentPhotoPath"
                'cmd.CommandType = CommandType.StoredProcedure
                'cmd.Connection = New SqlConnection(str_conn)
                'cmd.Parameters.AddRange(param)
                'cmd.ExecuteNonQuery()

                SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "saveStudentPhotoPath", param)

                status = param(3).Value

                ' '' nahyan to insert image as bas64 to crm integration 13SEP2017
                'Dim imagebase64 As String = Convert.ToBase64String(System.IO.File.ReadAllBytes(ConFigPath & "\" & Session("STU_BSU_ID") & "\" & Session("STU_ID") & "/STUPHOTO.jpg"))

                'SavePhotoToCRMINTEGRATION(imagebase64)
                ''nahyan ends here 
                'saveStudentPhotoPath
                'Dim str_Sql As String = "UPDATE STUDENT_M SET STU_PHOTOPATH='" & PHOTO_PATH & "'  WHERE STU_ID= " & Session("ACTIVE_STU_ID") & ""
                'status = SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_Sql)
                Return status
            Else
                Return 1000
            End If
        Catch ex As Exception
            Return 1000
        End Try
    End Function

    Function callTrans_Parent_M(ByVal trans As SqlTransaction, ByVal pType As Integer) As Integer
        Try
            'ViewState("ParentPHOTOFILEPATHoldPath") = ""
            'ViewState("ParentPHOTOFILEPATH") = ""
            Dim status As Integer
            Dim STU_ID As String = Session("STU_ID")
            Dim STU_BSU_ID As String = Session("STU_BSU_ID")

            Dim PHOTO_PATH As String = String.Empty

            Dim ConFigPath As String = WebConfigurationManager.ConnectionStrings("EmpFilepath").ConnectionString
            ' "/" & Session("STU_BSU_ID") & "/" & Session("STU_ID") & "/STUPHOTO/EMIRATESID/FATHER/" & "PARENTPHOTO"
            ''If ViewState("ParentPHOTOFILEPATH") <> "" Then
            ''    If pType = 1 Then
            ''        If Not Directory.Exists(ConFigPath & "\" & Session("STU_BSU_ID") & "\" & Session("STU_ID") & "\STUPHOTO\EMIRATESID\FATHER\") Then
            ''            Directory.CreateDirectory(ConFigPath & "\" & Session("STU_BSU_ID") & "\" & Session("STU_ID") & "\STUPHOTO\EMIRATESID\FATHER\")
            ''        End If
            ''    ElseIf pType = 2 Then
            ''        If Not Directory.Exists(ConFigPath & "\" & Session("STU_BSU_ID") & "\" & Session("STU_ID") & "\STUPHOTO\EMIRATESID\MOTHER\") Then
            ''            Directory.CreateDirectory(ConFigPath & "\" & Session("STU_BSU_ID") & "\" & Session("STU_ID") & "\STUPHOTO\EMIRATESID\MOTHER\")
            ''        End If
            ''    ElseIf pType = 3 Then
            ''        If Not Directory.Exists(ConFigPath & "\" & Session("STU_BSU_ID") & "\" & Session("STU_ID") & "\STUPHOTO\EMIRATESID\GUARDIAN\") Then
            ''            Directory.CreateDirectory(ConFigPath & "\" & Session("STU_BSU_ID") & "\" & Session("STU_ID") & "\STUPHOTO\EMIRATESID\GUARDIAN\")
            ''        End If
            ''    End If
            ''    ' Dim dold As New DirectoryInfo(ConFigPath & "\" & Session("STU_BSU_ID") & "\" & Session("STU_ID") & "\")

            ''    Dim fiold() As System.IO.FileInfo

            ''    'Dim d As New DirectoryInfo(ConFigPath & "\" & Session("STU_BSU_ID") & "\" & Session("STU_ID") & "\Temp\")
            ''    Dim fi() As System.IO.FileInfo

            ''    If pType = 1 Then
            ''        Dim dold As New DirectoryInfo(ConFigPath & "\" & Session("STU_BSU_ID") & "\" & Session("STU_ID") & "\STUPHOTO\EMIRATESID\FATHER\")
            ''        Dim d As New DirectoryInfo(ConFigPath & "\" & Session("STU_BSU_ID") & "\" & Session("STU_ID") & "\Temp\FATHER\")

            ''        fiold = dold.GetFiles("*.*", SearchOption.TopDirectoryOnly)
            ''        If fiold.Length > 0 Then '' If Having Attachments
            ''            For Each f As System.IO.FileInfo In fiold
            ''                f.Delete()
            ''            Next
            ''        End If

            ''        fi = d.GetFiles("*.*", SearchOption.TopDirectoryOnly)
            ''        If fi.Length > 0 Then '' If Having Attachments
            ''            For Each f As System.IO.FileInfo In fi

            ''                f.MoveTo(ConFigPath + ViewState("ParentPHOTOFILEPATH"))
            ''                PHOTO_PATH = ViewState("ParentPHOTOFILEPATH")


            ''            Next
            ''        End If

            ''        ViewState("ParentPHOTOFILEPATHoldPath") = ""
            ''        ViewState("ParentPHOTOFILEPATH") = ""


            ''    End If

            ''    If pType = 2 Then
            ''        Dim dold As New DirectoryInfo(ConFigPath & "\" & Session("STU_BSU_ID") & "\" & Session("STU_ID") & "\STUPHOTO\EMIRATESID\MOTHER\")
            ''        Dim d As New DirectoryInfo(ConFigPath & "\" & Session("STU_BSU_ID") & "\" & Session("STU_ID") & "\Temp\MOTHER\")

            ''        fiold = dold.GetFiles("*.*", SearchOption.TopDirectoryOnly)
            ''        If fiold.Length > 0 Then '' If Having Attachments
            ''            For Each f As System.IO.FileInfo In fiold
            ''                f.Delete()
            ''            Next
            ''        End If

            ''        fi = d.GetFiles("*.*", SearchOption.TopDirectoryOnly)
            ''        If fi.Length > 0 Then '' If Having Attachments
            ''            For Each f As System.IO.FileInfo In fi

            ''                f.MoveTo(ConFigPath + ViewState("ParentPHOTOFILEPATH"))
            ''                PHOTO_PATH = ViewState("ParentPHOTOFILEPATH")


            ''            Next
            ''        End If

            ''        ViewState("ParentPHOTOFILEPATHoldPath") = ""
            ''        ViewState("ParentPHOTOFILEPATH") = ""


            ''    End If

            ''    If pType = 3 Then
            ''        Dim dold As New DirectoryInfo(ConFigPath & "\" & Session("STU_BSU_ID") & "\" & Session("STU_ID") & "\STUPHOTO\EMIRATESID\GUARDIAN\")
            ''        Dim d As New DirectoryInfo(ConFigPath & "\" & Session("STU_BSU_ID") & "\" & Session("STU_ID") & "\Temp\GUARDIAN\")

            ''        fiold = dold.GetFiles("*.*", SearchOption.TopDirectoryOnly)
            ''        If fiold.Length > 0 Then '' If Having Attachments
            ''            For Each f As System.IO.FileInfo In fiold
            ''                f.Delete()
            ''            Next
            ''        End If

            ''        fi = d.GetFiles("*.*", SearchOption.TopDirectoryOnly)
            ''        If fi.Length > 0 Then '' If Having Attachments
            ''            For Each f As System.IO.FileInfo In fi

            ''                f.MoveTo(ConFigPath + ViewState("ParentPHOTOFILEPATH"))
            ''                PHOTO_PATH = ViewState("ParentPHOTOFILEPATH")


            ''            Next
            ''        End If

            ''        ViewState("ParentPHOTOFILEPATHoldPath") = ""
            ''        ViewState("ParentPHOTOFILEPATH") = ""


            ''    End If


            'End If
            ''  End If
            Dim parentType As String
            Dim ParentNS As String
            If pType = 1 Then
                parentType = "F"
                ParentNS = "FATHER"
            ElseIf pType = 2 Then
                parentType = "M"
                ParentNS = "MOTHER"
            ElseIf pType = 3 Then
                parentType = "G"
                ParentNS = "GUARDIAN"
            End If



            If ViewState("EMPPHOTOFILEPATH") <> "" Then
                Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
                Dim param(10) As SqlClient.SqlParameter
                param(0) = New SqlClient.SqlParameter("@STU_ID", Session("STU_ID"))
                param(1) = New SqlClient.SqlParameter("@PHOTOPATH", ViewState("EMPPHOTOFILEPATH"))
                param(2) = New SqlClient.SqlParameter("@SchoolType", "1")
                param(3) = New SqlClient.SqlParameter("@User", "Parent")
                param(4) = New SqlClient.SqlParameter("@Parent", parentType)
                status = SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "UPDATE_PARENTM_PHOTOPATH", param)


                ''nahayn on 10 may2018
                'Dim imagebase64 As String = Convert.ToBase64String(System.IO.File.ReadAllBytes(ConFigPath & "\" & Session("STU_BSU_ID") & "\" & Session("STU_ID") & "/STUPHOTO/EMIRATESID/" & ParentNS & "/PARENTPHOTO.jpg"))
                'Dim param1(4) As SqlClient.SqlParameter
                'param(0) = New SqlClient.SqlParameter("@SF_STUDENT_ID", ViewState("STUID"))
                'param(1) = New SqlClient.SqlParameter("@PHOTO_BASE64", imagebase64)

                'param(2) = New SqlClient.SqlParameter("@PHOTO_TYPE", parentType)
                'SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "dbo.SAVE_PHOTO_CRM_SF", param)
                Return status
            Else
                Return 1000
            End If
        Catch ex As Exception
            Return 1000
        End Try
    End Function

    Function ContainsSpecialChars(s As String) As Boolean
        Return s.IndexOfAny("[~`!@#$%^&*()-+=|{}':;,<>/?]".ToCharArray) <> -1
    End Function

    Private Function GetExtension(ByVal FileName As String) As String
        Dim Extension As String
        Dim split As String() = FileName.Split(".")
        Extension = split(split.Length - 1)
        Return Extension
    End Function
    'Private Function UpLoadPhoto() As Boolean

    '    Dim ImgUpload As Boolean = False
    '    If fileUpload.UploadedFiles.Count > 0 Then
    '        ViewState("EMPPHOTOFILEPATHoldPath") = ""
    '        ViewState("EMPPHOTOFILEPATH") = ""
    '        divUploadmsg.CssClass = ""
    '        divUploadmsg.Text = ""

    '        Try
    '            Dim strPostedFileName As String = fileUpload.UploadedFiles(0).GetName 'get the file name
    '            Dim ConFigPath As String = WebConfigurationManager.ConnectionStrings("EmpFilepath").ConnectionString
    '            Dim strExtn As String = System.IO.Path.GetExtension(strPostedFileName).ToLower

    '            If strPostedFileName <> "" And ConFigPath <> "" Then


    '                Dim intDocFileLength As Integer = fileUpload.UploadedFiles(0).ContentLength ' get the file size
    '                Dim filename As String = System.IO.Path.GetFileName(strPostedFileName).ToLower

    '                Dim Tempath As String = "\" & Session("STU_BSU_ID") & "\" & Session("STU_ID") & "\Temp\"
    '                Dim UploadfileName As String = "STUPHOTO" & strExtn

    '                Dim fileext = GetExtension(strExtn)
    '                'Checking file extension
    '                If fileext <> "" Then
    '                    If Not (fileext.ToUpper = "JPEG" Or fileext.ToUpper = "JPG") Then 'Or fileext.ToUpper = "PNG"
    '                        lblmsg.Visible = True
    '                        lblmsg.Text = "Upload JPEG/PNG Files Only!!"
    '                        ImgUpload = False
    '                        Return ImgUpload
    '                    End If
    '                End If

    '                'Checking file extension length
    '                If fileext.Length = 0 Then
    '                    lblmsg.Visible = True
    '                    lblmsg.CssClass = "alert alert-warning"
    '                    lblmsg.Text = "file with out extension not allowed...!!"
    '                    ImgUpload = False
    '                    Return ImgUpload
    '                End If

    '                'Checking Special Characters in file name
    '                Dim ContainsSplChar As Boolean = ContainsSpecialChars(strPostedFileName)
    '                If ContainsSplChar = True Then
    '                    lblmsg.Visible = True
    '                    lblmsg.CssClass = "alert alert-warning"
    '                    lblmsg.Text = "File Name with special characters are not allowed..!!"
    '                    ImgUpload = False
    '                    Return ImgUpload
    '                End If

    '                'Checking FileName length
    '                Dim FileNameLength As Integer = strPostedFileName.Length
    '                If FileNameLength = 0 Or FileNameLength > 255 Then '255
    '                    lblmsg.Visible = True
    '                    lblmsg.CssClass = "alert alert-warning"
    '                    lblmsg.Text = "Error with file Name Length..!!"
    '                    ImgUpload = False
    '                    Return ImgUpload
    '                End If

    '                Dim s As String = strPostedFileName
    '                Dim result() As String
    '                result = s.Split(".")
    '                If result.Length > 2 Then
    '                    lblmsg.Visible = True
    '                    lblmsg.CssClass = "alert alert-warning"
    '                    lblmsg.Text = "Invalid file Name..!!"
    '                    ImgUpload = False
    '                    Return ImgUpload


    '                End If


    '                If fileUpload.UploadedFiles(0).ContentLength > 50000 Then
    '                    lblmsg.Visible = True
    '                    lblmsg.CssClass = "alert alert-warning"
    '                    lblmsg.Text = "The Size of file is greater than 50 KB"
    '                    ImgUpload = False
    '                    Return ImgUpload
    '                End If

    '                Dim SqlCon As SqlConnection = ConnectionManger.GetOASISConnection
    '                Dim ds As New DataSet
    '                Dim sqlQuery As String = "SELECT SYS_ImgPhotoSize FROM dbo.vw_OSF_SYSINFO_S"
    '                ds = SqlHelper.ExecuteDataset(SqlCon, CommandType.Text, sqlQuery)
    '                If ds.Tables(0).Rows.Count > 0 Then
    '                    Dim iImgSize As Integer = ds.Tables(0).Rows(0)("SYS_ImgPhotoSize")
    '                    ''image size should be 50kb
    '                    iImgSize = 51200
    '                    If intDocFileLength > iImgSize Then
    '                        'divUploadmsg.CssClass = "diverrorUpload"
    '                        divUploadmsg.CssClass = "alert alert-info"
    '                        divUploadmsg.Text = "Select image size maximum 50KB"
    '                        ImgUpload = False
    '                        Return ImgUpload
    '                    End If
    '                End If

    '                If (strExtn <> ".jpg" OrElse strExtn <> ".jpeg") Then 'exten type
    '                    'divUploadmsg.CssClass = "diverrorUpload"
    '                    divUploadmsg.CssClass = "alert alert-info"
    '                    divUploadmsg.Text = "File type is different than allowed!"
    '                    ImgUpload = False
    '                    Return ImgUpload
    '                End If

    '                If (strPostedFileName <> String.Empty) Then

    '                    If Not Directory.Exists(ConFigPath + Tempath) Then
    '                        Directory.CreateDirectory(ConFigPath + Tempath)
    '                    Else
    '                        Dim d As New DirectoryInfo(ConFigPath + Tempath)

    '                        Dim fi() As System.IO.FileInfo
    '                        fi = d.GetFiles("*.*", SearchOption.TopDirectoryOnly)
    '                        If fi.Length > 0 Then '' If Having Attachments
    '                            For Each f As System.IO.FileInfo In fi
    '                                f.Delete()
    '                            Next
    '                        End If

    '                    End If


    '                    fileUpload.UploadedFiles(0).SaveAs(ConFigPath + Tempath & System.IO.Path.GetFileName(UploadfileName))

    '                    ViewState("EMPPHOTOFILEPATHoldPath") = ConFigPath + Tempath & UploadfileName
    '                    ViewState("EMPPHOTOFILEPATH") = "/" & Session("STU_BSU_ID") & "/" & Session("STU_ID") & "/" & "STUPHOTO" & strExtn

    '                    ' hf_reload.Value = "1"
    '                    'divUploadmsg.CssClass = "divvalidUpload"
    '                    divUploadmsg.CssClass = "alert alert-success"
    '                    divUploadmsg.Text = filename
    '                    hfUploadPath.Value = ""

    '                Else
    '                    ViewState("EMPPHOTOFILEPATHoldPath") = ""
    '                    ViewState("EMPPHOTOFILEPATH") = ""
    '                    'divUploadmsg.CssClass = "diverrorUpload"
    '                    divUploadmsg.CssClass = "alert alert-info"
    '                    divUploadmsg.Text = "There is no file to upload."
    '                End If

    '            End If
    '        Catch ex As Exception
    '            ViewState("EMPPHOTOFILEPATHoldPath") = ""
    '            ViewState("EMPPHOTOFILEPATH") = ""
    '            UtilityObj.Errorlog(ex.Message, "btnProcess_Click")

    '            'divUploadmsg.CssClass = "diverrorUpload"
    '            divUploadmsg.CssClass = "alert alert-danger"
    '            divUploadmsg.Text = "Error while updating the file!"
    '            imgParentImage.ImageUrl = Server.MapPath("~/ParentLogin/Images/no_image.gif")
    '            imgParentImage.AlternateText = "No Image found"
    '        End Try

    '    End If

    '    Return ImgUpload

    'End Function

    Private Sub UpLoadPhoto()


        If fileUpload.FileName <> "" Then
            ViewState("EMPPHOTOFILEPATHoldPath") = ""
            ViewState("EMPPHOTOFILEPATH") = ""
            divUploadmsg.CssClass = ""
            divUploadmsg.Text = ""

            Try


                'Dim intDocFileLength As Integer = fileUpload.UploadedFiles(0).ContentLength ' get the file size

                Dim strPostedFileName As String = fileUpload.PostedFile.FileName 'get the file name
                Dim filename As String = System.IO.Path.GetFileName(strPostedFileName).ToLower
                Dim strExtn As String = System.IO.Path.GetExtension(strPostedFileName).ToLower

                If Not (UCase(strExtn) = ".JPG" Or UCase(strExtn) = ".JPEG") Then
                    ''Throw New Exception
                    divUploadmsg.Text = "Select JPG Image Only"
                    lblmsg.CssClass = "alert alert-info"
                    lblmsg.Text = "Select JPG Image Only"
                    Exit Sub
                End If



                Dim Tempath As String = "\" & Session("STU_BSU_ID") & "\" & Session("STU_ID") & "\Temp\"
                Dim UploadfileName As String = "STUPHOTO" & strExtn
                Dim ConFigPath As String = WebConfigurationManager.ConnectionStrings("EmpFilepath").ConnectionString



                Dim SqlCon As SqlConnection = ConnectionManger.GetOASISConnection
                Dim ds As New DataSet
                Dim sqlQuery As String = "SELECT SYS_ImgPhotoSize FROM dbo.vw_OSF_SYSINFO_S"
                ds = SqlHelper.ExecuteDataset(SqlCon, CommandType.Text, sqlQuery)
                If ds.Tables(0).Rows.Count > 0 Then
                    Dim iImgSize As Integer = ds.Tables(0).Rows(0)("SYS_ImgPhotoSize")
                    ''image size should be 1mb on 19sep by charan/nahyan
                    iImgSize = 1048576
                    If fileUpload.PostedFile.ContentLength > iImgSize Then
                        divUploadmsg.CssClass = "alert alert-info"
                        divUploadmsg.Text = "Select image size maximum 1mb"
                        lblmsg.CssClass = "alert alert-info"
                        lblmsg.Text = "Select image size maximum 1mb"
                        Exit Sub
                    End If
                End If

                'If (strExtn <> ".jpg" OrElse strExtn <> ".jpeg") Then 'exten type
                '    divUploadmsg.CssClass = "diverrorUpload"
                '    divUploadmsg.Text = "File type is different than allowed!"
                '    Exit Sub
                'End If

                'Dim fs As New FileInfo(radFatherPhoto.PostedFile.FileName)

                If (strPostedFileName <> String.Empty) Then

                    If Not Directory.Exists(ConFigPath + Tempath) Then
                        Directory.CreateDirectory(ConFigPath + Tempath)
                    Else
                        Dim d As New DirectoryInfo(ConFigPath + Tempath)

                        Dim fi() As System.IO.FileInfo
                        fi = d.GetFiles("*.*", SearchOption.TopDirectoryOnly)
                        If fi.Length > 0 Then '' If Having Attachments
                            For Each f As System.IO.FileInfo In fi
                                f.Delete()
                            Next
                        End If

                    End If
                    

                    ' UtilityObj.Errorlog("file uploading1", "UpLoadPhoto")
                    'fileUpload.UploadedFiles(0).SaveAs(ConFigPath + Tempath & System.IO.Path.GetFileName(UploadfileName))
                    fileUpload.PostedFile.SaveAs(ConFigPath + Tempath & System.IO.Path.GetFileName(UploadfileName))
                    ' UtilityObj.Errorlog("file uploading2", "UpLoadPhoto")

                    ViewState("EMPPHOTOFILEPATHoldPath") = ConFigPath + Tempath & UploadfileName
                    ViewState("EMPPHOTOFILEPATH") = "/" & Session("STU_BSU_ID") & "/" & Session("STU_ID") & "/" & "STUPHOTO" & strExtn
                    ' UtilityObj.Errorlog(ViewState("EMPPHOTOFILEPATH"), "UpLoadPhoto")

                    ' hf_reload.Value = "1"
                    divUploadmsg.CssClass = "alert alert-warning"
                    divUploadmsg.Text = filename
                    hfUploadPath.Value = ""

                Else
                    ViewState("EMPPHOTOFILEPATHoldPath") = ""
                    ViewState("EMPPHOTOFILEPATH") = ""
                    divUploadmsg.CssClass = "alert alert-warning"
                    divUploadmsg.Text = "There is no file to upload."
                End If


            Catch ex As Exception
                ViewState("EMPPHOTOFILEPATHoldPath") = ""
                ViewState("EMPPHOTOFILEPATH") = ""
                UtilityObj.Errorlog(ex.Message, "btnProcess_Click")

                divUploadmsg.CssClass = "alert alert-danger"
                divUploadmsg.Text = "Error while updating the file!"
                imgParentImage.ImageUrl = Server.MapPath("~/ParentLogin/Images/no_image.gif")
                imgParentImage.AlternateText = "No Image found"
            End Try

        End If



    End Sub
    Private Sub UpLoadParentPhoto(ByVal pType As Integer)

        If pType = 1 Then


            If radFatherPhoto.FileName <> "" Then

                ''If Not (radFatherPhoto.PostedFile.ContentType.Contains("application/jpeg") OrElse radFatherPhoto.PostedFile.ContentType.Contains("application/jpg")) Then
                If Not (radFatherPhoto.PostedFile.ContentType.Contains("application/jpeg") OrElse radFatherPhoto.PostedFile.ContentType.Contains("application/jpg") OrElse radFatherPhoto.PostedFile.ContentType.Contains("image/jpeg") OrElse radFatherPhoto.PostedFile.ContentType.Contains("image/jpg")) Then
                    ''Throw New Exception
                    lblFather.Text = "Select JPG Image Only"
                    lblmsg.CssClass = "alert alert-info"
                    lblmsg.Text = "Select JPG Image Only"
                    Exit Sub
                End If

                If radFatherPhoto.HasFile Then

                    Dim SqlCon As SqlConnection = ConnectionManger.GetOASISConnection
                    Dim ds As New DataSet
                    Dim sqlQuery As String = "SELECT SYS_ImgPhotoSize FROM dbo.vw_OSF_SYSINFO_S"
                    ds = SqlHelper.ExecuteDataset(SqlCon, CommandType.Text, sqlQuery)
                    If ds.Tables(0).Rows.Count > 0 Then
                        Dim iImgSize As Integer = ds.Tables(0).Rows(0)("SYS_ImgPhotoSize")
                        ''image size should be 1mb on 19sep by charan/nahyan
                        iImgSize = 1048576
                        If radFatherPhoto.PostedFile.ContentLength > iImgSize Then
                            lblFather.Text = "Select Image Size Maximum 1mb"
                            lblmsg.CssClass = "alert alert-info"
                            lblmsg.Text = "Select Image Size Maximum 1mb"
                            Exit Sub
                        End If
                    End If
                End If
                Dim str_img As String = WebConfigurationManager.ConnectionStrings("empfilepath").ConnectionString
                Dim str_imgvirtual As String = WebConfigurationManager.ConnectionStrings("empfilepathvirtual").ConnectionString
                Dim fs As New FileInfo(radFatherPhoto.PostedFile.FileName)

                If Not Directory.Exists(str_img & "\" & Session("sbsuid") & "\" & Session("STU_ID") & "\STUPHOTO\EMIRATESID\FATHER\") Then
                    Directory.CreateDirectory(str_img & "\" & Session("sbsuid") & "\" & Session("STU_ID") & "\STUPHOTO\EMIRATESID\FATHER\")
                Else
                    Dim d As New DirectoryInfo(str_img & "\" & Session("sbsuid") & "\" & Session("STU_ID") & "\STUPHOTO\EMIRATESID\FATHER\")

                    Dim fi() As System.IO.FileInfo
                    fi = d.GetFiles("*.*", SearchOption.TopDirectoryOnly)
                    If fi.Length > 0 Then '' If Having Attachments
                        For Each f As System.IO.FileInfo In fi
                            f.Delete()
                        Next
                    End If
                End If

                Dim str_tempfilename As String = radFatherPhoto.FileName
                Dim strFilepath As String = str_img & "\" & Session("sbsuid") & "\" & Session("STU_ID") & "\" & "STUPHOTO\EMIRATESID\FATHER\" & "PARENTPHOTO" & fs.Extension
                '' nahyan to insert image as bas64 to crm integration 13SEP2017
                Dim fs1 As System.IO.Stream = radFatherPhoto.PostedFile.InputStream
                Dim br As New System.IO.BinaryReader(fs1)
                Dim bytes As Byte() = br.ReadBytes(CType(fs1.Length, Integer))
                Dim base64String As String = Convert.ToBase64String(bytes, 0, bytes.Length)
                SavePhotoToCRMINTEGRATION(base64String)
                ''nahyan ends here 
                ''ImgHeightnWidth(strFilepath)
                radFatherPhoto.PostedFile.SaveAs(strFilepath)
                Try
                    'If Not radFatherPhoto.PostedFile.ContentType.Contains("application/jpeg") Then
                    '    Throw New Exception
                    'End If
                    imgFatherPhoto.ImageUrl = str_imgvirtual & "\" & Session("sbsuid") & "\" & Session("STU_ID") & "\STUPHOTO\EMIRATESID\FATHER\" & "PARENTPHOTO" & fs.Extension
                    ViewState("EMPPHOTOFILEPATHoldPath") = str_img & "\" & Session("sbsuid") & "\" & Session("STU_ID") & "\STUPHOTO\EMIRATESID\FATHER\" & "PARENTPHOTO" & fs.Extension
                    ViewState("EMPPHOTOFILEPATH") = "\" & Session("sbsuid") & "\" & Session("STU_ID") & "\STUPHOTO\EMIRATESID\FATHER\" & "PARENTPHOTO" & fs.Extension
                Catch ex As Exception
                    'File.Delete(strFilepath)
                    ' hfParent.Value = ""
                    UtilityObj.Errorlog("No Image found")
                    imgFatherPhoto.ImageUrl = Server.MapPath("~/ParentLogin/Images/no_image.gif")
                    imgFatherPhoto.AlternateText = "No Image found"
                End Try

            End If
        End If
        If pType = 2 Then


            If radMotherPhoto.FileName <> "" Then

                '' If Not (radMotherPhoto.PostedFile.ContentType.Contains("application/jpeg") OrElse radMotherPhoto.PostedFile.ContentType.Contains("application/jpg")) Then
                If Not (radMotherPhoto.PostedFile.ContentType.Contains("application/jpeg") OrElse radMotherPhoto.PostedFile.ContentType.Contains("application/jpg") OrElse radMotherPhoto.PostedFile.ContentType.Contains("image/jpeg") OrElse radMotherPhoto.PostedFile.ContentType.Contains("image/jpg")) Then
                    ''Throw New Exception
                    lblMother.Text = "Select JPG Image Only"
                    lblmsg.CssClass = "alert alert-info"
                    lblmsg.Text = "Select JPG Image Only"
                    Exit Sub
                End If

                If radMotherPhoto.HasFile Then

                    Dim SqlCon As SqlConnection = ConnectionManger.GetOASISConnection
                    Dim ds As New DataSet
                    Dim sqlQuery As String = "SELECT SYS_ImgPhotoSize FROM dbo.vw_OSF_SYSINFO_S"
                    ds = SqlHelper.ExecuteDataset(SqlCon, CommandType.Text, sqlQuery)
                    If ds.Tables(0).Rows.Count > 0 Then
                        Dim iImgSize As Integer = ds.Tables(0).Rows(0)("SYS_ImgPhotoSize")
                        ''image size should be 1mb on 19sep by charan
                        iImgSize = 1048576
                        If radMotherPhoto.PostedFile.ContentLength > iImgSize Then
                            lblMother.Text = "Select Image Size Maximum 1mb"
                            lblmsg.CssClass = "alert alert-info"
                            lblmsg.Text = "Select Image Size Maximum 1mb"
                            Exit Sub
                        End If
                    End If
                End If
                Dim str_img As String = WebConfigurationManager.ConnectionStrings("empfilepath").ConnectionString
                Dim str_imgvirtual As String = WebConfigurationManager.ConnectionStrings("empfilepathvirtual").ConnectionString
                Dim fs As New FileInfo(radMotherPhoto.PostedFile.FileName)

                If Not Directory.Exists(str_img & "\" & Session("sbsuid") & "\" & Session("STU_ID") & "\STUPHOTO\EMIRATESID\MOTHER\") Then
                    Directory.CreateDirectory(str_img & "\" & Session("sbsuid") & "\" & Session("STU_ID") & "\STUPHOTO\EMIRATESID\MOTHER\")
                Else
                    Dim d As New DirectoryInfo(str_img & "\" & Session("sbsuid") & "\" & Session("STU_ID") & "\STUPHOTO\EMIRATESID\MOTHER\")

                    Dim fi() As System.IO.FileInfo
                    fi = d.GetFiles("*.*", SearchOption.TopDirectoryOnly)
                    If fi.Length > 0 Then '' If Having Attachments
                        For Each f As System.IO.FileInfo In fi
                            f.Delete()
                        Next
                    End If
                End If

                Dim str_tempfilename As String = radMotherPhoto.FileName
                Dim strFilepath As String = str_img & "\" & Session("sbsuid") & "\" & Session("STU_ID") & "\" & "STUPHOTO\EMIRATESID\MOTHER\" & "PARENTPHOTO" & fs.Extension
                '' nahyan to insert image as bas64 to crm integration 13SEP2017
                Dim fs1 As System.IO.Stream = radMotherPhoto.PostedFile.InputStream
                Dim br As New System.IO.BinaryReader(fs1)
                Dim bytes As Byte() = br.ReadBytes(CType(fs1.Length, Integer))
                Dim base64String As String = Convert.ToBase64String(bytes, 0, bytes.Length)
                SavePhotoToCRMINTEGRATION(base64String)
                ''nahyan ends here 
                ''ImgHeightnWidth(strFilepath)
                radMotherPhoto.PostedFile.SaveAs(strFilepath)
                Try
                    'If Not radMotherPhoto.PostedFile.ContentType.Contains("application/jpeg") Then
                    '    Throw New Exception
                    'End If
                    imgMother.ImageUrl = str_imgvirtual & "\" & Session("sbsuid") & "\" & Session("STU_ID") & "\STUPHOTO\EMIRATESID\MOTHER\" & "PARENTPHOTO" & fs.Extension
                    ViewState("EMPPHOTOFILEPATHoldPath") = str_img & "\" & Session("sbsuid") & "\" & Session("STU_ID") & "\STUPHOTO\EMIRATESID\MOTHER\" & "PARENTPHOTO" & fs.Extension
                    ViewState("EMPPHOTOFILEPATH") = "\" & Session("sbsuid") & "\" & Session("STU_ID") & "\STUPHOTO\EMIRATESID\MOTHER\" & "PARENTPHOTO" & fs.Extension
                Catch ex As Exception
                    'File.Delete(strFilepath)
                    ' hfParent.Value = ""
                    UtilityObj.Errorlog("No Image found")
                    imgMother.ImageUrl = Server.MapPath("~/ParentLogin/Images/no_image.gif")
                    imgMother.AlternateText = "No Image found"
                End Try

            End If
        End If

        If pType = 3 Then


            If radGuardian.FileName <> "" Then

                ' If Not (radGuardian.PostedFile.ContentType.Contains("application/jpeg") OrElse radGuardian.PostedFile.ContentType.Contains("application/jpg") OrElse radGuardian.PostedFile.ContentType.Contains("image/jpeg")) Then
                If Not (radGuardian.PostedFile.ContentType.Contains("application/jpeg") OrElse radGuardian.PostedFile.ContentType.Contains("application/jpg") OrElse radGuardian.PostedFile.ContentType.Contains("image/jpeg") OrElse radGuardian.PostedFile.ContentType.Contains("image/jpg")) Then
                    ''Throw New Exception
                    lblGuardian.Text = "Select JPG Image Only"
                    lblmsg.CssClass = "alert alert-info"
                    lblmsg.Text = "Select JPG Image Only"
                    Exit Sub
                End If

                If radGuardian.HasFile Then

                    Dim SqlCon As SqlConnection = ConnectionManger.GetOASISConnection
                    Dim ds As New DataSet
                    Dim sqlQuery As String = "SELECT SYS_ImgPhotoSize FROM dbo.vw_OSF_SYSINFO_S"
                    ds = SqlHelper.ExecuteDataset(SqlCon, CommandType.Text, sqlQuery)
                    If ds.Tables(0).Rows.Count > 0 Then
                        Dim iImgSize As Integer = ds.Tables(0).Rows(0)("SYS_ImgPhotoSize")
                        ''image size should be 1mb nahyan on 19sep-advised by charan
                        iImgSize = 1048576
                        If radGuardian.PostedFile.ContentLength > iImgSize Then
                            lblGuardian.Text = "Select Image Size Maximum 1mb"
                            lblmsg.CssClass = "alert alert-info"
                            lblmsg.Text = "Select Image Size Maximum 1mb"
                            Exit Sub
                        End If
                    End If
                End If
                Dim str_img As String = WebConfigurationManager.ConnectionStrings("empfilepath").ConnectionString
                Dim str_imgvirtual As String = WebConfigurationManager.ConnectionStrings("empfilepathvirtual").ConnectionString
                Dim fs As New FileInfo(radGuardian.PostedFile.FileName)

                If Not Directory.Exists(str_img & "\" & Session("sbsuid") & "\" & Session("STU_ID") & "\STUPHOTO\EMIRATESID\GUARDIAN\") Then
                    Directory.CreateDirectory(str_img & "\" & Session("sbsuid") & "\" & Session("STU_ID") & "\STUPHOTO\EMIRATESID\GUARDIAN\")
                Else
                    Dim d As New DirectoryInfo(str_img & "\" & Session("sbsuid") & "\" & Session("STU_ID") & "\STUPHOTO\EMIRATESID\GUARDIAN\")

                    Dim fi() As System.IO.FileInfo
                    fi = d.GetFiles("*.*", SearchOption.TopDirectoryOnly)
                    If fi.Length > 0 Then '' If Having Attachments
                        For Each f As System.IO.FileInfo In fi
                            f.Delete()
                        Next
                    End If
                End If

                Dim str_tempfilename As String = radFatherPhoto.FileName
                Dim strFilepath As String = str_img & "\" & Session("sbsuid") & "\" & Session("STU_ID") & "\" & "STUPHOTO\EMIRATESID\GUARDIAN\" & "PARENTPHOTO" & fs.Extension
                '' nahyan to insert image as bas64 to crm integration 13SEP2017
                Dim fs1 As System.IO.Stream = radGuardian.PostedFile.InputStream
                Dim br As New System.IO.BinaryReader(fs1)
                Dim bytes As Byte() = br.ReadBytes(CType(fs1.Length, Integer))
                Dim base64String As String = Convert.ToBase64String(bytes, 0, bytes.Length)
                SavePhotoToCRMINTEGRATION(base64String)
                ''nahyan ends here 
                ''ImgHeightnWidth(strFilepath)
                radGuardian.PostedFile.SaveAs(strFilepath)

                ''resizing of uploaded image
                ResizeImage(strFilepath, 120, 140)
                ''ends here
                Try
                    'If Not radGuardian.PostedFile.ContentType.Contains("application/jpeg") Then
                    '    Throw New Exception
                    'End If
                    imgGuardian.ImageUrl = str_imgvirtual & "\" & Session("sbsuid") & "\" & Session("STU_ID") & "\STUPHOTO\EMIRATESID\GUARDIAN\" & "PARENTPHOTO" & fs.Extension
                    ViewState("EMPPHOTOFILEPATHoldPath") = str_img & "\" & Session("sbsuid") & "\" & Session("STU_ID") & "\STUPHOTO\EMIRATESID\GUARDIAN\" & "PARENTPHOTO" & fs.Extension
                    ViewState("EMPPHOTOFILEPATH") = "\" & Session("sbsuid") & "\" & Session("STU_ID") & "\STUPHOTO\EMIRATESID\GUARDIAN\" & "PARENTPHOTO" & fs.Extension
                Catch ex As Exception
                    'File.Delete(strFilepath)
                    ' hfParent.Value = ""
                    UtilityObj.Errorlog("No Image found")
                    imgGuardian.ImageUrl = Server.MapPath("~/ParentLogin/Images/no_image.gif")
                    imgGuardian.AlternateText = "No Image found"
                End Try

            End If
        End If
    End Sub

    'Private Sub UpLoadParentPhoto1(ByVal pType As Integer)

    '    ''pType 1-Father,2-Mother,3-Guadian
    '    'EMIRATESID/FATHER
    '    'EMIRATESID/MOTHER
    '    'EMIRATESID/GUARDIAN

    '    ViewState("ParentPHOTOFILEPATHoldPath") = ""
    '    ViewState("ParentPHOTOFILEPATH") = ""
    '    divUploadmsg.CssClass = ""
    '    divUploadmsg.Text = ""
    '    Dim intDocFileLength As Integer = 0
    '    Dim strPostedFileName As String = String.Empty
    '    Dim filename As String = String.Empty
    '    Dim strExtn As String = String.Empty
    '    Dim Tempath As String = String.Empty
    '    Dim UploadfileName As String = String.Empty
    '    Dim ConFigPath As String = String.Empty
    '    Dim ds As New DataSet

    '    Dim SqlCon As SqlConnection = ConnectionManger.GetOASISConnection
    '    Dim sqlQuery As String = String.Empty
    '    ConFigPath = WebConfigurationManager.ConnectionStrings("EmpFilepath").ConnectionString

    '    If pType = 1 Then
    '        If radFatherPhoto.UploadedFiles.Count > 0 Then

    '            Try


    '                intDocFileLength = radFatherPhoto.UploadedFiles(0).ContentLength ' get the file size

    '                strPostedFileName = radFatherPhoto.UploadedFiles(0).GetName 'get the file name
    '                filename = System.IO.Path.GetFileName(strPostedFileName).ToLower
    '                strExtn = System.IO.Path.GetExtension(strPostedFileName).ToLower
    '                Tempath = "\" & Session("STU_BSU_ID") & "\" & Session("STU_ID") & "\Temp\FATHER\"
    '                UploadfileName = "PARENTPHOTO" & strExtn





    '                sqlQuery = "SELECT SYS_ImgPhotoSize FROM dbo.vw_OSF_SYSINFO_S"
    '                ds = SqlHelper.ExecuteDataset(SqlCon, CommandType.Text, sqlQuery)
    '                If ds.Tables(0).Rows.Count > 0 Then
    '                    Dim iImgSize As Integer = ds.Tables(0).Rows(0)("SYS_ImgPhotoSize")

    '                    If intDocFileLength > iImgSize Then
    '                        divUploadmsg.CssClass = "diverrorUpload"
    '                        divUploadmsg.Text = "Select image size maximum 20KB"
    '                        Exit Sub
    '                    End If
    '                End If

    '                If (strExtn <> ".jpg") Then 'exten type
    '                    divUploadmsg.CssClass = "diverrorUpload"
    '                    divUploadmsg.Text = "File type is different than allowed!"
    '                    Exit Sub
    '                End If

    '                If (strPostedFileName <> String.Empty) Then

    '                    If Not Directory.Exists(ConFigPath + Tempath) Then
    '                        Directory.CreateDirectory(ConFigPath + Tempath)
    '                    Else
    '                        Dim d As New DirectoryInfo(ConFigPath + Tempath)

    '                        Dim fi() As System.IO.FileInfo
    '                        fi = d.GetFiles("*.*", SearchOption.TopDirectoryOnly)
    '                        If fi.Length > 0 Then '' If Having Attachments
    '                            For Each f As System.IO.FileInfo In fi
    '                                f.Delete()
    '                            Next
    '                        End If

    '                    End If


    '                    radFatherPhoto.UploadedFiles(0).SaveAs(ConFigPath + Tempath & System.IO.Path.GetFileName(UploadfileName))

    '                    ViewState("ParentPHOTOFILEPATHoldPath") = ConFigPath + Tempath & UploadfileName
    '                    ViewState("ParentPHOTOFILEPATH") = "/" & Session("STU_BSU_ID") & "/" & Session("STU_ID") & "/STUPHOTO/EMIRATESID/FATHER/" & "PARENTPHOTO" & strExtn

    '                    ' hf_reload.Value = "1"
    '                    divUploadmsg.CssClass = "divvalidUpload"
    '                    divUploadmsg.Text = filename
    '                    hfUploadPath.Value = ""

    '                Else
    '                    ViewState("ParentPHOTOFILEPATHoldPath") = ""
    '                    ViewState("ParentPHOTOFILEPATH") = ""
    '                    divUploadmsg.CssClass = "diverrorUpload"
    '                    divUploadmsg.Text = "There is no file to upload."
    '                End If


    '            Catch ex As Exception
    '                ViewState("ParentPHOTOFILEPATHoldPath") = ""
    '                ViewState("ParentPHOTOFILEPATH") = ""
    '                UtilityObj.Errorlog(ex.Message, "btnProcess_Click")

    '                divUploadmsg.CssClass = "diverrorUpload"
    '                divUploadmsg.Text = "Error while updating the file!"
    '                imgParentImage.ImageUrl = Server.MapPath("~/ParentLogin/Images/no_image.gif")
    '                imgParentImage.AlternateText = "No Image found"
    '            End Try

    '        End If
    '    ElseIf pType = 2 Then
    '        If radMotherPhoto.UploadedFiles.Count > 0 Then

    '            Try


    '                intDocFileLength = radMotherPhoto.UploadedFiles(0).ContentLength ' get the file size

    '                strPostedFileName = radMotherPhoto.UploadedFiles(0).GetName 'get the file name
    '                filename = System.IO.Path.GetFileName(strPostedFileName).ToLower
    '                strExtn = System.IO.Path.GetExtension(strPostedFileName).ToLower
    '                Tempath = "\" & Session("STU_BSU_ID") & "\" & Session("STU_ID") & "\Temp\MOTHER\"
    '                UploadfileName = "PARENTPHOTO" & strExtn





    '                sqlQuery = "SELECT SYS_ImgPhotoSize FROM dbo.vw_OSF_SYSINFO_S"
    '                ds = SqlHelper.ExecuteDataset(SqlCon, CommandType.Text, sqlQuery)
    '                If ds.Tables(0).Rows.Count > 0 Then
    '                    Dim iImgSize As Integer = ds.Tables(0).Rows(0)("SYS_ImgPhotoSize")

    '                    If intDocFileLength > iImgSize Then
    '                        divUploadmsg.CssClass = "diverrorUpload"
    '                        divUploadmsg.Text = "Select image size maximum 20KB"
    '                        Exit Sub
    '                    End If
    '                End If

    '                If (strExtn <> ".jpg") Then 'exten type
    '                    divUploadmsg.CssClass = "diverrorUpload"
    '                    divUploadmsg.Text = "File type is different than allowed!"
    '                    Exit Sub
    '                End If

    '                If (strPostedFileName <> String.Empty) Then

    '                    If Not Directory.Exists(ConFigPath + Tempath) Then
    '                        Directory.CreateDirectory(ConFigPath + Tempath)
    '                    Else
    '                        Dim d As New DirectoryInfo(ConFigPath + Tempath)

    '                        Dim fi() As System.IO.FileInfo
    '                        fi = d.GetFiles("*.*", SearchOption.TopDirectoryOnly)
    '                        If fi.Length > 0 Then '' If Having Attachments
    '                            For Each f As System.IO.FileInfo In fi
    '                                f.Delete()
    '                            Next
    '                        End If

    '                    End If


    '                    radMotherPhoto.UploadedFiles(0).SaveAs(ConFigPath + Tempath & System.IO.Path.GetFileName(UploadfileName))

    '                    ViewState("ParentPHOTOFILEPATHoldPath") = ConFigPath + Tempath & UploadfileName
    '                    ViewState("ParentPHOTOFILEPATH") = "/" & Session("STU_BSU_ID") & "/" & Session("STU_ID") & "/STUPHOTO/EMIRATESID/MOTHER/" & "PARENTPHOTO" & strExtn

    '                    ' hf_reload.Value = "1"
    '                    divUploadmsg.CssClass = "divvalidUpload"
    '                    divUploadmsg.Text = filename
    '                    hfUploadPath.Value = ""

    '                Else
    '                    ViewState("ParentPHOTOFILEPATHoldPath") = ""
    '                    ViewState("ParentPHOTOFILEPATH") = ""
    '                    divUploadmsg.CssClass = "diverrorUpload"
    '                    divUploadmsg.Text = "There is no file to upload."
    '                End If


    '            Catch ex As Exception
    '                ViewState("ParentPHOTOFILEPATHoldPath") = ""
    '                ViewState("ParentPHOTOFILEPATH") = ""
    '                UtilityObj.Errorlog(ex.Message, "btnProcess_Click")

    '                divUploadmsg.CssClass = "diverrorUpload"
    '                divUploadmsg.Text = "Error while updating the file!"
    '                imgParentImage.ImageUrl = Server.MapPath("~/ParentLogin/Images/no_image.gif")
    '                imgParentImage.AlternateText = "No Image found"
    '            End Try

    '        End If
    '    ElseIf pType = 3 Then
    '        If radGuardianPhoto.UploadedFiles.Count > 0 Then

    '            Try


    '                intDocFileLength = radGuardianPhoto.UploadedFiles(0).ContentLength ' get the file size

    '                strPostedFileName = radGuardianPhoto.UploadedFiles(0).GetName 'get the file name
    '                filename = System.IO.Path.GetFileName(strPostedFileName).ToLower
    '                strExtn = System.IO.Path.GetExtension(strPostedFileName).ToLower
    '                Tempath = "\" & Session("STU_BSU_ID") & "\" & Session("STU_ID") & "\Temp\GUARDIAN\"
    '                UploadfileName = "PARENTPHOTO" & strExtn




    '                sqlQuery = "SELECT SYS_ImgPhotoSize FROM dbo.vw_OSF_SYSINFO_S"
    '                ds = SqlHelper.ExecuteDataset(SqlCon, CommandType.Text, sqlQuery)
    '                If ds.Tables(0).Rows.Count > 0 Then
    '                    Dim iImgSize As Integer = ds.Tables(0).Rows(0)("SYS_ImgPhotoSize")

    '                    If intDocFileLength > iImgSize Then
    '                        divUploadmsg.CssClass = "diverrorUpload"
    '                        divUploadmsg.Text = "Select image size maximum 20KB"
    '                        Exit Sub
    '                    End If
    '                End If

    '                If (strExtn <> ".jpg") Then 'exten type
    '                    divUploadmsg.CssClass = "diverrorUpload"
    '                    divUploadmsg.Text = "File type is different than allowed!"
    '                    Exit Sub
    '                End If

    '                If (strPostedFileName <> String.Empty) Then

    '                    If Not Directory.Exists(ConFigPath + Tempath) Then
    '                        Directory.CreateDirectory(ConFigPath + Tempath)
    '                    Else
    '                        Dim d As New DirectoryInfo(ConFigPath + Tempath)

    '                        Dim fi() As System.IO.FileInfo
    '                        fi = d.GetFiles("*.*", SearchOption.TopDirectoryOnly)
    '                        If fi.Length > 0 Then '' If Having Attachments
    '                            For Each f As System.IO.FileInfo In fi
    '                                f.Delete()
    '                            Next
    '                        End If

    '                    End If


    '                    radGuardianPhoto.UploadedFiles(0).SaveAs(ConFigPath + Tempath & System.IO.Path.GetFileName(UploadfileName))

    '                    ViewState("ParentPHOTOFILEPATHoldPath") = ConFigPath + Tempath & UploadfileName
    '                    ViewState("ParentPHOTOFILEPATH") = "/" & Session("STU_BSU_ID") & "/" & Session("STU_ID") & "/STUPHOTO/EMIRATESID/GUARDIAN/" & "PARENTPHOTO" & strExtn

    '                    ' hf_reload.Value = "1"
    '                    divUploadmsg.CssClass = "divvalidUpload"
    '                    divUploadmsg.Text = filename
    '                    hfUploadPath.Value = ""

    '                Else
    '                    ViewState("ParentPHOTOFILEPATHoldPath") = ""
    '                    ViewState("ParentPHOTOFILEPATH") = ""
    '                    divUploadmsg.CssClass = "diverrorUpload"
    '                    divUploadmsg.Text = "There is no file to upload."
    '                End If


    '            Catch ex As Exception
    '                ViewState("ParentPHOTOFILEPATHoldPath") = ""
    '                ViewState("ParentPHOTOFILEPATH") = ""
    '                UtilityObj.Errorlog(ex.Message, "btnProcess_Click")

    '                divUploadmsg.CssClass = "diverrorUpload"
    '                divUploadmsg.Text = "Error while updating the file!"
    '                imgParentImage.ImageUrl = Server.MapPath("~/ParentLogin/Images/no_image.gif")
    '                imgParentImage.AlternateText = "No Image found"
    '            End Try

    '        End If
    '    End If




    'End Sub



    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click

        Response.Redirect("~\Home.aspx")
    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim status As Integer
        Dim transaction As SqlTransaction
        'Dim lblmsg As Label = CType(Page.Master.FindControl("lblSiteMessage"), Label)
        lblmsg.Text = ""
        ' Dim IsUpload As Boolean =
        UpLoadPhoto()
        Try
            ' If IsUpload = True Then

            Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                transaction = conn.BeginTransaction("SampleTransaction")
                status = callTrans_Student_M(transaction)

                If status = 1000 Then
                    'lblmsg.CssClass = "diverror"
                    If lblmsg.Text = "" Then
                        lblmsg.CssClass = "alert alert-info"
                        lblmsg.Text = "Please upload photo"
                    End If
                ElseIf status <> 0 Then
                    'lblmsg.CssClass = "diverror"
                    lblmsg.CssClass = "alert alert-warning"
                    lblmsg.Text = "Photo upload failed"
                Else
                    divUploadmsg.CssClass = ""
                    divUploadmsg.Text = ""
                    'lblmsg.CssClass = "divvalid"
                    lblmsg.CssClass = "alert alert-success"
                    lblmsg.Text = "Photo saved successfully"
                End If

            End Using
            ' End If


        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)

        End Try
    End Sub

    Protected Sub btnUploadFatherPhoto_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUploadFatherPhoto.Click
        Dim status As Integer
        Dim transaction As SqlTransaction
        'Dim lblmsg As Label = CType(Page.Master.FindControl("lblSiteMessage"), Label)
        'lblmsg.Text = ""
        UpLoadParentPhoto(1)
        Try

            Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                transaction = conn.BeginTransaction("SampleTransaction")
                status = callTrans_Parent_M(transaction, 1)

                If status = 1000 Then
                    'lblmsg.CssClass = "diverror"
                    If lblmsg.Text = "" Then
                        lblmsg.CssClass = "alert alert-info"
                        lblmsg.Text = "Please upload photo"
                    End If
                    'ElseIf status <> 0 Then
                    '    lblmsg.CssClass = "diverror"
                    '    lblmsg.Text = "Photo upload failed"
                Else
                    divUploadmsg.CssClass = ""
                    divUploadmsg.Text = ""
                    'lblmsg.CssClass = "divvalid"
                    lblmsg.CssClass = "alert alert-success"
                    lblmsg.Text = "Photo saved successfully"
                End If

            End Using



        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)

        End Try
    End Sub

    Protected Sub btnUploadMotherPhoto_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUploadMotherPhoto.Click
        Dim status As Integer
        Dim transaction As SqlTransaction
        'Dim lblmsg As Label = CType(Page.Master.FindControl("lblSiteMessage"), Label)
        'lblmsg.Text = ""
        UpLoadParentPhoto(2)
        Try

            Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                transaction = conn.BeginTransaction("SampleTransaction")
                status = callTrans_Parent_M(transaction, 2)

                If status = 1000 Then
                    'lblmsg.CssClass = "diverror"
                    If lblmsg.Text = "" Then
                        lblmsg.CssClass = "alert alert-info"
                        lblmsg.Text = "Please upload photo"
                    End If
                    'ElseIf status <> 0 Then
                    '    lblmsg.CssClass = "diverror"
                    '    lblmsg.Text = "Photo upload failed"
                Else
                    divUploadmsg.CssClass = ""
                    divUploadmsg.Text = ""
                    'lblmsg.CssClass = "divvalid"
                    lblmsg.CssClass = "alert alert-success"
                    lblmsg.Text = "Photo saved successfully"
                End If

            End Using



        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)

        End Try
    End Sub

    Protected Sub btnUploadGuardianPhoto_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUploadGuardianPhoto.Click
        Dim status As Integer
        Dim transaction As SqlTransaction
        'Dim lblmsg As Label = CType(Page.Master.FindControl("lblSiteMessage"), Label)
        lblmsg.Text = ""
        UpLoadParentPhoto(3)
        Try

            Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                transaction = conn.BeginTransaction("SampleTransaction")
                status = callTrans_Parent_M(transaction, 3)

                If status = 1000 Then
                    'lblmsg.CssClass = "diverror"
                    If lblmsg.Text = "" Then
                        lblmsg.CssClass = "alert alert-info"
                        lblmsg.Text = "Please upload photo"
                    End If
                    
                    'ElseIf status <> 0 Then
                    '    lblmsg.CssClass = "diverror"
                    '    lblmsg.Text = "Photo upload failed"
                Else
                    divUploadmsg.CssClass = ""
                    divUploadmsg.Text = ""
                    'lblmsg.CssClass = "divvalid"
                    lblmsg.CssClass = "alert alert-success"
                    lblmsg.Text = "Photo saved successfully"
                End If

            End Using



        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)

        End Try
    End Sub

    Private Sub SavePhotoToCRMINTEGRATION(ByVal photobase64 As String)
        Try
            Dim str_Sql As String = String.Empty
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim param(10) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@SF_STUDENT_ID", Session("STUID"))
            param(1) = New SqlClient.SqlParameter("@PHOTO_BASE64", photobase64)

            param(2) = New SqlClient.SqlParameter("@PHOTO_TYPE", "S")
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "dbo.SAVE_PHOTO_CRM_SF", param)
        Catch ex As Exception

        End Try
    End Sub

    Public Function GetParentPhotoUploadEnabled() As Boolean
        Try
            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim BSUId As String = String.Empty
            BSUId = Session("STU_BSU_ID")

            Dim param(1) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@BsuId", Session("STU_BSU_ID"))
            Dim isphotoupload As Boolean = SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "dbo.Get_Parent_Photo_Upload_Enabled", param)
            Return isphotoupload
        Catch ex As Exception
            Return False
        End Try
    End Function



    Sub ResizeImage(ByVal fullpath As String, ByVal WIDTH As Integer, ByVal HEIGHT As Integer)
        Dim oBitmap As Bitmap
        oBitmap = New Bitmap(fullpath)
        If oBitmap.Height > HEIGHT Or oBitmap.Width > WIDTH Then
            Dim bmpNew As Bitmap = New Bitmap(WIDTH, HEIGHT)
            If bmpNew.PixelFormat = Drawing.Imaging.PixelFormat.Format1bppIndexed Or _
                bmpNew.PixelFormat = Drawing.Imaging.PixelFormat.Format4bppIndexed Or _
                bmpNew.PixelFormat = Drawing.Imaging.PixelFormat.Format8bppIndexed Or _
                bmpNew.PixelFormat = Drawing.Imaging.PixelFormat.Undefined Or _
                bmpNew.PixelFormat = Drawing.Imaging.PixelFormat.DontCare Or _
                bmpNew.PixelFormat = Drawing.Imaging.PixelFormat.Format16bppArgb1555 Or _
                bmpNew.PixelFormat = Drawing.Imaging.PixelFormat.Format16bppGrayScale Then
                Throw New NotSupportedException("Pixel format of the image is not supported.")
            End If

            Dim oGraphic As System.Drawing.Graphics = System.Drawing.Graphics.FromImage(bmpNew)
            oGraphic.SmoothingMode = Drawing.Drawing2D.SmoothingMode.HighQuality
            oGraphic.InterpolationMode = Drawing.Drawing2D.InterpolationMode.HighQualityBicubic
            oGraphic.DrawImage(oBitmap, New Rectangle(0, 0, bmpNew.Width, bmpNew.Height), 0, 0, oBitmap.Width, oBitmap.Height, GraphicsUnit.Pixel)
            oGraphic.Dispose()
            oBitmap.Dispose()
            oBitmap = bmpNew

            Dim oBrush As New SolidBrush(Color.Black)
            Dim ofont As New Font("Arial", 8)
            oGraphic.Dispose()
            ofont.Dispose()
            oBrush.Dispose()

            File.Delete(fullpath)
            oBitmap.Save(fullpath)
            oBitmap.Dispose()
        Else
            oBitmap.Dispose()
        End If
    End Sub
End Class
