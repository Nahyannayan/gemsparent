Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data


Partial Class ParentLogin_Changepassword
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        ' Dim lblmsg As Label = CType(Page.Master.FindControl("lblSiteMessage"), Label)
        
        If Page.IsValid = True Then
            If txtCurrentPassword.Text = txtNewpassword.Text Then
                lblmsg.CssClass = "text-danger"
                lblmsg.Text = "Current Password and New Password are Same!!!Please Re Enter the Passwords"

                txtCurrentPassword.Focus()
                Exit Sub
            ElseIf txtNewpassword.Text.Trim.Length < 7 Then
                lblmsg.CssClass = "text-danger"
                lblmsg.Text = "Your password must contain atleast  7 characters"
                Exit Sub
            End If
            If pwd_HISTORY() = 0 Then
                lblmsg.CssClass = "text-danger"
                lblmsg.Text = "Password needs to be 8 characters or more and avoid last Two passwords used "

                Exit Sub
            End If
            If Not UtilityObj.PasswordVerify(txtConfPassword.Text, 2, 0, 2) Then
                lblmsg.CssClass = "text-danger"
                lblmsg.Text = UtilityObj.getErrorMessage("781")

                Exit Sub
            End If
            Dim bPasswordUpdate As Boolean = False
            Try
                Dim status As Integer
                Dim transaction As SqlTransaction
                Using conn As SqlConnection = ConnectionManger.GetGLGConnection()

                    transaction = conn.BeginTransaction("SampleTransaction")
                    Dim MSG As String = ""
                    Try
                        Dim NEW_PASSWORD_hash As String = Encr_decrData.EncryptWithSaltedHash(txtNewpassword.Text)
                        status = passwordchange.changepassword(Session("OLU_ID"), Encr_decrData.Encrypt(txtCurrentPassword.Text), Encr_decrData.Encrypt(txtNewpassword.Text), lblmsg, NEW_PASSWORD_hash, transaction)
                        If status = 0 Then
                            transaction.Commit()
                            bPasswordUpdate = True
                            Session("bPasswdUpdate") = "False"
                            lblmsg.CssClass = "divvalid"
                            lblmsg.Text = String.Format("{0}," & lblmsg.Text & "", Session("username"))
                            Response.Redirect("~\general\Home.aspx", False)
                        Else
                            transaction.Rollback()

                            ''  lblError.Text = MSG.ToString()
                        End If
                    Catch ex As Exception
                        transaction.Rollback()
                        UtilityObj.Errorlog(ex.Message)
                        lblmsg.CssClass = "text-danger"
                        lblmsg.Text = "Password Change Failed..!"
                    End Try
                End Using
                If bPasswordUpdate Then
                    status = VerifyandUpdatePassword(Encr_decrData.Encrypt(txtNewpassword.Text), Encr_decrData.EncryptWithSaltedHash(txtNewpassword.Text), _
Session("username"))
                    ''by nhayan on 1june2017
                    'Dim vGLGUPDPWD As New com.ChangePWDWebService
                    'vGLGUPDPWD.Url = "http://10.10.1.35/release/ChangePWDWebService.asmx"
                    'Dim respon As String = vGLGUPDPWD.ChangePassword(Session("username"), txtNewpassword.Text, txtNewpassword.Text)
                    ''by nahyan on 1june2017
                    Dim chPWDSVC As New ChangePasswordSVC.ChangePWDWebServiceSoapClient

                    Dim respon As String = chPWDSVC.ChangePassword(Session("username"), txtCurrentPassword.Text, txtNewpassword.Text)
                    ''by nahyan on 1june2017

                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
                lblmsg.CssClass = "diverror"
                            lblmsg.Text = "Password Change Failed..!"
            End Try
        End If
    End Sub

    Public Shared Function VerifyandUpdatePassword(ByVal New_Password As String, ByVal NEW_PASSWORD_hash As String, _
    ByVal username As String) As Integer
        Dim connection As SqlConnection
        Try
            connection = ConnectionManger.GetGLGConnection()
            Dim pParms(4) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@USR_NAME", username)
            pParms(1) = New SqlClient.SqlParameter("@NEW_PWD", New_Password)
            pParms(3) = New SqlClient.SqlParameter("@NEWHASHEDPASSWORD", NEW_PASSWORD_hash)
            pParms(2) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(2).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "GLG.GLG_AD_PWDUPD_HASH", pParms)
            'SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "GLG.GLG_AD_PWDUPD", pParms)
            Return pParms(2).Value

        Catch
        Finally
            connection.Close()
        End Try
    End Function

    Public Function pwd_HISTORY() As Integer
        Dim i As Integer
        Dim connection As String = ConnectionManger.GetOASISConnectionString
        Dim pParms(4) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@OLU_ID", Session("OLU_ID"))
        pParms(1) = New SqlClient.SqlParameter("@USR_NAME", Session("username"))
        pParms(2) = New SqlClient.SqlParameter("@PWD", txtNewpassword.Text)

        Using DATAREADER As SqlDataReader = SqlHelper.ExecuteReader(connection, CommandType.StoredProcedure, "ONLINE.SAVE_PASSWORD_LOG", pParms)

            While DATAREADER.Read
                i = Convert.ToString(DATAREADER("STATUS_VALUE"))
            End While
        End Using
        Return i
    End Function

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClear.Click
        txtCurrentPassword.Text = ""
        txtConfPassword.Text = ""
        txtNewpassword.Text = ""
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("username") Is Nothing Then
            Session("Active_tab") = "Home"
            Session("Site_Path") = ""
            Response.Redirect("~\login.aspx")
        End If
  
        If Page.IsPostBack = False Then
          
        End If
        lbChildNameTop.Text = Session("STU_NAME")
    End Sub
End Class


