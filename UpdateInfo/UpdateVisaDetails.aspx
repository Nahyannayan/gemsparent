﻿<%@ Page Language="VB" MasterPageFile="~/ParentMaster.master" AutoEventWireup="false"
 CodeFile="UpdateVisaDetails.aspx.vb"
 Inherits="UpdateInfo_UpdateVisaDetails" title="GEMS EDUCATION" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphParent" Runat="Server">
<div class="content margin-top30 margin-bottom60">
                    <div class="container">
                        <div class="row">
                            
                         <!-- Posts Block -->
      <div class="posts-block col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="my-account">
    <asp:Label ID="lblError" runat="server" EnableViewState="False"></asp:Label>
<div class="mainheading">
      <div class="left">Passport/Visa Details</div>
       <div class="right"><asp:label ID="lbChildName" runat="server"  CssClass="lblChildNameCss"></asp:label></div>
      </div>
    <div> 
 <div class="alert alert-warning">
          <button type="button" class="close" data-dismiss="alert">×</button>
    <div>&nbsp;&nbsp;&bull;&nbsp;&nbsp;An asterisk (<font color="#ff0000">*</font>) indicates mandatory fields.</div>
         <asp:Label ID="lblValidate_msg" runat="server" EnableViewState="false"></asp:Label>
    </div>
 <table align="left" class="table table-striped table-bordered table-responsive text-left my-orders-table" width="100%">
 <tr><td align="left" style="vertical-align:middle" >Emirates ID</td> <td align="left" style="vertical-align:middle" >
  <asp:TextBox ID="txtEMIRATES_ID" runat="server" CssClass="form-control"></asp:TextBox></td>
  <td align="left" style="vertical-align:middle">Emirates ID Expiry Date
                                  </td>
                                
                                <td align="left" style="vertical-align:middle">
                                   <asp:TextBox ID="txtEMIRATES_IDExp_date" runat="server" CssClass="form-control" MaxLength="11"
                                TabIndex="32">
                            </asp:TextBox>&nbsp;<asp:ImageButton ID="imgBtnEMIRATES_IDExp_date" runat="server" ImageUrl="~/Images//Calendar.png"  CssClass="imgCal" />
                                                        <ajaxToolkit:FilteredTextBoxExtender ID="ftbEMIRATES_IDExp_date" runat="server" TargetControlID="txtEMIRATES_IDExp_date"
                                FilterType="Custom,UppercaseLetters,LowercaseLetters,Numbers" ValidChars="/">
                            </ajaxToolkit:FilteredTextBoxExtender>
                            <ajaxToolkit:CalendarExtender ID="ceEMIRATES_IDExp_date" runat="server" Format="dd/MMM/yyyy"
                                PopupButtonID="imgBtnEMIRATES_IDExp_date" TargetControlID="txtEMIRATES_IDExp_date">
                            </ajaxToolkit:CalendarExtender>
                             <asp:RegularExpressionValidator ID="revEmir_Exp_date"
                                            runat="server" ControlToValidate="txtEMIRATES_IDExp_date" Display="Dynamic" EnableViewState="False"
                                            ErrorMessage="Enter the Emirates ID expiry  Date in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                            ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                            ValidationGroup="groupM1">*</asp:RegularExpressionValidator><div class="remark">(dd/mmm/yyyy)</div>
                                </td></tr>
 
        <tr>  <td align="left" style="vertical-align:middle">
                                    Passport No<font color="red">*</font></td>
                             
                                <td align="left">
                                    <asp:TextBox ID="txtPNo" runat="server" CssClass="form-control"></asp:TextBox>
                                      <asp:RequiredFieldValidator id="RequiredFieldValidator5" runat="server" ControlToValidate="txtPNo" 
                                            Display="Dynamic" ErrorMessage="Enter Passport No" ForeColor="red" ValidationGroup="groupM1" >*</asp:RequiredFieldValidator>
                                </td>
                                <td align="left" style="vertical-align:middle">
                                    Passport Issue Place</td>
                                
                                <td align="left">
                                    <asp:TextBox ID="txtPIssPlace" runat="server"  CssClass="form-control"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="vertical-align:middle" >
                                    Passport Issue Date<font color="red">*</font></td>
                                
                                <td align="left"  >
                                    <asp:TextBox ID="txtPIssDate" runat="server" CssClass="form-control"></asp:TextBox>
                                    <asp:ImageButton ID="imgBtnPIssDate" runat="server" ImageUrl="~/Images//Calendar.png"  CssClass="imgCal"/>
                                         <asp:RequiredFieldValidator id="RequiredFieldValidator2" runat="server" ControlToValidate="txtPIssDate" 
                                            Display="Dynamic" ErrorMessage="Enter Passport Issue Date" ForeColor="red" ValidationGroup="groupM1">*</asp:RequiredFieldValidator>              
                                         <asp:RegularExpressionValidator ID="RegularExpressionValidator1"
                                            runat="server" ControlToValidate="txtPIssDate" Display="Dynamic" EnableViewState="False"
                                            ErrorMessage="Enter the Issue  Date in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                            ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                            ValidationGroup="groupM1">*</asp:RegularExpressionValidator><asp:CustomValidator
                                                ID="cvIss_date" runat="server" ControlToValidate="txtPIssDate" 
                                                Display="Dynamic" EnableViewState="False" ErrorMessage="Issued Date entered is not a valid date"
                                                ForeColor="red" ValidationGroup="groupM1">*</asp:CustomValidator><div class="remark">(dd/mmm/yyyy)</div></td>
                                <td align="left"  style="vertical-align:middle">
                                    Passport Expiry Date<font color="red">*</font></td>
                             
                                <td align="left" >
                                    <asp:TextBox ID="txtPExpDate" runat="server"  CssClass="form-control"></asp:TextBox>
                                    <asp:ImageButton ID="imgBtnPExpDate" runat="server" ImageUrl="~/Images//Calendar.png"  CssClass="imgCal"/>
                                         <asp:RequiredFieldValidator id="RequiredFieldValidator1" runat="server" ControlToValidate="txtPExpDate" 
                                            Display="Dynamic" ErrorMessage="Enter Passport Expiry Date" ForeColor="red" ValidationGroup="groupM1">*</asp:RequiredFieldValidator>              
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2"
                                            runat="server" ControlToValidate="txtPExpDate" Display="Dynamic" EnableViewState="False"
                                            ErrorMessage="Enter the Expiry  Date in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                            ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                            ValidationGroup="groupM1">*</asp:RegularExpressionValidator><asp:CustomValidator
                                                ID="cvExp_date" runat="server" ControlToValidate="txtPExpDate" 
                                                Display="Dynamic" EnableViewState="False" ErrorMessage="Visa Expiry Date entered is not a valid date and must be greater than Issue Date"
                                                ForeColor="red" ValidationGroup="groupM1">*</asp:CustomValidator><div class="remark">(dd/mmm/yyyy)</div></td>
                            </tr>
                            <tr>
                                <td align="left"  style="vertical-align:middle">
                                    Visa No</td>
                               
                                <td align="left"  >
                                    <asp:textbox ID="txtVNo" runat="server"  CssClass="form-control"></asp:TextBox>
                                </td>
                                <td align="left" style="vertical-align:middle">
                                    Visa Issue Place</td>
                                  <td align="left"  >
                                    <asp:TextBox ID="txtVIssPlace" runat="server"  CssClass="form-control"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td align="left"  style="vertical-align:middle">
                                    Visa Issue Date<font color="red">*</font></td>
                                <td align="left" >
                                    <asp:TextBox ID="txtVIssDate" runat="server"  CssClass="form-control"></asp:TextBox>
                                    <asp:ImageButton ID="imgBtnVIssDate" runat="server" 
                                    ImageUrl="~/Images//Calendar.png"   CssClass="imgCal"/>
                                        <asp:RequiredFieldValidator id="RequiredFieldValidator3" runat="server" ControlToValidate="txtVIssDate" 
                                            Display="Dynamic" ErrorMessage="Enter Visa Issue Date" ForeColor="red" ValidationGroup="groupM1">*</asp:RequiredFieldValidator>              
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator6"
                                            runat="server" ControlToValidate="txtVIssDate" Display="Dynamic" EnableViewState="False"
                                            ErrorMessage="Enter the Issue  Date in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                            ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                            ValidationGroup="groupM1">*</asp:RegularExpressionValidator><asp:CustomValidator
                                                ID="CustomValidator2" runat="server" ControlToValidate="txtVIssDate" 
                                                Display="Dynamic" EnableViewState="False" ErrorMessage="Issued Date entered is not a valid date"
                                                ForeColor="red" ValidationGroup="groupM1">*</asp:CustomValidator> <div class="remark">(dd/mmm/yyyy)</div></td>
                                <td align="left" style="vertical-align:middle" >
                                    Visa Expiry Date<font color="red">*</font></td>
                               
                                <td align="left"  >
                                    <asp:TextBox ID="txtVExpDate" runat="server"  CssClass="form-control"></asp:TextBox>
                                    <asp:ImageButton ID="imgBtnVExpdate" 
                                    runat="server" ImageUrl="~/Images//Calendar.png"  CssClass="imgCal"/>
                                      <asp:RequiredFieldValidator id="RequiredFieldValidator4" runat="server" ControlToValidate="txtVExpDate" 
                                            Display="Dynamic" ErrorMessage="Enter Visa Expiry Date" ForeColor="red" ValidationGroup="groupM1">*</asp:RequiredFieldValidator>              
                                     <asp:RegularExpressionValidator ID="RegularExpressionValidator8"
                                            runat="server" ControlToValidate="txtVExpDate" Display="Dynamic" EnableViewState="False"
                                            ErrorMessage="Enter the Expiry  Date in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                            ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                            ValidationGroup="groupM1">*</asp:RegularExpressionValidator><asp:CustomValidator
                                                ID="CustomValidator4" runat="server" ControlToValidate="txtVExpDate"
                                                Display="Dynamic" EnableViewState="False" ErrorMessage="Visa Expiry Date entered is not a valid date and must be greater than Issue Date"
                                                ForeColor="red" ValidationGroup="groupM1">*</asp:CustomValidator> <div class="remark">(dd/mmm/yyyy)</div></td>
                            </tr>
                            <tr>
                                <td align="left" style="vertical-align:middle">
                                    Issuing Authority</td>
                              
                                <td align="left"  colspan="4" >
                                    <asp:TextBox ID="txtVIssAuth" runat="server" CssClass="form-control"></asp:TextBox></td>
                            </tr>
                            <tr id="trPremises" runat="server">
                                <td align="left" style="vertical-align:middle">
                                     Premises ID </td>
                              
                                <td align="left"  colspan="4" >
                                     <asp:TextBox ID="txtPremisesID" runat="server"  CssClass="form-control" maxlength="12"></asp:TextBox>
                                      <ajaxToolkit:FilteredTextBoxExtender ID="ftbPremisesID" runat="server" TargetControlID="txtPremisesID"
                                                FilterType="Numbers">
                                            </ajaxToolkit:FilteredTextBoxExtender>
                                    
                                    
                                    </td>
                            </tr>
                             <tr>
           
            <td align="center"  valign="middle" colspan="4">
                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn btn-info"
                            ValidationGroup="groupM1" />&nbsp;<asp:Button ID="btnCancel" runat="server"
                             CausesValidation="False" CssClass="btn btn-info"
                               Text="Cancel" /></td>
            
        </tr>
     <tr>
      <td align="left"  valign="middle" colspan="4">  <asp:ValidationSummary ID="vsVisaDetails" runat="server"  ValidationGroup="groupM1" 
      CssClass="divinfoInner" ForeColor=""/></td>
     </tr>
                        </table>
    
     <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MMM/yyyy"
        PopupButtonID="imgBtnPIssDate" TargetControlID="txtPIssDate" ></ajaxToolkit:CalendarExtender>
         <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd/MMM/yyyy"
        PopupButtonID="imgBtnPExpDate" TargetControlID="txtPExpDate" ></ajaxToolkit:CalendarExtender>
         <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server" Format="dd/MMM/yyyy"
        PopupButtonID="imgBtnVIssDate" TargetControlID="txtVIssDate" ></ajaxToolkit:CalendarExtender>
         <ajaxToolkit:CalendarExtender ID="CalendarExtender4" runat="server" Format="dd/MMM/yyyy"
        PopupButtonID="imgBtnVExpdate" TargetControlID="txtVExpDate" ></ajaxToolkit:CalendarExtender>
 
    </div>
   </div>
      </div>
                            </div>
                        </div>
    </div>
</asp:Content>

