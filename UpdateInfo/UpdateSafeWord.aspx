﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ParentMaster.master" AutoEventWireup="false" CodeFile="UpdateSafeWord.aspx.vb" Inherits="UpdateInfo_UpdateSafeWord" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphParent" runat="Server">
    <div class="content margin-top30 margin-bottom60">
        <div class="container">
            <div class="row">

                <!-- Posts Block -->

                <div class="posts-block col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="my-account">
                        <div class="mainheading">
                            <div class="left">Update Safe Word</div>

                        </div>

                        <div align="center" style="text-align: center;">


                   
                                <div id="divNote" runat="server"  class="alert alert-warning">
          <button type="button" class="close" data-dismiss="alert">×</button>

                            </div>


                            <table class="table table-striped table-bordered table-responsive text-left my-orders-table" width="100%" cellpadding="8" cellspacing="0">


                                <tr id="trMain" runat="server">
                                    <td style="vertical-align:middle">Enter Safe Word<font color="red" size="1px">*</font></td>
                                    <td align="left" colspan="3">
                                        <asp:TextBox ID="txtSafeWord" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server"
                                            ControlToValidate="txtSafeWord" Display="Dynamic" EnableViewState="False"
                                            Font-Italic="True" ForeColor="red" ErrorMessage="Safe Word required"
                                            ValidationGroup="info">*</asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator Display="Dynamic" EnableViewState="False" ControlToValidate="txtSafeWord"
                                            ID="RegularExpressionValidator2" ValidationExpression="^[\s\S]{6,}$"
                                            runat="server" ErrorMessage="Safe word does not contain minimum requirement of 6 characters. Please re-enter" Font-Italic="True" ForeColor="red"
                                            ValidationGroup="info">*</asp:RegularExpressionValidator>
                                    </td>
                                </tr>
                                <tr id="trSave" runat="server">
                                    <td
                                        colspan="4" align="center">
                                        <asp:Button ID="btnSave" runat="server" CssClass="btn btn-info" Text="Submit"
                                            CausesValidation="true" ValidationGroup="info" />


                                    </td>
                                </tr>
                                <tr id="trOTP" class="trOTP" runat="server">
                                    <td class="tdblankAll" align="left">
                                        <asp:Label ID="Label4" runat="server" Cssstyle="vertical-align:middle" Text="OTP">
                                        </asp:Label>
                                    </td>
                                    <td class="tdblankAll" align="left">
                                        <asp:TextBox ID="txtOTP" runat="server"  CssClass="form-control">
                                        </asp:TextBox><br />
                                        <asp:Label ID="Label5" runat="server" Style="color: red;" Text="*OTP has been sent to your registered Mobile">
                                        </asp:Label>
                                    </td>
                                </tr>
                                <tr class="trbtnSearch" id="trbtnSearch" runat="server">
                                    <td class="tdblankAll" align="center" colspan="2">
                                        <asp:Button ID="btnEmpSearch" runat="server" CssClass="btn btn-info" ValidationGroup="popValid"
                                            Text="Continue" />
                                    </td>
                                </tr>
                                <tr>
                                    <td
                                        align="center" colspan="4">
                                        <asp:ValidationSummary ID="vsRef" runat="server"
                                            ValidationGroup="info" HeaderText="<div class='validationheader'>Please correct the following:</div>"
                                            CssClass="divinfoInner" ForeColor="" />

                                        <asp:Label ID="lbmsgInfo" runat="server"></asp:Label>

                                    </td>
                                </tr>
                                <tr class="tdblankAll">
                                    <td class="tdblankAll" align="center" colspan="2">
                                        <asp:Label ID="lblPopError" runat="server"></asp:Label>
                                        <asp:ValidationSummary ID="vsEmpDetails" runat="server" ValidationGroup="popValid"
                                            HeaderText="<div class='validationheader'>Please correct the following:</div>"
                                            CssClass="divinfoPopUp" ForeColor="" />
                                        <asp:HiddenField ID="h_EmpNo" Value="" runat="server" />
                                    </td>
                                </tr>

                            </table>
                            <div id="pldis" runat="server" style="display: none; overflow: visible; background-color: White; border-color: #b5cae7; border-style: solid; border-width: 4px; ">
                                <div style=" margin-top: 0px; vertical-align: middle; background-color: White;">
                                    <span style="clear: right; display: inline; float: right; visibility: visible; margin-top: 0px; vertical-align: top;">
                                        <asp:ImageButton ID="btnCloseedit" runat="server" ImageUrl="~/Images/Common/PageBody/close.png" /></span>
                                </div>
                                <div class="mainheading" style="margin-top: 15px; ">
                                    <div class="left">
                                        <asp:Label ID="lblPopHead" runat="server" Text="Referral History" Font-Size="11px" Font-Bold="true"></asp:Label></div>

                                </div>
                                <asp:Panel ID="plStudAtt" runat="server" 
                                    BackColor="White" BorderColor="Transparent" BorderStyle="none" ScrollBars="none">
                                </asp:Panel>
                            </div>

                            <asp:Button ID="btnShowPopup" runat="server" Style="display: none" />
                            <ajaxToolkit:ModalPopupExtender
                                ID="mdlPopup" runat="server" TargetControlID="btnShowPopup" PopupControlID="pldis"
                                BackgroundCssClass="modalBackground" DropShadow="false" RepositionMode="RepositionOnWindowResizeAndScroll">
                            </ajaxToolkit:ModalPopupExtender>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

