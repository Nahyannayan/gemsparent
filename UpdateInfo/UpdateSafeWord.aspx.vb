﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Net.Mail
Imports System.Text
Partial Class UpdateInfo_UpdateSafeWord
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try

            If Session("username") Is Nothing Then
                Session("Active_tab") = "Home"
                Session("Site_Path") = ""
                Response.Redirect("~\login.aspx")
            ElseIf Session("bPasswdChanged") = "False" Then
                Response.Redirect("~\UpdateInfo\ForcePasswordChange.aspx", False)
            ElseIf Session("bUpdateContactDetails") = "False" Then
                Response.Redirect(Session("ForceUpdate_stud"), False)
            End If
           
            If Page.IsPostBack = False Then

                EXISTS_WORD()
              

            End If
        Catch ex As Exception

        End Try
    End Sub
    Function valid_word() As Integer
        Dim i As Integer
        Dim connection As String = ConnectionManger.GetOASISConnectionString
        Dim pParms(5) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@TEXT", txtSafeWord.Text)

        i = 0
        Dim ds As DataSet = SqlHelper.ExecuteDataset(connection, CommandType.StoredProcedure, "OPL.VALID_SAFE_WORD", pParms)
        If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
            i = ds.Tables(0).Rows(0).Item("VALID")

        End If
        Return i
    End Function
    Sub EXISTS_WORD()
        Dim i As Integer
        Dim connection As String = ConnectionManger.GetOASISConnectionString
        Dim pParms(5) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@OLU_NAME", Session("username"))

        i = 0
        Dim ds As DataSet = SqlHelper.ExecuteDataset(connection, CommandType.StoredProcedure, "OPL.CHECK_SAFE_WORD_EXISTS", pParms)
        If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
            i = ds.Tables(0).Rows(0).Item("EXIST")
            txtSafeWord.Text = ds.Tables(0).Rows(0).Item("SAFE_TEXT")
            divNote.InnerText = ds.Tables(0).Rows(0).Item("MSG")
            ViewState("SW_ID") = ds.Tables(0).Rows(0).Item("SW_ID")
            ViewState("OTP") = ds.Tables(0).Rows(0).Item("OTP")
            ViewState("OTPREQ") = ds.Tables(0).Rows(0).Item("OTPREQ")
            ViewState("SAFE_VALUE") = ds.Tables(0).Rows(0).Item("SAFE_WORD")
        End If
        If ViewState("OTPREQ") = "1" Then


            If i = 1 Then

                txtSafeWord.Enabled = False
                trOTP.Visible = True
                trbtnSearch.Visible = True
                trSave.Visible = False
            Else
                If ViewState("SAFE_VALUE") <> "" Then
                    txtSafeWord.Enabled = False
                    txtSafeWord.Text = ViewState("SAFE_VALUE")
                    btnSave.Text = "Edit"
                End If
                Me.trOTP.Visible = False
                Me.trbtnSearch.Visible = False
            End If
        Else
            If ViewState("SAFE_VALUE") <> "" Then
                txtSafeWord.Enabled = False
                txtSafeWord.Text = ViewState("SAFE_VALUE")
                btnSave.Text = "Edit"
            End If
            Me.trOTP.Visible = False
            Me.trbtnSearch.Visible = False
        End If
    End Sub
    Sub save_word()
        Dim connection As String = ConnectionManger.GetOASISConnectionString
        Dim pParms(3) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@SW_OLU_NAME", Session("username"))
        pParms(1) = New SqlClient.SqlParameter("@SW_TEXT", txtSafeWord.Text)
        pParms(2) = New SqlClient.SqlParameter("@OTPREQ", ViewState("OTPREQ"))


        Dim ds As DataSet = SqlHelper.ExecuteDataset(connection, CommandType.StoredProcedure, "OPL.SAVE_SAFE_WORD_LOG", pParms)
        If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
            ViewState("SW_ID") = ds.Tables(0).Rows(0).Item("SW_ID")
            ViewState("OTP") = ds.Tables(0).Rows(0).Item("OTP")
            ViewState("mobno") = ds.Tables(0).Rows(0).Item("mobno")
            ViewState("smsText") = ds.Tables(0).Rows(0).Item("smsText")
        End If
    End Sub
    Sub update_text()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "update opl.SAFE_WORD_LOG set SW_ACTIVE='TRUE' where SW_ID=" & ViewState("SW_ID")
        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)

    End Sub
    Private Sub callSend_SMS_ALERT()
        Try
            Dim status As String
            Dim password As String = ConfigurationManager.AppSettings("smspwd").ToString
            Dim usrname As String = ConfigurationManager.AppSettings("smsUsername").ToString


            status = SmsService.sms.SendMessage(ViewState("mobno"), ViewState("smsText"), "5124", usrname, password)




        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod.Name)
        End Try
    End Sub
    Protected Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        lblPopError.CssClass = ""
        lblPopError.Text = ""

        If btnSave.Text = "Edit" Then
            txtSafeWord.Enabled = True
            btnSave.Text = "Submit"
            Exit Sub
        End If
        If valid_word() = 0 Then
            lblPopError.CssClass = "diverrorPopUp"
            lblPopError.Text = "Contains Special Characters.."
            Exit Sub
        End If



        save_word()
        If ViewState("OTPREQ") = "1" Then
            callSend_SMS_ALERT()
            trOTP.Visible = True
            trbtnSearch.Visible = True
            trSave.Visible = False
        Else
            update_text()
            txtSafeWord.Enabled = False
            trSave.Visible = False
            lblPopError.CssClass = "divinfoPopUp"
            lblPopError.Text = "Safe Word Added sucessfully..."
        End If


    End Sub
    Protected Sub btnEmpSearch_Click(sender As Object, e As EventArgs) Handles btnEmpSearch.Click
        lblPopError.CssClass = ""
        lblPopError.Text = ""

        If ViewState("OTP") = txtOTP.Text Then
            update_text()
            lblPopError.CssClass = "divinfoPopUp"
            lblPopError.Text = "Safe Word Added sucessfully..."
        Else
            lblPopError.CssClass = "diverrorPopUp"
            lblPopError.Text = "OTP is not matching.."
        End If



    End Sub
End Class
