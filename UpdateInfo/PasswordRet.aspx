<%@ Page Language="VB" AutoEventWireup="false" 
CodeFile="PasswordRet.aspx.vb" Inherits="ParentPasswordRet" %>
<%@ Register Assembly="MSCaptcha" Namespace="MSCaptcha" TagPrefix="MScap" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>GEMS Education</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
   <link href="../CSS/SiteStyle_Password.css" rel="stylesheet" type="text/css" />
    <link href="../CSS/MenuStyle.css" rel="stylesheet" type="text/css" />
    <meta content="GEMS Education" name="description" />
    <meta content="GEMS Education" name="keywords" />
    

</head>
<body>
    <form runat="server" id="SignIn">
    <div style="text-align:center !important; width:100%;">
<div class="wrapper">
<div class="header" style="text-align:left !important;"  ><div class="hometoplinks"><asp:LinkButton ID="lbHomeTop" runat="server" class="hometoplinks" Text="Home"></asp:LinkButton>&nbsp;|&nbsp;<a href="http://www.gemseducation.com/MENASA/parents/contents.php?pageid=6&expandable=0&parentid=1" target="_blank" class="hometoplinks" >About Us</a></div><div class="menubox"><asp:LinkButton ID="lbPwdAsst" runat="server" CssClass="sel-menu" ></asp:LinkButton></div>
</div>
 <div class="leftBox"><asp:Label ID="lblmsg" runat="server"  EnableViewState="False" ></asp:Label>
 
      <div class="mainheading">
      <div class="left">Password Recovery</div>
             </div>
    <div> 
         <div class="divinfoPW" style="width:100%">
         <div>&nbsp;&nbsp;&bull;&nbsp;&nbsp;Enter parent login id.Incase if you do not remember the login id please contact school <div>&nbsp;&nbsp;&bull;&nbsp;&nbsp;Enter primary contact email address as registered at school 
         </div>&nbsp;&nbsp;&bull;&nbsp;&nbsp;Enter the characters you see in the image without space (not case sensitive)<div>&nbsp;&nbsp;&bull;&nbsp;&nbsp;By submitting the form an email will be send to you with a link to create a new password</div></div>  </div>              
                          <table  align="center" class="tableNoborder"  width="65%">
                              <tr>
                                              <td align="left" class="tdfields">
                                                  Parent login id<font color="red">*</font></td>
                                             
                                              <td align="left" class="tdfields">
                                                  <asp:TextBox ID="txtUSR_ID" runat="server" Width="180px" class="input"></asp:TextBox>
                                                  <asp:RequiredFieldValidator ID="rfvUSR_ID" runat="server" 
                                                      ErrorMessage="Parent login id required." ForeColor="red" ValidationGroup="GroupPwd" 
                                                      ControlToValidate="txtUSR_ID">*</asp:RequiredFieldValidator>
                                                 </td>
                                          </tr>
                              <tr>
                                              <td align="left" class="tdfields">
                                                  Primary contact email address as registered at school<font color="red">*</font></td>
                                             
                                              <td align="left" class="tdfields">
                                                  <asp:TextBox ID="txtemailid" runat="server" Width="180px" class="input"></asp:TextBox>
                                                  <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                                                      ErrorMessage="Email address can not be left empty." ForeColor="red" ValidationGroup="GroupPwd" ControlToValidate="txtemailid">*</asp:RequiredFieldValidator>
                                                  <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                                                      ErrorMessage="Not a valid email id" ForeColor="red" 
                                                      ValidationGroup="GroupPwd" ControlToValidate="txtemailid"
                                                       ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:RegularExpressionValidator></td>
                                          </tr>
                                          <tr><td  align="left" class="tdfields">Enter the text you see in the image without space<font color="red">*</font><div>(not case sensitive)</div></td><td>  <div class="CapDiv_Float">
                               <div style="border:solid 1px #ccc;width:140px;"> <MScap:CaptchaControl ID="Captcha1" runat="server" CaptchaBackgroundNoise="None"
                                    CaptchaLength="5" CaptchaHeight="40" CaptchaWidth="170" CaptchaLineNoise="None"
                                    CaptchaMinTimeout="10" CaptchaMaxTimeout="240" FontColor="#1B80B6" Width="170"
                                    Height="35" CssClass="divCaptcha" /></div>
                            </div></td></tr>
                                 <tr >
                <td>
                    </td>
                    <td> 
                 <div class="CapDiv_Float">
                    <asp:TextBox ID="txtCapt" runat="server" class="input" Width="180px"></asp:TextBox> 
                    
                    </div>
                   
                            <asp:RequiredFieldValidator ID="rfvCapt" runat="server" 
                        ControlToValidate="txtCapt" Display="Dynamic" EnableViewState="False" ErrorMessage="Please enter the text you see in the image without space"
                        Font-Italic="True" Font-Names="Verdana" Font-Size="10px" ForeColor="red" 
                        ValidationGroup="GroupPwd" >*</asp:RequiredFieldValidator></td>
            </tr>
                           
                                      
                                          <tr>
                                              <td align="center"  colspan="2">
                                                  <asp:Button ID="btnSave" runat="server"  Text="Submit"
                                                   ValidationGroup="GroupPwd" CssClass="buttons"
                                                      Width="60px" />
      </td>
                                          </tr>
                                          <tr>
                                              <td align="center"  colspan="2">
                                                  &nbsp;
      </td>
                                          </tr>
                                      </table>
                                     
                                     
                               
         </div>   
          <div style="width:100%"> <asp:ValidationSummary ID="vsPass_Recover" runat="server"  
                                            ValidationGroup="GroupPwd"  HeaderText="<div class='validationheader'>Please correct the following:</div>"
                                 CssClass="validationsummary" ForeColor="" />
                                  <asp:Label ID="lblError" runat="server"  EnableViewState="False"  ></asp:Label></div>   
 </div>
      
       </div>
 </div>
    </form>
</body>
</html>

