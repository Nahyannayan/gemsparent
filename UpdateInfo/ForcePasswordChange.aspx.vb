Imports Microsoft.ApplicationBlocks.Data
Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Text

Partial Class ParentForcePasswordChange
    Inherits System.Web.UI.Page
    Dim SessionFlag As Integer
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("username") Is Nothing Then
            Session("Active_tab") = "Home"
            Session("Site_Path") = ""
            Response.Redirect("~\login.aspx")
        End If

        If Page.IsPostBack = False Then
            'Session("bPasswdChanged") = "False"
            Session("bChecked_Password") = "False"


        End If
        If Session("bPasswdChanged") = "False" And Session("bChecked_Password") = "False" Then
            vsVisaDetails.Enabled = False

            pwd_msg()
            ModalPopupExtender1.Show()
        ElseIf Session("bPasswdChanged") = "True" Then
            Session("Active_tab") = "Home"
            Session("Site_Path") = ""
            Response.Redirect("~\login.aspx")
        End If
    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim lblmsg As Label = CType(Page.Master.FindControl("lblSiteMessage"), Label)
        If Page.IsValid = True Then
            If txtCurrentPassword.Text = txtNewpassword.Text Then
                lblmsg.CssClass = "divinfo"

                lblmsg.Text = "Current Password and New Password are Same!!!Please Re Enter the Passwords"
                txtCurrentPassword.Focus()
                Exit Sub
            ElseIf txtNewpassword.Text.Trim.Length < 7 Then
                lblmsg.CssClass = "diverror"
                lblmsg.Text = "Your password must contain atleast  7 characters"
                Exit Sub
            End If
            If Not UtilityObj.PasswordVerify(txtConfPassword.Text, 2, 0, 2) Then
                lblmsg.CssClass = "divinfo"

                lblmsg.Text = UtilityObj.getErrorMessage("781")

                Exit Sub
            End If
            Try
                Dim status As Integer
                Dim bPasswordUpdate As Boolean = False
                Dim transaction As SqlTransaction
                Using conn As SqlConnection = ConnectionManger.GetGLGConnection

                    transaction = conn.BeginTransaction("SampleTransaction")
                    Dim MSG As String = ""
                    Try
                        Dim param(3) As SqlClient.SqlParameter
                        param(0) = New SqlClient.SqlParameter("@OLU_ID", Session("OLU_ID"))
                        param(1) = New SqlClient.SqlParameter("@OLU_Email", txtemailid.Text.Trim)
                        SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "ONLINE.CHANGE_EMAILID", param)

                        status = passwordchange.changepassword(Session("OLU_ID"), Encr_decrData.Encrypt(txtCurrentPassword.Text), Encr_decrData.Encrypt(txtNewpassword.Text), lblmsg, transaction)
                        If status = 0 Then
                            transaction.Commit()
                            bPasswordUpdate = True
                            Session("bPasswdChanged") = "True"

                            lblmsg.CssClass = "divvalid"

                            lblmsg.Text = String.Format("{0}," & lblmsg.Text & "", Session("username"))



                            'Response.Write("<script language='javascript'>alert('" + lblError.Text + "')</script>")
                            'Response.Redirect("~\ParentLogin\Home.aspx")
                        Else
                            transaction.Rollback()
                            ''  lblError.Text = MSG.ToString()
                        End If
                    Catch ex As Exception
                        transaction.Rollback()
                        UtilityObj.Errorlog(ex.Message)
                        lblmsg.CssClass = "diverror"

                        lblmsg.Text = "Password Change Failed..!"

                    End Try
                End Using
                If bPasswordUpdate Then
                    status = VerifyandUpdatePassword(Encr_decrData.Encrypt(txtNewpassword.Text), _
Session("username"))
                    ''commented by nahyan on 1june2017
                    'Dim vGLGUPDPWD As New com.ChangePWDWebService
                    'vGLGUPDPWD.Url = "http://10.10.1.35/release/ChangePWDWebService.asmx"
                    'Dim respon As String = vGLGUPDPWD.ChangePassword(Session("username"), txtNewpassword.Text, txtNewpassword.Text)
                    ''by nahyan on 1june2017
                    Dim chPWDSVC As New ChangePasswordSVC.ChangePWDWebServiceSoapClient

                    Dim respon As String = chPWDSVC.ChangePassword(Session("username"), txtCurrentPassword.Text, txtNewpassword.Text)
                    ''by nahyan on 1june2017

                End If

            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)

                lblmsg.CssClass = "diverror"

                lblmsg.Text = "Password Change Failed..!"
            End Try
        End If
    End Sub

    Public Shared Function VerifyandUpdatePassword(ByVal New_Password As String, _
        ByVal username As String) As Integer
        Using connection As SqlConnection = ConnectionManger.GetGLGConnection()
            Dim pParms(2) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@USR_NAME", username)
            pParms(1) = New SqlClient.SqlParameter("@NEW_PWD", New_Password)
            pParms(2) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(2).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "GLG.GLG_AD_PWDUPD", pParms)
            'SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "GLG.GLG_AD_PWDUPD", pParms)
            Return pParms(2).Value
        End Using
    End Function

    'Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
    '    Response.Redirect("Home.aspx")
    'End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClear.Click
        txtCurrentPassword.Text = ""
        txtConfPassword.Text = ""
        txtNewpassword.Text = ""
    End Sub
    Sub pwd_msg()
        Dim str_con As String = ConnectionManger.GetOASISConnectionString
        Dim Param(3) As SqlClient.SqlParameter
        Param(0) = New SqlClient.SqlParameter("@BSU_ID", Session("OLU_bsuid"))
        Param(1) = New SqlClient.SqlParameter("@OLU_NAME", Session("username"))


        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_con, CommandType.StoredProcedure, "ONLINE.GET_PWD_RESET_TEXT", Param)
        If ds.Tables(0).Rows.Count > 0 Then
            divInfo.InnerHtml = ds.Tables(0).Rows(0).Item("MSG")
        End If
    End Sub
    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        If ValidateDate() <> "-1" Then
            Dim dt As DateTime = txt_DOB.Text
            txt_DOB.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", dt)
            Dim msg_return As String
            Dim str_con As String = ConnectionManger.GetOASISConnectionString
            Dim Param(5) As SqlClient.SqlParameter
            Param(0) = New SqlClient.SqlParameter("@OLU_ID", Session("OLU_ID")) '
            Param(1) = New SqlClient.SqlParameter("@STU_FEE_ID", txtFee_id.Text.Trim)
            Param(2) = New SqlClient.SqlParameter("@STU_DOB", txt_DOB.Text)
            Param(3) = New SqlClient.SqlParameter("@MSG_RETURN", SqlDbType.VarChar, 100)
            Param(3).Direction = ParameterDirection.Output
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_con, CommandType.StoredProcedure, "ONLINE.CHECK_FEEID", Param)
            If ds.Tables(0).Rows.Count > 0 Then
                msg_return = Param(3).Value.ToString
                Session("bChecked_Password") = "True"
                ModalPopupExtender1.Hide()
                
                vsVisaDetails.Enabled = True
            Else
                lblerror2.CssClass = "diverrorPopUp"
                lblerror2.Text = "The details you have entered does not match the school records.please try again....!!"
            End If
        Else
            lblerror2.CssClass = "diverrorPopUp"
            lblerror2.Text = "Not a valid Date...!!(eg 01/jan/2010)"
        End If
    End Sub

    Protected Sub btn_signout_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_signout.Click
        Session("username") = Nothing
        Session("Active_tab") = "Home"
        Session("Site_Path") = ""
        Response.Redirect("~\login.aspx")
    End Sub

   

    Function ValidateDate() As String
        Try


            Dim ErrorStatus As String = String.Empty

            If txt_DOB.Text.Trim <> "" Then


                Dim strfDate As String = txt_DOB.Text.Trim
                Dim str_err As String = DateFunctions.checkdate(strfDate)
                If str_err <> "" Then
                    ErrorStatus = "-1"

                Else
                    txt_DOB.Text = strfDate
                    Dim dateTime1 As String
                    dateTime1 = Date.ParseExact(txt_DOB.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                    'check for the leap year date
                    If Not IsDate(dateTime1) Then
                        ErrorStatus = "-1"
                    End If
                End If
            Else
                ErrorStatus = "-1"
            End If

            Return ErrorStatus
        Catch ex As Exception
            UtilityObj.Errorlog("UNEXPECTED ERROR IN ADMISSION DATE SELECT", "rptAdmission_Details")
            Return "-1"
        End Try

    End Function
End Class
