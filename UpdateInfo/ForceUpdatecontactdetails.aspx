<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/ParentMaster.master" 
CodeFile="ForceUpdatecontactdetails.aspx.vb" Inherits="ParentLogin_ForceUpdatecontactdetails"  %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphParent" Runat="Server">

        <script language="javascript" type="text/javascript">      
                              
         function copyFromAbove()
          {
      var chkThis=document.getElementById('<%=chkCopy.ClientID %>')
           var chk_state=  chkThis.checked ;
      if (chk_state==true)
    {
    
     document.getElementById('<%=txtFPRMAddr1.ClientID %>').value=document.getElementById('<%=txtFAptNo.ClientID %>').value;
  document.getElementById('<%=txtFPRMAddr2.ClientID %>').value=document.getElementById('<%=txtFCOMADDR1.ClientID %>').value + '  '+ document.getElementById('<%=txtFCOMADDR2.ClientID %>').value + '  '+ document.getElementById('<%=txtFBldg.ClientID %>').value;  
       document.getElementById('<%=ddlFPRM_Country.ClientID %>').selectedIndex=document.getElementById('<%=ddlFCOMCountry.ClientID %>').selectedIndex;
        document.getElementById('<%=txtFPPRM_Country.ClientID %>').value=document.getElementById('<%=txtFMobile_Country.ClientID %>').value;
     document.getElementById('<%=txtFPPRM_Area.ClientID %>').value=document.getElementById('<%=txtFMobile_Area.ClientID %>').value;
     document.getElementById('<%=txtFPPRM_No.ClientID %>').value=document.getElementById('<%=txtFMobile_No.ClientID %>').value;
        document.getElementById('<%=txtFPRMPOBOX.ClientID %>').value=document.getElementById('<%=txtFCOMPOBOX.ClientID %>').value;
              
          }
          } 
                    
        </script>
    
        <script>
            if ($(window).width() < 979) {
                if ($(location).attr("href").indexOf("ForceUpdatecontactdetails_M.aspx") == -1) {
                    window.location = "\\GEMSPARENT\\UpdateInfo\\ForceUpdatecontactdetails_M.aspx";
                }
            }
            if ($(window).width() > 979) {
                if ($(location).attr("href").indexOf("ForceUpdatecontactdetails.aspx") == -1) {
                    alert((location).attr("href"));
                    window.location = "\\GEMSPARENT\\UpdateInfo\\ForceUpdatecontactdetails.aspx";
                }
            }
    </script>
         
    <div class="content margin-top30 margin-bottom60">
        <div class="container">
            <div class="row">

                <!-- Posts Block -->
                <div class="posts-block col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="my-account">

                        <div class="bottom-padding">
                            <div class="title-box">
                                <h3>Parent Communication</h3>
                            </div>
                            <!-- Table  -->
                            <div class="table-responsive">

           <asp:Label ID="lblError" runat="server" EnableViewState="False"></asp:Label>                              
                      
                      <div >
    <div  class="alert alert-warning">
                            <button type="button" class="close" data-dismiss="alert">�</button>
                            <div>
    &bull;For changing the primary contact,please send a request to the school registrar
    <br />&bull;An asterisk (<font color="#ff0000">*</font>) indicates mandatory fields.<br />
         <asp:ValidationSummary ID="vsAppl_info" runat="server" ValidationGroup="groupM1"
                 HeaderText="<div class='validationheaderInside'>Please update the following:</div>"
                class="validationsummaryInside" />
    <asp:Label id="lblValidate_msg" runat="Server"></asp:Label>
            </div>
        </div>
 <table align="left" class="table table-striped table-bordered table-responsive text-left my-orders-table" width="100%">
                             <tr class="trHeader">
                                           <td align="left" colspan="4" class="sub-heading">Parent Details </td></tr>
                           
                            
                            
                            <tr>
                                <td align="left" >
                                    Preferred mode of contact</td>
                               
                                <td align="left" colspan="3" >
                                    <asp:DropDownList ID="ddlPrefContact" runat="server"  CssClass="form-control">
                                        <asp:ListItem>Home Phone</asp:ListItem>
                                        <asp:ListItem>Office Phone</asp:ListItem>
                                        <asp:ListItem>Mobile</asp:ListItem>
                                        <asp:ListItem>Email</asp:ListItem>
                                    </asp:DropDownList></td>
                            </tr>
                             
                              <tr>
                                <td align="left" >
                                    Emergency Contact Number</td>
                                <td align="left"  colspan="3">
                                                                    
                                    <asp:TextBox ID="txtEmgContact_Country" runat="server" Width="32px" MaxLength="3" CssClass="form-control"></asp:TextBox>
                                    -
                                    <asp:TextBox ID="txtEmgContact_Area" runat="server" Width="32px" MaxLength="4" CssClass="form-control"></asp:TextBox>
                                    -
                                    <asp:TextBox ID="txtEmgContact_No" runat="server" Width="89px" MaxLength="10" CssClass="form-control"></asp:TextBox>
                                    <div class="remark">(Country-Area-Number)</div>
                                    <ajaxToolkit:FilteredTextBoxExtender ID="ftbeECont_Country" runat="server" 
                                     TargetControlID="txtEmgContact_Country"
                                            FilterType="Numbers">
                                        </ajaxToolkit:FilteredTextBoxExtender>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="ftbeEContact_Area" runat="server"
                                         TargetControlID="txtEmgContact_Area"
                                            FilterType="Numbers">
                                        </ajaxToolkit:FilteredTextBoxExtender>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="ftbeEContact_No" runat="server" 
                                        TargetControlID="txtEmgContact_No"
                                            FilterType="Numbers">
                                        </ajaxToolkit:FilteredTextBoxExtender>
                                    </td>
                                    </tr>
                                      <tr class="tdblankAll">
                           <td   colspan="4" 
                           class="tdblankAll" 
                             >&nbsp;</td></tr>
                       <tr class="tdblankAll">
                           <td   colspan="4" 
                            class="tdblankAll"
                             >&nbsp;</td></tr>
                       
                       <tr class="trHeader">
                                           <td align="left" colspan="4" class="sub-heading">
                                 Primary Contact Details (<asp:Label ID="lblPrimarycontact" runat="server"></asp:Label>)
                                 &nbsp;
                                     </td>
                            </tr>
                            <tr>
                                <td align="left" >
                                  
                                    Full Name As Per Passport<span style="color: #ff0000">*</span></td>
                          
                                <td align="left"  colspan="4">
                                    <asp:TextBox ID="txtFFirstName" runat="server" Width="162px" CssClass="form-control" MaxLength="100"></asp:TextBox>
                                    <asp:TextBox ID="txtFMidName" runat="server" Width="162px" CssClass="form-control" MaxLength="100"></asp:TextBox>
                                    <asp:TextBox ID="txtFLastName" runat="server" Width="162px" CssClass="form-control" MaxLength="100"></asp:TextBox>
                                    <asp:RequiredFieldValidator
                                        ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtFFirstName"
                                         ErrorMessage="Primary contact first name required" ForeColor="red"
                                          ValidationGroup="groupM1">*</asp:RequiredFieldValidator>
                                          
                                           <ajaxToolkit:FilteredTextBoxExtender ID="ftbFFNAME" runat="server"
                                                           TargetControlID="txtFFirstName" 
                                                           FilterType="Custom,UppercaseLetters,Numbers,LowercaseLetters"
                                                            ValidChars="\.'/ "></ajaxToolkit:FilteredTextBoxExtender>
                             <ajaxToolkit:FilteredTextBoxExtender ID="ftbFMNAME" runat="server"
                                                           TargetControlID="txtFMidName" 
                                                           FilterType="Custom,UppercaseLetters,Numbers,LowercaseLetters"
                                                            ValidChars="\.'/ "></ajaxToolkit:FilteredTextBoxExtender>
                                    <ajaxToolkit:FilteredTextBoxExtender ID="ftbFLNAME" runat="server"
                                                           TargetControlID="txtFLastName" 
                                                           FilterType="Custom,UppercaseLetters,Numbers,LowercaseLetters"
                                                            ValidChars="\.'/ "></ajaxToolkit:FilteredTextBoxExtender>
                                    
                                    </td>
                            </tr>
                            <tr>
                                 <td align="left" style="width:130px;">
                                    Nationality1</td>
                             
                                <td align="left">
                                    <asp:DropDownList ID="ddlFNationality1" runat="server" Width="200px"  
                                    CssClass="form-control">
                                    </asp:DropDownList></td>
                               <td align="left" style="width:130px;">
                                    Nationality2</td>
                             
                                <td align="left" style="width:150px;">
                                    <asp:DropDownList ID="ddlFNationality2" runat="server" Width="200px" CssClass="form-control">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                           
                           
                           <tr>
                             <%--Residential Address Street Was Here--%>
                                 <td align="left">  
                                    Residential Address Country</td>
                            
                                <td align="left" >
                                    <asp:DropDownList ID="ddlFCOMCountry" runat="server" CssClass="form-control" AutoPostBack="true">
                                    </asp:DropDownList>
                                    </td>
                                    
                                <td align="left"> 
                                    Residential Address City/State <span id="SpFRaddCity" runat="server" style="color:Red;" visible="true" >*</span></td>
                                                             <td align="left"  >
                                                             
                                                              <div class="tdPadDiv">
                                                City/State</div><asp:DropDownList ID="ddlFCity" runat="server" CssClass="form-control" AutoPostBack="true">
                                            </asp:DropDownList>
                                            
                                             <asp:RequiredFieldValidator ID="rfvFRaddCity" runat="server" ControlToValidate="ddlFCity"
                                         ErrorMessage="Father's residential addresscity/state required" ForeColor="red" ValidationGroup="groupM1">*</asp:RequiredFieldValidator>
                                         
                                            <div class="remark_info" style="padding-top: 4px; padding-bottom: 4px;">
                                                If you choose other,please specify the City/State Name</div>
                                            <asp:TextBox ID="txtFCOMCity" runat="server" MaxLength="50" Width="180px" CssClass="form-control">
                                            </asp:TextBox>
                                    </td>
                           
                               <%--Residential Address Area Was Here--%>  
                                
                            </tr>
                            <tr>
                              <%--Residential Address Buiding Was Here--%>  
                                <td align="left">  
                                    Residential Address Area<span id="SpFRaddA" runat="server" style="color:Red;" visible="true" >*</span></td>
                            
                              <td align="left" >
                                 <asp:DropDownList ID="ddlFArea" runat="server" CssClass="form-control">
                                            </asp:DropDownList>
                                            
                                            <asp:RequiredFieldValidator ID="rfvFRaddA" runat="server" ControlToValidate="ddlFArea"
                                         ErrorMessage="Father's residential address area required" ForeColor="red" ValidationGroup="groupM1">*</asp:RequiredFieldValidator>
                                         
                                            <div class="remark_info" style="padding-top: 4px; padding-bottom: 4px;">
                                                If you choose other,please specify the Area Name</div>
                                    <asp:TextBox ID="txtFCOMADDR2" runat="server" CssClass="form-control"></asp:TextBox>
                                    
                                    </td>
                                    
                                     <td align="left">  
                                    Residential Address Street<span id="SpFRaddS" runat="server" style="color:Red;" visible="true" >*</span></td>
                                    
                                    <td align="left"  >
                                    <asp:TextBox ID="txtFCOMADDR1" runat="server" CssClass="form-control"></asp:TextBox>
                                     <asp:RequiredFieldValidator ID="rfvFRaddS" runat="server" ControlToValidate="txtFCOMADDR1"
                                         ErrorMessage="Father's residential address street required" ForeColor="red" ValidationGroup="groupM1">*</asp:RequiredFieldValidator></td>


                               
                            </tr>
                            
                             <tr>
                             <%--Residential Address City Was Here--%>
                               <td align="left">  
                                    Residential Address Building<span id="SpFRaddB" runat="server" style="color:Red;" visible="true" >*</span></td>
                               
                                <td align="left"  >
                                    <asp:TextBox ID="txtFBldg" runat="server" CssClass="form-control"> </asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvFRaddB" runat="server" ControlToValidate="txtFBldg"
                                         ErrorMessage="Father's residential address building required" ForeColor="red" ValidationGroup="groupM1">*</asp:RequiredFieldValidator>
                                    </td>
                                      <%--Residential Address Country Was Here--%>
                                       <td align="left"> 
                                    Residential Address Apartment No.<span id="SpFRaddN" runat="server" style="color:Red;" visible="true" >*</span></td>
                                
                                <td align="left" >
                                    <asp:TextBox ID="txtFAptNo" runat="server" CssClass="form-control"> </asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvFRaddN" runat="server" ControlToValidate="txtFAptNo"
                                         ErrorMessage="Father's residential address apartment No. required" ForeColor="red" ValidationGroup="groupM1">*</asp:RequiredFieldValidator></td>
                                                                    </tr>
                                                                    
                                                                    
                            <tr>
                                 <td align="left" >
                                    P.O BOX<span style="color: #ff0000">*</span></td>
                              
                                <td align="left">
                                    <asp:TextBox ID="txtFCOMPOBOX" runat="server" Width="86px" CssClass="form-control"></asp:TextBox>
                                                                       <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtFCOMPOBOX"
                                         ErrorMessage="P.O Box No. required" ForeColor="red" ValidationGroup="groupM1">*</asp:RequiredFieldValidator></td>
                                <td align="left" >
                                    Emirate<span style="color: #ff0000">*</span></td>
                                
                                <td align="left" >
                                    <asp:DropDownList ID="ddlFCOMEmirate" runat="server" CssClass="form-control">
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ControlToValidate="ddlFCOMEmirate"
                                         ErrorMessage="Emirate needs to selected" ForeColor="red" InitialValue="0" ValidationGroup="groupM1">*</asp:RequiredFieldValidator></td>
                            </tr>
                           
                            <tr>
                              <td align="left" >
                                    Home Phone<span style="color: #ff0000">*</span></td>
                        
                                <td align="left" >
                                
                                    <asp:TextBox ID="txtFResPhone_Country" runat="server" Width="32px" MaxLength="3" CssClass="form-control">                                 
                                    </asp:TextBox>
                                    -
                                    <asp:TextBox ID="txtFResPhone_Area" runat="server" Width="32px" MaxLength="4" CssClass="form-control"></asp:TextBox>
                                    -
                                    <asp:TextBox ID="txtFResPhone_No" runat="server" Width="89px" MaxLength="10" CssClass="form-control"></asp:TextBox>
                                   <div class="remark">(Country-Area-Number)</div>
                                          <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server"
                                           TargetControlID="txtFResPhone_Country"
                                            FilterType="Numbers">
                                        </ajaxToolkit:FilteredTextBoxExtender>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server"
                                         TargetControlID="txtFResPhone_Area"
                                            FilterType="Numbers">
                                        </ajaxToolkit:FilteredTextBoxExtender>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" 
                                        TargetControlID="txtFResPhone_No"
                                            FilterType="Numbers">
                                        </ajaxToolkit:FilteredTextBoxExtender>  
                                        </td>
                                <td align="left" >
                                    Office Phone</td>
                               
                                <td align="left">
                                    <asp:TextBox ID="txtFOffPhone_Country" runat="server" Width="32px" MaxLength="3" CssClass="form-control"></asp:TextBox>
                                    -
                                    <asp:TextBox ID="txtFOffPhone_Area" runat="server" Width="32px" MaxLength="4" CssClass="form-control"></asp:TextBox>
                                    -
                                    <asp:TextBox ID="txtFOffPhone_No" runat="server" Width="89px" MaxLength="10" CssClass="form-control"></asp:TextBox>
                                   <div class="remark">(Country-Area-Number)</div>
                                     <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server"
                                           TargetControlID="txtFOffPhone_Country"
                                            FilterType="Numbers">
                                        </ajaxToolkit:FilteredTextBoxExtender>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" 
                                        TargetControlID="txtFOffPhone_Area"
                                            FilterType="Numbers">
                                        </ajaxToolkit:FilteredTextBoxExtender>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" 
                                        runat="server" TargetControlID="txtFOffPhone_No"
                                            FilterType="Numbers">
                                        </ajaxToolkit:FilteredTextBoxExtender>  
                                    </td>
                            </tr>
                            <tr>
                                <td align="left" >
                                    Mobile<span style="color: #ff0000">*</span></td>
                            
                                <td align="left"  style="width: 200px">
                                                                    
                                    <asp:TextBox ID="txtFMobile_Country" runat="server" Width="32px" MaxLength="3" CssClass="form-control"></asp:TextBox>
                                    -
                                    <asp:TextBox ID="txtFMobile_Area" runat="server" Width="32px" MaxLength="3" CssClass="form-control"></asp:TextBox>
                                    -
                                    <asp:TextBox ID="txtFMobile_No" runat="server" Width="89px" MaxLength="10" CssClass="form-control"></asp:TextBox> 
                                    <asp:RequiredFieldValidator ID="rfvFMobNo" runat="server" ControlToValidate="txtFMobile_No"
                                    Display="Dynamic" ErrorMessage="Primary contact mobile no required" ValidationGroup="groupM1" >*</asp:RequiredFieldValidator> 
                                    <div class="remark">(Country-Area-Number)</div>
                                     <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server"
                                           TargetControlID="txtFMobile_Country"
                                            FilterType="Numbers">
                                        </ajaxToolkit:FilteredTextBoxExtender>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" 
                                        TargetControlID="txtFMobile_Area"
                                            FilterType="Numbers">
                                        </ajaxToolkit:FilteredTextBoxExtender>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server" 
                                        TargetControlID="txtFMobile_No"
                                            FilterType="Numbers">
                                        </ajaxToolkit:FilteredTextBoxExtender>      
                                    </td>
                               <td align="left" >
                                    Fax No</td>
                                
                                <td align="left" >
                                    <asp:TextBox ID="txtFFax_Country" runat="server" Width="32px" MaxLength="3" CssClass="form-control"></asp:TextBox>
                                    -
                                    <asp:TextBox ID="txtFFax_Area" runat="server" Width="32px" MaxLength="4" CssClass="form-control"></asp:TextBox>
                                    -
                                    <asp:TextBox ID="txtFFax_No" runat="server" Width="89px" MaxLength="10" CssClass="form-control"></asp:TextBox>
                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" runat="server"
                                           TargetControlID="txtFFax_Country"
                                            FilterType="Numbers">
                                        </ajaxToolkit:FilteredTextBoxExtender>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender11" runat="server" 
                                        TargetControlID="txtFFax_Area"
                                            FilterType="Numbers">
                                        </ajaxToolkit:FilteredTextBoxExtender>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender12" runat="server" 
                                        TargetControlID="txtFFax_No"
                                            FilterType="Numbers">
                                        </ajaxToolkit:FilteredTextBoxExtender> 
                                    </td>
                            </tr>
                             <tr class="tdblankAll">
                           <td   colspan="4" 
                           class="tdblankAll" 
                             >&nbsp;</td></tr>
                       <tr class="tdblankAll">
                           <td   colspan="4" 
                            class="tdblankAll"
                             >&nbsp;</td></tr>
                       
                       <tr class="trHeader">
                                           <td align="left" colspan="4"  >   
                                 <asp:CheckBox ID="chkCopy" runat="server" Text="Copy from above"  
                                 onclick="copyFromAbove(this)" ToolTip="copyFromAbove"  CssClass="checkbox"/>&nbsp;</td>
                                
                             </tr>
                            <tr>
                                 <td align="left" >
                                    Overseas Address Line 1<span style="color: #ff0000">*</span></td>
                               
                                <td align="left" >
                                    <asp:TextBox ID="txtFPRMAddr1" runat="server" CssClass="form-control"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ControlToValidate="txtFPRMAddr1"
                                         ErrorMessage="Required overseas address Line 1" ForeColor="red" ValidationGroup="groupM1">*</asp:RequiredFieldValidator></td>
                                 <td align="left" >
                                    Overseas Address Line2<span style="color: #ff0000">*</span></td>
                          
                                <td align="left">
                                    <asp:TextBox ID="txtFPRMAddr2" runat="server" CssClass="form-control"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ControlToValidate="txtFPRMAddr2"
                                         ErrorMessage="Required overseas address Line 2" ForeColor="red" ValidationGroup="groupM1">*</asp:RequiredFieldValidator></td>
                            </tr>
                            <tr>
                                 <td align="left" >
                                    Overseas Address City/State<span style="color: #ff0000">*</span></td>
                            
                                <td align="left" >
                                    <asp:TextBox ID="txtFPRM_City" runat="server" CssClass="form-control"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ControlToValidate="txtFPRM_City"
                                         ErrorMessage="Required overseas address city/state" ForeColor="red" ValidationGroup="groupM1">*</asp:RequiredFieldValidator></td>
                                <td align="left" >
                                    Overseas Address Country<span style="color: #ff0000">*</span></td>
                             
                                <td align="left" >
                                    <asp:DropDownList ID="ddlFPRM_Country" runat="server" CssClass="form-control">
                                    </asp:DropDownList></td>
                            </tr>
                            <tr>
                              <td align="left" >
                                    Permanent Phone<span style="color: #ff0000">*</span></td>
                          
                                <td align="left" >
                                    <asp:TextBox ID="txtFPPRM_Country" runat="server" Width="32px" MaxLength="3" CssClass="form-control"></asp:TextBox>
                                    -
                                    <asp:TextBox ID="txtFPPRM_Area" runat="server" Width="32px" MaxLength="4" CssClass="form-control"></asp:TextBox>
                                    -
                                    <asp:TextBox ID="txtFPPRM_No" runat="server" Width="89px" MaxLength="10" CssClass="form-control"></asp:TextBox>
                                   <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ControlToValidate="txtFPPRM_No"
                                         ErrorMessage="Required permanent phone number" ForeColor="red" ValidationGroup="groupM1">*</asp:RequiredFieldValidator>
                                        <div class="remark">(Country-Area-Number)</div>
                                     <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender13" runat="server" 
                                     TargetControlID="txtFPPRM_Country"
                                            FilterType="Numbers">
                                        </ajaxToolkit:FilteredTextBoxExtender>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender14" runat="server"
                                         TargetControlID="txtFPPRM_Area"
                                            FilterType="Numbers">
                                        </ajaxToolkit:FilteredTextBoxExtender>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender15" 
                                        runat="server" TargetControlID="txtFPPRM_No"
                                            FilterType="Numbers">
                                        </ajaxToolkit:FilteredTextBoxExtender> 
                                       </td>
                                <td align="left" >
                                    P.O BOX<span style="color: #ff0000">*</span></td>
                              
                                <td align="left" >
                                    <asp:TextBox ID="txtFPRMPOBOX" runat="server" Width="86px" CssClass="form-control" MaxLength="20"></asp:TextBox>
                                    
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator16" runat="server" ControlToValidate="txtFPRMPOBOX"
                                         ErrorMessage="P.O Box No. required" ForeColor="red" ValidationGroup="groupM1">*</asp:RequiredFieldValidator></td>
                            </tr>
                            
                            <tr>
                                <td align="left" >
                                    Occupation<span style="color: #ff0000">*</span></td>
                           
                                <td align="left">
                                    <asp:TextBox ID="txtFOcc" runat="server" CssClass="form-control"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator17" runat="server" ControlToValidate="txtFOcc"
                                         ErrorMessage="Occupation required" ForeColor="red" ValidationGroup="groupM1">*</asp:RequiredFieldValidator></td>
                                 <td align="left" >
                                    Company Name</td>
                               
                                <td align="left" >
                                    <asp:DropDownList id="ddlFCompany_Name" runat="server" CssClass="form-control">
                                    </asp:DropDownList>
                                 <div class="remark" style="display:block;">(If you choose other,please specify the Company Name)</div>
                                    <asp:TextBox ID="txtFComp_Name" runat="server" CssClass="form-control"></asp:TextBox></td>
                            </tr>
                            <tr>
                                 <td align="left" >
                                    Company Address</td>
                            
                                <td align="left" >
                                    <asp:TextBox ID="txtFComp_Add" runat="server" CssClass="form-control"></asp:TextBox></td>
                                <td align="left" >
                                    Email<span style="color: #ff0000">*</span></td>
                               
                                <td align="left" >
                                    <asp:TextBox ID="txtFEmail" runat="server" CssClass="form-control"></asp:TextBox>
                                      <asp:RegularExpressionValidator ID="RegularExpressionValidator18" runat="server" ControlToValidate="txtFEmail"
                            Display="Dynamic" ErrorMessage="Enter valid email address"
                            ValidationExpression="^([0-9a-zA-Z]([-\.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$"
                            ValidationGroup="groupM1" ForeColor="red">*</asp:RegularExpressionValidator>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtFEmail"
                                        ErrorMessage="Email address required" ForeColor="red" ValidationGroup="groupM1">*</asp:RequiredFieldValidator></td>
                            </tr>
                             <tr>
                                  <td colspan="4">&nbsp;
                                 </td>
                             </tr>
                             <tr>
                                 <td align="center"  colspan="4">
                                     <asp:Button ID="btnSave" runat="server" Text="Save" ValidationGroup="groupM1"  CssClass="btn btn-info" />&nbsp;<asp:Button ID="btnSignout" runat="server" Text="Sign Out" CausesValidation="False"  CssClass="btn btn-info"/></td>
                             </tr>
                             <tr>
                             <td colspan="4"> <asp:ValidationSummary ID="ValidationSummary1" runat="server"  EnableViewState="False"
                                        ValidationGroup="groupM1"   HeaderText="<div class='validationheader'>Please correct the following:</div>"
                                 CssClass="divinfoInner" ForeColor=""/></td></tr>
                          </table>
                          <asp:HiddenField ID="h_StuID" runat="server" />
             <%--      <asp:Button ID="btnTarget" runat="server" Text="Button" Style="display: none" />
                     <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" runat="server" BackgroundCssClass="modalBackground"
                         DropShadow="True" PopupControlID="plPrev" TargetControlID="btnTarget" x="450" Y="160">
                     </ajaxToolkit:ModalPopupExtender>
     
                  
                  <div id="plPrev"  runat="server" style="width: 300px;display: none; overflow: visible; border-color: gray; border-style:solid;border-width:1px;">
         <div class="subheader_img" style="width: 300px;"> <div class="msg" align="center">
             Message</div>
           </div>
         
                  <asp:Panel ID="Panel1" runat="server" BackColor="white" Width="300px" Height="100px" >
                         <table border="0px" cellpadding="0px" cellspacing="0px">
                             <tr align="center" style="height:4px;vertical-align:middle">
                                 <td  colspan="3" rowspan="1" style="height: 28px" >
                                     <img src="../ParentLogin/Images/loading.gif" width="250px" /></td>
                             </tr>
                             <tr align="center">
                                 <td  colspan="3" 
                                     style="height: 45px;font-size:12px;line-height:20px;">
                                     Thank you for updating the Primary contacts Details.
                                     &nbsp;<asp:LinkButton ID="lnkbtn" runat="server" Font-Size="12px">Click Here</asp:LinkButton>to
                                     go to the home page</td>
                             </tr>
                         </table>
                     </asp:Panel>
                          </div>--%>
                    
               
        </div>
                           
                            </div>
                        <!-- /Table  -->
                        </div>

                    </div>
                </div>
                <!-- /Posts Block -->

            </div>
        </div>
    </div>
</asp:Content>