<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/ParentMaster.master" 
 CodeFile="ForcePasswordChange.aspx.vb" Inherits="ParentForcePasswordChange" Title="GEMS EDUCATION" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphParent" Runat="Server">    <%--<SCRIPT src="../ParentLogin/../LoginStyle/rotator.js" type=text/javascript></SCRIPT>--%>
        <script language="javascript" type="text/javascript">
         function TermsOfUse(url) 
        {            
            var width = 600;
            var height = 400;
            var left = parseInt((screen.availWidth/2) - (width/2));
            var top = parseInt((screen.availHeight/2) - (height/2));
            var windowFeatures = "width=" + width + ",height=" + height + ",status,resizable,left=" + left + ",top=" + top + "screenX=" + left + ",screenY=" + top;
            myWindow = window.open(url, "subWind", windowFeatures);
            return false;
        } 
    
      function SetSize()
            {
                if(screen.width == 1024)
                {
                    document.getElementById('SignIn').style.width = '1002px';
                }
                else if(screen.width == 1280)
                {
                    document.getElementById('SignIn').style.width = '1259px';
                }
                if(screen.height == 768)
                {
                    document.getElementById('SignIn').style.height = '560px';
                }
                else if(screen.height == 800)
                {
                    document.getElementById('SignIn').style.height = '580px';
                }
            }
            function correctingdate(len)
            {
           
                  if(len[1]==01)
                  {
                      document.getElementById('<%=txt_DOB.ClientID %>').value = len[0] + '/Jan/' + len[2]     
                   return
                  }  
                  else if(len[1]==02)
                  {
                      document.getElementById('<%=txt_DOB.ClientID %>').value = len[0] + '/Feb/' + len[2]     
                   return
                  }  
                  else if(len[1]==03)
                  {
                      document.getElementById('<%=txt_DOB.ClientID %>').value = len[0] + '/Mar/' + len[2]     
                   return
                  }
                  else if(len[1]==04)
                  {
                      document.getElementById('<%=txt_DOB.ClientID %>').value = len[0] + '/Apr/' + len[2]     
                   return
                  }
                  else if(len[1]==05)
                  {
                      document.getElementById('<%=txt_DOB.ClientID %>').value = len[0] + '/May/' + len[2]     
                   return
                  }
                   else if(len[1]==06)
                 {
                     document.getElementById('<%=txt_DOB.ClientID %>').value = len[0] + '/Jun/' + len[2]     
                  return
                 }
                 else if(len[1]==07)
                 {
                     document.getElementById('<%=txt_DOB.ClientID %>').value = len[0] + '/Jul/' + len[2]     
                  return
                 }
                 else if(len[1]==08)
                 {
                     document.getElementById('<%=txt_DOB.ClientID %>').value = len[0] + '/Aug/' + len[2]     
                  return
                 }
                  else if(len[1]==09)
                 {
                     document.getElementById('<%=txt_DOB.ClientID %>').value = len[0] + '/Sep/' + len[2]     
                  return
                 }
                 else if(len[1]==10)
                 {
                     document.getElementById('<%=txt_DOB.ClientID %>').value = len[0] + '/Oct/' + len[2]     
                  return
                 }
                 else if(len[1]==11)
                 {
                     document.getElementById('<%=txt_DOB.ClientID %>').value = len[0] + '/Nov/' + len[2]     
                  return
                 }
                 else if(len[1]==12)
                 {
                     document.getElementById('<%=txt_DOB.ClientID %>').value = len[0] + '/Dec/' + len[2]     
                  return
                 }                      
            }
          function valDate()
          {
              var date = document.getElementById('<%=txt_DOB.ClientID %>').value 
            if(date.search('/')==2)
            {
                var len=date.split('/')  
            }       
            else if(date.search('-')==2)
            {
                var len=date.split('-')  
            } 
            else
            {
                alert('Date format is like (eg 01/jan/2010)')
                return false
            }                     
            if(len[1]>12)
            {
                alert('Date format is like (eg 01/jan/2010)')
                return false
            }  
            correctingdate(len)
            return true   
          }      
      function validateOrFields(source, args)
      {
      var aa= valDate() 
//      alert(aa)
          if(aa==false)
          {    
              args.IsValid = false;         
          }
           else
           {
              args.IsValid = true;         
           }  
       return;  
     }
        </script>
        
         <div class="mainheading">
      <div class="left">Change your Password</div>
             </div>
    <div> 
     <div class="alert-warning alert">
         <button type="button" class="close" data-dismiss="alert">�</button>
     <div>
         <ul>
         <li>An asterisk (<font color="#ff0000">*</font>) indicates mandatory fields.</li>
         <li>Current password and new password cannot be same.</li>
         <li>Password must be at least 7 characters long.</li>
         </ul>
     </div>
  
     
     </div>
    <table id="tbl_AddGroup" runat="server"  align="left" class="table table-striped table-bordered table-responsive text-left my-orders-table" width="100%">
                                            <tr>
                                              <td align="left" class="tdfields" width="350px">
                                               
                                                      Enter your Current Password<font color="red">*</font>
                                              </td>
                                          
                                              <td align="left" >
                                                  <asp:TextBox ID="txtCurrentPassword" runat="server" MaxLength="100"
                                                  CssClass="form-control"
                                                      TextMode="Password"></asp:TextBox>
                                                  <asp:RequiredFieldValidator ID="rfvPassword" runat="server" ControlToValidate="txtCurrentPassword"
                                                       Display="Dynamic" ErrorMessage="CurrentPassword can not be left empty"
                                                      ForeColor="red" SetFocusOnError="True" ValidationGroup="GroupPwd">*</asp:RequiredFieldValidator>
                                              </td>
                                          </tr>
                                          <tr>
                                              <td align="left" class="tdfields">
                                                  Enter your New Password<font color="red">*</font></td>
                                              
                                              <td align="left" >
                                                  <asp:TextBox ID="txtNewpassword" runat="server" MaxLength="100" 
                                                 CssClass="form-control"
                                                      TextMode="Password"></asp:TextBox>
                                                 <%--  <ajaxToolkit:FilteredTextBoxExtender ID="ftbxNewpassword"
                             runat="server"  TargetControlID="txtNewpassword"
                                    FilterType="UppercaseLetters,LowercaseLetters,Numbers" >
                                </ajaxToolkit:FilteredTextBoxExtender>--%>
                       <%-- <asp:RegularExpressionValidator ID="revPassword" runat="server" ControlToValidate="txtNewpassword"
                            Display="Dynamic" ErrorMessage="Your password must contain atleast  7 characters"
                            ForeColor="red" SetFocusOnError="True" ValidationExpression="^[a-zA-Z0-9]{7,20}$" ValidationGroup="GroupPwd">*</asp:RegularExpressionValidator>--%>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtNewpassword"
                            Display="Dynamic" ErrorMessage="Please enter your new password"
                            ForeColor="red" ValidationGroup="GroupPwd">*</asp:RequiredFieldValidator>
                                                    
                                              </td>
                                          </tr>
                                          <tr>
                                              <td align="left" class="tdfields" style="height: 32px" >
                                                  Confirm your New Password<font color="red">*</font></td>
                                            
                                              <td align="left" style="height: 32px" >
                                                  <asp:TextBox ID="txtConfPassword" runat="server" CssClass="form-control" 
                                                  TextMode="Password"
                                                     ></asp:TextBox>
                                                   <%--<ajaxToolkit:FilteredTextBoxExtender ID="ftbxConfPassword"
                             runat="server"  TargetControlID="txtConfPassword"
                                    FilterType="UppercaseLetters,LowercaseLetters,Numbers" >
                                </ajaxToolkit:FilteredTextBoxExtender>--%>
                        <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="txtNewpassword"
                            ControlToValidate="txtConfPassword" Display="Dynamic" ErrorMessage="Password do not match"
                            ForeColor="red" ValidationGroup="GroupPwd">*</asp:CompareValidator>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtConfPassword"
                            Display="Dynamic" ErrorMessage="Confirm password can not be left empty"
                            ForeColor="red" ValidationGroup="GroupPwd">*</asp:RequiredFieldValidator></td>
                                          </tr>
                                          <tr>
                                              <td align="left" class="tdfields">
                                                  Email Address for password recovery<font color="red">*</font></td>
                                              
                                              <td align="left" >
                                                  <asp:TextBox ID="txtemailid" runat="server" CssClass="form-control"></asp:TextBox>
                                                  <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" CssClass="error"
                                                      ErrorMessage="Email address required" ForeColor="red" ValidationGroup="GroupPwd" ControlToValidate="txtemailid">*</asp:RequiredFieldValidator>
                                                  <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" CssClass="error"
                                                      ErrorMessage="Not a valid email address" ForeColor="red" ValidationGroup="GroupPwd" ControlToValidate="txtemailid" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:RegularExpressionValidator></td>
                                          </tr>
                                          <tr>
                                              
                                              <td align="center" colspan="2">
                                                  <asp:Button ID="btnSave" runat="server" 
                                                   Text="Save" ValidationGroup="GroupPwd"
                                                     CssClass="btn btn-info"/>
                                                  <asp:Button ID="btnClear" runat="server" CausesValidation="False"
                                                   Text="Clear" OnClientClick="redirect()"
                                                      CssClass="btn btn-info" />
       <%--                                           <asp:Button ID="btnCancel" runat="server" CausesValidation="False" Height="20px"
                                                      Text="Cancel" Width="70px" OnClientClick="redirect()" />--%></td>
                                          </tr>
                                          
                                      </table>
                                       <asp:ValidationSummary ID="vsVisaDetails" runat="server"  ValidationGroup="GroupPwd" 
                                  HeaderText="<div class='validationheader'>Please correct the following:</div>"
                                 CssClass="divinfoInner" ForeColor=""/>
                                
                
                    <asp:Panel ID="Panel1" runat="server"  CssClass="modalPopup" width="800px"
                         Style="display:block;" >
                          
                         <div class="mainheading"><div class="left">Authentication</div> </div>
    <div>
<div class="divinfo" style="width:98.4%;padding:3px;font-size:10px;" id="divInfo" runat="server" >
 </div>
                        <div id="DIVpOP" runat="server" visible="false">
                       <table   width="100%" class="tableNoborder">
                                <tr>
                                 <td class="tdfields" width="300px">
                                     Student Id (Admission number)<font color="red">*</font></td>
                               
                                 <td align="left" >
                                     <asp:TextBox ID="txtFee_id" runat="server" CssClass="input" Width="150px"></asp:TextBox><asp:RequiredFieldValidator        ID="RequiredFieldValidator4" runat="server" 
                                     ControlToValidate="txtFee_id"
                                         ErrorMessage="Student Id required" ForeColor="red"  Display="Dynamic"
                                         ValidationGroup="GroupAut">Student Id required</asp:RequiredFieldValidator>
                                         <ajaxToolkit:FilteredTextBoxExtender ID="ftStu_id" runat="server" FilterType="Numbers" FilterMode="ValidChars"
                                          TargetControlID="txtFee_id" ></ajaxToolkit:FilteredTextBoxExtender>
                                         </td>
                             </tr>
                             <tr>
                                 <td class="tdfields">
                                     Date of Birth<font color="red">*</font></td>
                                
                                 <td align="left">
                                     <asp:TextBox ID="txt_DOB" runat="server" CssClass="input" Width="120px"></asp:TextBox>
                                                            <asp:ImageButton ID="imgCal" runat="server" ImageUrl="~/Images/Common/PageBody/Calendar.png"  CssClass="imgCal"/><asp:RequiredFieldValidator ID="RequiredFieldValidator5" 
                                                                         runat="server" ControlToValidate="txt_DOB"
                                          ErrorMessage="Student date of birth required" ForeColor="red"  Display="Dynamic"
                                         ValidationGroup="GroupAut">Student date of birth required</asp:RequiredFieldValidator><asp:CustomValidator ID="CustomValidator1" runat="server" ErrorMessage="Not a valid Date" 
                                      ForeColor="Red"
                                     ClientValidationFunction="validateOrFields" ControlToValidate="txt_DOB"  Display="Dynamic"
                                         ValidationGroup="GroupAut">Not a valid Date</asp:CustomValidator>
                                      <div class="remark">(dd/mmm/yyyy)</div>
                                     <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MMM/yyyy"
                         PopupButtonID="imgCal" TargetControlID="txt_DOB">
                     </ajaxToolkit:CalendarExtender></td>
                             </tr>
                             <tr>
                                                            
                                 <td colspan="2" align="center">
                                     <asp:Button ID="btnSubmit" runat="server" Text="Submit" 
                                         ValidationGroup="GroupAut" Width="60px"
                                      CssClass="buttons" /><asp:Button ID="btn_signout"
                                         runat="server" Text="SignOut" CausesValidation="False"  CssClass="buttons" Width="60px"/></td>
                             </tr>
                             <tr>
                                 <td colspan="2" align="left" >
                                 
                                 <asp:Label ID="lblerror2" runat="server" EnableViewState="False"></asp:Label>
                               </td>
                             </tr>
                         </table>
                        </div>
        </div> 
                     </asp:Panel>
              
                     <asp:Label ID="lblPopup" runat="server"></asp:Label>
                     <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" 
                     runat="server" BackgroundCssClass="modalBackground" DropShadow="false"
                      PopupControlID="Panel1" TargetControlID="lblPopup">
                     </ajaxToolkit:ModalPopupExtender>
                                     
                        <ajaxToolkit:PasswordStrength ID="PasswordStrength1" runat="server" HelpStatusLabelID="TextBox1_HelpLabel"
                              MinimumNumericCharacters="2" PreferredPasswordLength="6" PrefixText="Strength:"
                              RequiresUpperAndLowerCaseCharacters="true" StrengthStyles="red;blue;grey;yellow;green"
                              TargetControlID="txtNewpassword" TextCssClass="" TextStrengthDescriptions="Very Poor;Weak;Average;Strong;Excellent">
                          </ajaxToolkit:PasswordStrength>   
                 
            
                     
                  
                   
               
   </div>         
      
</asp:Content>

