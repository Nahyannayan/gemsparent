<%@ Page Language="VB" MasterPageFile="~/ParentMaster.master" AutoEventWireup="true"
 CodeFile="UploadPhoto.aspx.vb" Inherits="UpdateInfo_UploadPhoto" title="GEMS EDUCATION" %>
   <%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ OutputCache Location="None" Duration="1" VaryByParam="None" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphParent" Runat="Server">

   

  <div class="content margin-top30 margin-bottom60">
        <div class="container">
            <div class="row">

                <!-- Posts Block -->
                <div class="posts-block col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="my-account">

                              <asp:Label ID="lblmsg" runat="server"></asp:Label> 
<div class="mainheading">
      <div>Upload Photo
       <div class="right"><asp:label ID="lbChildName" runat="server" CssClass="lblChildNameCss" ></asp:label></div></div>
      </div>
      <div id="divNote" runat="server"   class="alert alert-warning">
          <button type="button" class="close" data-dismiss="alert">�</button>
                  &nbsp;&nbsp;Upload a recent photograph of your child in school uniform.<br />&nbsp;&nbsp;Upload file format:JPG, file size max:1MB, resize to:3.5cmx4.5cm.  

                  </div>
    <div> 
         <table align="left" class="table table-striped table-bordered table-responsive text-left my-orders-table" width="100%">
          <tr>
                                           <td  colspan="4" class="sub-heading" >
                                      <span  style=" display: inline; left: 0px; position: relative; top: 3px; height: 3px; z-index: 100;"> &nbsp;Student Photo</span><span  style="float:right;">
                                </span>    
                                  </td>
                            
                            </tr>
    </table>
 <table align="left" class="table table-striped table-bordered table-responsive text-left my-orders-table" width="100%">
      <tr>
                     <td align="left"  style="width: 140px">
                     
              <div id="box">				

		<div id="lb">
		<div id="rb">
		<div id="bb"><div id="blc"><div id="brc">
		<div id="tb"><div id="tlc"><div id="trc">

				
		<div id="content2">	
		 <asp:Image ID="imgParentImage" runat="server" Height="132px" 
		 ImageUrl="~/Images/Common/PageBody/no_image.gif" Width="120px"  />
</div>
		
	
		</div></div></div></div>
		</div></div></div></div>
	
		
       
	</div>
                     </td>
                     <td align="left"  >
                     <%--<telerik:RadUpload runat="server" ID="fileUpload"   Width="400px" Height="25px" CssClass="form-control"
     AllowedFileExtensions=".jpg"     MaxFileSize="20000"  
     ControlObjectsVisibility="None" ></telerik:RadUpload>--%>
                         <asp:FileUpload ID="fileUpload" runat="server" />
                         <asp:Label ID="divUploadmsg" runat="server" ></asp:Label> <br /><div class="alert alert-info">
                     Upload a recent photograph of your child in school uniform. <br />
              Upload file format:JPG, file size max:1mb, resize to:3.5cmx4.5cm</div>
                     </td>
  
                     </tr> 
                   
                     
                      <tr>
                        <td align="center" colspan="2">
                        <asp:Button ID="btnSave" runat="server"  Text="Save"  
                         ValidationGroup="groupM1"  CssClass="btn btn-info"/>&nbsp;<asp:Button ID="btnBack" runat="server"  Text="Cancel"   CssClass="btn btn-info"/>&nbsp;
                      </td>
                     
                     </tr> 
                      </table>
                      
    <!-- father photo -->                  </div>
    <div>
        <table align="left" class="table table-striped table-bordered table-responsive text-left my-orders-table"  width="100%" id="tblFather" runat="server">
         <tr class="trSub_Header">
                                           <td  colspan="4" class="sub-heading" >
                                      <span  style="display: inline; left: 0px; position: relative; top: 3px; height: 3px; z-index: 100;"> &nbsp;Father Photo</span><span  style="float:right;">
                                </span>    
                                  </td>
                            
                            </tr>

                <tr>
                     <td align="left"  style="width: 140px">
                     
              <div id="box">				

		<div id="lb">
		<div id="rb">
		<div id="bb"><div id="blc"><div id="brc">
		<div id="tb"><div id="tlc"><div id="trc">

				
		<div id="content2">	
		 <asp:Image ID="imgFatherPhoto" runat="server" Height="132px" 
		 ImageUrl="~/Images/Common/PageBody/no_image.gif" Width="120px"  />
</div>
		
	
		</div></div></div></div>
		</div></div></div></div>
	
		
       
	</div>
                     </td>
                     <td align="left"  >
                           <asp:FileUpload ID="radFatherPhoto" runat="server" CssClass="form-control"/>      
                    <asp:Label ID="lblFather" runat="server"></asp:Label> <br /><span class="remark">
                    
              &nbsp;Upload file format:JPG, file size max:1MB, resize to:3.5cmx4.5cm</span>
                     </td>
  
                     </tr> 
                   
                     
                      <tr>
                        <td align="center" colspan="2">
                        <asp:Button ID="btnUploadFatherPhoto" runat="server"  Text="Save" 
                         ValidationGroup="groupM3"   CssClass="btn btn-info"/>&nbsp;<asp:Button ID="btnCancel" runat="server"  Text="Cancel"   CssClass="btn btn-info"/>&nbsp;
                      </td>
                     
                     </tr> 
            </table>
    </div>

      <!-- mother photo -->                 
    <div>
        <table align="left" class="table table-striped table-bordered table-responsive text-left my-orders-table"  width="100%" id="tblMother" runat="server">
         <tr class="trSub_Header">
                                           <td  colspan="4" class="sub-heading" >
                                      <span  style="display: inline; left: 0px; position: relative; top: 3px; height: 3px; z-index: 100;"> &nbsp;Mother Photo</span><span  style="float:right;">
                                </span>    
                                  </td>
                            
                            </tr>

                <tr>
                     <td align="left"  style="width: 140px">
                     
              <div id="box">				

		<div id="lb">
		<div id="rb">
		<div id="bb"><div id="blc"><div id="brc">
		<div id="tb"><div id="tlc"><div id="trc">

				
		<div id="content2">	
		 <asp:Image ID="imgMother" runat="server" Height="132px" 
		 ImageUrl="~/Images/Common/PageBody/no_image.gif" Width="120px"  />
</div>
		
	
		</div></div></div></div>
		</div></div></div></div>
	
		
       
	</div>
                     </td>
                     <td align="left"  >
                              <asp:FileUpload ID="radMotherPhoto" runat="server" CssClass="form-control"/>&nbsp;
                   <asp:Label ID="lblMother" runat="server"></asp:Label> <br /><span class="remark">
                    
              &nbsp;Upload file format:JPG, file size max:1MB, resize to:3.5cmx4.5cm</span>
                     </td>
  
                     </tr> 
                   
                     
                      <tr>
                        <td align="center" colspan="2">
                        <asp:Button ID="btnUploadMotherPhoto" runat="server"  Text="Save" 
                         ValidationGroup="groupM4"   CssClass="btn btn-info"/>&nbsp;<asp:Button ID="Button2" runat="server"  Text="Cancel"   CssClass="btn btn-info"/>&nbsp;
                      </td>
                     
                     </tr> 
            </table>
    </div>
      <!-- guardian photo -->                 
    <div>
        <table align="left" class="table table-striped table-bordered table-responsive text-left my-orders-table"  width="100%" id="tblGuardian" runat="server">
         <tr class="trSub_Header">
                                           <td  colspan="4"  class="sub-heading">
                                      <span  style="display: inline; left: 0px; position: relative; top: 3px; height: 3px; z-index: 100;"> &nbsp;Guardian Photo</span><span  style="float:right;">
                                </span>    
                                  </td>
                            
                            </tr>

                <tr>
                     <td align="left"  style="width: 140px">
                     
              <div id="box">				

		<div id="lb">
		<div id="rb">
		<div id="bb"><div id="blc"><div id="brc">
		<div id="tb"><div id="tlc"><div id="trc">

				
		<div id="content2">	
		 <asp:Image ID="imgGuardian" runat="server" Height="132px" 
		 ImageUrl="~/Images/Common/PageBody/no_image.gif" Width="120px"  />
</div>
		
	
		</div></div></div></div>
		</div></div></div></div>
	
		
       
	</div>
                     </td>
                     <td align="left"  >
                             <asp:FileUpload ID="radGuardian" runat="server" CssClass="form-control"/>&nbsp;
               
                  <asp:Label ID="lblGuardian" runat="server"></asp:Label> <br /><span class="remark">
                    
              &nbsp;Upload file format:JPG, file size max:1MB, resize to:3.5cmx4.5cm</span>
                     </td>
  
                     </tr> 
                   
                     
                      <tr>
                        <td align="center" colspan="2">
                        <asp:Button ID="btnUploadGuardianPhoto" runat="server"  Text="Save"  
                         ValidationGroup="groupM4"   CssClass="btn btn-info"/>&nbsp;<asp:Button ID="Button4" runat="server"  Text="Cancel"   CssClass="btn btn-info"/>&nbsp;
                      </td>
                     
                     </tr> 
            </table>
    </div>


                      <asp:HiddenField ID="hfParent" runat="server"/>                                             
                <asp:HiddenField ID="hfUploadPath" runat="server"/>                                             
               
              </div>
            </div>
        </div>
    </div>
    </div>
    </asp:Content>