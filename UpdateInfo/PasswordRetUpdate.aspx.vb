Imports Microsoft.ApplicationBlocks.Data
Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Text

Partial Class PasswordRetUpdate
    Inherits System.Web.UI.Page
    Dim SessionFlag As Integer
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load



        lbPwdAsst.Text = "<div class='left'><div class='right'>Password Assistance</div></div>"
        Page.Title = "GEMS EDUCATION"
        If Page.IsPostBack = False Then
            If Not (Request.QueryString("GID")) Is Nothing Then
                getLinkStatus(Request.QueryString("GID").Replace(" ", "+"))

            Else
                lblError.CssClass = "diverror"
                lblError.Text = "Link Expired !!! "
                btnLogin.Visible = False

            End If



        End If

    End Sub

    Private Sub getLinkStatus(ByVal GID As String)
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim pParms(4) As SqlClient.SqlParameter

        pParms(1) = New SqlClient.SqlParameter("@GID", GID)
        Using reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "OPL.GETLINK_STATUS", pParms)
            If reader.HasRows = False Then
                lblError.CssClass = "diverror"
                lblError.Text = "The link has expired. Please redo the reset password process. "
                btnLogin.Visible = False
            Else
                lblError.CssClass = ""
                lblError.Text = ""
                btnLogin.Visible = False
            End If
        End Using

    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim status As Integer
            Dim USR_NAME As String = String.Empty
            Dim errormessage As String = String.Empty
            Dim GID As String = String.Empty
            If Not (Request.QueryString("GID").Replace(" ", "+")) Is Nothing Then
                GID = Request.QueryString("GID").Replace(" ", "+")
            End If

            If Page.IsValid = True Then



                If txtNewpassword.Text.Trim.Length < 7 Then
                    lblError.CssClass = "diverror"
                    lblError.Text = "Your password must contain atleast  7 characters"
                    Exit Sub
                End If

                Dim transaction As SqlTransaction
                Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                    Try
                        transaction = conn.BeginTransaction("SampleTransaction")

                        Dim pParms(10) As SqlClient.SqlParameter
                        pParms(0) = New SqlClient.SqlParameter("@GID", GID)
                        pParms(1) = New SqlClient.SqlParameter("@NEW_PASSWORD", Encr_decrData.Encrypt(txtNewpassword.Text))

                        pParms(2) = New SqlClient.SqlParameter("@USR_NAME", SqlDbType.VarChar, 100)
                        pParms(2).Direction = ParameterDirection.Output
                        pParms(3) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                        pParms(3).Direction = ParameterDirection.ReturnValue
                        SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "OPL.SAVEPARENT_NEW_PASSWORD_RESET", pParms)
                        status = pParms(3).Value


                        If status = -121 Then
                            lblError.CssClass = "diverror"
                            lblError.Text = "The link has expired. Please redo the reset password process"
                        ElseIf status = -111 Then
                            lblError.CssClass = "diverror"
                            lblError.Text = "Old password and new password are same!!!Please re-enter the passwords !"

                        ElseIf status <> 0 Then

                            lblError.CssClass = "diverror"
                            lblError.Text = "Password reset failed..!"
                        Else
                            USR_NAME = IIf(TypeOf (pParms(2).Value) Is DBNull, String.Empty, pParms(2).Value)

                        End If
                    Catch ex As Exception
                        status = -1
                        errormessage = ex.Message
                        UtilityObj.Errorlog(ex.Message)
                        lblError.CssClass = "diverror"
                        lblError.Text = "Password recovery failed..!"
                    Finally
                        If status <> 0 Then
                            UtilityObj.Errorlog(errormessage)
                            transaction.Rollback()

                        Else
                            errormessage = ""
                            transaction.Commit()

                        End If
                    End Try

                End Using

                If status = 0 Then


                    status = VerifyandUpdatePassword(Encr_decrData.Encrypt(txtNewpassword.Text), USR_NAME)
                    Dim vGLGUPDPWD As New com.ChangePWDWebService
                    vGLGUPDPWD.Url = "http://10.10.1.35/release/ChangePWDWebService.asmx"
                    Dim respon As String = vGLGUPDPWD.ChangePassword(USR_NAME, txtNewpassword.Text, txtNewpassword.Text)
                    btnLogin.Visible = True
                    btnSave.Enabled = False
                    lblError.CssClass = "divvalid"
                    lblError.Text = "Password updated Successfully  !"
                    CLEAR()
                End If
            End If
        Catch ex As Exception
            lblError.CssClass = "diverror"
            lblError.Text = "Password recovery failed..!"
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Private Sub CLEAR()
        txtConfPassword.Text = ""
        txtNewpassword.Text = ""

    End Sub
    Private Function VerifyandUpdatePassword(ByVal New_Password As String, _
  ByVal username As String) As Integer
        Dim connection As SqlConnection
        Try
            connection = ConnectionManger.GetGLGConnection()
            Dim pParms(2) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@USR_NAME", username)
            pParms(1) = New SqlClient.SqlParameter("@NEW_PWD", New_Password)
            pParms(2) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(2).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "GLG.GLG_AD_PWDUPD", pParms)
            'SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "GLG.GLG_AD_PWDUPD", pParms)
            Return pParms(2).Value

        Catch
        Finally
            connection.Close()
        End Try
    End Function

    Protected Sub btnLogin_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLogin.Click
        Response.Redirect("https://oasis.gemseducation.com/general/Home.aspx")
    End Sub

    Protected Sub lbHomeTop_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbHomeTop.Click
        Response.Redirect("https://oasis.gemseducation.com/general/Home.aspx")
    End Sub
End Class
