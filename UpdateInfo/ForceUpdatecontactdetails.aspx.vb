Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Collections.Generic
Imports System.Collections
Partial Class ParentLogin_ForceUpdatecontactdetails
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        Page.Title = "GEMS EDUCATION"
        If Session("username") Is Nothing Then
            Session("Active_tab") = "Home"
            Session("Site_Path") = ""
            Response.Redirect("~\login.aspx")
        End If

        'Dim lblmsg As Label = CType(Page.Master.FindControl("lblSiteMessage"), Label)
        'lblmsg.CssClass = "divinfo"
        'lblmsg.Text = "Please update the details to proceed..."
        'onclick = "javascript:return copyFromAbove(this);"|
        chkCopy.Attributes.Add("onclick", "copyFromAbove()")

        If Page.IsPostBack = False Then

            Try
                txtFFirstName.Enabled = False
                txtFMidName.Enabled = False
                txtFLastName.Enabled = False
                Dim CurBsUnit As String = Session("sBsuid")
                Session("sUsr_name") = Session("username")
                ViewState("datamode") = "edit"
                ViewState("STUID") = Session("STU_ID")
                h_StuID.Value = Session("STU_ID")
                Call BindEmirate_info(ddlFCOMEmirate)
                'Call Student_M_Details(ViewState("STUID"))

                ''btnCancel.Text = "Back"
                Call GetCountry_info()
                Call Student_D_Details(ViewState("STUID"), CurBsUnit)
                Call GetNational_info()
                Call DropDownList_RecordSelect()
                Call GetCompany_Name()
                ' Call GetBusinessUnits_info_staff()

                '' Call control_Disable()
                UtilityObj.beforeLoopingControls(Me.Page)
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try
        End If
    End Sub

    Private Sub BindEmirate_info(ByVal ddlEmirate As DropDownList)
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String = "SELECT EMR_CODE,EMR_DESCR  FROM EMIRATE_M order by EMR_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        ddlEmirate.DataSource = ds
        ddlEmirate.DataTextField = "EMR_DESCR"
        ddlEmirate.DataValueField = "EMR_CODE"
       
        ddlEmirate.DataBind()
        Dim lst As New ListItem
        lst.Text = "----Select----"
        lst.Value = 0
        ddlEmirate.Items.Insert(0, lst)
    End Sub
    Private Sub bind_FCITY()
        Dim sql_conn As String = ConnectionManger.GetOASISConnectionString
        Dim PARAM(1) As SqlParameter
        PARAM(0) = New SqlParameter("@CTY_ID", ddlFCOMCountry.SelectedValue)
        PARAM(1) = New SqlParameter("@INFO_TYPE", "STATE")

        Try
            Using State_reader As SqlDataReader = SqlHelper.ExecuteReader(sql_conn, CommandType.StoredProcedure, "ONLINE_ENQ.GETSTATE_AREA", PARAM)
                ddlFCity.Items.Clear()
                ddlFCity.Items.Add(New ListItem("", "-1"))
                If State_reader.HasRows = True Then
                    While State_reader.Read
                        ddlFCity.Items.Add(New ListItem(State_reader("EMR_DESCR"), State_reader("EMR_ID")))
                    End While
                End If

                ddlFCity.Items.Add(New ListItem("Other", "0"))
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "bind_FState")
        End Try
    End Sub
    Private Sub bind_FArea()
        Dim sql_conn As String = ConnectionManger.GetOASISConnectionString
        Dim PARAM(1) As SqlParameter
        PARAM(0) = New SqlParameter("@STATE_ID", ddlFCity.SelectedValue)
        PARAM(1) = New SqlParameter("@INFO_TYPE", "AREA")

        Try
            Using AREA_reader As SqlDataReader = SqlHelper.ExecuteReader(sql_conn, CommandType.StoredProcedure, "ONLINE_ENQ.GETSTATE_AREA", PARAM)
                ddlFArea.Items.Clear()
                ddlFArea.Items.Add(New ListItem("", "-1"))
                If AREA_reader.HasRows = True Then
                    While AREA_reader.Read
                        ddlFArea.Items.Add(New ListItem(AREA_reader("EMA_DESCR"), AREA_reader("EMA_ID")))
                    End While
                End If

                ddlFArea.Items.Add(New ListItem("Other", "0"))
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "bind_FArea")
        End Try
    End Sub
    Protected Sub ddlFCOMCountry_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlFCOMCountry.SelectedIndexChanged
        bind_FCITY()
        ddlFCity_SelectedIndexChanged(ddlFCity, Nothing)

    End Sub
    Protected Sub ddlFCity_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlFCity.SelectedIndexChanged
        bind_FArea()
    End Sub
    Sub Student_D_Details(ByVal STU_ID As String, ByVal CurBusUnit As String)

        Dim temp_FOFFPhone, temp_FRESPhone, temp_FFax, temp_FMobile, temp_FPRMPHONE As String, temp_EMGCONTACT As String
        Dim arInfo As String() = New String(2) {}
        Dim splitter As Char = "-"

        Using readerStudent_D_Detail As SqlDataReader = studClass.GetStudent_D(STU_ID, CurBusUnit)
            If readerStudent_D_Detail.HasRows = True Then
                While readerStudent_D_Detail.Read

                    ViewState("temp_PrimaryContact") = Convert.ToString(readerStudent_D_Detail("STU_PRIMARYCONTACT"))
                    If UCase(ViewState("temp_PrimaryContact")) = "F" Then
                        lblPrimarycontact.Text = "Father"

                        'handle the null value returned from the reader incase  convert.tostring
                        txtFFirstName.Text = Convert.ToString(readerStudent_D_Detail("STS_FFIRSTNAME"))
                        txtFMidName.Text = Convert.ToString(readerStudent_D_Detail("STS_FMIDNAME"))
                        txtFLastName.Text = Convert.ToString(readerStudent_D_Detail("STS_FLASTNAME"))
                        'txtFCOMADDR1.Text = Convert.ToString(readerStudent_D_Detail("STS_FCOMADDR1"))
                        'txtFCOMADDR2.Text = Convert.ToString(readerStudent_D_Detail("STS_FCOMADDR2"))
                        txtFCOMADDR1.Text = Convert.ToString(readerStudent_D_Detail("STS_FCOMSTREET"))
                        txtFCOMADDR2.Text = Convert.ToString(readerStudent_D_Detail("STS_FCOMAREA"))
                        txtFBldg.Text = Convert.ToString(readerStudent_D_Detail("STS_FCOMBLDG"))
                        txtFAptNo.Text = Convert.ToString(readerStudent_D_Detail("STS_FCOMAPARTNO"))
                        txtFCOMPOBOX.Text = Convert.ToString(readerStudent_D_Detail("STS_FCOMPOBOX"))
                        txtFCOMCity.Text = Convert.ToString(readerStudent_D_Detail("STS_FCOMCITY"))
                        'txtFCOMCity.Text = Convert.ToString(readerStudent_D_Detail("STS_FCOMSTATE"))
                        txtFPRMAddr1.Text = Convert.ToString(readerStudent_D_Detail("STS_FPRMADDR1"))
                        txtFPRMAddr2.Text = Convert.ToString(readerStudent_D_Detail("STS_FPRMADDR2"))
                        txtFPRMPOBOX.Text = Convert.ToString(readerStudent_D_Detail("STS_FPRMPOBOX"))
                        'temp_Nationality = Convert.ToString(readerStudent_D_Detail("SNATIONALITY"))
                        txtFPRM_City.Text = Convert.ToString(readerStudent_D_Detail("STS_FPRMCITY"))
                        txtFOcc.Text = Convert.ToString(readerStudent_D_Detail("STS_FOCC"))

                        ViewState("temp_PrefContact") = Convert.ToString(readerStudent_D_Detail("STU_PREFCONTACT"))
                        ViewState("temp_PrimaryContact") = Convert.ToString(readerStudent_D_Detail("STU_PRIMARYCONTACT"))

                        temp_EMGCONTACT = Convert.ToString(readerStudent_D_Detail("STU_EMGCONTACT"))
                        arInfo = temp_EMGCONTACT.Split(splitter)
                        If arInfo.Length > 2 Then
                            txtEmgContact_Country.Text = arInfo(0)
                            txtEmgContact_Area.Text = arInfo(1)
                            txtEmgContact_No.Text = arInfo(2)
                        ElseIf arInfo.Length = 2 Then
                            txtEmgContact_Area.Text = arInfo(0)
                            txtEmgContact_No.Text = arInfo(1)

                        Else
                            txtEmgContact_No.Text = temp_EMGCONTACT
                        End If


                        If Not ddlFCOMEmirate.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_FEMIR"))) Is Nothing Then
                            ddlFCOMEmirate.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_FEMIR"))).Selected = True
                        Else
                            ddlFCOMEmirate.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("BSU_CITY"))).Selected = True
                        End If
                        txtFEmail.Text = Convert.ToString(readerStudent_D_Detail("STS_FEMAIL"))
                        txtFComp_Add.Text = Convert.ToString(readerStudent_D_Detail("STS_FCOMPANY_ADDR"))
                        ''Viewstate settings
                        ViewState("temp_FNation1") = Convert.ToString(readerStudent_D_Detail("STS_FNATIONALITY"))
                        ViewState("temp_FNation2") = Convert.ToString(readerStudent_D_Detail("STS_FNATIONALITY2"))
                        ViewState("temp_FComCountry") = Convert.ToString(readerStudent_D_Detail("STS_FCOMCOUNTRY"))
                        ViewState("temp_FPRM_Country") = Convert.ToString(readerStudent_D_Detail("STS_FPRMCOUNTRY"))
                        ViewState("temp_F_BSU_ID") = Convert.ToString(readerStudent_D_Detail("STS_F_BSU_ID"))

                        ViewState("temp_FEMIR") = Convert.ToString(readerStudent_D_Detail("STS_FEMIR"))
                        ViewState("temp_MEMIR") = Convert.ToString(readerStudent_D_Detail("STS_MEMIR"))
                        ViewState("temp_GEMIR") = Convert.ToString(readerStudent_D_Detail("STS_GEMIR"))
                        ViewState("temp_STS_F_COMP_ID") = Convert.ToString(readerStudent_D_Detail("STS_F_COMP_ID"))
                        txtFComp_Name.Text = Convert.ToString(readerStudent_D_Detail("STS_FCOMPANY"))

                        temp_FOFFPhone = Convert.ToString(readerStudent_D_Detail("STS_FOFFPHONE"))
                        arInfo = temp_FOFFPhone.Split(splitter)
                        If arInfo.Length > 2 Then
                            txtFOffPhone_Country.Text = arInfo(0)
                            txtFOffPhone_Area.Text = arInfo(1)
                            txtFOffPhone_No.Text = arInfo(2)
                        ElseIf arInfo.Length = 2 Then
                            txtFOffPhone_Area.Text = arInfo(0)
                            txtFOffPhone_No.Text = arInfo(1)
                        Else
                            txtFOffPhone_No.Text = temp_FOFFPhone
                        End If

                        temp_FRESPhone = Convert.ToString(readerStudent_D_Detail("STS_FRESPHONE"))
                        arInfo = temp_FRESPhone.Split(splitter)
                        If arInfo.Length > 2 Then
                            txtFResPhone_Country.Text = arInfo(0)
                            txtFResPhone_Area.Text = arInfo(1)
                            txtFResPhone_No.Text = arInfo(2)
                        ElseIf arInfo.Length = 2 Then
                            txtFResPhone_Area.Text = arInfo(0)
                            txtFResPhone_No.Text = arInfo(1)
                        Else
                            txtFResPhone_No.Text = temp_FRESPhone
                        End If
                        temp_FFax = Convert.ToString(readerStudent_D_Detail("STS_FFAX"))
                        arInfo = temp_FFax.Split(splitter)
                        If arInfo.Length > 2 Then
                            txtFFax_Country.Text = arInfo(0)
                            txtFFax_Area.Text = arInfo(1)
                            txtFFax_No.Text = arInfo(2)
                        ElseIf arInfo.Length = 2 Then
                            txtFFax_Area.Text = arInfo(0)
                            txtFFax_No.Text = arInfo(1)
                        Else
                            txtFFax_No.Text = temp_FFax
                        End If
                        temp_FMobile = Convert.ToString(readerStudent_D_Detail("STS_FMOBILE"))
                        arInfo = temp_FMobile.Split(splitter)
                        If arInfo.Length > 2 Then
                            txtFMobile_Country.Text = arInfo(0)
                            txtFMobile_Area.Text = arInfo(1)
                            txtFMobile_No.Text = arInfo(2)
                        ElseIf arInfo.Length = 2 Then
                            txtFMobile_Area.Text = arInfo(0)
                            txtFMobile_No.Text = arInfo(1)
                        Else
                            txtFMobile_No.Text = temp_FMobile
                        End If

                        temp_FPRMPHONE = Convert.ToString(readerStudent_D_Detail("STS_FPRMPHONE"))
                        arInfo = temp_FPRMPHONE.Split(splitter)
                        If arInfo.Length > 2 Then
                            txtFPPRM_Country.Text = arInfo(0)
                            txtFPPRM_Area.Text = arInfo(1)
                            txtFPPRM_No.Text = arInfo(2)
                        ElseIf arInfo.Length = 2 Then
                            txtFPPRM_Area.Text = arInfo(0)
                            txtFPPRM_Area.Text = arInfo(1)
                        Else
                            txtFPPRM_No.Text = temp_FPRMPHONE
                        End If

                        txtFCOMADDR2.Text = Convert.ToString(readerStudent_D_Detail("STS_FCOMAREA"))
                        txtFCOMCity.Text = Convert.ToString(readerStudent_D_Detail("STS_FCOMCITY"))
                        If Not ddlFCOMCountry.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_FCOMCOUNTRY"))) Is Nothing Then
                            ddlFCOMCountry.ClearSelection()
                            ddlFCOMCountry.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_FCOMCOUNTRY"))).Selected = True
                            ddlFCOMCountry_SelectedIndexChanged(ddlFCOMCountry, Nothing)
                        End If


                        If Not ddlFCity.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_FCOMSTATE_ID"))) Is Nothing Then
                            ddlFCity.ClearSelection()
                            ddlFCity.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_FCOMSTATE_ID"))).Selected = True
                            ddlFCity_SelectedIndexChanged(ddlFCity, Nothing)

                        End If



                        If Not ddlFArea.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_FCOMAREA_ID"))) Is Nothing Then
                            ddlFArea.ClearSelection()
                            ddlFArea.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_FCOMAREA_ID"))).Selected = True
                        End If


                    ElseIf UCase(ViewState("temp_PrimaryContact")) = "M" Then
                        lblPrimarycontact.Text = "Mother"

                        'handle the null value returned from the reader incase  convert.tostring
                        txtFFirstName.Text = Convert.ToString(readerStudent_D_Detail("STS_MFIRSTNAME"))
                        txtFMidName.Text = Convert.ToString(readerStudent_D_Detail("STS_MMIDNAME"))
                        txtFLastName.Text = Convert.ToString(readerStudent_D_Detail("STS_MLASTNAME"))
                        'txtFCOMADDR1.Text = Convert.ToString(readerStudent_D_Detail("STS_FCOMADDR1"))
                        'txtFCOMADDR2.Text = Convert.ToString(readerStudent_D_Detail("STS_FCOMADDR2"))
                        txtFCOMADDR1.Text = Convert.ToString(readerStudent_D_Detail("STS_MCOMSTREET"))
                        txtFCOMADDR2.Text = Convert.ToString(readerStudent_D_Detail("STS_MCOMAREA"))
                        txtFBldg.Text = Convert.ToString(readerStudent_D_Detail("STS_MCOMBLDG"))
                        txtFAptNo.Text = Convert.ToString(readerStudent_D_Detail("STS_MCOMAPARTNO"))
                        txtFCOMPOBOX.Text = Convert.ToString(readerStudent_D_Detail("STS_MCOMPOBOX"))
                        txtFCOMCity.Text = Convert.ToString(readerStudent_D_Detail("STS_MCOMCITY"))
                        'txtFCOMCity.Text = Convert.ToString(readerStudent_D_Detail("STS_FCOMSTATE"))
                        txtFPRMAddr1.Text = Convert.ToString(readerStudent_D_Detail("STS_MPRMADDR1"))
                        txtFPRMAddr2.Text = Convert.ToString(readerStudent_D_Detail("STS_MPRMADDR2"))
                        txtFPRMPOBOX.Text = Convert.ToString(readerStudent_D_Detail("STS_MPRMPOBOX"))
                        'temp_Nationality = Convert.ToString(readerStudent_D_Detail("SNATIONALITY"))
                        txtFPRM_City.Text = Convert.ToString(readerStudent_D_Detail("STS_MPRMCITY"))
                        txtFOcc.Text = Convert.ToString(readerStudent_D_Detail("STS_MOCC"))

                        ViewState("temp_PrefContact") = Convert.ToString(readerStudent_D_Detail("STU_PREFCONTACT"))
                        ViewState("temp_PrimaryContact") = Convert.ToString(readerStudent_D_Detail("STU_PRIMARYCONTACT"))


                        If Not ddlFCOMEmirate.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_MEMIR"))) Is Nothing Then
                            ddlFCOMEmirate.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_MEMIR"))).Selected = True
                        Else
                            ddlFCOMEmirate.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("BSU_CITY"))).Selected = True
                        End If
                        txtFEmail.Text = Convert.ToString(readerStudent_D_Detail("STS_MEMAIL"))
                        txtFComp_Add.Text = Convert.ToString(readerStudent_D_Detail("STS_MCOMPANY_ADDR"))
                        ''Viewstate settings
                        ViewState("temp_FNation1") = Convert.ToString(readerStudent_D_Detail("STS_MNATIONALITY"))
                        ViewState("temp_FNation2") = Convert.ToString(readerStudent_D_Detail("STS_MNATIONALITY2"))
                        ViewState("temp_FComCountry") = Convert.ToString(readerStudent_D_Detail("STS_MCOMCOUNTRY"))
                        ViewState("temp_FPRM_Country") = Convert.ToString(readerStudent_D_Detail("STS_MPRMCOUNTRY"))
                        ViewState("temp_F_BSU_ID") = Convert.ToString(readerStudent_D_Detail("STS_M_BSU_ID"))
                        ViewState("temp_MEMIR") = Convert.ToString(readerStudent_D_Detail("STS_MEMIR"))
                        ViewState("temp_STS_F_COMP_ID") = Convert.ToString(readerStudent_D_Detail("STS_M_COMP_ID"))
                        txtFComp_Name.Text = Convert.ToString(readerStudent_D_Detail("STS_MCOMPANY"))

                        temp_FOFFPhone = Convert.ToString(readerStudent_D_Detail("STS_MOFFPHONE"))
                        arInfo = temp_FOFFPhone.Split(splitter)
                        If arInfo.Length > 2 Then
                            txtFOffPhone_Country.Text = arInfo(0)
                            txtFOffPhone_Area.Text = arInfo(1)
                            txtFOffPhone_No.Text = arInfo(2)
                        ElseIf arInfo.Length = 2 Then
                            txtFOffPhone_Area.Text = arInfo(0)
                            txtFOffPhone_No.Text = arInfo(1)
                        Else
                            txtFOffPhone_No.Text = temp_FOFFPhone
                        End If

                        temp_FRESPhone = Convert.ToString(readerStudent_D_Detail("STS_MRESPHONE"))
                        arInfo = temp_FRESPhone.Split(splitter)
                        If arInfo.Length > 2 Then
                            txtFResPhone_Country.Text = arInfo(0)
                            txtFResPhone_Area.Text = arInfo(1)
                            txtFResPhone_No.Text = arInfo(2)
                        ElseIf arInfo.Length = 2 Then
                            txtFResPhone_Area.Text = arInfo(0)
                            txtFResPhone_No.Text = arInfo(1)
                        Else
                            txtFResPhone_No.Text = temp_FRESPhone
                        End If
                        temp_FFax = Convert.ToString(readerStudent_D_Detail("STS_MFAX"))
                        arInfo = temp_FFax.Split(splitter)
                        If arInfo.Length > 2 Then
                            txtFFax_Country.Text = arInfo(0)
                            txtFFax_Area.Text = arInfo(1)
                            txtFFax_No.Text = arInfo(2)
                        ElseIf arInfo.Length = 2 Then
                            txtFFax_Area.Text = arInfo(0)
                            txtFFax_No.Text = arInfo(1)
                        Else
                            txtFFax_No.Text = temp_FFax
                        End If
                        temp_FMobile = Convert.ToString(readerStudent_D_Detail("STS_MMOBILE"))
                        arInfo = temp_FMobile.Split(splitter)
                        If arInfo.Length > 2 Then
                            txtFMobile_Country.Text = arInfo(0)
                            txtFMobile_Area.Text = arInfo(1)
                            txtFMobile_No.Text = arInfo(2)
                        ElseIf arInfo.Length = 2 Then
                            txtFMobile_Area.Text = arInfo(0)
                            txtFMobile_No.Text = arInfo(1)
                        Else
                            txtFMobile_No.Text = temp_FMobile
                        End If

                        temp_FPRMPHONE = Convert.ToString(readerStudent_D_Detail("STS_MPRMPHONE"))
                        arInfo = temp_FPRMPHONE.Split(splitter)
                        If arInfo.Length > 2 Then
                            txtFPPRM_Country.Text = arInfo(0)
                            txtFPPRM_Area.Text = arInfo(1)
                            txtFPPRM_No.Text = arInfo(2)
                        ElseIf arInfo.Length = 2 Then
                            txtFPPRM_Area.Text = arInfo(0)
                            txtFPPRM_Area.Text = arInfo(1)
                        Else
                            txtFPPRM_No.Text = temp_FPRMPHONE
                        End If

                        txtFCOMADDR2.Text = Convert.ToString(readerStudent_D_Detail("STS_MCOMAREA"))
                        txtFCOMCity.Text = Convert.ToString(readerStudent_D_Detail("STS_MCOMCITY"))
                        If Not ddlFCOMCountry.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_MCOMCOUNTRY"))) Is Nothing Then
                            ddlFCOMCountry.ClearSelection()
                            ddlFCOMCountry.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_MCOMCOUNTRY"))).Selected = True
                            ddlFCOMCountry_SelectedIndexChanged(ddlFCOMCountry, Nothing)
                        End If


                        If Not ddlFCity.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_MCOMSTATE_ID"))) Is Nothing Then
                            ddlFCity.ClearSelection()
                            ddlFCity.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_MCOMSTATE_ID"))).Selected = True
                            ddlFCity_SelectedIndexChanged(ddlFCity, Nothing)

                        End If



                        If Not ddlFArea.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_MCOMAREA_ID"))) Is Nothing Then
                            ddlFArea.ClearSelection()
                            ddlFArea.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_MCOMAREA_ID"))).Selected = True
                        End If


                    Else
                        lblPrimarycontact.Text = "Guardian"


                        'handle the null value returned from the reader incase  convert.tostring
                        txtFFirstName.Text = Convert.ToString(readerStudent_D_Detail("STS_GFIRSTNAME"))
                        txtFMidName.Text = Convert.ToString(readerStudent_D_Detail("STS_GMIDNAME"))
                        txtFLastName.Text = Convert.ToString(readerStudent_D_Detail("STS_GLASTNAME"))
                        'txtFCOMADDR1.Text = Convert.ToString(readerStudent_D_Detail("STS_FCOMADDR1"))
                        'txtFCOMADDR2.Text = Convert.ToString(readerStudent_D_Detail("STS_FCOMADDR2"))
                        txtFCOMADDR1.Text = Convert.ToString(readerStudent_D_Detail("STS_GCOMSTREET"))
                        txtFCOMADDR2.Text = Convert.ToString(readerStudent_D_Detail("STS_GCOMAREA"))
                        txtFBldg.Text = Convert.ToString(readerStudent_D_Detail("STS_GCOMBLDG"))
                        txtFAptNo.Text = Convert.ToString(readerStudent_D_Detail("STS_GCOMAPARTNO"))
                        txtFCOMPOBOX.Text = Convert.ToString(readerStudent_D_Detail("STS_GCOMPOBOX"))
                        txtFCOMCity.Text = Convert.ToString(readerStudent_D_Detail("STS_GCOMCITY"))
                        'txtFCOMCity.Text = Convert.ToString(readerStudent_D_Detail("STS_FCOMSTATE"))
                        txtFPRMAddr1.Text = Convert.ToString(readerStudent_D_Detail("STS_GPRMADDR1"))
                        txtFPRMAddr2.Text = Convert.ToString(readerStudent_D_Detail("STS_GPRMADDR2"))
                        txtFPRMPOBOX.Text = Convert.ToString(readerStudent_D_Detail("STS_GPRMPOBOX"))
                        'temp_Nationality = Convert.ToString(readerStudent_D_Detail("SNATIONALITY"))
                        txtFPRM_City.Text = Convert.ToString(readerStudent_D_Detail("STS_GPRMCITY"))
                        txtFOcc.Text = Convert.ToString(readerStudent_D_Detail("STS_GOCC"))

                        ViewState("temp_PrefContact") = Convert.ToString(readerStudent_D_Detail("STU_PREFCONTACT"))



                        If Not ddlFCOMEmirate.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_GEMIR"))) Is Nothing Then
                            ddlFCOMEmirate.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_GEMIR"))).Selected = True
                        Else
                            ddlFCOMEmirate.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("BSU_CITY"))).Selected = True
                        End If
                        txtFEmail.Text = Convert.ToString(readerStudent_D_Detail("STS_GEMAIL"))
                        txtFComp_Add.Text = Convert.ToString(readerStudent_D_Detail("STS_GCOMPANY_ADDR"))
                        ''Viewstate settings
                        ViewState("temp_FNation1") = Convert.ToString(readerStudent_D_Detail("STS_GNATIONALITY"))
                        ViewState("temp_FNation2") = Convert.ToString(readerStudent_D_Detail("STS_GNATIONALITY2"))
                        ViewState("temp_FComCountry") = Convert.ToString(readerStudent_D_Detail("STS_GCOMCOUNTRY"))
                        ViewState("temp_FPRM_Country") = Convert.ToString(readerStudent_D_Detail("STS_GPRMCOUNTRY"))
                        'ViewState("temp_F_BSU_ID") = Convert.ToString(readerStudent_D_Detail("STS_G_BSU_ID"))
                        ViewState("temp_GEMIR") = Convert.ToString(readerStudent_D_Detail("STS_GEMIR"))
                        ViewState("temp_STS_F_COMP_ID") = Convert.ToString(readerStudent_D_Detail("STS_G_COMP_ID"))
                        txtFComp_Name.Text = Convert.ToString(readerStudent_D_Detail("STS_GCOMPANY"))

                        temp_FOFFPhone = Convert.ToString(readerStudent_D_Detail("STS_GOFFPHONE"))
                        arInfo = temp_FOFFPhone.Split(splitter)
                        If arInfo.Length > 2 Then
                            txtFOffPhone_Country.Text = arInfo(0)
                            txtFOffPhone_Area.Text = arInfo(1)
                            txtFOffPhone_No.Text = arInfo(2)
                        ElseIf arInfo.Length = 2 Then
                            txtFOffPhone_Area.Text = arInfo(0)
                            txtFOffPhone_No.Text = arInfo(1)
                        Else
                            txtFOffPhone_No.Text = temp_FOFFPhone
                        End If

                        temp_FRESPhone = Convert.ToString(readerStudent_D_Detail("STS_GRESPHONE"))
                        arInfo = temp_FRESPhone.Split(splitter)
                        If arInfo.Length > 2 Then
                            txtFResPhone_Country.Text = arInfo(0)
                            txtFResPhone_Area.Text = arInfo(1)
                            txtFResPhone_No.Text = arInfo(2)
                        ElseIf arInfo.Length = 2 Then
                            txtFResPhone_Area.Text = arInfo(0)
                            txtFResPhone_No.Text = arInfo(1)
                        Else
                            txtFResPhone_No.Text = temp_FRESPhone
                        End If
                        temp_FFax = Convert.ToString(readerStudent_D_Detail("STS_GFAX"))
                        arInfo = temp_FFax.Split(splitter)
                        If arInfo.Length > 2 Then
                            txtFFax_Country.Text = arInfo(0)
                            txtFFax_Area.Text = arInfo(1)
                            txtFFax_No.Text = arInfo(2)
                        ElseIf arInfo.Length = 2 Then
                            txtFFax_Area.Text = arInfo(0)
                            txtFFax_No.Text = arInfo(1)
                        Else
                            txtFFax_No.Text = temp_FFax
                        End If
                        temp_FMobile = Convert.ToString(readerStudent_D_Detail("STS_GMOBILE"))
                        arInfo = temp_FMobile.Split(splitter)
                        If arInfo.Length > 2 Then
                            txtFMobile_Country.Text = arInfo(0)
                            txtFMobile_Area.Text = arInfo(1)
                            txtFMobile_No.Text = arInfo(2)
                        ElseIf arInfo.Length = 2 Then
                            txtFMobile_Area.Text = arInfo(0)
                            txtFMobile_No.Text = arInfo(1)
                        Else
                            txtFMobile_No.Text = temp_FMobile
                        End If

                        temp_FPRMPHONE = Convert.ToString(readerStudent_D_Detail("STS_GPRMPHONE"))
                        arInfo = temp_FPRMPHONE.Split(splitter)
                        If arInfo.Length > 2 Then
                            txtFPPRM_Country.Text = arInfo(0)
                            txtFPPRM_Area.Text = arInfo(1)
                            txtFPPRM_No.Text = arInfo(2)
                        ElseIf arInfo.Length = 2 Then
                            txtFPPRM_Area.Text = arInfo(0)
                            txtFPPRM_Area.Text = arInfo(1)
                        Else
                            txtFPPRM_No.Text = temp_FPRMPHONE
                        End If
                        txtFCOMADDR2.Text = Convert.ToString(readerStudent_D_Detail("STS_GCOMAREA"))
                        txtFCOMCity.Text = Convert.ToString(readerStudent_D_Detail("STS_GCOMCITY"))
                        If Not ddlFCOMCountry.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_GCOMCOUNTRY"))) Is Nothing Then
                            ddlFCOMCountry.ClearSelection()
                            ddlFCOMCountry.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_GCOMCOUNTRY"))).Selected = True
                            ddlFCOMCountry_SelectedIndexChanged(ddlFCOMCountry, Nothing)
                        End If


                        If Not ddlFCity.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_GCOMSTATE_ID"))) Is Nothing Then
                            ddlFCity.ClearSelection()
                            ddlFCity.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_GCOMSTATE_ID"))).Selected = True
                            ddlFCity_SelectedIndexChanged(ddlFCity, Nothing)

                        End If



                        If Not ddlFArea.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_GCOMAREA_ID"))) Is Nothing Then
                            ddlFArea.ClearSelection()
                            ddlFArea.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_GCOMAREA_ID"))).Selected = True
                        End If

                    End If


                End While
            Else
            End If
        End Using
    End Sub
    Sub DropDownList_RecordSelect()
        Try
            For ItemTypeCounter As Integer = 0 To ddlPrefContact.Items.Count - 1
                'keep loop until you get the School to Not Available into  the SelectedIndex
                If UCase(ddlPrefContact.Items(ItemTypeCounter).Value) = UCase(ViewState("temp_PrefContact")) Then
                    ddlPrefContact.SelectedIndex = ItemTypeCounter
                End If
            Next
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "DropDownList_RecordSelect()")
        End Try
    End Sub
    Sub GetCompany_Name()
        Try
            Using GetCompany_Name_reader As SqlDataReader = studClass.GetCompany_Name()
                ddlFCompany_Name.Items.Clear()
              
                ddlFCompany_Name.Items.Add(New ListItem("Other", "0"))
               

                If GetCompany_Name_reader.HasRows = True Then
                    While GetCompany_Name_reader.Read
                        ddlFCompany_Name.Items.Add(New ListItem(GetCompany_Name_reader("comp_Name"), GetCompany_Name_reader("comp_ID")))
                
                    End While
                End If

            End Using
            For ItemTypeCounter As Integer = 0 To ddlFCompany_Name.Items.Count - 1
                'keep loop until you get the Country to Not Available into  the SelectedIndex
                If ddlFCompany_Name.Items(ItemTypeCounter).Value = ViewState("temp_STS_F_COMP_ID") Then
                    ddlFCompany_Name.SelectedIndex = ItemTypeCounter
                End If
            Next

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, " GetCompany_Name()")
        End Try
    End Sub
    Sub GetCountry_info()
        Try
            ddlFCOMCountry.Items.Clear()
            ddlFPRM_Country.Items.Clear()

            ddlFCOMCountry.Items.Add(New ListItem("", ""))
            ddlFPRM_Country.Items.Add(New ListItem("", ""))

            Using AllCountry_reader As SqlDataReader = AccessRoleUser.GetCountry()
                Dim di_Country As ListItem

                If AllCountry_reader.HasRows = True Then
                    While AllCountry_reader.Read
                        di_Country = New ListItem(AllCountry_reader("CTY_DESCR"), AllCountry_reader("CTY_ID"))
                        ddlFCOMCountry.Items.Add(New ListItem(AllCountry_reader("CTY_DESCR"), AllCountry_reader("CTY_ID")))
                        ddlFPRM_Country.Items.Add(New ListItem(AllCountry_reader("CTY_DESCR"), AllCountry_reader("CTY_ID")))
                   
                    End While
                    For ItemTypeCounter As Integer = 0 To ddlFCOMCountry.Items.Count - 1

                        If ddlFCOMCountry.Items(ItemTypeCounter).Value = ViewState("temp_FComCountry") Then
                            ddlFCOMCountry.SelectedIndex = ItemTypeCounter
                        End If
                        If ddlFPRM_Country.Items(ItemTypeCounter).Value = ViewState("temp_FPRM_Country") Then
                            ddlFPRM_Country.SelectedIndex = ItemTypeCounter
                        End If

                    Next
                End If
            End Using

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "GetCountry_info")
        End Try
    End Sub

    'Getting national info
    Sub GetNational_info()
        Try
            Using AllNational_reader As SqlDataReader = studClass.GetNational()
                Dim di_National As ListItem
                ddlFNationality1.Items.Clear()
                ddlFNationality2.Items.Clear()
              
                ddlFNationality1.Items.Add(New ListItem("", ""))
                ddlFNationality2.Items.Add(New ListItem("", ""))
            

                If AllNational_reader.HasRows = True Then
                    While AllNational_reader.Read()
                        di_National = New ListItem(AllNational_reader("CTY_NATIONALITY"), AllNational_reader("CTY_ID"))
                        ddlFNationality1.Items.Add(New ListItem(AllNational_reader("CTY_NATIONALITY"), AllNational_reader("CTY_ID")))
                        ddlFNationality2.Items.Add(New ListItem(AllNational_reader("CTY_NATIONALITY"), AllNational_reader("CTY_ID")))
                     
                    End While

                    For ItemTypeCounter As Integer = 0 To ddlFNationality1.Items.Count - 1
                        'keep loop until you get the Country to Not Available into  the SelectedIndex
                        If ddlFNationality1.Items(ItemTypeCounter).Value = ViewState("temp_FNation1") Then
                            ddlFNationality1.SelectedIndex = ItemTypeCounter
                        End If
                        If ddlFNationality2.Items(ItemTypeCounter).Value = ViewState("temp_FNation2") Then
                            ddlFNationality2.SelectedIndex = ItemTypeCounter
                        End If
                     
                    Next
                End If
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "GetNational_info()")
        End Try
    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim lblmsg As Label = CType(Page.Master.FindControl("lblSiteMessage"), Label)
        If Page.IsValid Then
            Dim status As Integer
            Dim transaction As SqlTransaction
            Dim sPrimaryContact As String = ViewState("temp_PrimaryContact")

            If ViewState("datamode") = "edit" Then
                'If txtFCOMPOBOX.Text = "" Then
                '    Response.Write("<script language='javascript' type='text/javascript'>alert('Please enter P.O BOX')</script >")
                '    Exit Sub
                'End If

                Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                    transaction = conn.BeginTransaction("SampleTransaction")
                    Try
                        UtilityObj.InsertAuditdetails(transaction, "edit", "STUDENT_M", "STU_ID", "STU_ID", "STU_ID=" + h_StuID.Value.ToString)
                        UtilityObj.InsertAuditdetails(transaction, "edit", "STUDENT_D", "STS_STU_ID", "STS_STU_ID", "STS_STU_ID=" + h_StuID.Value.ToString)
                        '       UtilityObj.InsertAuditdetails(transaction, "EDIT", "STUDENT_M", "STU_ID", "STU_ID", "STU_ID=" + hfSTU_ID.Value.ToString)
                        status = callTrans_Student_M(transaction)
                        If status <> 0 Then
                            Throw New ArgumentException(UtilityObj.getErrorMessage(status))
                        End If
                        status = callTrans_Student_D(transaction)
                        If status <> 0 Then
                            Throw New ArgumentException(UtilityObj.getErrorMessage(status))
                        End If
                        status = callUSER_M(transaction)
                        If status <> 0 Then
                            Throw New ArgumentException(UtilityObj.getErrorMessage(status))
                        End If
                        transaction.Commit()
                        Session("bUpdateContactDetails") = "True"
                        Response.Redirect("~\login.aspx")

                        lblmsg.CssClass = "divvalid"
                        lblmsg.Text = "Record updated successfully.You can now access your child's detail by clicking on the respective tabs."
                        'ModalPopupExtender1.Show()
                        'ViewState("datamode") = "none"
                    Catch myex As ArgumentException
                        transaction.Rollback()
                        lblValidate_msg.Focus()
                        lblmsg.CssClass = "diverror"
                        lblmsg.Text = myex.Message
                        'Catch ex As Exception
                        '    transaction.Rollback()
                        '    lblValidate_msg.Focus()
                        '    lblmsg.CssClass = "diverror"
                        '    lblmsg.Text = "Record could not be Updated"
                    End Try
                End Using
                ' Response.Redirect("Home.aspx")
            End If
        End If

        
    End Sub
    Function callUSER_M(ByVal trans As SqlTransaction) As Integer
        Try
            Dim status As Integer
            Dim OLU_ID As String = Session("OLU_ID")
            Dim param(4) As SqlClient.SqlParameter

            param(0) = New SqlClient.SqlParameter("@OLU_ID", OLU_ID)
            param(1) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            param(1).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "ONLINE.UpdateUSER_M", param)
            status = param(1).Value
            Return status
        Catch ex As Exception
            Return 1000
        End Try
    End Function
    Function callTrans_Student_M(ByVal trans As SqlTransaction) As Integer
        Try
            Dim status As Integer
            Dim STU_ID As String = ViewState("STUID")
            Dim STU_PREFCONTACT As String = ddlPrefContact.SelectedItem.Value
            Dim STU_EMGCONTACT As String = txtEmgContact_Country.Text & "-" & txtEmgContact_Area.Text & "-" & txtEmgContact_No.Text
            status = studClass.UpdateSTUDENT_M(STU_ID, STU_PREFCONTACT, STU_EMGCONTACT, trans)
            Return status
        Catch ex As Exception
            Return 1000
        End Try
    End Function

    Function callTrans_Student_D(ByVal trans As SqlTransaction) As Integer
        Try
            Dim status As Integer
            Dim STS_STU_ID As String = String.Empty

            STS_STU_ID = h_StuID.Value
            Dim STS_FFIRSTNAME As String = txtFFirstName.Text
            Dim STS_FMIDNAME As String = txtFMidName.Text
            Dim STS_FLASTNAME As String = txtFLastName.Text
            Dim STS_FNATIONALITY As String = ddlFNationality1.SelectedItem.Value
            Dim STS_FNATIONALITY2 As String = ddlFNationality2.SelectedItem.Value
            Dim STS_FCOMADDR1 As String = txtFCOMADDR1.Text
            Dim STS_FCOMADDR2 As String = txtFCOMADDR2.Text
            Dim STS_FCOMPOBOX As String = txtFCOMPOBOX.Text
            Dim STS_FCOMCITY As String = txtFCOMCity.Text
            Dim STS_FCOMSTATE As String = txtFCOMCity.Text
            Dim STS_FCOMCOUNTRY As String = ddlFCOMCountry.SelectedItem.Value
            Dim STS_FOFFPHONE As String
            STS_FOFFPHONE = txtFOffPhone_Country.Text & "-" & txtFOffPhone_Area.Text & "-" & txtFOffPhone_No.Text
            Dim STS_FRESPHONE As String
            STS_FRESPHONE = txtFResPhone_Country.Text & "-" & txtFResPhone_Area.Text & "-" & txtFResPhone_No.Text
            Dim STS_FFAX As String
            STS_FFAX = txtFFax_Country.Text & "-" & txtFFax_Area.Text & "-" & txtFFax_No.Text
            Dim STS_FMOBILE As String
            STS_FMOBILE = txtFMobile_Country.Text & "-" & txtFMobile_Area.Text & "-" & txtFMobile_No.Text
            Dim STS_FPRMADDR1 As String = txtFPRMAddr1.Text
            Dim STS_FPRMADDR2 As String = txtFPRMAddr2.Text
            Dim STS_FPRMPOBOX As String = txtFPRMPOBOX.Text
            Dim STS_FPRMCITY As String = txtFPRM_City.Text
            Dim STS_FPRMCOUNTRY As String = ddlFPRM_Country.SelectedItem.Value
            Dim STS_FPRMPHONE As String
            STS_FPRMPHONE = txtFPPRM_Country.Text & "-" & txtFPPRM_Area.Text & "-" & txtFPPRM_No.Text
            Dim STS_FOCC As String = txtFOcc.Text
            ' Dim STS_bFGEMSEMP As Boolean = chkbFGEMSEMP.Checked
            Dim STS_FCOMPANY_ADDR As String = txtFComp_Add.Text
            Dim STS_FEMAIL As String = txtFEmail.Text
            Dim STS_FCOMPANY As String
            Dim STS_F_COMP_ID As String
            If ddlFCompany_Name.SelectedItem.Text = "Other" Then
                STS_FCOMPANY = txtFComp_Name.Text
                STS_F_COMP_ID = "0"
            Else
                STS_FCOMPANY = ""
                STS_F_COMP_ID = ddlFCompany_Name.SelectedItem.Value
            End If
            Dim STS_FCOMSTREET As String = txtFCOMADDR1.Text
            Dim STS_FCOMAREA As String = txtFCOMADDR2.Text
            Dim STS_FCOMBLDG As String = txtFBldg.Text
            Dim STS_FCOMAPARTNO As String = txtFAptNo.Text
            Dim STS_FEMIR As String = ddlFCOMEmirate.SelectedValue

            If ddlFArea.SelectedValue = "0" Then
                STS_FCOMAREA = txtFCOMADDR2.Text
                STS_FCOMADDR2 = txtFCOMADDR2.Text
                STS_FCOMCITY = txtFCOMCity.Text
                STS_FCOMSTATE = txtFCOMCity.Text

            ElseIf ddlFArea.SelectedValue = "-1" Then
                STS_FCOMAREA = txtFCOMADDR2.Text
                STS_FCOMADDR2 = txtFCOMADDR2.Text
                STS_FCOMCITY = txtFCOMCity.Text
                STS_FCOMSTATE = txtFCOMCity.Text

            Else
                STS_FCOMAREA = ddlFArea.SelectedItem.Text
                STS_FCOMADDR2 = ddlFArea.SelectedItem.Text
                STS_FCOMCITY = ddlFCity.SelectedItem.Text
                STS_FCOMSTATE = ddlFCity.SelectedItem.Text
            End If
            Dim STS_FCOMAREA_ID As String = ddlFArea.SelectedValue ' IIf(ddlFArea.SelectedValue = 0, "-1", ddlFArea.SelectedValue)
            Dim STS_FCOMSTATE_ID As String = ddlFCity.SelectedValue ' IIf(ddlFCity.SelectedValue = 0, "-1", ddlFCity.SelectedValue)

           

            ' status = studClass.UpdateSTUDENT_D(STS_STU_ID, STS_FFIRSTNAME, STS_FMIDNAME, STS_FLASTNAME, STS_FNATIONALITY, STS_FNATIONALITY2, STS_FCOMADDR1, STS_FCOMADDR2, STS_FCOMPOBOX, STS_FCOMCITY, STS_FCOMSTATE, STS_FCOMCOUNTRY, STS_FOFFPHONE, STS_FRESPHONE, STS_FFAX, STS_FMOBILE, STS_FPRMADDR1, STS_FPRMADDR2, STS_FPRMPOBOX, STS_FPRMCITY, STS_FPRMCOUNTRY, STS_FPRMPHONE, STS_FOCC, STS_FCOMPANY, STS_FCOMPANY_ADDR, STS_FEMAIL, STS_F_COMP_ID, STS_FCOMSTREET, STS_FCOMAREA, STS_FCOMBLDG, STS_FCOMAPARTNO, STS_FEMIR, trans)

            Dim param(140) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@STS_STU_ID", STS_STU_ID)
            param(1) = New SqlClient.SqlParameter("@STS_PRIMARYCONTACT", ViewState("temp_PrimaryContact"))
            param(2) = New SqlClient.SqlParameter("@STS_FFIRSTNAME", STS_FFIRSTNAME)
            param(3) = New SqlClient.SqlParameter("@STS_FMIDNAME", STS_FMIDNAME)
            param(4) = New SqlClient.SqlParameter("@STS_FLASTNAME", STS_FLASTNAME)
            param(5) = New SqlClient.SqlParameter("@STS_FNATIONALITY", STS_FNATIONALITY)
            param(6) = New SqlClient.SqlParameter("@STS_FNATIONALITY2", STS_FNATIONALITY2)
            param(7) = New SqlClient.SqlParameter("@STS_FCOMADDR1", STS_FCOMADDR1)
            param(8) = New SqlClient.SqlParameter("@STS_FCOMADDR2", STS_FCOMADDR2)
            param(9) = New SqlClient.SqlParameter("@STS_FCOMPOBOX", STS_FCOMPOBOX)
            param(10) = New SqlClient.SqlParameter("@STS_FCOMCITY", STS_FCOMCITY)
            param(11) = New SqlClient.SqlParameter("@STS_FCOMSTATE", STS_FCOMSTATE)
            param(12) = New SqlClient.SqlParameter("@STS_FCOMCOUNTRY", STS_FCOMCOUNTRY)
            param(13) = New SqlClient.SqlParameter("@STS_FOFFPHONE", STS_FOFFPHONE)
            param(14) = New SqlClient.SqlParameter("@STS_FRESPHONE", STS_FRESPHONE)
            param(15) = New SqlClient.SqlParameter("@STS_FFAX", STS_FFAX)
            param(16) = New SqlClient.SqlParameter("@STS_FMOBILE", STS_FMOBILE)
            param(17) = New SqlClient.SqlParameter("@STS_FPRMADDR1", STS_FPRMADDR1)
            param(18) = New SqlClient.SqlParameter("@STS_FPRMADDR2", STS_FPRMADDR2)
            param(19) = New SqlClient.SqlParameter("@STS_FPRMPOBOX", STS_FPRMPOBOX)
            param(20) = New SqlClient.SqlParameter("@STS_FPRMCITY", STS_FPRMCITY)
            param(21) = New SqlClient.SqlParameter("@STS_FPRMCOUNTRY", STS_FPRMCOUNTRY)
            param(22) = New SqlClient.SqlParameter("@STS_FPRMPHONE", STS_FPRMPHONE)
            param(23) = New SqlClient.SqlParameter("@STS_FOCC", STS_FOCC)
            param(24) = New SqlClient.SqlParameter("@STS_FCOMPANY", STS_FCOMPANY)
            param(25) = New SqlClient.SqlParameter("@STS_FCOMPANY_ADDR", STS_FCOMPANY_ADDR)
            param(26) = New SqlClient.SqlParameter("@STS_FEMAIL", STS_FEMAIL)
            param(27) = New SqlClient.SqlParameter("@STS_F_COMP_ID", STS_F_COMP_ID)
            param(28) = New SqlClient.SqlParameter("@STS_FCOMSTREET", STS_FCOMSTREET)
            param(29) = New SqlClient.SqlParameter("@STS_FCOMAREA", STS_FCOMAREA)
            param(30) = New SqlClient.SqlParameter("@STS_FCOMBLDG", STS_FCOMBLDG)
            param(31) = New SqlClient.SqlParameter("@STS_FCOMAPARTNO", STS_FCOMAPARTNO)
            param(32) = New SqlClient.SqlParameter("@STS_FEMIR", STS_FEMIR)
            param(108) = New SqlClient.SqlParameter("@STS_FCOMAREA_ID", STS_FCOMAREA_ID)
            param(109) = New SqlClient.SqlParameter("@STS_FCOMSTATE_ID", STS_FCOMSTATE_ID)

            param(33) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            param(33).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "online.UpdateStudent_D_ParentDetails", param)
            status = param(33).Value
            Return status
        Catch ex As Exception
            Return -1
        End Try
    End Function

    Protected Sub btnSignout_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSignout.Click
        Session("username") = Nothing
        Session("Active_tab") = "Home"
        Session("Site_Path") = ""
        Response.Redirect("~\login.aspx")
    End Sub

   
End Class
