﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Partial Class UpdateInfo_UpdateParentComm
    Inherits System.Web.UI.Page
    Dim BCur_ID As String
    Dim lstrErrMsg As String
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("username") Is Nothing Then
            Session("Active_tab") = "Home"
            Session("Site_Path") = ""
            Response.Redirect("~\login.aspx")
        ElseIf Session("bPasswdChanged") = "False" Then
            Response.Redirect("~\UpdateInfo\ForcePasswordChange.aspx", False)
        ElseIf Session("bUpdateContactDetails") = "False" Then
            Response.Redirect(Session("ForceUpdate_stud"), False)
        End If
        If Page.IsPostBack = False Then

            Try
                ViewState("datamode") = "edit"
                Call BindEmirate_info(ddlFCOMEmirate)
                Call BindEmirate_info(ddlMCOMEmirate)
                Call BindEmirate_info(ddlGCOMEmirate)
                Call GetCountry_info()
                Call GetNational_info()
                Call GetCompany_Name()
                'Call Student_M_Details(Session("ACTIVE_STU_ID"))
                Call Student_D_Details(Session("STU_ID"), Session("STU_BSU_ID"))
                ' Call GetBusinessUnits_info_staff()
                Call control_Disable()
                UtilityObj.beforeLoopingControls(Me.Page)
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try
        End If
    End Sub

    Private Sub control_Disable()
        Dim sPrimaryContact As String = ""
        sPrimaryContact = ViewState("temp_PrimaryContact")
        If UCase(sPrimaryContact) = "F" Then
            txtFFirstName.Enabled = False
            txtFMidName.Enabled = False
            txtFLastName.Enabled = False

            SpFRaddS.Visible = True
            rfvFRaddS.Enabled = True
            SpFRaddA.Visible = True
            rfvFRaddA.Enabled = True
            SpFRaddCity.Visible = True
            rfvFRaddCity.Enabled = True
            SpMRaddCity.Visible = False
            rfvMRaddCity.Enabled = False
            SpGRaddCity.Visible = False
            rfvGRaddCity.Enabled = False

            SpFRaddB.Visible = True
            rfvFRaddB.Enabled = True
            SpFRaddN.Visible = True
            rfvFRaddN.Enabled = True
            FpoBox.Visible = True
            rfvFcompobox.Enabled = True
            SpFMobNo.Visible = True
            rfvFMobNo.Enabled = True
            SpFPAddL1.Visible = True
            rfvFPAddL1.Enabled = True
            SpFPAddL2.Visible = True
            rfvFPAddL2.Enabled = True
            SpFPAddCity.Visible = True
            rfvFPAddCity.Enabled = True
            SpFPphNo.Visible = True
            rfvFPphNo.Enabled = True
            SpFOcc.Visible = True
            rfvFOcc.Enabled = True
            SpFEmail.Visible = True
            rfvFEmail.Enabled = True


            SpMRaddS.Visible = False
            rfvMRaddS.Enabled = False
            SpMRaddA.Visible = False
            rfvMRaddA.Enabled = False
            SpMRaddB.Visible = False
            rfvMRaddB.Enabled = False
            SpMRaddN.Visible = False
            rfvMRaddN.Enabled = False
            MpoBox.Visible = False
            rfvMcompobox.Enabled = False
            SpMMobNo.Visible = False
            rfvMMobNo.Enabled = False
            SpMPAddL1.Visible = False
            rfvMPAddL1.Enabled = False
            SpMPAddL2.Visible = False
            rfvMPAddL2.Enabled = False
            SpMPAddCity.Visible = False
            rfvMPAddCity.Enabled = False
            SpMPphNo.Visible = False
            rfvMPphNo.Enabled = False

            SpGRaddS.Visible = False
            rfvGRaddS.Enabled = False
            SpGRaddA.Visible = False
            rfvGRaddA.Enabled = False
            SpGRaddB.Visible = False
            rfvGRaddB.Enabled = False
            SpGRaddN.Visible = False
            rfvGRaddN.Enabled = False
            GpoBox.Visible = False
            rfvGcompobox.Enabled = False
            SpGMobNo.Visible = False
            rfvGMobNo.Enabled = False
            SpGPAddL1.Visible = False
            rfvGPAddL1.Enabled = False
            SpGPAddL2.Visible = False
            rfvGPAddL2.Enabled = False
            SpGPAddCity.Visible = False
            rfvGPAddCity.Enabled = False
            SpGPphNo.Visible = False
            rfvGPphNo.Enabled = False
            SpGOcc.Visible = False
            rfvGOcc.Enabled = False
            SpGEmail.Visible = False
            rfvGEmail.Enabled = False













        ElseIf UCase(sPrimaryContact) = "M" Then
            txtMFirstName.Enabled = False
            txtMMidName.Enabled = False
            txtMLastName.Enabled = False


            SpFRaddS.Visible = False
            rfvFRaddS.Enabled = False
            SpFRaddA.Visible = False
            rfvFRaddA.Enabled = False
            SpFRaddCity.Visible = False
            rfvFRaddCity.Enabled = False
            SpMRaddCity.Visible = True
            rfvMRaddCity.Enabled = True
            SpGRaddCity.Visible = False
            rfvGRaddCity.Enabled = False

            SpFRaddB.Visible = False
            rfvFRaddB.Enabled = False
            SpFRaddN.Visible = False
            rfvFRaddN.Enabled = False
            FpoBox.Visible = False
            rfvFcompobox.Enabled = False
            SpFMobNo.Visible = False
            rfvFMobNo.Enabled = False
            SpFPAddL1.Visible = False
            rfvFPAddL1.Enabled = False
            SpFPAddL2.Visible = False
            rfvFPAddL2.Enabled = False
            SpFPAddCity.Visible = False
            rfvFPAddCity.Enabled = False
            SpFPphNo.Visible = False
            rfvFPphNo.Enabled = False
            SpFOcc.Visible = False
            rfvFOcc.Enabled = False
            SpFEmail.Visible = False
            rfvFEmail.Enabled = False




            SpMRaddS.Visible = True
            rfvMRaddS.Enabled = True

            SpMRaddA.Visible = True
            rfvMRaddA.Enabled = True

            SpMRaddB.Visible = True
            rfvMRaddB.Enabled = True
            SpMRaddN.Visible = True
            rfvMRaddN.Enabled = True

            MpoBox.Visible = True
            rfvMcompobox.Enabled = True

            SpMMobNo.Visible = True
            rfvMMobNo.Enabled = True


            SpMPAddL1.Visible = True
            rfvMPAddL1.Enabled = True
            SpMPAddL2.Visible = True
            rfvMPAddL2.Enabled = True

            SpMPAddCity.Visible = True
            rfvMPAddCity.Enabled = True
            SpMPphNo.Visible = True
            rfvMPphNo.Enabled = True


            SpGRaddS.Visible = False
            rfvGRaddS.Enabled = False

            SpGRaddA.Visible = False
            rfvGRaddA.Enabled = False

            SpGRaddB.Visible = False
            rfvGRaddB.Enabled = False
            SpGRaddN.Visible = False
            rfvGRaddN.Enabled = False

            GpoBox.Visible = False
            rfvGcompobox.Enabled = False

            SpGMobNo.Visible = False
            rfvGMobNo.Enabled = False


            SpGPAddL1.Visible = False
            rfvGPAddL1.Enabled = False
            SpGPAddL2.Visible = False
            rfvGPAddL2.Enabled = False

            SpGPAddCity.Visible = False
            rfvGPAddCity.Enabled = False
            SpGPphNo.Visible = False
            rfvGPphNo.Enabled = False

            SpGOcc.Visible = False
            rfvGOcc.Enabled = False
            SpGEmail.Visible = False
            rfvGEmail.Enabled = False



        ElseIf UCase(sPrimaryContact) = "G" Then
            txtGFirstName.Enabled = False
            txtGMidName.Enabled = False
            txtGLastName.Enabled = False

            SpFRaddS.Visible = False
            rfvFRaddS.Enabled = False
            SpFRaddA.Visible = False
            rfvFRaddA.Enabled = False
            SpFRaddCity.Visible = False
            rfvFRaddCity.Enabled = False
            SpMRaddCity.Visible = False
            rfvMRaddCity.Enabled = False
            SpGRaddCity.Visible = True
            rfvGRaddCity.Enabled = True
            SpFRaddB.Visible = False
            rfvFRaddB.Enabled = False
            SpFRaddN.Visible = False
            rfvFRaddN.Enabled = False
            FpoBox.Visible = False
            rfvFcompobox.Enabled = False
            SpFMobNo.Visible = False
            rfvFMobNo.Enabled = False
            SpFPAddL1.Visible = False
            rfvFPAddL1.Enabled = False
            SpFPAddL2.Visible = False
            rfvFPAddL2.Enabled = False
            SpFPAddCity.Visible = False
            rfvFPAddCity.Enabled = False
            SpFPphNo.Visible = False
            rfvFPphNo.Enabled = False
            SpFOcc.Visible = False
            rfvFOcc.Enabled = False
            SpFEmail.Visible = False
            rfvFEmail.Enabled = False

            SpMRaddS.Visible = False
            rfvMRaddS.Enabled = False

            SpMRaddA.Visible = False
            rfvMRaddA.Enabled = False

            SpMRaddB.Visible = False
            rfvMRaddB.Enabled = False
            SpMRaddN.Visible = False
            rfvMRaddN.Enabled = False

            MpoBox.Visible = False
            rfvMcompobox.Enabled = False

            SpMMobNo.Visible = False
            rfvMMobNo.Enabled = False


            SpMPAddL1.Visible = False
            rfvMPAddL1.Enabled = False
            SpMPAddL2.Visible = False
            rfvMPAddL2.Enabled = False

            SpMPAddCity.Visible = False
            rfvMPAddCity.Enabled = False
            SpMPphNo.Visible = False
            rfvMPphNo.Enabled = False






            SpGRaddS.Visible = True
            rfvGRaddS.Enabled = True

            SpGRaddA.Visible = True
            rfvGRaddA.Enabled = True

            SpGRaddB.Visible = True
            rfvGRaddB.Enabled = True
            SpGRaddN.Visible = True
            rfvGRaddN.Enabled = True

            GpoBox.Visible = True
            rfvGcompobox.Enabled = True

            SpGMobNo.Visible = True
            rfvGMobNo.Enabled = True


            SpGPAddL1.Visible = True
            rfvGPAddL1.Enabled = True
            SpGPAddL2.Visible = True
            rfvGPAddL2.Enabled = True

            SpGPAddCity.Visible = True
            rfvGPAddCity.Enabled = True
            SpGPphNo.Visible = True
            rfvGPphNo.Enabled = True
            SpGOcc.Visible = True
            rfvGOcc.Enabled = True
            SpGEmail.Visible = True
            rfvGEmail.Enabled = True

        End If
    End Sub

    Private Sub BindEmirate_info(ByVal ddlEmirate As DropDownList)
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String = "SELECT EMR_CODE,EMR_DESCR  FROM EMIRATE_M order by EMR_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        ddlEmirate.DataSource = ds
        ddlEmirate.DataTextField = "EMR_DESCR"
        ddlEmirate.DataValueField = "EMR_CODE"
        ddlEmirate.DataBind()
    End Sub
    Protected Sub chkCopyF_Details_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkCopyF_Details.CheckedChanged
        If chkCopyF_Details.Checked = True Then

            If Not ddlMNationality1.Items.FindByValue(ddlFNationality1.SelectedValue) Is Nothing Then
                ddlMNationality1.ClearSelection()
                ddlMNationality1.Items.FindByValue(ddlFNationality1.SelectedValue).Selected = True
            End If

            If Not ddlMNationality2.Items.FindByValue(ddlFNationality2.SelectedValue) Is Nothing Then
                ddlMNationality2.ClearSelection()
                ddlMNationality2.Items.FindByValue(ddlFNationality2.SelectedValue).Selected = True
            End If


            If Not ddlMCOMCountry.Items.FindByValue(ddlFCOMCountry.SelectedValue) Is Nothing Then
                ddlMCOMCountry.ClearSelection()
                ddlMCOMCountry.Items.FindByValue(ddlFCOMCountry.SelectedValue).Selected = True
                ddlMCOMCountry_SelectedIndexChanged(ddlMCOMCountry, Nothing)
            End If

            If Not ddlMCity.Items.FindByValue(ddlFCity.SelectedValue) Is Nothing Then
                ddlMCity.ClearSelection()
                ddlMCity.Items.FindByValue(ddlFCity.SelectedValue).Selected = True
                ddlMCity_SelectedIndexChanged(ddlMCity, Nothing)
            End If

            If Not ddlMArea.Items.FindByValue(ddlFArea.SelectedValue) Is Nothing Then
                ddlMArea.ClearSelection()
                ddlMArea.Items.FindByValue(ddlFArea.SelectedValue).Selected = True
            End If

            txtMCOMADDR1.Text = txtMCOMADDR1.Text
            txtMCOMADDR2.Text = txtMCOMADDR2.Text
            txtMBldg.Text = txtFBldg.Text
            txtMAptNo.Text = txtFAptNo.Text
            txtMCOMPOBOX.Text = txtFCOMPOBOX.Text
            If Not ddlMCOMEmirate.Items.FindByValue(ddlFCOMEmirate.SelectedValue) Is Nothing Then
                ddlMCOMEmirate.ClearSelection()
                ddlMCOMEmirate.Items.FindByValue(ddlFCOMEmirate.SelectedValue).Selected = True
            End If

            txtMResPhone_Country.Text = txtFResPhone_Country.Text
            txtMResPhone_Country.Text = txtFResPhone_Country.Text
            txtMResPhone_Area.Text = txtFResPhone_Area.Text
            txtMResPhone_No.Text = txtFResPhone_No.Text
            txtMPRMAddr1.Text = txtFPRMAddr1.Text
            txtMPRMAddr2.Text = txtFPRMAddr2.Text
            txtMPRM_City.Text = txtFPRM_City.Text

            If Not ddlMPRM_Country.Items.FindByValue(ddlFPRM_Country.SelectedValue) Is Nothing Then
                ddlMPRM_Country.ClearSelection()
                ddlMPRM_Country.Items.FindByValue(ddlFPRM_Country.SelectedValue).Selected = True
            End If

            txtMPPermant_Country.Text = txtFPPRM_Country.Text
            txtMPPermant_Area.Text = txtFPPRM_Area.Text
            txtMPPermant_No.Text = txtFPPRM_No.Text
            txtMPRMPOBOX.Text = txtMPRMPOBOX.Text


        Else
            ddlMNationality1.ClearSelection()
            ddlMNationality1.SelectedIndex = 0


            ddlMNationality2.ClearSelection()
            ddlMNationality2.SelectedIndex = 0


            ddlMCOMCountry.ClearSelection()
            ddlMCOMCountry.SelectedIndex = 0

            ddlMCity.ClearSelection()
            ddlMCity.SelectedIndex = 0

            ddlMArea.ClearSelection()
            ddlMArea.SelectedIndex = 0

            txtMCOMADDR1.Text = ""
            txtMCOMADDR2.Text = ""
            txtMBldg.Text = ""
            txtMAptNo.Text = ""
            txtMCOMPOBOX.Text = ""
            ddlMCOMEmirate.ClearSelection()
            ddlMCOMEmirate.SelectedIndex = 0

            txtMResPhone_Country.Text = ""
            txtMResPhone_Country.Text = ""
            txtMResPhone_Area.Text = ""
            txtMResPhone_No.Text = ""
            txtMPRMAddr1.Text = ""
            txtMPRMAddr2.Text = ""
            txtMPRM_City.Text = ""


            ddlMPRM_Country.ClearSelection()
            ddlMPRM_Country.SelectedIndex = 0

            txtMPPermant_Country.Text = ""
            txtMPPermant_Area.Text = ""
            txtMPPermant_No.Text = ""
            txtMPRMPOBOX.Text = ""
        End If
    End Sub
    Private Sub bind_FCITY()
        Dim sql_conn As String = ConnectionManger.GetOASISConnectionString
        Dim PARAM(1) As SqlParameter
        PARAM(0) = New SqlParameter("@CTY_ID", ddlFCOMCountry.SelectedValue)
        PARAM(1) = New SqlParameter("@INFO_TYPE", "STATE")

        Try
            Using State_reader As SqlDataReader = SqlHelper.ExecuteReader(sql_conn, CommandType.StoredProcedure, "ONLINE_ENQ.GETSTATE_AREA", PARAM)
                ddlFCity.Items.Clear()
                ddlFCity.Items.Add(New ListItem("", "-1"))
                If State_reader.HasRows = True Then
                    While State_reader.Read
                        ddlFCity.Items.Add(New ListItem(State_reader("EMR_DESCR"), State_reader("EMR_ID")))
                    End While
                End If

                ddlFCity.Items.Add(New ListItem("Other", "0"))
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "bind_FState")
        End Try
    End Sub
    Private Sub bind_MCITY()
        Dim sql_conn As String = ConnectionManger.GetOASISConnectionString
        Dim PARAM(1) As SqlParameter
        PARAM(0) = New SqlParameter("@CTY_ID", ddlMCOMCountry.SelectedValue)
        PARAM(1) = New SqlParameter("@INFO_TYPE", "STATE")

        Try
            Using State_reader As SqlDataReader = SqlHelper.ExecuteReader(sql_conn, CommandType.StoredProcedure, "ONLINE_ENQ.GETSTATE_AREA", PARAM)
                ddlMCity.Items.Clear()
                ddlMCity.Items.Add(New ListItem("", "-1"))
                If State_reader.HasRows = True Then
                    While State_reader.Read
                        ddlMCity.Items.Add(New ListItem(State_reader("EMR_DESCR"), State_reader("EMR_ID")))
                    End While
                End If

                ddlMCity.Items.Add(New ListItem("Other", "0"))
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "bind_MState")
        End Try
    End Sub
    Private Sub bind_GCITY()
        Dim sql_conn As String = ConnectionManger.GetOASISConnectionString
        Dim PARAM(1) As SqlParameter
        PARAM(0) = New SqlParameter("@CTY_ID", ddlGCOMCountry.SelectedValue)
        PARAM(1) = New SqlParameter("@INFO_TYPE", "STATE")

        Try
            Using State_reader As SqlDataReader = SqlHelper.ExecuteReader(sql_conn, CommandType.StoredProcedure, "ONLINE_ENQ.GETSTATE_AREA", PARAM)
                ddlGCity.Items.Clear()
                ddlGCity.Items.Add(New ListItem("", "-1"))
                If State_reader.HasRows = True Then
                    While State_reader.Read
                        ddlGCity.Items.Add(New ListItem(State_reader("EMR_DESCR"), State_reader("EMR_ID")))
                    End While
                End If

                ddlGCity.Items.Add(New ListItem("Other", "0"))
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "bind_GState")
        End Try
    End Sub
    Private Sub bind_FArea()
        Dim sql_conn As String = ConnectionManger.GetOASISConnectionString
        Dim PARAM(1) As SqlParameter
        PARAM(0) = New SqlParameter("@STATE_ID", ddlFCity.SelectedValue)
        PARAM(1) = New SqlParameter("@INFO_TYPE", "AREA")

        Try
            Using AREA_reader As SqlDataReader = SqlHelper.ExecuteReader(sql_conn, CommandType.StoredProcedure, "ONLINE_ENQ.GETSTATE_AREA", PARAM)
                ddlFArea.Items.Clear()
                ddlFArea.Items.Add(New ListItem("", "-1"))
                If AREA_reader.HasRows = True Then
                    While AREA_reader.Read
                        ddlFArea.Items.Add(New ListItem(AREA_reader("EMA_DESCR"), AREA_reader("EMA_ID")))
                    End While
                End If

                ddlFArea.Items.Add(New ListItem("Other", "0"))
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "bind_FArea")
        End Try
    End Sub
    Private Sub bind_MArea()
        Dim sql_conn As String = ConnectionManger.GetOASISConnectionString
        Dim PARAM(1) As SqlParameter
        PARAM(0) = New SqlParameter("@STATE_ID", ddlMCity.SelectedValue)
        PARAM(1) = New SqlParameter("@INFO_TYPE", "AREA")

        Try
            Using AREA_reader As SqlDataReader = SqlHelper.ExecuteReader(sql_conn, CommandType.StoredProcedure, "ONLINE_ENQ.GETSTATE_AREA", PARAM)
                ddlMArea.Items.Clear()
                ddlMArea.Items.Add(New ListItem("", "-1"))
                If AREA_reader.HasRows = True Then
                    While AREA_reader.Read
                        ddlMArea.Items.Add(New ListItem(AREA_reader("EMA_DESCR"), AREA_reader("EMA_ID")))
                    End While
                End If

                ddlMArea.Items.Add(New ListItem("Other", "0"))
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "bind_FArea")
        End Try
    End Sub
    Private Sub bind_GArea()
        Dim sql_conn As String = ConnectionManger.GetOASISConnectionString
        Dim PARAM(1) As SqlParameter
        PARAM(0) = New SqlParameter("@STATE_ID", ddlGCity.SelectedValue)
        PARAM(1) = New SqlParameter("@INFO_TYPE", "AREA")

        Try
            Using AREA_reader As SqlDataReader = SqlHelper.ExecuteReader(sql_conn, CommandType.StoredProcedure, "ONLINE_ENQ.GETSTATE_AREA", PARAM)
                ddlGArea.Items.Clear()
                ddlGArea.Items.Add(New ListItem("", "-1"))
                If AREA_reader.HasRows = True Then
                    While AREA_reader.Read
                        ddlGArea.Items.Add(New ListItem(AREA_reader("EMA_DESCR"), AREA_reader("EMA_ID")))
                    End While
                End If

                ddlGArea.Items.Add(New ListItem("Other", "0"))
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "bind_FArea")
        End Try
    End Sub
    Protected Sub ddlFCOMCountry_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlFCOMCountry.SelectedIndexChanged
        bind_FCITY()
        ddlFCity_SelectedIndexChanged(ddlFCity, Nothing)

    End Sub
    Protected Sub ddlMCOMCountry_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlMCOMCountry.SelectedIndexChanged
        bind_MCITY()
        ddlMCity_SelectedIndexChanged(ddlMCity, Nothing)
    End Sub
    Protected Sub ddlGCOMCountry_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlGCOMCountry.SelectedIndexChanged
        bind_GCITY()
        ddlGCity_SelectedIndexChanged(ddlGCity, Nothing)
    End Sub
    Protected Sub ddlFCity_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlFCity.SelectedIndexChanged
        bind_FArea()
    End Sub
    Protected Sub ddlMCity_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlMCity.SelectedIndexChanged
        bind_MArea()
    End Sub
    Protected Sub ddlGCity_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlGCity.SelectedIndexChanged
        bind_GArea()
    End Sub
    Sub Student_D_Details(ByVal STU_ID As String, ByVal CurBusUnit As String)

        Dim temp_FOFFPhone, temp_FRESPhone, temp_FFax, temp_FMobile, temp_FPRMPHONE, temp_MFAX, temp_MOFFPHONE, temp_MRESPHONE, temp_MMOBILE, temp_MPRMPHONE, temp_GOFFPHONE, temp_GResPhone, temp_GFax, temp_GMOBILE, temp_GPRMPHONE As String, temp_EMGCONTACT As String
        Dim arInfo As String() = New String(2) {}
        Dim splitter As Char = "-"
        Dim sPrimaryContact As String = ""
        Dim CONN As String = ConnectionManger.GetOASISConnectionString
        Dim param(2) As SqlParameter
        param(0) = New SqlParameter("@STU_ID", STU_ID)


        Using readerStudent_D_Detail As SqlDataReader = SqlHelper.ExecuteReader(CONN, CommandType.StoredProcedure, "OPL.GETPROF_STUDENT_DETAILS", param)
            If readerStudent_D_Detail.HasRows = True Then
                While readerStudent_D_Detail.Read

                    'handle the null value returned from the reader incase  convert.tostring
                    txtFFirstName.Text = Convert.ToString(readerStudent_D_Detail("STS_FFIRSTNAME"))
                    txtFMidName.Text = Convert.ToString(readerStudent_D_Detail("STS_FMIDNAME"))
                    txtFLastName.Text = Convert.ToString(readerStudent_D_Detail("STS_FLASTNAME"))
                    'txtFCOMADDR1.Text = Convert.ToString(readerStudent_D_Detail("STS_FCOMADDR1"))
                    'txtFCOMADDR2.Text = Convert.ToString(readerStudent_D_Detail("STS_FCOMADDR2"))
                    txtFCOMADDR1.Text = Convert.ToString(readerStudent_D_Detail("STS_FCOMSTREET"))
                    txtFCOMADDR2.Text = Convert.ToString(readerStudent_D_Detail("STS_FCOMAREA"))
                    txtFBldg.Text = Convert.ToString(readerStudent_D_Detail("STS_FCOMBLDG"))
                    txtFAptNo.Text = Convert.ToString(readerStudent_D_Detail("STS_FCOMAPARTNO"))
                    txtFCOMPOBOX.Text = Convert.ToString(readerStudent_D_Detail("STS_FCOMPOBOX"))
                    txtFCOMCity.Text = Convert.ToString(readerStudent_D_Detail("STS_FCOMCITY"))
                    'txtFCOMCity.Text = Convert.ToString(readerStudent_D_Detail("STS_FCOMSTATE"))
                    txtFPRMAddr1.Text = Convert.ToString(readerStudent_D_Detail("STS_FPRMADDR1"))
                    txtFPRMAddr2.Text = Convert.ToString(readerStudent_D_Detail("STS_FPRMADDR2"))
                    txtFPRMPOBOX.Text = Convert.ToString(readerStudent_D_Detail("STS_FPRMPOBOX"))
                    'temp_Nationality = Convert.ToString(readerStudent_D_Detail("SNATIONALITY"))
                    txtFPRM_City.Text = Convert.ToString(readerStudent_D_Detail("STS_FPRMCITY"))
                    txtFOcc.Text = Convert.ToString(readerStudent_D_Detail("STS_FOCC"))

                    If Not ddlPrefContact.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STU_PREFCONTACT"))) Is Nothing Then
                        ddlPrefContact.ClearSelection()
                        ddlPrefContact.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STU_PREFCONTACT"))).Selected = True

                    End If




                    ViewState("temp_PrimaryContact") = Convert.ToString(readerStudent_D_Detail("STU_PRIMARYCONTACT"))
                    sPrimaryContact = ViewState("temp_PrimaryContact")

                    If UCase(ViewState("temp_PrimaryContact")) = "F" Then
                        lblPriContact.Text = "Father"
                    ElseIf UCase(ViewState("temp_PrimaryContact")) = "M" Then
                        lblPriContact.Text = "Mother"
                    ElseIf UCase(ViewState("temp_PrimaryContact")) = "G" Then
                        lblPriContact.Text = "Guardian"
                    End If



                    temp_EMGCONTACT = Convert.ToString(readerStudent_D_Detail("STU_EMGCONTACT"))
                    arInfo = temp_EMGCONTACT.Split(splitter)
                    If arInfo.Length > 2 Then
                        txtEmgContact_Country.Text = arInfo(0)
                        txtEmgContact_Area.Text = arInfo(1)
                        txtEmgContact_No.Text = arInfo(2)
                    ElseIf arInfo.Length = 2 Then
                        txtEmgContact_Area.Text = arInfo(0)
                        txtEmgContact_No.Text = arInfo(1)

                    Else
                        txtEmgContact_No.Text = temp_EMGCONTACT
                    End If

                    If Not ddlFCOMEmirate.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_FEMIR"))) Is Nothing Then
                        ddlFCOMEmirate.ClearSelection()
                        ddlFCOMEmirate.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_FEMIR"))).Selected = True
                    Else
                        ddlFCOMEmirate.ClearSelection()
                        ddlFCOMEmirate.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("BSU_CITY"))).Selected = True
                    End If
                    txtFEmail.Text = Convert.ToString(readerStudent_D_Detail("STS_FEMAIL"))
                    txtFComp_Add.Text = Convert.ToString(readerStudent_D_Detail("STS_FCOMPANY_ADDR"))
                    txtMFirstName.Text = Convert.ToString(readerStudent_D_Detail("STS_MFIRSTNAME"))
                    txtMMidName.Text = Convert.ToString(readerStudent_D_Detail("STS_MMIDNAME"))
                    txtMLastName.Text = Convert.ToString(readerStudent_D_Detail("STS_MLASTNAME"))
                    'txtMCOMADDR1.Text = Convert.ToString(readerStudent_D_Detail("STS_MCOMADDR1"))
                    'txtMCOMADDR2.Text = Convert.ToString(readerStudent_D_Detail("STS_MCOMADDR2"))
                    txtMCOMADDR1.Text = Convert.ToString(readerStudent_D_Detail("STS_MCOMSTREET"))
                    txtMCOMADDR2.Text = Convert.ToString(readerStudent_D_Detail("STS_MCOMAREA"))
                    txtMBldg.Text = Convert.ToString(readerStudent_D_Detail("STS_MCOMBLDG"))
                    txtMAptNo.Text = Convert.ToString(readerStudent_D_Detail("STS_MCOMAPARTNO"))
                    txtMCOMPOBOX.Text = Convert.ToString(readerStudent_D_Detail("STS_MCOMPOBOX"))
                    txtMCOMCity.Text = Convert.ToString(readerStudent_D_Detail("STS_MCOMCITY"))
                    'txtMCOMCity.Text = Convert.ToString(readerStudent_D_Detail("STS_MCOMSTATE"))
                    txtMPRMAddr1.Text = Convert.ToString(readerStudent_D_Detail("STS_MPRMADDR1"))
                    txtMPRMAddr2.Text = Convert.ToString(readerStudent_D_Detail("STS_MPRMADDR2"))
                    txtMPRMPOBOX.Text = Convert.ToString(readerStudent_D_Detail("STS_MPRMPOBOX"))
                    txtMPRM_City.Text = Convert.ToString(readerStudent_D_Detail("STS_MPRMCITY"))
                    txtMOcc.Text = Convert.ToString(readerStudent_D_Detail("STS_MOCC"))

                    If Not ddlMCOMEmirate.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_MEMIR"))) Is Nothing Then
                        ddlMCOMEmirate.ClearSelection()
                        ddlMCOMEmirate.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_MEMIR"))).Selected = True
                    Else
                        ddlMCOMEmirate.ClearSelection()
                        ddlMCOMEmirate.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("BSU_CITY"))).Selected = True
                    End If
                    txtMEmail.Text = Convert.ToString(readerStudent_D_Detail("STS_MEMAIL"))
                    txtMComp_Add.Text = Convert.ToString(readerStudent_D_Detail("STS_MCOMPANY_ADDR"))

                    txtGFirstName.Text = Convert.ToString(readerStudent_D_Detail("STS_GFIRSTNAME"))
                    txtGMidName.Text = Convert.ToString(readerStudent_D_Detail("STS_GMIDNAME"))
                    txtGLastName.Text = Convert.ToString(readerStudent_D_Detail("STS_GLASTNAME"))
                    'txtGCOMADDR1.Text = Convert.ToString(readerStudent_D_Detail("STS_GCOMADDR1"))
                    'txtGCOMADDR2.Text = Convert.ToString(readerStudent_D_Detail("STS_GCOMADDR2"))
                    txtGCOMADDR1.Text = Convert.ToString(readerStudent_D_Detail("STS_GCOMSTREET"))
                    txtGCOMADDR2.Text = Convert.ToString(readerStudent_D_Detail("STS_GCOMAREA"))
                    txtGBldg.Text = Convert.ToString(readerStudent_D_Detail("STS_GCOMBLDG"))
                    txtGAptNo.Text = Convert.ToString(readerStudent_D_Detail("STS_GCOMAPARTNO"))
                    txtGCOMPOBOX.Text = Convert.ToString(readerStudent_D_Detail("STS_GCOMPOBOX"))
                    txtGCOMCity.Text = Convert.ToString(readerStudent_D_Detail("STS_GCOMCITY"))
                    'txtGCOMCity.Text = Convert.ToString(readerStudent_D_Detail("STS_GCOMSTATE"))
                    txtGPRMAddr1.Text = Convert.ToString(readerStudent_D_Detail("STS_GPRMADDR1"))
                    txtGPRMAddr2.Text = Convert.ToString(readerStudent_D_Detail("STS_GPRMADDR2"))
                    txtGPRMPOBOX.Text = Convert.ToString(readerStudent_D_Detail("STS_GPRMPOBOX"))
                    txtGPRM_City.Text = Convert.ToString(readerStudent_D_Detail("STS_GPRMCITY"))
                    txtGOcc.Text = Convert.ToString(readerStudent_D_Detail("STS_GOCC"))

                    If Not ddlGCOMEmirate.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_GEMIR"))) Is Nothing Then
                        ddlGCOMEmirate.ClearSelection()
                        ddlGCOMEmirate.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_GEMIR"))).Selected = True
                    Else
                        ddlGCOMEmirate.ClearSelection()
                        ddlGCOMEmirate.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("BSU_CITY"))).Selected = True
                    End If
                    txtGEmail.Text = Convert.ToString(readerStudent_D_Detail("STS_GEMAIL"))
                    txtGComp_Add.Text = Convert.ToString(readerStudent_D_Detail("STS_GCOMPANY_ADDR"))

                    If Not ddlFNationality1.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_FNATIONALITY"))) Is Nothing Then
                        ddlFNationality1.ClearSelection()
                        ddlFNationality1.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_FNATIONALITY"))).Selected = True
                    End If
                    If Not ddlFNationality2.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_FNATIONALITY2"))) Is Nothing Then
                        ddlFNationality2.ClearSelection()
                        ddlFNationality2.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_FNATIONALITY2"))).Selected = True
                    End If
                    If Not ddlMNationality1.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_MNATIONALITY"))) Is Nothing Then
                        ddlMNationality1.ClearSelection()
                        ddlMNationality1.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_MNATIONALITY"))).Selected = True
                    End If
                    If Not ddlMNationality2.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_MNATIONALITY2"))) Is Nothing Then
                        ddlMNationality2.ClearSelection()
                        ddlMNationality2.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_MNATIONALITY2"))).Selected = True
                    End If

                    If Not ddlGNationality1.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_GNATIONALITY"))) Is Nothing Then
                        ddlGNationality1.ClearSelection()
                        ddlGNationality1.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_GNATIONALITY"))).Selected = True
                    End If
                    If Not ddlGNationality2.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_GNATIONALITY2"))) Is Nothing Then
                        ddlGNationality2.ClearSelection()
                        ddlGNationality2.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_GNATIONALITY2"))).Selected = True
                    End If


                    txtFCOMADDR2.Text = Convert.ToString(readerStudent_D_Detail("STS_FCOMAREA"))
                    txtFCOMCity.Text = Convert.ToString(readerStudent_D_Detail("STS_FCOMCITY"))
                    If Not ddlFCOMCountry.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_FCOMCOUNTRY"))) Is Nothing Then
                        ddlFCOMCountry.ClearSelection()
                        ddlFCOMCountry.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_FCOMCOUNTRY"))).Selected = True
                        ddlFCOMCountry_SelectedIndexChanged(ddlFCOMCountry, Nothing)
                    End If


                    If Not ddlFCity.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_FCOMSTATE_ID"))) Is Nothing Then
                        ddlFCity.ClearSelection()
                        ddlFCity.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_FCOMSTATE_ID"))).Selected = True
                        ddlFCity_SelectedIndexChanged(ddlFCity, Nothing)

                    End If



                    If Not ddlFArea.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_FCOMAREA_ID"))) Is Nothing Then
                        ddlFArea.ClearSelection()
                        ddlFArea.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_FCOMAREA_ID"))).Selected = True
                    End If



                    txtMCOMADDR2.Text = Convert.ToString(readerStudent_D_Detail("STS_MCOMAREA"))
                    txtMCOMCity.Text = Convert.ToString(readerStudent_D_Detail("STS_MCOMCITY"))
                    If Not ddlMCOMCountry.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_MCOMCOUNTRY"))) Is Nothing Then
                        ddlMCOMCountry.ClearSelection()
                        ddlMCOMCountry.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_MCOMCOUNTRY"))).Selected = True
                        ddlMCOMCountry_SelectedIndexChanged(ddlMCOMCountry, Nothing)
                    End If

                    If Not ddlMCity.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_MCOMSTATE_ID"))) Is Nothing Then
                        ddlMCity.ClearSelection()
                        ddlMCity.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_MCOMSTATE_ID"))).Selected = True
                        ddlMCity_SelectedIndexChanged(ddlMCity, Nothing)
                    End If

                    If Not ddlMArea.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_MCOMAREA_ID"))) Is Nothing Then
                        ddlMArea.ClearSelection()
                        ddlMArea.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_MCOMAREA_ID"))).Selected = True
                    End If


                    txtGCOMADDR2.Text = Convert.ToString(readerStudent_D_Detail("STS_GCOMAREA"))
                    txtGCOMCity.Text = Convert.ToString(readerStudent_D_Detail("STS_GCOMCITY"))
                    If Not ddlGCOMCountry.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_GCOMCOUNTRY"))) Is Nothing Then
                        ddlGCOMCountry.ClearSelection()
                        ddlGCOMCountry.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_GCOMCOUNTRY"))).Selected = True
                        ddlGCOMCountry_SelectedIndexChanged(ddlGCOMCountry, Nothing)
                    Else
                        ddlGCOMCountry.ClearSelection()
                        ddlGCOMCountry.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_FCOMCOUNTRY"))).Selected = True
                        ddlGCOMCountry_SelectedIndexChanged(ddlGCOMCountry, Nothing)
                    End If


                    If Not ddlGCity.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_GCOMSTATE_ID"))) Is Nothing Then
                        ddlGCity.ClearSelection()
                        ddlGCity.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_GCOMSTATE_ID"))).Selected = True
                        ddlGCity_SelectedIndexChanged(ddlGCity, Nothing)
                    End If


                    If Not ddlGArea.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_GCOMAREA_ID"))) Is Nothing Then
                        ddlGArea.ClearSelection()
                        ddlGArea.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_GCOMAREA_ID"))).Selected = True
                    End If

                    If Not ddlFPRM_Country.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_FPRMCOUNTRY"))) Is Nothing Then
                        ddlFPRM_Country.ClearSelection()
                        ddlFPRM_Country.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_FPRMCOUNTRY"))).Selected = True
                    End If

                    If Not ddlMCOMCountry.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_MCOMCOUNTRY"))) Is Nothing Then
                        ddlMCOMCountry.ClearSelection()
                        ddlMCOMCountry.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_MCOMCOUNTRY"))).Selected = True
                    End If
                    If Not ddlMPRM_Country.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_MPRMCOUNTRY"))) Is Nothing Then
                        ddlMPRM_Country.ClearSelection()
                        ddlMPRM_Country.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_MPRMCOUNTRY"))).Selected = True
                    End If

                    If Not ddlGCOMCountry.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_GCOMCOUNTRY"))) Is Nothing Then
                        ddlGCOMCountry.ClearSelection()
                        ddlGCOMCountry.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_GCOMCOUNTRY"))).Selected = True
                    End If
                    If Not ddlGPRM_Country.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_GPRMCOUNTRY"))) Is Nothing Then
                        ddlGPRM_Country.ClearSelection()
                        ddlGPRM_Country.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_GPRMCOUNTRY"))).Selected = True
                    End If

                    If Not ddlFCompany_Name.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_F_COMP_ID"))) Is Nothing Then
                        ddlFCompany_Name.ClearSelection()
                        ddlFCompany_Name.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_F_COMP_ID"))).Selected = True
                    End If
                    If Not ddlMCompany_Name.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_M_COMP_ID"))) Is Nothing Then
                        ddlMCompany_Name.ClearSelection()
                        ddlMCompany_Name.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_M_COMP_ID"))).Selected = True
                    End If

                    If Not ddlGCompany_Name.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_G_COMP_ID"))) Is Nothing Then
                        ddlGCompany_Name.ClearSelection()
                        ddlGCompany_Name.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_G_COMP_ID"))).Selected = True
                    End If


                    txtFComp_Name.Text = Convert.ToString(readerStudent_D_Detail("STS_FCOMPANY"))
                    txtMComp_Name.Text = Convert.ToString(readerStudent_D_Detail("STS_MCOMPANY"))
                    txtGComp_Name.Text = Convert.ToString(readerStudent_D_Detail("STS_GCOMPANY"))



                    ''Viewstate settings

                    'ViewState("temp_F_BSU_ID") = Convert.ToString(readerStudent_D_Detail("STS_F_BSU_ID"))
                    'ViewState("temp_M_BSU_ID") = Convert.ToString(readerStudent_D_Detail("STS_M_BSU_ID"))





                    'chkbFGEMSEMP.Checked = Convert.ToBoolean(readerStudent_D_Detail("STS_bFGEMSEMP")) '
                    'If chkbFGEMSEMP.Checked = False Then
                    '    ddlF_BSU_ID.Enabled = False
                    'End If
                    'chkbMGEMSEMP.Checked = Convert.ToBoolean(readerStudent_D_Detail("STS_bMGEMSEMP"))
                    'If chkbMGEMSEMP.Checked = False Then
                    '    ddlM_BSU_ID.Enabled = False
                    'End If
                    'phone settings
                    temp_FOFFPhone = Convert.ToString(readerStudent_D_Detail("STS_FOFFPHONE"))
                    arInfo = temp_FOFFPhone.Split(splitter)
                    If arInfo.Length > 2 Then
                        txtFOffPhone_Country.Text = arInfo(0)
                        txtFOffPhone_Area.Text = arInfo(1)
                        txtFOffPhone_No.Text = arInfo(2)
                    ElseIf arInfo.Length = 2 Then
                        txtFOffPhone_Area.Text = arInfo(0)
                        txtFOffPhone_No.Text = arInfo(1)

                    Else
                        txtFOffPhone_No.Text = temp_FOFFPhone
                    End If

                    temp_FRESPhone = Convert.ToString(readerStudent_D_Detail("STS_FRESPHONE"))
                    arInfo = temp_FRESPhone.Split(splitter)
                    If arInfo.Length > 2 Then
                        txtFResPhone_Country.Text = arInfo(0)
                        txtFResPhone_Area.Text = arInfo(1)
                        txtFResPhone_No.Text = arInfo(2)
                    ElseIf arInfo.Length = 2 Then
                        txtFResPhone_Area.Text = arInfo(0)
                        txtFResPhone_No.Text = arInfo(1)

                    Else
                        txtFResPhone_No.Text = temp_FRESPhone
                    End If

                    temp_FFax = Convert.ToString(readerStudent_D_Detail("STS_FFAX"))
                    arInfo = temp_FFax.Split(splitter)
                    If arInfo.Length > 2 Then
                        txtFFax_Country.Text = arInfo(0)
                        txtFFax_Area.Text = arInfo(1)
                        txtFFax_No.Text = arInfo(2)
                    ElseIf arInfo.Length = 2 Then
                        txtFFax_Area.Text = arInfo(0)
                        txtFFax_No.Text = arInfo(1)
                    Else
                        txtFFax_No.Text = temp_FFax
                    End If
                    temp_FMobile = Convert.ToString(readerStudent_D_Detail("STS_FMOBILE"))
                    arInfo = temp_FMobile.Split(splitter)
                    If arInfo.Length > 2 Then
                        txtFMobile_Country.Text = arInfo(0)
                        txtFMobile_Area.Text = arInfo(1)
                        txtFMobile_No.Text = arInfo(2)
                    ElseIf arInfo.Length = 2 Then
                        txtFMobile_Area.Text = arInfo(0)
                        txtFMobile_No.Text = arInfo(1)

                    Else
                        txtFMobile_No.Text = temp_FMobile
                    End If





                    temp_FPRMPHONE = Convert.ToString(readerStudent_D_Detail("STS_FPRMPHONE"))
                    arInfo = temp_FPRMPHONE.Split(splitter)
                    If arInfo.Length > 2 Then
                        txtFPPRM_Country.Text = arInfo(0)
                        txtFPPRM_Area.Text = arInfo(1)
                        txtFPPRM_No.Text = arInfo(2)
                    ElseIf arInfo.Length = 2 Then
                        txtFPPRM_Area.Text = arInfo(0)
                        txtFPPRM_Area.Text = arInfo(1)

                    Else
                        txtFPPRM_No.Text = temp_FPRMPHONE
                    End If
                    temp_MOFFPHONE = Convert.ToString(readerStudent_D_Detail("STS_MOFFPHONE"))
                    arInfo = temp_MOFFPHONE.Split(splitter)
                    If arInfo.Length > 2 Then
                        txtMOffPhone_Country.Text = arInfo(0)
                        txtMOffPhone_Area.Text = arInfo(1)
                        txtMOffPhone_No.Text = arInfo(2)
                    ElseIf arInfo.Length = 2 Then
                        txtMOffPhone_Area.Text = arInfo(0)
                        txtMOffPhone_No.Text = arInfo(1)

                    Else
                        txtMOffPhone_No.Text = temp_MOFFPHONE
                    End If
                    temp_MRESPHONE = Convert.ToString(readerStudent_D_Detail("STS_MRESPHONE"))
                    arInfo = temp_MRESPHONE.Split(splitter)
                    If arInfo.Length > 2 Then
                        txtMResPhone_Country.Text = arInfo(0)
                        txtMResPhone_Area.Text = arInfo(1)
                        txtMResPhone_No.Text = arInfo(2)
                    ElseIf arInfo.Length = 2 Then
                        txtMResPhone_Area.Text = arInfo(0)
                        txtMResPhone_No.Text = arInfo(1)

                    Else
                        txtMResPhone_No.Text = temp_MRESPHONE
                    End If
                    temp_MFAX = Convert.ToString(readerStudent_D_Detail("STS_MFAX"))
                    arInfo = temp_MFAX.Split(splitter)
                    If arInfo.Length > 2 Then
                        txtMFax_Country.Text = arInfo(0)
                        txtMFax_Area.Text = arInfo(1)
                        txtMFax_No.Text = arInfo(2)
                    ElseIf arInfo.Length = 2 Then
                        txtMFax_Area.Text = arInfo(0)
                        txtMFax_No.Text = arInfo(1)

                    Else
                        txtMFax_No.Text = temp_MFAX
                    End If

                    temp_MMOBILE = Convert.ToString(readerStudent_D_Detail("STS_MMOBILE"))
                    arInfo = temp_MMOBILE.Split(splitter)
                    If arInfo.Length > 2 Then
                        txtMMobile_Country.Text = arInfo(0)
                        txtMMobile_Area.Text = arInfo(1)
                        txtMMobile_No.Text = arInfo(2)
                    ElseIf arInfo.Length = 2 Then
                        txtMMobile_Area.Text = arInfo(0)
                        txtMMobile_No.Text = arInfo(1)

                    Else
                        txtMMobile_No.Text = temp_MMOBILE
                    End If
                    temp_MPRMPHONE = Convert.ToString(readerStudent_D_Detail("STS_MPRMPHONE"))
                    arInfo = temp_MPRMPHONE.Split(splitter)
                    If arInfo.Length > 2 Then
                        txtMPPermant_Country.Text = arInfo(0)
                        txtMPPermant_Area.Text = arInfo(1)
                        txtMPPermant_No.Text = arInfo(2)
                    ElseIf arInfo.Length = 2 Then
                        txtMPPermant_Area.Text = arInfo(0)
                        txtMPPermant_No.Text = arInfo(1)

                    Else
                        txtMPPermant_No.Text = temp_MPRMPHONE
                    End If
                    temp_GOFFPHONE = Convert.ToString(readerStudent_D_Detail("STS_GOFFPHONE"))
                    arInfo = temp_GOFFPHONE.Split(splitter)
                    If arInfo.Length > 2 Then
                        txtGOffPhone_Country.Text = arInfo(0)
                        txtGOffPhone_Area.Text = arInfo(1)
                        txtGOffPhone_No.Text = arInfo(2)
                    ElseIf arInfo.Length = 2 Then
                        txtGOffPhone_Area.Text = arInfo(0)
                        txtGOffPhone_No.Text = arInfo(1)
                    Else
                        txtGOffPhone_No.Text = temp_GOFFPHONE
                    End If
                    temp_GResPhone = Convert.ToString(readerStudent_D_Detail("STS_GRESPHONE"))
                    arInfo = temp_GResPhone.Split(splitter)
                    If arInfo.Length > 2 Then
                        txtGResPhone_Country.Text = arInfo(0)
                        txtGResPhone_Area.Text = arInfo(1)
                        txtGResPhone_No.Text = arInfo(2)
                    ElseIf arInfo.Length = 2 Then
                        txtGResPhone_Area.Text = arInfo(0)
                        txtGResPhone_No.Text = arInfo(1)
                    Else
                        txtGResPhone_No.Text = temp_GResPhone
                    End If
                    temp_GFax = Convert.ToString(readerStudent_D_Detail("STS_GFAX"))
                    arInfo = temp_GFax.Split(splitter)
                    If arInfo.Length > 2 Then
                        txtGFax_Country.Text = arInfo(0)
                        txtGFax_Area.Text = arInfo(1)
                        txtGFax_No.Text = arInfo(2)
                    ElseIf arInfo.Length = 2 Then
                        txtGFax_Area.Text = arInfo(0)
                        txtGFax_Area.Text = arInfo(1)
                    Else
                        txtGFax_No.Text = temp_GFax
                    End If
                    temp_GMOBILE = Convert.ToString(readerStudent_D_Detail("STS_GMOBILE"))
                    arInfo = temp_GMOBILE.Split(splitter)
                    If arInfo.Length > 2 Then
                        txtGMobile_Country.Text = arInfo(0)
                        txtGMobile_Area.Text = arInfo(1)
                        txtGMobile_No.Text = arInfo(2)
                    ElseIf arInfo.Length = 2 Then
                        txtGMobile_Area.Text = arInfo(0)
                        txtGMobile_No.Text = arInfo(1)
                    Else
                        txtGMobile_No.Text = temp_GMOBILE
                    End If
                    temp_GPRMPHONE = Convert.ToString(readerStudent_D_Detail("STS_GPRMPHONE"))
                    arInfo = temp_GPRMPHONE.Split(splitter)
                    If arInfo.Length > 2 Then
                        txtGPPERM_Country.Text = arInfo(0)
                        txtGPPERM_Area.Text = arInfo(1)
                        txtGPPERM_No.Text = arInfo(2)
                    ElseIf arInfo.Length = 2 Then
                        txtGPPERM_Area.Text = arInfo(0)
                        txtGPPERM_No.Text = arInfo(1)
                    Else
                        txtGPPERM_No.Text = temp_GPRMPHONE
                    End If
                    txtFEMIRATES_ID.Text = Convert.ToString(readerStudent_D_Detail("STS_FEMIRATES_ID"))
                    txtMEMIRATES_ID.Text = Convert.ToString(readerStudent_D_Detail("STS_MEMIRATES_ID"))
                    txtGEMIRATES_ID.Text = Convert.ToString(readerStudent_D_Detail("STS_GEMIRATES_ID"))


                    txtFEMIRATES_ID_EXPDATE.Text = Convert.ToString(readerStudent_D_Detail("STS_FEMIRATES_ID_EXPDATE"))
                    txtMEMIRATES_ID_EXPDATE.Text = Convert.ToString(readerStudent_D_Detail("STS_MEMIRATES_ID_EXPDATE"))
                    txtGEMIRATES_ID_EXPDATE.Text = Convert.ToString(readerStudent_D_Detail("STS_GEMIRATES_ID_EXPDATE"))

                End While
            Else
            End If
        End Using
    End Sub




    Sub GetCompany_Name()
        Try
            Using GetCompany_Name_reader As SqlDataReader = studClass.GetCompany_Name()
                ddlFCompany_Name.Items.Clear()
                ddlMCompany_Name.Items.Clear()
                ddlGCompany_Name.Items.Clear()

                ddlFCompany_Name.Items.Add(New ListItem("Other", "0"))
                ddlMCompany_Name.Items.Add(New ListItem("Other", "0"))
                ddlGCompany_Name.Items.Add(New ListItem("Other", "0"))

                If GetCompany_Name_reader.HasRows = True Then
                    While GetCompany_Name_reader.Read
                        ddlFCompany_Name.Items.Add(New ListItem(GetCompany_Name_reader("comp_Name"), GetCompany_Name_reader("comp_ID")))
                        ddlMCompany_Name.Items.Add(New ListItem(GetCompany_Name_reader("comp_Name"), GetCompany_Name_reader("comp_ID")))
                        ddlGCompany_Name.Items.Add(New ListItem(GetCompany_Name_reader("comp_Name"), GetCompany_Name_reader("comp_ID")))
                    End While
                End If

            End Using


        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, " GetCompany_Name()")
        End Try
    End Sub
    Sub GetCountry_info()
        Try
            ddlFCOMCountry.Items.Clear()
            ddlFPRM_Country.Items.Clear()
            ddlMCOMCountry.Items.Clear()
            ddlGCOMCountry.Items.Clear()
            ddlGPRM_Country.Items.Clear()

            ddlFCOMCountry.Items.Add(New ListItem("", ""))
            ddlFPRM_Country.Items.Add(New ListItem("", ""))
            ddlMCOMCountry.Items.Add(New ListItem("", ""))
            ddlMPRM_Country.Items.Add(New ListItem("", ""))
            ddlGCOMCountry.Items.Add(New ListItem("", ""))
            ddlGPRM_Country.Items.Add(New ListItem("", ""))

            Using AllCountry_reader As SqlDataReader = AccessRoleUser.GetCountry()
                Dim di_Country As ListItem

                If AllCountry_reader.HasRows = True Then
                    While AllCountry_reader.Read
                        di_Country = New ListItem(AllCountry_reader("CTY_DESCR"), AllCountry_reader("CTY_ID"))
                        ddlFCOMCountry.Items.Add(New ListItem(AllCountry_reader("CTY_DESCR"), AllCountry_reader("CTY_ID")))
                        ddlFPRM_Country.Items.Add(New ListItem(AllCountry_reader("CTY_DESCR"), AllCountry_reader("CTY_ID")))
                        ddlMCOMCountry.Items.Add(New ListItem(AllCountry_reader("CTY_DESCR"), AllCountry_reader("CTY_ID")))
                        ddlMPRM_Country.Items.Add(New ListItem(AllCountry_reader("CTY_DESCR"), AllCountry_reader("CTY_ID")))
                        ddlGCOMCountry.Items.Add(New ListItem(AllCountry_reader("CTY_DESCR"), AllCountry_reader("CTY_ID")))
                        ddlGPRM_Country.Items.Add(New ListItem(AllCountry_reader("CTY_DESCR"), AllCountry_reader("CTY_ID")))
                    End While

                End If
            End Using

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "GetCountry_info")
        End Try
    End Sub
    'Getting national info
    Sub GetNational_info()
        Try
            Using AllNational_reader As SqlDataReader = studClass.GetNational()
                Dim di_National As ListItem
                ddlFNationality1.Items.Clear()
                ddlFNationality2.Items.Clear()
                ddlMNationality1.Items.Clear()
                ddlMNationality2.Items.Clear()
                ddlGNationality1.Items.Clear()
                ddlGNationality2.Items.Clear()
                ddlFNationality1.Items.Add(New ListItem("", ""))
                ddlFNationality2.Items.Add(New ListItem("", ""))
                ddlMNationality1.Items.Add(New ListItem("", ""))
                ddlMNationality2.Items.Add(New ListItem("", ""))
                ddlGNationality1.Items.Add(New ListItem("", ""))
                ddlGNationality2.Items.Add(New ListItem("", ""))

                If AllNational_reader.HasRows = True Then
                    While AllNational_reader.Read()
                        di_National = New ListItem(AllNational_reader("CTY_NATIONALITY"), AllNational_reader("CTY_ID"))
                        ddlFNationality1.Items.Add(New ListItem(AllNational_reader("CTY_NATIONALITY"), AllNational_reader("CTY_ID")))
                        ddlFNationality2.Items.Add(New ListItem(AllNational_reader("CTY_NATIONALITY"), AllNational_reader("CTY_ID")))
                        ddlMNationality1.Items.Add(New ListItem(AllNational_reader("CTY_NATIONALITY"), AllNational_reader("CTY_ID")))
                        ddlMNationality2.Items.Add(New ListItem(AllNational_reader("CTY_NATIONALITY"), AllNational_reader("CTY_ID")))
                        ddlGNationality1.Items.Add(New ListItem(AllNational_reader("CTY_NATIONALITY"), AllNational_reader("CTY_ID")))
                        ddlGNationality2.Items.Add(New ListItem(AllNational_reader("CTY_NATIONALITY"), AllNational_reader("CTY_ID")))
                    End While

                End If
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "GetNational_info()")
        End Try
    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim lblmsg As Label = CType(Page.Master.FindControl("lblSiteMessage"), Label)
        Dim status As Integer
        Dim transaction As SqlTransaction
        Dim sPrimaryContact As String = ViewState("temp_PrimaryContact")

        If ViewState("datamode") = "edit" Then
            If UCase(sPrimaryContact) = "F" Then
                If txtFCOMPOBOX.Text = "" Then
                    Response.Write("<script language='javascript' type='text/javascript'>alert('Please enter Father P.O BOX')</script >")
                    Exit Sub
                End If
            ElseIf UCase(sPrimaryContact) = "M" Then
                If txtMCOMPOBOX.Text = "" Then
                    Response.Write("<script language='javascript' type='text/javascript'>alert('Please enter Mother P.O BOX')</script >")
                    Exit Sub
                End If
            ElseIf UCase(sPrimaryContact) = "G" Then
                If txtGCOMPOBOX.Text = "" Then
                    Response.Write("<script language='javascript' type='text/javascript'>alert('Please enter Guardian P.O BOX')</script >")
                    Exit Sub
                End If
            End If

            If validate_date() <> "" Then
                lblValidate_msg.Text = validate_date()
                lblValidate_msg.Focus()
                Exit Sub
            End If
            Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                'transaction = conn.BeginTransaction("SampleTransaction")
                Try
                    'status = callTrans_Student_M(transaction)
                    Save_Student_M()
                    SAve_Student_D()

                    'If status <> 0 Then
                    'Throw New ArgumentException(UtilityObj.getErrorMessage(status))
                    'End If
                    'status = callTrans_Student_D(transaction)
                    'If status <> 0 Then
                    'Throw New ArgumentException(UtilityObj.getErrorMessage(status))
                    'End If
                    ' ''Dim flagAudit As Integer = UtilityObj.operOnAudiTable(ViewState("MainMnu_code"), ViewState("Eid"), "Edit", Page.User.Identity.Name.ToString, Me.Page)

                    ' ''If flagAudit <> 0 Then
                    ' ''    Throw New ArgumentException("Could not process your request")
                    ' ''End If

                    'UtilityObj.InsertAuditdetails(transaction, "edit", "STUDENT_M", "STU_ID", "STU_ID", "STU_ID=" + h_StuID.Value.ToString)
                    ' UtilityObj.InsertAuditdetails(transaction, "edit", "STUDENT_D", "STS_STU_ID", "STS_STU_ID", "STS_STU_ID=" + h_StuID.Value.ToString)
                    'transaction.Commit()

                    Response.Redirect("~\login.aspx")
                    Dim msg As String = String.Empty

                    lblmsg.CssClass = "divvalid"
                    lblmsg.Text = "Record Updated Successfully"
                    ' lblError.Text = "Record Updated Successfully"
                    'ViewState("datamode") = "none"
                Catch myex As ArgumentException
                    '  transaction.Rollback()
                    lblValidate_msg.Focus()
                    lblmsg.CssClass = "diverror"
                    lblmsg.Text = myex.Message
                    'lblError.Text = myex.Message
                    'Catch ex As Exception
                    '    transaction.Rollback()
                    '    lblValidate_msg.Focus()
                    '    lblmsg.CssClass = "diverror"
                    '    lblmsg.Text = "Record could not be Updated"

                    'lblError.Text = "Record could not be Updated"
                End Try
            End Using
        End If
    End Sub
    
    Function callTrans_Student_D(ByVal trans As SqlTransaction) As Integer
    End Function
    Function callTrans_Student_M(ByVal trans As SqlTransaction) As Integer
    End Function
    Private Function validate_date() As String

        Dim str_error As String = String.Empty
        lblValidate_msg.Text = ""

        Dim sPrimaryContact As String = ViewState("temp_PrimaryContact")

        If ViewState("datamode") = "edit" Then
            If UCase(sPrimaryContact) = "F" Then
                If txtFCOMPOBOX.Text = "" Then
                    str_error = str_error & "<b style='padding-left:10px;padding-top:-1px;margin-top:-1px;margin-bottom:4px;display:block;'>&#149;&nbsp;&nbsp;Please enter father's P.O BOX</b>"
                End If
            ElseIf UCase(sPrimaryContact) = "M" Then
                If txtMCOMPOBOX.Text = "" Then
                    str_error = str_error & "<b style='padding-left:10px;padding-top:-1px;margin-top:-1px;margin-bottom:4px;display:block;'>&#149;&nbsp;&nbsp;Please enter mother's P.O BOX</b>"

                End If
            ElseIf UCase(sPrimaryContact) = "G" Then
                If txtGCOMPOBOX.Text = "" Then
                    str_error = str_error & "<b style='padding-left:10px;padding-top:-1px;margin-top:-1px;margin-bottom:4px;display:block;'>&#149;&nbsp;&nbsp;Please enter guardian P.O BOX</b>"
                End If
            End If
        End If


        '-----------------------------------Fathers EMIRATES_ID_EXPDATE date-----------------------------
        If txtFEMIRATES_ID_EXPDATE.Text.Trim <> "" Then
            Dim strFEMIRATES_ID_EXPDATE As String = txtFEMIRATES_ID_EXPDATE.Text.Trim
            Dim str_FEMIRATES_ID_EXPDATE As String = DateFunctions.checkdate(strFEMIRATES_ID_EXPDATE)
            If str_FEMIRATES_ID_EXPDATE <> "" Then
                str_error = str_error & "<b style='padding-left:10px;padding-top:-1px;margin-top:-1px;margin-bottom:4px;display:block;'>&#149;&nbsp;&nbsp;Please verify father's Emirates ID expiry date</b>"
            Else
                txtFEMIRATES_ID_EXPDATE.Text = strFEMIRATES_ID_EXPDATE
            End If
        End If
        '-----------------------------------Passport expire date date-----------------------------
        If txtMEMIRATES_ID_EXPDATE.Text.Trim <> "" Then
            Dim strMEMIRATES_ID_EXPDATE As String = txtMEMIRATES_ID_EXPDATE.Text.Trim
            Dim str_MEMIRATES_ID_EXPDATE As String = DateFunctions.checkdate(strMEMIRATES_ID_EXPDATE)
            If str_MEMIRATES_ID_EXPDATE <> "" Then
                str_error = str_error & "<b style='padding-left:10px;padding-top:-1px;margin-top:-1px;margin-bottom:4px;display:block;'>&#149;&nbsp;&nbsp;Please verify mother's Emirates ID expiry date</b>"
            Else
                txtMEMIRATES_ID_EXPDATE.Text = strMEMIRATES_ID_EXPDATE
            End If
        End If

        '-----------------------------------Visa Issue date date-----------------------------
        If txtGEMIRATES_ID_EXPDATE.Text.Trim <> "" Then
            Dim strGEMIRATES_ID_EXPDATE As String = txtGEMIRATES_ID_EXPDATE.Text.Trim
            Dim str_GEMIRATES_ID_EXPDATE As String = DateFunctions.checkdate(strGEMIRATES_ID_EXPDATE)
            If str_GEMIRATES_ID_EXPDATE <> "" Then
                str_error = str_error & "<b style='padding-left:10px;padding-top:-1px;margin-top:-1px;margin-bottom:4px;display:block;'>&#149;&nbsp;&nbsp;Please verify guardian's Emirates ID expiry date</b>"
            Else
                txtGEMIRATES_ID_EXPDATE.Text = strGEMIRATES_ID_EXPDATE
            End If
        End If

        Return str_error

    End Function
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Call Student_D_Details(Session("STU_ID"), Session("STU_BSU_ID"))
        ' Call GetBusinessUnits_info_staff()
        Call control_Disable()
    End Sub
    Function Save_Student_M()
        Try
            Dim status As Integer
            Dim STU_ID As String = Session("STU_ID")
            '' Dim STU_BSU_ID As String = Session("sBsuid")
            'Dim STU_PRIMARYCONTACT As String
            'If rdFather.Checked = True Then
            '    STU_PRIMARYCONTACT = "F"
            'ElseIf rdMother.Checked = True Then
            '    STU_PRIMARYCONTACT = "M"
            'Else
            '    STU_PRIMARYCONTACT = "G"
            'End If
            Dim STU_PREFCONTACT As String = ddlPrefContact.SelectedItem.Value
            Dim STU_EMGCONTACT As String
            'If txtMOffPhone_Country.Text = "" Or txtMOffPhone_Area.Text = "" Or txtMOffPhone_No.Text = "" Then
            '    STS_MOFFPHONE = ""
            'Else
            STU_EMGCONTACT = txtEmgContact_Country.Text & "-" & txtEmgContact_Area.Text & "-" & txtEmgContact_No.Text

            status = UpdateSTUDENT_M(STU_ID, STU_PREFCONTACT, STU_EMGCONTACT)
            Return status
        Catch ex As Exception
            Return 1000
        End Try
    End Function



    'Dim objConn As New SqlConnection(connectionString)
    '    objConn.Open()
    'Dim stTrans As SqlTransaction = objConn.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted)
    '    Try
    'Dim RetVal As String = Mainclass.ExecuteParamQRY(objConn, stTrans, "PostJob", pParms)
    '        If RetVal = "-1" Then
    '            lblError.Text = "Unexpected Error !!!"
    '            stTrans.Rollback()
    '            Exit Sub
    '        Else
    '            stTrans.Commit()
    '            If action = "P" Then
    '                writeWorkFlowTPTH(0, "Modified", "Job Order Posted to Finance :" & trnno, "P")
    '            Else
    '                writeWorkFlowTPTH(0, "Modified", "Job Order Converted to Execution before posting to Finance :" & trnno, "S")
    '            End If
    '            Response.Redirect(ViewState("ReferrerUrl"))
    '        End If
    '    Catch ex As Exception
    '        Errorlog(ex.Message)
    '        lblError.Text = ex.Message
    '    End Try


    Function UpdateSTUDENT_M(ByVal STU_ID As String, ByVal STU_PREFCONTACT As String, ByVal STU_EMGCONTACT As String) As Integer

        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()

            Try
                Dim pParms(6) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@STU_ID", STU_ID)
                pParms(1) = New SqlClient.SqlParameter("@STU_PREFCONTACT", STU_PREFCONTACT)
                pParms(2) = New SqlClient.SqlParameter("@STU_EMGCONTACT", STU_EMGCONTACT)
                pParms(3) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                pParms(3).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "UpdateStudent_M_PrimaryContact", pParms)
                Dim ReturnFlag As Integer = pParms(3).Value
                Return ReturnFlag
            Catch ex As Exception
                Return 1000
            End Try
        End Using
    End Function

    Function SAve_Student_D()
        Try
            Dim status As Integer
            Dim STS_STU_ID As String = String.Empty
            h_StuID.Value = Session("STU_ID")
            STS_STU_ID = h_StuID.Value
            Dim STS_FFIRSTNAME As String = txtFFirstName.Text
            Dim STS_FMIDNAME As String = txtFMidName.Text
            Dim STS_FLASTNAME As String = txtFLastName.Text
            Dim STS_FNATIONALITY As String = ddlFNationality1.SelectedItem.Value
            Dim STS_FNATIONALITY2 As String = ddlFNationality2.SelectedItem.Value
            Dim STS_FCOMADDR1 As String = txtFCOMADDR1.Text
            Dim STS_FCOMADDR2 As String = txtFCOMADDR2.Text
            Dim STS_FCOMPOBOX As String = txtFCOMPOBOX.Text
            Dim STS_FCOMCITY As String = txtFCOMCity.Text
            Dim STS_FCOMSTATE As String = txtFCOMCity.Text
            Dim STS_FCOMCOUNTRY As String = ddlFCOMCountry.SelectedItem.Value

            Dim STS_FOFFPHONE As String
            STS_FOFFPHONE = txtFOffPhone_Country.Text & "-" & txtFOffPhone_Area.Text & "-" & txtFOffPhone_No.Text
            Dim STS_FRESPHONE As String
            STS_FRESPHONE = txtFResPhone_Country.Text & "-" & txtFResPhone_Area.Text & "-" & txtFResPhone_No.Text
            Dim STS_FFAX As String
            STS_FFAX = txtFFax_Country.Text & "-" & txtFFax_Area.Text & "-" & txtFFax_No.Text
            Dim STS_FMOBILE As String
            STS_FMOBILE = txtFMobile_Country.Text & "-" & txtFMobile_Area.Text & "-" & txtFMobile_No.Text
            Dim STS_FPRMADDR1 As String = txtFPRMAddr1.Text
            Dim STS_FPRMADDR2 As String = txtFPRMAddr2.Text
            Dim STS_FPRMPOBOX As String = txtFPRMPOBOX.Text
            Dim STS_FPRMCITY As String = txtFPRM_City.Text
            Dim STS_FPRMCOUNTRY As String = ddlFPRM_Country.SelectedItem.Value
            Dim STS_FPRMPHONE As String
            STS_FPRMPHONE = txtFPPRM_Country.Text & "-" & txtFPPRM_Area.Text & "-" & txtFPPRM_No.Text
            Dim STS_FOCC As String = txtFOcc.Text
            ' Dim STS_bFGEMSEMP As Boolean = chkbFGEMSEMP.Checked
            Dim STS_F_BSU_ID As String

            'If chkbFGEMSEMP.Checked Then
            '    STS_F_BSU_ID = ddlF_BSU_ID.SelectedItem.Value
            'Else
            '    STS_F_BSU_ID = ""
            'End If
            'code added by dhanya 29 apr 08
            Dim STS_FCOMPANY_ADDR As String = txtFComp_Add.Text
            Dim STS_FEMAIL As String = txtFEmail.Text
            Dim STS_MFIRSTNAME As String = txtMFirstName.Text
            Dim STS_MMIDNAME As String = txtMMidName.Text
            Dim STS_MLASTNAME As String = txtMLastName.Text
            Dim STS_MNATIONALITY As String = ddlMNationality1.SelectedItem.Value
            Dim STS_MNATIONALITY2 As String = ddlMNationality2.SelectedItem.Value
            Dim STS_MCOMADDR1 As String = txtMCOMADDR1.Text
            Dim STS_MCOMADDR2 As String = txtMCOMADDR2.Text
            Dim STS_MCOMPOBOX As String = txtMCOMPOBOX.Text
            Dim STS_MCOMCITY As String = txtMCOMCity.Text
            Dim STS_MCOMSTATE As String = txtMCOMCity.Text
            Dim STS_MCOMCOUNTRY As String = ddlMCOMCountry.SelectedItem.Value
            Dim STS_MOFFPHONE As String
            'If txtMOffPhone_Country.Text = "" Or txtMOffPhone_Area.Text = "" Or txtMOffPhone_No.Text = "" Then
            '    STS_MOFFPHONE = ""
            'Else
            STS_MOFFPHONE = txtMOffPhone_Country.Text & "-" & txtMOffPhone_Area.Text & "-" & txtMOffPhone_No.Text
            'End If
            Dim STS_MRESPHONE As String
            'If txtMResPhone_Country.Text = "" Or txtMResPhone_Area.Text = "" Or txtMResPhone_No.Text = "" Then
            '    STS_MRESPHONE = ""
            'Else
            STS_MRESPHONE = txtMResPhone_Country.Text & "-" & txtMResPhone_Area.Text & "-" & txtMResPhone_No.Text
            'End If
            Dim STS_MFAX As String
            'If txtMFax_Country.Text = "" Or txtMFax_Area.Text = "" Or txtMFax_No.Text = "" Then
            '    STS_MFAX = ""
            'Else
            STS_MFAX = txtMFax_Country.Text & "-" & txtMFax_Area.Text & "-" & txtMFax_No.Text
            '  End If
            Dim STS_MMOBILE As String
            'If txtMMobile_Country.Text = "" Or txtMMobile_Area.Text = "" Or txtMMobile_No.Text = "" Then
            '    STS_MMOBILE = ""
            'Else
            STS_MMOBILE = txtMMobile_Country.Text & "-" & txtMMobile_Area.Text & "-" & txtMMobile_No.Text
            'End If
            Dim STS_MPRMADDR1 As String = txtMPRMAddr1.Text
            Dim STS_MPRMADDR2 As String = txtMPRMAddr2.Text
            Dim STS_MPRMPOBOX As String = txtMPRMPOBOX.Text
            Dim STS_MPRMCITY As String = txtMPRM_City.Text
            Dim STS_MPRMCOUNTRY As String = ddlMPRM_Country.SelectedItem.Value
            Dim STS_MPRMPHONE As String
            'If txtMPPermant_Country.Text = "" Or txtMPPermant_Area.Text = "" Or txtMPPermant_No.Text = "" Then
            '    STS_MPRMPHONE = ""
            'Else
            STS_MPRMPHONE = txtMPPermant_Country.Text & "-" & txtMPPermant_Area.Text & "-" & txtMPPermant_No.Text
            'End If
            Dim STS_MOCC As String = txtMOcc.Text
            'Dim STS_bMGEMSEMP As Boolean = chkbMGEMSEMP.Checked
            'Dim STS_M_BSU_ID As String
            'If chkbMGEMSEMP.Checked Then
            '    STS_M_BSU_ID = ddlM_BSU_ID.SelectedItem.Value
            'Else
            '    STS_M_BSU_ID = ""
            'End If
            'code added by dhanya 29 apr 08
            Dim STS_MCOMPANY_ADDR As String = txtMComp_Add.Text
            Dim STS_MEMAIL As String = txtMEmail.Text
            Dim STS_GFIRSTNAME As String = txtGFirstName.Text
            Dim STS_GMIDNAME As String = txtGMidName.Text
            Dim STS_GLASTNAME As String = txtGLastName.Text
            Dim STS_GNATIONALITY As String = ddlGNationality1.SelectedItem.Value
            Dim STS_GNATIONALITY2 As String = ddlGNationality2.SelectedItem.Value
            Dim STS_GCOMADDR1 As String = txtGCOMADDR1.Text
            Dim STS_GCOMADDR2 As String = txtGCOMADDR2.Text
            Dim STS_GCOMPOBOX As String = txtGCOMPOBOX.Text
            Dim STS_GCOMCITY As String = txtGCOMCity.Text
            Dim STS_GCOMSTATE As String = txtGCOMCity.Text
            Dim STS_GCOMCOUNTRY As String = ddlGCOMCountry.SelectedItem.Value
            Dim STS_GOFFPHONE As String
            'If txtGOffPhone_Country.Text = "" Or txtGOffPhone_Area.Text = "" Or txtGOffPhone_No.Text = "" Then
            '    STS_GOFFPHONE = ""
            'Else
            STS_GOFFPHONE = txtGOffPhone_Country.Text & "-" & txtGOffPhone_Area.Text & "-" & txtGOffPhone_No.Text
            'End If
            Dim STS_GRESPHONE As String
            'If txtGResPhone_Country.Text = "" Or txtGResPhone_Area.Text = "" Or txtGResPhone_No.Text = "" Then
            '    STS_GRESPHONE = ""
            'Else
            STS_GRESPHONE = txtGResPhone_Country.Text & "-" & txtGResPhone_Area.Text & "-" & txtGResPhone_No.Text
            ' End If

            Dim STS_GFAX As String
            'If txtGFax_Country.Text = "" Or txtGFax_Area.Text = "" Or txtGFax_No.Text = "" Then
            '    STS_GFAX = ""
            'Else
            STS_GFAX = txtGFax_Country.Text & "-" & txtGFax_Area.Text & "-" & txtGFax_No.Text
            ' End If

            Dim STS_GMOBILE As String
            'If txtGMobile_Country.Text = "" Or txtGMobile_Area.Text = "" Or txtGMobile_No.Text = "" Then
            '    STS_GMOBILE = ""
            'Else
            STS_GMOBILE = txtGMobile_Country.Text & "-" & txtGMobile_Area.Text & "-" & txtGMobile_No.Text
            ' End If
            Dim STS_GPRMADDR1 As String = txtGPRMAddr1.Text
            Dim STS_GPRMADDR2 As String = txtGPRMAddr2.Text
            Dim STS_GPRMPOBOX As String = txtGPRMPOBOX.Text
            Dim STS_GPRMCITY As String = txtGPRM_City.Text
            Dim STS_GPRMCOUNTRY As String = ddlGPRM_Country.SelectedItem.Value
            Dim STS_GPRMPHONE As String
            'If txtGPPERM_Country.Text = "" Or txtGPPERM_Area.Text = "" Or txtGPPERM_No.Text = "" Then
            '    STS_GPRMPHONE = ""
            'Else
            STS_GPRMPHONE = txtGPPERM_Country.Text & "-" & txtGPPERM_Area.Text & "-" & txtGPPERM_No.Text
            ' End If

            'code added by dhanya 29 apr 08
            Dim STS_GCOMPANY_ADDR As String = txtGComp_Add.Text
            Dim STS_GEMAIL As String = txtGEmail.Text
            Dim STS_GOCC As String = txtGOcc.Text
            'code added by Lijo 25 May 08 field added(STS_F_COMP_ID ,STS_M_COMP_ID,STS_G_COMP_ID)
            Dim STS_FCOMPANY As String
            Dim STS_MCOMPANY As String
            Dim STS_GCOMPANY As String
            Dim STS_F_COMP_ID As String
            Dim STS_M_COMP_ID As String
            Dim STS_G_COMP_ID As String

            If ddlFCompany_Name.SelectedItem.Text = "Other" Then
                STS_FCOMPANY = txtFComp_Name.Text
                STS_F_COMP_ID = "0"
            Else
                STS_FCOMPANY = ""
                STS_F_COMP_ID = ddlFCompany_Name.SelectedItem.Value
            End If

            If ddlMCompany_Name.SelectedItem.Text = "Other" Then
                STS_MCOMPANY = txtMComp_Name.Text
                STS_M_COMP_ID = "0"
            Else
                STS_MCOMPANY = ""
                STS_M_COMP_ID = ddlMCompany_Name.SelectedItem.Value
            End If

            If ddlGCompany_Name.SelectedItem.Text = "Other" Then
                STS_GCOMPANY = txtGComp_Name.Text
                STS_G_COMP_ID = "0"
            Else
                STS_GCOMPANY = ""
                STS_G_COMP_ID = ddlGCompany_Name.SelectedItem.Value
            End If

            Dim STS_FCOMSTREET As String = txtFCOMADDR1.Text
            Dim STS_FCOMAREA As String = txtFCOMADDR2.Text
            Dim STS_FCOMBLDG As String = txtFBldg.Text
            Dim STS_FCOMAPARTNO As String = txtFAptNo.Text
            Dim STS_FEMIR As String = ddlFCOMEmirate.SelectedValue

            Dim STS_MCOMSTREET As String = txtMCOMADDR1.Text
            Dim STS_MCOMAREA As String = txtMCOMADDR2.Text
            Dim STS_MCOMBLDG As String = txtMBldg.Text
            Dim STS_MCOMAPARTNO As String = txtMAptNo.Text
            Dim STS_MEMIR As String = ddlMCOMEmirate.SelectedValue

            Dim STS_GCOMSTREET As String = txtGCOMADDR1.Text
            Dim STS_GCOMAREA As String = txtGCOMADDR2.Text
            Dim STS_GCOMBLDG As String = txtGBldg.Text
            Dim STS_GCOMAPARTNO As String = txtGAptNo.Text
            Dim STS_GEMIR As String = ddlGCOMEmirate.SelectedValue
            Dim STS_FEMIRATES_ID As String = txtFEMIRATES_ID.Text
            Dim STS_MEMIRATES_ID As String = txtMEMIRATES_ID.Text
            Dim STS_GEMIRATES_ID As String = txtGEMIRATES_ID.Text
            Dim STS_FEMIRATES_ID_EXPDATE As String = txtFEMIRATES_ID_EXPDATE.Text
            Dim STS_MEMIRATES_ID_EXPDATE As String = txtMEMIRATES_ID_EXPDATE.Text
            Dim STS_GEMIRATES_ID_EXPDATE As String = txtGEMIRATES_ID_EXPDATE.Text

            Dim STS_FCOMAREA_ID As String = ddlFArea.SelectedValue ' IIf(ddlFArea.SelectedValue = 0, "-1", ddlFArea.SelectedValue)
            Dim STS_FCOMSTATE_ID As String = ddlFCity.SelectedValue ' IIf(ddlFCity.SelectedValue = 0, "-1", ddlFCity.SelectedValue)

            Dim STS_MCOMAREA_ID As String = ddlMArea.SelectedValue ' IIf(ddlMArea.SelectedValue = 0, "-1", ddlMArea.SelectedValue)
            Dim STS_MCOMSTATE_ID As String = ddlMCity.SelectedValue ' IIf(ddlMCity.SelectedValue = 0, "-1", ddlMCity.SelectedValue)

            Dim STS_GCOMAREA_ID As String = ddlGArea.SelectedValue ' IIf(ddlGArea.SelectedValue = 0, "-1", ddlGArea.SelectedValue)
            Dim STS_GCOMSTATE_ID As String = ddlGCity.SelectedValue 'IIf(ddlGCity.SelectedValue = 0, "-1", ddlGCity.SelectedValue)

            STS_FCOMAREA = String.Empty
            STS_FCOMADDR2 = String.Empty
            STS_FCOMCITY = String.Empty
            STS_FCOMSTATE = String.Empty

            If ddlFArea.SelectedValue = "0" Then
                STS_FCOMAREA = txtFCOMADDR2.Text
                STS_FCOMADDR2 = txtFCOMADDR2.Text
                STS_FCOMCITY = txtFCOMCity.Text
                STS_FCOMSTATE = txtFCOMCity.Text

            ElseIf ddlFArea.SelectedValue = "-1" Then
                STS_FCOMAREA = txtFCOMADDR2.Text
                STS_FCOMADDR2 = txtFCOMADDR2.Text
                STS_FCOMCITY = txtFCOMCity.Text
                STS_FCOMSTATE = txtFCOMCity.Text

            Else
                STS_FCOMAREA = ddlFArea.SelectedItem.Text
                STS_FCOMADDR2 = ddlFArea.SelectedItem.Text
                STS_FCOMCITY = ddlFCity.SelectedItem.Text
                STS_FCOMSTATE = ddlFCity.SelectedItem.Text
            End If

            STS_MCOMAREA = String.Empty
            STS_MCOMADDR2 = String.Empty
            STS_MCOMCITY = String.Empty
            STS_MCOMSTATE = String.Empty

            If ddlMArea.SelectedValue = "0" Then
                STS_MCOMAREA = txtMCOMADDR2.Text
                STS_MCOMADDR2 = txtMCOMADDR2.Text
                STS_MCOMCITY = txtMCOMCity.Text
                STS_MCOMSTATE = txtMCOMCity.Text

            ElseIf ddlMArea.SelectedValue = "-1" Then
                STS_MCOMAREA = txtMCOMADDR2.Text
                STS_MCOMADDR2 = txtMCOMADDR2.Text
                STS_MCOMCITY = txtMCOMCity.Text
                STS_MCOMSTATE = txtMCOMCity.Text


            Else
                STS_MCOMAREA = ddlMArea.SelectedItem.Text
                STS_MCOMADDR2 = ddlMArea.SelectedItem.Text
                STS_MCOMCITY = ddlMCity.SelectedItem.Text
                STS_MCOMSTATE = ddlMCity.SelectedItem.Text
            End If




            STS_GCOMAREA = String.Empty
            STS_GCOMADDR2 = String.Empty
            STS_GCOMCITY = String.Empty
            STS_GCOMSTATE = String.Empty


            If ddlGArea.SelectedValue = "0" Then
                STS_GCOMAREA = txtGCOMADDR2.Text
                STS_GCOMADDR2 = txtGCOMADDR2.Text
                STS_GCOMCITY = txtGCOMCity.Text
                STS_GCOMSTATE = txtGCOMCity.Text

            ElseIf ddlGArea.SelectedValue = "-1" Then
                STS_GCOMAREA = txtGCOMADDR2.Text
                STS_GCOMADDR2 = txtGCOMADDR2.Text
                STS_GCOMCITY = txtGCOMCity.Text
                STS_GCOMSTATE = txtGCOMCity.Text



            Else
                STS_GCOMAREA = ddlGArea.SelectedItem.Text
                STS_GCOMADDR2 = ddlGArea.SelectedItem.Text
                STS_GCOMCITY = ddlGCity.SelectedItem.Text
                STS_GCOMCITY = ddlGCity.SelectedItem.Text
            End If

            STS_FCOMSTREET = txtFCOMADDR1.Text

            STS_FCOMBLDG = txtFBldg.Text
            STS_FCOMAPARTNO = txtFAptNo.Text
            STS_FEMIR = ddlFCOMEmirate.SelectedValue

            STS_MCOMSTREET = txtMCOMADDR1.Text

            STS_MCOMBLDG = txtMBldg.Text
            STS_MCOMAPARTNO = txtMAptNo.Text
            STS_MEMIR = ddlMCOMEmirate.SelectedValue

            STS_GCOMSTREET = txtGCOMADDR1.Text

            STS_GCOMBLDG = txtGBldg.Text
            STS_GCOMAPARTNO = txtGAptNo.Text
            STS_GEMIR = ddlGCOMEmirate.SelectedValue

            status = UpdateSTUDENT_D(STS_STU_ID, STS_FFIRSTNAME, STS_FMIDNAME, STS_FLASTNAME, STS_FNATIONALITY, STS_FNATIONALITY2, STS_FCOMADDR1, _
           STS_FCOMADDR2, STS_FCOMPOBOX, STS_FCOMCITY, STS_FCOMSTATE, STS_FCOMCOUNTRY, STS_FOFFPHONE, STS_FRESPHONE, STS_FFAX, _
           STS_FMOBILE, STS_FPRMADDR1, STS_FPRMADDR2, STS_FPRMPOBOX, STS_FPRMCITY, STS_FPRMCOUNTRY, STS_FPRMPHONE, STS_FOCC, _
           STS_FCOMPANY, STS_MFIRSTNAME, STS_MMIDNAME, STS_MLASTNAME, STS_MNATIONALITY, STS_MNATIONALITY2, _
           STS_MCOMADDR1, STS_MCOMADDR2, STS_MCOMPOBOX, STS_MCOMCITY, STS_MCOMSTATE, STS_MCOMCOUNTRY, STS_MOFFPHONE, STS_MRESPHONE, _
           STS_MFAX, STS_MMOBILE, STS_MPRMADDR1, STS_MPRMADDR2, STS_MPRMPOBOX, STS_MPRMCITY, STS_MPRMCOUNTRY, STS_MPRMPHONE, _
           STS_MOCC, STS_MCOMPANY, STS_GFIRSTNAME, STS_GMIDNAME, STS_GLASTNAME, STS_GNATIONALITY, _
           STS_GNATIONALITY2, STS_GCOMADDR1, STS_GCOMADDR2, STS_GCOMPOBOX, STS_GCOMCITY, STS_GCOMSTATE, STS_GCOMCOUNTRY, STS_GOFFPHONE, _
           STS_GRESPHONE, STS_GFAX, STS_GMOBILE, STS_GPRMADDR1, STS_GPRMADDR2, STS_GPRMPOBOX, STS_GPRMCITY, STS_GPRMCOUNTRY, _
           STS_GPRMPHONE, STS_GOCC, STS_GCOMPANY, STS_FCOMPANY_ADDR, STS_FEMAIL, STS_MCOMPANY_ADDR, STS_MEMAIL, STS_GCOMPANY_ADDR, _
           STS_GEMAIL, STS_F_COMP_ID, STS_M_COMP_ID, STS_G_COMP_ID, STS_FCOMSTREET, STS_FCOMAREA, STS_FCOMBLDG, STS_FCOMAPARTNO, _
           STS_MCOMSTREET, STS_MCOMAREA, STS_MCOMBLDG, STS_MCOMAPARTNO, STS_GCOMSTREET, STS_GCOMAREA, STS_GCOMBLDG, STS_GCOMAPARTNO, _
           STS_FEMIR, STS_MEMIR, STS_GEMIR, STS_FEMIRATES_ID, STS_MEMIRATES_ID, STS_GEMIRATES_ID, STS_FEMIRATES_ID_EXPDATE, STS_MEMIRATES_ID_EXPDATE, STS_GEMIRATES_ID_EXPDATE, STS_FCOMAREA_ID, STS_FCOMSTATE_ID, STS_MCOMAREA_ID, STS_MCOMSTATE_ID, STS_GCOMAREA_ID, STS_GCOMSTATE_ID)

            Return status
        Catch ex As Exception
            Return -1
        End Try
    End Function
    Function UpdateSTUDENT_D(ByVal STS_STU_ID As String, ByVal STS_FFIRSTNAME As String, ByVal STS_FMIDNAME As String, _
  ByVal STS_FLASTNAME As String, ByVal STS_FNATIONALITY As String, ByVal STS_FNATIONALITY2 As String, ByVal STS_FCOMADDR1 As String, _
  ByVal STS_FCOMADDR2 As String, ByVal STS_FCOMPOBOX As String, ByVal STS_FCOMCITY As String, ByVal STS_FCOMSTATE As String, _
  ByVal STS_FCOMCOUNTRY As String, ByVal STS_FOFFPHONE As String, ByVal STS_FRESPHONE As String, ByVal STS_FFAX As String, _
  ByVal STS_FMOBILE As String, ByVal STS_FPRMADDR1 As String, ByVal STS_FPRMADDR2 As String, ByVal STS_FPRMPOBOX As String, _
  ByVal STS_FPRMCITY As String, ByVal STS_FPRMCOUNTRY As String, ByVal STS_FPRMPHONE As String, ByVal STS_FOCC As String, _
  ByVal STS_FCOMPANY As String, ByVal STS_MFIRSTNAME As String, _
  ByVal STS_MMIDNAME As String, ByVal STS_MLASTNAME As String, ByVal STS_MNATIONALITY As String, ByVal STS_MNATIONALITY2 As String, _
  ByVal STS_MCOMADDR1 As String, ByVal STS_MCOMADDR2 As String, ByVal STS_MCOMPOBOX As String, ByVal STS_MCOMCITY As String, _
  ByVal STS_MCOMSTATE As String, ByVal STS_MCOMCOUNTRY As String, ByVal STS_MOFFPHONE As String, ByVal STS_MRESPHONE As String, _
  ByVal STS_MFAX As String, ByVal STS_MMOBILE As String, ByVal STS_MPRMADDR1 As String, ByVal STS_MPRMADDR2 As String, _
  ByVal STS_MPRMPOBOX As String, ByVal STS_MPRMCITY As String, ByVal STS_MPRMCOUNTRY As String, ByVal STS_MPRMPHONE As String, _
  ByVal STS_MOCC As String, ByVal STS_MCOMPANY As String, _
  ByVal STS_GFIRSTNAME As String, ByVal STS_GMIDNAME As String, ByVal STS_GLASTNAME As String, ByVal STS_GNATIONALITY As String, _
  ByVal STS_GNATIONALITY2 As String, ByVal STS_GCOMADDR1 As String, ByVal STS_GCOMADDR2 As String, ByVal STS_GCOMPOBOX As String, _
  ByVal STS_GCOMCITY As String, ByVal STS_GCOMSTATE As String, ByVal STS_GCOMCOUNTRY As String, ByVal STS_GOFFPHONE As String, _
  ByVal STS_GRESPHONE As String, ByVal STS_GFAX As String, ByVal STS_GMOBILE As String, ByVal STS_GPRMADDR1 As String, _
  ByVal STS_GPRMADDR2 As String, ByVal STS_GPRMPOBOX As String, ByVal STS_GPRMCITY As String, ByVal STS_GPRMCOUNTRY As String, _
  ByVal STS_GPRMPHONE As String, ByVal STS_GOCC As String, ByVal STS_GCOMPANY As String, _
  ByVal STS_FCOMPANY_ADDR As String, ByVal STS_FEMAIL As String, ByVal STS_MCOMPANY_ADDR As String, ByVal STS_MEMAIL As String, ByVal STS_GCOMPANY_ADDR As String, ByVal STS_GEMAIL As String, _
  ByVal STS_F_COMP_ID As String, ByVal STS_M_COMP_ID As String, ByVal STS_G_COMP_ID As String, _
  ByVal STS_FCOMSTREET As String, ByVal STS_FCOMAREA As String, ByVal STS_FCOMBLDG As String, ByVal STS_FCOMAPARTNO As String, _
  ByVal STS_MCOMSTREET As String, ByVal STS_MCOMAREA As String, ByVal STS_MCOMBLDG As String, ByVal STS_MCOMAPARTNO As String, _
  ByVal STS_GCOMSTREET As String, ByVal STS_GCOMAREA As String, ByVal STS_GCOMBLDG As String, ByVal STS_GCOMAPARTNO As String, _
  ByVal STS_FEMIR As String, ByVal STS_MEMIR As String, ByVal STS_GEMIR As String, _
   ByVal STS_FEMIRATES_ID As String, ByVal STS_MEMIRATES_ID As String, ByVal STS_GEMIRATES_ID As String, _
  ByVal STS_FEMIRATES_ID_EXPDATE As String, ByVal STS_MEMIRATES_ID_EXPDATE As String, ByVal STS_GEMIRATES_ID_EXPDATE As String, _
    ByVal STS_FCOMAREA_ID As String, ByVal STS_FCOMSTATE_ID As String, ByVal STS_MCOMAREA_ID As String, ByVal STS_MCOMSTATE_ID As String, ByVal STS_GCOMAREA_ID As String, ByVal STS_GCOMSTATE_ID As String
  ) As Integer
        Dim SESSION_USER As String
        SESSION_USER = HttpContext.Current.Session("username")

        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            'Dim stTrans As SqlTransaction = connection.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted)
            Try
                Dim pParms(121) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@STS_STU_ID", STS_STU_ID)
                pParms(1) = New SqlClient.SqlParameter("@STS_FFIRSTNAME", STS_FFIRSTNAME)
                pParms(2) = New SqlClient.SqlParameter("@STS_FMIDNAME", STS_FMIDNAME)
                pParms(3) = New SqlClient.SqlParameter("@STS_FLASTNAME", STS_FLASTNAME)
                pParms(4) = New SqlClient.SqlParameter("@STS_FNATIONALITY", STS_FNATIONALITY)
                pParms(5) = New SqlClient.SqlParameter("@STS_FNATIONALITY2", STS_FNATIONALITY2)
                pParms(6) = New SqlClient.SqlParameter("@STS_FCOMADDR1", STS_FCOMADDR1)
                pParms(7) = New SqlClient.SqlParameter("@STS_FCOMADDR2", STS_FCOMADDR2)
                pParms(8) = New SqlClient.SqlParameter("@STS_FCOMPOBOX", STS_FCOMPOBOX)
                pParms(9) = New SqlClient.SqlParameter("@STS_FCOMCITY", STS_FCOMCITY)
                pParms(10) = New SqlClient.SqlParameter("@STS_FCOMSTATE", STS_FCOMSTATE)
                pParms(11) = New SqlClient.SqlParameter("@STS_FCOMCOUNTRY", STS_FCOMCOUNTRY)
                pParms(12) = New SqlClient.SqlParameter("@STS_FOFFPHONE", STS_FOFFPHONE)
                pParms(13) = New SqlClient.SqlParameter("@STS_FRESPHONE", STS_FRESPHONE)
                pParms(14) = New SqlClient.SqlParameter("@STS_FFAX", STS_FFAX)
                pParms(15) = New SqlClient.SqlParameter("@STS_FMOBILE", STS_FMOBILE)
                pParms(16) = New SqlClient.SqlParameter("@STS_FPRMADDR1", STS_FPRMADDR1)
                pParms(17) = New SqlClient.SqlParameter("@STS_FPRMADDR2", STS_FPRMADDR2)
                pParms(18) = New SqlClient.SqlParameter("@STS_FPRMPOBOX", STS_FPRMPOBOX)
                pParms(19) = New SqlClient.SqlParameter("@STS_FPRMCITY", STS_FPRMCITY)
                pParms(20) = New SqlClient.SqlParameter("@STS_FPRMCOUNTRY", STS_FPRMCOUNTRY)
                pParms(21) = New SqlClient.SqlParameter("@STS_FPRMPHONE", STS_FPRMPHONE)
                pParms(22) = New SqlClient.SqlParameter("@STS_FOCC", STS_FOCC)
                pParms(23) = New SqlClient.SqlParameter("@STS_FCOMPANY", STS_FCOMPANY)
                'pParms(24) = New SqlClient.SqlParameter("@STS_bFGEMSEMP", STS_bFGEMSEMP)
                'pParms(25) = New SqlClient.SqlParameter("@STS_F_BSU_ID", STS_F_BSU_ID)
                pParms(24) = New SqlClient.SqlParameter("@STS_MFIRSTNAME", STS_MFIRSTNAME)
                pParms(25) = New SqlClient.SqlParameter("@STS_MMIDNAME", STS_MMIDNAME)
                pParms(26) = New SqlClient.SqlParameter("@STS_MLASTNAME", STS_MLASTNAME)
                pParms(27) = New SqlClient.SqlParameter("@STS_MNATIONALITY", STS_MNATIONALITY)
                pParms(28) = New SqlClient.SqlParameter("@STS_MNATIONALITY2", STS_MNATIONALITY2)
                pParms(29) = New SqlClient.SqlParameter("@STS_MCOMADDR1", STS_MCOMADDR1)
                pParms(30) = New SqlClient.SqlParameter("@STS_MCOMADDR2", STS_MCOMADDR2)
                pParms(31) = New SqlClient.SqlParameter("@STS_MCOMPOBOX", STS_MCOMPOBOX)
                pParms(32) = New SqlClient.SqlParameter("@STS_MCOMCITY", STS_MCOMCITY)
                pParms(33) = New SqlClient.SqlParameter("@STS_MCOMSTATE", STS_MCOMSTATE)
                pParms(34) = New SqlClient.SqlParameter("@STS_MCOMCOUNTRY", STS_MCOMCOUNTRY)
                pParms(35) = New SqlClient.SqlParameter("@STS_MOFFPHONE", STS_MOFFPHONE)
                pParms(36) = New SqlClient.SqlParameter("@STS_MRESPHONE", STS_MRESPHONE)
                pParms(37) = New SqlClient.SqlParameter("@STS_MFAX", STS_MFAX)
                pParms(38) = New SqlClient.SqlParameter("@STS_MMOBILE", STS_MMOBILE)
                pParms(39) = New SqlClient.SqlParameter("@STS_MPRMADDR1", STS_MPRMADDR1)
                pParms(40) = New SqlClient.SqlParameter("@STS_MPRMADDR2", STS_MPRMADDR2)
                pParms(41) = New SqlClient.SqlParameter("@STS_MPRMPOBOX", STS_MPRMPOBOX)
                pParms(42) = New SqlClient.SqlParameter("@STS_MPRMCITY", STS_MPRMCITY)
                pParms(43) = New SqlClient.SqlParameter("@STS_MPRMCOUNTRY", STS_MPRMCOUNTRY)
                pParms(44) = New SqlClient.SqlParameter("@STS_MPRMPHONE", STS_MPRMPHONE)
                pParms(45) = New SqlClient.SqlParameter("@STS_MOCC", STS_MOCC)
                pParms(46) = New SqlClient.SqlParameter("@STS_MCOMPANY", STS_MCOMPANY)
                'pParms(49) = New SqlClient.SqlParameter("@STS_bMGEMSEMP", STS_bMGEMSEMP)
                ' pParms(50) = New SqlClient.SqlParameter("@STS_M_BSU_ID", STS_M_BSU_ID)
                pParms(47) = New SqlClient.SqlParameter("@STS_GFIRSTNAME", STS_GFIRSTNAME)
                pParms(48) = New SqlClient.SqlParameter("@STS_GMIDNAME", STS_GMIDNAME)
                pParms(49) = New SqlClient.SqlParameter("@STS_GLASTNAME", STS_GLASTNAME)
                pParms(50) = New SqlClient.SqlParameter("@STS_GNATIONALITY", STS_GNATIONALITY)
                pParms(51) = New SqlClient.SqlParameter("@STS_GNATIONALITY2", STS_GNATIONALITY2)
                pParms(52) = New SqlClient.SqlParameter("@STS_GCOMADDR1", STS_GCOMADDR1)
                pParms(53) = New SqlClient.SqlParameter("@STS_GCOMADDR2", STS_GCOMADDR2)
                pParms(54) = New SqlClient.SqlParameter("@STS_GCOMPOBOX", STS_GCOMPOBOX)
                pParms(55) = New SqlClient.SqlParameter("@STS_GCOMCITY", STS_GCOMCITY)
                pParms(56) = New SqlClient.SqlParameter("@STS_GCOMSTATE", STS_GCOMSTATE)
                pParms(57) = New SqlClient.SqlParameter("@STS_GCOMCOUNTRY", STS_GCOMCOUNTRY)
                pParms(58) = New SqlClient.SqlParameter("@STS_GOFFPHONE", STS_GOFFPHONE)
                pParms(59) = New SqlClient.SqlParameter("@STS_GRESPHONE", STS_GRESPHONE)
                pParms(60) = New SqlClient.SqlParameter("@STS_GFAX", STS_GFAX)
                pParms(61) = New SqlClient.SqlParameter("@STS_GMOBILE", STS_GMOBILE)
                pParms(62) = New SqlClient.SqlParameter("@STS_GPRMADDR1", STS_GPRMADDR1)
                pParms(63) = New SqlClient.SqlParameter("@STS_GPRMADDR2", STS_GPRMADDR2)

                pParms(64) = New SqlClient.SqlParameter("@STS_GPRMPOBOX", STS_GPRMPOBOX)
                pParms(65) = New SqlClient.SqlParameter("@STS_GPRMCITY", STS_GPRMCITY)
                pParms(66) = New SqlClient.SqlParameter("@STS_GPRMCOUNTRY", STS_GPRMCOUNTRY)
                pParms(67) = New SqlClient.SqlParameter("@STS_GPRMPHONE", STS_GPRMPHONE)
                pParms(68) = New SqlClient.SqlParameter("@STS_GOCC", STS_GOCC)
                pParms(69) = New SqlClient.SqlParameter("@STS_GCOMPANY", STS_GCOMPANY)
                pParms(70) = New SqlClient.SqlParameter("@STS_FCOMPANY_ADDR", STS_FCOMPANY_ADDR)
                pParms(71) = New SqlClient.SqlParameter("@STS_FEMAIL", STS_FEMAIL)
                pParms(72) = New SqlClient.SqlParameter("@STS_MCOMPANY_ADDR", STS_MCOMPANY_ADDR)
                pParms(73) = New SqlClient.SqlParameter("@STS_MEMAIL", STS_MEMAIL)
                pParms(74) = New SqlClient.SqlParameter("@STS_GCOMPANY_ADDR", STS_GCOMPANY_ADDR)
                pParms(75) = New SqlClient.SqlParameter("@STS_GEMAIL", STS_GEMAIL)
                pParms(76) = New SqlClient.SqlParameter("@STS_F_COMP_ID", STS_F_COMP_ID)
                pParms(77) = New SqlClient.SqlParameter("@STS_M_COMP_ID", STS_M_COMP_ID)
                pParms(78) = New SqlClient.SqlParameter("@STS_G_COMP_ID", STS_G_COMP_ID)

                pParms(79) = New SqlClient.SqlParameter("@STS_FCOMSTREET", STS_FCOMSTREET)
                pParms(80) = New SqlClient.SqlParameter("@STS_FCOMAREA", STS_FCOMAREA)
                pParms(81) = New SqlClient.SqlParameter("@STS_FCOMBLDG", STS_FCOMBLDG)
                pParms(82) = New SqlClient.SqlParameter("@STS_FCOMAPARTNO", STS_FCOMAPARTNO)


                pParms(83) = New SqlClient.SqlParameter("@STS_MCOMSTREET", STS_MCOMSTREET)
                pParms(84) = New SqlClient.SqlParameter("@STS_MCOMAREA", STS_MCOMAREA)
                pParms(85) = New SqlClient.SqlParameter("@STS_MCOMBLDG", STS_MCOMBLDG)
                pParms(86) = New SqlClient.SqlParameter("@STS_MCOMAPARTNO", STS_MCOMAPARTNO)

                pParms(87) = New SqlClient.SqlParameter("@STS_GCOMSTREET", STS_GCOMSTREET)
                pParms(88) = New SqlClient.SqlParameter("@STS_GCOMAREA", STS_GCOMAREA)
                pParms(89) = New SqlClient.SqlParameter("@STS_GCOMBLDG", STS_GCOMBLDG)
                pParms(90) = New SqlClient.SqlParameter("@STS_GCOMAPARTNO", STS_GCOMAPARTNO)

                pParms(91) = New SqlClient.SqlParameter("@STS_FEMIR", STS_FEMIR)
                pParms(92) = New SqlClient.SqlParameter("@STS_MEMIR", STS_MEMIR)
                pParms(93) = New SqlClient.SqlParameter("@STS_GEMIR", STS_GEMIR)

                pParms(94) = New SqlClient.SqlParameter("@STS_FEMIRATES_ID", STS_FEMIRATES_ID)
                pParms(95) = New SqlClient.SqlParameter("@STS_MEMIRATES_ID", STS_MEMIRATES_ID)
                pParms(96) = New SqlClient.SqlParameter("@STS_GEMIRATES_ID", STS_GEMIRATES_ID)

                pParms(97) = New SqlClient.SqlParameter("@STS_FEMIRATES_ID_EXPDATE", IIf(STS_FEMIRATES_ID_EXPDATE = "", System.DBNull.Value, STS_FEMIRATES_ID_EXPDATE))
                pParms(98) = New SqlClient.SqlParameter("@STS_MEMIRATES_ID_EXPDATE", IIf(STS_MEMIRATES_ID_EXPDATE = "", System.DBNull.Value, STS_MEMIRATES_ID_EXPDATE))
                pParms(99) = New SqlClient.SqlParameter("@STS_GEMIRATES_ID_EXPDATE", IIf(STS_GEMIRATES_ID_EXPDATE = "", System.DBNull.Value, STS_GEMIRATES_ID_EXPDATE))

                pParms(108) = New SqlClient.SqlParameter("@STS_FCOMAREA_ID", STS_FCOMAREA_ID)
                pParms(109) = New SqlClient.SqlParameter("@STS_FCOMSTATE_ID", STS_FCOMSTATE_ID)
                pParms(110) = New SqlClient.SqlParameter("@STS_MCOMAREA_ID", STS_MCOMAREA_ID)

                pParms(111) = New SqlClient.SqlParameter("@STS_MCOMSTATE_ID", STS_MCOMSTATE_ID)
                pParms(112) = New SqlClient.SqlParameter("@STS_GCOMAREA_ID", STS_GCOMAREA_ID)
                pParms(113) = New SqlClient.SqlParameter("@STS_GCOMSTATE_ID", STS_GCOMSTATE_ID)

                pParms(114) = New SqlClient.SqlParameter("@SESSION_USER", SESSION_USER)

                pParms(100) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                pParms(100).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "UpdateStudent_D_PrimaryContact", pParms)

            Catch ex As Exception
                Return 1000
            End Try
        End Using
    End Function




End Class
