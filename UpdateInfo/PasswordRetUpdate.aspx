<%@ Page Language="VB" AutoEventWireup="false" 
CodeFile="PasswordRetUpdate.aspx.vb" Inherits="PasswordRetUpdate" %>




<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>GEMS Education</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
   <link href="../CSS/SiteStyle_Password.css" rel="stylesheet" type="text/css" />
    <link href="../CSS/MenuStyle.css" rel="stylesheet" type="text/css" />
    <meta content="GEMS Education" name="description" />
    <meta content="GEMS Education" name="keywords" />
   

</head>
<body>
    <form runat="server" id="SignIn">
    <div style="text-align:center !important; width:100%;">
<div class="wrapper">
<div class="header" style="text-align:left !important;"  ><div class="hometoplinks"><asp:LinkButton ID="lbHomeTop" runat="server" class="hometoplinks" Text="Home"></asp:LinkButton>&nbsp;|&nbsp;<a href="http://www.gemseducation.com/MENASA/parents/contents.php?pageid=6&expandable=0&parentid=1" target="_blank" class="hometoplinks" >About Us</a></div><div class="menubox"><asp:LinkButton ID="lbPwdAsst" runat="server" CssClass="sel-menu" ></asp:LinkButton></div>
</div>
 <div class="leftBox"><asp:Label ID="lblError" runat="server"  EnableViewState="False" ></asp:Label>
   <div class="mainheading">
      <div class="left">Create New Password</div>
             </div>
    <div> 
              <div class="divinfoPW" style="width:100%;">
     <div>&nbsp;&nbsp;&bull;&nbsp;&nbsp;An asterisk (<font color="#ff0000">*</font>) indicates mandatory fields.<br />&nbsp;&nbsp;&bull;&nbsp;&nbsp;Old password and new password cannot be same.
     <br />&nbsp;&nbsp;&bull;&nbsp;&nbsp;Password must be at least 7 characters long.</div>
     </div>  <div style="padding-top:15px;">          
                          <table  align="center" class="tableNoborder" width="50%">
                               <tr>
                    <td align="left" class="tdfields" >
                        Enter your New Password<font color="red">*</font></td>
                  
                    <td align="left" class="tdfields"  >
                        <asp:TextBox ID="txtNewpassword" runat="server" 
                        MaxLength="100" CssClass="input"
                            TextMode="Password" Width="161px"></asp:TextBox>
                                                  <%-- <asp:RegularExpressionValidator ID="revPassword" runat="server" ControlToValidate="txtNewpassword"
                            Display="Dynamic" ErrorMessage="Your password must contain atleast  7 characters"
                            ForeColor="red" SetFocusOnError="True" ValidationExpression="^[a-zA-Z0-9]{7,20}$" 
                            ValidationGroup="GroupPwd">*</asp:RegularExpressionValidator>--%>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtNewpassword"
                            Display="Dynamic" ErrorMessage="Please enter your new password"
                            ForeColor="red" ValidationGroup="GroupPwd">*</asp:RequiredFieldValidator><br />
                        </td>
                </tr>
                <tr><td colspan="2"> &nbsp;</td></tr>
                <tr>
                    <td align="left" class="tdfields"  >
                        Confirm your New Password<font color="red">*</font></td>
                  
                    <td align="left"  class="tdfields" >
                        <asp:TextBox ID="txtConfPassword" runat="server"
                         CssClass="input" TextMode="Password"
                            Width="161px"></asp:TextBox><asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="txtNewpassword"
                            ControlToValidate="txtConfPassword" Display="Dynamic" ErrorMessage="Password do not match"
                            ForeColor="red" ValidationGroup="GroupPwd">*</asp:CompareValidator>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtConfPassword"
                            Display="Dynamic" ErrorMessage="Confirm password can not be left empty"
                            ForeColor="red" ValidationGroup="GroupPwd">*</asp:RequiredFieldValidator></td>
                </tr>
                            <tr><td colspan="2"> &nbsp;</td></tr> 
                                      
                                          <tr>
                                              <td align="center"  colspan="3">
                                                  <asp:Button ID="btnSave" runat="server"  Text="Save"
                                                   ValidationGroup="GroupPwd" CssClass="buttons"
                                                      Width="60px" />
                                                   <asp:Button ID="btnLogin" runat="server"  Text="Login"
                                                   CssClass="buttons" CausesValidation="false"
                                                      Width="60px" />
       <%--                                           <asp:Button ID="btnCancel" runat="server" CausesValidation="False" Height="20px"
                                                      Text="Cancel" Width="70px" OnClientClick="redirect()" />--%></td>
                                          </tr>
                                          <tr>
                                           <td align="center"  colspan="3">
                                            
                                           </td>
                                          </tr>
                                      </table>
                                     
                </div>    
                 <div style="width:100%"> <asp:ValidationSummary ID="vsPass_Recover" runat="server"  
                                            ValidationGroup="GroupPwd"  HeaderText="<div class='validationheader'>Please correct the following:</div>"
                                 CssClass="validationsummary" ForeColor="" />   </div>        
         </div> 
      </div>
      
       </div>
 </div>
    </form>
</body>
</html>
     


