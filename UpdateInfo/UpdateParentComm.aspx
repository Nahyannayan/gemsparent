
<%@ Page Language="VB" MasterPageFile="~/ParentMaster.master" AutoEventWireup="false" 
CodeFile="UpdateParentComm.aspx.vb" Inherits="UpdateInfo_UpdateParentComm" title="GEMS EDUCATION" %> 

<asp:Content ID="Content1" ContentPlaceHolderID="cphParent" Runat="Server">
 
    <script language="javascript" type="text/javascript">
    
      

 function copyFathertoMother(chkThis)
 {
   
    var chk_state=  chkThis.checked ;
      if (chk_state==true)
    {
     document.getElementById('<%=ddlMNationality1.ClientID %>').selectedIndex=document.getElementById('<%=ddlFNationality1.ClientID %>').selectedIndex;
     document.getElementById('<%=ddlMNationality2.ClientID %>').selectedIndex=document.getElementById('<%=ddlFNationality2.ClientID %>').selectedIndex;
     document.getElementById('<%=txtMCOMADDR1.ClientID %>').value=document.getElementById('<%=txtFCOMADDR1.ClientID %>').value;
     document.getElementById('<%=txtMCOMADDR2.ClientID %>').value = document.getElementById('<%=txtFCOMADDR2.ClientID %>').value;
     document.getElementById('<%=txtMBldg.ClientID %>').value = document.getElementById('<%=txtFBldg.ClientID %>').value;
     document.getElementById('<%=txtMAptNo.ClientID %>').value = document.getElementById('<%=txtFAptNo.ClientID %>').value;
      
     document.getElementById('<%=txtMCOMPOBOX.ClientID %>').value=document.getElementById('<%=txtFCOMPOBOX.ClientID %>').value;
     document.getElementById('<%=ddlMCOMEmirate.ClientID %>').selectedIndex=document.getElementById('<%=ddlFCOMEmirate.ClientID %>').selectedIndex;
     document.getElementById('<%=txtMCOMCity.ClientID %>').value=document.getElementById('<%=txtFCOMCity.ClientID %>').value;
     document.getElementById('<%=ddlMCOMCountry.ClientID %>').selectedIndex=document.getElementById('<%=ddlFCOMCountry.ClientID %>').selectedIndex;
     document.getElementById('<%=ddlMCity.ClientID %>').selectedIndex=document.getElementById('<%=ddlFCity.ClientID %>').selectedIndex;
      document.getElementById('<%=ddlMCity.ClientID %>').onchange();
      
     document.getElementById('<%=ddlMArea.ClientID %>').selectedIndex=document.getElementById('<%=ddlFArea.ClientID %>').selectedIndex;
     document.getElementById('<%=txtMResPhone_Country.ClientID %>').value=document.getElementById('<%=txtFResPhone_Country.ClientID %>').value;
       document.getElementById('<%=txtMResPhone_Country.ClientID %>').value=document.getElementById('<%=txtFResPhone_Country.ClientID %>').value;
     document.getElementById('<%=txtMResPhone_Area.ClientID %>').value=document.getElementById('<%=txtFResPhone_Area.ClientID %>').value;
     document.getElementById('<%=txtMResPhone_No.ClientID %>').value=document.getElementById('<%=txtFResPhone_No.ClientID %>').value;
//     document.getElementById('<%=txtMOffPhone_Country.ClientID %>').value=document.getElementById('<%=txtFOffPhone_Country.ClientID %>').value;
//     document.getElementById('<%=txtMOffPhone_Area.ClientID %>').value=document.getElementById('<%=txtFOffPhone_Area.ClientID %>').value;
//     document.getElementById('<%=txtMOffPhone_No.ClientID %>').value=document.getElementById('<%=txtFOffPhone_No.ClientID %>').value;
//     document.getElementById('<%=txtMMobile_Country.ClientID %>').value=document.getElementById('<%=txtFMobile_Country.ClientID %>').value;
//     document.getElementById('<%=txtMMobile_Area.ClientID %>').value=document.getElementById('<%=txtFMobile_Area.ClientID %>').value;
//     document.getElementById('<%=txtMMobile_No.ClientID %>').value=document.getElementById('<%=txtFMobile_No.ClientID %>').value;
//     document.getElementById('<%=txtMFax_Country.ClientID %>').value=document.getElementById('<%=txtFFax_Country.ClientID %>').value;
//     document.getElementById('<%=txtMFax_Area.ClientID %>').value=document.getElementById('<%=txtFFax_Area.ClientID %>').value;
//     document.getElementById('<%=txtMFax_No.ClientID %>').value=document.getElementById('<%=txtFFax_No.ClientID %>').value;
     document.getElementById('<%=txtMPRMAddr1.ClientID %>').value=document.getElementById('<%=txtFPRMAddr1.ClientID %>').value;
     document.getElementById('<%=txtMPRMAddr2.ClientID %>').value=document.getElementById('<%=txtFPRMAddr2.ClientID %>').value;
     document.getElementById('<%=txtMPRM_City.ClientID %>').value=document.getElementById('<%=txtFPRM_City.ClientID %>').value;
     document.getElementById('<%=ddlMPRM_Country.ClientID %>').selectedIndex=document.getElementById('<%=ddlFPRM_Country.ClientID %>').selectedIndex;
     document.getElementById('<%=txtMPPermant_Country.ClientID %>').value=document.getElementById('<%=txtFPPRM_Country.ClientID %>').value;
     document.getElementById('<%=txtMPPermant_Area.ClientID %>').value=document.getElementById('<%=txtFPPRM_Area.ClientID %>').value;
     document.getElementById('<%=txtMPPermant_No.ClientID %>').value=document.getElementById('<%=txtFPPRM_No.ClientID %>').value;
     document.getElementById('<%=txtMPRMPOBOX.ClientID %>').value=document.getElementById('<%=txtFPRMPOBOX.ClientID %>').value;
     }
     else
     {
     document.getElementById('<%=ddlMNationality1.ClientID %>').selectedIndex=0;
     document.getElementById('<%=ddlMNationality2.ClientID %>').selectedIndex=0;
     document.getElementById('<%=txtMCOMADDR1.ClientID %>').value="";
     document.getElementById('<%=txtMCOMADDR2.ClientID %>').value = "";
     document.getElementById('<%=txtMBldg.ClientID %>').value = "";
     document.getElementById('<%=txtMAptNo.ClientID %>').value = "";
     document.getElementById('<%=txtMCOMPOBOX.ClientID %>').value="";
     document.getElementById('<%=ddlMCOMEmirate.ClientID %>').selectedIndex=0;
     document.getElementById('<%=txtMCOMCity.ClientID %>').value="";
     document.getElementById('<%=ddlMCOMCountry.ClientID %>').selectedIndex=0;
     document.getElementById('<%=ddlMCity.ClientID %>').selectedIndex=0;
     document.getElementById('<%=ddlMArea.ClientID %>').selectedIndex=0;
     
     document.getElementById('<%=txtMResPhone_Country.ClientID %>').value="";
     document.getElementById('<%=txtMResPhone_Area.ClientID %>').value="";
     document.getElementById('<%=txtMResPhone_No.ClientID %>').value="";
     document.getElementById('<%=txtMOffPhone_Country.ClientID %>').value="";
     document.getElementById('<%=txtMOffPhone_Area.ClientID %>').value="";
     document.getElementById('<%=txtMOffPhone_No.ClientID %>').value="";
     document.getElementById('<%=txtMMobile_Country.ClientID %>').value="";
     document.getElementById('<%=txtMMobile_Area.ClientID %>').value="";
     document.getElementById('<%=txtMMobile_No.ClientID %>').value="";
     document.getElementById('<%=txtMFax_Country.ClientID %>').value="";
     document.getElementById('<%=txtMFax_Area.ClientID %>').value="";
     document.getElementById('<%=txtMFax_No.ClientID %>').value="";
     document.getElementById('<%=txtMPRMAddr1.ClientID %>').value="";
     document.getElementById('<%=txtMPRMAddr2.ClientID %>').value="";
     document.getElementById('<%=txtMPRM_City.ClientID %>').value="";
     document.getElementById('<%=ddlMPRM_Country.ClientID %>').selectedIndex=0;
     document.getElementById('<%=txtMPPermant_Country.ClientID %>').value="";
     document.getElementById('<%=txtMPPermant_Area.ClientID %>').value="";
     document.getElementById('<%=txtMPPermant_No.ClientID %>').value="";
     document.getElementById('<%=txtMPRMPOBOX.ClientID %>').value="";
     }
     return true;
    }
    
    </script>
    <div class="content margin-top30 margin-bottom60">
                    <div class="container">
                        <div class="row">
                            
                         <!-- Posts Block -->
      <div class="posts-block col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="my-account">
   <asp:Label ID="lblError" runat="server" EnableViewState="False"></asp:Label>
      <div class="mainheading">Parent Communication</div>
    <div>
     <div class="alert alert-warning">
          <button type="button" class="close" data-dismiss="alert">�</button>
    <div>An asterisk (<font color="#ff0000">*</font>) indicates mandatory fields.</div>
     <asp:ValidationSummary ID="vsAppl_info" runat="server" ValidationGroup="groupM1"
                 HeaderText="<div class='validationheaderInside'>Please update the following:</div>"
                class="validationsummaryInside" />
    <asp:Label id="lblValidate_msg" runat="Server"></asp:Label>
    </div>
   
 <table align="left" class="table table-striped table-bordered table-responsive text-left my-orders-table" width="100%">

  <tr>
        <td  colspan="4" class="sub-heading">
                               Parent </td>
                       </tr>
                         
                             <tr>
                                <td align="left">
                                    Primary Contact</td>
                              
                                <td align="left"  colspan="4" >
                                     <asp:Label ID="lblPriContact" runat="server"  ></asp:Label> 
                                   </td>
                            </tr>
                            
                            <tr>
                                <td align="left" style="vertical-align:middle">
                                    Preferred Contact</td>
                              
                                <td align="left"  colspan="4"><asp:DropDownList ID="ddlPrefContact" runat="server" CssClass="form-control" >
                                        <asp:ListItem>Home Phone</asp:ListItem>
                                        <asp:ListItem>Office Phone</asp:ListItem>
                                        <asp:ListItem>Mobile</asp:ListItem>
                                        <asp:ListItem>Email</asp:ListItem>
                                    </asp:DropDownList></td>
                            </tr>
                            
                            
                             <tr>
                                <td align="left" style="vertical-align:middle"> 
                                    Emergency Contact Number</td>
                              
                                <td align="left"  colspan="4" >
                                                                    
                                    <asp:TextBox ID="txtEmgContact_Country" runat="server"  MaxLength="3" CssClass="form-control" Width="60px"></asp:TextBox>
                                    -
                                    <asp:TextBox ID="txtEmgContact_Area" runat="server" MaxLength="4" CssClass="form-control" Width="60px"></asp:TextBox>
                                    -
                                    <asp:TextBox ID="txtEmgContact_No" runat="server"  MaxLength="10" CssClass="form-control" Width="90px"></asp:TextBox>
                                    <div class="remark">(Country-Area-Number)</div>
                                     <ajaxToolkit:FilteredTextBoxExtender ID="ftbeECont_Country" runat="server" 
                                     TargetControlID="txtEmgContact_Country"
                                            FilterType="Numbers">
                                         
                                        </ajaxToolkit:FilteredTextBoxExtender>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="ftbeEContact_Area" runat="server"
                                         TargetControlID="txtEmgContact_Area"
                                            FilterType="Numbers">
                                        </ajaxToolkit:FilteredTextBoxExtender>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="ftbeEContact_No" runat="server" 
                                        TargetControlID="txtEmgContact_No"
                                            FilterType="Numbers">
                                        </ajaxToolkit:FilteredTextBoxExtender>
                                    </td>
                                    </tr>
                                    
                                  
                     
                       <tr class="tdblankAll">
                           <td   colspan="4" 
                            class="tdblankAll"
                             >&nbsp;</td></tr>
                  
                       <tr>
                                           <td  colspan="4" class="sub-heading">
                               Father Details</td>
                       </tr>
                          
                            <tr>
                                <td align="left">
                                  
                                    Full Name As Per Passport</td>
                             
                                <td align="left"  colspan="4">
                                    <asp:TextBox ID="txtFFirstName" runat="server"  CssClass="form-control" MaxLength="100"></asp:TextBox>
                                    <asp:TextBox ID="txtFMidName" runat="server" CssClass="form-control" MaxLength="100"></asp:TextBox>
                                    <asp:TextBox ID="txtFLastName" runat="server" CssClass="form-control" MaxLength="100"></asp:TextBox>    
                                    <ajaxToolkit:FilteredTextBoxExtender ID="ftbFFNAME" runat="server"
                                                           TargetControlID="txtFFirstName" 
                                                           FilterType="Custom,UppercaseLetters,Numbers,LowercaseLetters"
                                                            ValidChars="\.'/ "></ajaxToolkit:FilteredTextBoxExtender>
                             <ajaxToolkit:FilteredTextBoxExtender ID="ftbFMNAME" runat="server"
                                                           TargetControlID="txtFMidName" 
                                                           FilterType="Custom,UppercaseLetters,Numbers,LowercaseLetters"
                                                            ValidChars="\.'/ "></ajaxToolkit:FilteredTextBoxExtender>
                                    <ajaxToolkit:FilteredTextBoxExtender ID="ftbFLNAME" runat="server"
                                                           TargetControlID="txtFLastName" 
                                                           FilterType="Custom,UppercaseLetters,Numbers,LowercaseLetters"
                                                            ValidChars="\.'/ "></ajaxToolkit:FilteredTextBoxExtender>
                                                
                                    </td>
                            </tr>
                            <tr><td align="left" style="vertical-align:middle">Emirates ID</td> <td align="left" width="30%; style="vertical-align:middle"">
  <asp:TextBox ID="txtFEMIRATES_ID" runat="server" Width="146px" CssClass="form-control"></asp:TextBox></td>
  <td align="left" style="vertical-align:middle">Emirates ID Expiry Date
                                  </td>
                                
                                <td align="left">
                                   <asp:TextBox ID="txtFEMIRATES_ID_EXPDATE" runat="server" Width="110px" CssClass="form-control" MaxLength="11"
                                TabIndex="32">
                            </asp:TextBox>&nbsp;<asp:ImageButton ID="imgBtnFEMIRATES_IDExp_date" runat="server" ImageUrl="~/Images/Calendar.png"  CssClass="imgCal" />
                                                        <ajaxToolkit:FilteredTextBoxExtender ID="ftbFEMIRATES_IDExp_date" runat="server" TargetControlID="txtFEMIRATES_ID_EXPDATE"
                                FilterType="Custom,UppercaseLetters,LowercaseLetters,Numbers" ValidChars="/">
                            </ajaxToolkit:FilteredTextBoxExtender>
                            <ajaxToolkit:CalendarExtender ID="ceFEMIRATES_IDExp_date" runat="server" Format="dd/MMM/yyyy"
                                PopupButtonID="imgBtnFEMIRATES_IDExp_date" TargetControlID="txtFEMIRATES_ID_EXPDATE">
                            </ajaxToolkit:CalendarExtender>
                             <asp:RegularExpressionValidator ID="revFEmir_Exp_date"
                                            runat="server" ControlToValidate="txtFEMIRATES_ID_EXPDATE" Display="Dynamic" EnableViewState="False"
                                            ErrorMessage="Enter father's Emirates ID expiry  Date in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                            ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                            ValidationGroup="groupM1">*</asp:RegularExpressionValidator><div class="remark">(dd/mmm/yyyy)</div>
                                </td></tr>
                            <tr>
                                <td align="left" style="vertical-align:middle" style="width:130px;">
                                    Nationality1</td>
                            
                                <td align="left" style="width:150px;" >
                                    <asp:DropDownList ID="ddlFNationality1" runat="server" CssClass="form-control">
                                    </asp:DropDownList></td>
                                <td align="left" style="vertical-align:middle">
                                    Nationality2</td>
                               
                                <td align="left" >
                                    <asp:DropDownList ID="ddlFNationality2" runat="server" CssClass="form-control">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                             <%--Residential Address Street Was Here--%>
                                 <td align="left" style="vertical-align:middle">  
                                    Residential Address Country</td>
                            
                                <td align="left" style="vertical-align:middle">
                                    <asp:DropDownList ID="ddlFCOMCountry" runat="server" CssClass="form-control" AutoPostBack="true">
                                    </asp:DropDownList>
                                    </td>
                                    
                                <td align="left" style="vertical-align:middle"> 
                                    Residential Address City/State <span id="SpFRaddCity" runat="server" style="color:Red;" visible="false" >*</span></td>
                                                             <td align="left"  >
                                                             
                                                              <div class="tdPadDiv">
                                                City/State</div><asp:DropDownList ID="ddlFCity" runat="server" CssClass="form-control" AutoPostBack="true">
                                            </asp:DropDownList>
                                            
                                             <asp:RequiredFieldValidator ID="rfvFRaddCity" runat="server" ControlToValidate="ddlFCity"
                                         ErrorMessage="Father's residential addresscity/state required" ForeColor="red" ValidationGroup="groupM1">*</asp:RequiredFieldValidator>
                                         
                                            <div class="remark_info" style="padding-top: 4px; padding-bottom: 4px;">
                                                If you choose other,please specify the City/State Name</div>
                                            <asp:TextBox ID="txtFCOMCity" runat="server" MaxLength="50" CssClass="form-control">
                                            </asp:TextBox>
                                    </td>
                           
                               <%--Residential Address Area Was Here--%>  
                                
                            </tr>
                            <tr>
                              <%--Residential Address Buiding Was Here--%>  
                                <td align="left" style="vertical-align:middle">  
                                    Residential Address Area<span id="SpFRaddA" runat="server" style="color:Red;" visible="false" >*</span></td>
                            
                                <td align="left" >
                                 <asp:DropDownList ID="ddlFArea" runat="server" CssClass="form-control">
                                            </asp:DropDownList>
                                            
                                            <asp:RequiredFieldValidator ID="rfvFRaddA" runat="server" ControlToValidate="ddlFArea"
                                         ErrorMessage="Father's residential address area required" ForeColor="red" ValidationGroup="groupM1">*</asp:RequiredFieldValidator>
                                         
                                            <div class="remark_info" style="padding-top: 4px; padding-bottom: 4px;">
                                                If you choose other,please specify the Area Name</div>
                                    <asp:TextBox ID="txtFCOMADDR2" runat="server" CssClass="form-control"></asp:TextBox>
                                    
                                    </td>
                                    
                                     <td align="left" style="vertical-align:middle">  
                                    Residential Address Street<span id="SpFRaddS" runat="server" style="color:Red;" visible="false" >*</span></td>
                                    
                                    <td align="left" style="vertical-align:middle" >
                                    <asp:TextBox ID="txtFCOMADDR1" runat="server" CssClass="form-control"></asp:TextBox>
                                     <asp:RequiredFieldValidator ID="rfvFRaddS" runat="server" ControlToValidate="txtFCOMADDR1"
                                         ErrorMessage="Father's residential address street required" ForeColor="red" ValidationGroup="groupM1">*</asp:RequiredFieldValidator></td>


                               
                            </tr>
                            
                             <tr>
                             <%--Residential Address City Was Here--%>
                               <td align="left" style="vertical-align:middle">  
                                    Residential Address Building<span id="SpFRaddB" runat="server" style="color:Red;" visible="false" >*</span></td>
                               
                                <td align="left"  >
                                    <asp:TextBox ID="txtFBldg" runat="server" CssClass="form-control"> </asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvFRaddB" runat="server" ControlToValidate="txtFBldg"
                                         ErrorMessage="Father's residential address building required" ForeColor="red" ValidationGroup="groupM1">*</asp:RequiredFieldValidator>
                                    </td>
                                      <%--Residential Address Country Was Here--%>
                                       <td align="left" style="vertical-align:middle"> 
                                    Residential Address Apartment No.<span id="SpFRaddN" runat="server" style="color:Red;" visible="false" >*</span></td>
                                
                                <td align="left" >
                                    <asp:TextBox ID="txtFAptNo" runat="server" CssClass="form-control"> </asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvFRaddN" runat="server" ControlToValidate="txtFAptNo"
                                         ErrorMessage="Father's residential address apartment No. required" ForeColor="red" ValidationGroup="groupM1">*</asp:RequiredFieldValidator></td>
                                                                    </tr>
                            
                            
                            <tr>
                                <td align="left" style="vertical-align:middle">  
                                    P.O BOX<span id="Fpobox" runat="server" style="color:Red;" visible="false" >*</span></td>
                             
                                <td align="left"  >
                                    <asp:TextBox ID="txtFCOMPOBOX" runat="server" 
                                    Width="86px" CssClass="form-control" MaxLength="20"></asp:TextBox>
       <asp:RequiredFieldValidator ID="rfvFcompobox" runat="server" ControlToValidate="txtFCOMPOBOX"
                                    Display="Dynamic" ErrorMessage="Please enter Father's P.O BOX" ValidationGroup="groupM1" >*</asp:RequiredFieldValidator>  

                                    </td>
                                <td align="left" style="vertical-align:middle"> 
                                    Emirate</td>
                                
                                <td align="left" >
                                    <asp:DropDownList ID="ddlFCOMEmirate" runat="server" CssClass="form-control">
                                    </asp:DropDownList>
                                    
                                 </td>
                            </tr>
                           
                            <tr>
                                <td align="left" style="vertical-align:middle">
                                    Home Phone</td>
                               
                                <td align="left"  >
                                
                                    <asp:TextBox ID="txtFResPhone_Country" runat="server"  Width="60px" MaxLength="3" CssClass="form-control"> </asp:TextBox>
                                    -
                                    <asp:TextBox ID="txtFResPhone_Area" runat="server" Width="60px" MaxLength="4" CssClass="form-control"></asp:TextBox>
                                    -
                                    <asp:TextBox ID="txtFResPhone_No" runat="server" Width="90px" MaxLength="10" CssClass="form-control"></asp:TextBox><br />
                                    <div class="remark">(Country-Area-Number)</div>
                                          <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server"
                                           TargetControlID="txtFResPhone_Country"
                                            FilterType="Numbers">
                                        </ajaxToolkit:FilteredTextBoxExtender>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server"
                                         TargetControlID="txtFResPhone_Area"
                                            FilterType="Numbers">
                                        </ajaxToolkit:FilteredTextBoxExtender>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" 
                                        TargetControlID="txtFResPhone_No"
                                            FilterType="Numbers">
                                        </ajaxToolkit:FilteredTextBoxExtender>  
                                     </td>
                                <td align="left"  style="vertical-align:middle">
                                    Office Phone</td>
                               
                                <td align="left"  >
                                    <asp:TextBox ID="txtFOffPhone_Country" runat="server" Width="60px" MaxLength="3" CssClass="form-control"></asp:TextBox>
                                    -
                                    <asp:TextBox ID="txtFOffPhone_Area" runat="server" Width="60px" MaxLength="4" CssClass="form-control"></asp:TextBox>
                                    -
                                    <asp:TextBox ID="txtFOffPhone_No" runat="server" Width="90px" MaxLength="10" CssClass="form-control"></asp:TextBox>
                                    <div class="remark">(Country-Area-Number)</div>
                                     <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server"
                                           TargetControlID="txtFOffPhone_Country"
                                            FilterType="Numbers">
                                        </ajaxToolkit:FilteredTextBoxExtender>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" 
                                        TargetControlID="txtFOffPhone_Area"
                                            FilterType="Numbers">
                                        </ajaxToolkit:FilteredTextBoxExtender>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server"
                                         TargetControlID="txtFOffPhone_No"
                                            FilterType="Numbers">
                                        </ajaxToolkit:FilteredTextBoxExtender>    
                                    </td>
                            </tr>
                            <tr>
                                <td align="left" style="vertical-align:middle">  
                                    Mobile<span id="SpFMobNo" runat="server" style="color:Red;" visible="false" >*</span></td>
                               
                                <td align="left"  >
                                                                    
                                    <asp:TextBox ID="txtFMobile_Country" runat="server" Width="60px" MaxLength="3" CssClass="form-control"></asp:TextBox>
                                    -
                                    <asp:TextBox ID="txtFMobile_Area" runat="server" Width="60px" MaxLength="4" CssClass="form-control"></asp:TextBox>
                                    -
                                    <asp:TextBox ID="txtFMobile_No" runat="server" Width="90px" MaxLength="10" CssClass="form-control"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvFMobNo" runat="server" ControlToValidate="txtFMobile_No"
                                    Display="Dynamic" ErrorMessage="Father's mobile no required" ValidationGroup="groupM1" >*</asp:RequiredFieldValidator>  
                                    <div class="remark">(Country-Area-Number)</div>
                                     <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server"
                                           TargetControlID="txtFMobile_Country"
                                            FilterType="Numbers">
                                        </ajaxToolkit:FilteredTextBoxExtender>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" 
                                        TargetControlID="txtFMobile_Area"
                                            FilterType="Numbers">
                                        </ajaxToolkit:FilteredTextBoxExtender>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server" 
                                        TargetControlID="txtFMobile_No"
                                            FilterType="Numbers">
                                        </ajaxToolkit:FilteredTextBoxExtender>     
                                    </td>
                                <td align="left" style="vertical-align:middle"> 
                                    Fax No</td>
                               
                                <td align="left" >
                                    <asp:TextBox ID="txtFFax_Country" runat="server" Width="60px" MaxLength="3" CssClass="form-control"></asp:TextBox>
                                    -
                                    <asp:TextBox ID="txtFFax_Area" runat="server" Width="60px" MaxLength="4" CssClass="form-control"></asp:TextBox>
                                    -
                                    <asp:TextBox ID="txtFFax_No" runat="server" Width="90px" MaxLength="10" CssClass="form-control"></asp:TextBox>
                                    <div class="remark">(Country-Area-Number)</div>
                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" runat="server"
                                           TargetControlID="txtFFax_Country"
                                            FilterType="Numbers">
                                        </ajaxToolkit:FilteredTextBoxExtender>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender11" runat="server" 
                                        TargetControlID="txtFFax_Area"
                                            FilterType="Numbers">
                                        </ajaxToolkit:FilteredTextBoxExtender>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender12" runat="server" 
                                        TargetControlID="txtFFax_No"
                                            FilterType="Numbers">
                                        </ajaxToolkit:FilteredTextBoxExtender> 
                                    </td>
                            </tr>
                            <tr>
                                <td align="left" style="vertical-align:middle">
                                    Overseas Address Line 1<span id="SpFPAddL1" runat="server" style="color:Red;" visible="false" >*</span></td>
                            
                                <td align="left" >
                                    <asp:TextBox ID="txtFPRMAddr1" runat="server" CssClass="form-control"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvFPAddL1" runat="server" ControlToValidate="txtFPRMAddr1"
                                         ErrorMessage="Father's Required overseas address Line 1" ForeColor="red" ValidationGroup="groupM1">*</asp:RequiredFieldValidator>
                                    </td>
                                <td align="left"  style="vertical-align:middle">
                                    Overseas Address Line 2<span id="SpFPAddL2" runat="server" style="color:Red;" visible="false" >*</span></td>
                             
                                <td align="left"  >
                                    <asp:TextBox ID="txtFPRMAddr2" runat="server" CssClass="form-control"></asp:TextBox>
                                     <asp:RequiredFieldValidator ID="rfvFPAddL2" runat="server" ControlToValidate="txtFPRMAddr2"
                                         ErrorMessage="Father's required overseas address Line 2" ForeColor="red" ValidationGroup="groupM1">*</asp:RequiredFieldValidator></td>
                            </tr>
                            <tr>
                                <td align="left"  style="vertical-align:middle">
                                    Overseas Address City/State<span id="SpFPAddCity" runat="server" style="color:Red;" visible="false" >*</span></td>
                                
                                <td align="left"  >
                                    <asp:TextBox ID="txtFPRM_City" runat="server" CssClass="form-control"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvFPAddCity" runat="server" ControlToValidate="txtFPRM_City"
                                         ErrorMessage="Father's overseas address city/state required" ForeColor="red" ValidationGroup="groupM1">*</asp:RequiredFieldValidator></td>
                                <td align="left"  style="vertical-align:middle">
                                    Overseas Address Country</td>
                               
                                <td align="left"  >
                                    <asp:DropDownList ID="ddlFPRM_Country" runat="server" CssClass="form-control">
                                    </asp:DropDownList></td>
                            </tr>
                            <tr>
                                <td align="left" style="vertical-align:middle">  
                                    Permanent Phone<span id="SpFPphNo" runat="server" style="color:Red;" visible="false" >*</span></td>
                              
                                <td align="left"  >
                                    <asp:TextBox ID="txtFPPRM_Country" runat="server" Width="60px" MaxLength="3" CssClass="form-control"></asp:TextBox>
                                    -
                                    <asp:TextBox ID="txtFPPRM_Area" runat="server" Width="60px" MaxLength="4" CssClass="form-control"></asp:TextBox>
                                    -
                                    <asp:TextBox ID="txtFPPRM_No" runat="server" Width="90px" MaxLength="10" CssClass="form-control"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvFPphNo" runat="server" ControlToValidate="txtFPPRM_No"
                                         ErrorMessage="Father's permanent phone number required" ForeColor="red" ValidationGroup="groupM1">*</asp:RequiredFieldValidator>
                                    <div class="remark">(Country-Area-Number)</div>
                                     <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender13" runat="server" 
                                     TargetControlID="txtFPPRM_Country"
                                            FilterType="Numbers">
                                        </ajaxToolkit:FilteredTextBoxExtender>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender14" runat="server"
                                         TargetControlID="txtFPPRM_Area"
                                            FilterType="Numbers">
                                        </ajaxToolkit:FilteredTextBoxExtender>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender15" 
                                        runat="server" TargetControlID="txtFPPRM_No"
                                            FilterType="Numbers">
                                        </ajaxToolkit:FilteredTextBoxExtender> 
                                    </td>
                                <td align="left" style="vertical-align:middle"> 
                                    P.O BOX</td>
                          
                                <td align="left" style="vertical-align:middle">
                                    <asp:TextBox ID="txtFPRMPOBOX" runat="server" Width="86px" MaxLength="20"
                                    CssClass="form-control"></asp:TextBox>
                                       
                                </td>
                            </tr>
                            
                            <tr>
                                <td align="left" style="vertical-align:middle"> 
                                    Occupation<span id="SpFOcc" runat="server" style="color:Red;" visible="false" >*</span></td>
                               
                                <td align="left"  >
                                    <asp:TextBox ID="txtFOcc" runat="server" CssClass="form-control"></asp:TextBox>
                                     <asp:RequiredFieldValidator ID="rfvFOcc" runat="server" ControlToValidate="txtFOcc"
                                         ErrorMessage="Occupation required" ForeColor="red" ValidationGroup="groupM1">*</asp:RequiredFieldValidator>
                                    
                                    </td>
                                <td align="left" style="vertical-align:middle">  
                                    Company Name</td>
                               
                                <td align="left" >
                                    <asp:DropDownList id="ddlFCompany_Name" runat="server" CssClass="form-control">
                                    </asp:DropDownList><br />
                                    <div class="remark">(If you choose other,please specify the Company Name)</div>
                                    <asp:TextBox ID="txtFComp_Name" runat="server" Width="205px" CssClass="form-control"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td align="left" style="vertical-align:middle"> 
                                    Company Address</td>
                               
                                <td align="left"  >
                                    <asp:TextBox ID="txtFComp_Add" runat="server" CssClass="form-control"></asp:TextBox></td>
                                <td align="left" style="vertical-align:middle">  
                                    Email<span id="SpFEmail" runat="server" style="color:Red;" visible="false" >*</span></td>
                            
                                <td align="left" >
                                    <asp:TextBox ID="txtFEmail" runat="server" CssClass="form-control"></asp:TextBox>
                                      <asp:RegularExpressionValidator ID="RegularExpressionValidator18" runat="server" ControlToValidate="txtFEmail"
                            Display="Dynamic" ErrorMessage="Enter valid email address"
                            ValidationExpression="^([0-9a-zA-Z]([-\.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$"
                            ValidationGroup="groupM1">*</asp:RegularExpressionValidator>
                             <asp:RequiredFieldValidator ID="rfvFEmail" runat="server" ControlToValidate="txtFEmail"
                                        ErrorMessage="Father's email address required" ForeColor="red" ValidationGroup="groupM1">*</asp:RequiredFieldValidator>
                                    </td>
                            </tr>
                              
                        
                           
                              <tr class="tdblankAll">
                           <td   colspan="4" 
                           class="tdblankAll" 
                             >&nbsp;</td></tr>
                       <tr class="tdblankAll">
                           <td   colspan="4" 
                            class="tdblankAll"
                             >&nbsp;</td></tr>
                       
                       <tr>
                                           <td  colspan="4" class="sub-heading">
                                      Mother Details<span style="float:right;"><asp:CheckBox ID="chkCopyF_Details" runat="server"  CssClass="checkboxHeader" Text="Copy Father Details" AutoPostBack="true" /></span>    
                                  </td>
                            
                            </tr>
                              <tr>
                                <td align="left"  style="vertical-align:middle">
                                  
                                    Full Name As Per Passport</td>
                               
                                <td align="left"  colspan="4">
                                    <asp:TextBox ID="txtMFirstName" runat="server"  CssClass="form-control" maxlength="100"></asp:TextBox>
                                    <asp:TextBox ID="txtMMidName" runat="server" CssClass="form-control" maxlength="100"></asp:TextBox>
                                    <asp:TextBox ID="txtMLastName" runat="server" CssClass="form-control" maxlength="100"></asp:TextBox>
                                    <ajaxToolkit:FilteredTextBoxExtender ID="fbeMFname" runat="server"
                                                           TargetControlID="txtMFirstName" 
                                                           FilterType="Custom,UppercaseLetters,Numbers,LowercaseLetters"
                                                            ValidChars="\.'/ "></ajaxToolkit:FilteredTextBoxExtender>
                             <ajaxToolkit:FilteredTextBoxExtender ID="fbeMMname" runat="server"
                                                           TargetControlID="txtMMidName" 
                                                           FilterType="Custom,UppercaseLetters,Numbers,LowercaseLetters"
                                                            ValidChars="\.'/ "></ajaxToolkit:FilteredTextBoxExtender>
                                    <ajaxToolkit:FilteredTextBoxExtender ID="fbeMLname" runat="server"
                                                           TargetControlID="txtMLastName" 
                                                           FilterType="Custom,UppercaseLetters,Numbers,LowercaseLetters"
                                                            ValidChars="\.'/ "></ajaxToolkit:FilteredTextBoxExtender>
                                    
                                    
                                    
                                    </td>
                            </tr>
                              <tr><td align="left" style="vertical-align:middle">Emirates ID</td> <td align="left" style="vertical-align:middle" >
  <asp:TextBox ID="txtMEMIRATES_ID" runat="server" Width="146px" CssClass="form-control"></asp:TextBox></td>
  <td align="left" style="vertical-align:middle">Emirates ID Expiry Date
                                  </td>
                                
                                <td align="left">
                                   <asp:TextBox ID="txtMEMIRATES_ID_EXPDATE" runat="server" Width="110px" CssClass="form-control" MaxLength="11"
                                TabIndex="32">
                            </asp:TextBox>&nbsp;<asp:ImageButton ID="imgBtnMEMIRATES_IDExp_date" runat="server" ImageUrl="~/Images/Calendar.png"  CssClass="fa fa-calendar" />
                                                        <ajaxToolkit:FilteredTextBoxExtender ID="ftbMEmirates_ExpDt" runat="server" TargetControlID="txtMEMIRATES_ID_EXPDATE"
                                FilterType="Custom,UppercaseLetters,LowercaseLetters,Numbers" ValidChars="/">
                            </ajaxToolkit:FilteredTextBoxExtender>
                            <ajaxToolkit:CalendarExtender ID="CeMemirates_expdt" runat="server" Format="dd/MMM/yyyy"
                                PopupButtonID="imgBtnMEMIRATES_IDExp_date" TargetControlID="txtMEMIRATES_ID_EXPDATE">
                            </ajaxToolkit:CalendarExtender>
                             <asp:RegularExpressionValidator ID="revMEmir_Exp_date"
                                            runat="server" ControlToValidate="txtMEMIRATES_ID_EXPDATE" Display="Dynamic" EnableViewState="False"
                                            ErrorMessage="Enter mother's Emirates ID expiry  Date in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                            ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                            ValidationGroup="groupM1">*</asp:RegularExpressionValidator><div class="remark">(dd/mmm/yyyy)</div>
                                </td></tr>  
                            
                            
                            <tr>
                                <td align="left" style="vertical-align:middle" >
                                    Nationality1</td>
                              
                                <td align="left"  >
                                    <asp:DropDownList ID="ddlMNationality1" runat="server" CssClass="form-control">
                                    </asp:DropDownList></td>
                                <td align="left"  style="vertical-align:middle">
                                    Nationality2</td>
                               
                                <td align="left"  >
                                    <asp:DropDownList ID="ddlMNationality2" runat="server" CssClass="form-control">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                             <%--Residential Address Street Was Here--%>
                                 <td align="left" style="vertical-align:middle">  
                                    Residential Address Country</td>
                            
                                <td align="left" style="vertical-align:middle" >
                                    <asp:DropDownList ID="ddlMCOMCountry" runat="server" CssClass="form-control" AutoPostBack="true">
                                    </asp:DropDownList>
                                    </td>
                                    
                                <td align="left" style="vertical-align:middle"> 
                                    Residential Address City/State <span id="SpMRaddCity" runat="server" style="color:Red;" visible="false" >*</span></td>
                                                             <td align="left"  >
                                                             
                                                              <div class="tdPadDiv">
                                                City/State</div><asp:DropDownList ID="ddlMCity" runat="server" CssClass="form-control" AutoPostBack="true">
                                            </asp:DropDownList>
                                            
                                             <asp:RequiredFieldValidator ID="rfvMRaddCity" runat="server" ControlToValidate="ddlMCity"
                                         ErrorMessage="Mother's residential addresscity/state required" ForeColor="red" ValidationGroup="groupM1">*</asp:RequiredFieldValidator>
                                         
                                            <div class="remark_info" style="padding-top: 4px; padding-bottom: 4px;">
                                                If you choose other,please specify the City/State Name</div>
                                            <asp:TextBox ID="txtMCOMCity" runat="server" MaxLength="50" Width="180px" CssClass="form-control">
                                            </asp:TextBox>
                                    </td>
                           
                               <%--Residential Address Area Was Here--%>  
                                
                            </tr>
                            <tr>
                              <%--Residential Address Buiding Was Here--%>  
                                <td align="left" style="vertical-align:middle">  
                                    Residential Address Area<span id="SpMRaddA" runat="server" style="color:Red;" visible="false" >*</span></td>
                            
                                <td align="left" >
                                 <asp:DropDownList ID="ddlMArea" runat="server" CssClass="form-control">
                                            </asp:DropDownList>
                                              <asp:RequiredFieldValidator ID="rfvMRaddA" runat="server" ControlToValidate="ddlMArea"
                                         ErrorMessage="Mother's residential address area required" ForeColor="red" ValidationGroup="groupM1">*</asp:RequiredFieldValidator>
                                         
                                            <div class="remark_info" style="padding-top: 4px; padding-bottom: 4px;">
                                                If you choose other,please specify the Area Name</div>
                                    <asp:TextBox ID="txtMCOMADDR2" runat="server" CssClass="form-control"></asp:TextBox>
                                    
                                    </td>
                                    
                                     <td align="left" style="vertical-align:middle">  
                                    Residential Address Street<span id="SpMRaddS" runat="server" style="color:Red;" visible="false" >*</span></td>
                                    
                                    <td align="left" style="vertical-align:middle"  >
                                    <asp:TextBox ID="txtMCOMADDR1" runat="server" CssClass="form-control"></asp:TextBox>
                                     <asp:RequiredFieldValidator ID="rfvMRaddS" runat="server" ControlToValidate="txtMCOMADDR1"
                                         ErrorMessage="Mother's residential address street required" ForeColor="red" ValidationGroup="groupM1">*</asp:RequiredFieldValidator></td>


                               
                            </tr>
                            
                             <tr>
                             <%--Residential Address City Was Here--%>
                               <td align="left" style="vertical-align:middle">  
                                    Residential Address Building<span id="SpMRaddB" runat="server" style="color:Red;" visible="false" >*</span></td>
                               
                                <td align="left"  >
                                    <asp:TextBox ID="txtMBldg" runat="server" CssClass="form-control"> </asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvMRaddB" runat="server" ControlToValidate="txtMBldg"
                                         ErrorMessage="Mother's residential address building required" ForeColor="red" ValidationGroup="groupM1">*</asp:RequiredFieldValidator>
                                    </td>
                                      <%--Residential Address Country Was Here--%>
                                       <td align="left" style="vertical-align:middle"> 
                                    Residential Address Apartment No.<span id="SpMRaddN" runat="server" style="color:Red;" visible="false" >*</span></td>
                                
                                <td align="left" >
                                    <asp:TextBox ID="txtMAptNo" runat="server" CssClass="form-control"> </asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvMRaddN" runat="server" ControlToValidate="txtMAptNo"
                                         ErrorMessage="Mother's residential address apartment No. required" ForeColor="red" ValidationGroup="groupM1">*</asp:RequiredFieldValidator></td>
                                                                    </tr>
                            <tr>
                                <td align="left" style="vertical-align:middle">  
                                    P.O BOX<span id="MpoBox" runat="server" style="color:Red;" visible="false" >*</span></td>
                               
                                <td align="left"  >
                                    <asp:TextBox ID="txtMCOMPOBOX" runat="server" Width="86px" MaxLength="20"
                                    CssClass="form-control"></asp:TextBox>
                                   <asp:RequiredFieldValidator ID="rfvMcompobox" runat="server" ControlToValidate="txtMCOMPOBOX"
                                    Display="Dynamic" ErrorMessage="Please enter Mother P.O BOX" ValidationGroup="groupM1" >*</asp:RequiredFieldValidator>  
 
                                   </td>
                                <td align="left" style="vertical-align:middle">  
                                    Emirate</td>
                               
                                <td align="left" >
                                    <asp:DropDownList ID="ddlMCOMEmirate" runat="server" CssClass="form-control">
                                    </asp:DropDownList>
                                     </td>
                            </tr>
                           
                            <tr>
                                <td align="left"  style="vertical-align:middle">
                                    Home Phone</td>
                             
                                <td align="left" >
                                
                                    <asp:TextBox ID="txtMResPhone_Country" runat="server" Width="60px" MaxLength="3" CssClass="form-control"></asp:TextBox>
                                    -
                                    <asp:TextBox ID="txtMResPhone_Area" runat="server" Width="60px" MaxLength="4" CssClass="form-control"></asp:TextBox>
                                    -
                                    <asp:TextBox ID="txtMResPhone_No" runat="server" Width="90px" MaxLength="10" CssClass="form-control"></asp:TextBox>
                                    <div class="remark">(Country-Area-Number)</div>
                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender16" runat="server" 
                                    TargetControlID="txtMResPhone_Country"
                                            FilterType="Numbers">
                                        </ajaxToolkit:FilteredTextBoxExtender>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender17" runat="server" 
                                        TargetControlID="txtMResPhone_Area"
                                            FilterType="Numbers">
                                        </ajaxToolkit:FilteredTextBoxExtender>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender18" runat="server" 
                                        TargetControlID="txtMResPhone_No"
                                            FilterType="Numbers">
                                        </ajaxToolkit:FilteredTextBoxExtender>    

                                    </td>
                                <td align="left"  style="vertical-align:middle">
                                    Office Phone</td>
                               
                                <td align="left">
                                    <asp:TextBox ID="txtMOffPhone_Country" runat="server" Width="60px" MaxLength="3" CssClass="form-control"></asp:TextBox>
                                    -
                                    <asp:TextBox ID="txtMOffPhone_Area" runat="server" Width="60px" MaxLength="4" CssClass="form-control"></asp:TextBox>
                                    -
                                    <asp:TextBox ID="txtMOffPhone_No" runat="server" Width="90px" MaxLength="10" CssClass="form-control"></asp:TextBox>
                                    <div class="remark">(Country-Area-Number)</div>
                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender19" runat="server" 
                                    TargetControlID="txtMOffPhone_Country"
                                            FilterType="Numbers">
                                        </ajaxToolkit:FilteredTextBoxExtender>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender20" runat="server" 
                                        TargetControlID="txtMOffPhone_Area"
                                            FilterType="Numbers">
                                        </ajaxToolkit:FilteredTextBoxExtender>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender21" runat="server" 
                                        TargetControlID="txtMOffPhone_No"
                                            FilterType="Numbers">
                                        </ajaxToolkit:FilteredTextBoxExtender>     

                                    </td>
                            </tr>
                            <tr>
                                <td align="left" style="vertical-align:middle">  
                                    Mobile<span id="SpMMobNo" runat="server" style="color:Red;" visible="false" >*</span></td>
                                
                                <td align="left"  >
                                                                    
                                    <asp:TextBox ID="txtMMobile_Country" runat="server" Width="60px" MaxLength="3" CssClass="form-control"></asp:TextBox>
                                    -
                                    <asp:TextBox ID="txtMMobile_Area" runat="server" Width="60px" MaxLength="4" CssClass="form-control"></asp:TextBox>
                                    -
                                    <asp:TextBox ID="txtMMobile_No" runat="server" Width="90px" MaxLength="10" CssClass="form-control"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvMMobNo" runat="server" ControlToValidate="txtMMobile_No"
                                    Display="Dynamic" ErrorMessage="Mother's mobile no required" ValidationGroup="groupM1" >*</asp:RequiredFieldValidator>  

                                    <div class="remark">(Country-Area-Number)</div>
                                   <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender22" runat="server" 
                                    TargetControlID="txtMMobile_Country"
                                            FilterType="Numbers">
                                        </ajaxToolkit:FilteredTextBoxExtender>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender23" runat="server" 
                                        TargetControlID="txtMMobile_Area"
                                            FilterType="Numbers">
                                        </ajaxToolkit:FilteredTextBoxExtender>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender24" runat="server" 
                                        TargetControlID="txtMMobile_No"
                                            FilterType="Numbers">
                                        </ajaxToolkit:FilteredTextBoxExtender>   
                                    </td>
                                <td align="left" style="vertical-align:middle"> 
                                    Fax No</td>
                               
                                <td align="left" >
                                    <asp:TextBox ID="txtMFax_Country" runat="server" Width="60px" MaxLength="3" CssClass="form-control"></asp:TextBox>
                                    -
                                    <asp:TextBox ID="txtMFax_Area" runat="server" Width="60px" MaxLength="4" CssClass="form-control"></asp:TextBox>
                                    -
                                    <asp:TextBox ID="txtMFax_No" runat="server" Width="90px" MaxLength="10" CssClass="form-control"></asp:TextBox><br />
                                    <div class="remark">(Country-Area-Number)</div>
                                   <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender25" runat="server" 
                                    TargetControlID="txtMFax_Country"
                                            FilterType="Numbers">
                                        </ajaxToolkit:FilteredTextBoxExtender>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender26" runat="server" 
                                        TargetControlID="txtMFax_Area"
                                            FilterType="Numbers">
                                        </ajaxToolkit:FilteredTextBoxExtender>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender27" runat="server" 
                                        TargetControlID="txtMFax_No"
                                            FilterType="Numbers">
                                        </ajaxToolkit:FilteredTextBoxExtender>   
                                    </td>
                            </tr>
                            <tr>
                                <td align="left" style="vertical-align:middle">  
                                    Overseas Address Line 1<span id="SpMPAddL1" runat="server" style="color:Red;" visible="false" >*</span></td>
                              
                                <td align="left"  >
                                    <asp:TextBox ID="txtMPRMAddr1" runat="server" CssClass="form-control"></asp:TextBox> <asp:RequiredFieldValidator
                                     ID="rfvMPAddL1" runat="server" ControlToValidate="txtMPRMAddr1"
                                         ErrorMessage="Mother's Required overseas address Line 1" ForeColor="red" ValidationGroup="groupM1">
*</asp:RequiredFieldValidator>
                                    </td>
                                <td align="left" style="vertical-align:middle">  
                                    Overseas Address Line 2<span id="SpMPAddL2" runat="server" style="color:Red;" visible="false" >*</span></td>
                              
                                <td align="left" >
                                    <asp:TextBox ID="txtMPRMAddr2" runat="server" CssClass="form-control"></asp:TextBox><asp:RequiredFieldValidator 
                                    ID="rfvMPAddL2" runat="server" ControlToValidate="txtMPRMAddr2"
                                         ErrorMessage="Mother's required overseas address Line 2" ForeColor="red" ValidationGroup="groupM1">*</asp:RequiredFieldValidator></td>
                            </tr>
                            <tr>
                                <td align="left" style="vertical-align:middle">
                                    Overseas Address City/State<span id="SpMPAddCity" runat="server" style="color:Red;" visible="false" >*</span></td>
                              
                                <td align="left" style="vertical-align:middle" >
                                    <asp:TextBox ID="txtMPRM_City" runat="server" CssClass="form-control"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvMPAddCity" runat="server" ControlToValidate="txtMPRM_City"
                                         ErrorMessage="Mother's overseas address city/state required" ForeColor="red" ValidationGroup="groupM1">*</asp:RequiredFieldValidator></td>
                                <td align="left" style="vertical-align:middle" >
                                    Overseas Address Country</td>
                                
                                <td align="left" >
                                    <asp:DropDownList ID="ddlMPRM_Country" runat="server" CssClass="form-control">
                                    </asp:DropDownList></td>
                            </tr>
                            <tr>
                                <td align="left" style="vertical-align:middle">  
                                    Permanent Phone<span id="SpMPphNo" runat="server" style="color:Red;" visible="false" >*</span></td>
                              
                                <td align="left"  >
                                    <asp:TextBox ID="txtMPPermant_Country" runat="server" Width="60px" MaxLength="3" CssClass="form-control"></asp:TextBox>
                                    -
                                    <asp:TextBox ID="txtMPPermant_Area" runat="server" Width="60px" MaxLength="4" CssClass="form-control"></asp:TextBox>
                                    -
                                    <asp:TextBox ID="txtMPPermant_No" runat="server" Width="90px" MaxLength="10" CssClass="form-control"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvMPphNo" runat="server" ControlToValidate="txtMPPermant_No"
                                         ErrorMessage="Mother's permanent phone number required" ForeColor="red" ValidationGroup="groupM1">*</asp:RequiredFieldValidator><br />
                                    <div class="remark">(Country-Area-Number)</div>
                                    
                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender28" runat="server" 
                                    TargetControlID="txtMPPermant_Country"
                                            FilterType="Numbers">
                                        </ajaxToolkit:FilteredTextBoxExtender>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender29" runat="server" 
                                        TargetControlID="txtMPPermant_Area"
                                            FilterType="Numbers">
                                        </ajaxToolkit:FilteredTextBoxExtender>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender30" runat="server" 
                                        TargetControlID="txtMPPermant_No"
                                            FilterType="Numbers">
                                        </ajaxToolkit:FilteredTextBoxExtender>   

                                    </td>
                                <td align="left" style="vertical-align:middle">  
                                    P.O BOX</td>
                               
                                <td align="left" style="vertical-align:middle" >
                                    <asp:TextBox ID="txtMPRMPOBOX" runat="server" Width="86px"
                                     CssClass="form-control" MaxLength="20"></asp:TextBox>
                                   
                                    </td>
                            </tr>
                      
                            <tr>
                                <td align="left" style="vertical-align:middle">  
                                    Occupation<span id="SpMOcc" runat="server" style="color:Red;" visible="false" >*</span></td>
                              
                                <td align="left"  style="vertical-align:middle" >
                                    <asp:TextBox ID="txtMOcc" runat="server" CssClass="form-control"></asp:TextBox><asp:RequiredFieldValidator ID="rfvMOcc" 
                                    runat="server" ControlToValidate="txtMOcc"
                                         ErrorMessage="Mother's occupation required" ForeColor="red" ValidationGroup="groupM1">*</asp:RequiredFieldValidator></td>
                                <td align="left" style="vertical-align:middle">  
                                    Company Name</td>
                              
                                <td align="left" >
                                    <asp:DropDownList id="ddlMCompany_Name" runat="server" CssClass="form-control">
                                    </asp:DropDownList><br />
                                    <div class="remark">(If you choose other,please specify the Company Name)</div>
                                    <asp:TextBox ID="txtMComp_Name" runat="server" Width="205px" CssClass="form-control"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td align="left" style="vertical-align:middle">  
                                    Company Address</td>
                                
                                <td align="left"  >
                                    <asp:TextBox ID="txtMComp_Add" runat="server" CssClass="form-control"></asp:TextBox></td>
                                <td align="left" style="vertical-align:middle">  
                                    Email<span id="SpMEmail" runat="server" style="color:Red;" visible="false" >*</span></td>
                              
                                <td align="left" >
                                    <asp:TextBox ID="txtMEmail" runat="server" CssClass="form-control"></asp:TextBox> <asp:RequiredFieldValidator
                                     ID="rfvMEmail" runat="server" ControlToValidate="txtMEmail"
                                        ErrorMessage="Mother's email address required" ForeColor="red" ValidationGroup="groupM1">*</asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator45" runat="server" ControlToValidate="txtMEmail"
                            Display="Dynamic" ErrorMessage="Enter valid email address"
                            ValidationExpression="^([0-9a-zA-Z]([-\.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$"
                            ValidationGroup="groupM1">*</asp:RegularExpressionValidator>
                                    </td>
                            </tr>  
                              
                                  
                              <tr class="tdblankAll">
                           <td   colspan="4" 
                           class="tdblankAll" 
                             >&nbsp;</td></tr>
                       <tr class="tdblankAll">
                           <td   colspan="4" 
                            class="tdblankAll"
                             >&nbsp;</td></tr>
                       
                       <tr>
                                           <td  colspan="4" class="sub-heading">
                         Guardian Details
                             </td></tr>
                                  <!-- <asp:CheckBox ID="chkCopyF_Details_Guard" runat="server" Text="Copy Father Details" onclick="javascript:return copyFathertoGuardian(this);"/>
                                    <asp:CheckBox ID="chkCopyM_Details_Guard" runat="server" Text="Copy Mother Details" onclick="javascript:return copyMothertoGuardian(this);"/>-->
                            
                             <tr>
                                <td align="left" style="vertical-align:middle" >
                                  
                                    Full Name As Per Passport</td>
                              
                                <td align="left"  colspan="4">
                                    <asp:TextBox ID="txtGFirstName" runat="server" Width="162px" CssClass="form-control"></asp:TextBox>
                                    <asp:TextBox ID="txtGMidName" runat="server" Width="162px" CssClass="form-control"></asp:TextBox>
                                    <asp:TextBox ID="txtGLastName" runat="server" Width="162px" CssClass="form-control"></asp:TextBox>
                                   <ajaxToolkit:FilteredTextBoxExtender ID="fbeGFname" runat="server"
                                                           TargetControlID="txtGFirstName" 
                                                           FilterType="Custom,UppercaseLetters,Numbers,LowercaseLetters"
                                                            ValidChars="\.'/ "></ajaxToolkit:FilteredTextBoxExtender>
                             <ajaxToolkit:FilteredTextBoxExtender ID="fbeGMname" runat="server"
                                                           TargetControlID="txtGMidName" 
                                                           FilterType="Custom,UppercaseLetters,Numbers,LowercaseLetters"
                                                            ValidChars="\.'/ "></ajaxToolkit:FilteredTextBoxExtender>
                                    <ajaxToolkit:FilteredTextBoxExtender ID="fbeGLname" runat="server"
                                                           TargetControlID="txtGLastName" 
                                                           FilterType="Custom,UppercaseLetters,Numbers,LowercaseLetters"
                                                            ValidChars="\.'/ "></ajaxToolkit:FilteredTextBoxExtender>
                                                                                    
                                    </td>
                            </tr>
                            
                            <tr><td align="left" style="vertical-align:middle">Emirates ID</td> <td align="left" style="vertical-align:middle" >
  <asp:TextBox ID="txtGEMIRATES_ID" runat="server" Width="146px" CssClass="form-control"></asp:TextBox></td>
  <td align="left" style="vertical-align:middle">Emirates ID Expiry Date
                                  </td>
                                
                                <td align="left">
                                   <asp:TextBox ID="txtGEMIRATES_ID_EXPDATE" runat="server" Width="110px" CssClass="form-control" MaxLength="11"
                                TabIndex="32">
                            </asp:TextBox>&nbsp;<asp:ImageButton ID="imgBtnGEMIRATES_IDExp_date" runat="server" ImageUrl="~/Images/Calendar.png"  CssClass="imgCal" />
                                                        <ajaxToolkit:FilteredTextBoxExtender ID="ftbGEmirate_ExpDt" runat="server" TargetControlID="txtGEMIRATES_ID_EXPDATE"
                                FilterType="Custom,UppercaseLetters,LowercaseLetters,Numbers" ValidChars="/">
                            </ajaxToolkit:FilteredTextBoxExtender>
                            <ajaxToolkit:CalendarExtender ID="CeGEmirate_ExpDt" runat="server" Format="dd/MMM/yyyy"
                                PopupButtonID="imgBtnGEMIRATES_IDExp_date" TargetControlID="txtGEMIRATES_ID_EXPDATE">
                            </ajaxToolkit:CalendarExtender>
                             <asp:RegularExpressionValidator ID="revGEmir_Exp_date"
                                            runat="server" ControlToValidate="txtGEMIRATES_ID_EXPDATE" Display="Dynamic" EnableViewState="False"
                                            ErrorMessage="Enter guardian's Emirates ID expiry  Date in given format dd/mmm/yyyy e.g.  21/Sep/2007 or 21/09/2007"
                                            ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                            ValidationGroup="groupM1">*</asp:RegularExpressionValidator><div class="remark">(dd/mmm/yyyy)</div>
                                </td></tr> 
                            
                            <tr>
                                <td align="left" style="vertical-align:middle" >
                                    Nationality1</td>
                               
                                <td align="left"  >
                                    <asp:DropDownList ID="ddlGNationality1" runat="server" width="200px" CssClass="form-control">
                                    </asp:DropDownList></td>
                                <td align="left"  style="vertical-align:middle">
                                    Nationality2</td>
                               
                                <td align="left"  >
                                    <asp:DropDownList ID="ddlGNationality2" runat="server" width="200px" CssClass="form-control">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                             <tr>
                             <%--Residential Address Street Was Here--%>
                                 <td align="left" style="vertical-align:middle">  
                                    Residential Address Country</td>
                            
                                <td align="left" style="vertical-align:middle" >
                                    <asp:DropDownList ID="ddlGCOMCountry" runat="server" CssClass="form-control" AutoPostBack="true">
                                    </asp:DropDownList>
                                    </td>
                                    
                                <td align="left" style="vertical-align:middle"> 
                                    Residential Address City/State <span id="SpGRaddCity" runat="server" style="color:Red;" visible="false" >*</span></td>
                                                             <td align="left"  >
                                                             
                                                              <div class="tdPadDiv">
                                                City/State</div><asp:DropDownList ID="ddlGCity" runat="server" CssClass="form-control" AutoPostBack="true">
                                            </asp:DropDownList>
                                            
                                             <asp:RequiredFieldValidator ID="rfvGRaddCity" runat="server" ControlToValidate="ddlGCity"
                                         ErrorMessage="Guardian's residential addresscity/state required" ForeColor="red" ValidationGroup="groupM1">*</asp:RequiredFieldValidator>
                                         
                                            <div class="remark_info" style="padding-top: 4px; padding-bottom: 4px;">
                                                If you choose other,please specify the City/State Name</div>
                                            <asp:TextBox ID="txtGCOMCity" runat="server" MaxLength="50" Width="180px" CssClass="form-control">
                                            </asp:TextBox>
                                    </td>
                           
                               <%--Residential Address Area Was Here--%>  
                                
                            </tr>
                            <tr>
                              <%--Residential Address Buiding Was Here--%>  
                                <td align="left" style="vertical-align:middle">  
                                    Residential Address Area<span id="SpGRaddA" runat="server" style="color:Red;" visible="false" >*</span></td>
                            
                                <td align="left" >
                                 <asp:DropDownList ID="ddlGArea" runat="server" CssClass="form-control">
                                            </asp:DropDownList>
                                              <asp:RequiredFieldValidator ID="rfvGRaddA" runat="server" ControlToValidate="ddlGArea"
                                         ErrorMessage="Guardian's residential address area required" ForeColor="red" ValidationGroup="groupM1">*</asp:RequiredFieldValidator>
                                         
                                            <div class="remark_info" style="padding-top: 4px; padding-bottom: 4px;">
                                                If you choose other,please specify the Area Name</div>
                                    <asp:TextBox ID="txtGCOMADDR2" runat="server" CssClass="form-control"></asp:TextBox>
                                    
                                    </td>
                                    
                                     <td align="left" style="vertical-align:middle">  
                                    Residential Address Street<span id="SpGRaddS" runat="server" style="color:Red;" visible="false" >*</span></td>
                                    
                                    <td align="left"  style="vertical-align:middle" >
                                    <asp:TextBox ID="txtGCOMADDR1" runat="server" CssClass="form-control"></asp:TextBox>
                                     <asp:RequiredFieldValidator ID="rfvGRaddS" runat="server" ControlToValidate="txtGCOMADDR1"
                                         ErrorMessage="Mother's residential address street required" ForeColor="red" ValidationGroup="groupM1">*</asp:RequiredFieldValidator></td>


                               
                            </tr>
                            
                             <tr>
                             <%--Residential Address City Was Here--%>
                               <td align="left" style="vertical-align:middle">  
                                    Residential Address Building<span id="SpGRaddB" runat="server" style="color:Red;" visible="false" >*</span></td>
                               
                                <td align="left"  >
                                    <asp:TextBox ID="txtGBldg" runat="server" CssClass="form-control"> </asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvGRaddB" runat="server" ControlToValidate="txtGBldg"
                                         ErrorMessage="Mother's residential address building required" ForeColor="red" ValidationGroup="groupM1">*</asp:RequiredFieldValidator>
                                    </td>
                                      <%--Residential Address Country Was Here--%>
                                       <td align="left" style="vertical-align:middle"> 
                                    Residential Address Apartment No.<span id="SpGRaddN" runat="server" style="color:Red;" visible="false" >*</span></td>
                                
                                <td align="left" >
                                    <asp:TextBox ID="txtGAptNo" runat="server" CssClass="form-control"> </asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvGRaddN" runat="server" ControlToValidate="txtGAptNo"
                                         ErrorMessage="Mother's residential address apartment No. required" ForeColor="red" ValidationGroup="groupM1">*</asp:RequiredFieldValidator></td>
                                                                    </tr>
                            <tr>
                                <td align="left" style="vertical-align:middle"> 
                                    P.O BOX<span id="Gpobox" runat="server" style="color:Red;" visible="false" >*</span></td>
                            
                                <td align="left"  >
                                    <asp:TextBox ID="txtGCOMPOBOX" runat="server"  CssClass="form-control" 
                                    MaxLength="20"></asp:TextBox>
                               <asp:RequiredFieldValidator ID="rfvGcompobox" runat="server" 
                               ControlToValidate="txtGCOMPOBOX"
                                    Display="Dynamic" ErrorMessage="Please enter Guardian P.O BOX" ValidationGroup="groupM1" >*</asp:RequiredFieldValidator>  
                                    </td>
                                <td align="left" style="vertical-align:middle"> 
                                    Emirate</td>
                               
                                <td align="left" >
                                    <asp:DropDownList ID="ddlGCOMEmirate" runat="server" CssClass="form-control">
                                    </asp:DropDownList>
                                    
                                    </td>
                            </tr>
                           
                            <tr>
                                <td align="left" style="vertical-align:middle" >
                                    Home Phone</td>

                                <td align="left" >
                                
                                    <asp:TextBox ID="txtGResPhone_Country" runat="server" Width="60px" MaxLength="3" CssClass="form-control"></asp:TextBox>
                                    -
                                    <asp:TextBox ID="txtGResPhone_Area" runat="server" Width="60px" MaxLength="4" CssClass="form-control"></asp:TextBox>
                                    -
                                    <asp:TextBox ID="txtGResPhone_No" runat="server" Width="90px" 
                                    MaxLength="10" CssClass="form-control"></asp:TextBox>
                                    <div class="remark">(Country-Area-Number)</div>
                                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender31" runat="server" TargetControlID="txtGResPhone_Country"
                                            FilterType="Numbers">
                                        </ajaxToolkit:FilteredTextBoxExtender>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender32" runat="server" TargetControlID="txtGResPhone_Area"
                                            FilterType="Numbers">
                                        </ajaxToolkit:FilteredTextBoxExtender>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender33" runat="server" TargetControlID="txtGResPhone_No"
                                            FilterType="Numbers">
                                        </ajaxToolkit:FilteredTextBoxExtender>  
                                    </td>
                                <td align="left" style="vertical-align:middle" >
                                    Office Phone</td>
                               
                                <td align="left"  >
                                    <asp:TextBox ID="txtGOffPhone_Country" runat="server" Width="60px" MaxLength="3" CssClass="form-control"></asp:TextBox>
                                    -
                                    <asp:TextBox ID="txtGOffPhone_Area" runat="server" Width="60px" MaxLength="4" CssClass="form-control"></asp:TextBox>
                                    -
                                    <asp:TextBox ID="txtGOffPhone_No" runat="server" Width="90px" MaxLength="10" CssClass="form-control"></asp:TextBox><br />
                                    <div class="remark">(Country-Area-Number)</div>
                                     <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender34" runat="server" 
                                     TargetControlID="txtGOffPhone_Country"
                                            FilterType="Numbers">
                                        </ajaxToolkit:FilteredTextBoxExtender>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender35" runat="server" 
                                        TargetControlID="txtGOffPhone_Area"
                                            FilterType="Numbers">
                                        </ajaxToolkit:FilteredTextBoxExtender>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender36" runat="server"
                                         TargetControlID="txtGOffPhone_No"
                                            FilterType="Numbers">
                                        </ajaxToolkit:FilteredTextBoxExtender>  
                                    </td>
                            </tr>
                            <tr>
                                <td align="left" style="vertical-align:middle">  
                                    Mobile<span id="SpGMobNo" runat="server" style="color:Red;" visible="false" >*</span></td>
                                
                                <td align="left"  >
                                                                    
                                    <asp:TextBox ID="txtGMobile_Country" runat="server" Width="60px" MaxLength="3" CssClass="form-control"></asp:TextBox>
                                    -
                                    <asp:TextBox ID="txtGMobile_Area" runat="server" Width="60px" MaxLength="4" CssClass="form-control"></asp:TextBox>
                                    -
                                    <asp:TextBox ID="txtGMobile_No" runat="server" Width="90px" MaxLength="10" CssClass="form-control">
                                    </asp:TextBox> <asp:RequiredFieldValidator ID="rfvGMobNo" runat="server" ControlToValidate="txtGMobile_No"
                                    Display="Dynamic" ErrorMessage="Guardian's mobile no required" ValidationGroup="groupM1" >*</asp:RequiredFieldValidator>  
                                   <div class="remark">(Country-Area-Number)</div>
                                     <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender37" runat="server" 
                                     TargetControlID="txtGMobile_Country"
                                            FilterType="Numbers">
                                        </ajaxToolkit:FilteredTextBoxExtender>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender38" runat="server" 
                                        TargetControlID="txtGMobile_Area"
                                            FilterType="Numbers">
                                        </ajaxToolkit:FilteredTextBoxExtender>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender39" runat="server"
                                         TargetControlID="txtGMobile_No"
                                            FilterType="Numbers">
                                        </ajaxToolkit:FilteredTextBoxExtender> 

                                    </td>
                                <td align="left" style="vertical-align:middle">  
                                    Fax No</td>
                                
                                <td align="left" >
                                    <asp:TextBox ID="txtGFax_Country" runat="server" Width="60px" MaxLength="3" CssClass="form-control"></asp:TextBox>
                                    -
                                    <asp:TextBox ID="txtGFax_Area" runat="server" Width="60px" MaxLength="4" CssClass="form-control"></asp:TextBox>
                                    -
                                    <asp:TextBox ID="txtGFax_No" runat="server" Width="90px" MaxLength="10" CssClass="form-control"></asp:TextBox>
                                    <div class="remark">(Country-Area-Number)</div>
                                 
                                     <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender40" runat="server" 
                                     TargetControlID="txtGFax_Country"
                                            FilterType="Numbers">
                                        </ajaxToolkit:FilteredTextBoxExtender>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender41" runat="server" 
                                        TargetControlID="txtGFax_Area"
                                            FilterType="Numbers">
                                        </ajaxToolkit:FilteredTextBoxExtender>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender42" runat="server"
                                         TargetControlID="txtGFax_No"
                                            FilterType="Numbers">
                                        </ajaxToolkit:FilteredTextBoxExtender> 
                                    </td>
                            </tr>
                            <tr>
                                <td align="left" style="vertical-align:middle">  
                                    Overseas Address Line 1<span id="SpGPAddL1" runat="server" style="color:Red;" visible="false" >*</span></td>
                           
                                <td align="left"  style="vertical-align:middle" >
                                    <asp:TextBox ID="txtGPRMAddr1" runat="server" CssClass="form-control"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvGPAddL1" runat="server" ControlToValidate="txtGPRMAddr1"
                                         ErrorMessage="Guardian's Required overseas address Line 1" ForeColor="red" ValidationGroup="groupM1"></asp:RequiredFieldValidator>
                                         </td>
                                <td align="left" style="vertical-align:middle"> 
                                    Overseas Address Line 2<span id="SpGPAddL2" runat="server" style="color:Red;" visible="false" >*</span></td>
                             
                                <td align="left"  style="vertical-align:middle">
                                    <asp:TextBox ID="txtGPRMAddr2" runat="server" CssClass="form-control"></asp:TextBox><asp:RequiredFieldValidator ID="rfvGPAddL2" runat="server" 
                                    ControlToValidate="txtGPRMAddr2"
                                         ErrorMessage="Guardian's required overseas address Line 2" ForeColor="red" ValidationGroup="groupM1">*</asp:RequiredFieldValidator></td>
                            </tr>
                            <tr>
                                <td align="left" style="vertical-align:middle" >
                                    Overseas Address City/State
<span id="SpGPAddCity" runat="server" style="color:Red;" visible="false" >*</span></td>
                             
                                <td align="left" >
                                    <asp:TextBox ID="txtGPRM_City" runat="server" CssClass="form-control"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvGPAddCity" runat="server" ControlToValidate="txtGPRM_City"
                                         ErrorMessage="Guardian's overseas address city/state required" ForeColor="red" ValidationGroup="groupM1">*</asp:RequiredFieldValidator></td>
                                <td align="left" style="vertical-align:middle" >
                                    Overseas Address Country</td>
                                
                                <td align="left"  >
                                    <asp:DropDownList ID="ddlGPRM_Country" runat="server" CssClass="form-control">
                                    </asp:DropDownList></td>
                            </tr>
                            <tr>
                                <td align="left" style="vertical-align:middle">  
                                    Permanent Phone<span id="SpGPphNo" runat="server" style="color:Red;" visible="false" >*</span></td>
                               
                                <td align="left"  >
                                    <asp:TextBox ID="txtGPPERM_Country" runat="server" Width="60px" MaxLength="3" CssClass="form-control"></asp:TextBox>
                                    -
                                    <asp:TextBox ID="txtGPPERM_Area" runat="server" Width="60px" MaxLength="4" CssClass="form-control"></asp:TextBox>
                                    -
                                    <asp:TextBox ID="txtGPPERM_No" runat="server" Width="90px" MaxLength="10" CssClass="form-control"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvGPphNo" runat="server" ControlToValidate="txtGPPERM_No"
                                         ErrorMessage="Guardian's permanent phone number required" ForeColor="red" ValidationGroup="groupM1">*</asp:RequiredFieldValidator>

                                    <div class="remark">(Country-Area-Number)</div>
                                   <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender43" runat="server" TargetControlID="txtGPPERM_Country"
                                            FilterType="Numbers">
                                        </ajaxToolkit:FilteredTextBoxExtender>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender44" runat="server" TargetControlID="txtGPPERM_Area"
                                            FilterType="Numbers">
                                        </ajaxToolkit:FilteredTextBoxExtender>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender45" runat="server" TargetControlID="txtGPPERM_No"
                                            FilterType="Numbers">
                                        </ajaxToolkit:FilteredTextBoxExtender>   
                                    </td>
                                <td align="left" style="vertical-align:middle">  
                                    P.O BOX</td>
                                
                                <td align="left" style="vertical-align:middle" >
                                    <asp:TextBox ID="txtGPRMPOBOX" runat="server" MaxLength="20"
                                     CssClass="form-control"></asp:TextBox>
                                      
                                    </td>
                            </tr>
                            <tr>
                                <td align="left" style="vertical-align:middle" > 
                                    Occupation<span id="SpGOcc" runat="server" style="color:Red;" visible="false" >*</span></td>
                               
                                <td align="left" style="vertical-align:middle" >
                                    <asp:TextBox ID="txtGOcc" runat="server" CssClass="form-control"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvGOcc" runat="server" ControlToValidate="txtGOcc"
                                         ErrorMessage="Guardian's occupation required" ForeColor="red" ValidationGroup="groupM1">*</asp:RequiredFieldValidator></td>
                                <td align="left" style="vertical-align:middle">  
                                    Company Name</td>
                                
                                <td align="left" >
                                    <asp:DropDownList id="ddlGCompany_Name" runat="server" CssClass="form-control">
                                    </asp:DropDownList><br />
                                    <div class="remark">(If you choose other,please specify the Company Name)</div>
                                    <asp:TextBox ID="txtGComp_Name" runat="server" CssClass="form-control"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td align="left"  style="vertical-align:middle">
                                    Company Address</td>
                               
                                <td align="left"  >
                                    <asp:TextBox ID="txtGComp_Add" runat="server" CssClass="form-control"></asp:TextBox></td>
                                <td align="left"  style="vertical-align:middle">
                                    Email<span id="SpGEmail" runat="server" style="color:Red;" visible="false" >*</span></td>
                             
                                <td align="left"  >
                                    <asp:TextBox ID="txtGEmail" runat="server" CssClass="form-control"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvGEmail" runat="server" ControlToValidate="txtGEmail"
                                        ErrorMessage="Guardian's email address required" ForeColor="red" ValidationGroup="groupM1">*</asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator63" runat="server" ControlToValidate="txtGEmail"
                            Display="Dynamic" ErrorMessage="Enter valid email address"
                            ValidationExpression="^([0-9a-zA-Z]([-\.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$"
                            ValidationGroup="groupM1">*</asp:RegularExpressionValidator>
                                    </td>
                            </tr>
                             <tr>
                              
                        
            <td align="left" colspan="4">
            <asp:Label ID="lblSave" runat="server"  EnableViewState="False"></asp:Label>
            </td>
        </tr>
        <tr>
           
            
            
            <td align="center" colspan="4">
                <asp:Button ID="btnEdit" visible="false" runat="server" CausesValidation="False" CssClass="btn btn-info" 
                        Text="Edit" />&nbsp;<asp:Button ID="btnSave" runat="server" Text="Save"  
                            ValidationGroup="groupM1" CssClass="btn btn-info"  />&nbsp;<asp:Button ID="btnCancel" runat="server" CausesValidation="False"
                                CssClass="btn btn-info" Text="Cancel" /></td>
           
         </tr>
         <tr>
                <td 
                    align="center" colspan="4">
                     <asp:ValidationSummary ID="vsRef" runat="server" 
         ValidationGroup="groupM1"  HeaderText="<div class='validationheader'>Please correct the following:</div>"
                                 CssClass="divinfoInner" ForeColor=""/>  
          
          <asp:Label ID="lbmsgInfo" runat="server"></asp:Label>
          
          </td></tr>   
        
                        </table>
  <asp:HiddenField ID="h_StuID" runat="server" />
   </div>
            </div>
          </div>
                         <!-- /Posts Block -->

                        </div>
                    </div>
        </div>
</asp:Content>

