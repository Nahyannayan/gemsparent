Imports Microsoft.ApplicationBlocks.Data
Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Text

Partial Class ParentPasswordRet
    Inherits System.Web.UI.Page
    Dim SessionFlag As Integer
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Page.Title = "GEMS EDUCATION"
        ' lbPwdAsst.Text = "<div class='left'><div class='right'>Password Assistance</div></div>"

        lbPwdAsst.Text = "<div class='left'><div class='right'>Password Assistance</div></div>"
     
    End Sub
    
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click

        lblError.CssClass = ""
        Dim PR_ID As String = String.Empty
        Dim status As Integer
        Dim flag As Integer
        Dim errorMessage As String = String.Empty
        Dim RESET_PASSWORD As String = String.Empty
        Dim URL_DATE As String = String.Empty
        If Page.IsValid = True Then

            Captcha1.ValidateCaptcha(txtCapt.Text.Trim())

            If Captcha1.UserValidated = False Then
                lblError.CssClass = "divinfoInnertext"
                lblError.Text = "<div class='validationheader'>Please update the following:</div><ul><li>The text you entered didn't match the image shown. Please try again.</li></ul>"

            Else
                Dim transaction As SqlTransaction
                Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                    Try
                        transaction = conn.BeginTransaction("SampleTransaction")
                        RESET_PASSWORD = DateTime.Now.ToString("ddMMMyyyyHHmmss")
                        URL_DATE = DateTime.Now.ToString("dd/MMM/yyyy HH:mm:ss")
                        Dim pParms(10) As SqlClient.SqlParameter
                        pParms(0) = New SqlClient.SqlParameter("@EMAIL_ID", txtemailid.Text)
                        pParms(1) = New SqlClient.SqlParameter("@OLU_USR_ID", txtUSR_ID.Text.Trim)
                        pParms(2) = New SqlClient.SqlParameter("@URL_TEXT", Encr_decrData.Encrypt(URL_DATE))
                        pParms(3) = New SqlClient.SqlParameter("@RESET_PASSWORD", Encr_decrData.Encrypt(RESET_PASSWORD))
                        pParms(4) = New SqlClient.SqlParameter("@PR_ID_OUT", SqlDbType.BigInt)
                        pParms(4).Direction = ParameterDirection.Output
                        pParms(5) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                        pParms(5).Direction = ParameterDirection.ReturnValue
                        SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "OPL.SAVEPARENT_PASSWORD_RESET", pParms)
                        status = pParms(5).Value
                        CLEAR()

                        If status = -121 Then
                            lblmsg.CssClass = "diverror"
                            lblmsg.Text = "Incorrect login id / email address.Please re-enter."
                        ElseIf status <> 0 Then

                            lblmsg.CssClass = "diverror"
                            lblmsg.Text = "Password recovery failed..!"
                        Else
                            PR_ID = IIf(TypeOf (pParms(4).Value) Is DBNull, String.Empty, pParms(4).Value)

                        End If
                    Catch ex As Exception
                        status = -1
                        errorMessage = ex.Message
                        UtilityObj.Errorlog(ex.Message)
                        lblmsg.CssClass = "diverror"
                        lblmsg.Text = "Password recovery failed..!"
                    Finally
                        If status <> 0 Then
                            UtilityObj.Errorlog(errorMessage)
                            transaction.Rollback()

                        Else
                            errorMessage = ""
                            transaction.Commit()

                        End If
                    End Try

                End Using
                If PR_ID <> "" Then



                    status = VerifyandUpdatePassword(Encr_decrData.Encrypt(RESET_PASSWORD), txtUSR_ID.Text.Trim)
                    ''commented by nahyan on 1june2017
                    'Dim vGLGUPDPWD As New com.ChangePWDWebService
                    'vGLGUPDPWD.Url = "http://10.10.1.35/release/ChangePWDWebService.asmx"
                    'Dim respon As String = vGLGUPDPWD.ChangePassword(txtUSR_ID.Text.Trim, RESET_PASSWORD, RESET_PASSWORD)

                    ''by nahyan on 1june2017
                    Dim chPWDSVC As New ChangePasswordSVC.ChangePWDWebServiceSoapClient

                    Dim respon As String = chPWDSVC.ChangePassword(txtUSR_ID.Text.Trim, RESET_PASSWORD, RESET_PASSWORD)
                    ''by nahyan on 1june2017

                    Dim STR_CONN As String = ConnectionManger.GetOASISConnectionString
                    Dim Parammail(2) As SqlClient.SqlParameter
                    Parammail(0) = New SqlClient.SqlParameter("@PR_ID", PR_ID)
                    Parammail(1) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                    Parammail(1).Direction = ParameterDirection.ReturnValue
                    SqlHelper.ExecuteNonQuery(STR_CONN, CommandType.StoredProcedure, "OPL.SAVEPARENT_PASSWORD_RESET_EMAIL", Parammail)
                    status = Parammail(1).Value
                    If status = 0 Then
                        lblmsg.CssClass = "divvalid"
                        lblmsg.Text = "Process successful.Link has been sent to your email address. "
                        btnSave.Text = "Resend"
                        lblError.CssClass = "divinfoInnertext"
                        lblError.Text = "In case you have not received the email with the link click the resend button."
                    Else
                        lblmsg.CssClass = "diverror"
                        lblmsg.Text = "Error occurred while sending email.Please try again later. "
                    End If
                Else

                    lblmsg.CssClass = "diverror"
                    lblmsg.Text = "Incorrect login id / email address.Please re-enter."
                End If



            End If
        End If



    End Sub
    Private Sub CLEAR()
        txtCapt.Text = ""
        'txtemailid.Text = ""
        'txtUSR_ID.Text = ""
    End Sub


    Private Function VerifyandUpdatePassword(ByVal New_Password As String, _
   ByVal username As String) As Integer
        Dim connection As SqlConnection
        Try
            connection = ConnectionManger.GetGLGConnection()
            Dim pParms(2) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@USR_NAME", username)
            pParms(1) = New SqlClient.SqlParameter("@NEW_PWD", New_Password)
            pParms(2) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(2).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "GLG.GLG_AD_PWDUPD", pParms)
            'SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "GLG.GLG_AD_PWDUPD", pParms)
            Return pParms(2).Value

        Catch
        Finally
            connection.Close()
        End Try
    End Function

    Protected Sub lbHomeTop_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbHomeTop.Click
        Response.Redirect("https://oasis.gemseducation.com/general/Home.aspx")
    End Sub
End Class
