<%@ Page Language="VB" MasterPageFile="~/ParentMaster.master"
    AutoEventWireup="false" CodeFile="Changepassword_old.aspx.vb" Inherits="ParentLogin_Changepassword" Title="::GEMS EDUCATION::" %>

<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphParent" runat="Server">
    <div class="content margin-top30 margin-bottom60">
        <div class="container">
            <div class="row">

                <!-- Posts Block -->
                <div class="posts-block col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="my-account">
                        <asp:Label ID="Label1" runat="server" EnableViewState="False"></asp:Label>
                        
                        <div class="title-box">
                                <h3>Change Password 
                                    <span class="profile-right">
                                <asp:Label ID="lbChildNameTop" runat="server"></asp:Label>
                            </span></h3>
                        </div>

                        <div  class="alert alert-warning">
                            <button type="button" class="close" data-dismiss="alert">�</button>
                              <ul><li>An asterisk (<font color="#ff0000">*</font>) indicates mandatory fields.</li>
                              <li>Current password and new password cannot be same.</li>                                 

                              <li>Password must be at least 7 characters long.</li>
                                  </ul>
                            </div>
                        <asp:Label ID="lblmsg" runat="server"></asp:Label>
                        <div align="center" style="text-align: center; width: 100%;">


                            <table id="tbl_AddGroup" runat="server" class="table table-striped table-bordered table-responsive text-left my-orders-table" width="100%">

                                <tr>
                                    <td align="left" style="vertical-align:middle;width:30%">Enter Current Password<font color="red">*</font></td>
                                    <td align="left">
                                        <asp:TextBox ID="txtCurrentPassword" runat="server" MaxLength="100"
                                            CssClass="form-control"
                                            TextMode="Password"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvPassword" runat="server" ControlToValidate="txtCurrentPassword"
                                            Display="Dynamic" ErrorMessage="CurrentPassword can not be left empty"
                                            ForeColor="red" SetFocusOnError="True" ValidationGroup="GroupPwd">*</asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" style="vertical-align:middle">Enter New Password<font color="red">*</font></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtNewpassword" runat="server"
                                            MaxLength="100" CssClass="form-control"
                                            TextMode="Password"></asp:TextBox>
                                        <%--<ajaxToolkit:FilteredTextBoxExtender ID="ftbxNewpassword"
                             runat="server"  TargetControlID="txtNewpassword"
                                    FilterType="UppercaseLetters,LowercaseLetters,Numbers" >
                                </ajaxToolkit:FilteredTextBoxExtender>--%>
                                        <%-- <asp:RegularExpressionValidator ID="revPassword" runat="server" ControlToValidate="txtNewpassword"
                            Display="Dynamic" ErrorMessage="Your password must contain atleast  7 characters"
                            ForeColor="red" SetFocusOnError="True" ValidationExpression="^[a-zA-Z0-9]{7,20}$" 
                            ValidationGroup="GroupPwd">*</asp:RegularExpressionValidator>--%>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtNewpassword"
                                            Display="Dynamic" ErrorMessage="Please enter your new password"
                                            ForeColor="red" ValidationGroup="GroupPwd">*</asp:RequiredFieldValidator><br />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" style="vertical-align:middle">Confirm&nbsp;New Password<font color="red">*</font></td>

                                    <td align="left">
                                        <asp:TextBox ID="txtConfPassword" runat="server"
                                            CssClass="form-control" TextMode="Password"></asp:TextBox>
                                        <%--   <ajaxToolkit:FilteredTextBoxExtender ID="ftbxConfPassword"
                             runat="server"  TargetControlID="txtConfPassword"
                                    FilterType="UppercaseLetters,LowercaseLetters,Numbers" >
                                </ajaxToolkit:FilteredTextBoxExtender>--%>
                                        <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="txtNewpassword"
                                            ControlToValidate="txtConfPassword" Display="Dynamic" ErrorMessage="Password do not match"
                                            ForeColor="red" ValidationGroup="GroupPwd">*</asp:CompareValidator>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtConfPassword"
                                            Display="Dynamic" ErrorMessage="Confirm password can not be left empty"
                                            ForeColor="red" ValidationGroup="GroupPwd">*</asp:RequiredFieldValidator></td>
                                </tr>

                                <tr>
                                    <td></td>
                                    <td align="left">
                                        <asp:Button ID="btnSave" runat="server" Text="Save"
                                            ValidationGroup="GroupPwd" CssClass="btn btn-info" />
                                        <asp:Button ID="btnClear" runat="server" CausesValidation="False"
                                                Text="Clear"  CssClass="btn btn-info" /></td>
                                </tr>
                                
                            </table>

                            
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" EnableViewState="False" CssClass="alert-warning alert"
                                            ForeColor="" HeaderText="You must enter a value in the following fields:" ValidationGroup="GroupPwd" />
                            <ajaxToolkit:PasswordStrength ID="PasswordStrength1" runat="server"
                                HelpStatusLabelID="TextBox1_HelpLabel" MinimumNumericCharacters="2" PreferredPasswordLength="7"
                                PrefixText="&nbsp;Strength:" RequiresUpperAndLowerCaseCharacters="true" StrengthStyles="red;blue;grey;yellow;green" TargetControlID="txtNewpassword" TextStrengthDescriptions="Very Poor&nbsp;;Weak&nbsp;;Average&nbsp;;Strong;Excellent" TextCssClass="">
                            </ajaxToolkit:PasswordStrength>
                            

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

