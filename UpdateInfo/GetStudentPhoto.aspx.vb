Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Web.Security

Partial Class ParentLogin_GetStudentPhoto
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("username") Is Nothing Then
            Session("Active_tab") = "Home"
            Session("Site_Path") = ""
            Response.Redirect("~\login.aspx")
        ElseIf Session("bPasswdChanged") = "False" Then
            Response.Redirect("~\UpdateInfo\ForcePasswordChange.aspx", False)
        ElseIf Session("bUpdateContactDetails") = "False" Then
            Response.Redirect(Session("ForceUpdate_stud"), False)
        End If
        Response.ContentType = "image/jpeg"
        Response.Clear()
        ' Buffer response so that page is sent
        ' after processing is complete.
        Response.BufferOutput = True
        Dim str_noimagepath As String = Server.MapPath("Images\no_image.gif")
        Dim str_path As String = GetStudentPhotoActualPath()
        If str_path.Trim = "" Then
            str_path = str_noimagepath
        End If
        Response.WriteFile(str_path)
        ' Send the output to the client.
        Response.Flush()
        Response.End()
    End Sub

    Public Function GetStudentPhotoActualPath() As String
        Dim Virtual_Path As String = Web.Configuration.WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ToString()
        Dim Phy_Path As String = Web.Configuration.WebConfigurationManager.ConnectionStrings("EmpFilepath").ToString()
        Try
            Dim encr As New Encryption64
            Dim id As String = encr.Decrypt(Context.Request.QueryString("id").ToString().Replace(" ", "+"))
            Dim strqry As String = "select isnull(STU_PHOTOPATH,'') STU_PHOTOPATH from student_m where stu_id='" & id & "'"
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, strqry)
            Dim str_path As String = ds.Tables(0).Rows(0)(0)
            If File.Exists(Phy_Path + str_path) Then
                Return Phy_Path + str_path ' + "?id=" & Encr_decrData.Encrypt(DateTime.Now).ToString().Substring(1, 15)


            End If
            Return ""
        Catch ex As Exception
            Return ""
        End Try
    End Function

End Class
