<%@ Page Language="VB" MasterPageFile="~/ParentMaster.master"  
 AutoEventWireup="false" CodeFile="PasswordRecovery.aspx.vb" Inherits="ParentLogin_PasswordRecovery" title="::GEMS EDUCATION::"  %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphParent" Runat="Server">      

<table id="tbl_AddGroup" runat="server" align="center" border="0" cellpadding="2"
    cellspacing="0" class="mattersParent" style="width: 83%">
    <tr>
        <td align="center" style=" height: 19px" valign="bottom">
        </td>
    </tr>
   
    <tr>
        <td style="height: 161px" valign="top">
            <table align="center" class="BlueTable" style="width: 100%" border="0" >
             <tr class="subheader_img">
                <td align="center"  style=" height: 19px" colspan="3" >
                  Password Recovery 
                </td>
            </tr>
             
             
              <tr>
                                              <td align="left" class="matters" width="30%">
                                                  Email Id for password recovery*</td>
                                              <td width="5%">
                                                  :</td>
                                              <td align="left" class="matter" width="75%">
                                                  <asp:TextBox ID="txtemailid" runat="server" size="40"></asp:TextBox>
                                                 
                                                  <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" CssClass="error"
                                                      ErrorMessage="Invalid email id" ForeColor="" ValidationGroup="GroupPwd" ControlToValidate="txtemailid" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                                      
                                                       <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" CssClass="error"
                                                      ErrorMessage="Invalid email id" ForeColor="" ValidationGroup="GroupPwd" ControlToValidate="txtemailid"></asp:RequiredFieldValidator></td>
                                          </tr>
              
                <tr>
                    <td align="center" class="matters" colspan="3">
                        &nbsp;<asp:Button ID="btnSave" runat="server" Text="Update" Height="20px" Width="70px" ValidationGroup="GroupPwd" />
                       </td>
                </tr>
            </table>
                        
            <asp:Label ID="lblError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>

        </td>
    </tr>
</table>
</asp:Content>

