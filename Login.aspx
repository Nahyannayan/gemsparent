﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Login.aspx.vb" Inherits="Login" EnableEventValidation="false" %>

<!DOCTYPE html>
<!--[if IE 8]>          <html class="ie ie8"> <![endif]-->
<!--[if IE 9]>          <html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->
<html>
<!--<![endif]-->


<head>
    <meta charset="utf-8">
    <title>GEMS Parent Portal</title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Google Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Raleway:100,200,300,700,800,900' rel='stylesheet' type='text/css'>
    <!-- Library CSS -->
    <link rel="stylesheet" href="/css/bootstrap.css">
    <link rel="stylesheet" href="/css/bootstrap-theme.css">
    <link rel="stylesheet" href="/css/fonts/font-awesome/css/font-awesome.css">
    <link rel="stylesheet" href="/css/animations.css" media="screen">
    <link rel="stylesheet" href="/css/superfish.css" media="screen">
    <link rel="stylesheet" href="/css/revolution-slider/css/settings.css" media="screen">
    <link rel="stylesheet" href="/css/revolution-slider/css/extralayers.css" media="screen">
    <link rel="stylesheet" href="/css/prettyPhoto.css" media="screen">
    <!-- Theme CSS -->
    <link rel="stylesheet" href="/css/style.css">
    <!-- Skin -->
    <link rel="stylesheet" href="/css/colors/blue.css" class="colors">
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="/css/theme-responsive.css">
    <!-- Switcher CSS -->
    <link href="/css/switcher.css" rel="stylesheet">
    <link href="/css/spectrum.css" rel="stylesheet">
    <!-- Favicons -->
    <link rel="shortcut icon" href="/img/ico/favicon.ico">
    <link rel="apple-touch-icon" href="/img/ico/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/img/ico/apple-touch-icon-72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/img/ico/apple-touch-icon-114.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/img/ico/apple-touch-icon-144.png">
    <!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
        <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <script src="/js/respond.min.js"></script>
        <![endif]-->
    <!--[if IE]>
        <link rel="stylesheet" href="/css/ie.css">
        <![endif]-->
    <script type="text/javascript">
        function doClick(e, t) {
            //the purpose of this function is to allow the enter key to 
            //point to the correct button to click.
            var key;

            if (window.event)
                key = window.event.keyCode;     //IE
            else
                key = e.which;     //firefox

            if (key == 13) {

                //Get the button the user wants to have clicked
                var btn = document.getElementById('<%= btnLogin.ClientID%>');
                if (btn != null) { //If we find the button click it
                    btn.click();
                    event.keyCode = 0
                }
            }
        }
    </script>
</head>
<body class="home">
    <form id="form1" runat="server" method="post">
        <!-- Wrap -->
        <div class="wrap">
            <!-- Header -->
            <header id="header">
                <!-- Header Top Bar -->
                <div class="top-bar">
                    <div class="slidedown collapse in">
                        <div class="container">
                            <div class="pull-left">
                            </div>
                            <%--<div class="phone-login pull-right">
                               <form class="form-inline">
                                   <asp:TextBox id="txtLoginId" runat="server" class="input-sm"> </asp:TextBox>
                                   <asp:TextBox id="txtPass" runat="server" class="input-sm" TextMode="Password"></asp:TextBox>
                                   <asp:LinkButton id="btnLogin" runat="server" class="btn btn-default"><i class="fa fa-sign-in"></i> Login</asp:LinkButton>
                                   <asp:LinkButton  ID="lbtnForgot" runat="server" class="btn btn-default"><i class="fa fa-reset"></i> Forgot Password</asp:LinkButton>
                               </form>
                                
                            </div>--%>
                        </div>
                    </div>
                </div>
                <!-- /Header Top Bar -->
                <!-- Main Header -->
                <div class="main-header">
                    <div class="container">
                        <!-- TopNav -->
                        <%--<div class="topnav navbar-header">
                            <a class="navbar-toggle down-button" data-toggle="collapse" data-target=".slidedown">
                            <i class="fa fa-angle-up icon-current"></i>
                            </a> 
                        </div>--%>
                        <!-- /TopNav-->
                        <!-- Logo -->
                        <div class="logo pull-left">
                            <h1>
                                <a href="#">
                                    <img class="logo-color" src="/img/logos/logo_green-f.png" alt="GEMS Parent Portal" width="160" height="60">
                                </a>
                            </h1>
                        </div>
                        <!-- /Logo -->
                        <!-- Login section added here -->
                        <div class="phone-login pull-right row">
                            <form class="form-inline col-lg-12">
                                <asp:TextBox ID="txtLoginId" runat="server" class="form-control margin-bottom5 margin-right4 col-lg-5 col-md-5 col-sm-5 col-xs-5" placeholder="Username" AutoComplete="off"> </asp:TextBox>
                                <asp:TextBox ID="txtPass" runat="server" class="form-control margin-bottom5 margin-right4 col-lg-5 col-md-5 col-sm-5 col-xs-5" TextMode="Password" placeholder="Password" onkeypress="doClick(event, this)"></asp:TextBox>
                                <div class="row">
                                    <div class="col-sm-4 col-xs-4 right">
                                        <asp:LinkButton ID="btnLogin" runat="server" class="btn btn-default"><i class="fa fa-sign-in"></i> Login</asp:LinkButton>
                                    </div>
                                    <div class="col-sm-4 col-xs-4">
                                        <asp:LinkButton ID="lbtnForgot" runat="server" class="btn btn-link"><i class="fa fa-key"></i> Forgot Password</asp:LinkButton>
                                    </div>
                                </div>
                                <asp:Label ID="lblmsg" runat="server" Style="position: absolute; margnin: 10px; padding: 2px 8px;"></asp:Label>
                            </form>

                        </div>
                        <!-- /Login section -->
                        <!-- Mobile Menu -->
                        <%--<div class="mobile navbar-header">
                            <a class="navbar-toggle" data-toggle="collapse" href=".html">
                            <i class="fa fa-bars fa-2x"></i>
                            </a> 
                        </div>--%>
                        <!-- /Mobile Menu -->

                    </div>
                </div>
                <!-- /Main Header -->
            </header>
            <!-- /Header -->
            <!-- Main Section -->
            <section id="main">
                <!-- Slider -->
                <div class="fullwidthbanner-container tp-banner-container">
                    <div class="fullwidthbanner rslider tp-banner">
                        <ul>
                            <!-- THE FIRST SLIDE -->
                            <li data-transition="fade" data-slotamount="7" data-masterspeed="2000" data-thumb="homeslider_thumb2.jpg" data-delay="10000" data-saveperformance="on">
                                <!-- MAIN IMAGE -->
                                <img src="/img/dummy.png" alt="laptopmockup_sliderdy" data-lazyload="/img/slider/slider-bg-1.jpg" data-bgposition="right top" data-kenburns="on" data-duration="12000" data-ease="Power0.easeInOut" data-bgfit="115" data-bgfitend="100" data-bgpositionend="center bottom">
                                <!-- LAYERS -->
                                <!-- LAYER NR. 1 -->
                                <div class="tp-caption customin fadeout rs-parallaxlevel-10"
                                    data-x="655"
                                    data-y="116"
                                    data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                    data-speed="300"
                                    data-start="2700"
                                    data-easing="Power3.easeInOut"
                                    data-elementdelay="0.1"
                                    data-endelementdelay="0.1"
                                    data-endspeed="300"
                                    style="z-index: 2;">
                                    <img src="/img/dummy.png" alt="" data-lazyload="/img/redbg_big.png">
                                </div>
                                <!-- LAYER NR. 2 -->
                                <div class="tp-caption light_heavy_50 customin fadeout tp-resizeme rs-parallaxlevel-10"
                                    data-x="680"
                                    data-y="154"
                                    data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                    data-speed="300"
                                    data-start="2850"
                                    data-easing="Power3.easeInOut"
                                    data-splitin="none"
                                    data-splitout="none"
                                    data-elementdelay="0.1"
                                    data-endelementdelay="0.1"
                                    data-endspeed="300"
                                    style="z-index: 3; max-width: auto; max-height: auto; white-space: nowrap;">
                                    &nbsp;
                                </div>
                                <!-- LAYER NR. 3 -->
                                <div class="tp-caption black_heavy_70 skewfromleftshort fadeout tp-resizeme rs-parallaxlevel-10"
                                    data-x="650"
                                    data-y="183"
                                    data-speed="500"
                                    data-start="2400"
                                    data-easing="Power3.easeInOut"
                                    data-splitin="chars"
                                    data-splitout="none"
                                    data-elementdelay="0.1"
                                    data-endelementdelay="0.1"
                                    data-endspeed="300"
                                    style="z-index: 4; max-width: auto; max-height: auto; white-space: nowrap;">
                                    Parent Portal
                                </div>

                                <!-- LAYER NR. 4 -->
                                <%-- <div class="tp-caption black_bold_40 skewfromrightshort fadeout tp-resizeme rs-parallaxlevel-10"
                                    data-x="900"
                                    data-y="232" 
                                    data-speed="500"
                                    data-start="3200"
                                    data-easing="Power3.easeInOut"
                                    data-splitin="chars"
                                    data-splitout="none"
                                    data-elementdelay="0.1"
                                    data-endelementdelay="0.1"
                                    data-endspeed="300"
                                    style="z-index: 5; max-width: auto; max-height: auto; white-space: nowrap;">OASIS
                                </div>--%>
                               
                               
                            </li>
                            <!-- /THE FIRST SLIDE -->
                            <!-- THE RESPONSIVE SLIDE -->

                        </ul>
                    </div>
                </div>
                <!-- Slider -->
                <div class="portfolio bottom-pad margin-top100">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="portfolio-title text-center">
                                    <h2 class="wow fadeIn">Welcome to GEMS Parent Portal</h2>
                                    <h4 class="wow fadeInRight">
                                        <br />

                                    </h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Product Content -->
                <div class="product-lead bottom-pad margin-top100">
                    <div class="pattern-overlay">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-6 col-sm-12 col-xs-12 text-center wow fadeInLeft">
                                    <div class="row">
                                        <div class="col-md-6 col-sm-12 col-xs-12 text-center wow fadeInLeft animated" style="visibility: visible;">
                                            <img class="app-img" src="/img/gems-connect-parent-uae.png" alt="Gems Connect App">
                                        </div>
                                        <div class="col-md-6 col-sm-12 col-xs-12 text-center wow fadeInLeft animated" style="visibility: visible;">
                                            <div class="app-service padding-bottom50">
                                                <h3 class="light padding-bottom30">GEMS Connect</h3>

                                                <p class="padding-bottom30">With the <strong>GEMS Connect App</strong>, parents can now access student information, track bus location, 
                                                    pay fees, view circulars, newsletters, timetable, assesment reports and track attendance, 
                                                    submit leave requests and do much more in one place.</p>
                                                <div class="divider"></div>
                                                <div class="learnmore wow fadeInRight padding-bottom20">
                                                    <img class="app-img" src="/img/gems-connect-qrcode.png" alt="Gems Connect QR code" width="100">
                                                </div>
                                                <div class="learnmore wow fadeInDown">
                                                    <a href="https://play.google.com/store/apps/details?id=com.GEMS.Connect" target="_blank">
                                                        <img class="app-img" src="/img/apple-store.png" alt="Gems Connect on Apple Store" width="125"></a>
                                                    <a href="https://itunes.apple.com/ae/app/gems-connect/id1462613346?mt=8" target="_blank">
                                                        <img class="app-img" src="/img/google-play.png" alt="Gems Connect on Apple Store" width="125"></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12 col-xs-12 wow fadeInRight">
                                    <div class="row">
                                        <div class="col-md-6 col-sm-12 col-xs-12 text-center wow fadeInLeft animated" style="visibility: visible;">
                                            <img class="app-img" src="/img/gems-rewards-parent.png" alt="Gems Rewards App">
                                        </div>
                                        <div class="col-md-6 col-sm-12 col-xs-12 text-center wow fadeInLeft animated" style="visibility: visible;">
                                            <div class="app-service padding-bottom50">
                                                <h3 class="light">GEMS Rewards ( Applicable to UAE schools only)</h3>

                                                <p>
                                                    GEMS Education is delighted to introduce GEMS Rewards, an exclusive programme for our community of students, parents &amp; staff.<br>
                                                    GEMS families can now explore cashback opportunities and enjoy incredible savings and discounts across various experiences.
                                                </p>
                                                <div class="divider"></div>
                                                <div class="learnmore wow fadeInRight padding-bottom20">
                                                    <img class="app-img" src="/img/gems-rewards-qr-code.png" alt="Gems Connect QR code" width="100">
                                                </div>
                                                <div class="learnmore wow fadeInDown">
                                                    <a href="https://itunes.apple.com/us/app/gems-rewards/id1287373984?ls=1&mt=8" target="_blank">
                                                        <img class="app-img" src="/img/apple-store.png" alt="Gems Connect on Apple Store" width="125"></a>
                                                    <a href="https://play.google.com/store/apps/details?id=com.clubclass.gemsrewards" target="_blank">
                                                        <img class="app-img" src="/img/google-play.png" alt="Gems Connect on Apple Store" width="125"></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /Product Content -->



                <!-- Our Clients -->
                <div class="our-clients">
                    <div class="container">
                        <div class="row">
                            <div class="client">
                                <div class="client-logo">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="text-center">
                                            <h2 class="wow fadeIn">Our Schools</h2>

                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="clearfix"></div>
                                        <div class="row text-center padding-top40">
                                            <div id="client-carousel" class="client-carousel carousel slide">
                                                <div class="carousel-inner">
                                                    <div class="item active">
                                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 item wow fadeIn">
                                                            <div class="item-inner"><a href="#">
                                                                <img alt="Upportdash" src="/img/clientslogo/akn.png"></a></div>
                                                        </div>
                                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 item wow fadeIn">
                                                            <div class="item-inner"><a href="#">
                                                                <img alt="Upportdash" src="/img/clientslogo/gaa.png"></a></div>
                                                        </div>
                                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 item wow fadeIn">
                                                            <div class="item-inner"><a href="#">
                                                                <img alt="Upportdash" src="/img/clientslogo/gfs.png"></a></div>
                                                        </div>
                                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 item wow fadeIn">
                                                            <div class="item-inner"><a href="#">
                                                                <img alt="Upportdash" src="/img/clientslogo/jc.png"></a></div>
                                                        </div>
                                                    </div>
                                                    <div class="item">
                                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 item">
                                                            <div class="item-inner"><a href="#">
                                                                <img alt="Upportdash" src="/img/clientslogo/jps.png"></a></div>
                                                        </div>
                                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 item">
                                                            <div class="item-inner"><a href="#">
                                                                <img alt="Upportdash" src="/img/clientslogo/mts.png"></a></div>
                                                        </div>
                                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 item">
                                                            <div class="item-inner"><a href="#">
                                                                <img alt="Upportdash" src="/img/clientslogo/nsb.png"></a></div>
                                                        </div>
                                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 item">
                                                            <div class="item-inner"><a href="#">
                                                                <img alt="Upportdash" src="/img/clientslogo/nsg.png"></a></div>
                                                        </div>
                                                    </div>
                                                    <div class="item">
                                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 item">
                                                            <div class="item-inner"><a href="#">
                                                                <img alt="Upportdash" src="/img/clientslogo/oow.png"></a></div>
                                                        </div>
                                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 item">
                                                            <div class="item-inner"><a href="#">
                                                                <img alt="Upportdash" src="/img/clientslogo/rds.png"></a></div>
                                                        </div>
                                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 item">
                                                            <div class="item-inner"><a href="#">
                                                                <img alt="Upportdash" src="/img/clientslogo/tms.png"></a></div>
                                                        </div>
                                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 item">
                                                            <div class="item-inner"><a href="#">
                                                                <img alt="Upportdash" src="/img/clientslogo/wek.png"></a></div>
                                                        </div>
                                                    </div>
                                                    <div class="item">
                                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 item">
                                                            <div class="item-inner"><a href="#">
                                                                <img alt="Upportdash" src="/img/clientslogo/win.png"></a></div>
                                                        </div>
                                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 item">
                                                            <div class="item-inner"><a href="#">
                                                                <img alt="Upportdash" src="/img/clientslogo/wis.png"></a></div>
                                                        </div>
                                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 item">
                                                            <div class="item-inner"><a href="#">
                                                                <img alt="Upportdash" src="/img/clientslogo/chs.png"></a></div>
                                                        </div>
                                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 item">
                                                            <div class="item-inner"><a href="#">
                                                                <img alt="Upportdash" src="/img/clientslogo/cia.png"></a></div>
                                                        </div>
                                                    </div>
                                                    <div class="item">
                                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 item">
                                                            <div class="item-inner"><a href="#">
                                                                <img alt="Upportdash" src="/img/clientslogo/daa.png"></a></div>
                                                        </div>
                                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 item">
                                                            <div class="item-inner"><a href="#">
                                                                <img alt="Upportdash" src="/img/clientslogo/fps.png"></a></div>
                                                        </div>
                                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 item">
                                                            <div class="item-inner"><a href="#">
                                                                <img alt="Upportdash" src="/img/clientslogo/gep.png"></a></div>
                                                        </div>
                                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 item">
                                                            <div class="item-inner"><a href="#">
                                                                <img alt="Upportdash" src="/img/clientslogo/gfm.png"></a></div>
                                                        </div>
                                                    </div>
                                                    <div class="item">
                                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 item">
                                                            <div class="item-inner"><a href="#">
                                                                <img alt="Upportdash" src="/img/clientslogo/ghs.png"></a></div>
                                                        </div>
                                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 item">
                                                            <div class="item-inner"><a href="#">
                                                                <img alt="Upportdash" src="/img/clientslogo/gis.png"></a></div>
                                                        </div>
                                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 item">
                                                            <div class="item-inner"><a href="#">
                                                                <img alt="Upportdash" src="/img/clientslogo/gma.png"></a></div>
                                                        </div>
                                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 item">
                                                            <div class="item-inner"><a href="#">
                                                                <img alt="Upportdash" src="/img/clientslogo/gms.png"></a></div>
                                                        </div>
                                                    </div>
                                                    <div class="item">
                                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 item">
                                                            <div class="item-inner"><a href="#">
                                                                <img alt="Upportdash" src="/img/clientslogo/gus.png"></a></div>
                                                        </div>
                                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 item">
                                                            <div class="item-inner"><a href="#">
                                                                <img alt="Upportdash" src="/img/clientslogo/gwa.png"></a></div>
                                                        </div>
                                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 item">
                                                            <div class="item-inner"><a href="#">
                                                                <img alt="Upportdash" src="/img/clientslogo/kgs.png"></a></div>
                                                        </div>
                                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 item">
                                                            <div class="item-inner"><a href="#">
                                                                <img alt="Upportdash" src="/img/clientslogo/nms.png"></a></div>
                                                        </div>
                                                    </div>
                                                    <div class="item">
                                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 item">
                                                            <div class="item-inner"><a href="#">
                                                                <img alt="Upportdash" src="/img/clientslogo/ood.png"></a></div>
                                                        </div>
                                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 item">
                                                            <div class="item-inner"><a href="#">
                                                                <img alt="Upportdash" src="/img/clientslogo/ool.png"></a></div>
                                                        </div>
                                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 item">
                                                            <div class="item-inner"><a href="#">
                                                                <img alt="Upportdash" src="/img/clientslogo/uis.png"></a></div>
                                                        </div>
                                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 item">
                                                            <div class="item-inner"><a href="#">
                                                                <img alt="Upportdash" src="/img/clientslogo/waa.png"></a></div>
                                                        </div>
                                                    </div>
                                                    <div class="item">
                                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 item">
                                                            <div class="item-inner"><a href="#">
                                                                <img alt="Upportdash" src="/img/clientslogo/wgp.png"></a></div>
                                                        </div>
                                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 item">
                                                            <div class="item-inner"><a href="#">
                                                                <img alt="Upportdash" src="/img/clientslogo/wps.png"></a></div>
                                                        </div>
                                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 item">
                                                            <div class="item-inner"><a href="#">
                                                                <img alt="Upportdash" src="/img/clientslogo/wsd.png"></a></div>
                                                        </div>
                                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 item">
                                                            <div class="item-inner"><a href="#">
                                                                <img alt="Upportdash" src="/img/clientslogo/wso.png"></a></div>
                                                        </div>
                                                    </div>
                                                    <div class="item">
                                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 item">
                                                            <div class="item-inner"><a href="#">
                                                                <img alt="Upportdash" src="/img/clientslogo/wsr.png"></a></div>
                                                        </div>
                                                        <%-- <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 item">
                                                            <div class="item-inner"><a href="#"><img alt="Upportdash" src="/img/clientslogo/wps.png"></a></div>
                                                        </div>
                                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 item">
                                                            <div class="item-inner"><a href="#"><img alt="Upportdash" src="/img/clientslogo/wsd.png"></a></div>
                                                        </div>
                                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 item">
                                                            <div class="item-inner"><a href="#"><img alt="Upportdash" src="/img/clientslogo/wso.png"></a></div>
                                                        </div>--%>
                                                    </div>
                                                </div>



                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 wow fadeInUp">
                                        <div class="clients-title">
                                            <h3 class="title">&nbsp;</h3>
                                            <div class="carousel-controls pull-right">
                                                <a class="prev" href="#client-carousel" data-slide="prev"><i class="fa fa-angle-left"></i></a>
                                                <a class="next" href="#client-carousel" data-slide="next"><i class="fa fa-angle-right"></i></a>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /Our Clients -->



                <!-- Random Facts -->
                <div class=" margin-top70" id="random-facts">
                    <div class="pattern-overlay">
                        <div class="container">

                            <div class="row">
                                <asp:Repeater ID="rptStat" runat="server">
                                    <ItemTemplate>

                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 wow fadeIn animated" style="visibility: visible;">
                                            <div class="random-box padding-top30 padding-bottom40">
                                                <div class="random-box-icon">
                                                    <i class='<%# Eval("CLASS")%>'></i>
                                                </div>
                                                <div class="random-box-info">
                                                    <span class="facts" data-speed="5000" data-refresh-interval="50" data-to='<%# Eval("CNT")%>' data-from="0"><%# Eval("CNT")%></span>
                                                    <p>
                                                        <%# Eval("CATEGORY")%>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>

                                    </ItemTemplate>
                                </asp:Repeater>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /Random Facts -->

            </section>
            <!-- /Main Section -->
            <!-- Footer -->
            <footer id="footer">
                <div class="pattern-overlay">
                    <!-- Footer Bottom -->
                    <div class="footer-bottom">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-xs-12 col-sm-6 ">
                                    <p class="credits">Copyright &copy; 2018 <a href="#">GEMS Education</a>. All Rights Reserved. </p>
                                </div>
                                <div class="col-lg-6 col-md-6 col-xs-12 col-sm-6 ">
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /Footer Bottom -->
                </div>
            </footer>
            <!-- Modal -->
            <section id="modals">
                <!-- Login Modal -->
                <div class="modal login fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="loginModal" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h2 class="form-signin-heading modal-title" id="myModalLabel">Login</h2>
                            </div>
                            <form method="post" id="login">
                                <div class="modal-body">
                                    <fieldset>
                                        <div class="row">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <input class="form-control" id="username" name="username" type="text" placeholder="Username" value="" required>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <input class="form-control" type="email" id="email" name="email" placeholder="Email" value="" required>
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                                <div class="modal-footer">
                                    <a href="password-recovery.html" class="pull-left">(Lost Password?)</a>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <button type="button" class="btn btn-color">Login</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- /Logan Modal -->
                <!-- Registration Modal -->
                <div class="modal register fade" id="registrationModal" tabindex="-1" role="dialog" aria-labelledby="registrationModal" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h2 class="form-signin-heading modal-title" id="registrationModalLabel">Create a new account</h2>
                            </div>
                            <form method="post" id="registration">
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="form-group">
                                            <div class="col-md-6">
                                                <input type="text" value="" class="form-control" placeholder="First Name">
                                            </div>
                                            <div class="col-md-6">
                                                <input type="text" value="" class="form-control" placeholder="Last Name">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <input type="text" value="" class="form-control" placeholder="E-mail Address">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group">
                                            <div class="col-md-6">
                                                <input type="password" value="" class="form-control" placeholder="Password">
                                            </div>
                                            <div class="col-md-6">
                                                <input type="password" value="" class="form-control" placeholder="Re-enter Password">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <button type="button" class="btn btn-color">Register</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- /Registration Modal -->
            </section>
            <!-- Scroll To Top -->
            <a href="#" class="scrollup"><i class="fa fa-angle-up"></i></a>
        </div>
        <!-- /Wrap -->
    </form>
    <!-- The Scripts -->
    <script src="/js/jquery.min.js"></script>
    <script src="/js/jquery-migrate-1.0.0.js"></script>
    <script src="/js/jquery-ui.js"></script>
    <script src="/js/bootstrap.js"></script>
    <script src="/js/revolution-slider/js/jquery.themepunch.plugins.min.js"></script>
    <script src="/js/revolution-slider/js/jquery.themepunch.revolution.min.js"></script>
    <script src="/js/jquery.parallax.js"></script>
    <script src="/js/jquery.wait.js"></script>
    <script src="/js/fappear.js"></script>
    <script src="/js/modernizr-2.6.2.min.js"></script>
    <script src="/js/jquery.bxslider.min.js"></script>
    <script src="/js/jquery.prettyPhoto.js"></script>
    <script src="/js/superfish.js"></script>

    <script src="/js/jquery.gmap.min.js"></script>
    <script src="/js/jquery.sticky.js"></script>
    <script src="/js/jquery.countTo.js"></script>
    <script src="/js/jflickrfeed.js"></script>
    <script src="/js/imagesloaded.pkgd.min.js"></script>
    <script src="/js/waypoints.min.js"></script>
    <script src="/js/wow.js"></script>
    <script src="/js/jquery.fitvids.js"></script>
    <script src="/js/spectrum.js"></script>
    <%--<script src="/js/switcher.js"></script>--%>
    <script src="/js/custom.js"></script>
</body>


</html>

