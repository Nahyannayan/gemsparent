﻿Imports Microsoft.ApplicationBlocks.Data
Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Text
Imports System.Net.Mail
Imports System.DirectoryServices
Imports System.Web.Security
Partial Class Login
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub btnLogin_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLogin.Click

        Try

            Dim str_conn2 As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString

            ''check is AD suer or not returs true if not in AD
            Dim isNotAdUser As Boolean = False
            Dim pParms1(1) As SqlClient.SqlParameter
            pParms1(0) = New SqlClient.SqlParameter("@USR_NAME", txtLoginId.Text)
            Using reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn2, CommandType.StoredProcedure, "dbo.Is_Not_AD_User_online", pParms1)
                While reader.Read
                    'Session("sUsr_name") = txtUsername.Text

                    isNotAdUser = Convert.ToBoolean(reader("usr_not_in_ad"))

                End While
            End Using
            Dim Authenticated As Boolean = False


            If checkAD_BSU() = 1 Then



                If isNotAdUser Then
                    ''do something if not ad user
                    lblmsg.CssClass = "alert-warning"
                    lblmsg.Text = "Invalid Username/Password"
                    Exit Sub
                Else
                    If ValidateActiveDirectoryLogin("LDAP://10.100.100.5", txtLoginId.Text, txtPass.Text) Then
                        Authenticated = True
                    Else
                        lblmsg.CssClass = "alert-warning"
                        lblmsg.Text = "Invalid Username/Password"
                        Exit Sub
                    End If
                End If


            End If


            Dim Email_send As New Emailsending
            Dim passwordEncr As New Encryption64
            Dim password As String = passwordEncr.Encrypt(txtPass.Text)
            Dim con As String = ConnectionManger.GetOASISConnectionString
            Dim param(4) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@username", txtLoginId.Text)
            param(1) = New SqlClient.SqlParameter("@pass", password)
            param(2) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
            param(2).Direction = ParameterDirection.ReturnValue
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "[ONLINE].[OASISGETONLINE_USERS_V2]", param)
            If param(2).Value = "0" Then
                If ds.Tables(0).Rows.Count > 0 Then
                    StoreDatasForSession(ds)
                    If Session("bPasswdChanged") = "False" Then
                        Response.Redirect("~\UpdateInfo\ForcePasswordChange.aspx", False)
                    ElseIf Session("bPasswdUpdate") = "True" Then
                        Response.Redirect("~\UpdateInfo\Changepassword.aspx", False)

                    ElseIf Session("bUpdateContactDetails") = "False" Then
                        'commented by nahyan on 4thmay 2014...
                        'Response.Redirect(Session("ForceUpdate_stud"), False)
                        'commentend s here by nahyan on 4thmay 2014...

                        Dim isNewContact As Boolean = Is_New_Contact_Details(Session("sBsuid").ToString)
                        Session("isNewContact") = isNewContact

                        If isNewContact Then
                            Session("ForceUpdate_stud") = "~\StudProf\StudentMainDetails.aspx"
                            Response.Redirect("~\StudProf\StudentMainDetails.aspx", False)
                        Else
                            Session("ForceUpdate_stud") = "~\UpdateInfo\ForceUpdatecontactdetails.aspx"
                            Response.Redirect(Session("ForceUpdate_stud"), False)
                        End If

                    Else
                        'If Session("Active") = "F" Then
                        '    Response.Redirect("Home_TC.aspx", False)
                        'Else


                        Response.Redirect("~\Home.aspx", False)
                        ' End If
                    End If
                    HttpContext.Current.ApplicationInstance.CompleteRequest()
                Else
                    '  lblError.Text = UtilityObj.getErrorMessage("560")
                End If
            Else
                ' lblError.Text = UtilityObj.getErrorMessage(param(2).Value)
                If param(2).Value = "560" Then
                    Dim olu_id = Convert.ToString(ds.Tables(0).Rows(0)("OLU_ID"))
                    Dim Return_msg As String = Email_send.Emailsend(olu_id)
                    'Dim lblmsg As Label = CType(Page.Master.FindControl("lblSiteMessage"), Label)
                    lblmsg.CssClass = "alert-warning"
                    lblmsg.Text = Return_msg

                Else
                    'Dim lblmsg As Label = CType(Page.Master.FindControl("lblSiteMessage"), Label)
                    lblmsg.CssClass = "alert-warning"
                    lblmsg.Text = "Invalid Username/Password"

                    ' lblError.Text = lblError.Text + "  " + Return_msg
                End If


            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "Login PG")
            ' lblError.Text = UtilityObj.getErrorMessage("1000")
        End Try
    End Sub

    Function checkAD_BSU() As Integer
        Dim isadBSu As Integer
        Dim str_conn2 As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim pParms1(1) As SqlClient.SqlParameter
        pParms1(0) = New SqlClient.SqlParameter("@olu_name", txtLoginId.Text)
        Using reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn2, CommandType.StoredProcedure, "get_AD_BSU_active", pParms1)
            While reader.Read
                'Session("sUsr_name") = txtUsername.Text

                isadBSu = Convert.ToBoolean(reader("bAD"))

            End While
        End Using
    End Function
    Private Sub StoreDatasForSession(ByVal ds As DataSet)
        For Each dr As DataRow In ds.Tables(0).Rows
            Session("USERDISPLAYNAME") = dr("OLU_DISPLAYNAME")
            Session("OLU_ID") = dr("OLU_ID")
            Session("username") = dr("OLU_NAME")
            Session("OLU_bsuid") = dr("OLU_BSU_ID")
            Session("BSU_NAME") = dr("BSU_NAME")
            Session("lastlogtime") = dr("lastlogtime")
            Session("logincount") = dr("OLU_LoginCount")
            Session("Login_STUID") = Convert.ToString(dr("STU_ID"))
            Session("STU_ID") = Convert.ToString(dr("STU_ID"))
            Session("STU_NAME") = Convert.ToString(dr("STU_NAME"))
            Session("STU_NO") = Convert.ToString(dr("STU_NO"))
            Session("STU_ACD_ID") = Convert.ToString(dr("STU_ACD_ID"))
            Session("sBsuid") = Convert.ToString(dr("STU_BSU_ID"))
            Session("STU_GRD_ID") = Convert.ToString(dr("STU_GRD_ID"))
            Session("STU_SCT_ID") = Convert.ToString(dr("STU_SCT_ID"))
            Session("ACD_CLM_ID") = Convert.ToString(dr("ACD_CLM_ID"))
            Session("STU_SIBLING_ID") = Convert.ToString(dr("STU_SIBLING_ID"))
            Session("BSU_CURRENCY") = Convert.ToString(dr("BSU_CURRENCY"))
            Session("CUR_DENOMINATION") = Convert.ToString(dr("CUR_DENOMINATION"))
            Session("ACY_DESCR") = Convert.ToString(dr("ACY_DESCR"))
            Session("sroleid") = "1"
            Session("STU_BSU_ID") = Convert.ToString(dr("STU_BSU_ID"))
            Session("SUser_Name") = Convert.ToString(dr("STU_NAME"))
            Session("CLM") = Convert.ToString(dr("ACD_CLM_ID")) '
            Session("sModule") = "FF"
            Session("bPasswdChanged") = Convert.ToString(dr("OLU_bPasswdChanged"))
            Session("bUpdateContactDetails") = Convert.ToString(dr("OLU_bUpdateContactDetails"))
            Session("bPasswdUpdate") = Convert.ToString(dr("OLU_bPasswdUpdate"))
            Session("Active") = Convert.ToString(dr("ACTIVE"))
            Session("IsSelected") = True
            Session("STU_STM_ID") = Convert.ToString(dr("STU_STM_ID"))
            Session("STU_FEE_ID") = Convert.ToString(dr("STU_FEE_ID"))
            Session("IsNotif") = "1"
            Session("BSU_bFPSEnabled") = Convert.ToBoolean(dr("BSU_bFPSEnabled")) '---------added by Jacob on 22Dec2019 for GWS Fee Protection System
        Next
       
    End Sub

    Protected Sub lbtnForgot_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbtnForgot.Click
        'Response.Redirect("~\UpdateInfo\PasswordRet.aspx")
        Response.Redirect("https://selfreset.gemseducation.com")
    End Sub

    Private Function Is_New_Contact_Details(ByVal bsu_id As String) As Boolean
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim isNew As Boolean = False
        Dim param(2) As SqlParameter
        param(0) = New SqlParameter("@bsuId", bsu_id)
        param(1) = New SqlParameter("@OLU_ID", Session("OLU_ID"))
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "Get_Is_New_Contact_Details_By_bsu", param)
        If ds.Tables(0).Rows.Count > 0 Then
            Dim dt As New DataTable
            dt = ds.Tables(0)
            isNew = Convert.ToBoolean(dt.Rows(0)("bsu_contact_details_new").ToString)
        End If
        Return isNew
    End Function
    Public Sub bind_Stat()
        Dim conn As String = ConnectionManger.GetOASISConnectionString
        Dim param(5) As SqlParameter
        param(0) = New SqlParameter("@INFO_TYPE", "SCHOOL")

        Using datareader As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "OPL.OASIS_STATS", param)
            rptStat.DataSource = datareader
            rptStat.DataBind()
        End Using
    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim usrname As String = ""
        'Session("CLEV_MODULE") = ""

        bind_Stat()
        If Page.IsPostBack = False Then

            If Not (Request.QueryString("uid") Is Nothing) Then
                usrname = Encr_decrData.Decrypt(Request.QueryString("uid").Replace(" ", "+"))
                If usrname.Length > 25 Then
                    txtLoginId.Text = ""
                Else
                    txtLoginId.Text = usrname
                End If

            End If
            If Not (Request.QueryString("mode") Is Nothing) Then
                Session("CLEV_MODULE") = Encr_decrData.Decrypt(Request.QueryString("mode").Replace(" ", "+"))
            End If

        End If
    End Sub
    Private Function ValidateActiveDirectoryLogin(ByVal Domain As String, ByVal Username As String, ByVal Password As String) As Boolean
        Dim Success As Boolean = False
        Dim Entry As New System.DirectoryServices.DirectoryEntry(Domain, Username, Password)
        Dim Searcher As New System.DirectoryServices.DirectorySearcher(Entry)
        Searcher.SearchScope = DirectoryServices.SearchScope.OneLevel
        Try
            Dim Results As System.DirectoryServices.SearchResult = Searcher.FindOne
            Success = Not (Results Is Nothing)
        Catch exception As Exception
            Dim exm As String = exception.Message
            Success = False
        End Try
        Return Success
    End Function
    
  
End Class
