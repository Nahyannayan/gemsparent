<%@ Page Title="" Language="VB" MasterPageFile="~/ParentMaster.master" AutoEventWireup="false"
    CodeFile="ServiceRequest.aspx.vb" Inherits="ParentLogin_Service_ServiceRequest" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphParent" runat="Server">
    <%--    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
--%>
<%--  <link href="../CSS/SiteStyle.css" rel="stylesheet" type="text/css" />--%>
    <script src="../Scripts/jquery-1.8.2.min.js" type="text/javascript"></script>

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
          
           
                        <div class="mainheading">
                            <div class="left">
                                Request Services
                            </div>
                            <div class="right">
                                <asp:LinkButton ID="lnkViewServices" CssClass="currChildLink" CausesValidation="false"
                                    OnClientClick="javascript:hideshowResetPass();return false;" runat="server">View Services</asp:LinkButton>
                            </div>
                        </div>
                  <table style="width: 100%" class="BlueTable_simple"">
               
                <tr align="left">
                    <td class="tdfields" style="width: 170px">
                        Student
                    </td>
                    <td class="tdfields" align="center" style="width: 2px">
                        :
                    </td>
                    <td >
                        <asp:DropDownList ID="ddlStudents" runat="server" Width="210px" AutoPostBack="True" Enabled="false">
                        </asp:DropDownList>
                       <%-- <telerik:RadComboBox ID="ddlStudents" runat="server" AutoPostBack="True" Enabled="false"
                            Width="210px">
                        </telerik:RadComboBox>--%>
                    </td>
                </tr>
                <tr align="left">
                    <td class="tdfields">
                        Academic Year
                    </td>
                    <td class="tdfields" align="center">
                        :
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True" Width="210px">
                        </asp:DropDownList>
                     <%--   <telerik:RadComboBox ID="ddlAcademicYear" runat="server" AutoPostBack="True" Width="210px">
                        </telerik:RadComboBox>--%>
                    </td>
                </tr>
                <tr align="left">
                    <td class="tdfields">
                        Service Category
                    </td>
                    <td class="tdfields" align="center">
                        :
                    </td>
                    <td >
                        <asp:DropDownList ID="ddlSVCategory" runat="server" AutoPostBack="True" Width="210px">
                        </asp:DropDownList>
                  <%--      <telerik:RadComboBox ID="ddlSVCategory" runat="server" AutoPostBack="True" Width="210px">
                        </telerik:RadComboBox>--%>
                        <%--javascript:confirm('all the unsaved data will loose when you change the category')--%>
                    </td>
                </tr>
                <tr align="left">
                    <td class="tdfields">
                        <asp:Label ID="lblLanguage" runat="server" Text="Service"></asp:Label>
                    </td>
                    <td class="tdfields" align="center">
                        :
                    </td>
                    <td >
                    <%--    <telerik:RadComboBox ID="ddlServices" runat="server" AutoPostBack="True" Width="210px">
                        </telerik:RadComboBox>--%>
                        <asp:DropDownList ID="ddlServices" runat="server" AutoPostBack="True" Width="210px">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr align="left" runat="server" id="tr_CurrentTeacher">
                    <td class="tdfields">
                        Current Teacher
                    </td>
                    <td class="tdfields" align="center">
                        :
                    </td>
                    <td >
                        <asp:TextBox ID="txtCurrentTeacher" runat="server" Height="50px" SkinID="MultiText"
                            TextMode="MultiLine" Width="210px"></asp:TextBox>
                    </td>
                </tr>
                <tr align="left" runat="server" id="tr_Level">
                    <td class="tdfields">
                        Level<span style="color: Red;">*</span>
                    </td>
                    <td class="tdfields" align="center">
                        :
                    </td>
                    <td>
                       <%-- <telerik:RadComboBox ID="ddlLevels" runat="server" EmptyMessage="<--Select One-->"
                            Width="210px">
                            <Items>
                                <telerik:RadComboBoxItem runat="server" Text="<--Select One-->" Value="<--Select One-->" />
                                <telerik:RadComboBoxItem runat="server" Text="Beginner" Value="1" />
                                <telerik:RadComboBoxItem runat="server" Text="Intermediate" Value="2" />
                                <telerik:RadComboBoxItem runat="server" Text="Advanced" Value="3" />
                            </Items>
                        </telerik:RadComboBox>--%>
                        <asp:DropDownList ID="ddlLevels" runat="server" EmptyMessage="<--Select One-->"
                            Width="210px">
                            <asp:ListItem Text="<--Select One-->" Value="<--Select One-->" ></asp:ListItem>
                             <asp:ListItem Text="Beginner" Value="1"  ></asp:ListItem>
                               <asp:ListItem Text="Intermediate" Value="2"  ></asp:ListItem>
                               <asp:ListItem Text="Advanced" Value="3" ></asp:ListItem>                           
                            
                        </asp:DropDownList>
                        
                        <asp:CompareValidator runat="server" ID="Comparevalidator1" ValueToCompare="<--Select One-->"
                            Operator="NotEqual" ControlToValidate="ddlLevels" ErrorMessage="Level Required"
                            CssClass="validationClass" ValidationGroup="Addterm" />
                    </td>
                </tr>
                <tr align="left" runat="server" id="trOwnIns" style="display: none">
                    <td class="tdfields">
                        Own instrument ?
                    </td>
                    <td class="tdfields" align="center">
                        :
                    </td>
                    <td >
                        <asp:RadioButtonList ID="rbOwnIns" runat="server" RepeatDirection="Horizontal">
                            <asp:ListItem Value="1">Yes</asp:ListItem>
                            <asp:ListItem Selected="True" Value="0">No</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr align="left" runat="server" id="tr_Terms">
                    <td class="tdfields">
                        Term fee details
                        <%--        <asp:GridView ID="gvTerms" runat="server" CssClass="rgMasterTable">
                    </asp:GridView>--%>
                    </td>
                    <td class="tdfields" align="center">
                        :
                    </td>
                    <td>
                        <telerik:RadGrid ID="gvTerms" runat="server" AutoGenerateColumns="false">
                            <MasterTableView GridLines="Both">
                                <Columns>
                                    <telerik:GridTemplateColumn UniqueName="colImgDetailView">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="cbTerm" runat="server" Checked="true" />
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Term">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTRM_DESCRIPTION" runat="server" Text='<%# Bind("TRM_DESCRIPTION") %>'></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Term fees">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTERMFEES" runat="server" Text='<%# Bind("TERMFEES") %>'></asp:Label>
                                            <asp:HiddenField ID="hf_TRMID" runat="server" Value='<%# Bind("TRM_ID") %>' />
                                            <asp:HiddenField ID="hf_STM_ID" runat="server" Value='<%# Bind("STM_ID") %>' />
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridBoundColumn DataField="STM_START_DATE" HeaderText="Start date" DataFormatString="{0:dd/MMM/yyyy}">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="STM_CLOSE_DATE" HeaderText="Close date" DataFormatString="{0:dd/MMM/yyyy}"
                                        Visible="false">
                                    </telerik:GridBoundColumn>
                                </Columns>
                            </MasterTableView>
                        </telerik:RadGrid>
                    </td>
                </tr>
                <tr id="tr_Music" runat="server">
                    <td colspan="3" align="center">
                        <asp:HiddenField ID="HF_svc_count" runat="server" />
                        <asp:HiddenField ID="HF_MAX_SVC" runat="server" />
                        <asp:Button ID="btnRequestService" runat="server" Text="Add" Width="55px" ValidationGroup="Addterm"
                            CssClass="button" ToolTip="Click here to add the selected terms" />
                    </td>
                </tr>
           <%--     </table>
                <table width="100%"  class="tableNoborder" >--%>
                 <tr id="tr_MusicServiceReq" runat="server">
                    <td  colspan="3">
                        <asp:GridView ID="gvServicesRequested" runat="server" AutoGenerateColumns="False"
                            SkinID="GridViewNormal" Width="100%" Style="text-indent: 5px" ShowFooter="True">
                            <Columns>
                                <asp:TemplateField HeaderText="Student Name">
                                    <ItemTemplate>
                                        <asp:Label ID="lblSTU_Name" runat="server" Text='<%# Bind("STU_Name") %>'></asp:Label>
                                        <asp:HiddenField ID="hf_STU_ID" runat="server" Value='<%# Bind("STU_ID") %>' />
                                        <asp:HiddenField ID="hf_STM_ID" runat="server" Value='<%# Bind("ID") %>' />
                                        <asp:HiddenField ID="HF_SVG_ID" runat="server" Value='<%# Bind("SVG_ID") %>' />
                                        <asp:HiddenField ID="HF_SVC_ID" runat="server" Value='<%# Bind("SVC_ID") %>' />
                                        <asp:HiddenField ID="HF_ACD_ID" runat="server" Value='<%# Bind("ACD_ID") %>' />
                                        <asp:HiddenField ID="HF_Level" runat="server" Value='<%# Bind("LEVEL") %>' />
                                        <asp:HiddenField ID="HF_Teacher" runat="server" Value='<%# Bind("CurrentTeacher") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Academic Year" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:Label ID="lblACADEMIC_YEAR" runat="server" Text='<%# Bind("ACADEMIC_YEAR") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Language" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:Label ID="lblSERVICE" runat="server" Text='<%# Bind("SERVICE") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Term" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:Label ID="lblTRM_DESCRIPTION" runat="server" Text='<%# Bind("TERM") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Amount" ItemStyle-HorizontalAlign="Right">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAMOUNT" runat="server" Text='<%# Bind("AMOUNT") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Delete" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="img_delete" runat="server" ToolTip="Delete the Service request"
                                            ImageUrl="~/Images/DELETE.png" OnClick="img_delete_Click" Width="15px" Height="15px" />
                                    </ItemTemplate>
                                    <HeaderStyle Width="10px" />
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
                
                <tr  align="left" id="tr_ServiceStartdate" runat="server">
                    <td class="tdfields">
                        Start Date<span style="color: Red">*</span>
                    </td>
                    <td class="tdfields" align="center">
                        :
                    </td>
                    <td>
                        <asp:TextBox ID="txtfromdate" runat="server" Width="150px" AutoPostBack="false"></asp:TextBox>
                        <asp:ImageButton ID="imgFrom" runat="server" ImageUrl="~/Images/Common/PageBody/Calendar.png"
                            TabIndex="4" />
                        &nbsp;
                        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="txtfromdate"
                            ErrorMessage=" Start Date Required" ValidationGroup="save">*</asp:RequiredFieldValidator>
                        <ajaxToolkit:CalendarExtender ID="DocDate" runat="server" Format="dd/MMM/yyyy" PopupButtonID="imgFrom"
                            TargetControlID="txtfromdate" PopupPosition="BottomRight">
                        </ajaxToolkit:CalendarExtender>
                        <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MMM/yyyy"
                            PopupButtonID="txtfromdate" TargetControlID="txtfromdate" PopupPosition="BottomRight">
                        </ajaxToolkit:CalendarExtender>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ValidationGroup="save"
                            ErrorMessage="Not a valid date, please try like this(01/Jan/2013) " ControlToValidate="txtfromdate"
                            ValidationExpression="^(([0-9])|([0-2][0-9])|([3][0-1]))\/(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec|jan|feb|mar|apr|may|jun|jul|aug|sep|oct|nov|dec)\/\d{4}$"></asp:RegularExpressionValidator>
                    
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="true"
                            ShowSummary="false" ValidationGroup="save" />
                    </td>
                </tr>
                <tr>
                <td colspan="2" ></td>
                <td align="left" >  <asp:Button ID="btn_SaveRequest" runat="server" Text="Save & proceed for payment"
                            ValidationGroup="save" CssClass="buttons_grid" ToolTip="Click here to Save & proceed for payment" />
                        <ajaxToolkit:ConfirmButtonExtender ID="cbe" runat="server" DisplayModalPopupID=""
                            TargetControlID="btn_SaveRequest" ConfirmText="Please confirm to proceed for Registration & Payment." />
                            <div>
                        <asp:ValidationSummary ID="vSUMMARY" runat="server" ShowMessageBox="true" ShowSummary="false"
                            ValidationGroup="Addterm" />
                        <asp:Label ID="lblerror" runat="server" EnableViewState="False" ForeColor="Red"></asp:Label>
                        </div>
                </td></tr>
               
      </table>
                     

              
                 
           
            <asp:Panel ID="pnlSvcCatChange" runat="server" CssClass="darkPanlvisible" Visible="false">
                <div class="panelQual">
                    <center>
                        <table class="tableNoborder">
                            <tr>
                                <td align="center" style="font-weight: bold">
                                    If you change the service category,then you will loose unsaved datas</td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button ID="btn_Proceed" runat="server" Text="Proceed" CssClass="buttons_grid" />&nbsp;&nbsp;<asp:Button
                                        ID="btn_cancel" runat="server" Text="Cancel" CssClass="buttons_grid" /></td>
                            </tr>
                        </table>
                    </center>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlProceed2Payment" runat="server" CssClass="darkPanlvisible" Visible="false">
                <div class="panelQual">
                    <table width="80%" style="height: 170px" cellpadding="0" cellspacing="0" class="tableNoborder">
                        <tr>
                            <td colspan="2" align="left">
                                <span style="font-size: small; font-weight: bold">
                                    <asp:Label ID="lblProceed2Payment" runat="server" Text="" ForeColor="Red"></asp:Label>
                                    Click<asp:LinkButton ID="lnkbtnProceed2Payment" runat="server" Font-Bold="true" Font-Size="Small"> here</asp:LinkButton>
                                    to proceed for payment</span>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="2">
                                <%-- <asp:Button ID="btnCancelProceed2Payment" runat="server" Text="Cancel" 
                        CausesValidation="false"                         
                        CssClass="button"  />--%>
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
            <div id="divApplicantNotes" runat="server" class="darkPanlvisible" style="display: none">
                <div class="panelQual">
                    <div class="mainheading">
                        <div class="left">
                            Already Requested Services
                        </div>
                        <div class="right">
                            <asp:LinkButton ForeColor="red" ID="lbtnApplicantNotesClose" ToolTip="click here to close"
                                CssClass="" runat="server" Text="X" Font-Underline="false" CausesValidation="false"
                                OnClientClick="javascript:hideshowResetPass();return false;">X</asp:LinkButton>
                        </div>
                    </div>
                    <table width="100%" cellpadding="0" cellspacing="0" class="tableNoborder">
                        <tr>
                            <td class="tdfields">
                                <div style="overflow: auto; height: 300px">
                                    <telerik:RadGrid ID="gvStudentServicesDetails" runat="server" AutoGenerateColumns="False"
                                        GridLines="None" Width="99%">
                                        <MasterTableView GridLines="Both">
                                            <Columns>
                                                <telerik:GridTemplateColumn HeaderText="Name">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSTU_Name0" runat="server" Text='<%# Bind("STU_Name") %>'></asp:Label>
                                                        <asp:HiddenField ID="hf_STU_ID0" runat="server" Value='<%# Bind("STU_ID") %>' />
                                                        <asp:HiddenField ID="HF_SVG_ID0" runat="server" Value='<%# Bind("SVG_ID") %>' />
                                                        <asp:HiddenField ID="hf_STM_ID0" runat="server" Value='<%# Bind("STM_ID") %>' />
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                </telerik:GridTemplateColumn>
                                                <telerik:GridTemplateColumn HeaderText="Academic Year">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblACADEMIC_YEAR0" runat="server" Text='<%# Bind("ACADEMIC_YEAR") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </telerik:GridTemplateColumn>
                                                <telerik:GridTemplateColumn HeaderText="Language">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSERVICE0" runat="server" Text='<%# Bind("SERVICE") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                </telerik:GridTemplateColumn>
                                                <telerik:GridTemplateColumn HeaderText="Term">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblTRM_DESCRIPTION0" runat="server" Text='<%# Bind("TERM") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                </telerik:GridTemplateColumn>
                                                <telerik:GridTemplateColumn HeaderText="Amount">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAMOUNT0" runat="server" Text='<%# Bind("AMOUNT") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Right" />
                                                </telerik:GridTemplateColumn>
                                                <telerik:GridTemplateColumn HeaderText="Status">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAPP_Status" runat="server" Text='<%# Bind("APP_Status") %>'></asp:Label>
                                                        <%--<asp:Image ID="IMG_STATUS" runat="server" ImageUrl='<%# Eval("APP_Status") %>' Height="20px" />--%>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle Width="10px" />
                                                </telerik:GridTemplateColumn>
                                            </Columns>
                                        </MasterTableView>
                                    </telerik:RadGrid>
                                </div>
                            </td>
                            <td class="tdfields">
                                &nbsp;
                            </td>
                        </tr>
                        <%--<tr style="height: 40px">
                    <td class="tdfields" align="right">
                        <table class="style1">
                            <tr>
                                <td>
                                    <img alt="" class="style2" src="../Images/APPLY.png" />
                                    Approved</td>
                                <td>
                                    <img alt="s" class="style2" src="../Images/PENDING.png" />Pending</td>
                                <td>
                                    <img alt="s" class="style2" src="../Images/DELETE.png" />Rejected</td>
                            </tr>
                        </table>
                    </td>
                    <td>
                    </td>
                </tr>--%>
                        <tr>
                            <td align="center" colspan="2">
                                <asp:Button ID="btnCancelResetPassword" runat="server" Text="Close" CausesValidation="false"
                                    OnClientClick="javascript:hideshowResetPass();return false;" CssClass="button" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>

            <script type="text/javascript" language="javascript">
            function handleError()
{
return true;
}
window.onerror = handleError;
                function hideshowResetPass() {
                    var which = document.getElementById('<%= divApplicantNotes.ClientID %>')// document.getElementById('<%=divApplicantNotes.ClientID%>');
                    if (which.style.display == "block")
                        which.style.display = "none";
                    else
                        which.style.display = "block";
                  return false;
                }
                $(document).ready(function () {
                    $(document).click(function (e) {
                        //$('#<%= divApplicantNotes.ClientID %>').show("slow");
                        var lnkDocumentFwd = $('#<%= divApplicantNotes.ClientID %>');
                        var div = $('#<%= divApplicantNotes.ClientID %>').attr("id");
                        var which = document.getElementById('<%=divApplicantNotes.ClientID%>');
                        if (e.target.id == div) {
                            if (which.style.display == "block")
                                which.style.display = "none";
                        }
                    });
                });
            </script>

        </ContentTemplate>
    </asp:UpdatePanel>
    <%--   <asp:UpdateProgress ID="upProgGv" runat="server" DisplayAfter="1">
        <progresstemplate>
            <asp:Panel ID="pnlProgress" runat="server" CssClass="screenCenter">
                <br />
                <asp:Image ID="Image2" runat="server" ImageUrl="~/ParentLogin/Images/loading6.gif">
                </asp:Image><br />
                Please Wait....</asp:Panel>
            <ajaxToolkit:AlwaysVisibleControlExtender ID="avcProgress" runat="server" TargetControlID="pnlProgress"
                VerticalSide="Middle" HorizontalSide="Center" VerticalOffset="10" ScrollEffectDuration=".1"
                HorizontalOffset="10">
            </ajaxToolkit:AlwaysVisibleControlExtender>
        </progresstemplate>
    </asp:UpdateProgress>--%>
</asp:Content>
