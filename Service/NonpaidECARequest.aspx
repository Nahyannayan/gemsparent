﻿<%@ Page Language="VB" MasterPageFile="~/ParentMaster.master" AutoEventWireup="false"
    CodeFile="NonpaidECARequest.aspx.vb" Inherits="Service_NonpaidECARequest" Title="GEMS EDUCATION" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphParent" runat="Server">
    <script src="../Scripts/jquery-1.9.1.js" type="text/javascript"></script>
    <script type="text/javascript" src="../Scripts/jquery-1.4.3.min.js"></script>
    <script type="text/javascript" src="../Scripts/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
    <link rel="stylesheet" type="text/css" href="../Scripts/fancybox/jquery.fancybox-1.3.4.css"
        media="screen" />
    <script type="text/javascript">
        $(document).ready(function() {

            $(".frameParticipant").fancybox({
                type: 'iframe',
                maxWidth: 100,
                maxHeight: 650,
                fitToView: false,
                width: '63.1%',
                height: '96%',
                autoSize: false,
                closeClick: false,
                openEffect: 'none',
                closeEffect: 'none'
            });
        });
    </script>
    <div class="mainheading">
        <div class="left">
            Extra Curricular Activity Request</div>
        <div class="right">
            <asp:Label ID="lbChildName" runat="server">
            </asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
    </div>
    <table class="BlueTable_simple" width="100%" cellpadding="8" cellspacing="0">
    </table>
    <table align="center" border="5" bordercolor="#1b80b6" cellpadding="5" cellspacing="0"
        class="BlueTable_simpleASA" width="50%">
        <tr>
            <td class="subheader_img" colspan="6" align="center">
                <asp:GridView ID="gvActivity" runat="server" AutoGenerateColumns="False" EnableModelValidation="True"
                    Width="100%" PageSize="25" AllowPaging="true" OnRowDataBound="gvActivity_RowDataBound" OnPageIndexChanging="gvActivity_PageIndexChanging" CellSpacing="2">
                       <RowStyle CssClass="griditem" Height="20px"  />
                    <Columns>
                        <asp:TemplateField HeaderText="No." ItemStyle-Width="20px">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblSlNo"><%# Container.DataItemIndex + 1 %></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Category">
                            <ItemTemplate>
                                <asp:Label ID="lblCategory" runat="server" Text='<%# Bind("SSC_DESC") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle Width="25%" HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="CategoryId" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lblCategoryId" runat="server" Text='<%# Bind("SVC_ID") %>'></asp:Label>
                                 <asp:Label ID="lblSVGID" runat="server" Text='<%# Bind("SVG_ID") %>'></asp:Label>
                                 
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Activity">
                            <ItemTemplate>
                                <asp:Label ID="lblSubCategory" runat="server" Text='<%# Bind("SVC_DESCRIPTION") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle Width="25%" HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Max Seat">
                            <ItemTemplate>
                                <asp:Label ID="lblMaxSeat" runat="server" Text='<%# Bind("SVG_MAX_SEAT_CAPACITY") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle Width="20%" HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Available Seat" >
                            <ItemTemplate>
                               <asp:Label ID="lblAvailSeat" runat="server" Text='<%# Bind("AVAILABLE_COUNT") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle Width="20%" HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Action">
                            <ItemTemplate>
                               <a id="framePartcpnt" class="frameParticipant" href="NonpaidECARequestView.aspx?svg=<%#Eval("SVG_ID")%>">
                                               <asp:Label ID="lblStat" runat="server" Text='<%#Bind("STATUS_S") %>'></asp:Label></a>
                              
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle Width="20%" HorizontalAlign="Center" />
                        </asp:TemplateField>
                 
                       
                    </Columns>
                   
                     <SelectedRowStyle BackColor="Wheat" />
                                <HeaderStyle CssClass="gridheader_pop" Height="25px" HorizontalAlign="Center" VerticalAlign="Middle" />
                                <AlternatingRowStyle CssClass="griditem_alternative" />
                </asp:GridView>
            </td>
        </tr>
    </table>
</asp:Content>
