﻿Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Partial Class Service_NonpaidECARequestView
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            If Request.QueryString("svg") IsNot Nothing And Request.QueryString("svg") <> "" Then
                Try
                    Dim id As Integer = Convert.ToInt32(Request.QueryString("svg").ToString())
                    ViewState("SVG_ID") = id
                    ViewState("SSR_ID") = "0"
                    lblName.Text = Convert.ToString(Session("STU_NAME"))
                    lblStuNo.Text = Convert.ToString(Session("STU_NO"))
                    BindServiceDetails(id)

                    If ViewState("SSR_ID") = "0" Then
                        CheckMaxParamExceeded()
                    End If

                Catch ex As Exception
                    Dim exM As String = ex.Message
                End Try
               

            End If
        End If


    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click

        Me.SaveData()
    End Sub

    Private Sub SaveData()

        'If Not ValidateMandatoryActivity() Then Exit Sub

        Dim transaction As SqlTransaction



        Using conn As SqlConnection = ConnectionManger.GetOASIS_SERVICESConnection

          



            transaction = conn.BeginTransaction("SampleTransaction")
            Try

                Dim status As Integer
                Dim pParms(9) As SqlClient.SqlParameter
                Dim tmpStr, tmpstr1 As String

                For Each item As ListItem In chkPeriods.Items
                    If item.Selected = True Then
                        tmpStr += item.Text.Split("(")(0) & " & "
                        tmpstr1 += item.Value & "|"
                    End If
                Next
                If Not tmpStr Is Nothing AndAlso tmpStr.EndsWith(" & ") Then
                    tmpStr = tmpStr.Substring(0, tmpStr.Length - 3)
                End If
                If Not tmpstr1 Is Nothing AndAlso tmpstr1.EndsWith("|") Then
                    tmpstr1 = tmpstr1.Substring(0, tmpstr1.Length - 1)
                End If



                pParms(0) = New SqlClient.SqlParameter("@acd_ID", Session("stu_acd_id"))
                pParms(1) = New SqlClient.SqlParameter("@Grd_ID", Session("stu_grd_id"))
                pParms(2) = New SqlClient.SqlParameter("@usr_ID", "")
                pParms(3) = New SqlClient.SqlParameter("@stu_IDs", Session("STU_ID") & "^|")
                pParms(4) = New SqlClient.SqlParameter("@bsu_ID", Session("STU_BSU_ID"))
                pParms(5) = New SqlClient.SqlParameter("@SVG_ID", ViewState("SVG_ID"))
                pParms(6) = New SqlClient.SqlParameter("@STM_IDS", tmpstr1)
                pParms(7) = New SqlClient.SqlParameter("@Msg_Out", SqlDbType.VarChar)
                pParms(8) = New SqlClient.SqlParameter("@bOnline", "True")
                'pParms(7).Direction = ParameterDirection.Output
                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "ST.SAVESTUD_SERVICE_REQUEST", pParms)


                'Send the email to the parent
                Dim pParms1(4) As SqlClient.SqlParameter
                pParms1(0) = New SqlClient.SqlParameter("@bsu_ID", Session("sBsuid"))
                pParms1(1) = New SqlClient.SqlParameter("@acd_ID", Session("stu_acd_id"))
                pParms1(2) = New SqlClient.SqlParameter("@stu_ID", Session("STU_ID"))
                pParms1(3) = New SqlClient.SqlParameter("@olu_ID", Session("OLU_ID"))
                pParms1(4) = New SqlClient.SqlParameter("@SVG_ID", ViewState("SVG_ID"))
                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "SEND_ECA_PARENT_SERVICE_REQUEST_EMAIL", pParms1)


                ''check autoapproval is enabled

                Dim isAutoApp As Boolean = IsAutoApproval()

                If isAutoApp Then
                    Dim MaxCapacity As Integer
                    Dim ApprovedCount As Integer
                    Dim AvailableSeat As Integer = 0
                    Dim ds As DataSet
                    Dim dt22 As DataTable
                    Dim pParms2(3) As SqlClient.SqlParameter
                    pParms2(0) = New SqlParameter("@SVG_ID", ViewState("SVG_ID"))
                    ds = SqlHelper.ExecuteDataset(transaction, CommandType.StoredProcedure, "ST.GET_MAXSEAT_AND_AVAILABLE_CAPACITY", pParms2)

                    If ds.Tables(0).Rows.Count > 0 Then
                        dt22 = ds.Tables(0)
                        MaxCapacity = dt22.Rows(0).Item(1)
                        ApprovedCount = dt22.Rows(0).Item(0)

                        If MaxCapacity > ApprovedCount Then
                            AvailableSeat = MaxCapacity - ApprovedCount
                        End If

                    End If


                    If AvailableSeat >= 1 Then
                        Dim pParmsA(4) As SqlClient.SqlParameter
                        pParmsA(0) = New SqlClient.SqlParameter("@BSU_ID", Session("sBsuid"))
                        pParmsA(1) = New SqlClient.SqlParameter("@ACD_ID", Session("stu_acd_id"))
                        pParmsA(2) = New SqlClient.SqlParameter("@STU_ID", Session("STU_ID"))
                        pParmsA(3) = New SqlClient.SqlParameter("@SVG_ID", ViewState("SVG_ID"))
                        SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "ST.ECA_AUTO_APPROVE_REQUEST", pParmsA)
                    End If
                End If
               
                ''Auto approval operations ends here

                transaction.Commit()
                ' Me.lblMessage.Text = "<font color=blue>Your request have been saved successfully</font>"

                divNote.Visible = True
                divNote.Attributes("class") = "msgInfoBox msgInfoSuccess"
                lblError.Text = "<div>Your activity is now confirmed.</div>"
                btnSave.Visible = False
            Catch ex As Exception
                transaction.Rollback()
                UtilityObj.Errorlog(ex.Message)
                divNote.Visible = True
                divNote.Attributes("class") = "msgInfoBox msgInfoError"
                lblError.Text = "<div>There was an issue saving your request. </div>"
                'Me.lblMessage.Text = "<font color=red>There was an issue saving your request</font>"
            Finally

            End Try

        End Using
    End Sub

    Private Sub CheckMaxParamExceeded()
        Dim CONN As String = ConnectionManger.GetOASIS_SERVICESConnectionString
        Try
            Dim p1Parms(5) As SqlClient.SqlParameter

            p1Parms(0) = New SqlClient.SqlParameter("@ACD_ID", Convert.ToInt32(Session("stu_acd_id")))
            p1Parms(1) = New SqlClient.SqlParameter("@GRD_ID", Session("stu_grd_id"))

            p1Parms(2) = New SqlClient.SqlParameter("@STU_ID", Convert.ToInt32(Session("STU_ID")))
            p1Parms(3) = New SqlClient.SqlParameter("@BSU_ID", Session("STU_BSU_ID"))
            p1Parms(4) = New SqlClient.SqlParameter("@SVG_ID", ViewState("SVG_ID"))

            Dim checkPAram As Integer = SqlHelper.ExecuteScalar(CONN, CommandType.StoredProcedure, "ST.CHECK_ECA_BSU_PARAM_FOR_STUDENT", p1Parms)
            '' Dim ds As DataSet = SqlHelper.ExecuteDataset(CONN, CommandType.StoredProcedure, "ST.CHECK_ECA_BSU_PARAM_FOR_STUDENT", p1Parms)

            If checkPAram = -1 Then
                divNote.Visible = True
                divNote.Attributes("class") = "msgInfoBox2 msgInfoError"
                lblError.Text = "<div >Exceeded maximum allowed requests. </div>"
                btnSave.Visible = False
            ElseIf checkPAram = -2 Then
                divNote.Visible = True
                divNote.Attributes("class") = "msgInfoBox msgInfoError"
                lblError.Text = "<div >You have already selected activity on same day </div>"
                btnSave.Visible = False
                ''added bay nahyan /rashmi if the available capacity is full
            ElseIf checkPAram = -3 Then
                divNote.Visible = True
                divNote.Attributes("class") = "msgInfoBox msgInfoError"
                lblError.Text = "<div > Activity full </div>"
                btnSave.Visible = False
            End If
        Catch ex As Exception
            Dim exM As String = ex.Message
        End Try
    End Sub

    Private Function IsAutoApproval() As Boolean
        Dim isAutoAppr As Boolean = False
        Dim CONN As String = ConnectionManger.GetOASIS_SERVICESConnectionString
        Dim ds1 As DataSet
        Try
            Dim p1Parms(5) As SqlClient.SqlParameter
            p1Parms(0) = New SqlClient.SqlParameter("@ACD_ID", Session("stu_acd_id"))
            p1Parms(1) = New SqlClient.SqlParameter("@GRD_ID", Session("stu_grd_id"))
            p1Parms(2) = New SqlClient.SqlParameter("@BSU_ID", Session("STU_BSU_ID"))
            ds1 = SqlHelper.ExecuteDataset(CONN, CommandType.StoredProcedure, "SM.GET_ECA_BSU_PARAM_BY_BSU_ACD_GRD", p1Parms)

            If ds1.Tables IsNot Nothing Then
                If ds1.Tables.Count > 0 Then
                    Dim dt As DataTable = ds1.Tables(0)

                    isAutoAppr = Convert.ToBoolean(dt.Rows(0)("PARAM_AUTO_APPROVE"))
                End If
            End If
            Return isAutoAppr

        Catch ex As Exception

        End Try
    End Function

    Private Sub BindServiceDetails(ByVal svgId As Integer)
        Dim CONN As String = ConnectionManger.GetOASIS_SERVICESConnectionString

        Try
            Dim PARAM(5) As SqlParameter
           
            PARAM(1) = New SqlParameter("@SVG_ID", svgId)

            PARAM(2) = New SqlParameter("@STU_ID", Session("STU_ID"))
           

            Using SubCategoryDetailsReader As SqlDataReader = SqlHelper.ExecuteReader(CONN, CommandType.StoredProcedure, "ST.GET_SERVICEDETAILS_BY_STUDENT", PARAM)
                chkPeriods.Items.Clear()
                While SubCategoryDetailsReader.Read
                    lblCategory.Text = Convert.ToString(SubCategoryDetailsReader("SSC_DESC"))
                    lblActivity.Text = Convert.ToString(SubCategoryDetailsReader("SVC_DESCRIPTION"))
                    lblMaxSeat.Text = Convert.ToString(SubCategoryDetailsReader("SVG_MAX_SEAT_CAPACITY"))
                    lblAvailable.Text = Convert.ToString(SubCategoryDetailsReader("AVAILABLE_COUNT"))

                    If Convert.ToString(SubCategoryDetailsReader("SSR_ID")) <> "0" Then
                        ViewState("SSR_ID") = Convert.ToString(SubCategoryDetailsReader("SSR_ID"))
                    Else
                        ViewState("SSR_ID") = 0
                    End If

                    If Convert.ToString(SubCategoryDetailsReader("SSR_ID")) <> "0" Then
                        lblStatus.Text = Convert.ToString(SubCategoryDetailsReader("STATUS_S"))
                        trStatus.Visible = True
                    Else
                        trStatus.Visible = False
                    End If
                    lblStatus.Text = Convert.ToString(SubCategoryDetailsReader("STATUS_S"))
                    If Convert.ToString(SubCategoryDetailsReader("SSR_ID")) <> "0" Then
                        lblTermDesc.Text = Convert.ToString(SubCategoryDetailsReader("TermDesc"))
                    Else
                        chkPeriods.Items.Add(New ListItem( _
                                        Convert.ToString(SubCategoryDetailsReader("Duration")), _
                                        Convert.ToString(SubCategoryDetailsReader("STM_ID"))))

                    End If
                    lblRemarks.Text = Convert.ToString(SubCategoryDetailsReader("SVG_REMARKS"))

                    lblDays.Text = Convert.ToString(SubCategoryDetailsReader("TrainingDays"))
                    If Convert.ToString(SubCategoryDetailsReader("SSR_ID")) <> "0" Then
                        btnSave.Visible = False
                    Else
                        If Convert.ToString(SubCategoryDetailsReader("isRegOpen")) <> "0" Then
                            btnSave.Visible = True
                        Else
                            btnSave.Visible = False
                            divNote.Visible = True
                            divNote.Attributes("class") = "msgInfoBox msgInfoError"
                            lblError.Text = "<div >Registration for the selected activity is closed. </div>"
                        End If


                    End If
                End While

                For Each item As ListItem In chkPeriods.Items
                    item.Selected = True
                Next

            End Using


        Catch ex As Exception
            Dim ex1 As String = ex.Message
        End Try


    End Sub
End Class
