﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ParentMaster.master" AutoEventWireup="false"
    CodeFile="ViewServices.aspx.vb" Inherits="ParentLogin_Service_ViewServices" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphParent" runat="Server">
    <style type="text/css">
        .imgstyle
        {
            height:15px;
            width :15px
        }
        </style>
    <%--       <telerik:RadComboBox ID="ddlLevels" runat="server" EmptyMessage="<--Select One-->">
                            <Items>
                                <telerik:RadComboBoxItem runat="server" Text="<--Select One-->" Value="" />
                                <telerik:RadComboBoxItem runat="server" Text="Beginner" Value="1" />
                                <telerik:RadComboBoxItem runat="server" Text="Intermediate" Value="2" />
                                <telerik:RadComboBoxItem runat="server" Text="Advanced" Value="3" />
                            </Items>
                        </telerik:RadComboBox>
            <asp:Button ID="Button1" runat="server" Text="Button" />--%>
    <asp:LinkButton ID="LinkButton1" runat="server" Font-Bold="true" Font-Size="Small"
        CssClass="currChildLink" Width="200px" PostBackUrl="~/Service/ServiceRequest.aspx">Request a new service</asp:LinkButton>
    <table width="90%" class="tableNoborder">
        <tr>
            <td align="center" style="height: 25px">
                <div class="mainheading">
                    <div class="left">
                        Services</div>
                </div>
            </td>
        </tr>
        <tr>
            <td class="tdblankTop">
                <asp:GridView ID="gvStudentServicesDetails" runat="server" AutoGenerateColumns="False"
                    CssClass="gridheader_new" Width="100%" Style="text-indent: 5px" ShowFooter="false"
                    SkinID="GridViewNormal">
                    <columns>
                           <%--<asp:TemplateField HeaderText="Name" ItemStyle-HorizontalAlign="Left"  >
                              <ItemTemplate>
                                    <asp:Label ID="lblStu_Name" runat="server" Text='<%# Bind("STU_Name") %>' ></asp:Label>
                                </ItemTemplate> 
                           </asp:TemplateField>--%>
                             <asp:TemplateField  HeaderText="Requested Date" ItemStyle-HorizontalAlign="Center" >
                              <ItemTemplate>
                                    <asp:Label ID="lblRequestedDate" runat="server" Text='<%# Bind("SSR_REQUESTED_DT") %>' ></asp:Label>
                                </ItemTemplate>   
                           </asp:TemplateField>
                           
                              <asp:TemplateField HeaderText="Academic year" ItemStyle-HorizontalAlign="Center" >
                             <ItemTemplate >
                                    <asp:Label ID="lblACADEMIC_YEAR" runat="server" Text='<%# Bind("ACADEMIC_YEAR") %>' ></asp:Label>
                                </ItemTemplate>   
                           </asp:TemplateField> 
                              <asp:TemplateField HeaderText="Service" ItemStyle-HorizontalAlign="Left"  >
                                  <ItemTemplate >
                                    <asp:Label ID="lblSERVICE" runat="server" Text='<%# Bind("SERVICE") %>' ></asp:Label>
                                    <asp:HiddenField ID="HF_SVC_ID" runat="server" Value='<%# Bind("SVC_ID") %>' />   
                                     <asp:HiddenField ID="HF_SSR_ID" runat="server" Value='<%# Bind("SSR_ID") %>' />   
                                     <asp:HiddenField ID="hf_SSV_ID" runat="server" Value='<%# Bind("SSV_ID") %>' />   
                                      <asp:HiddenField ID="HF_Stu_Name" runat="server" Value='<%# Bind("STU_Name") %>' />   
                                     <asp:HiddenField ID="HF_link" runat="server" Value='<%# Bind("link") %>' />           
                                      <asp:HiddenField ID="HF_DISCONTINUED" runat="server" Value='<%# Bind("DISCONTINUED") %>' /> 
                                      <asp:HiddenField ID="HF_SSR_bPaid" runat="server" Value='<%# Bind("SSR_bPaid") %>' /> 
                                                                             
                                </ItemTemplate>   
                                
                           </asp:TemplateField>
                              <asp:TemplateField  HeaderText="Start date" ItemStyle-HorizontalAlign="Center" >
                              <ItemTemplate>
                                    <asp:Label ID="lblStartDate" runat="server" Text='<%# Bind("SSR_SERVICE_STARTDT") %>' ></asp:Label>
                                </ItemTemplate>   
                           </asp:TemplateField>                     
                              <asp:TemplateField HeaderText="Pay fee" ItemStyle-HorizontalAlign="Center"  >
                            <ItemTemplate>

                              <%--      <asp:ImageButton  ID="img_Discontinue" runat="server" ImageUrl="~/ParentLogin/Images/DELETE.png"  Width="15px" Height="15px" OnClick="img_Discontinue_Click"  />--%>

                                      <asp:LinkButton ID="lnk_paynow" runat="server" OnClick="lnk_paynow_Click" ForeColor="Blue"   Font-Bold="true"  >Pay Now</asp:LinkButton>
                                </ItemTemplate>
                           </asp:TemplateField>                                                                         
                              <asp:TemplateField HeaderText="Request">
                            <ItemTemplate>

                                    <asp:Image  ID="img_Discontinue" runat="server" ImageUrl="~/Images/DELETE.png" Visible="false" Width="15px" Height="15px"  />
                                       <asp:LinkButton ID="lnk_DeleteReq" runat="server" OnClick="lnk_DeleteReq_Click" ForeColor="Red" Font-Bold="true" Visible="false"   >Delete</asp:LinkButton>
                                      <asp:LinkButton ID="lnk_Discontinue" runat="server" OnClick="lnk_Discontinue_Click" ForeColor="Red" Font-Bold="true" Visible="false">Discontinue</asp:LinkButton>
                            
                                         <ajaxToolkit:ConfirmButtonExtender ID="cbe" runat="server" DisplayModalPopupID=""
    TargetControlID="lnk_DeleteReq"
    ConfirmText="Please confirm to Delete"/>
                              </ItemTemplate>
                            
                           </asp:TemplateField>                           
                           </columns>
                </asp:GridView>
                <br />
                *Discontinue request is subject to approval
            </td>
        </tr>
        <tr class="tdfields">
            <td>
                <asp:Label ID="lblerror" runat="server" EnableViewState="False" ForeColor="Red"></asp:Label></td>
        </tr>
        <tr class="subheader_img" style="display: none">
            <td align="center" style="height: 25px">
                Services Log</td>
        </tr>
        <tr style="display: none">
            <td>
                <asp:GridView ID="gvPendingAppSvc" runat="server" AutoGenerateColumns="False" SkinID="GridViewNormal"
                    Width="100%" Style="text-indent: 5px" ShowFooter="false">
                    <columns>
                         <%--  <asp:TemplateField HeaderText="Student Name" ItemStyle-HorizontalAlign="Left">
                            <ItemTemplate >
                              <asp:Label ID="lblSTU_Name0" runat="server" Text='<%# Bind("STU_Name") %>' ></asp:Label>
                                         
                            </ItemTemplate> 
                            </asp:TemplateField> --%>
                            <asp:TemplateField  HeaderText="Requested Date" ItemStyle-HorizontalAlign="Center" >
                              <ItemTemplate>
                                    <asp:Label ID="lblRequestedDate0" runat="server" Text='<%# Bind("SRS_REQUESTED_DATE") %>' ></asp:Label>
                                </ItemTemplate>   
                           </asp:TemplateField>
                            <asp:TemplateField HeaderText="Academic Year" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate >
                             <asp:Label ID="lblACADEMIC_YEAR0" runat="server" 
                                        Text='<%# Bind("ACADEMIC_YEAR") %>' ></asp:Label>
                            </ItemTemplate> 
                            </asp:TemplateField> 

                              <asp:TemplateField HeaderText="Service" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Label ID="lblSERVICE0" runat="server" Text='<%# Bind("SERVICE") %>' ></asp:Label> 
                                    <asp:HiddenField ID="hf_STU_ID0" runat="server"  
                                        Value='<%# Bind("STU_ID") %>' />
                                </ItemTemplate>                              
                            </asp:TemplateField>
                              <asp:TemplateField  HeaderText="Start Date" ItemStyle-HorizontalAlign="Center" >
                              <ItemTemplate>
                                    <asp:Label ID="lblStartDate0" runat="server" Text='<%# Bind("SSR_SERVICE_STARTDT") %>' ></asp:Label>
                                </ItemTemplate>   
                           </asp:TemplateField>
                       
                           
                            <asp:TemplateField HeaderText="Amount" ItemStyle-HorizontalAlign="Right">
                            <ItemTemplate >
                            <asp:Label ID="lblAMOUNT0" runat="server" Text='<%# Bind("AMOUNT") %>' ></asp:Label>
                            </ItemTemplate> 
                            </asp:TemplateField> 

                             <asp:TemplateField  HeaderText="Status" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                 <asp:Label ID="lblAPP_Status" runat="server" Text='<%# Bind("APP_Status") %>' ForeColor="ButtonHighlight"  ></asp:Label>
                                    <%--<asp:Image ID="IMG_STATUS" runat="server" ImageUrl='<%# Eval("APP_Status") %>'  Height="15px"  />        --%>                          
                                </ItemTemplate>
                            </asp:TemplateField> 

                          

                            </columns>
                </asp:GridView>
                <%--<telerik:RadGrid ID="gvPendingAppSvc1" runat="server" 
                        AutoGenerateColumns="False" GridLines="None">
                      <MasterTableView GridLines="Both">
                           <Columns>
                                   <telerik:GridTemplateColumn HeaderText="Student Name" ItemStyle-HorizontalAlign="Left"   >
                                <ItemTemplate>
                                    <asp:Label ID="lblSTU_Name0" runat="server" Text='<%# Bind("STU_Name") %>' ></asp:Label>
                                     <asp:HiddenField ID="hf_STU_ID0" runat="server"  
                                        Value='<%# Bind("STU_ID") %>' />                                                                                                        
                                </ItemTemplate>                              
                            </telerik:GridTemplateColumn>

                                  <telerik:GridTemplateColumn HeaderText="Academic Year" ItemStyle-HorizontalAlign="Left" >
                                <ItemTemplate>
                                    <asp:Label ID="lblACADEMIC_YEAR0" runat="server" 
                                        Text='<%# Bind("ACADEMIC_YEAR") %>' ></asp:Label>
                                </ItemTemplate>                              
                            </telerik:GridTemplateColumn  >

                                     <telerik:GridTemplateColumn HeaderText="Service" ItemStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <asp:Label ID="lblSERVICE0" runat="server" Text='<%# Bind("SERVICE") %>' ></asp:Label>
                                </ItemTemplate>                              
                            </telerik:GridTemplateColumn>
                               <telerik:GridTemplateColumn HeaderText="Term">
                                <ItemTemplate>
                                    <asp:Label ID="lblTRM_DESCRIPTION0" runat="server" Text='<%# Bind("TERM") %>' ></asp:Label>
                                </ItemTemplate>                              
                            </telerik:GridTemplateColumn>
                              <telerik:GridTemplateColumn HeaderText="Amount" ItemStyle-HorizontalAlign="Right" >
                                <ItemTemplate>
                                    <asp:Label ID="lblAMOUNT0" runat="server" Text='<%# Bind("AMOUNT") %>' ></asp:Label>
                                </ItemTemplate> 
                                <HeaderStyle HorizontalAlign="Right" />                               
                            </telerik:GridTemplateColumn>

                                <telerik:GridTemplateColumn  HeaderText="Approval Status" >
                                <ItemTemplate>
                                    <asp:Image ID="IMG_STATUS" runat="server" ImageUrl='<%# Eval("APP_Status") %>'  Height="15px"  />                                  
                                </ItemTemplate>
                                
                                <HeaderStyle Width="10px" />
                            </telerik:GridTemplateColumn>
                            </Columns>  
                            
                            </MasterTableView>
                      </telerik:RadGrid>--%>
            </td>
        </tr>
        <%--   <tr>   <td align="right"  > <table class="style1">
                    <tr>
                            <td>
                                <img alt="" class="imgstyle" src="../Images/APPLY.png" /> Approved</td>
                            <td>
                                <img alt="s" class="imgstyle" src="../Images/PENDING.png" />Pending</td>
                                 <td>
                                <img alt="s" class="imgstyle" src="../Images/DELETE.png" />Rejected</td>
                        </tr>
                    
                    </table></td></tr> --%>
    </table>
    <asp:Panel ID="pnlDiscontinue" runat="server" CssClass="darkPanlvisible" Visible="false">
        <div class="panelQual">
            <div class="mainheading">
                <div style="float: left; width: 88%;">
                    <b>Discontinue Request</b></div>
                <div style="float: right; vertical-align: top;">
                    <asp:LinkButton ForeColor="red" ID="lbtnApplicantNotesClose" ToolTip="click here to close"
                        CssClass="" runat="server" Text="X" Font-Underline="false" CausesValidation="false">X</asp:LinkButton>
                </div>
            </div>
            <center>
                <table width="80%" style="height: 170px" cellpadding="0" cellspacing="0" class="tableNoborder">
                    <tr>
                        <td colspan="2">
                        </td>
                    </tr>
                    <tr class="tdfields">
                        <td align="left">
                            Discontinue From <span style="color: Red">*</span>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtfromdate" runat="server" Width="104px" AutoPostBack="false">
                            </asp:TextBox>
                            <asp:ImageButton ID="imgFrom" runat="server" ImageUrl="~/Images/Common/PageBody/Calendar.png"
                                TabIndex="4" />
                            &nbsp;<asp:RequiredFieldValidator runat="server" ID="rfvFromDate" ControlToValidate="txtfromdate"
                                ErrorMessage=" From Date:" ValidationGroup="Save" Display="Dynamic">*</asp:RequiredFieldValidator>
                            <ajaxToolkit:CalendarExtender ID="DocDate" runat="server" Format="dd/MMM/yyyy" PopupButtonID="imgFrom"
                                TargetControlID="txtfromdate" PopupPosition="BottomRight">
                            </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MMM/yyyy"
                                PopupButtonID="txtfromdate" TargetControlID="txtfromdate" PopupPosition="BottomRight">
                            </ajaxToolkit:CalendarExtender>
                            <br />
                            <%--   <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToValidate="txtfromdate"
                            ErrorMessage="Date should be greater or equal to service start date" Operator="GreaterThanEqual"
                            Type="Date" ValidationGroup="Save"  Display="Dynamic"></asp:CompareValidator>--%>
                            <br />
                            <%--  <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="txtfromdate"
                            ErrorMessage="Selected date should be greater or equal to todays date" Operator="GreaterThanEqual"
                            Type="Date" ValidationGroup="Save"></asp:CompareValidator>--%>
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="true"
                                ShowSummary="false" ValidationGroup="Save" />
                        </td>
                    </tr>
                    <tr>
                        <td class="tdfields" align="left">
                            Remarks<span style="color: Red">*</span>
                        </td>
                        <td class="tdfields" align="left">
                            <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine" SkinID="MultiText">
                            </asp:TextBox>
                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="txtRemarks"
                                ErrorMessage="Remarks Required" ValidationGroup="Save" Display="Dynamic">*</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdfields" align="left">
                            Student Name
                        </td>
                        <td class="tdfields" align="left">
                            <asp:Label ID="lblStu_name_D" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdfields" align="left">
                            Service
                        </td>
                        <td class="tdfields" align="left">
                            <asp:Label ID="lblsvc" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdfields" align="left">
                            Service Start Date
                        </td>
                        <td class="tdfields" align="left">
                            <asp:Label ID="lblfromdate_D" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdfields" colspan="2" align="center">
                            <asp:Label ID="lbldiserror" runat="server" Text="" ForeColor="Red"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td align="center">
                            <table>
                                <tr>
                                    <td>
                                        <asp:Button ID="btnSaveDisRequest" runat="server" Text="Save" ValidationGroup="Save"
                                            CausesValidation="true" CssClass="button" /></td>
                                    <td>
                                        <asp:Button ID="btnCancelResetPassword" runat="server" Text="Cancel" CausesValidation="false"
                                            CssClass="button" /></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </center>
        </div>
    </asp:Panel>
</asp:Content>
