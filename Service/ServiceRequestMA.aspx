<%@ Page Title="" Language="VB" MasterPageFile="~/ParentMaster.master" AutoEventWireup="false"
    CodeFile="ServiceRequestMA.aspx.vb" Inherits="ParentLogin_Service_ServiceRequestMA" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphParent" runat="Server">
    <%-- <telerik:RadComboBox ID="ddlStudents" runat="server" AutoPostBack="True" Enabled="false"
                            Width="210px">
                        </telerik:RadComboBox>--%>
    <%--   <telerik:RadComboBox ID="ddlAcademicYear" runat="server" AutoPostBack="True" Width="210px">
                        </telerik:RadComboBox>--%>

    <script src="../Scripts/jquery-1.9.1.js" type="text/javascript"></script>

    <link rel="stylesheet" type="text/css" href="../Scripts/jQuery-ui-1.10.3.css" />

    <script type="text/javascript" src="../Scripts/jquery-ui.js"></script>

    <script type="text/javascript">
      function pageLoad() {
          $(function() {
              $(".datepicker").datepicker({
                  dateFormat: 'dd/M/yy'
              });
          }
    );
      }    
      function isAgreed(chkAgree)
                {
                    if($(chkAgree).attr('checked')==false)
                    {
                        alert('Please accept the Terms and Conditions to continue');
                        return false;
                    }
                    else
                        return true;
                }
      function SaveRequestDialog() {
          $("#dialog-message").dialog({
          dialogClass: 'no-close',
          resizable: false,
          height: 154,
          width:400,
          modal: true,
          buttonClass: 'btn btn-info',
          buttons: {
          
			Proceed: function () {
				location = "ViewStudentServices.aspx"; // wherever you want to go 
			}
              
		}
	});
}
    </script>

    <%--
<script type="text/javascript" src="../Scripts/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
	<script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
	<link rel="stylesheet" type="text/css" href="../Scripts/fancybox/jquery.fancybox-1.3.4.css" media="screen" />--%>

          <!-- Posts Block -->
    <div class="content margin-top30 margin-bottom60">
        <div class="container">
            <div class="row">

                <!-- Posts Block -->
                <div class="posts-block col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="my-account">
                     
           
                        <div>
                            <div id="trError">
                             <asp:Label ID="lblErrorMsg" runat="server" ForeColor="Red"></asp:Label>
                                   </div>
                             <div >
                               <asp:LinkButton ID="lnkViewServices" CausesValidation="false"
                        runat="server">View Requested Services</asp:LinkButton>

                                    </div>
                               <div class="mainheading">
                            <div> Request Services</div>
                           
                        </div>
                            <div align="left">

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>


    <%--        <div class="mainheading">
                <div class="left">
                    Request Services
                </div>
                <div class="right">
                
                    <asp:LinkButton ID="lnkViewServices" CssClass="currChildLink" CausesValidation="false"
                        runat="server">View Requested Services</asp:LinkButton>
                </div>
            </div>--%>
            <table   class="table table-striped table-bordered table-responsive text-left my-orders-table" width="100%" style="border-collapse:inherit !important;">
<%--                <tr id="trError" runat="server" visible="false">
                    <td colspan="3">
                        <asp:Label ID="lblErrorMsg" runat="server" ForeColor="Red"></asp:Label>
                    </td>
                </tr>--%>
                <tr align="left">
                    <th class="tdfields" style="width: 170px">
                        Student
                    </th>
                   <%-- <td class="tdfields" align="center" style="width: 2px">
                        :
                    </td>--%>
                    <td>
                        <asp:DropDownList ID="ddlStudent" runat="server" CssClass="form-control" AutoPostBack="True"
                            Enabled="false">
                        </asp:DropDownList>
                        <%-- <telerik:RadComboBox ID="ddlStudents" runat="server" AutoPostBack="True" Enabled="false"
                            Width="210px">
                        </telerik:RadComboBox>--%>
                    </td>
                </tr>
                <tr align="left">
                    <th class="tdfields" style="width: 170px">
                        Student No
                    </th>
                   <%-- <td class="tdfields" align="center" style="width: 2px">
                        :
                    </td>--%>
                    <td>
                        <asp:Label ID="lblStuNo" runat="server" ></asp:Label>
                    </td>
                </tr>
                <tr id="tmpRow" runat="server" visible="false" align="left">
                    <td class="tdfields" style="width: 170px">
                        &nbsp;</td>
                   <%-- <td align="center" class="tdfields" style="width: 2px">
                        &nbsp;</td>--%>
                    <td>
                        <asp:DropDownList ID="ddlGrade" runat="server" AutoPostBack="true" CssClass="form-control">
                        </asp:DropDownList>
                        <asp:DropDownList ID="ddlCategory" runat="server" AutoPostBack="true"  CssClass="form-control">
                        </asp:DropDownList>
                        <asp:DropDownList ID="ddlSection" runat="server" AutoPostBack="true"  CssClass="form-control">
                        </asp:DropDownList>
                        <asp:DropDownList ID="ddlSubCategory" runat="server" AutoPostBack="true"  CssClass="form-control">
                        </asp:DropDownList>
                        <asp:DropDownList ID="ddlServices" runat="server" AutoPostBack="True"  CssClass="form-control"
                            Visible="False">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr align="left">
                    <th class="tdfields" style="width: 170px">
                        Academic Year
                    </th>
                    <%--<td class="tdfields" align="center" style="width: 2px">
                        :
                    </td>--%>
                    <td>
                        <asp:DropDownList ID="ddlGMAcademicYear" runat="server" AutoPostBack="True" CssClass="form-control">
                        </asp:DropDownList>
                        <%--   <telerik:RadComboBox ID="ddlAcademicYear" runat="server" AutoPostBack="True" Width="210px">
                        </telerik:RadComboBox>--%>
                        <asp:DropDownList ID="ddlAcademicYear" runat="server" AutoPostBack="True" CssClass="form-control"
                            Visible="False">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr align="left">
                    <th class="tdfields" style="width: 170px">
                        Service Category
                    </th>
                   <%-- <td class="tdfields" align="center" style="width: 2px">
                        :
                    </td>--%>
                    <td>
                        Music Academy<asp:DropDownList ID="ddlSVCategory" runat="server" AutoPostBack="True"
                            CssClass="form-control" Enabled="False" Visible="False">
                        </asp:DropDownList>
                        <%--      <telerik:RadComboBox ID="ddlSVCategory" runat="server" AutoPostBack="True" Width="210px">
                        </telerik:RadComboBox>--%>
                        <%--javascript:confirm('all the unsaved data will loose when you change the category')--%>
                    </td>
                </tr>
                <tr align="left">
                    <th class="tdfields" valign="top" style="width: 170px">
                        <asp:Label ID="lblLanguage" runat="server" Text="Service"></asp:Label>
                    </th>
                   <%-- <td class="tdfields" align="center" valign="top" style="width: 2px">
                        :
                    </td>--%>
                    <td valign="top">
                        <%--    <telerik:RadComboBox ID="ddlServices" runat="server" AutoPostBack="True" Width="210px">
                        </telerik:RadComboBox>--%>
                        <asp:DropDownList ID="ddlSubCategoryMixed" runat="server" CssClass="form-control" Visible="false">
                        </asp:DropDownList>
                        <asp:DropDownList ID="ddlGMASVC" runat="server" CssClass="form-control" AutoPostBack="true"
                            OnDataBound="ddlGMASVC_DataBound">
                        </asp:DropDownList><asp:DropDownList ID="ddlAdditionalLessons" runat="server" CssClass="form-control"
                            Visible="false" CausesValidation="true">
                        </asp:DropDownList><br />
                        <asp:Label ID="lblmessage" runat="server" ForeColor="ControlDark" Font-Size="Smaller"
                            Visible="False" Text="Please select an instrument /Service which requires Additional Lessons"></asp:Label>
                        <asp:RequiredFieldValidator ControlToValidate="ddlAdditionalLessons" ID="RequiredFieldValidator2"
                            ValidationGroup="save" CssClass="error" ErrorMessage="Please select a service for Additional Lessons"
                            InitialValue="0" Text="*" runat="server" Display="Dynamic">
                        </asp:RequiredFieldValidator>
                     
                        <div>
                            <asp:Label ID="lblMessageDisplay" runat="server"></asp:Label></div>
                    </td>
                </tr>
                <tr>
                    <th align="left" style="width: 170px" class="tdfields">
                        Level
                    </th>
                   <%-- <td align="center" style="width: 2px" class="tdfields">
                        :
                    </td>--%>
                    <td align="left" style="height: 36px" class="tdfields">
                        <asp:DropDownList ID="ddlLevel" runat="server" CssClass="form-control">
                            <asp:ListItem Value="0">Please select</asp:ListItem>
                            <asp:ListItem Value="1">Beginner</asp:ListItem>
                            <asp:ListItem Value="2">Intermediate</asp:ListItem>
                            <asp:ListItem Value="3">Advance</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <th align="left" style="width: 170px" class="tdfields">
                        Period
                    </th>
                    <%--<td align="center" style="width: 2px" class="tdfields" valign="top">
                        :
                    </td>--%>
                    <td align="left" style="height: 36px" class="tdfields">
                        <asp:CheckBoxList ID="chkPeriods2" runat="server" BorderWidth="0px" BorderStyle="none" CssClass="form-control"
                            BorderColor="white" ForeColor="Black" Font-Bold="false" Font-Size="12PX" RepeatColumns="4"
                            RepeatDirection="Horizontal" RepeatLayout="Flow" CellPadding="4" CellSpacing="4"
                            AutoPostBack="true" OnSelectedIndexChanged="chkPeriods_SelectedIndexChnaged">
                        </asp:CheckBoxList>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <table style="width: 100%;">
                            <tr>
                                <th align="left"  class="tdfields" style="width: 22%;">
                                    From
                                </th> <%--style="height: 21px; width: 446px;"--%>
                               <%-- <td align="right" style="width: 8px; height: 21px" class="tdfields">
                                    :
                                </td>--%>
                                <td align="left"  class="tdfields" style="width: 25%;">
                                    <input type="text" id="txtFrom" class="datepicker form-control" runat="server"  />
                                </td> <%--style="height: 21px; width: 188px;"--%>
                                 <td align="left"  class="tdfields" style="width: 8%;">
                                   
                                </td>
                                <th align="left"  class="tdfields" style="width: 10%;">
                                    To
                                </th>  <%--style="height: 21px;"--%>
                                <%--<td align="center" style="width: 2px; height: 21px" class="tdfields">
                                    :
                                </td>--%>
                                <td align="left"  class="tdfields" style="width: 35%;">
                                    <input type="text" id="txtTo" class="datepicker form-control" runat="server" /> 
                                </td> <%--style="height: 21px"--%>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <asp:CheckBox ID="chkApproval" runat="server" Checked="True" />
                        Forward request for Approval
                    </td>
                    <tr>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <%--  <input type="checkbox" id="chkAccept" runat="server"  />--%>
                            <asp:CheckBox ID="chkAccepted" runat="server" OnCheckedChanged="chkAccepted_CheckedChanged"
                                AutoPostBack="true" />
                            <span>I/We accept the <a href="ServiceTerms/GMAParentsPolicy.pdf" target="_blank"><font
                                color="blue">Terms and Conditions </font></a>as outlined in attached Policy for
                                Parents</span>
                        </td>
                    </tr>
                    <div id="dialog-message" title="SUCCESS" style="display: none">
                        <p>
                            <span class="ui-icon ui-icon-circle-check" style="float: left; margin: 0 25px 30px 0;"></span>
                            <asp:Label ID="lblPEmail" runat="server"></asp:Label>
                        </p>
                    </div>
                    <tr align="left" id="trMAxseat" runat="server" style="display: none">
                        <td class="tdfields">
                            Max Sub Category</td>
                        <td align="center" class="tdfields">
                            :</td>
                        <td>
                            <asp:Label ID="lblMaxSubCat" runat="server" Font-Bold="False" Font-Size="8pt" ForeColor="#033448"></asp:Label>
                            <asp:Label ID="lblMaxCat" runat="server" Font-Bold="False" Font-Size="8pt" ForeColor="#033448"
                                Visible="False"></asp:Label>
                        </td>
                    </tr>
                    <tr align="left" id="trSeat" runat="server" visible="false" style="display: none">
                        <td class="tdfields">
                            Seat Capacity</td>
                        <td align="center" class="tdfields">
                            :</td>
                        <td>
                            <asp:Label ID="lblSeatCap" runat="server" Font-Bold="False" Font-Size="8pt" ForeColor="#033448"></asp:Label></td>
                    </tr>
                    <tr align="left" id="trDuration" runat="server" visible="false" style="display: none">
                        <td class="tdfields">
                            Duration</td>
                        <td align="center" class="tdfields">
                            :</td>
                        <td>
                            <asp:CheckBoxList ID="chkPeriods" runat="server" CellPadding="15" CellSpacing="15"
                                RepeatLayout="Flow" Visible="False" CssClass="form-control">
                            </asp:CheckBoxList>
                            <asp:GridView ID="gvDuration" runat="server" AutoGenerateColumns="False" EnableModelValidation="True"
                                Width="100%" CssClass="table table-striped table-bordered table-responsive text-left my-orders-table">
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkDuration" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="From Date">
                                        <ItemTemplate>
                                            <asp:Label ID="lblFromDate" runat="server" Text='<%# bind("FromDate") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="To Date">
                                        <ItemTemplate>
                                            <asp:Label ID="lblToDate" runat="server" Text='<%# bind("ToDate") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Term_Ids" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTermId" runat="server" Text='<%# bind("TermId") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr align="left">
                        <td align="center" colspan="3">
                            <asp:Button ID="btnAdd" runat="server" CssClass="btn btn-info" Text="Add" 
                                Visible="false" />
                            &nbsp;
                            <asp:Button ID="btnSaveData1" runat="server" CssClass="btn btn-info" Text="Save Request"
                                />
                            &nbsp;
                            <asp:Button ID="btnReset0" runat="server" CssClass="btn btn-info" Text="Reset"  />
                            &nbsp;
                            <asp:Button ID="btnView0" runat="server" CssClass="btn btn-info" Text="View Requested Activities"
                                Visible="false" />
                            &nbsp;
                        </td>
                    </tr>
                    <tr align="left" style="display: none">
                        <td class="tdfields">
                            &nbsp;</td>
                        <td align="center" class="tdfields">
                            &nbsp;</td>
                        <td align="center">
                            <asp:GridView ID="gvActivity" runat="server" AutoGenerateColumns="False" EnableModelValidation="True"
                                Width="100%" CssClass="table table-striped table-bordered table-responsive text-left my-orders-table">
                                <Columns>
                                    <asp:TemplateField HeaderText="ECA Category">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCategory" runat="server" Text='<%# Bind("Category") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" Width="25%" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="CategoryId" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCategoryId" runat="server" Text='<%# Bind("CategoryId") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="EC Activity">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSubCategory" runat="server" Text='<%# Bind("SubCategory") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Center" Width="25%" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Durations">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDuration" runat="server" Text='<%# Bind("Duration") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Center" Width="40%" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Term_Ids" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTermIds" runat="server" Text='<%# Bind("TermIds") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="SVG_ID" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSVGId" runat="server" Text='<%# Bind("SVGID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:CommandField DeleteText="Remove" ShowDeleteButton="True">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle Font-Bold="False" ForeColor="Red" HorizontalAlign="Center" Width="10%" />
                                    </asp:CommandField>
                                </Columns>
                            </asp:GridView>
                            <asp:Label ID="lblerror" runat="server" EnableViewState="False" ForeColor="Red"></asp:Label>
                        </td>
                    </tr>
                    <tr align="left" runat="server" id="tr_CurrentTeacher" visible="false">
                        <td class="tdfields">
                            Current Teacher
                        </td>
                        <td class="tdfields" align="center">
                            :
                        </td>
                        <td>
                            <asp:TextBox ID="txtCurrentTeacher" runat="server" SkinID="MultiText"
                                TextMode="MultiLine" ></asp:TextBox>
                            <asp:HiddenField ID="hfGMABsuId" runat="server" Value="500610" />
                            <asp:HiddenField ID="hfStudentGender" runat="server" Value="0" />
                            <asp:HiddenField ID="hfGender" runat="server" Value="3" />
                            <asp:HiddenField ID="hfTerms" runat="server" Value="0" />
                            <asp:HiddenField ID="hfStuId" runat="server" Value="0" />
                        </td>
                    </tr>
                    <tr align="left" runat="server" id="tr_Level" visible="false">
                        <td class="tdfields">
                            Level<span style="color: Red;">*</span>
                        </td>
                        <td class="tdfields" align="center">
                            :
                        </td>
                        <td>
                            <%-- <telerik:RadComboBox ID="ddlLevels" runat="server" EmptyMessage="<--Select One-->"
                            Width="210px">
                            <Items>
                                <telerik:RadComboBoxItem runat="server" Text="<--Select One-->" Value="<--Select One-->" />
                                <telerik:RadComboBoxItem runat="server" Text="Beginner" Value="1" />
                                <telerik:RadComboBoxItem runat="server" Text="Intermediate" Value="2" />
                                <telerik:RadComboBoxItem runat="server" Text="Advanced" Value="3" />
                            </Items>
                        </telerik:RadComboBox>--%>
                            <asp:DropDownList ID="ddlLevels" runat="server" EmptyMessage="<--Select One-->" CssClass="form-control">
                                <asp:ListItem Text="<--Select One-->" Value="<--Select One-->"></asp:ListItem>
                                <asp:ListItem Text="Beginner" Value="1"></asp:ListItem>
                                <asp:ListItem Text="Intermediate" Value="2"></asp:ListItem>
                                <asp:ListItem Text="Advanced" Value="3"></asp:ListItem>
                            </asp:DropDownList>
                            <asp:CompareValidator runat="server" ID="Comparevalidator1" ValueToCompare="<--Select One-->"
                                Operator="NotEqual" ControlToValidate="ddlLevels" ErrorMessage="Level Required"
                                CssClass="validationClass" ValidationGroup="Addterm" />
                        </td>
                    </tr>
                    <tr align="left" runat="server" id="trOwnIns" style="display: none" visible="false">
                        <td class="tdfields">
                            Own instrument ?
                        </td>
                        <td class="tdfields" align="center">
                            :
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rbOwnIns" runat="server" RepeatDirection="Horizontal" CssClass="form-control">
                                <asp:ListItem Value="1">Yes</asp:ListItem>
                                <asp:ListItem Selected="True" Value="0">No</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr align="left" runat="server" id="tr_Terms" visible="false">
                        <td class="tdfields">
                            Term fee details
                            <%--        <asp:GridView ID="gvTerms" runat="server" CssClass="rgMasterTable">
                    </asp:GridView>--%>
                        </td>
                        <td class="tdfields" align="center">
                            :
                        </td>
                        <td>
                            <telerik:RadGrid ID="gvTerms" runat="server" AutoGenerateColumns="false" CssClass="table table-striped table-bordered table-responsive text-left my-orders-table">
                                <MasterTableView GridLines="Both">
                                    <Columns>
                                        <telerik:GridTemplateColumn UniqueName="colImgDetailView">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="cbTerm" runat="server" Checked="true" />
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn HeaderText="Term">
                                            <ItemTemplate>
                                                <asp:Label ID="lblTRM_DESCRIPTION" runat="server" Text='<%# Bind("TRM_DESCRIPTION") %>'></asp:Label>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn HeaderText="Term fees">
                                            <ItemTemplate>
                                                <asp:Label ID="lblTERMFEES" runat="server" Text='<%# Bind("TERMFEES") %>'></asp:Label>
                                                <asp:HiddenField ID="hf_TRMID" runat="server" Value='<%# Bind("TRM_ID") %>' />
                                                <asp:HiddenField ID="hf_STM_ID" runat="server" Value='<%# Bind("STM_ID") %>' />
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridBoundColumn DataField="STM_START_DATE" HeaderText="Start date" DataFormatString="{0:dd/MMM/yyyy}">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="STM_CLOSE_DATE" HeaderText="Close date" DataFormatString="{0:dd/MMM/yyyy}"
                                            Visible="false">
                                        </telerik:GridBoundColumn>
                                    </Columns>
                                </MasterTableView>
                            </telerik:RadGrid>
                        </td>
                    </tr>
                    <tr id="tr_Music" runat="server" visible="false">
                        <td colspan="3" align="center">
                            <asp:HiddenField ID="HF_svc_count" runat="server" />
                            <asp:HiddenField ID="HF_MAX_SVC" runat="server" />
                            <asp:Button ID="btnRequestService" runat="server" Text="Add" Width="55px" ValidationGroup="Addterm"
                                CssClass="btn btn-info" ToolTip="Click here to add the selected terms" />
                        </td>
                    </tr>
                    <%--     </table>
                <table width="100%"  class="tableNoborder" >--%>
                    <tr id="tr_MusicServiceReq" runat="server" style="display: none">
                        <td colspan="3">
                            <asp:GridView ID="gvServicesRequested" runat="server" AutoGenerateColumns="False"
                                SkinID="GridViewNormal" Width="100%" Style="text-indent: 5px" ShowFooter="True" CssClass="table table-striped table-bordered table-responsive text-left my-orders-table">
                                <Columns>
                                    <asp:TemplateField HeaderText="Student Name">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSTU_Name" runat="server" Text='<%# Bind("STU_Name") %>'></asp:Label>
                                            <asp:HiddenField ID="hf_STU_ID" runat="server" Value='<%# Bind("STU_ID") %>' />
                                            <asp:HiddenField ID="hf_STM_ID" runat="server" Value='<%# Bind("ID") %>' />
                                            <asp:HiddenField ID="HF_SVG_ID" runat="server" Value='<%# Bind("SVG_ID") %>' />
                                            <asp:HiddenField ID="HF_SVC_ID" runat="server" Value='<%# Bind("SVC_ID") %>' />
                                            <asp:HiddenField ID="HF_ACD_ID" runat="server" Value='<%# Bind("ACD_ID") %>' />
                                            <asp:HiddenField ID="HF_Level" runat="server" Value='<%# Bind("LEVEL") %>' />
                                            <asp:HiddenField ID="HF_Teacher" runat="server" Value='<%# Bind("CurrentTeacher") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Academic Year" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblACADEMIC_YEAR" runat="server" Text='<%# Bind("ACADEMIC_YEAR") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Language" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSERVICE" runat="server" Text='<%# Bind("SERVICE") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Term" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTRM_DESCRIPTION" runat="server" Text='<%# Bind("TERM") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Amount" ItemStyle-HorizontalAlign="Right">
                                        <ItemTemplate>
                                            <asp:Label ID="lblAMOUNT" runat="server" Text='<%# Bind("AMOUNT") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Delete" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="img_delete" runat="server" ToolTip="Delete the Service request"
                                                ImageUrl="~/Images/DELETE.png" OnClick="img_delete_Click" Width="15px" Height="15px" />
                                        </ItemTemplate>
                                        <HeaderStyle Width="10px" />
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr align="left" id="tr_ServiceStartdate" runat="server" visible="false">
                        <td class="tdfields">
                            Start Date<span style="color: Red">*</span>
                        </td>
                        <td class="tdfields" align="center">
                            :
                        </td>
                        <td>
                            <asp:TextBox ID="txtfromdate" runat="server" Width="150px" AutoPostBack="false"></asp:TextBox>
                            <asp:ImageButton ID="imgFrom" runat="server" ImageUrl="~/Images/Common/PageBody/Calendar.png"
                                TabIndex="4" />
                            &nbsp;
                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="txtfromdate"
                                ErrorMessage=" Start Date Required" ValidationGroup="save">*</asp:RequiredFieldValidator>
                            <ajaxToolkit:CalendarExtender ID="DocDate" runat="server" Format="dd/MMM/yyyy" PopupButtonID="imgFrom"
                                TargetControlID="txtfromdate" PopupPosition="BottomRight">
                            </ajaxToolkit:CalendarExtender>
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MMM/yyyy"
                                PopupButtonID="txtfromdate" TargetControlID="txtfromdate" PopupPosition="BottomRight">
                            </ajaxToolkit:CalendarExtender>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ValidationGroup="save"
                                ErrorMessage="Not a valid date, please try like this(01/Jan/2013) " ControlToValidate="txtfromdate"
                                ValidationExpression="^(([0-9])|([0-2][0-9])|([3][0-1]))\/(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec|jan|feb|mar|apr|may|jun|jul|aug|sep|oct|nov|dec)\/\d{4}$"></asp:RegularExpressionValidator>
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="true"
                                ShowSummary="false" ValidationGroup="save" />
                        </td>
                    </tr>
                    <tr id="tr_SaveAndProceed" runat="server" visible="false">
                        <td colspan="2">
                        </td>
                        <td align="left">
                            <asp:Button ID="btn_SaveRequest" runat="server" Text="Save & proceed for payment"
                                ValidationGroup="save" CssClass="btn btn-info" ToolTip="Click here to Save & proceed for payment" />
                            <ajaxToolkit:ConfirmButtonExtender ID="cbe" runat="server" DisplayModalPopupID=""
                                TargetControlID="btn_SaveRequest" ConfirmText="Please confirm to proceed for Registration & Payment." />
                            <div>
                                <asp:ValidationSummary ID="vSUMMARY" runat="server" ShowMessageBox="true" ShowSummary="false"
                                    ValidationGroup="Addterm" />
                            </div>
                        </td>
                    </tr>
            </table>
            <asp:Panel ID="pnlSvcCatChange" runat="server" CssClass="darkPanlvisible" Visible="false">
                <div class="panelQual">
                    <center>
                        <table class="tableNoborder">
                            <tr>
                                <td align="center" style="font-weight: bold">
                                    If you change the service category,then you will loose unsaved datas</td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button ID="btn_Proceed" runat="server" Text="Proceed"  CssClass="btn btn-info" />&nbsp;&nbsp;<asp:Button
                                        ID="btn_cancel" runat="server" Text="Cancel"  CssClass="btn btn-info" /></td>
                            </tr>
                        </table>
                    </center>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlProceed2Payment" runat="server" CssClass="darkPanlvisible" Visible="false">
                <div class="panelQual">
                    <table width="80%" style="height: 170px" cellpadding="0" cellspacing="0" class="tableNoborder">
                        <tr>
                            <td colspan="2" align="left">
                                <span style="font-size: small; font-weight: bold">
                                    <asp:Label ID="lblProceed2Payment" runat="server" Text="" ForeColor="Red"></asp:Label>
                                    Click<asp:LinkButton ID="lnkbtnProceed2Payment" runat="server" Font-Bold="true" Font-Size="Small"> here</asp:LinkButton>
                                    to proceed for payment</span>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="2">
                                <%-- <asp:Button ID="btnCancelProceed2Payment" runat="server" Text="Cancel" 
                        CausesValidation="false"                         
                        CssClass="button"  />--%>
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
            <div id="divApplicantNotes" runat="server" class="darkPanlvisible" style="display: none">
                <div class="panelQual">
                    <div class="mainheading">
                        <div class="left">
                            Already Requested Services
                        </div>
                        <div class="right">
                            <asp:LinkButton ForeColor="red" ID="lbtnApplicantNotesClose" ToolTip="click here to close"
                                CssClass="" runat="server" Text="X" Font-Underline="false" CausesValidation="false"
                                OnClientClick="javascript:hideshowResetPass();return false;">X</asp:LinkButton>
                        </div>
                    </div>
                    <table width="100%" cellpadding="0" cellspacing="0" class="tableNoborder">
                        <tr>
                            <td class="tdfields">
                                <div style="overflow: auto; height: 300px">
                                    <telerik:RadGrid ID="gvStudentServicesDetails" runat="server" AutoGenerateColumns="False"
                                        GridLines="None" Width="99%" CssClass="table table-striped table-bordered table-responsive text-left my-orders-table">
                                        <MasterTableView GridLines="Both">
                                            <Columns>
                                                <telerik:GridTemplateColumn HeaderText="Name">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSTU_Name0" runat="server" Text='<%# Bind("STU_Name") %>'></asp:Label>
                                                        <asp:HiddenField ID="hf_STU_ID0" runat="server" Value='<%# Bind("STU_ID") %>' />
                                                        <asp:HiddenField ID="HF_SVG_ID0" runat="server" Value='<%# Bind("SVG_ID") %>' />
                                                        <asp:HiddenField ID="hf_STM_ID0" runat="server" Value='<%# Bind("STM_ID") %>' />
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                </telerik:GridTemplateColumn>
                                                <telerik:GridTemplateColumn HeaderText="Academic Year">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblACADEMIC_YEAR0" runat="server" Text='<%# Bind("ACADEMIC_YEAR") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </telerik:GridTemplateColumn>
                                                <telerik:GridTemplateColumn HeaderText="Language">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSERVICE0" runat="server" Text='<%# Bind("SERVICE") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                </telerik:GridTemplateColumn>
                                                <telerik:GridTemplateColumn HeaderText="Term">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblTRM_DESCRIPTION0" runat="server" Text='<%# Bind("TERM") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                </telerik:GridTemplateColumn>
                                                <telerik:GridTemplateColumn HeaderText="Amount">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAMOUNT0" runat="server" Text='<%# Bind("AMOUNT") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Right" />
                                                </telerik:GridTemplateColumn>
                                                <telerik:GridTemplateColumn HeaderText="Status">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAPP_Status" runat="server" Text='<%# Bind("APP_Status") %>'></asp:Label>
                                                        <%--<asp:Image ID="IMG_STATUS" runat="server" ImageUrl='<%# Eval("APP_Status") %>' Height="20px" />--%>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <HeaderStyle Width="10px" />
                                                </telerik:GridTemplateColumn>
                                            </Columns>
                                        </MasterTableView>
                                    </telerik:RadGrid>
                                </div>
                            </td>
                            <td class="tdfields">
                                &nbsp;
                            </td>
                        </tr>
                        <%--<tr style="height: 40px">
                    <td class="tdfields" align="right">
                        <table class="style1">
                            <tr>
                                <td>
                                    <img alt="" class="style2" src="../Images/APPLY.png" />
                                    Approved</td>
                                <td>
                                    <img alt="s" class="style2" src="../Images/PENDING.png" />Pending</td>
                                <td>
                                    <img alt="s" class="style2" src="../Images/DELETE.png" />Rejected</td>
                            </tr>
                        </table>
                    </td>
                    <td>
                    </td>
                </tr>--%>
                        <tr>
                            <td align="center" colspan="2">
                                <asp:Button ID="btnCancelResetPassword" runat="server" Text="Close" CausesValidation="false"
                                    OnClientClick="javascript:hideshowResetPass();return false;"  CssClass="btn btn-info" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>

            <script type="text/javascript" language="javascript">
            function handleError()
{
return true;
}
window.onerror = handleError;
                function hideshowResetPass() {
                    var which = document.getElementById('<%= divApplicantNotes.ClientID %>')// document.getElementById('<%=divApplicantNotes.ClientID%>');
                    if (which.style.display == "block")
                        which.style.display = "none";
                    else
                        which.style.display = "block";
                  return false;
                }
                $(document).ready(function () {
                    $(document).click(function (e) {
                        //$('#<%= divApplicantNotes.ClientID %>').show("slow");
                        var lnkDocumentFwd = $('#<%= divApplicantNotes.ClientID %>');
                        var div = $('#<%= divApplicantNotes.ClientID %>').attr("id");
                        var which = document.getElementById('<%=divApplicantNotes.ClientID%>');
                        if (e.target.id == div) {
                            if (which.style.display == "block")
                                which.style.display = "none";
                        }
                    });
                });
                
            </script>

        </ContentTemplate>
    </asp:UpdatePanel>
    <%--   <asp:UpdateProgress ID="upProgGv" runat="server" DisplayAfter="1">
        <progresstemplate>
            <asp:Panel ID="pnlProgress" runat="server" CssClass="screenCenter">
                <br />
                <asp:Image ID="Image2" runat="server" ImageUrl="~/ParentLogin/Images/loading6.gif">
                </asp:Image><br />
                Please Wait....</asp:Panel>
            <ajaxToolkit:AlwaysVisibleControlExtender ID="avcProgress" runat="server" TargetControlID="pnlProgress"
                VerticalSide="Middle" HorizontalSide="Center" VerticalOffset="10" ScrollEffectDuration=".1"
                HorizontalOffset="10">
            </ajaxToolkit:AlwaysVisibleControlExtender>
        </progresstemplate>
    </asp:UpdateProgress>--%>
   </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
