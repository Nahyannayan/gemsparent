﻿Imports Microsoft.ApplicationBlocks.Data
Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Net.Mail

Partial Class Others_ECARequestView
    Inherits System.Web.UI.Page
    Dim dtActivity As New DataTable

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("username") Is Nothing Then
            Session("Active_tab") = "Home"
            Session("Site_Path") = ""
            Response.Redirect("~\General\Home.aspx")
        ElseIf Session("bPasswdChanged") = "False" Then
            Response.Redirect("~\UpdateInfo\ForcePasswordChange.aspx", False)
        ElseIf Session("bUpdateContactDetails") = "False" Then
            Response.Redirect(Session("ForceUpdate_stud"), False)
        End If

        Dim dr As IDataReader, strDur As String

        If Not Page.IsPostBack Then
            
            'Dim qry As String = "SELECT  SSA.SSA_ID AS SSAID , SC.SSC_DESC AS Category , svc.SVC_DESCRIPTION AS SubCategory , SSA.SSA_DURATIONS AS Duration , CASE WHEN SSA.SSA_APPR_FLAG IS NULL THEN 'Pending Approval' WHEN SSA.SSA_APPR_FLAG = 'A' THEN 'Approved' WHEN SSA.SSA_APPR_FLAG = 'R' THEN 'Rejected' END AS STATUS , ISNULL(SSA.SSA_APPR_FLAG, 'P') AS StatusFlag FROM    dbo.STUDENT_SERVICE_APPLICATION SSA INNER JOIN services_category SC ON SSA.SSA_SSC_ID = SC.SSC_ID INNER JOIN dbo.SERVICES_GRD_M SVG ON ssa.SSA_SVG_ID = svg.SVG_ID INNER JOIN dbo.SERVICES_SYS_M SVC ON svg.SVG_SVC_ID = SVC.SVC_ID Where SSA_Stu_Id = " & Session("stu_id") & " and ssa_acd_id = " & Session("stu_acd_id") & " and ssa_grd_id = " & Session("stu_grd_id")
            Dim Enc As New Encryption64
            '  Dim qry As String = "SELECT  SSR.SSR_ID AS SSRID , SC.SSC_DESC AS Category , svc.SVC_DESCRIPTION AS SubCategory , '' AS Duration , CASE WHEN SSR.SSR_APPR_FLAG IS NULL THEN 'Pending Approval' WHEN SSR.SSR_APPR_FLAG = 'A' THEN 'Approved' WHEN SSR.SSR_APPR_FLAG = 'R' THEN 'Rejected' When SSR.SSR_APPR_FLAG = 'P' Then 'Pending Approval' END AS STATUS , ISNULL(SSR.SSR_APPR_FLAG, 'P') AS StatusFlag, CASE WHEN SVG.SVG_bREG_ONLINE  = 0 THEN 'F' WHEN SVG.SVG_bREG_ONLINE = 1 THEN 'T' ELSE 'F' END  AS 'Online', SVG.SVG_MANDATORY_MAIN As 'MANDATORY', SSR.SSR_STM_IDS FROM  dbo.STUDENT_SERVICES_REQUEST SSR INNER JOIN dbo.SERVICES_SYS_M SVC ON SSR.SSR_SVC_ID = SVC.SVC_ID INNER JOIN dbo.SERVICES_CATEGORY SC ON SVC.SVC_SSC_ID = SC.SSC_ID INNER JOIN dbo.SERVICES_GRD_M SVG ON SSR.SSR_SVG_ID = SVG.SVG_ID WHERE SSR_Stu_Id = " & Enc.Decrypt(Request.QueryString("Stu_Id").Replace(" ", "+")) & " AND ssR_acd_id = " & Enc.Decrypt(Request.QueryString("Acd_Id").Replace(" ", "+")) & " AND SVG.SVG_GRD_ID = '" & Enc.Decrypt(Request.QueryString("Grd_Id").Replace(" ", "+")) & "' And SSR_Bsu_Id = '" & Enc.Decrypt(Request.QueryString("Bsu_Id").Replace(" ", "+")) & "'"
            Dim qry As String = "SELECT  SSR.SSR_ID AS SSRID , SC.SSC_DESC AS Category , svc.SVC_DESCRIPTION AS SubCategory , REPLACE(CONVERT(VARCHAR(11), SSR_FROMDATE, 106), ' ', '/') + ' - ' +  REPLACE(CONVERT(VARCHAR(11), SSR_TODATE, 106), ' ', '/') AS Duration , CASE WHEN SSR.SSR_APPR_FLAG IS NULL THEN 'Pending Approval' WHEN SSR.SSR_APPR_FLAG = 'A' THEN 'Approved' WHEN SSR.SSR_APPR_FLAG = 'R' THEN 'Rejected' When SSR.SSR_APPR_FLAG = 'P' Then 'Pending Approval' END AS STATUS , ISNULL(SSR.SSR_APPR_FLAG, 'P') AS StatusFlag, CASE WHEN SSR_bONLINE  = 0 THEN 'F' WHEN SSR_bONLINE = 1 THEN 'T' ELSE 'F' END  AS 'Online',  SSR.SSR_STM_IDS FROM  dbo.STUDENT_SERVICES_REQUEST SSR INNER JOIN dbo.SERVICES_SYS_M SVC ON SSR.SSR_SVC_ID = SVC.SVC_ID INNER JOIN dbo.SERVICES_CATEGORY SC ON SVC.SVC_SSC_ID = SC.SSC_ID  WHERE  isnull(Is_Removed,0)= 0 and SSR_Stu_Id = " & Enc.Decrypt(Request.QueryString("Stu_Id").Replace(" ", "+")) & " AND ssR_acd_id = " & Enc.Decrypt(Request.QueryString("Acd_Id").Replace(" ", "+")) & "  And SSR_Bsu_Id = '" & Enc.Decrypt(Request.QueryString("Bsu_Id").Replace(" ", "+")) & "'"
            Try
                Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_SERVICESConnectionString, CommandType.Text, qry)
                'For Each row As DataRow In ds.Tables(0).Rows
                '    qry = "SELECT  REPLACE(CONVERT(VARCHAR(12), TRM_STARTDATE, 106), ' ', '/') + ' to ' + REPLACE(CONVERT(VARCHAR(12), TRM_ENDDATE, 106), ' ', '/') As Duration FROM    dbo.SERVICE_TERM_M STM INNER JOIN oasis.dbo.TRM_M TM ON STM.STM_TRM_ID = TM.TRM_ID WHERE   STM_ID IN ( SELECT  ID FROM    oasis.dbo.fnSplitMe('" & row.Item("SSR_STM_IDS") & "', '|') )"
                '    dr = SqlHelper.ExecuteReader(ConnectionManger.GetOASIS_SERVICESConnectionString, CommandType.Text, qry)
                '    Do While dr.Read
                '        strDur &= dr.Item("Duration") & " & "
                '    Loop
                '    dr.Close()
                '    strDur = strDur.Remove(strDur.Length - 3, 3)
                '    row.Item("Duration") = strDur
                '    strDur = ""
                'Next
                ds.AcceptChanges()
                Session("ECARequestView_dtActivity") = ds.Tables(0)
                Me.gvActivity.DataSource = ds.Tables(0)
                Me.gvActivity.DataBind()

            Catch ex As Exception
                Me.lblMessage.Text = "There was an issue getting the data. Please try again"
            End Try

        End If


    End Sub

    Protected Sub gvActivity_RowDeleting(sender As Object, e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvActivity.RowDeleting
        Try
            Dim lblId As String = CType(Me.gvActivity.Rows(e.RowIndex).FindControl("lblSSRId"), Label).Text
            Dim lblStatusFlag As String = CType(Me.gvActivity.Rows(e.RowIndex).FindControl("lblStatusFlag"), Label).Text
            Dim lblOnline As String = CType(Me.gvActivity.Rows(e.RowIndex).FindControl("lblOnline"), Label).Text
            'Dim lblMandatory As String = CType(Me.gvActivity.Rows(e.RowIndex).FindControl("lblMandatory"), Label).Text

            'If lblMandatory = "1" Or lblMandatory = "True" Then
            '    lblMessage.Text = "Requested activity is mandatory and cannot be cancelled"
            '    Exit Sub
            'End If

            If lblOnline = "F" Then
                lblMessage.Text = "Requested activity is applied by the school on behalf of the student and is not available online for cancelling. Kindly contact the school to cancel this activity."
                Exit Sub
            End If

            If lblStatusFlag = "A" Then
                lblMessage.Text = "Requested activity is already approved and cannot be cancelled"
                Exit Sub
            ElseIf lblStatusFlag = "R" Then
                lblMessage.Text = "Requested activity is already rejected and cannot be cancelled"
                Exit Sub
            ElseIf lblStatusFlag = "D" Then
                lblMessage.Text = "Requested activity is already Discontinued and cannot be cancelled"
                Exit Sub

            End If
              

            'Send the email to the parent
            Dim Enc As New Encryption64
            Dim pParms1(4) As SqlClient.SqlParameter
            pParms1(0) = New SqlClient.SqlParameter("@bsu_ID", Session("sBsuid"))
            pParms1(1) = New SqlClient.SqlParameter("@acd_ID", Enc.Decrypt(Request.QueryString("Acd_Id").Replace(" ", "+")))
            pParms1(2) = New SqlClient.SqlParameter("@stu_ID", Enc.Decrypt(Request.QueryString("Stu_Id").Replace(" ", "+")))
            pParms1(3) = New SqlClient.SqlParameter("@olu_ID", Session("OLU_ID"))
            pParms1(4) = New SqlClient.SqlParameter("@ssr_ID", lblId)
            SqlHelper.ExecuteNonQuery(ConnectionManger.GetOASIS_SERVICESConnectionString, CommandType.StoredProcedure, "dbo.SEND_ECA_PARENT_SERVICE_CANCEL_REQUEST_EMAIL", pParms1)

            'Delete the record
            SqlHelper.ExecuteNonQuery(ConnectionManger.GetOASIS_SERVICESConnectionString, CommandType.Text, "UPDATE  Student_Services_Request SET Is_Removed=1 Where SSR_ID = " & lblId)

            Dim dt As DataTable = Session("ECARequestView_dtActivity")
            Dim row() As DataRow = dt.Select("ssrid = " & lblId)
            If row.Length > 0 Then
                dt.Rows.Remove(row(0))
                dt.AcceptChanges()
                Session("ECARequestView_dtActivity") = dt
                Me.gvActivity.DataSource = dt
                Me.gvActivity.DataBind()
            End If
            Me.lblMessage.Text = "Selected ECA request was cancelled successfully"

        Catch ex As Exception
            lblMessage.Text = "There was an issue canceling your request"
        End Try


        
    End Sub

    Protected Sub gvActivity_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvActivity.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim lblId As Label = DirectCast(e.Row.FindControl("lblSSRId"), Label)
                Dim lblStatusFlag As Label = DirectCast(e.Row.FindControl("lblStatusFlag"), Label)
                Dim lblOnline As Label = DirectCast(e.Row.FindControl("lblOnline"), Label)
                Dim btnCancel As ImageButton = DirectCast(e.Row.FindControl("btnCancel"), ImageButton)
                Dim lblStatus As Label = DirectCast(e.Row.FindControl("lblStatus"), Label)
                If lblOnline.Text = "F" Then
                    btnCancel.Visible = False
                End If

                If lblStatusFlag.Text = "A" Then
                     btnCancel.Visible = False
                ElseIf lblStatusFlag.Text = "R" Then
                    btnCancel.Visible = False
                ElseIf lblStatusFlag.Text = "D" Then
                    btnCancel.Visible = False
                End If

                If lblStatusFlag.Text = "P" Then
                    lblStatus.Text = "PENDING"
                End If
                If lblStatusFlag.Text = "D" Then
                    lblStatus.Text = "DISCONTINUED"
                End If

                If lblStatusFlag.Text = "R" Then
                    lblStatus.Text = "REJECTED"
                End If
            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub gvActivity_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvActivity.PageIndexChanging
        gvActivity.PageIndex = e.NewPageIndex


        Me.gvActivity.DataSource = Session("ECARequestView_dtActivity")
        Me.gvActivity.DataBind()
    End Sub
End Class
