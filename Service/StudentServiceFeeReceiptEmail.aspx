﻿<%@ Page Language="VB"  AutoEventWireup="false" CodeFile="StudentServiceFeeReceiptEmail.aspx.vb" Inherits="Service_StudentServiceFeeReceiptEmail" title="Untitled Page" %>


<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Online Payment Receipt</title>
    <base target="_self" />
    <style>
        img
        {
            display: block;
        }
    p.MsoNormal
	{margin-bottom:.0001pt;
	font-size:12.0pt;
	font-family:"Times New Roman","serif";
	        margin-left: 0in;
            margin-right: 0in;
            margin-top: 0in;
        }
a:link
	{color:blue;
	text-decoration:underline;
	text-underline:single;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <table width="100%">
        <tr>
            <td width="100%">
                <p class="MsoNormal">
                    <span style="font-size:11.0pt;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;
color:#1F497D">
                        <div id="divEmailCoverLetter" runat="server"></div>
                        </span>&nbsp;
                    <span style="font-size: 11pt; color: #1f497d; font-family: 'Calibri','sans-serif'">
                        <br />
                        Thanks<o:p></o:p></span><p class="MsoNormal">
                    <o:p>
                           <span  style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;
mso-fareast-font-family:Calibri;mso-fareast-theme-font:minor-latin;color:#1F497D;
mso-ansi-language:EN-US;mso-fareast-language:EN-US;mso-bidi-language:AR-SA"> <asp:Label ID="lblSchool0" runat="server"></asp:Label></span>
                        </o:p>
                </p>
                <p class="MsoNormal">
                    <o:p><i>
                    <span style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;
mso-fareast-font-family:Calibri;mso-fareast-theme-font:minor-latin;color:#1F497D;
mso-ansi-language:EN-US;mso-fareast-language:EN-US;mso-bidi-language:AR-SA">This is an automated 
                    e-mail generated by PHOENIX system. Please do not respond to this address</span></i></o:p></p>
                <p class="MsoNormal">
                    <o:p></o:p>
                </p>
                <p class="MsoNormal">
                    <o:p></o:p>
                </p>
                <p class="MsoNormal">
                    <o:p></o:p>
                </p>
            </td>
        </tr>
    </table>
    <br />
      <br />
    <table align="center" border="0" cellpadding="0" cellspacing="0" width="800">
        <tr valign="top">
            <td colspan="2" align="center" style="border-bottom: #000095 2pt solid">
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left" rowspan="4" width="10%">
                            <asp:Image ID="imgLogo" runat="server" />
                        </td>
                        <td style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 13px; font-weight: bold;
                            color: #000095" align="center">
                            <asp:Label ID="lblSchool" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px; font-weight: bold;
                            color: #000095" align="center">
                            <asp:Label ID="lblHeader1" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px; font-weight: bold;
                            color: #000095" align="center">
                            <asp:Label ID="lblHeader2" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px; font-weight: bold;
                            color: #000095" align="center">
                            <asp:Label ID="lblHeader3" runat="server"></asp:Label>
                        </td>
                    </tr>
                </table>
            </td>
            <td align="center" colspan="1" style="height: 90px">
            </td>
        </tr>
        <tr>
            <td height="20px">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td valign="top" align="center">
                <table align="center">
                    <tr>
                        <td colspan="6" align="center" style="font-weight: bold; font-size: 10pt; color: #000095;
                            font-family: Verdana; height: 19px;">
                            Online Payment Receipt<br />
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="6">
                            <!-- content starts here -->
                            <table cellpadding="3" width="100%" border="0" style="border-right: #7f83ee 1pt solid;
                                border-top: #7f83ee 1pt solid; border-left: #7f83ee 1pt solid; border-bottom: #7f83ee 1pt solid">
                                <tr>
                                    <td align="left" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px;
                                        color: #000095" width="110">
                                        Receipt No
                                    </td>
                                    <td align="center" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px;
                                        font-weight: bold; color: #000095; width: 1px">
                                        :
                                    </td>
                                    <td align="left" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px;
                                        font-weight: bold; color: #000095">
                                        <asp:Label ID="lblRecno" runat="server"></asp:Label>
                                    </td>
                                    <td align="left" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px;
                                        color: #000095" width="50px">
                                        Date
                                    </td>
                                    <td align="left" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px;
                                        font-weight: bold; color: #000095; width: 1px">
                                        :
                                    </td>
                                    <td align="left" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px;
                                        color: #000095" width="70">
                                        <asp:Label ID="lblDate" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px;
                                        color: #000095">
                                        Student ID
                                    </td>
                                    <td align="center" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px;
                                        font-weight: bold; color: #000095; width: 1px">
                                        :
                                    </td>
                                    <td align="left" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px;
                                        font-weight: bold; color: #000095">
                                        <asp:Label ID="lblStudentNo" runat="server"></asp:Label>
                                    </td>
                                    <td align="left" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px;
                                        color: #000095">
                                        Grade
                                    </td>
                                    <td align="left" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px;
                                        font-weight: bold; color: #000095; width: 1px">
                                        :
                                    </td>
                                    <td align="left" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px;
                                        color: #000095">
                                        <asp:Label ID="lblGrade" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px;
                                        color: #000095">
                                        Name
                                    </td>
                                    <td align="center" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px;
                                        font-weight: bold; color: #000095; width: 1px">
                                        :
                                    </td>
                                    <td align="left" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px;
                                        font-weight: bold; color: #000095" colspan="4">
                                        <asp:Label ID="lblStudentName" runat="server"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                            <!-- content ends here -->
                        </td>
                    </tr>
                    <tr id="Tr1" class="ReceiptCaption" runat="server">
                        <td colspan="6" align="center" style="height: 19px; font-weight: bold; font-size: 10pt;
                            color: #000095; font-family: Verdana;">
                            Fee Details
                        </td>
                    </tr>
                    <tr>
                        <td align="center" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px;
                            color: #000095; text-indent: 20px;" colspan="6" height="120" valign="top">
                            <asp:GridView ID="gvFeeDetails" runat="server" AutoGenerateColumns="False" CellPadding="4"
                                BorderColor="#7f83ee" BorderWidth="1pt" Width="100%">
                                <Columns>
                                    <asp:BoundField DataField="FEE_DESCR" HeaderText="Fee">
                                        <HeaderStyle HorizontalAlign="Left" Font-Bold="true" Font-Names="Verdana" Font-Size="10px"
                                            ForeColor="#000095" />
                                        <ItemStyle Font-Names="Verdana" Font-Size="10px" ForeColor="#000095" HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="FCS_AMOUNT" HeaderText="Amount" DataFormatString="{0:0.00}">
                                        <HeaderStyle HorizontalAlign="Right" Font-Bold="true" Font-Names="Verdana" Font-Size="10px"
                                            ForeColor="#000095" />
                                        <ItemStyle Font-Names="Verdana" Font-Size="10px" ForeColor="#000095" HorizontalAlign="Right" />
                                    </asp:BoundField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr id="Tr2" runat="server">
                        <td align="right" colspan="6" style="height: 19px; font-family: Verdana, Arial, Helvetica, sans-serif;
                            font-size: 10px; font-weight: bold; color: #000095">
                            <asp:Label ID="lblBalance" runat="server"></asp:Label>&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="middle" style="text-indent: 10px; font-family: Verdana, Arial, Helvetica, sans-serif;
                            font-size: 10px; color: #000095" colspan="6">
                            <br />
                            <br />
                            <asp:Label ID="lblPaymentDetals" runat="server"></asp:Label>
                            <br />
                            <br />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="center" style="vertical-align: middle; font-family: Verdana, Arial, Helvetica, sans-serif;
                font-size: 8px; color: #000095; height: 177px;" valign="middle">
                <asp:Image ID="ImgPrintLogo" runat="server" ImageUrl="~/Images/Fees/Receipt/receipt.gif" /><br />
                This receipt is electronically generated and does not require any signature.<br />
                <asp:Label ID="lblProviderMessage" runat="server" Text=" This receipt is subject to Network International LLC crediting the amount to our account."
                    Visible="true"> </asp:Label>
                <asp:Label ID="lblDiscount" Visible="false" Text="The discounted fee structure is applicable on the existing fee structure of the School.<br />Any  revision in fee for the period if approved by the regulatory authority later, the difference shall be payable by the parent"
                    runat="server"> </asp:Label>
            </td>
        </tr>
        <tr>
            <td align="center" style="vertical-align: middle; font-family: Verdana, Arial, Helvetica, sans-serif;
                font-size: 8px; color: #000095" valign="middle">
                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%; height: 100%">
                    <tr>
                        <td align="left" style="font-size: 8px; color: #000095; font-family: Verdana, Arial, Helvetica, sans-serif;
                            font-size: 8px; color: #000095" valign="bottom">
                            Source: PHOENIX
                        </td>
                        <td valign="bottom" align="right">
                            <asp:Image ID="imgSwooosh" runat="server" ImageUrl="~/Images/Fees/Receipt/gemsfooter.png" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="right" valign="bottom" style="font-family: Verdana, Arial, Helvetica, sans-serif;
                font-size: 10px; color: #000095">
                <table border="0" cellpadding="0" cellspacing="0" style="border-top: #003A63 1px solid;
                    width: 100%; height: 100%">
                    <tr>
                        <td align="left" style="font-size: 8px; color: #000095; font-family: Verdana, Arial, Helvetica, sans-serif;
                            font-size: 8px; color: #000095" valign="top">
                            <asp:Label ID="lblPrintTime" runat="server"></asp:Label>
                        </td>
                        <td>
                            <td valign="top" align="right" style="font-size: 8px; color: #000095; font-family: Verdana, Arial, Helvetica, sans-serif;
                                font-size: 8px; color: #000095">
                                <asp:Label ID="lblLogged" runat="server" Text="User : "></asp:Label>
                                <asp:Label ID="lbluserloggedin" runat="server"></asp:Label>
                            </td>
                            <td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </form>
</p>
</body>
</html>

