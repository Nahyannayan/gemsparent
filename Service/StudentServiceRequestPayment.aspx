﻿<%@ Page Language="VB" MasterPageFile="~/ParentMaster.master" AutoEventWireup="false"
    CodeFile="StudentServiceRequestPayment.aspx.vb" Inherits="Service_StudentServiceRequestPayment"
    Title="Untitled Page" Debug="true" %>

<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphParent" runat="Server">



    <!-- Posts Block -->
    <div class="content margin-top30 margin-bottom60">
        <div class="container">
            <div class="row">

                <!-- Posts Block -->
                <div class="posts-block col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="my-account">


                        <div>
                            <div>
                                <asp:Label ID="lblPayMsg" runat="server" Font-Names="Verdana" ForeColor="Red" Font-Size="11pt"
                                    EnableViewState="False"></asp:Label>
                            </div>


                            <div class="mainheading">
                                <div>Service Fee Online Payment</div>


                            </div>
                            <div align="left">
                                <table align="center" style="width: 98%;" cellpadding="8" cellspacing="0"
                                    class="table table-striped table-bordered table-responsive text-left my-orders-table">
                                    <%--<tr class="trSub_Header">
            <td colspan="2" align="left" style="height: 18px">
                Service Fee Online Payment
            </td>
        </tr>--%>
                                    <tr>
                                        <th align="left" class="tdfields" width="20%">Date
                                        </th>
                                        <td align="left">
                                            <asp:Label ID="lblDate" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th align="left" class="tdfields">Student ID
                                        </th>
                                        <td align="left">
                                            <asp:Label ID="txtStdNo" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th align="left" class="tdfields">Student Name
                                        </th>
                                        <td align="left">
                                            <asp:Label ID="txtStudentname" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th align="left" class="tdfields">Academic Year
                                        </th>
                                        <td align="left">
                                            <asp:Label ID="lblacademicyear" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th align="left" class="tdfields">Grade &amp; Section
                                        </th>
                                        <td align="left">
                                            <asp:Label ID="stugrd" runat="server"></asp:Label>
                                            -<strong></strong>
                                            <asp:Label ID="lblsct" runat="server"></asp:Label>
                                            &nbsp; &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <th align="left" class="tdfields">School
                                        </th>
                                        <td align="left">
                                            <asp:Label ID="lblschool" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                </table>

                                <table  align="center" style="width: 98%;" runat="server" border="0">
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblLastPayment" runat="server" ForeColor="Maroon">   </asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblError" runat="server" EnableViewState="False" ForeColor="Navy"></asp:Label>
                                        </td>
                                    </tr>
                                </table>


                                <table class="table table-striped table-bordered table-responsive text-left my-orders-table" align="center" style="width: 98%;" runat="server" id="tbl_Feedetailsgrid">
                                    <tr id="tr_AddFee" runat="server" class="trSub_Header">
                                        <th align="left">
                                            <asp:HiddenField ID="hfPaymentProvider" runat="server" />
                                            Service Details</th>
                                    </tr>
                                    <tr>
                                        <td class="tdfields" align="right">
                                            <br />
                                            <table width="100%">
                                                <tr>
                                                    <td>

                                                        <asp:GridView ID="gvFeeCollection" runat="server" AutoGenerateColumns="False" EmptyDataText="No Details Added"
                                                            CssClass="table table-striped table-bordered table-responsive text-left my-orders-table" Width="100%">
                                                            <RowStyle Height="20px" VerticalAlign="Top" />
                                                            <Columns>
                                                                <asp:BoundField DataField="SERVICE" HeaderText="Service">
                                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="SSR_RATE" HeaderText="Amount">
                                                                    <ItemStyle HorizontalAlign="Right" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="SSR_DISCOUNT" HeaderText="Discount">
                                                                    <ItemStyle HorizontalAlign="Right" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="NETAMOUNT" HeaderText="Net Amount">
                                                                    <ItemStyle HorizontalAlign="Right" />
                                                                </asp:BoundField>
                                                                <asp:TemplateField HeaderText="Paying Now">
                                                                    <ItemTemplate>
                                                                        <asp:HiddenField ID="HF_SSR_ID" runat="server" Value='<%# Bind("SSR_ID") %>' />
                                                                        <asp:HiddenField ID="HF_NET_AMOUNT" runat="server" Value='<%# Bind("NETAMOUNT") %>' />
                                                                        <asp:HiddenField ID="HF_MINPAYAMOUNT" runat="server" Value='<%# Bind("FEE_MinPaymentAmount") %>' />
                                                                        <asp:HiddenField ID="HF_PossibleAdvPaymentAmount" runat="server" Value='<%# Bind("FEE_PossibleAdvPaymentAmount") %>' />
                                                                        <div style="text-align: right;">
                                                                            <asp:DropDownList ID="ddlAmountToPay" runat="server" AutoPostBack="True" CssClass="form-control" Style="text-align: right" OnSelectedIndexChanged="ddlAmountToPay_SelectedIndexChanged">
                                                                            </asp:DropDownList>
                                                                        </div>


                                                                        <asp:TextBox ID="txtAmountToPay" AutoCompleteType="Disabled" runat="server" AutoPostBack="True" Visible="False"
                                                                            onFocus="this.select();" OnTextChanged="txtAmountToPay_TextChanged" Style="text-align: right"
                                                                            TabIndex="52" Text='<%# Bind("Amount", "{0:0.00}") %>' CssClass="form-control"></asp:TextBox>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                                                    <ItemStyle HorizontalAlign="Right" Width="89px" />
                                                                </asp:TemplateField>

                                                            </Columns>
                                                        </asp:GridView>
                                                    </td>
                                                </tr>

                                                <tr id="Tr1" runat="server">
                                                    <td align="right" class="tdfields" valign="middle">
                                                        <h4>Total :
                <asp:TextBox ID="txtGridTotal" Style="text-align: right" runat="server" TabIndex="150"
                    CssClass="form-control">
                </asp:TextBox></h4>
                                                    </td>
                                                </tr>
                                                <tr id="Tr2" runat="server">
                                                    <td align="right" class="tdfields" valign="middle">
                                                        <h4>Processing Charge :
                <asp:TextBox ID="txtProcessingCharge" Style="text-align: right" runat="server" TabIndex="150"
                    CssClass="form-control">
                </asp:TextBox></h4>
                                                    </td>
                                                </tr>
                                                <tr id="tr_DiscountTotal" runat="server">
                                                    <td align="right" class="tdfields" valign="middle">
                                                        <h4>
                                                            <asp:HiddenField ID="hfCardCharges" runat="server" />
                                                            Net Total (<asp:Label ID="lblCurrency" runat="server"></asp:Label>
                                                            ):
                <asp:TextBox ID="txtTotal" Style="text-align: right" runat="server" TabIndex="151"
                    CssClass="form-control">
                </asp:TextBox></h4>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>


                                    <tr id="Tr29" runat="server">
                                        <td align="center" style="font-size: 12px; color: #ff0000; height: 18px">&nbsp;<asp:Button ID="btnSave" runat="server" CssClass="btn btn-info" TabIndex="155" Text="Confirm & Proceed" />
                                            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="btn btn-info" Text="Cancel" TabIndex="158" />
                                        </td>
                                    </tr>
                                </table>
                                <%--    <table id="Table1" style="width: 98%" runat="server" class="tableNoborder">
        <tr id="Tr29" runat="server">
            <td align="center" style="font-size: 12px; color: #ff0000; height: 18px">
                &nbsp;<asp:Button ID="btnSave" runat="server" CssClass="btn btn-info" TabIndex="155" Text="Confirm & Proceed"   />
                <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="btn btn-info" Text="Cancel" TabIndex="158" />
            </td>
        </tr>
    </table>--%>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
