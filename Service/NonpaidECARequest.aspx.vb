﻿Imports Microsoft.ApplicationBlocks.Data
Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Net.Mail
Partial Class Service_NonpaidECARequest
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("username") Is Nothing Then
            Session("Active_tab") = "Home"
            Session("Site_Path") = ""
            Response.Redirect("~\General\Home.aspx")
        ElseIf Session("bPasswdChanged") = "False" Then
            Response.Redirect("~\UpdateInfo\ForcePasswordChange.aspx", False)
        ElseIf Session("bUpdateContactDetails") = "False" Then
            Response.Redirect("~\UpdateInfo\ForceUpdatecontactdetails.aspx", False)
        End If

        If Not Page.IsPostBack Then
            'hfSave.Value = "0"
            lbChildName.Text = Session("STU_NAME")
            'hfTCM_ID.Value = "0"
            BindServiceGrid()
        End If

    End Sub
    Protected Sub gvActivity_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvActivity.PageIndex = e.NewPageIndex
        BindServiceGrid()
    End Sub
    Protected Sub gvActivity_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim lblStat As Label = DirectCast(e.Row.FindControl("lblStat"), Label)
            If lblStat.Text = "" Then
                lblStat.Text = "APPLY"
            End If
        End If
    End Sub

    Private Sub BindServiceGrid()
        Dim CONN As String = ConnectionManger.GetOASIS_SERVICESConnectionString

        Try
            Dim PARAM(5) As SqlParameter
            PARAM(0) = New SqlParameter("@BSU_ID", Session("STU_BSU_ID"))
            PARAM(1) = New SqlParameter("@ACD_ID", Session("STU_ACD_ID"))
            PARAM(2) = New SqlParameter("@GRD_ID", Session("STU_GRD_ID"))
            PARAM(3) = New SqlParameter("@Gender", 1)
            PARAM(4) = New SqlParameter("@STU_ID", Session("STU_ID"))
            Dim ds As DataSet

            ds = SqlHelper.ExecuteDataset(CONN, CommandType.StoredProcedure, "[ST].[GET_SERVICE_DETAILS_FOR_STUDENT]", PARAM)

            If ds IsNot Nothing Then
                If ds.Tables(0).Rows.Count > 0 Then
                    gvActivity.DataSource = ds.Tables(0)
                    gvActivity.DataBind()
                Else

                End If
            Else
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvActivity.DataSource = ds.Tables(0)
                Try
                    gvActivity.DataBind()

                Catch ex As Exception
                End Try

                Dim columnCount As Integer = gvActivity.Rows(0).Cells.Count

                gvActivity.Rows(0).Cells.Clear()
                gvActivity.Rows(0).Cells.Add(New TableCell)
                gvActivity.Rows(0).Cells(0).ColumnSpan = columnCount
                gvActivity.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvActivity.Rows(0).Cells(0).Text = "There is no activity exists for the specific grade."
            End If
        Catch ex As Exception
            Dim ex1 As String = ex.Message
        End Try
       

    End Sub
End Class
