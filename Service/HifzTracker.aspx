﻿<%@ Page Language="VB" AutoEventWireup="true" CodeFile="HifzTracker.aspx.vb" Inherits="Service_HifzTracker" MasterPageFile="~/ParentMaster.master" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphParent" runat="Server">
        <meta charset="utf-8" />

 <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <%--<script type="text/javascript" src="../Scripts/HifzTracker.js"></script>--%>

    <script type="text/javascript">
        var fileSizeInMB = 5;

        var validationErrorMessage = {
            FileSelect: 'Please select the audio/video file.',
            FileSize: 'File size can&#39;t exceed ' + fileSizeInMB + 'MB.',
            FileType: "Allowed extensions are {0}",
            FileSubmit: 'Error! A problem has been occurred while submitting your data.',
            TechnicalError: 'Techinal Error! Some problem occured while submitting your data.'
        };

        var fileUploader = function () {

            loadUploadPopup = function (source, isError) {
                $("#myModal").modal('show');

                if (source != undefined) {
                    $('#myModal').one('shown.bs.modal', function () {
                        clearUploadPopup();
                        initUploadPopup(source);
                    });
                }
                if (isError == true) showFileError(validationErrorMessage.FileSubmit);
            },

                initUploadPopup = function (source) {
                    $('#hdnTrackerId').val(source.data('trackerid'));
                    $('#hdnTransactionId').val(source.data('transactionid'));
                    $('#hdnVerseNo').val(source.data('verseno'));
                    var target = source.closest('.verse-field').find('input.verse-check');
                    $('#hdnIsCompleted').val(target.prop('checked'));
                },

                onClosePopup = function (filePath) {
                    $("#myModal").modal('hide');
                    $(".modal-backdrop").removeClass('in');
                    clearUploadPopup();
                },

                clearUploadPopup = function () {
                    $("#lblUploadedMsg").text('');
                    $("#lnkUploadedFile").text('');
                    $('#hdnTrackerId').val('');
                    $('#hdnVerseNo').val('');
                    $('#hdnIsCompleted').val('');
                    $('#hdnTransactionId').val('');
                },

                initializeDownloadFileRequest = function (source) {
                    var relationId = source.data('relationid');
                    $.ajax({
                        type: "POST",
                        url: "HifzTracker.aspx/SetDownloadPath",
                        data: '{ relationId: ' + relationId + ' }',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            if (data.d == true) {
                                var iframe = document.createElement("iframe");
                                iframe.src = "/Service/HifzFileDownloads.aspx";
                                iframe.style.display = "none";
                                document.body.appendChild(iframe);
                            }
                            else
                                showTechinalError()
                        },
                        failure: function (response) {
                            showTechinalError(response.d)
                        }
                    });
                },

                downloadHifzCertificateAsPdf = function (source) {
                    var trackerId = source.data('id');
                    $.ajax({
                        type: "POST",
                        url: '<%= ResolveUrl("~/Service/HifzTracker.aspx/SetCertificateDownloadPath")%>',
                      //  url: "HifzTracker.aspx/SetCertificateDownloadPath",
                        data: '{ trackerId: ' + trackerId + ' }',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            if (data.d == true) {
                                var iframe = document.createElement("iframe");
                                iframe.src = "/Service/HifzCertificate.aspx";
                                iframe.style.display = "none";
                                document.body.appendChild(iframe);
                            }
                            else
                                showTechinalError();
                        },
                        failure: function (response) {
                            showTechinalError(response.d)
                        }
                    });
                },

                validateAndSaveFileToCollection = function (uploader) {
                    if (uploader == undefined) {
                        uploader = document.getElementById('hifzFileUpload');
                    }
                    var file = uploader.files[0];
                    if (file == undefined) {
                        showFileError(validationErrorMessage.FileSelect);
                        return false;
                    }

                    var fileSize = file.size;
                    fileSize = (fileSize / (1024 * 1024)).toFixed(2);
                    if (fileSize > fileSizeInMB) {
                        showFileError(validationErrorMessage.FileSize);
                        return false;
                    }

                    var lblMsg = $(uploader).parents('div.fileuploader').find("#lblUploadedMsg");
                    var targetFileLink = $(uploader).parents('div.fileuploader').find(".file-input-name");
                    var invalidMessage = validateFileType(file.name);
                    if (invalidMessage == undefined || invalidMessage == '') {
                        lblMsg.text("");
                        targetFileLink.text("");
                    }
                    else {
                        showFileError(invalidMessage);
                        $(uploader).val("");
                        return false;
                    }

                    targetFileLink.attr('href', 'javascript:void(0);');
                    targetFileLink.text(file.name);
                    return true;
                },

                updateHifzTransaction = function (source) {
                    $.ajax({
                        type: "POST",
                        url: "HifzTracker.aspx/UpdateHifzTransaction",
                        data: '{ trackerId: ' + source.data('trackerid') + ', verseNo: "' + source.data('verseno') + '", isCompleted: ' + source.prop('checked') + ' }',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            if (parseInt(data.d) > 0)
                                showHideUploadButton(source, data.d);
                            else
                                showTechinalError()
                        },
                        failure: function (response) {
                            showTechinalError(response.d)
                        }
                    });
                },

                deleteAttachment = function (source) {
                    $.ajax({
                        type: "POST",
                        url: "HifzTracker.aspx/DeleteHifzTransactionAttachment",
                        data: '{ relationId: ' + source.data('relationid') + ' }',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            if (data.d == true)
                                showUploadAndHideDownloadButton(source);
                            else
                                showTechinalError()
                        },
                        failure: function (response) {
                            showTechinalError(response.d)
                        }
                    });
                },

                checkAttachmentExistsInGrid = function (targetGrid, value) {
                    var isExists = false;
                    var tblAttachment = $(targetGrid);
                    tblAttachment.find('tr > td.col-name').each(function () {
                        if ($(this).data('filename') == value) isExists = true;
                    });
                    return isExists;
                },

                validateFileType = function (fileName) {
                    var validExtensions = ['mp3', 'mp4', 'avi', 'aac', 'wav', 'mov', 'm4v','bwv','aiff','wma','m4a','ogg'];
                    var fileNameExt = fileName.substr(fileName.lastIndexOf('.') + 1).toLowerCase();
                    if ($.inArray(fileNameExt, validExtensions) == -1) {
                        return validationErrorMessage.FileType.replace("{0}", validExtensions.join(', '));
                    }
                },

                showHideUploadButton = function (source, id) {
                    source.prop('disabled', true);
                    var target = source.closest('.verse-field').find('span.file-field');
                    if (source.prop('checked'))
                        showUploadAndHideDownloadButton(source, id);
                    else
                        target.addClass('hidden');
                },

                showUploadAndHideDownloadButton = function (source, id) {
                    var target = source.closest('.verse-field').find('span.file-field');
                    var sourceData = source.data();
                    var iconTitle = "Upload Audio/Video for verse " + sourceData.verseno;
                    var checkboxAttr = " data-verseno='" + sourceData.verseno + "' data-trackerid='" + sourceData.trackerid + "' data-transactionid='" + id + "'";
                    var uploadIcon = "<i class='fa fa-upload upload-field' title='" + iconTitle + "' " + checkboxAttr + " ></i>";
                    target.html(uploadIcon);
                    target.removeClass('hidden');
                },



                showFileError = function (message) {
                    $("#lblUploadedMsg").html(message)
                    $("#lblUploadedMsg").addClass("field-validation-error");
                },

                showTechinalError = function (failureMessage) {
                    console.log(failureMessage);
                    alert(validationErrorMessage.TechnicalError);
                },


                getURLParameter = function (name) {
                    name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
                    var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
                    var results = regex.exec(location.search);
                    return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
                },

            //Function used to remove querystring
                removeQueryStringFromURL = function (key) {
                    var urlValue = document.location.href;

                    //Get query string value
                    var searchUrl = location.search;

                    if (key != "") {
                        oldValue = getParameterByName(key);
                        removeVal = key + "=" + oldValue;
                        if (searchUrl.indexOf('?' + removeVal + '&') != "-1") {
                            urlValue = urlValue.replace('?' + removeVal + '&', '?');
                        }
                        else if (searchUrl.indexOf('&' + removeVal + '&') != "-1") {
                            urlValue = urlValue.replace('&' + removeVal + '&', '&');
                        }
                        else if (searchUrl.indexOf('?' + removeVal) != "-1") {
                            urlValue = urlValue.replace('?' + removeVal, '');
                        }
                        else if (searchUrl.indexOf('&' + removeVal) != "-1") {
                            urlValue = urlValue.replace('&' + removeVal, '');
                        }
                    }
                    else {
                        searchUrl = location.search;
                        urlValue = urlValue.replace(searchUrl, '');
                    }
                    history.pushState({ state: 1, rand: Math.random() }, '', urlValue);
                };

            return {
                validateAndSaveFileToCollection: validateAndSaveFileToCollection,
                updateHifzTransaction: updateHifzTransaction,
                loadUploadPopup: loadUploadPopup,
                onClosePopup: onClosePopup,
                deleteAttachment: deleteAttachment,
                clearUploadPopup: clearUploadPopup,
                getURLParameter: getURLParameter,
                initializeDownloadFileRequest: initializeDownloadFileRequest,
                removeQueryStringFromURL: removeQueryStringFromURL,
                downloadHifzCertificateAsPdf: downloadHifzCertificateAsPdf
            }
        }();



        $(document).ready(function () {
            $(document).on('change', '.verse-check', function () {
                fileUploader.updateHifzTransaction($(this));
            });

            $(document).on('click', '.upload-field', function () {
                fileUploader.loadUploadPopup($(this));
            });

            $(document).on('click', '.delete-attachment', function () {
                fileUploader.deleteAttachment($(this));
            });

            $(document).on('click', '.download-field', function () {
                var filepath = $(this).data('filepath');
            });

            $('#myModal').one('hide.bs.modal', function () {
                fileUploader.clearUploadPopup();
            });

            var isError = fileUploader.getURLParameter('error');
            if (isError != undefined && isError == 'true') {
                fileUploader.loadUploadPopup(undefined, true);
                fileUploader.removeQueryStringFromURL('error');
            }
        });

    </script>
    <style>
        .file-field {
            position: relative;
            display: inline-block;
            overflow: hidden;
            top: 2px;
            margin-right: 10px;
        }

        .file-field {
            cursor: pointer;
        }

            .file-field input[type=file] {
                position: absolute;
                top: 0;
                right: 0;
                left: 0;
                bottom: 0;
                width: 100%;
                margin: 0;
                padding: 0;
                cursor: pointer;
                opacity: 0;
                outline: solid 1px green;
            }



        .RadGrid_Default input {
            position: relative;
            top: 2px;
        }



        .panel-heading.custom {
            border-top-left-radius: 3px !important;
            border-top-right-radius: 3px !important;
            border-radius: 3px !important;
            padding: 10px 15px;
        }

        .file-input-wrapper input[type=file], .file-input-wrapper input[type=file]:focus, .file-input-wrapper input[type=file]:hover {
            position: absolute;
            top: 0;
            left: 0;
            cursor: pointer;
            opacity: 0;
            z-index: 99;
            outline: 0;
            height: 30px;
        }

        .panel .panel-footer {
            background: #F5F5F5;
            border: 0;
            border-top: 1px solid #E3E3E3;
            line-height: 30px;
            padding: 10px;
            float: left;
            width: 100%;
            border-bottom-right-radius: 3px;
            border-bottom-left-radius: 3px;
        }

        .modal-content .panel {
            margin-bottom: 0px;
        }

        .modal-dialog .btn {
            font-size: 12px;
            padding: 4px 15px !important;
            line-height: 20px;
            margin: 0px !important;
        }

        .field-validation-error {
            color: red;
            font-size: 12px;
        }

        .padding-0 {
            padding: 0px !important;
        }

        .fa-upload {
            color: #3772be;
        }

        .fa-download {
            color: orange;
        }

        .fa-trash {
            color: red;
        }

        .fa-check-square {
            color: green;
        }

        span.verse-field {
            width: 20%;
            display: inline-block;
        }
    </style>

    <div class="content margin-top30 margin-bottom60">
        <div class="container">
            <div class="row">
                <div class="posts-block col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="my-account">
                        <div class="title-box">
                            <h3>The Holy Quran Hif'z Tracker
                            <span class="profile-right">
                                <asp:Label ID="lbChildName" runat="server"></asp:Label>
                            </span></h3>
                        </div>
                        <div>
                             <div  class="alert alert-warning">
                           
                            <div>
                                <asp:Label ID="lblError" runat="server" Text="" />

                            </div>
                                </div>
               
           
                            <telerik:RadGrid ID="gv_HifzTracker" runat="server" AutoGenerateColumns="False" GridLines="None" OnItemDataBound="gv_HifzTracker_ItemDataBound" ViewStateMode="Enabled">
                                <MasterTableView GridLines="Both" DataKeyNames="HifzTrackerId,NoOfVerses,IsCertificateEnabled">
                                    <Columns>
                                        <telerik:GridTemplateColumn HeaderText="S.No" ItemStyle-Width="5%">
                                            <ItemTemplate>
                                                <asp:Label ID="lblRowIndex" runat="server" Text='<%# Bind("RowIndex") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn HeaderText="Link" ItemStyle-Width="15%">
                                            <ItemTemplate>
                                                <a id="hifzTrackerURL" target="_blank" href='<%# If(Convert.ToString(Eval("URL")).StartsWith("http"), Eval("URL"), "https://" & Eval("URL"))   %>'><%# Eval("URL") %></a>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn HeaderText="Surah" ItemStyle-Width="12%">
                                            <ItemTemplate>
                                                <asp:Label ID="lblTitle" runat="server" Text='<%# Bind("Title") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn HeaderText="No. of verses" ItemStyle-Width="53%">
                                            <ItemTemplate>
                                                <asp:Literal ID="tblVersesHtml" runat="server" EnableViewState="false"></asp:Literal>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </telerik:GridTemplateColumn>
                                       <%-- <telerik:GridTemplateColumn HeaderText="Download the certificate" ItemStyle-Width="15%" UniqueName="DownloadCertificate">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="btnDownloadPdf" runat="server" CssClass=" fa fa-save fa-2x" Text="" OnClick="btnDownloadPdf_Click" CommandArgument='<%#Eval("HifzTrackerId")%>' />
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" />
                                        </telerik:GridTemplateColumn>--%>
                                        <telerik:GridTemplateColumn HeaderText="Download the certificate" ItemStyle-Width="15%" UniqueName="DownloadCertificate">
                                            <ItemTemplate>
                                                <a id="btnDownloadPdf" class=" fa fa-save fa-2x" href="javascript:void(0)" data-id='<%#Eval("HifzTrackerId") %>' onclick='fileUploader.downloadHifzCertificateAsPdf($(this));' />
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" />
                                        </telerik:GridTemplateColumn>
                                    </Columns>
                                </MasterTableView>
                            </telerik:RadGrid>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="panel panel-info">
                            <div class="panel-heading custom">
                                <span id="modal_heading">Upload Audio or Video</span>
                                <span style="margin-left:180px;" class="text-danger">Max file size 5mb</span>
                                <button id="btnClose" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" id="btn_model">&times;</span></button>
                            </div>
                            <div id="modalBody" class="panel-body full-width">
                                <div id="modal_Loader">
                                    <div class="panel-body">
                                        <div class="fileuploader">
                                            <div class="input-group">
                                                <a class="file-input-wrapper btn btn-default fileinput">
                                                    <span>Upload</span>
                                                    <asp:FileUpload runat="server" ID="hifzFileUpload" enctype="multipart/form-data" ClientIDMode="Static" onchange="fileUploader.validateAndSaveFileToCollection(this);return false;" CssClass="file form-field valid" />
                                                </a>
                                                &nbsp;
                                                <a id="lnkUploadedFile" class="file-input-name" href="javascript:void(0);" target="_blank"></a>
                                            </div>
                                            <asp:HiddenField ID="hdnVerseNo" runat="server" ClientIDMode="Static" />
                                            <asp:HiddenField ID="hdnTrackerId" runat="server" ClientIDMode="Static" />
                                            <asp:HiddenField ID="hdnTransactionId" runat="server" ClientIDMode="Static" />
                                            <asp:HiddenField ID="hdnIsCompleted" runat="server" ClientIDMode="Static" />
                                            <span id="lblUploadedMsg" class="col-md-12 padding-0"></span>
                                        </div>
                                    </div>
                                    <div class="panel-footer">
                                        <button class="pull-left btn btn-info" id="btnCancel" type="button" onclick="fileUploader.onClosePopup()">Cancel</button>
                                        <%--<input type="button" id="btnHifzTrackerUpload" class="pull-right btn btn-info" onclick="fileUploader.saveHifzTransactionAttachment()" value="Save"  />--%>
                                        <asp:Button runat="server" CssClass="pull-right btn btn-info" Text="Save" ID="btnHifzTrackerUpload" OnClick="btnHifzTrackerUpload_Click" OnClientClick="return fileUploader.validateAndSaveFileToCollection()" />
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnHifzTrackerUpload" />
            <%-- <asp:AsyncPostBackTrigger ControlID="btnHifzTrackerUpload" />--%>
        </Triggers>
    </asp:UpdatePanel>
    <CR:CrystalReportSource ID="rs" runat="server" CacheDuration="1">
    </CR:CrystalReportSource>
</asp:Content>

