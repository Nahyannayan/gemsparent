﻿<%@ Page Language="VB" MasterPageFile="~/ParentMaster.master" AutoEventWireup="false"
    CodeFile="ECARequestView.aspx.vb" Inherits="Others_ECARequestView" Title="GEMS EDUCATION" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphParent" runat="Server">



    
                <!-- Posts Block -->
    <div class="content margin-top30 margin-bottom60">
        <div class="container">
            <div class="row">

                <!-- Posts Block -->
                <div class="posts-block col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="my-account">
                     
           
                        <div>
                            
                               <div class="mainheading">
                            <div >Extra Curricular Activity Request</div>
                                      <div class="right" style="display:none;">
            <asp:Label ID="lbChildName" runat="server" style="display:none;">
            </asp:Label></div>
                           
                        </div>
                            <div align="left">

  <table  class="BlueTable_simple" width="100%"   cellpadding="8" cellspacing="0" > 
   
           </table>
      <table align="center"  class="table table-striped table-bordered table-responsive text-left my-orders-table" width="50%">
          <tr>
              <td class="subheader_img">
                  <strong>Requested Activities</strong></td>
          </tr>
          <tr id="trMessage" runat="server" >
              <td class="subheader_img" align="left">
                  To change the durations for any activity, kindly cancel it first and then make a 
                  new request.</td>
          </tr>
          <tr  >
              <td class="subheader_img" align="center">
                  <asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label>
              </td>
          </tr>
          <tr >
              <td class="subheader_img" align="center">
                  <asp:GridView ID="gvActivity" runat="server" AutoGenerateColumns="False" 
                      EnableModelValidation="True" Width="100%" CssClass="table table-striped table-bordered table-responsive text-left my-orders-table" AllowPaging="true" PageSize="25">
                      <Columns>
                          <asp:TemplateField HeaderText="Category">
                              <ItemTemplate>
                                  <asp:Label ID="lblCategory" runat="server" Text='<%# Bind("Category") %>'></asp:Label>
                              </ItemTemplate>
                              <HeaderStyle HorizontalAlign="Center" />
                              <ItemStyle Width="25%" HorizontalAlign="Center" />
                          </asp:TemplateField>
                          <asp:TemplateField HeaderText="Activity">
                              <ItemTemplate>
                                  <asp:Label ID="lblSubCategory" runat="server" Text='<%# Bind("SubCategory") %>'></asp:Label>
                              </ItemTemplate>
                              <HeaderStyle HorizontalAlign="Left" />
                              <ItemStyle Width="25%" HorizontalAlign="Left" />
                          </asp:TemplateField>
                          <asp:TemplateField HeaderText="Durations">
                              <ItemTemplate>
                                  <asp:Label ID="lblDuration" runat="server" Text='<%# Bind("Duration") %>'></asp:Label>
                              </ItemTemplate>
                              <HeaderStyle HorizontalAlign="Left" />
                              <ItemStyle Width="30%" HorizontalAlign="Left" />
                          </asp:TemplateField>
                          <asp:TemplateField HeaderText="Status">
                              <ItemTemplate>
                                  <asp:Label ID="lblStatus" runat="server" Text='<%# Bind("Status") %>'></asp:Label>
                              </ItemTemplate>
                              <ItemStyle HorizontalAlign="Left" Width="20%" />
                          </asp:TemplateField>
                          <asp:TemplateField HeaderText="Status Flag" Visible="False">
                              <ItemTemplate>
                                  <asp:Label ID="lblStatusFlag" runat="server" Text='<%# Bind("StatusFlag") %>'></asp:Label>
                              </ItemTemplate>
                          </asp:TemplateField>
                          <asp:TemplateField HeaderText="SSR_Id" Visible="False">
                              <ItemTemplate>
                                  <asp:Label ID="lblSSRID" runat="server" Text='<%# Bind("SSRID") %>'></asp:Label>
                              </ItemTemplate>
                          </asp:TemplateField>
                        <asp:TemplateField HeaderText="Online" Visible="False">
                              <ItemTemplate>
                                  <asp:Label ID="lblOnline" runat="server" Text='<%# Bind("Online") %>'></asp:Label>
                              </ItemTemplate>
                          </asp:TemplateField>
                         <%-- <asp:TemplateField HeaderText="Mandatory" Visible="False">
                              <ItemTemplate>
                                  <asp:Label ID="lblMandatory" runat="server" Text='<%# Bind("Mandatory") %>'></asp:Label>
                              </ItemTemplate>
                          </asp:TemplateField>--%>
                          <asp:CommandField DeleteText="Cancel" ShowDeleteButton="False" Visible="False">
                          <HeaderStyle HorizontalAlign="Center"  />
                          <ItemStyle ForeColor="Red" Font-Bold="False" HorizontalAlign="Right" 
                              Width="5%" />
                          </asp:CommandField>
                          <asp:TemplateField>
                              <ItemTemplate>
                                  <asp:ImageButton ID="btnCancel" runat="server" CommandName="Delete" 
                                      ImageUrl="~/Images/DELETE.png"  
                                      onclientclick="return confirm('Are you sure you want to cancel the selected ECA request?');" 
                                      EnableTheming="True" Height="14px" Width="14px" />
                              </ItemTemplate>
                               <ItemStyle HorizontalAlign="center" Width="20%" />
                          </asp:TemplateField>
                      </Columns>
                       <RowStyle CssClass="griditem" Height="25px" />
                    <SelectedRowStyle BackColor="Aqua" />
                    <HeaderStyle CssClass="gridheader_pop" Height="25px" />
                    <AlternatingRowStyle CssClass="griditem_alternative" />
                  </asp:GridView>
              </td>
          </tr>
      </table>

  <div id="divbutton" runat="server" style="width:100%;text-align:left;" align="left"> 
      <asp:HiddenField ID="hfStudentGender" runat="server" value="0" />
      <asp:HiddenField ID="hfGender" runat="server" value="3" />
      <asp:HiddenField ID="hfTerms" runat="server" value="0" />
    </div>
   
<div id="divTC"  runat="server" style="display: none; overflow: visible;background-color:White; border-color: #b5cae7; 
          border-style:solid;border-width:4px;width:700px;">
            <div  style="width: 701px;margin-top:0px;vertical-align:middle;background-color:White;">   
                       <span style="clear: right;display: inline; float: right; visibility: visible;margin-top:0px; vertical-align:top; ">
               <asp:ImageButton ID="btnCloseedit" runat="server" ImageUrl="~/Images/Common/PageBody/close.png" 
               /></span> 
           </div>
           
       
<asp:Panel id="plTC" runat="server" Width="701px" height="500px" 
 BackColor="White" BorderColor="Transparent" BorderStyle="none"  ScrollBars="None">
  <div class="mainheading">
        <div class="left">
            TC Request</div>
          </div>
    <table runat="server" class="tableNoborder" style="width: 100%">
       <tr>
       <td colspan="3">
                <asp:Label ID="lbltcMsg" runat="server" ></asp:Label>
              </td>
       
       </tr>
        <tr class="trHeader">
            <td colspan="3">
                &nbsp;Parent Info
            </td>
        </tr>
         <tr>
            <td align="left" class="tdfields">
                Passport Name
            </td>
            <td align="left">
                <asp:Label ID="lblName" runat="server" Text="Label"></asp:Label>
            </td>
        </tr>
         <tr>
            <td align="left" class="tdfields">
               Grade & Section
            </td>
            <td align="left">
                <asp:Label ID="lblGrade" runat="server" Text="Label"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="left" class="tdfields">
                Name of Father
            </td>
            <td align="left">
                <asp:Label ID="lblFather" runat="server" Text="Label"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="left" class="tdfields">
                Primary Contact Mob.No
            </td>
            <td align="left" >
                <asp:Label ID="lblPhone" runat="server" Text="Label"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="left" class="tdfields">
                Primary Contact Email ID
            </td>
            <td align="left" >
                <asp:Label ID="lblEmail" runat="server" Text="Label"></asp:Label>
            </td>
        </tr>
        <tr class="trHeader">
            <td colspan="3">
                &nbsp;Details
            </td>
        </tr>
        <tr>
            <td align="left" class="tdfields">
                Last Date of Attendance<span style="color: red">*</span>
            </td>
            <td align="left" >
                <asp:TextBox ID="txtLastDt" runat="server" CssClass="form-control" ></asp:TextBox>
                 <asp:ImageButton ID="imgLastDt" runat="server" ImageUrl="~/Images/Common/PageBody/Calendar.png"  CssClass="imgCal"/>
                    <asp:RequiredFieldValidator id="rfvLastDT" runat="server" ControlToValidate="txtLastDt"
                    Display="Dynamic" ErrorMessage="Please enter the last date of attendance" ForeColor="red" ValidationGroup="popValid">*</asp:RequiredFieldValidator>              
                 <asp:RegularExpressionValidator ID="REVLastDT"
                    runat="server" ControlToValidate="txtLastDt" Display="Dynamic" EnableViewState="False"
                    ErrorMessage="Enter Last Attendance  Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                    ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                    ValidationGroup="popValid">*</asp:RegularExpressionValidator><asp:CustomValidator
                        ID="cvFrmDt" runat="server" ControlToValidate="txtLastDt"
                        Display="Dynamic" EnableViewState="False" ErrorMessage="Last attendance date entered is not a valid date"
                        ForeColor="red" ValidationGroup="popValid">*</asp:CustomValidator><div class="remark">(dd/mmm/yyyy)</div>
            </td>
                    </tr>
        <tr>
            <td align="left" class="tdfields" valign="top">
                Name of the school to be transferred to
            </td>
            <td align="left" >
                <asp:DropDownList ID="ddlTrans_School" CssClass="form-control" runat="server" >
                </asp:DropDownList>
                <br />
                (If you choose other,please specify the school)<br />
                <asp:TextBox ID="txtOthers" runat="server" CssClass="form-control"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="left" class="tdfields">
                Country<span style="color: red">*</span>
            </td>
            <td align="left" >
                <asp:DropDownList ID="ddlCountry" CssClass="form-control" runat="server" >
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td align="left" class="tdfields">
                Transfer Type<span style="color: red">*</span>
            </td>
            <td align="left">
                <asp:DropDownList ID="ddlTransferType" CssClass="form-control" runat="server" >
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td align="left" class="tdfields">
                Reason For Transfer / Withdrawal
            </td>
            <td>
                <asp:DropDownList ID="ddlReason" CssClass="form-control" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td colspan="3" align="center">
                <asp:Button ID="btnSave" runat="server" Text="Save"  CssClass="btn btn-info"/>
                <asp:Button ID="btnCancel" runat="server" CausesValidation="False" Text="Close"   CssClass="btn btn-info" />
            </td>
        </tr>
    </table>
    </asp:Panel>
    
   
    </div>
    <div id="dvTc2"  runat="server" style="display: none; overflow: visible;background-color:White; border-color: #b5cae7; 
          border-style:solid;border-width:4px;width:300px;height:200px;">
     <asp:Panel ID="PnlTc2" runat="server" >
      <table id="Table1" runat="server" class="table table-striped table-bordered table-responsive text-left my-orders-table" style="width: 100%" cellspacing="7">
       <tr>
          
            <td align="left" colspan="2" class="tdfields">
                <asp:Label ID="lblLastDtText" runat="server" Text="Label"></asp:Label>
            </td>
        </tr>
       <tr>
            <td align="left" class="tdfields">
                Last Date of Attendance<span style="color: red">*</span>
            </td>
            <td align="left" width="200px" >
                <asp:TextBox ID="txtLastDtConfirm" runat="server"  CssClass="form-control"></asp:TextBox>
                 <asp:ImageButton ID="imgLastDtConfirm" runat="server" ImageUrl="~/Images/Common/PageBody/Calendar.png"  CssClass="imgCal"/>
                    <asp:RequiredFieldValidator id="RequiredFieldValidator1" runat="server" ControlToValidate="txtLastDtConfirm"
                    Display="Dynamic" ErrorMessage="Please enter the last date of attendance" ForeColor="red" ValidationGroup="popValid">*</asp:RequiredFieldValidator>              
                 <asp:RegularExpressionValidator ID="RegularExpressionValidator1"
                    runat="server" ControlToValidate="txtLastDtConfirm" Display="Dynamic" EnableViewState="False"
                    ErrorMessage="Enter Last Attendance  Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                    ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                    ValidationGroup="popValid">*</asp:RegularExpressionValidator><asp:CustomValidator
                        ID="CustomValidator1" runat="server" ControlToValidate="txtLastDtConfirm"
                        Display="Dynamic" EnableViewState="False" ErrorMessage="Last attendance date entered is not a valid date"
                        ForeColor="red" ValidationGroup="popValid">*</asp:CustomValidator><div class="remark">(dd/mmm/yyyy)</div>
            </td>
           
        </tr>
         <tr>
            <td colspan="3" align="center">
                <asp:Button ID="btnAccept" runat="server" Text="Accept" CssClass="btn btn-info" />
                <asp:Button ID="btnCancelConfirm" runat="server" CausesValidation="False" Text="Cancel"  CssClass="btn btn-info" />
            </td>
        </tr>
      </table> 
    </asp:Panel>
    </div>

                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </asp:Content>
