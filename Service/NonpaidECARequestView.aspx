﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="NonpaidECARequestView.aspx.vb"
    Inherits="Service_NonpaidECARequestView" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

    <title>GEMS EDUCATION</title>
  <link href="../CSS/SiteStyle.css" rel="stylesheet" type="text/css" />
    <script src="../Scripts/jquery-1.9.1.js" type="text/javascript"></script>

    <script type="text/javascript" src="../Scripts/jquery-ui.js"></script>

    <script type="text/javascript">


        function fancyClose() {

            parent.$.fancybox.close();
            parent.location.reload();
        } 
    </script>
<style type="Text/css">
.mydiv {
    top: 20%;
    left: 50%;
    width:30em;
    height:18em;
    margin-top: -9em; /*set to a negative number 1/2 of your height*/
    margin-left: -15em; /*set to a negative number 1/2 of your width*/
    border: 1px solid #ccc;
    background-color: #f3f3f3;
    position:fixed;
}

</style>
</head>
<body>
    <form id="form1" runat="server">
      <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </ajaxToolkit:ToolkitScriptManager>
    <br />
          <div id="pldis" runat="server" class="ECABorder">
        <table style="width: 100%" cellspacing="0" cellpadding="0" border="0">
            <tr class="mainheading" style="margin-top: 15px; width: 97%;height:15px;">
                <td style="height: 25px" align="left">
                    <asp:Literal ID="ltHeader" runat="server" Text="Extra Curriculur Activities"></asp:Literal>
                    <asp:HiddenField ID="hdnSelected" runat="server" />
                </td>
            </tr>
        </table>
        <table width="97%" cellpadding="5" border="0" cellspacing="2" align="left"  bordercolor="#1b80b6" 
       
            id="tblCategory">
            <tr>
            <td colspan="2"> <asp:Label ID="lblMessage" runat ="server" ></asp:Label></td>
            </tr>
              <tr class="trHeader">
            <td colspan="2">
                &nbsp;Student Info
            </td>
        </tr>
             <tr>
                <td class="tdfields" style="color:#084777;">
                    Student No:
                </td>
                <td class="matters">
                    <asp:Label ID="lblStuNo" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="tdfields" style="color:#084777;">
                    Name :
                </td>
                <td class="matters">
                    <asp:Label ID="lblName" runat="server"></asp:Label>
                </td>
            </tr>
              <tr class="trHeader">
            <td colspan="2">
                &nbsp;Activity Details
            </td>
        </tr>
            <tr>
                <td class="tdfields" style="color:#084777;">
                    Category :
                </td>
                <td class="matters">
                    <asp:Label ID="lblCategory" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="tdfields" style="color:#084777;">
                    Activity :
                </td>
                <td class="matters">
                    <asp:Label ID="lblActivity" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="tdfields" style="color:#084777;">
                    Duration :
                </td>
                <td class="matters">
                    <asp:CheckBoxList ID="chkPeriods" runat="server" CellPadding="15" CellSpacing="15"
                        RepeatLayout="Flow">
                        
                    </asp:CheckBoxList>
                    <asp:Label ID="lblTermDesc" runat="server" ></asp:Label>
                </td>
            </tr>
             <tr>
                <td class="tdfields" style="color:#084777;">
                    Day(s) :
                </td>
                <td class="matters">
                 
                    <asp:Label ID="lblDays" runat="server" ></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="tdfields" style="color:#084777;">
                    Max Seat :
                </td>
                <td class="matters">
                    <asp:Label ID="lblMaxSeat" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="tdfields" style="color:#084777;">
                    Available Seat :
                </td>
                <td class="matters">
                    <asp:Label ID="lblAvailable" runat="server"></asp:Label>
                </td>
            </tr>
            <tr id="trStatus" runat="Server">
                <td class="tdfields" style="color:#084777;">
                    Status :
                </td>
                <td class="matters">
                    <asp:Label ID="lblStatus" runat="server"></asp:Label>
                </td>
            </tr>
              <tr>
                <td class="tdfields" style="color:#084777;">
                    Remarks :
                </td>
                <td class="matters">
                    <asp:Label ID="lblRemarks" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
            
                    <input type="button" class="button" id="btnCancel1" title="Close" value="Close"  onclick="fancyClose()" />
                       <asp:Button ID="btnSave" runat="server" Text="SAVE" CssClass="button"  />
                        <ajaxToolkit:ConfirmButtonExtender ID="cbeConfirmPDP" runat="server" TargetControlID="btnSave"
                        ConfirmText="Are you sure you want to Apply for this activity?" />
                </td>
            </tr>
            <tr>
            <td colspan="2">
            
             <div id="divNote" runat="server"  title="" visible="false"  ><span class="msgInfoclose"></span>
                                       
                        <asp:Label ID="lblError" runat="server" EnableViewState="false"></asp:Label>
                   
                </div>
            </td>
            </tr>
            
            
        </table>
        

    </div>
    </form>
</body>
</html>
