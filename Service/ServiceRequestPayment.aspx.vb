Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class ParentLogin_Service_ServiceRequestPayment
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Private Property TBLServiceRequests() As DataTable
        Get
            Return ViewState("TBLServiceRequests")
        End Get
        Set(ByVal value As DataTable)
            ViewState("TBLServiceRequests") = value
        End Set
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If Session("username") Is Nothing Then
                Response.Redirect("~\General\Home.aspx")
            End If
            hfPaymentProvider.Value = GetPaymentProvider(Session("sBsuid"))
            Me.txtGridTotal.Attributes.Add("readonly", "readonly")
            Me.txtProcessingCharge.Attributes.Add("readonly", "readonly")
            Me.txtTotal.Attributes.Add("readonly", "readonly")

            If Request.QueryString("SSRID") <> "" Then
                ViewState("SSRID") = Encr_decrData.Decrypt(Request.QueryString("SSRID").Replace(" ", "+"))
                BindStudentServices(ViewState("SSRID"))
            Else
                ViewState("SSRID") = 0
                BindStudentServices(0)
            End If

        End If
    End Sub
    Private Function getPendingServiceRequest(ByVal SSR_ID As Int64) As DataTable
        Dim con As String = ConnectionManger.GetOASISConnectionString
        Dim param(4) As SqlClient.SqlParameter
        param(0) = New SqlClient.SqlParameter("@SSR_ID", SSR_ID)
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "[ONLINE].[GET_PENDING_SERVICES_REQUESTED_PAYMENT]", param)
        If ds.Tables.Count > 0 Then
            getPendingServiceRequest = ds.Tables(0)
        Else
            getPendingServiceRequest = Nothing
        End If
    End Function
    Public Sub BindStudentServices(ByVal SSR_ID As Int64)
        Dim ds As New DataTable
        ds = getPendingServiceRequest(SSR_ID)
        If ds Is Nothing Then
            lblError.Text = "Error"
            TBLServiceRequests = Nothing
            btnSave.Enabled = False
        ElseIf ds.Rows.Count = 0 Then
            lblError.Text = "No Pending Payments."
            TBLServiceRequests = ds
            btnSave.Enabled = False
        Else
            lblDate.Text = Now.Date.ToString("dd/MMM/yyyy")
            TBLServiceRequests = ds
            Session("sBsuid") = TBLServiceRequests.Rows(0).Item("BSU_ID").ToString
            Session("SrvUsrName") = TBLServiceRequests.Rows(0).Item("OLU_USRNAME").ToString
            BindStudentDetail(TBLServiceRequests.Rows(0).Item("STU_ID"))
            lblCurrency.Text = TBLServiceRequests.Rows(0).Item("BSU_CURRENCY").ToString
            SetTotal(TBLServiceRequests)
            'txtTotal.Text = TBLServiceRequests.Compute("sum(Amount)", "").ToString
            gvFeeCollection.DataSource = ds
            gvFeeCollection.DataBind()
        End If
    End Sub

    Public Sub BindStudentDetail(ByVal stuid As String)
        Try
            Dim con As String = ConnectionManger.GetOASISConnectionString
            Dim param(0) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@STU_ID", stuid)
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "GETSTUDENTDETAIL", param)
            If ds.Tables(0).Rows.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    txtStdNo.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("STU_NO").ToString())
                    txtStudentname.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("STU_Name").ToString())
                    lblacademicyear.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("ACY_DESCR").ToString())
                    stugrd.Text = Convert.ToString((ds.Tables(0).Rows(0)("STU_GRD_ID")).ToString)
                    lblsct.Text = Convert.ToString(ds.Tables(0).Rows(0)("SCT_DESCR"))
                    lblschool.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("BSU_NAME").ToString())
                    Session("STU_NO") = Convert.ToString(ds.Tables(0).Rows(0).Item("STU_NO").ToString())
                    Session("STU_NAME") = Convert.ToString(ds.Tables(0).Rows(0).Item("STU_Name").ToString())
                    lblacademicyear.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("ACY_DESCR").ToString())
                    Session("STU_GRD_ID") = Convert.ToString((ds.Tables(0).Rows(0)("STU_GRD_ID")).ToString)
                    Session("stu_section") = Convert.ToString(ds.Tables(0).Rows(0)("SCT_DESCR"))
                    Session("BSU_NAME") = Convert.ToString(ds.Tables(0).Rows(0).Item("BSU_NAME").ToString())
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub btnServicePayment_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        DoCollection(ViewState("SSRID"))
    End Sub
    Public Function GetPaymentProvider(ByVal BSU_ID As String) As Int16
        Dim pParms(1) As SqlClient.SqlParameter
        hfCardCharges.Value = ""
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(0).Value = BSU_ID
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISFINConnectionString, _
          CommandType.StoredProcedure, "GetMerchantDetails_BSU_ID", pParms)
        If Not dsData Is Nothing AndAlso dsData.Tables.Count > 0 Then
            For Each row As DataRow In dsData.Tables(0).Rows
                If row("CPS_CPM_ID") = 2 Then
                    hfCardCharges.Value = hfCardCharges.Value & IIf(hfCardCharges.Value = "", "", "|") & row("CPS_ID") & "=" & row("CRR_CLIENT_RATE")
                    Return row("CPS_ID")
                End If
            Next
            Return 0
        End If
    End Function
    Protected Sub txtAmountToPay_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            If Not IsNumeric(sender.text) Then
                lblError.Text = "Invalid Amount"
                sender.text = 0
                Exit Sub
            End If
            Dim HF_SSR_ID As HiddenField
            If sender.parent IsNot Nothing Then
                HF_SSR_ID = sender.parent.findcontrol("HF_SSR_ID")
                If HF_SSR_ID IsNot Nothing Then
                    If TBLServiceRequests.Select("SSR_ID=" & Val(HF_SSR_ID.Value).ToString).Length > 0 Then
                        TBLServiceRequests.Select("SSR_ID=" & Val(HF_SSR_ID.Value).ToString)(0)("Amount") = sender.text
                        SetTotal(TBLServiceRequests)
                        'txtGridTotal.Text = TBLServiceRequests.Compute("sum(Amount)", "").ToString
                        'txtProcessingCharge.Text = Format(Math.Ceiling(GetCardProcessingCharge(2) * Val(txtGridTotal.Text)), "0.00")
                        'txtTotal.Text = Format(Val(txtGridTotal.Text) + Val(txtProcessingCharge.Text), "0.00")
                    End If
                End If
            End If

        Catch ex As Exception

        End Try
    End Sub
    Private Sub SetTotal(ByVal dt As DataTable)
        txtGridTotal.Text = dt.Compute("sum(Amount)", "").ToString
        txtProcessingCharge.Text = Format(Math.Ceiling(GetCardProcessingCharge(CInt(hfPaymentProvider.Value)) * Val(txtGridTotal.Text)), "0.00")
        txtTotal.Text = Format(Val(txtGridTotal.Text) + Val(txtProcessingCharge.Text), "0.00")
    End Sub
    Public Function GetCardProcessingCharge(ByVal CPS_ID As Integer) As Double
        Dim CardCharge As Double = 0
        If hfCardCharges.Value <> "" And CPS_ID <> 0 Then
            Dim Temp As String()
            Temp = hfCardCharges.Value.Split("|")
            For i As Int16 = 0 To Temp.Length - 1
                If Temp(i).Split("=")(0) = CPS_ID Then
                    CardCharge = Temp(i).Split("=")(1) / 100
                    Exit For
                End If
            Next
        End If
        Return CardCharge
    End Function
    Private Function DoCollection(ByVal SSR_ID As Int64) As Boolean
        Dim vpc_OrderInfo As String = String.Empty
        Dim vpc_ReturnURL As String = String.Empty
        Dim boolPaymentInitiated As Boolean = False
        Dim str_error As String = ""
        Dim PaymentGatewayID As Int16 = 0
        Dim STUNo As String, STU_ID As Int32, ACD_ID As Int32
        Dim BSU_ID As String
        Dim PostDate, SSR_IDs As String
        PostDate = Now.Date.ToString("dd/MMM/yyyy")
        SSR_IDs = ""
        If TBLServiceRequests.Rows.Count = 0 Then
            str_error = str_error & "No pending payments<br />"
        End If

        Dim dblTotal As Decimal = 0

        STUNo = "" : BSU_ID = "" : ACD_ID = 0
        If TBLServiceRequests.Rows.Count > 0 Then
            PaymentGatewayID = GetPaymentProvider(TBLServiceRequests.Rows(0).Item("BSU_ID"))
            STUNo = TBLServiceRequests.Rows(0).Item("STU_NO")
            STU_ID = TBLServiceRequests.Rows(0).Item("STU_ID")
            BSU_ID = TBLServiceRequests.Rows(0).Item("BSU_ID")
            ACD_ID = TBLServiceRequests.Rows(0).Item("ACD_ID")
            dblTotal = TBLServiceRequests.Compute("sum(Amount)", "")
            txtProcessingCharge.Text = Format(Math.Ceiling(GetCardProcessingCharge(PaymentGatewayID) * dblTotal), "0.00")
            dblTotal = Format(dblTotal + Val(txtProcessingCharge.Text), "0.00")

            dblTotal = CDbl(dblTotal)
            If dblTotal <= 0 Then
                str_error = str_error & "Invalid amount <br />"
            End If
        End If
        If PaymentGatewayID = 0 Then
            str_error = str_error & "Invalid payment gateway <br />"
        End If
        Dim irow As DataRow
        Dim AdvAmt As Double, PossibleAdvAmount As String, PossibleAdvAmounts() As String, iStr As String, iSuccess As Boolean, PossibleAdvAmountStr As String
        For Each irow In TBLServiceRequests.Rows
            If CDbl(irow("Amount")) > 0 And CDbl(irow("Amount")) > CDbl(irow("NetAmount")) Then
                iSuccess = False
                PossibleAdvAmount = irow("FEE_PossibleAdvPaymentAmount")
                PossibleAdvAmounts = PossibleAdvAmount.Split("|")
                AdvAmt = CDbl(irow("Amount")) - CDbl(irow("NetAmount"))
                PossibleAdvAmountStr = ""
                If PossibleAdvAmounts.Length = 0 Or CDbl(irow("FEE_MinPaymentAmount")) = 0 Then
                    Exit For
                End If
                For Each iStr In PossibleAdvAmounts
                    If IsNumeric(iStr) Then
                        PossibleAdvAmountStr &= IIf(PossibleAdvAmountStr = "", "", ",") & iStr & " " & irow("BSU_CURRENCY")
                        If AdvAmt = CDbl(iStr) Then
                            iSuccess = True
                            Exit For
                        End If
                    End If
                Next
                'If AdvAmt - CDbl(irow("FEE_MinPaymentAmount")) < 0 Then
                If Not iSuccess Then
                    str_error = "Possible Advance Fee Payment amount for the " & IIf(irow("SVC_TYPE") = 1, "instrument", "service") & " - " & irow("SERVICE") & " is " & PossibleAdvAmountStr & ". <br />"
                End If
            End If
        Next



        If str_error <> "" Then
            lblError.Text = str_error
            Exit Function
        End If
        '  Exit Function
        Dim str_new_FCL_ID As Long
        Dim str_NEW_FCL_RECNO As String = ""
        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim objConn As New SqlConnection(str_conn) '
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try
            Dim retval As String = "1000"
            Dim STR_TYPE As Char = "S"
            Dim mRow As DataRow

            retval = FeeCollectionOnline.F_SaveFEECOLLECTION_H_ONLINE(0, "Online", PostDate, _
             ACD_ID, STU_ID, "S", dblTotal, False, str_new_FCL_ID, _
             BSU_ID, "Online Fee Collection", "CR", str_NEW_FCL_RECNO, _
            Request.UserHostAddress.ToString, PaymentGatewayID, stTrans)
            If retval = "0" Then
                For Each mRow In TBLServiceRequests.Rows
                    If CDbl(mRow("Amount")) > 0 Then
                        retval = FeeCollectionOnline.F_SaveFEECOLLSUB_ONLINE(0, str_new_FCL_ID, mRow("FEE_ID"), _
                                CDbl(mRow("Amount")), -1, CDbl(mRow("Amount")), 0, stTrans)
                        SSR_IDs &= mRow("SSR_ID") & "|"
                        If retval <> 0 Then
                            Exit For
                        End If
                    End If
                Next
            End If
            '----------------Added by Jacob, on 24/oct/2013----------------
            If retval = "0" And Val(Me.txtProcessingCharge.Text) > 0 Then
                retval = FeeCollectionOnline.F_SaveFEECOLLSUB_ONLINE(0, str_new_FCL_ID, 162, _
                                Val(Me.txtProcessingCharge.Text), -1, Val(Me.txtProcessingCharge.Text), 0, stTrans)
            End If

            If retval = "0" Then
                If dblTotal > 0 And retval = "0" Then 'cash  here
                    retval = FeeCollectionOnline.F_SaveFEECOLLSUB_D_ONLINE(0, str_new_FCL_ID, COLLECTIONTYPE.CASH, _
                     CDbl(dblTotal), "", PostDate, 0, "", "", "", stTrans, Val(Me.txtProcessingCharge.Text))
                End If
                If retval = "0" Then
                    ' retval = FEECOLLECTION_H_ONLINE_Validation(str_new_FCL_ID, stTrans)
                End If
                If retval = "0" Then
                    stTrans.Commit()
                    btnSave.Enabled = False
                    Session("vpc_Amount") = CStr(dblTotal * 100).Split(".")(0)
                    vpc_OrderInfo = "Payment for " & STUNo.Replace("  ", " ")
                    If vpc_OrderInfo.Length > 34 Then
                        vpc_OrderInfo = Left(vpc_OrderInfo, 34)
                    End If
                    Session("vpc_OrderInfo") = vpc_OrderInfo

                    vpc_ReturnURL = Request.Url.ToString.Replace("ServiceRequestPayment.aspx", "PaymentResultPage.aspx")
                    Session("vpc_ReturnURL") = vpc_ReturnURL
                    Session("vpc_MerchTxnRef") = str_new_FCL_ID
                    Session("CPS_ID") = PaymentGatewayID
                    Session("SSR_IDs") = SSR_IDs
                    Dim flagAudit As Integer = UtilityObj.operOnAudiTable("Fee Online Payment", str_new_FCL_ID, "Insert", Page.User.Identity.Name.ToString, Me.Page, Server.MachineName)
                    'lblError.Text = "<br />" & "Please note reference no. <B>" & str_new_FCL_ID & "</B><br />" & "Now press Proceed to continue.you are about to pay a amount of<B> AED." & dblTotal & "</B> "
                    lblError.Text = "<br />Click on Proceed to continue with this payment " & IIf(Session("BSU_CURRENCY") Is Nothing, "", Session("BSU_CURRENCY")) & " :" & dblTotal & "<br />" & _
                    "Please note reference no. " & str_new_FCL_ID & " of this transaction for any future communication.<br /><br />"
                    lblError.Text &= "Please do not close this window or Log off until you get the final receipt."
                    boolPaymentInitiated = True
                Else
                    stTrans.Rollback()
                    lblError.Text = getErrorMessage(retval)
                End If
            Else
                stTrans.Rollback()
                lblError.Text = getErrorMessage(retval)
            End If

        Catch ex As Exception
            stTrans.Rollback()
            lblError.Text = getErrorMessage(1000)
            Errorlog(ex.Message)
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
        If boolPaymentInitiated Then
            Response.Redirect("feePaymentRedirectNew.aspx")
            '  PaymentRedirect(vpc_ReturnURL, vpc_OrderInfo)
        End If
    End Function
    Sub PaymentRedirect(ByVal vpc_ReturnURL As String, ByVal vpc_OrderInfo As String)

        Dim data As New NameValueCollection()
        data("virtualPaymentClientURL") = "https://migs.mastercard.com.au/vpcpay"
        data("vpc_Command") = "pay"
        data("vpc_Version") = "1"
        data("vpc_ReturnURL") = vpc_ReturnURL
        data("vpc_Locale") = "en"
        data("vpc_OrderInfo") = vpc_OrderInfo
        data("Title") = "ASP VPC 3-Party"
        HttpHelper.RedirectAndPOST(Me.Page, "CS_VPC_3Party_DO.aspx", data)


        'data("vpc_AccessCode") = "2F06AB64" '"BDBBF779" '
        'data("vpc_MerchTxnRef") = Session("vpc_MerchTxnRef")
        'data("vpc_Merchant") = "001110246642"  '"TEST001110246451" 


        '<input id="Title" runat="server" name="Title" type="hidden" value="ASP VPC 3-Party" />
        '<input id="virtualPaymentClientURL" runat="server" name="virtualPaymentClientURL" type="hidden" value="https://migs.mastercard.com.au/vpcpay" />
        '<input id="vpc_Command" runat="server" name="vpc_Command" type="hidden" value="pay" />
        '<input id="vpc_Version" runat="server" name="vpc_Version" type="hidden" value="1" />
        '<input id="vpc_ReturnURL" runat="server" name="vpc_ReturnURL" type="hidden" />
        '<input id="vpc_Locale" runat="server" name="vpc_Locale" type="hidden" value="en" />
        '<input id="vpc_OrderInfo" runat="server" name="vpc_OrderInfo" type="hidden" />

    End Sub
    Public Shared Function FEECOLLECTION_H_ONLINE_Validation(ByVal FCO_ID As Integer, ByVal p_stTrans As SqlTransaction) As String
        Dim pParms(2) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@FCO_ID", SqlDbType.Int)
        pParms(0).Value = FCO_ID

        pParms(1) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(1).Direction = ParameterDirection.ReturnValue
        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "FEES.FEECOLLECTION_H_ONLINE_Validation", pParms)
        FEECOLLECTION_H_ONLINE_Validation = pParms(1).Value
    End Function

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect("ViewServices.aspx")
    End Sub
End Class


