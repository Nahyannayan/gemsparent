﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ParentMaster.master" AutoEventWireup="false" CodeFile="StudentServicePaymentResult.aspx.vb"
    Inherits="Service_StudentServicePaymentResult" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphParent" runat="Server">

    <%--    <style type="text/css">
        .style1 {
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 10px;
            font-weight: bold;
            color: #5a5a58;
            height: 31px;
        }

        .style2 {
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 10px;
            font-weight: bold;
            color: #5a5a58;
            height: 21px;
        }
    </style>--%>
    <script src="../Scripts/jquery-1.9.1.js" type="text/javascript"></script>
    <!-- Add fancyBox -->
    <script type="text/javascript" src="../Scripts/Fancybox/jquery.fancybox.js?v=2.1.5"></script>
    <script type="text/javascript" src="../Scripts/Fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
    <link rel="stylesheet" href="../Scripts/Fancybox/jquery.fancybox.css" type="text/css" media="screen" />
    <script type="text/javascript" src="../Scripts/PopupJQuery.js"></script>
    <%--<script type="text/javascript" language="javascript">
        function CheckForPrint() {
            if (document.getElementById('<%=h_Recno.ClientID %>').value != '') {
                var bsuid = document.getElementById('<%=hf_encr_bsuId.ClientID%>').value;
                showModelessDialog('StudentServiceFeeReceipt.aspx?type=REC&BsuId=' + bsuid + '&id=' + document.getElementById('<%=h_Recno.ClientID %>').value, '', "dialogWidth: 800px; dialogHeight: 700px; help: no; resizable: no; scroll: yes; status: no; unadorned: no;");
                return false;
            }
            else
                alert('Sorry. There is some problem with the transaction');
        }
    </script>--%>

    
   
                <!-- Posts Block -->
    <div class="content margin-top30 margin-bottom60">
        <div class="container">
            <div class="row">

                <!-- Posts Block -->
                <div class="posts-block col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="my-account">
                     
           
                        <div>
                             
                             <%--  <div class="mainheading">
                            <div ></div>
                                      
                           
                        </div>--%>
                             <asp:Label ID="lblError" runat="server" EnableViewState="False"></asp:Label>
                            <div align="left">

    <%--<table align="center" cellpadding="0" cellspacing="0" class="table table-striped table-bordered table-responsive text-left my-orders-table"  >
        <tr style="display: none;">
            <td class="HeaderBG" width="100%">
                <asp:Image ID="Image4" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/Common/PageBody/header1.png" />
            </td>
        </tr>
        <tr class="subheader_img">
            <td style="height: 19px" align="left">
               
            </td>
        </tr>
        <tr>
            <td>--%>
                <table width="100%" class="table table-striped table-bordered table-responsive text-left my-orders-table" >
                    <tr class="tdfields">
                        <th align="left" width="30%">Date
                        </th>
                        <td align="left">
                            <asp:Label ID="lblDate" runat="server" CssClass="tdfields"></asp:Label>
                        </td>
                    </tr>
                    <tr class="tdfields">
                        <th align="left">Student ID 
                        </th>
                        <td align="left">
                            <asp:Label ID="lblStudentNo" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr class="tdfields">
                        <th align="left">Student Name
                        </th>
                        <td align="left">
                            <asp:Label ID="lblStudentName" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr class="tdfields">
                        <th align="left">Transaction Reference
                        </th>
                        <td align="left" valign="middle">
                            <asp:Label ID="Label_MerchTxnRef" runat="server" />
                        </td>
                    </tr>
                    <tr class="tdfields">
                        <th align="left">Amount
                        </th>
                        <td align="left" valign="middle">
                            <asp:Label ID="Label_Amount" runat="server" />
                        </td>
                    </tr>
                    <tr class="tdfields">
                        <th align="left">Receipt Number
                        </th>
                        <td align="left" valign="middle">&nbsp;<asp:LinkButton ID="lnkReceipt" runat="server" CssClass="MenuLink"></asp:LinkButton>
                            <asp:HiddenField ID="h_Recno" runat="server" />
                            <asp:HiddenField ID="hf_encr_bsuId" runat="server" />
                        </td>
                    </tr>

                      <tr>
            <td align="left" style="font-weight: bold; font-size: 12px; color: #ff0000" colspan="2">
                <asp:Label ID="lblPrintMsg" runat="server" EnableViewState="False" Visible="False">Please print the receipt (Click on the receipt number) for your reference.</asp:Label>
            </td>
        </tr>
        <tr style="display: none;">
            <td align="center" style="font-weight: bold; font-size: 12px; color: #ff0000" colspan="2">
                <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="btn btn-info"
                    Text="Cancel" TabIndex="158" Visible="false" />
            </td>
        </tr>
                </table>
          <%--  </td>
        </tr>
      
    </table>--%>
    <br />
    <table align="center" border="0" width="70%" style="display: none">
        <tr class="title">
            <td colspan="2">
                <iframe id="frame1" scrolling="auto" runat="server"></iframe>
                <p>
                    <strong>&nbsp;Transaction Receipt Fields</strong>
                </p>
                <asp:Label ID="Label_Title" runat="server" />
            </td>
        </tr>
        <tr>
            <td align="right">
                <strong><i>VPC API Version: </i></strong>
            </td>
            <td>
                <asp:Label ID="Label_Version" runat="server" />
            </td>
        </tr>
        <tr class='shade'>
            <td align="right">
                <strong><i>Command: </i></strong>
            </td>
            <td>
                <asp:Label ID="Label_Command" runat="server" />
            </td>
        </tr>
        <tr>
            <td align="right">
                <strong><em>MerchTxnRef: </em></strong>
            </td>
            <td></td>
        </tr>
        <tr class="shade">
            <td align="right">
                <strong><em>Merchant ID: </em></strong>
            </td>
            <td>
                <asp:Label ID="Label_MerchantID" runat="server" />
            </td>
        </tr>
        <tr>
            <td align="right">
                <strong><em>OrderInfo: </em></strong>
            </td>
            <td>
                <asp:Label ID="Label_OrderInfo" runat="server" />
            </td>
        </tr>
        <tr class="shade">
            <td align="right">
                <strong><em>Transaction Amount: </em></strong>
            </td>
            <td></td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <div class='bl'>
                    Fields above are the primary request values.<hr>
                    Fields below are receipt data fields.
                </div>
            </td>
        </tr>
        <tr class="shade">
            <td align="right">
                <strong><em>Transaction Response Code: </em></strong>
            </td>
            <td>
                <asp:Label ID="Label_TxnResponseCode" runat="server" />
            </td>
        </tr>
        <tr>
            <td align="right">
                <strong><em>QSI Response Code Description: </em></strong>
            </td>
            <td>
                <asp:Label ID="Label_TxnResponseCodeDesc" runat="server" />
            </td>
        </tr>
        <tr class='shade'>
            <td align="right">
                <strong><i>Message: </i></strong>
            </td>
            <td>
                <asp:Label ID="Label_Message" runat="server" />
            </td>
        </tr>
        <asp:Panel ID="Panel_Receipt" runat="server" Visible="false">
            <!-- only display these next fields if not an error -->
            <tr>
                <td align="right">
                    <strong><em>Shopping Transaction Number: </em></strong>
                </td>
                <td>
                    <asp:Label ID="Label_TransactionNo" runat="server" />
                </td>
            </tr>
            <tr class="shade">
                <td align="right">
                    <strong><em>Batch Number for this transaction: </em></strong>
                </td>
                <td>
                    <asp:Label ID="Label_BatchNo" runat="server" />
                </td>
            </tr>
            <tr>
                <td align="right">
                    <strong><em>Acquirer Response Code: </em></strong>
                </td>
                <td>
                    <asp:Label ID="Label_AcqResponseCode" runat="server" />
                </td>
            </tr>
            <tr class="shade">
                <td align="right">
                    <strong><em>Receipt Number: </em></strong>
                </td>
                <td>
                    <asp:Label ID="Label_ReceiptNo" runat="server" />
                </td>
            </tr>
            <tr>
                <td align="right">
                    <strong><em>Authorization ID: </em></strong>
                </td>
                <td>
                    <asp:Label ID="Label_AuthorizeID" runat="server" />
                </td>
            </tr>
            <tr class="shade">
                <td align="right">
                    <strong><em>Card Type: </em></strong>
                </td>
                <td>
                    <asp:Label ID="Label_CardType" runat="server" />
                </td>
            </tr>
        </asp:Panel>
        <tr>
            <td colspan="2" style="height: 32px">
                <hr />
                &nbsp;
            </td>
        </tr>
        <tr class="title">
            <td colspan="2" height="25">
                <p>
                    <strong>&nbsp;Hash Validation</strong>
                </p>
            </td>
        </tr>
        <tr>
            <td align="right">
                <strong><em>Hash Validated Correctly: </em></strong>
            </td>
            <td>
                <asp:Label ID="Label_HashValidation" runat="server" />
            </td>
        </tr>
        <asp:Panel ID="Panel_StackTrace" runat="server">
            <!-- only display these next fields if an stacktrace output exists-->
            <tr>
                <td colspan="2">&nbsp;
                </td>
            </tr>
            <tr class="title">
                <td colspan="2">
                    <p>
                        <strong>&nbsp;Exception Stack Trace</strong>
                    </p>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Label ID="Label_StackTrace" runat="server" />
                </td>
            </tr>
        </asp:Panel>
        <tr>
            <td width="50%">&nbsp;
            </td>
            <td width="50%">&nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <asp:Label ID="Label_AgainLink" runat="server" />
            </td>
        </tr>
        <tr>
            <td align="center" colspan="2">
                <asp:Panel ID="Panel_Debug" runat="server" Visible="false">
                    <!-- only display these next fields if debug enabled -->
                    <table>
                        <tr>
                            <td>
                                <asp:Label ID="Label_Debug" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label_DigitalOrder" runat="server" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
    </table>

                                 </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>


