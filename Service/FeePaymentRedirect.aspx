<%@ Page Language="C#" DEBUG="false" %>

<!DOCTYPE HTML PUBLIC '-'W3C'DTD HTML 4.01 Transitional'EN'>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<script runat="server"> 

// _____________________________________________________________________________

// Declare the global variables
private string debugData ;


//______________________________________________________________________________

private class VPCStringComparer: IComparer 
{ 

    public int Compare(Object a, Object b) 
    {
        /*
         <summary>Compare method using Ordinal comparison</summary>
         <param name="a">The first string in the comparison.</param>
         <param name="b">The second string in the comparison.</param>
         <returns>An int containing the result of the comparison.</returns>
         */

        // Return if we are comparing the same object or one of the 
        // objects is null, since we don't need to go any further.
        if (a == b) return 0;
        if (a == null) return -1;
        if (b == null) return 1;

        // Ensure we have string to compare
        string sa = a as string;
        string sb = b as string;

        // Get the CompareInfo object to use for comparing
        System.Globalization.CompareInfo myComparer = System.Globalization.CompareInfo.GetCompareInfo("en-US");
        if (sa != null && sb != null)
        {
            // Compare using an Ordinal Comparison.
            return myComparer.Compare(sa, sb, System.Globalization.CompareOptions.Ordinal);
        }
        throw new ArgumentException("a and b should be strings.");
    }
}

//______________________________________________________________________________

private string CreateMD5Signature (string RawData)
{
    /*
     <summary>Creates a MD5 Signature</summary>
     <param name="RawData">The string used to create the MD5 signautre.</param>
     <returns>A string containing the MD5 signature.</returns>
     */
    
    System.Security.Cryptography.MD5 hasher = System.Security.Cryptography.MD5CryptoServiceProvider.Create();
    byte[] HashValue = hasher.ComputeHash(Encoding.ASCII.GetBytes(RawData));

    string strHex = "";
    foreach(byte b in HashValue) 
    {
        strHex += b.ToString("x2");
    }
    return strHex.ToUpper();
}

// _____________________________________________________________________________

private void Page_Load(object sender, System.EventArgs e)
{
    if (Session["username"] == null)
    {
        //Response.Redirect("~\\ParentLogin\\Login.aspx");
        Response.Redirect("~\\General\\Home.aspx");
    } 

    // This is secret for encoding the MD5 hash
    // This secret will vary from merchant to merchant
    // To not create a secure hash, let SECURE_SECRET be an empty string - ""
    // SECURE_SECRET can be found in Merchant Administration/Setup page 
    

    Panel_Debug.Visible = false;
    Panel_StackTrace.Visible = false;

    // define message string for errors
    string message = "No Messages";
    
    // error exists flag
    bool errorExists = false;

    // transaction response code
    string txnResponseCode = "";

    try
    {
       /*
        *************************
        * START OF MAIN PROGRAM *
        *************************
        */

        // NOTE: We use our own overloaded IComparer interface to ensure we do 
        // an Ordinal sort of the data.
        System.Collections.SortedList transactionData = new System.Collections.SortedList(new VPCStringComparer());

        // create start of QueryString data
        string queryString = "https://migs.mastercard.com.au/vpcpay";
        
        // Collect debug information
        # if DEBUG
            debugData += "<u>Data from Order Page</u><br/>";
        # endif

        // Transaction data should not be passed in in this manner for a 
        // production system - see notes above.
        // loop through all the data in the Page.Request.Form
        //foreach (string item in Page.Request.Form) {
            
        //    // Collect debug information
        //    # if DEBUG 
        //        debugData += item +"="+ Page.Request.Form[item] +"<br/>";
        //    # endif
            
        //   /* Only include those fields required for a transaction
        //    * Extract the Form POST data and ignore the Virtual Payment Client
        //    * URL,  the ReturnURL, the Submit button and any empty form fields, 
        //    * as we do not want to send these fields on, or in the case of the
        //    * vpc_ReturnURL modify it before sending. 
        //    */
        //    if ((Page.Request.Form[item] != "") && 
        //        (item != "virtualPaymentClientURL") && 
        //        (item != "SubButL") &&
        //        (item != "Title") &&
        //        !(item.StartsWith("__"))) 
        //    {
        //       // transactionData.Add (item, Page.Request.Form[item]);
        //    }
        //}

       /* The AgainLink is for the receipt/error page to do another transaction.
        * The title is only used to display on the receipt/error page
        *
        * Note: These parameters are ONLY used for this example and is NOT 
        * required for production code. 
        *
        * However, it does show how extra session variables can be added to the 
        * transaction request so they will be returned in the receipt.
        *
        * Transaction data should not be passed in as hidden fields but be
        * addedhere as key value pairs, e.g.
        * transactionData.Add("vpc_Merchant", merchantValue); 
        * transactionData.Add("vpc_Amount", amountValue);
        * etc.
        */
         string  MerchantID="", MerchantCode="",SECURE_SECRET="";
         FeeCollectionOnline.GetMerchantDetails_CPS_ID(ref MerchantID, ref MerchantCode, ref SECURE_SECRET, Session["CPS_ID"].ToString());
        //Our data
        transactionData.Add("vpc_AccessCode", MerchantCode);
        transactionData.Add("vpc_Merchant", MerchantID);
        transactionData.Add("vpc_MerchTxnRef", Session["vpc_MerchTxnRef"].ToString());
        transactionData.Add("vpc_Amount", Session["vpc_Amount"].ToString()); 
        
        // add the AgainLink & Title fields
        transactionData.Add ("AgainLink", Page.Request.ServerVariables["HTTP_REFERER"]);
       // transactionData.Add ("Title", Page.Request.Form["Title"]);

        //new changes eliminating post 
        transactionData.Add("virtualPaymentClientURL", "https://migs.mastercard.com.au/vpcpay");
        transactionData.Add("vpc_Command", "pay");
        transactionData.Add("vpc_Version", "1");
        transactionData.Add("vpc_ReturnURL", Request.Url.ToString().Replace("FeePaymentRedirect.aspx", "feePaymentResultPage.aspx"));
        transactionData.Add("vpc_Locale", "en");
        transactionData.Add("vpc_OrderInfo", Session["vpc_OrderInfo"]);
        transactionData.Add("Title", "ASP VPC 3-Party"); 

        // Collect debug information
        # if DEBUG
            debugData += "<br/><u>Data from Transaction Sorted List</u><br/>";
        # endif
              
        string rawHashData = SECURE_SECRET;
        string seperator = "?";

        // Loop through all the data in the SortedList transaction data
        foreach (System.Collections.DictionaryEntry item in transactionData) {

            // Collect debug information
            # if DEBUG 
                debugData += item.Key.ToString() +"="+ item.Value.ToString() +"<br/>";
            # endif
            
            // build the query string, URL Encoding all keys and their values
            queryString += seperator + System.Web.HttpUtility.UrlEncode(item.Key.ToString()) + "=" + System.Web.HttpUtility.UrlEncode(item.Value.ToString());
            seperator = "&";

            // Collect the data required for the MD5 sugnature if required
            if (SECURE_SECRET.Length > 0)
            {
                rawHashData += item.Value.ToString();
            }
        }

        // Create the MD5 signature if required
        string signature = "";
        if (SECURE_SECRET.Length > 0)
        {
			// create the signature and add it to the query string
            signature = CreateMD5Signature(rawHashData);
            queryString += seperator + "vpc_SecureHash=" + signature;
            
            // Collect debug information
            # if DEBUG 
                debugData += "<br/><u>Hash Data Input</u>: " + rawHashData + "<br/><br/><u>Signature Created</u>: "+signature+"<br/>";
            # endif
        }
        try
        {
            FeeCollectionOnline.SAVE_ONLINE_PAYMENT_AUDIT("FEES", "COMMAND", queryString, Session["vpc_MerchTxnRef"].ToString());
        }
        catch
        {
        }
        // Collect debug information
        # if DEBUG
            debugData += "<br/><u>Final QueryString Data String</u>: " + queryString + "<br/><br/><a href=\'" + queryString + "'>Click here to proceed.</a><br/>";
        # else
            // Transmit the DO to the Payment Server via a browser redirect
       
            
            Page.Response.Redirect(queryString);
        # endif
       
        
    }
    catch (Exception ex)
    {
       message = "(51) Exception encountered. " + ex.Message;
       if (ex.StackTrace.Length > 0)
       {
           UtilityObj.Errorlog(ex.Message,"Redirect");
           Label_StackTrace.Text = ex.ToString();
           Panel_StackTrace.Visible = true;
       }
       errorExists = true;
    }
    
    Label_Message.Text = message;

    // Output debug data to the screen
    # if DEBUG
        debugData += "<br/><br/>End of debug information<br/>";
        Label_Debug.Text    = debugData;
        Panel_Debug.Visible = true;
    # endif

   /*
    **********************
    * END OF MAIN PROGRAM
    **********************
    *
    * FINISH TRANSACTION - Output the VPC Response Data
    * =====================================================
    * For the purposes of demonstration, we simply display the Result fields
    * on a web page.
    */
}
</script>

<title>Redirecting to Payment Gateway</title>
<meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1'>

</head>
<body>

<asp:Panel id="Panel_Debug" runat=server>
<!-- only display these next fields if debug enabled -->
<table>
    <tr>
        <td><asp:Label id=Label_Debug runat="server"/></td>
    </tr>
    <tr>
        <td><asp:Label id=Label_DigitalOrder runat="server"/></td>
    </tr>
</table>
</asp:Panel>

<asp:Panel id="Panel_StackTrace" runat="server">
<!-- only display these next fields if an stacktrace output exists-->
    <tr>
        <td colspan="2">&nbsp;</td>
    </tr>
    <tr class="title">
        <td colspan="2"><p><strong>&nbsp;Exception Stack Trace</strong></p></td>
    </tr>
    <tr>
        <td colspan="2"><asp:Label id=Label_StackTrace runat="server"/></td>
    </tr>
</asp:Panel>

<table>
    <tr>
        <td align="right"><strong><i>Message: </i></strong></td>
        <td><asp:Label id=Label_Message runat="server"/></td>
    </tr>
</table>


</body>
</html>
