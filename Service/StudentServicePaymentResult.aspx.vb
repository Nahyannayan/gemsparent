﻿Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data.SqlTypes
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data
Partial Class Service_StudentServicePaymentResult
    Inherits System.Web.UI.Page

    Dim debugData As String = String.Empty

    '
    'Version 3.1
    '
    '---------------- Disclaimer --------------------------------------------------
    '
    'Copyright 2004 Dialect Solutions Holdings.  All rights reserved.
    '
    'This document is provided by Dialect Holdings on the basis that you will treat
    'it as confidential.
    '
    'No part of this document may be reproduced or copied in any form by any means
    'without the written permission of Dialect Holdings.  Unless otherwise
    'expressly agreed in writing, the information contained in this document is
    'subject to change without notice and Dialect Holdings assumes no
    'responsibility for any alteration to, or any error or other deficiency, in
    'this document.
    '
    'All intellectual property rights in the Document and in all extracts and
    'things derived from any part of the Document are owned by Dialect and will be
    'assigned to Dialect on their creation. You will protect all the intellectual
    'property rights relating to the Document in a manner that is equal to the
    'protection you provide your own intellectual property.  You will notify
    'Dialect immediately, and in writing where you become aware of a breach of
    'Dialect's intellectual property rights in relation to the Document.
    '
    'The names "Dialect", "QSI Payments" and all similar words are trademarks of
    'Dialect Holdings and you must not use that name or any similar name.
    '
    'Dialect may at its sole discretion terminate the rights granted in this
    'document with immediate effect by notifying you in writing and you will
    'thereupon return (or destroy and certify that destruction to Dialect) all
    'copies and extracts of the Document in its possession or control.
    '
    'Dialect does not warrant the accuracy or completeness of the Document or its
    'content or its usefulness to you or your merchant customers.   To the extent
    'permitted by law, all conditions and warranties implied by law (whether as to
    'fitness for any particular purpose or otherwise) are excluded.  Where the
    'exclusion is not effective, Dialect limits its liability to $100 or the
    'resupply of the Document (at Dialect's option).
    '
    'Data used in examples and sample data files are intended to be fictional and
    'any resemblance to real persons or companies is entirely coincidental.
    '
    'Dialect does not indemnify you or any third party in relation to the content
    'or any use of the content as contemplated in these terms and conditions.
    '
    'Mention of any product not owned by Dialect does not constitute an endorsement
    'of that product.
    '
    'This document is governed by the laws of New South Wales, Australia and is
    'intended to be legally binding.
    '
    'Author: Dialect Solutions Group Pty Ltd
    '
    '------------------------------------------------------------------------------


    '
    '<summary>ASP.NET C# 3-Party example for the Virtual Payment Client</summary>
    '<remarks>
    '
    '<para>
    'This example assumes that a transaction GET response has been sent to this 
    'example from the Payment Server with the required fields via a cardholder's 
    'browser redirect. The example then checks the value of an MD5 signature to 
    'ensure the data has not been changed during transmission.
    '</para>
    '
    '<para>
    'The to instantiate the MD5 signature check, the MD5 seed must be saved in the 
    'SECURE_SECRET value which is first parameter in the PageLoad() class. The 
    'SECURE_SECRET value can be found in Merchant Administration/Setup page on the 
    'Payment Server.
    '</para>
    '
    '</remarks>
    '


    ' _____________________________________________________________________________

    ' Declare the global variables
    'private string debugData = "";

    ' _____________________________________________________________________________

    Private Function getResponseDescription(ByVal vResponseCode As String) As String
        ' 
        '    <summary>Maps the vpc_TxnResponseCode to a relevant description</summary>
        '    <param name="vResponseCode">The vpc_TxnResponseCode returned by the transaction.</param>
        '    <returns>The corresponding description for the vpc_TxnResponseCode.</returns>
        ' 
        Dim result As String = "Unknown"
        If vResponseCode.Length > 0 Then
            Select Case vResponseCode
                Case "0"
                    result = "Transaction Successful"
                    Exit Select
                Case "1"
                    result = "Transaction Declined"
                    Exit Select
                Case "2"
                    result = "Bank Declined Transaction"
                    Exit Select
                Case "3"
                    result = "No Reply from Bank"
                    Exit Select
                Case "4"
                    result = "Expired Card"
                    Exit Select
                Case "5"
                    result = "Insufficient Funds"
                    Exit Select
                Case "6"
                    result = "Error Communicating with Bank"
                    Exit Select
                Case "7"
                    result = "Payment Server detected an error"
                    Exit Select
                Case "8"
                    result = "Transaction Type Not Supported"
                    Exit Select
                Case "9"
                    result = "Bank declined transaction (Do not contact Bank)"
                    Exit Select
                Case "A"
                    result = "Transaction Aborted"
                    Exit Select
                Case "B"
                    result = "Transaction Declined - Contact the Bank"
                    Exit Select
                Case "C"
                    result = "Transaction Cancelled"
                    Exit Select
                Case "D"
                    result = "Deferred transaction has been received and is awaiting processing"
                    Exit Select
                Case "F"
                    result = "3-D Secure Authentication failed"
                    Exit Select
                Case "I"
                    result = "Card Security Code verification failed"
                    Exit Select
                Case "L"
                    result = "Shopping Transaction Locked (Please try the transaction again later)"
                    Exit Select
                Case "N"
                    result = "Cardholder is not enrolled in Authentication scheme"
                    Exit Select
                Case "P"
                    result = "Transaction has been received by the Payment Adaptor and is being processed"
                    Exit Select
                Case "R"
                    result = "Transaction was not processed - Reached limit of retry attempts allowed"
                    Exit Select
                Case "S"
                    result = "Duplicate SessionID"
                    Exit Select
                Case "T"
                    result = "Address Verification Failed"
                    Exit Select
                Case "U"
                    result = "Card Security Code Failed"
                    Exit Select
                Case "V"
                    result = "Address Verification and Card Security Code Failed"
                    Exit Select
                Case Else
                    result = "Unable to be determined"
                    Exit Select
            End Select
        End If
        Return result
    End Function

    Private Function displayAVSResponse(ByVal vAVSResultCode As String) As String
        '
        '    <summary>Maps the vpc_AVSResultCode to a relevant description</summary>
        '    <param name="vAVSResultCode">The vpc_AVSResultCode returned by the transaction.</param>
        '    <returns>The corresponding description for the vpc_AVSResultCode.</returns>
        '  
        Dim result As String = "Unknown"

        If vAVSResultCode.Length > 0 Then
            If vAVSResultCode.Equals("Unsupported") Then
                result = "AVS not supported or there was no AVS data provided"
            Else
                Select Case vAVSResultCode
                    Case "X"
                        result = "Exact match - address and 9 digit ZIP/postal code"
                        Exit Select
                    Case "Y"
                        result = "Exact match - address and 5 digit ZIP/postal code"
                        Exit Select
                    Case "S"
                        result = "Service not supported or address not verified (international transaction)"
                        Exit Select
                    Case "G"
                        result = "Issuer does not participate in AVS (international transaction)"
                        Exit Select
                    Case "A"
                        result = "Address match only"
                        Exit Select
                    Case "W"
                        result = "9 digit ZIP/postal code matched, Address not Matched"
                        Exit Select
                    Case "Z"
                        result = "5 digit ZIP/postal code matched, Address not Matched"
                        Exit Select
                    Case "R"
                        result = "Issuer system is unavailable"
                        Exit Select
                    Case "U"
                        result = "Address unavailable or not verified"
                        Exit Select
                    Case "E"
                        result = "Address and ZIP/postal code not provided"
                        Exit Select
                    Case "N"
                        result = "Address and ZIP/postal code not matched"
                        Exit Select
                    Case "0"
                        result = "AVS not requested"
                        Exit Select
                    Case Else
                        result = "Unable to be determined"
                        Exit Select
                End Select
            End If
        End If
        Return result
    End Function

    Private Function displayCSCResponse(ByVal vCSCResultCode As String) As String        '
        '    <summary>Maps the vpc_CSCResultCode to a relevant description</summary>
        '    <param name="vCSCResultCode">The vpc_CSCResultCode returned by the transaction.</param>
        '    <returns>The corresponding description for the vpc_CSCResultCode.</returns>
        Dim result As String = "Unknown"
        If vCSCResultCode.Length > 0 Then
            If vCSCResultCode.Equals("Unsupported") Then
                result = "CSC not supported or there was no CSC data provided"
            Else

                Select Case vCSCResultCode
                    Case "M"
                        result = "Exact code match"
                        Exit Select
                    Case "S"
                        result = "Merchant has indicated that CSC is not present on the card (MOTO situation)"
                        Exit Select
                    Case "P"
                        result = "Code not processed"
                        Exit Select
                    Case "U"
                        result = "Card issuer is not registered and/or certified"
                        Exit Select
                    Case "N"
                        result = "Code invalid or not matched"
                        Exit Select
                    Case Else
                        result = "Unable to be determined"
                        Exit Select
                End Select
            End If
        End If
        Return result
    End Function

    '______________________________________________________________________________

    Private Function splitResponse(ByVal rawData As String) As System.Collections.Hashtable
        '
        '    * <summary>This function parses the content of the VPC response
        '    * <para>This function parses the content of the VPC response to extract the
        '    * individual parameter names and values. These names and values are then
        '    * returned as a Hashtable.</para>
        '    *
        '    * <para>The content returned by the VPC is a HTTP POST, so the content will
        '    * be in the format "parameter1=value&parameter2=value&parameter3=value".
        '    * i.e. key/value pairs separated by ampersands "&".</para>
        '    *
        '    * <param name="RawData"> data string containing the raw VPC response content
        '    * <returns> responseData - Hashtable containing the response data
        '    

        Dim responseData As New System.Collections.Hashtable()
        Try
            ' Check if there was a response containing parameters
            If rawData.IndexOf("=") > 0 Then
                ' Extract the key/value pairs for each parameter
                For Each pair As String In rawData.Split("&"c)
                    Dim equalsIndex As Integer = pair.IndexOf("=")
                    If equalsIndex > 1 AndAlso pair.Length > equalsIndex Then
                        Dim paramKey As String = System.Web.HttpUtility.UrlDecode(pair.Substring(0, equalsIndex))
                        Dim paramValue As String = System.Web.HttpUtility.UrlDecode(pair.Substring(equalsIndex + 1))
                        responseData.Add(paramKey, paramValue)
                    End If
                Next
            Else
                ' There were no parameters so create an error
                responseData.Add("vpc_Message", "The data contained in the response was not a valid receipt.<br/>" & vbLf & "The data was: <pre>" & rawData & "</pre><br/>" & vbLf)
            End If
            Return responseData
        Catch ex As Exception
            ' There was an exception so create an error
            responseData.Add("vpc_Message", (vbLf & "The was an exception parsing the response data.<br/>" & vbLf & "The data was: <pre>" & rawData & "</pre><br/>" & vbLf & "<br/>" & vbLf & "Exception: ") + ex.ToString() & "<br/>" & vbLf)
            Return responseData
        End Try
    End Function

    ' _________________________________________________________________________

    Private Function CreateMD5Signature(ByVal RawData As String) As String
        '
        '    <summary>Creates a MD5 Signature</summary>
        '    <param name="RawData">The string used to create the MD5 signautre.</param>
        '    <returns>A string containing the MD5 signature.</returns>
        '    


        Dim hasher As System.Security.Cryptography.MD5 = System.Security.Cryptography.MD5CryptoServiceProvider.Create()
        Dim HashValue As Byte() = hasher.ComputeHash(Encoding.ASCII.GetBytes(RawData))

        Dim strHex As String = ""
        For Each b As Byte In HashValue
            strHex += b.ToString("x2")
        Next
        Return strHex.ToUpper()
    End Function

    Private Function null2unknown(ByVal req As Object) As String
        Try
            If req Is Nothing Then
                Return String.Empty
            Else
                Return req.ToString()
            End If
        Catch ex As Exception
            Return String.Empty
        End Try
    End Function

    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        
        Dim SECURE_SECRET As String = FeeCollectionOnline.GetSECURE_SECRET(Session("sBsuid").ToString())

        Panel_Debug.Visible = False
        Panel_Receipt.Visible = False
        Panel_StackTrace.Visible = False

        Dim message As String = ""
        Dim errorExists As Boolean = False
        Dim txnResponseCode As String = ""
        Dim rawHashData As String = SECURE_SECRET

        Label_HashValidation.Text = "<font color='orange'><b>NOT CALCULATED</b></font>"
        Dim hashValidated As Boolean = True
        Dim MyPaymentGatewayClass As New PaymentGatewayClass(Session("CPS_ID"))
        Try
          
            ' collect debug information
#If DEBUG Then
        debugData += "<br/><u>Start of Debug Data</u><br/><br/>"
#End If

            Dim signature As String = ""
            If Page.Request.QueryString("vpc_SecureHash").Length > 0 Then
                ' collect debug information
#If DEBUG Then
            debugData += "<u>Data from Payment Server</u><br/>"
#End If
                If MyPaymentGatewayClass.CPM_GATEWAY_TYPE = "MIGS" Then
                    rawHashData = ""
                    Dim seperator As String = ""
                    For Each item As String In Page.Request.QueryString
                        If Not item.Equals("vpc_SecureHash") AndAlso Not item.Equals("vpc_SecureHashType") Then
                            If item.StartsWith("vpc_") Or item.StartsWith("user_") Then
                                rawHashData &= seperator & item & "=" & Page.Request.QueryString(item)
                                seperator = "&"
                            End If
                        End If
                    Next
                    If SECURE_SECRET.Length > 0 Then
                        signature = MyPaymentGatewayClass.CreateMIGS_SHA256Signature(rawHashData, SECURE_SECRET)
                    End If
                Else
                    If SECURE_SECRET.Length > 0 Then
                        For Each item As String In Page.Request.QueryString
                            ' collect debug information
#If DEBUG Then
                debugData += (item & "=") + Page.Request.QueryString(item) & "<br/>"
#End If
                            If Not item.Equals("vpc_SecureHash") Then
                                rawHashData += Page.Request.QueryString(item)
                            End If
                        Next
                        signature = CreateMD5Signature(rawHashData)
                    End If
                End If
            End If


            If SECURE_SECRET.Length > 0 Then
                ' Collect debug information
#If DEBUG Then
            debugData += ("<br/><u>Hash Data Input</u>: " & rawHashData & "<br/><br/><u>Signature Created</u>: ") + signature & "<br/>"
#End If

                If Page.Request.QueryString("vpc_SecureHash").Equals(signature) Then
                    Label_HashValidation.Text = "<font color='#00AA00'><b>CORRECT</b></font>"
                Else
                    Label_HashValidation.Text = "<font color='#FF0066'><b>INVALID HASH</b></font>"
                    hashValidated = False
                End If
            End If
            ' Get the standard receipt data from the parsed response
            txnResponseCode = IIf(Page.Request.QueryString("vpc_TxnResponseCode").Length > 0, Page.Request.QueryString("vpc_TxnResponseCode"), "Unknown")
            Label_TxnResponseCode.Text = txnResponseCode
            Label_TxnResponseCodeDesc.Text = getResponseDescription(txnResponseCode)

            'Label_Amount.Text = IIf(Page.Request.QueryString("vpc_Amount").Length > 0, Page.Request.QueryString("vpc_Amount"), "Unknown")
            Label_Command.Text = IIf(Page.Request.QueryString("vpc_Command").Length > 0, Page.Request.QueryString("vpc_Command"), "Unknown")
            Label_Version.Text = IIf(Page.Request.QueryString("vpc_Version").Length > 0, Page.Request.QueryString("vpc_Version"), "Unknown")
            Label_OrderInfo.Text = IIf(Page.Request.QueryString("vpc_OrderInfo").Length > 0, Page.Request.QueryString("vpc_OrderInfo"), "Unknown")
            Label_MerchantID.Text = IIf(Page.Request.QueryString("vpc_Merchant").Length > 0, Page.Request.QueryString("vpc_Merchant"), "Unknown")

            Dim amount As String = null2unknown(Page.Request.QueryString("vpc_Amount"))
            Dim locale As String = null2unknown(Page.Request.QueryString("vpc_Locale"))
            Dim batchNo As String = null2unknown(Page.Request.QueryString("vpc_BatchNo"))
            Dim command As String = null2unknown(Page.Request.QueryString("vpc_Command"))
            Dim version As String = null2unknown(Page.Request.QueryString("vpc_Version"))
            Dim cardType As String = null2unknown(Page.Request.QueryString("vpc_Card"))
            Dim orderInfo As String = null2unknown(Page.Request.QueryString("vpc_OrderInfo"))
            Dim receiptNo As String = null2unknown(Page.Request.QueryString("vpc_ReceiptNo"))
            Dim merchantID As String = null2unknown(Page.Request.QueryString("vpc_Merchant"))
            Dim authorizeID As String = null2unknown(Page.Request.QueryString("vpc_AuthorizeId"))
            Dim merchTxnRef As String = null2unknown(Page.Request.QueryString("vpc_MerchTxnRef"))
            Dim transactionNo As String = null2unknown(Page.Request.QueryString("vpc_TransactionNo"))
            Dim acqResponseCode As String = null2unknown(Page.Request.QueryString("vpc_AcqResponseCode"))
            
            Dim verType As String = null2unknown(Page.Request.QueryString("vpc_VerType"))
            Dim verStatus As String = null2unknown(Page.Request.QueryString("vpc_VerStatus"))
            Dim token As String = null2unknown(Page.Request.QueryString("vpc_VerToken"))
            Dim verSecurLevel As String = null2unknown(Page.Request.QueryString("vpc_VerSecurityLevel"))
            Dim enrolled As String = null2unknown(Page.Request.QueryString("vpc_3DSenrolled"))
            Dim xid As String = null2unknown(Page.Request.QueryString("vpc_3DSXID"))
            Dim acqECI As String = null2unknown(Page.Request.QueryString("vpc_3DSECI"))
            Dim authStatus As String = null2unknown(Page.Request.QueryString("vpc_3DSstatus"))
            Dim recno As String = "", msgServer As String = "", retval As String = ""
            If merchTxnRef = Session("vpc_MerchTxnRef").ToString() Then
                retval = ECAFeeCollectionOnline.F_SaveFEECOLLECTION_H_ONLINE_PAYMENT_FEES(Session("vpc_MerchTxnRef").ToString(), _
                         txnResponseCode, getResponseDescription(txnResponseCode), message, receiptNo, transactionNo, _
                          acqResponseCode, authorizeID, batchNo, cardType, hashValidated.ToString(), amount, _
                          orderInfo, merchantID, command, version, _
                          verType & "|" + verStatus & "|" + token & "|" + verSecurLevel & "|" + enrolled & "|" + xid & "|" + acqECI & "|" + authStatus, _
                          recno, msgServer = "")
                If retval = "0" Then
                    Try
                        Dim objConn As New SqlConnection(ConnectionManger.GetOASIS_SERVICESConnectionString)
                        Dim pParms(3) As SqlClient.SqlParameter
                        objConn.Open()
                        Dim stTrans As SqlTransaction = objConn.BeginTransaction
                        pParms(0) = New SqlClient.SqlParameter("@FCL_RECNO", SqlDbType.VarChar, 30)
                        pParms(0).Value = recno
                        pParms(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
                        pParms(1).Value = Session("sBsuid")
                        pParms(2) = New SqlClient.SqlParameter("@SSR_ID", SqlDbType.VarChar, 1000)
                        pParms(2).Value = Session("SSR_IDs")
                        pParms(3) = New SqlClient.SqlParameter("@ReturnValue", SqlDbType.VarChar)
                        pParms(3).Direction = ParameterDirection.ReturnValue
                        SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "[ONLINE].[UPDATE_SERVICE_REQ_PAYMENT_STATUS]", pParms)
                        If pParms(3).Value = 0 Then
                            retval = pParms(3).Value
                            stTrans.Commit()
                        Else
                            stTrans.Rollback()
                        End If
                    Catch ex As Exception
                        lblError.Text = "System Error !!! ( " & ex.Message & " )"
                    End Try

                End If

            End If
            Try
                ECAFeeCollectionOnline.SAVE_ONLINE_PAYMENT_AUDIT("FEES", "RESPONSE", _
                Page.Request.Url.ToString, Session("vpc_MerchTxnRef").ToString())
            Catch ex As Exception
            End Try
            Session("vpc_MerchTxnRef") = Nothing
            lnkReceipt.Text = recno
            lblError.Text = msgServer
            lblStudentNo.Text = Session("STU_NO").ToString()
            lblStudentName.Text = Session("STU_NAME").ToString()
            Label_MerchTxnRef.Text = merchTxnRef
            Dim Encr_decrData As New Encryption64

            If IsNumeric(amount) Then
                Label_Amount.Text = Format(Convert.ToDecimal(amount) / 100, "0.00")
            Else
                Label_Amount.Text = ""
            End If

            lblDate.Text = Format(Date.Now, "dd/MMM/yyyy")
            ' if message was not provided locally then obtain value from server
            If message.Length = 0 Then
                message = IIf(Page.Request.QueryString("vpc_Message").Length > 0, Page.Request.QueryString("vpc_Message"), "Unknown")
            End If

            If recno.Trim <> "" Then
                h_Recno.Value = Encr_decrData.Encrypt(recno.Trim)
                hf_encr_bsuId.Value = Encr_decrData.Encrypt(Session("sBsuid"))
                Dim PopForm As String = "StudentServiceFeeReceipt.aspx"
                lnkReceipt.Attributes.Add("onClick", "return ShowSubWindowWithClose('" & PopForm & "?type=REC&BsuId=" & hf_encr_bsuId.Value & "&id=" & h_Recno.Value & "', '', '80%', '80%');")
                lblPrintMsg.Visible = True
                Dim str_sendmailurl As String = Page.Request.Url.ToString
                str_sendmailurl = str_sendmailurl.Split("?"c)(0) & "?type=REC&id=" & h_Recno.Value & "&bsu_id=" & Encr_decrData.Encrypt(Session("sBsuid")) & "&user=" & Encr_decrData.Encrypt(Session("SrvUsrName"))
                str_sendmailurl = str_sendmailurl.Replace("StudentServicePaymentResult.aspx", "StudentServiceFeeReceiptEMail.aspx")
                If retval = 0 Then
                    frame1.Attributes("src") = str_sendmailurl
                End If

            End If
        Catch ex As Exception
            message = "(51) Exception encountered. " & ex.Message
            UtilityObj.Errorlog(ex.Message)
            UtilityObj.Errorlog(ex.ToString())
            If ex.StackTrace.Length > 0 Then
                Label_StackTrace.Text = ex.ToString()
                Panel_StackTrace.Visible = True
            End If
            errorExists = True
        End Try
        Label_Message.Text = message

        ' Create a link to the example's HTML order page
        Label_AgainLink.Text = "<a href=""" & Page.Request.QueryString("AgainLink") & """>Another Transaction</a>"

        ' Determine the appropriate title for the receipt page
        Label_Title.Text = IIf((errorExists OrElse txnResponseCode.Equals("7") OrElse txnResponseCode.Equals("Unknown") OrElse hashValidated = False), Page.Request.QueryString("Title") & " Error Page", Page.Request.QueryString("Title") & " Receipt Page")

        ' output debug data to the screen
#If DEBUG Then
    debugData += "<br/><u>End of debug information</u><br/>"
    Label_Debug.Text = debugData
#End If

        Panel_Debug.Visible = True
    End Sub


    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect("ViewStudentServices.aspx")
    End Sub

End Class
