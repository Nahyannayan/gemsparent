﻿Imports System.IO
Partial Class Service_HifzFileDownloads
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
        Dim hifzFileName As String = String.Empty
        Try
            If Not String.IsNullOrEmpty(Session("HifzFileName").ToString()) Then
                hifzFileName = Session("HifzFileName").ToString()
                Session("HifzFileName") = ""
            End If

            If hifzFileName = "" Then Return

            Dim folderPath As String = Convert.ToString(ConfigurationManager.AppSettings("HifzTrackerAudioFolderLocalPath"))
            Dim fullFileLocalPath As String = Path.Combine(folderPath, hifzFileName)

            Dim folderVirtualPath As String = Convert.ToString(ConfigurationManager.AppSettings("HifzTrackerAudioFolderVirtualPath"))
            Dim fullFileVirtualPath As String = Path.Combine(folderPath, hifzFileName)

            Response.Clear()
            Response.ClearContent()
            Response.ClearHeaders()
            Response.BufferOutput = True
            Dim fileName As String = hifzFileName


            If File.Exists(fullFileLocalPath) Then
                Response.AddHeader("Content-Disposition", "attachment; filename=" & fileName)
                Response.ContentType = "application/octet-stream"
                Response.WriteFile(fullFileVirtualPath)
                Response.Flush()
                HttpContext.Current.ApplicationInstance.CompleteRequest()
            Else
                If Request.UrlReferrer IsNot Nothing Then
                    Dim csType As Type = [GetType]()
                    Dim jsScript As String = "alert('File Not Found');"
                    ScriptManager.RegisterClientScriptBlock(Page, csType, "popup", jsScript, True)
                End If
            End If

        Catch ex As Exception
            Dim errorMsg As String = ex.Message
        End Try
    End Sub

End Class
