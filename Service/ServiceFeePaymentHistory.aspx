﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ParentMaster.master" AutoEventWireup="false"
    CodeFile="ServiceFeePaymentHistory.aspx.vb" Inherits="Fees_ServiceFeePaymentHistory" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphParent" runat="Server">

    <script src="../Scripts/jquery-1.9.1.js" type="text/javascript"></script>

    <script type="text/javascript" language="javascript">
        function CheckForPrint(id,BsuId) {
            if (id != '') {
                var frmReceipt;
                frmReceipt = "StudentServiceFeeReceipt.aspx";
                //showModelessDialog('feereceipt.aspx?type=REC&id=' + id, '', "dialogWidth: 800px; dialogHeight: 700px; help: no; resizable: no; scroll: yes; status: no; unadorned: no;");
                window.open(frmReceipt + '?type=REC&BsuId=' + BsuId + '&id=' + id, "_blank", "dialogWidth: 800px; dialogHeight: 700px; help: no; resizable: no; scroll: yes; status: no; unadorned: no;");
                return false;
            }
            // else
            //alert('Sorry. There is some problem with the transaction');
        }              
    </script>

   
                <!-- Posts Block -->
    <div class="content margin-top30 margin-bottom60">
        <div class="container">
            <div class="row">

                <!-- Posts Block -->
                <div class="posts-block col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="my-account">
                     
           
                        <div>
                             <asp:Label ID="lblerror" runat="server" CssClass="error"></asp:Label>
    <asp:HiddenField ID="hfTaxable" runat="server" Value="0" />
                               <div class="mainheading">
                            <div >Payment History
            <%= IIf(Session("BSU_CURRENCY") Is Nothing, "", "(Amount In " & Session("BSU_CURRENCY") & ")")%></div>
                                      
                           
                        </div>
                            <div align="left">
    <table class="table table-striped table-bordered table-responsive text-left my-orders-table" width="100%">
        <tr class="matters">
            <td>
                From Date
            </td>
            <td>
                <asp:TextBox ID="txtFrom" runat="server" CssClass="form-control" >
                </asp:TextBox>
            </td>
            <td align="center">
                To Date
            </td>
            <td align="center">
                <asp:TextBox ID="txtTo" runat="server" CssClass="form-control" >
                </asp:TextBox>
            </td>
            <td style="width: 100px;">
                <asp:Button ID="btnsearch" runat="server" Text="Show History" ValidationGroup="s"
                    CssClass="btn btn-info"/>
            </td>
        </tr>
        <tr class="matters">
            <td colspan="5">
                <asp:GridView ID="gvFeeCollection" runat="server" AutoGenerateColumns="False" SkinID="GridViewNormal"
                    Width="99%" CssClass="table table-striped table-bordered table-responsive text-left my-orders-table">
                    <columns>
                        <asp:TemplateField HeaderText="Rec.No.">
                            <ItemTemplate>
                                <asp:LinkButton ID="lbPrint" CssClass="MenuLink" runat="server" OnClientClick="<%# &quot;javascript:CheckForPrint('&quot;& Receiptencrypt(Container.DataItem(&quot;FCL_RECNO&quot;))&&quot;','&quot;& Receiptencrypt(Container.DataItem(&quot;FCL_BSU_ID&quot;))&&quot;');return false;&quot; %>"
                                    Text='<%# Bind("FCL_RECNO") %>'></asp:LinkButton>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:BoundField DataField="FCL_DATE" HeaderText="Receipt Date" DataFormatString="{0:dd/MMM/yyyy}"
                            HtmlEncode="False">
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="FCL_NARRATION" HeaderText="Description">
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField DataField="FCL_ID" HeaderText="Ref. No.">
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField DataField="FCL_AMOUNT" DataFormatString="{0:0.00}" HeaderText="Amount">
                            <ItemStyle HorizontalAlign="Right" />
                            <HeaderStyle HorizontalAlign="Right" />
                        </asp:BoundField>
                        <asp:BoundField DataField="FCL_STATUS" HeaderText="Status">
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                    </columns>
                </asp:GridView>
            </td>
        </tr>
    </table>
    <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtTo"
        Format="dd/MMM/yyyy">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtFrom"
        Format="dd/MMM/yyyy">
    </ajaxToolkit:CalendarExtender>

    </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
