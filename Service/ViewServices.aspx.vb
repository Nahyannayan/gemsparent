﻿Imports System.Data
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports Telerik.Web.UI

Imports System
Imports System.Data.SqlClient
Imports System.IO
Imports System.Web.Mail

Partial Class ParentLogin_Service_ViewServices
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If Session("username") Is Nothing Then
                Response.Redirect("~\General\Home.aspx")
            End If
            'Session("OLU_ID") = "47521" '"243197"
            ViewState("SSV_ID") = "0"
            ViewState("SSR_ID") = "0"
            ViewState("SVC_ID") = "0"
            BindStudentServices()
            'BindPendingApprovalStudentServices()
        End If
    End Sub
    Public Sub BindStudentServices()
        Dim con As String = ConnectionManger.GetOASISConnectionString
        Dim param(4) As SqlClient.SqlParameter
        param(0) = New SqlClient.SqlParameter("@STU_ID", Session("STU_ID"))
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "[ONLINE].[SERVICES_APPROVED_STUDENT_SVC]", param)
        gvStudentServicesDetails.DataSource = ds.Tables(0)
        gvStudentServicesDetails.DataBind()

    End Sub
    Public Sub BindPendingApprovalStudentServices()
        Dim con As String = ConnectionManger.GetOASISConnectionString
        Dim param(4) As SqlClient.SqlParameter
        param(0) = New SqlClient.SqlParameter("@STU_ID", Session("STU_ID"))
        param(1) = New SqlClient.SqlParameter("@OPTION", 1)
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "[ONLINE].[SERVICES_GETSTUDENT_REQUESTED_SVC]", param)
        gvPendingAppSvc.DataSource = ds.Tables(0)
        gvPendingAppSvc.DataBind()

    End Sub

    Protected Sub lnk_Discontinue_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        pnlDiscontinue.Visible = True

        Dim lblSERVICE As Label = sender.parent.findcontrol("lblSERVICE")
        Dim lblStartDate As Label = sender.parent.findcontrol("lblStartDate")
        Dim hf_SSV_ID As HiddenField = sender.parent.findcontrol("hf_SSV_ID")
        Dim HF_Stu_Name As HiddenField = sender.parent.findcontrol("HF_Stu_Name")
        Dim HF_SSR_ID As HiddenField = sender.parent.findcontrol("HF_SSR_ID")
        Dim HF_SVC_ID As HiddenField = sender.parent.findcontrol("HF_SVC_ID")

        ViewState("SSR_ID") = HF_SSR_ID.Value
        ViewState("SVC_ID") = HF_SVC_ID.Value
        ViewState("SSV_ID") = hf_SSV_ID.Value

        lblsvc.Text = lblSERVICE.Text
        lblStu_name_D.Text = HF_Stu_Name.Value
        lblfromdate_D.Text = lblStartDate.Text
        Dim hf_STU_ID As HiddenField = sender.parent.findcontrol("hf_STU_ID")


        'CompareValidator2.ValueToCompare = Convert.ToDateTime(lblStartDate.Text).Date

    End Sub
    Protected Sub lnk_DeleteReq_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim HF_SSR_ID As HiddenField = sender.parent.findcontrol("HF_SSR_ID")
        Dim constring As String = ConnectionManger.GetOASISConnectionString
        Dim con As SqlConnection = New SqlConnection(constring)
        con.Open()
        Dim sqltran As SqlTransaction
        sqltran = con.BeginTransaction("trans")
        Try
            Dim param(10) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@SSR_ID", HF_SSR_ID.Value)
            param(1) = New SqlClient.SqlParameter("@MSG", SqlDbType.VarChar, 1000)
            param(1).Direction = ParameterDirection.Output
            SqlHelper.ExecuteNonQuery(sqltran, CommandType.StoredProcedure, "[ONLINE].[SERVICES_DELETE_REQUEST]", param)
            lblerror.Text = param(1).Value
            sqltran.Commit()
            BindStudentServices()
        Catch ex As Exception
            lbldiserror.Text = ex.Message
            sqltran.Rollback()
        End Try
    End Sub
    Protected Sub lnk_paynow_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub
    Protected Sub btnCancelResetPassword_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelResetPassword.Click
        txtfromdate.Text = ""
        lblStu_name_D.Text = ""
        lblfromdate_D.Text = ""
        ViewState("SSV_ID") = "0"
        ViewState("SSR_ID") = "0"
        ViewState("SVC_ID") = "0"
        txtRemarks.Text = ""
        pnlDiscontinue.Visible = False
    End Sub

    Protected Sub lbtnApplicantNotesClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbtnApplicantNotesClose.Click
        txtfromdate.Text = ""
        lblStu_name_D.Text = ""
        lblfromdate_D.Text = ""
        ViewState("SSV_ID") = "0"
        ViewState("SSR_ID") = "0"
        ViewState("SVC_ID") = "0"
        txtRemarks.Text = ""
        pnlDiscontinue.Visible = False
    End Sub

    Protected Sub btnSaveDisRequest_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveDisRequest.Click

        ' If (Convert.ToDateTime(txtfromdate.Text).Date >= Date.Now.Date) Then
        If (Convert.ToDateTime(txtfromdate.Text).Date >= (Convert.ToDateTime(lblfromdate_D.Text).Date)) Then
            Dim constring As String = ConnectionManger.GetOASISConnectionString
            Dim con As SqlConnection = New SqlConnection(constring)
            con.Open()
            Dim sqltran As SqlTransaction
            sqltran = con.BeginTransaction("trans")
            If (ViewState("SSV_ID") <> "0") Then
                Try
                    Dim param(10) As SqlClient.SqlParameter
                    param(0) = New SqlClient.SqlParameter("@SSV_ID", ViewState("SSV_ID"))
                    param(1) = New SqlClient.SqlParameter("@SSD_FROM", txtfromdate.Text)
                    param(2) = New SqlClient.SqlParameter("@SSD_REQ_USR", Session("username"))
                    param(3) = New SqlClient.SqlParameter("@MSG", SqlDbType.VarChar, 1000)
                    param(3).Direction = ParameterDirection.Output
                    param(4) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                    param(4).Direction = ParameterDirection.ReturnValue
                    param(5) = New SqlClient.SqlParameter("@SSD_REMARKS", txtRemarks.Text)

                    SqlHelper.ExecuteNonQuery(sqltran, CommandType.StoredProcedure, "[ONLINE].[SERVICES_DISCONTINUE_REQUEST]", param)
                    lblerror.Text = param(3).Value ' "Service discontinue request submitted sucessfully"

                    Dim ReturnFlag As Integer = param(4).Value
                    If ReturnFlag = 600 Then
                        If ViewState("SVC_ID") <> "35" Then
                            callSend_mail(Session("STU_ID"), ViewState("SSR_ID"), ViewState("SVC_ID"), sqltran, 3)
                        End If

                        sqltran.Commit()
                        lblerror.Text = param(3).Value
                        txtfromdate.Text = ""
                        lblStu_name_D.Text = ""
                        lblfromdate_D.Text = ""
                        ViewState("SSV_ID") = "0"
                        ViewState("SSR_ID") = "0"
                        ViewState("SVC_ID") = "0"
                        pnlDiscontinue.Visible = False
                    Else
                        lbldiserror.Text = param(3).Value
                        sqltran.Rollback()
                    End If
                    BindStudentServices()
                Catch ex As Exception
                    lbldiserror.Text = ex.Message
                    sqltran.Rollback()
                End Try
            End If
        Else
            lbldiserror.Text = "Selected date should be greater than or equal to service start date"
        End If
        'Else
        'lbldiserror.Text = "Selected date should be greater than or equal today's date"
        'End If

    End Sub

    Protected Sub gvStudentServicesDetails_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvStudentServicesDetails.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim HF_DISCONTINUED As HiddenField = DirectCast(e.Row.FindControl("HF_DISCONTINUED"), HiddenField)

                Dim HF_SSR_bPaid As HiddenField = DirectCast(e.Row.FindControl("HF_SSR_bPaid"), HiddenField)

                Dim hf_SSV_ID As HiddenField = DirectCast(e.Row.FindControl("hf_SSV_ID"), HiddenField)

                Dim lnk_paynow As LinkButton = DirectCast(e.Row.FindControl("lnk_paynow"), LinkButton)
                Dim lnk_Discontinue As LinkButton = DirectCast(e.Row.FindControl("lnk_Discontinue"), LinkButton)
                Dim lnk_DeleteReq As LinkButton = DirectCast(e.Row.FindControl("lnk_DeleteReq"), LinkButton)

                Dim img_Discontinue As Image = DirectCast(e.Row.FindControl("img_Discontinue"), Image)

                lnk_paynow.Enabled = (IIf(HF_SSR_bPaid.Value = "True", False, True))


                'If hf_SSV_ID.Value = "0" Then
                '    lnk_DeleteReq.Visible = True
                '    lnk_Discontinue.Visible = False
                'Else
                '    lnk_DeleteReq.Visible = False
                '    lnk_Discontinue.Visible = True
                'End If

                Dim HF_SSR_ID As HiddenField = DirectCast(e.Row.FindControl("HF_SSR_ID"), HiddenField)
                Dim HF_link As HiddenField = DirectCast(e.Row.FindControl("HF_link"), HiddenField)
                Dim link As String = Convert.ToString(HF_link.Value)
                link = link.Replace("@SSR_ID", Encr_decrData.Encrypt(HF_SSR_ID.Value))
                lnk_paynow.PostBackUrl = link

                If (HF_DISCONTINUED.Value = "0" And hf_SSV_ID.Value <> "0") Then
                    Dim hf_bapproved As HiddenField = DirectCast(e.Row.FindControl("hf_bapproved"), HiddenField)
                    lnk_Discontinue.Visible = True
                    img_Discontinue.Visible = False
                ElseIf (hf_SSV_ID.Value = "0") Then
                    lnk_DeleteReq.Visible = True
                    lnk_Discontinue.Visible = False
                Else
                    lnk_Discontinue.Visible = False
                    img_Discontinue.Visible = True
                End If

               
            End If
        Catch ex As Exception

        End Try

    End Sub
    Sub callSend_mail(ByVal STU_ID As String, ByVal SSR_ID As String, ByVal SVC_ID As String, ByVal sqltran As SqlTransaction, ByVal status As String)

        Dim htmlString As String = String.Empty
        Dim html As String = ScreenScrapeHtml(Server.MapPath("EmailContent\emailTemplate.htm"))
        Dim msg As New MailMessage

        Dim BSC_HOST As String = String.Empty
        Dim BSC_USERNAME As String = String.Empty
        Dim BSC_PASSWORD As String = String.Empty
        Dim SUBJECT As String = String.Empty
        Dim EMAIL_ADD As String = String.Empty
        Dim BSU_ID As String = String.Empty
        Dim ACD_ID As String = String.Empty


        Dim STUDENTNAME As String = String.Empty
        Dim STUDENTNO As String = String.Empty
        Dim SERVICE As String = String.Empty
        Dim BSU_NAME As String = String.Empty
        Dim SERVICE_CAT As String = String.Empty
        'html = html.Replace("@Content", htmlString)

        Dim pParms(4) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@STU_ID", STU_ID)
        pParms(2) = New SqlClient.SqlParameter("@SSR_ID", SSR_ID)
        pParms(1) = New SqlClient.SqlParameter("@OPTION", status)

        Using EmailHost_reader As SqlDataReader = SqlHelper.ExecuteReader(sqltran, CommandType.StoredProcedure, "[ONLINE].[SERVICES_EMAIL_TEXT]", pParms)
            If EmailHost_reader.HasRows Then
                While EmailHost_reader.Read
                    SUBJECT = Convert.ToString(EmailHost_reader("SUBJECT"))
                    SUBJECT = SUBJECT.Replace("@servicecategory", Convert.ToString(EmailHost_reader("SERVICE_CAT")))
                    SUBJECT = SUBJECT.Replace("@service", Convert.ToString(EmailHost_reader("SERVICE")))


                    htmlString = Convert.ToString(EmailHost_reader("EMAIL_MESSAGE"))
                    html = html.Replace("@Content", htmlString)
                    BSU_ID = Convert.ToString(EmailHost_reader("BSU_ID"))
                    html = html.Replace("@school", Convert.ToString(EmailHost_reader("BSU_NAME")))
                    html = html.Replace("@service", Convert.ToString(EmailHost_reader("SERVICE")))
                    html = html.Replace("@name ", Convert.ToString(EmailHost_reader("STUDENTNAME")))
                    html = html.Replace("@studentno ", Convert.ToString(EmailHost_reader("STUDENTNO")))


                    SERVICE_CAT = Convert.ToString(EmailHost_reader("SERVICE_CAT"))
                    STUDENTNO = Convert.ToString(EmailHost_reader("STUDENTNO"))
                    STUDENTNAME = Convert.ToString(EmailHost_reader("STUDENTNAME"))
                    SERVICE = Convert.ToString(EmailHost_reader("SERVICE"))
                    BSU_NAME = Convert.ToString(EmailHost_reader("BSU_NAME"))

                    EMAIL_ADD = Convert.ToString(EmailHost_reader("EMAIL_ID"))
                    ACD_ID = Convert.ToString(EmailHost_reader("ACD_ID"))
                End While
            End If
        End Using

        Dim param(9) As SqlClient.SqlParameter
        param(0) = New SqlClient.SqlParameter("@EML_BSU_ID", BSU_ID)
        param(1) = New SqlClient.SqlParameter("@EML_TYPE", "SERVICE-D")
        param(2) = New SqlClient.SqlParameter("@EML_PROFILE_ID", SSR_ID)
        param(3) = New SqlClient.SqlParameter("@EML_TOEMAIL", EMAIL_ADD) 'EMAIL_ADD)
        param(4) = New SqlClient.SqlParameter("@EML_SUBJECT", SUBJECT)
        param(5) = New SqlClient.SqlParameter("@EML_MESSAGE", html)
        SqlHelper.ExecuteNonQuery(sqltran, CommandType.StoredProcedure, "dbo.InsertIntoEmailSendSchedule", param)



        html = ScreenScrapeHtml(Server.MapPath("EmailContent\emailTemplate.htm"))
        Dim param1(9) As SqlClient.SqlParameter
        param1(0) = New SqlClient.SqlParameter("@SBA_BSU_ID", BSU_ID)
        param1(1) = New SqlClient.SqlParameter("@SBA_ACD_ID", ACD_ID)
        param1(2) = New SqlClient.SqlParameter("@SBA_SVC_ID", SVC_ID)
        param1(3) = New SqlClient.SqlParameter("@OPTION", status)
        Dim staI As Integer = 0
        EMAIL_ADD = ""
        Using EmailHost_reader As SqlDataReader = SqlHelper.ExecuteReader(sqltran, CommandType.StoredProcedure, "[ONLINE].[SERVICES_APPROVES_EMAIL]", param1)
            If EmailHost_reader.HasRows Then
                While EmailHost_reader.Read
                    SUBJECT = Convert.ToString(EmailHost_reader("SUBJECT"))
                    SUBJECT = SUBJECT.Replace("@servicecategory", SERVICE_CAT)
                    SUBJECT = SUBJECT.Replace("@service", SERVICE)

                    htmlString = Convert.ToString(EmailHost_reader("EMAIL_MESSAGE"))
                    html = html.Replace("@Content", htmlString)
                    html = html.Replace("@school", BSU_NAME)
                    html = html.Replace("@service", SERVICE)
                    html = html.Replace("@name", STUDENTNAME)
                    html = html.Replace("@studentno", STUDENTNO)
                    EMAIL_ADD = Convert.ToString(EmailHost_reader("EMD_EMAIL")) + ";" + EMAIL_ADD
                    staI = staI + 1
                End While
            End If
        End Using

        If (staI > 0) Then
            Dim EMAIL_ADD_TO As String() = New String(9) {}
            EMAIL_ADD_TO = EMAIL_ADD.Split(";")
            For Each toEmail As String In EMAIL_ADD_TO
                If (toEmail <> "") Then
                    param(0) = New SqlClient.SqlParameter("@EML_BSU_ID", BSU_ID)
                    param(1) = New SqlClient.SqlParameter("@EML_TYPE", IIf(status = "1", "SERVICE-A", "SERVICE-R"))
                    param(2) = New SqlClient.SqlParameter("@EML_PROFILE_ID", SSR_ID)
                    param(3) = New SqlClient.SqlParameter("@EML_TOEMAIL", toEmail) 'EMAIL_ADD)
                    param(4) = New SqlClient.SqlParameter("@EML_SUBJECT", SUBJECT)
                    param(5) = New SqlClient.SqlParameter("@EML_MESSAGE", html)
                    SqlHelper.ExecuteNonQuery(sqltran, CommandType.StoredProcedure, "dbo.InsertIntoEmailSendSchedule", param)
                End If
            Next
        End If
    End Sub
    Public Shared Function ScreenScrapeHtml(ByVal url As String) As String
        Dim objRequest As System.Net.WebRequest = System.Net.HttpWebRequest.Create(url)
        Dim sr As New StreamReader(objRequest.GetResponse().GetResponseStream())
        Dim result As String = sr.ReadToEnd()
        sr.Close()
        Return result
    End Function




End Class
