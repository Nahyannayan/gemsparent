﻿Imports System.Data
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports Telerik.Web.UI

Imports System
Imports System.Data.SqlClient
Imports System.Web.Mail
Imports System.IO

Partial Class ParentLogin_Service_ServiceRequestMA
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim dtActivity As New DataTable
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("username") Is Nothing Then
            Response.Redirect("~\General\Home.aspx")
        End If
        'CompareValidator2.ValueToCompare = DateTime.Now.Date
        If Not IsPostBack Then
            trOwnIns.Visible = False
            tr_CurrentTeacher.Visible = False
            ViewState("SVCategory_Oldvalue") = "0"


            ' Session("OLU_ID") = "47521" '"243197"
            ' ''BindStudentServices()
            ' ''BindStudents()
            ' ''BindServiceCategory()

            ' ''AcademicYear()
            ' ''BindServices()
            ' ''bindTerms()
            ' ''BindStudentServices()
            ' ''hidingtables()
            ' ''enableButton()
            ' ''ViewState("SVC_Value") = ddlSVCategory.SelectedValue

        

            Me.BindGmaAcademicYear()
            Me.GetStudentAndSiblingList(Session("stu_id"))

            'Build dtActivity datatable and add it to current session
            If Not Me.IsPostBack Then
                dtActivity.Columns.Add("Category", GetType(String))
                dtActivity.Columns.Add("CategoryId", GetType(Long))
                dtActivity.Columns.Add("SubCategory", GetType(String))
                dtActivity.Columns.Add("Duration", GetType(String))
                dtActivity.Columns.Add("TermIds", GetType(String))
                dtActivity.Columns.Add("SVGID", GetType(Long))
                dtActivity.Columns.Add("Remarks", GetType(String))
                Session.Add("ECARequest_dtActivity", dtActivity)
                'Me.btn_SaveRequest.Attributes.Add("onclick", "alert('" & chkAccepted.ClientID & "');")
                Me.chkApproval.Checked = True
                Me.chkApproval.Enabled = False
            End If
            If Request.QueryString("ssr_id") IsNot Nothing AndAlso Request.QueryString("ssr_id").ToString <> "" Then
                ViewState("ssr_id") = Encr_decrData.Decrypt(Request.QueryString("ssr_id").Replace(" ", "+"))
                BindTermBy_BSU_ACD()
                GetServiceRequestById(Convert.ToInt32(ViewState("ssr_id")))

            Else
                ViewState("ssr_id") = 0
            End If
        End If

    End Sub

    Private Function GetStudentAndSiblingList(ByVal StuId As Long) As Boolean
        GetStudentAndSiblingList = False
        Dim dr As SqlDataReader, Item As ListItem
        Try
            Dim CONN As String = ConnectionManger.GetOASISConnectionString
            dr = SqlHelper.ExecuteReader(CONN, CommandType.Text, "SELECT stu_id, ISNULL(STU_FIRSTNAME,'') + ' ' + ISNULL(STU_MIDNAME,'') + ' ' + ISNULL(STU_LASTNAME,'') AS stu_name  FROM dbo.STUDENT_M WHERE STU_ID = " & StuId & " UNION SELECT stu_id, ISNULL(STU_FIRSTNAME,'') + ' ' + ISNULL(STU_MIDNAME,'') + ' ' + ISNULL(STU_LASTNAME,'') AS stu_name  FROM dbo.STUDENT_M WHERE STU_ID = (SELECT STU_SIBLING_ID FROM dbo.STUDENT_M WHERE STU_ID = " & StuId & ")")
            If dr.HasRows Then
                Do While dr.Read
                    Item = New ListItem(dr.Item("stu_name"), dr.Item("stu_id"))
                    Me.ddlStudent.Items.Add(Item)
                Loop
            End If
            dr.Close()
            Me.ddlStudent.SelectedIndex = 0
            If Me.ddlStudent.Items.Count <= 1 Then
                '''Me.lblStudentName.Text = Me.ddlStudent.SelectedItem.Text
                Me.GetStudentDetails(Me.ddlStudent.SelectedValue)
            End If
            If ddlStudent.Items.Count > 1 Then
                ddlStudent.Enabled = True
            End If
        Catch ex As Exception
            'Me.trMessage.Visible = True
            Me.lblerror.Text = "There was some problem processing your request. Please try again."
        End Try
    End Function

    Private Function GetServiceRequestById(ByVal SSR_ID As Integer) As Boolean
        Dim connStr As String = ConnectionManger.GetOASIS_SERVICESConnectionString
        Dim Dt As DataTable = New DataTable
        Dim ds As DataSet
        Dim PARAM(5) As SqlParameter

        PARAM(0) = New SqlParameter("@SSR_ID", SSR_ID)

        Dim SP As String = "ST.GET_SERVICE_REQUEST_BY_ID"
        ds = SqlHelper.ExecuteDataset(connStr, CommandType.StoredProcedure, SP, PARAM)
        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            Dt = ds.Tables(0)

            If Dt.Rows.Count > 0 Then
                ddlGMAcademicYear.ClearSelection()
                ddlGMASVC.ClearSelection()
                ddlGMAcademicYear.Items.FindByValue(Dt.Rows("0")("SSR_ACD_ID").ToString).Selected = True
                ddlGMASVC.Items.FindByValue(Dt.Rows("0")("SSR_SVC_ID").ToString).Selected = True

                If Dt.Rows("0")("SSR_STUDENT_LEVEL").ToString <> "" Then
                    ddlLevel.ClearSelection()
                    ddlLevel.Items.FindByValue(Dt.Rows("0")("SSR_STUDENT_LEVEL").ToString).Selected = True
                End If
                ''chkPeriods2.ClearSelection()

                If Dt.Rows("0")("SSR_STM_IDS").ToString <> "" Then
                    Dim TermIDs As String() = Dt.Rows("0")("SSR_STM_IDS").ToString.Split("|")
                    BindTermBy_BSU_ACD_By_TermID(TermIDs, Dt.Rows("0")("SSR_SVC_ID").ToString)
                End If
            End If
        End If
    End Function

    Private Sub getGMASVC_color()
        Dim dts As DataSet

        dts = GetStudentServiceInfo(Session("stu_id"))

        For Each myItem As ListItem In ddlGMASVC.Items
            'Do some things to determine the color of the item
            'Set the item background-color like so:

            Dim myItemVal As String = String.Empty
            myItemVal = myItem.Value


            If CheckValue(dts.Tables(0), "SSR_SVC_ID", myItemVal) Then
                If CheckValue(dts.Tables(0), "SSR_APPR_FLAG", myItemVal) Then
                    myItem.Attributes.Add("style", "color:red")
                Else
                    myItem.Attributes.Add("style", "color:green")
                End If

            End If

        Next
    End Sub


    Protected Sub ddlGMASVC_DataBound(ByVal sender As Object, ByVal e As EventArgs) Handles ddlGMASVC.DataBound

        Dim dts As DataSet

        dts = GetStudentServiceInfo(Session("stu_id"))

        For Each myItem As ListItem In ddlGMASVC.Items
            'Do some things to determine the color of the item
            'Set the item background-color like so:

            Dim myItemVal As String = String.Empty
            myItemVal = myItem.Value


            If CheckValue(dts.Tables(0), "SSR_SVC_ID", myItemVal) Then
                If CheckValue(dts.Tables(0), "SSR_APPR_FLAG", myItemVal) Then
                    myItem.Attributes.Add("style", "color:red")
                Else
                    myItem.Attributes.Add("style", "color:green")
                End If

            End If

        Next
    End Sub

    Shared Function CheckValue(ByVal myTable As DataTable, ByVal columnName As String, ByVal searchValue As String) As Boolean
        For Each row As DataRow In myTable.Rows
            If row(columnName) = searchValue Then Return True
        Next
        Return False
    End Function

    Private Function GetStudentServiceInfo(ByVal STU_ID As String) As DataSet
        Dim connStr As String = ConnectionManger.GetOASIS_SERVICESConnectionString
        Dim Dt As DataTable = New DataTable
        Dim ds As DataSet
        Dim PARAM(5) As SqlParameter

        PARAM(0) = New SqlParameter("@ACD_ID", ddlGMAcademicYear.SelectedValue)
        PARAM(1) = New SqlParameter("@sBSU_ID", 0)
        PARAM(2) = New SqlParameter("@STU_ID", STU_ID)


        Dim SP As String = "Get_Student Service_List"
        ds = SqlHelper.ExecuteDataset(connStr, CommandType.StoredProcedure, SP, PARAM)
        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            Dt = ds.Tables(0)
        End If
        Return ds

    End Function
    '

    Private Function getParentEmailAddress() As String
        Dim connStr As String = ConnectionManger.GetOASIS_SERVICESConnectionString

        Dim email As String = String.Empty

        Dim PARAM(1) As SqlParameter

        PARAM(0) = New SqlParameter("@STU_ID", Session("STU_ID"))
        PARAM(1) = New SqlClient.SqlParameter("@EMAIL_ID", SqlDbType.VarChar, 50)
        PARAM(1).Direction = ParameterDirection.Output

        Dim SP As String = "Get_Parent_Information_by_STU_ID"
        Dim Re = SqlHelper.ExecuteScalar(connStr, CommandType.StoredProcedure, SP, PARAM)
        email = PARAM(1).Value.ToString

        Return email

    End Function

    Private Function IsServiceAlreadyRequested(ByVal STU_ID As String, ByVal SVC_ID As String, ByVal ACD_ID As String) As Boolean
        Dim connStr As String = ConnectionManger.GetOASIS_SERVICESConnectionString
        Dim Dt As DataTable = New DataTable
        Dim ds As DataSet
        Dim status As Boolean = False

        Dim PARAM(5) As SqlParameter

        PARAM(0) = New SqlParameter("@ACD_ID", ddlGMAcademicYear.SelectedValue)
        PARAM(1) = New SqlParameter("@sBSU_ID", 0)
        PARAM(2) = New SqlParameter("@STU_ID", STU_ID)
        PARAM(3) = New SqlParameter("@SVC_ID", SVC_ID)

        Dim SP As String = "Get_Student_Service_List_by_SVC"
        ds = SqlHelper.ExecuteDataset(connStr, CommandType.StoredProcedure, SP, PARAM)
        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            Dt = ds.Tables(0)

            If Dt.Rows.Count > 0 Then
                status = True
            End If
        End If
        Return status

    End Function

    Private Function ServiceMessageToDisplay() As String
        Dim connStr As String = ConnectionManger.GetOASIS_SERVICESConnectionString

        Dim message As String = String.Empty





        Dim SP As String = "Get_Student_Sevice_DisplayMessage "
        message = SqlHelper.ExecuteScalar(connStr, CommandType.StoredProcedure, SP)

        Return message

    End Function



    Private Function GetStudentDetails(ByVal StuId As Long) As Boolean
        GetStudentDetails = False
        Dim dr As IDataReader, Item As ListItem
        Try
            Dim CONN As String = ConnectionManger.GetOASISConnectionString
            dr = SqlHelper.ExecuteReader(CONN, CommandType.Text, "SELECT STU_ACD_ID, STU_GRD_ID, STU_SCT_ID, STU_GENDER,STU_NO FROM dbo.STUDENT_M WHERE STU_ID = " & StuId)
            With dr.Read
                Item = New ListItem
                Item.Text = dr.Item("Stu_acd_Id")
                Item.Value = dr.Item("Stu_acd_Id")
                ddlAcademicYear.Items.Add(Item)
                ddlAcademicYear.SelectedIndex = 0
                Item = New ListItem
                Item.Text = dr.Item("Stu_Grd_Id")
                Item.Value = dr.Item("Stu_Grd_Id")
                ddlGrade.Items.Add(Item)
                ddlGrade.SelectedIndex = 0
                Item = New ListItem
                Item.Text = dr.Item("Stu_sct_Id")
                Item.Value = dr.Item("Stu_sct_Id")
                ddlSection.Items.Add(Item)
                ddlSection.SelectedIndex = 0
                Me.hfStudentGender.Value = dr.Item("stu_gender")
                ddlGrade_SelectedIndexChanged(ddlGrade, Nothing)

                lblStuNo.Text = dr.Item("STU_NO")
            End With
            dr.Close()
            Return True
        Catch ex As Exception
            'Me.trMessage.Visible = True
            Me.lblerror.Text = "There was some problem processing your request. Please try again."
        End Try
    End Function

    Protected Sub ddlGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGrade.SelectedIndexChanged
        'Session("ServicehashCheck") = Nothing
        'callGrade_Section()
        'bindCategory()
        'bindSubCategoryMixed()
        'bindCategory()
    End Sub

    Private Sub bindSubCategoryMixed()
        Try
            Dim ACD_ID As String = String.Empty
            If ddlAcademicYear.SelectedIndex = -1 Then
                ACD_ID = ""
            Else
                ACD_ID = ddlAcademicYear.SelectedItem.Value
            End If
            Dim GRD_ID As String = String.Empty
            If ddlGrade.SelectedIndex = -1 Then
                GRD_ID = ""
            Else
                GRD_ID = ddlGrade.SelectedItem.Value
            End If
            Dim SSC_ID As String = String.Empty
            If ddlCategory.SelectedIndex = -1 Then
                SSC_ID = ""
            Else
                SSC_ID = ddlCategory.SelectedItem.Value
            End If

            Dim CONN As String = ConnectionManger.GetOASIS_SERVICESConnectionString
            Dim PARAM(3) As SqlParameter
            PARAM(0) = New SqlParameter("@BSU_ID", Session("sBsuid"))
            PARAM(1) = New SqlParameter("@ACD_ID", ACD_ID)
            PARAM(2) = New SqlParameter("@GRD_ID", GRD_ID)
            PARAM(3) = New SqlParameter("@Gender", IIf(Me.hfStudentGender.Value = "M", 1, 2))

            'Using SubCategoryReader As SqlDataReader = SqlHelper.ExecuteReader(CONN, CommandType.StoredProcedure, "[ST].[GETCAT_GROUP_GRADE]", PARAM)
            Using SubCategoryReader As SqlDataReader = SqlHelper.ExecuteReader(CONN, CommandType.StoredProcedure, "[ST].[GETCAT_GROUP_GRADE_GMA]", PARAM)
                ddlSubCategoryMixed.Items.Clear()

                If SubCategoryReader.HasRows = True Then

                    ddlSubCategoryMixed.DataSource = SubCategoryReader
                    'ddlsubcategorymixed.DataTextField = "SVC_DESCRIPTION"
                    'ddlSubCategoryMixed.DataTextField = "Activity"
                    ddlSubCategoryMixed.DataTextField = "SVC_Description"
                    ddlSubCategoryMixed.DataValueField = "SVG_ID"
                    ddlSubCategoryMixed.DataBind()

                End If
            End Using
            ddlSubCategoryMixed_SelectedIndexChanged(ddlSubCategoryMixed, Nothing)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub bindGMAServices()
        Try
            Dim ACD_ID As String = String.Empty
            If ddlAcademicYear.SelectedIndex = -1 Then
                ACD_ID = ""
            Else
                ACD_ID = ddlAcademicYear.SelectedItem.Value
            End If
            Dim GRD_ID As String = String.Empty
            'If ddlGrade.SelectedIndex = -1 Then
            '    GRD_ID = ""
            'Else
            '    GRD_ID = ddlGrade.SelectedItem.Value
            'End If
            Dim CONN As String = ConnectionManger.GetOASIS_SERVICESConnectionString
            Dim PARAM(5) As SqlParameter

            PARAM(0) = New SqlParameter("@ACD_ID", ddlGMAcademicYear.SelectedValue)
            PARAM(1) = New SqlParameter("@sBSU_ID", 0)
            PARAM(2) = New SqlParameter("@BSU_ID", Session("sBsuid"))

            Using CategoryReader As SqlDataReader = SqlHelper.ExecuteReader(CONN, CommandType.StoredProcedure, "ST.GET_GMA_SVC_BY_BSU", PARAM)
                ddlSubCategoryMixed.Items.Clear()


                If CategoryReader.HasRows = True Then

                    ddlGMASVC.DataSource = CategoryReader
                    ddlGMASVC.DataTextField = "SVC_DESCRIPTION"
                    ddlGMASVC.DataValueField = "SVC_ID"
                    ddlGMASVC.DataBind()
                    ddlGMASVC.Items.Insert(0, New ListItem("-Select-", "0"))
                Else
                    ddlGMASVC.Items.Insert(0, New ListItem("-No Activity Exists-", "0"))

                End If
            End Using
            '' ddlCategory_SelectedIndexChanged(ddlCategory, Nothing)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub ddlSubCategoryMixed_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSubCategoryMixed.SelectedIndexChanged
        GetCategoryFromSubCategory()
        ' BindSubCategoryMixed_Details()
        bindCategory()
        'gridbind()
    End Sub

    Private Sub bindCategory()
        Try
            Dim ACD_ID As String = String.Empty
            If ddlAcademicYear.SelectedIndex = -1 Then
                ACD_ID = ""
            Else
                ACD_ID = ddlAcademicYear.SelectedItem.Value
            End If
            Dim GRD_ID As String = String.Empty
            'If ddlGrade.SelectedIndex = -1 Then
            '    GRD_ID = ""
            'Else
            '    GRD_ID = ddlGrade.SelectedItem.Value
            'End If
            Dim CONN As String = ConnectionManger.GetOASIS_SERVICESConnectionString
            Dim PARAM(5) As SqlParameter

            PARAM(0) = New SqlParameter("@ACD_ID", ddlAcademicYear.SelectedValue)
            PARAM(1) = New SqlParameter("@sBSU_ID", 0)
            PARAM(2) = New SqlParameter("@BSU_ID", Session("sBsuid"))

            Using CategoryReader As SqlDataReader = SqlHelper.ExecuteReader(CONN, CommandType.StoredProcedure, "ST.GET_GMA_SVC_BY_BSU", PARAM)
                ddlCategory.Items.Clear()


                If CategoryReader.HasRows = True Then

                    ddlSubCategoryMixed.DataSource = CategoryReader
                    ddlSubCategoryMixed.DataTextField = "SVC_DESCRIPTION"
                    ddlSubCategoryMixed.DataValueField = "SVC_ID"
                    ddlSubCategoryMixed.DataBind()
                    ddlSubCategoryMixed.Items.Insert(0, New ListItem("-All-", "0"))
                Else
                    ddlSubCategoryMixed.Items.Insert(0, New ListItem("-No Activity Exists-", "0"))

                End If
            End Using
            'ddlSubCategoryMixed_SelectedIndexChanged(ddlCategory, Nothing)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub GetCategoryFromSubCategory()
        Dim dr As IDataReader
        Dim ds As DataSet
        Try
            'dr = SqlHelper.ExecuteReader(ConnectionManger.GetOASIS_SERVICESConnectionString, CommandType.Text, "SELECT  sc.SSC_ID , sc.SSC_DESC FROM    dbo.SERVICES_GRD_M sgm INNER JOIN dbo.SERVICES_SYS_M ssm ON sgm.SVG_SVC_ID = ssm.SVC_ID INNER JOIN dbo.SERVICES_CATEGORY sc ON sc.SSC_ID = ssm.SVC_SSC_ID WHERE   sgm.SVG_ID = " & Me.ddlSubCategoryMixed.SelectedValue)
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_SERVICESConnectionString, CommandType.Text, "SELECT  sc.SSC_ID , sc.SSC_DESC FROM    dbo.SERVICES_GRD_M sgm INNER JOIN dbo.SERVICES_SYS_M ssm ON sgm.SVG_SVC_ID = ssm.SVC_ID INNER JOIN dbo.SERVICES_CATEGORY sc ON sc.SSC_ID = ssm.SVC_SSC_ID WHERE   sgm.SVG_ID = " & Me.ddlSubCategoryMixed.SelectedValue)
            'With dr.Read
            '    ddlCategory.Items.Clear()

            '    ddlCategory.DataSource = dr
            '    ddlCategory.DataTextField = "SSC_DESC"
            '    ddlCategory.DataValueField = "SSC_ID"
            '    ddlCategory.DataBind()

            'End With
            ddlCategory.DataSource = ds
            ddlCategory.DataTextField = "SSC_DESC"
            ddlCategory.DataValueField = "SSC_ID"
            ddlCategory.DataBind()
            '''lblParentCategory.Text = ddlCategory.SelectedItem.Text
            lblerror.Text = Nothing
        Catch ex As Exception
            lblerror.Text = "There was an issue processing the category"
        End Try
    End Sub

    Private Sub BindSubCategoryMixed_Details()
        Dim bAdded As Boolean
        Dim ACD_ID As String = String.Empty
        Dim bterm As Boolean
        If ddlAcademicYear.SelectedIndex = -1 Then
            ACD_ID = ""
        Else
            ACD_ID = ddlAcademicYear.SelectedItem.Value
        End If
        Dim GRD_ID As String = String.Empty
        If ddlGrade.SelectedIndex = -1 Then
            GRD_ID = ""
        Else
            GRD_ID = ddlGrade.SelectedItem.Value
        End If
        Dim SSC_ID As String = String.Empty
        If ddlCategory.SelectedIndex = -1 Then
            SSC_ID = ""
        Else
            SSC_ID = ddlCategory.SelectedItem.Value
        End If
        Dim SVG_ID As String = String.Empty
        If ddlSubCategoryMixed.SelectedIndex = -1 Then
            SVG_ID = ""
        Else
            SVG_ID = ddlSubCategoryMixed.SelectedItem.Value
        End If
        Dim CONN As String = ConnectionManger.GetOASIS_SERVICESConnectionString
        Dim PARAM(8) As SqlParameter
        PARAM(0) = New SqlParameter("@INFO_TYPE", "PERIOD")
        PARAM(1) = New SqlParameter("@ACD_ID", ACD_ID)
        PARAM(2) = New SqlParameter("@GRD_ID", GRD_ID)
        PARAM(3) = New SqlParameter("@BSU_ID", Session("sBsuid"))
        PARAM(4) = New SqlParameter("@USR_ID", Session("sUsr_id"))
        PARAM(5) = New SqlParameter("@SSC_ID", SSC_ID)
        PARAM(6) = New SqlParameter("@SVG_ID", SVG_ID)

        Dim tmpDs As New DataSet
        tmpDs.Tables.Add()
        tmpDs.Tables(0).Columns.Add("FromDate", GetType(String))
        tmpDs.Tables(0).Columns.Add("ToDate", GetType(String))
        tmpDs.Tables(0).Columns.Add("TermId", GetType(Int32))
        tmpDs.AcceptChanges()

        Using SubCategoryDetailsReader As SqlDataReader = SqlHelper.ExecuteReader(CONN, CommandType.StoredProcedure, "SM.GETSERVICE_MASTER_DETAILS", PARAM)
            chkPeriods.Items.Clear()
            lblMaxCat.Text = ""
            lblMaxSubCat.Text = ""
            lblSeatCap.Text = ""
            '''lblGrp.Text = ""
            hfGender.Value = "0"

            While SubCategoryDetailsReader.Read

                chkPeriods.Items.Add(New ListItem( _
                                     Convert.ToString(SubCategoryDetailsReader("DATES")), _
                                     Convert.ToString(SubCategoryDetailsReader("ID"))))

                Dim row As DataRow = tmpDs.Tables(0).NewRow
                row.Item("FromDate") = SubCategoryDetailsReader("Trm_StartDate").ToString.Split(" ")(0)
                row.Item("ToDate") = SubCategoryDetailsReader("Trm_EndDate").ToString.Split(" ")(0)
                row.Item("TermId") = SubCategoryDetailsReader("ID")
                tmpDs.Tables(0).Rows.Add(row)

                If bAdded = False Then

                    lblMaxCat.Text = Convert.ToString(SubCategoryDetailsReader("GCM_MAX_CATEGORY"))
                    lblMaxSubCat.Text = Convert.ToString(SubCategoryDetailsReader("GMCA_MAX_ACTIVITY"))
                    lblSeatCap.Text = Convert.ToString(SubCategoryDetailsReader("SVG_MAX_SEAT_CAPACITY"))
                    '''lblGrp.Text = Convert.ToString(SubCategoryDetailsReader("GENDER"))
                    bterm = Convert.ToBoolean(SubCategoryDetailsReader("TERMWISE"))
                    hfGender.Value = Convert.ToString(SubCategoryDetailsReader("SVG_GENDER"))
                    bAdded = True
                End If

            End While

            tmpDs.AcceptChanges()

            If tmpDs.Tables(0).Rows.Count > 0 Then
                Me.gvDuration.DataSource = tmpDs
                Me.gvDuration.DataBind()
            End If
            If bterm = False Then
                hfTerms.Value = "0"
                For Each item As ListItem In chkPeriods.Items
                    item.Selected = True
                    item.Enabled = False
                Next
            Else
                hfTerms.Value = "1"
            End If
        End Using
        Me.lblerror.Text = Nothing
    End Sub

    Sub BindStudents()
        Try
            Dim con As String = ConnectionManger.GetOASISConnectionString
            Dim param(4) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@OLU_ID", Session("OLU_ID"))
            param(1) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
            param(1).Direction = ParameterDirection.ReturnValue
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "[ONLINE].[SERVICES_GETSIBLINGS]", param)
            ddlStudent.DataSource = ds.Tables(0)
            ddlStudent.DataTextField = "StudName"
            ddlStudent.DataValueField = "stu_id"
            ddlStudent.DataBind()

            If Not ddlStudent.Items.FindByValue(Session("STU_ID")) Is Nothing Then
                ddlStudent.Items.FindByValue(Session("STU_ID")).Selected = True 'FindByValue
            End If
        Catch ex As Exception

        End Try
    End Sub

    Sub BindGmaAcademicYear()
        Try
            Dim con As String = ConnectionManger.GetOASIS_SERVICESConnectionString
            Dim param(2) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@Bsu_ID", Me.hfGMABsuId.Value)
            param(1) = New SqlClient.SqlParameter("@ACD_CURRENT", 1)

            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "[ST].[GET_GMA_ACADEMIC_YEARS]", param)
            ddlGMAcademicYear.DataSource = ds.Tables(0)
            ddlGMAcademicYear.DataTextField = "ACY_DESCR"
            ddlGMAcademicYear.DataValueField = "ACD_ID"
            ddlGMAcademicYear.DataBind()

            ddlGMAcademicYear.SelectedIndex = 0
            ddlGMAcademicYear_SelectedIndexChanged(ddlGMAcademicYear, Nothing)
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub ddlStudents_SelectedIndexChanged(ByVal o As Object, ByVal e As System.EventArgs) Handles ddlStudent.SelectedIndexChanged
        BindServiceCategory()
        AcademicYear()
        BindServices()
        bindTerms()

        HF_svc_count.Value = ""
        HF_MAX_SVC.Value = ""
    End Sub
    Sub BindServiceCategory()
        Dim con As String = ConnectionManger.GetOASISConnectionString
        Dim param(4) As SqlClient.SqlParameter
        'param(0) = New SqlClient.SqlParameter("@OLU_ID", Session("OLU_ID"))
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "[ONLINE].[SERVICES_GETSERVICES_CATEGORY]", param)
        ddlSVCategory.DataSource = ds.Tables(0)
        ddlSVCategory.DataTextField = "SSC_DESC"
        ddlSVCategory.DataValueField = "SSC_ID"
        ddlSVCategory.DataBind()
        If ds.Tables(0).Rows.Count > 0 Then
            lblLanguage.Text = IIf(Convert.ToString(ds.Tables(0).Rows(0)("SSC_LBL_DESC")) <> "", Convert.ToString(ds.Tables(0).Rows(0)("SSC_LBL_DESC")), "Service")
        Else
            lblLanguage.Text = "Service"
        End If
    End Sub
    Sub BindServices()
        Try
            Dim ACDID_GRDID As String() = New String(9) {}
            ACDID_GRDID = ddlAcademicYear.SelectedValue.Split("_")
            Dim con As String = ConnectionManger.GetOASISConnectionString
            Dim param(4) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@SSC_ID", ddlSVCategory.SelectedValue)
            param(1) = New SqlClient.SqlParameter("@ACD_ID", ACDID_GRDID(0))
            param(2) = New SqlClient.SqlParameter("@GRD_ID", ACDID_GRDID(1))
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "[ONLINE].[SERVICES_GETSERVICES]", param)
            ddlServices.DataSource = ds
            ddlServices.DataTextField = "SVC_DESCRIPTION"
            ddlServices.DataValueField = "SVCID_SVGID"
            ddlServices.DataBind()
            If ds.Tables(0).Rows.Count > 0 Then
                lblLanguage.Text = IIf(Convert.ToString(ds.Tables(0).Rows(0)("SSC_LBL_DESC")) <> "", Convert.ToString(ds.Tables(0).Rows(0)("SSC_LBL_DESC")), "Service")
            Else
                lblLanguage.Text = "Service"
            End If

            bindTerms()
            hidingtables()
        Catch ex As Exception
            'ddlServices.
        End Try

    End Sub

    Protected Sub ddlGMAcademicYear_SelectedIndexChanged(ByVal o As Object, ByVal e As System.EventArgs) Handles ddlGMAcademicYear.SelectedIndexChanged
        'BindServiceCategory()
        'BindServices()
        'HF_svc_count.Value = ""
        'HF_MAX_SVC.Value = ""
        '' BindTermBy_BSU_ACD()
        bindGMAServices()
    End Sub

    Protected Sub ddlGMASVC_SelectedIndexChanged(ByVal o As Object, ByVal e As System.EventArgs) Handles ddlGMASVC.SelectedIndexChanged
        getGMASVC_color()
        BindTermBy_BSU_ACD()
        If ddlGMASVC.SelectedValue = 63 Then 'Additional Lessions(GMA)
            Me.ddlAdditionalLessons.Visible = True
            Me.lblmessage.Visible = True
            BindAdditionalLessons(Session("stu_id"))
        Else
            Me.ddlAdditionalLessons.Visible = False
            Me.lblmessage.Visible = False
            Me.ddlAdditionalLessons.Items.Clear()
        End If

        'If (IsServiceAlreadyRequested(Session("STU_ID"), ddlGMASVC.SelectedValue, ddlGMAcademicYear.SelectedValue)) Then
        '    btnSaveData1.Visible = False
        '    lblerror.Text = "you are already requested for this service"
        'End If
        Dim messageSvc As String = String.Empty

        If messageSvc.ToString = "" Then
            messageSvc = ServiceMessageToDisplay()
            lblMessageDisplay.Text = messageSvc
        End If

        If chkAccepted.Checked Then
            btnSaveData1.Enabled = True
        End If
    End Sub

    Sub BindAdditionalLessons(ByVal stuid As Integer)
        Me.ddlAdditionalLessons.DataValueField = "SSR_SVC_ID"
        Me.ddlAdditionalLessons.DataTextField = "SVC_DESCRIPTION"
        Dim ds As DataSet = GetStudentServiceInfo(stuid)
        If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            Me.ddlAdditionalLessons.DataSource = GetStudentServiceInfo(stuid)
            Me.ddlAdditionalLessons.DataBind()
            ddlAdditionalLessons.Items.Insert(0, New ListItem("-Select-", "0"))
            ddlAdditionalLessons.SelectedIndex = 0
        Else
            ddlAdditionalLessons.Items.Insert(0, New ListItem("-No Activity Exists-", "0"))
        End If


    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal o As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        BindServiceCategory()
        BindServices()
        HF_svc_count.Value = ""
        HF_MAX_SVC.Value = ""

    End Sub
    Sub AcademicYear()
        Dim con As String = ConnectionManger.GetOASISConnectionString
        Dim param(4) As SqlClient.SqlParameter
        param(0) = New SqlClient.SqlParameter("@STU_ID", ddlStudent.SelectedValue)
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "[ONLINE].[SERVICES_GETACADEMICYEAR]", param)
        ddlAcademicYear.DataSource = ds.Tables(0)
        ddlAcademicYear.DataTextField = "ACY_DESCR"
        ddlAcademicYear.DataValueField = "ACDID_GRDID"
        ddlAcademicYear.DataBind()
    End Sub

    Sub BindTermBy_BSU_ACD()
        Dim str_conn As String = ConnectionManger.GetOASIS_SERVICESConnectionString
        Dim param(3) As SqlClient.SqlParameter
        chkPeriods2.ClearSelection()
        chkPeriods2.Items.Clear()

        param(0) = New SqlClient.SqlParameter("@BSU_ID", Me.hfGMABsuId.Value)
        param(1) = New SqlClient.SqlParameter("@ACD_ID", ddlGMAcademicYear.SelectedValue)
        param(2) = New SqlClient.SqlParameter("@SVC_ID", ddlGMASVC.SelectedValue)
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GET_TERMS_BY_BSU_GEN_SERVICE", param)

        If ds.Tables(0).Rows.Count > 0 Then
            For Each row As datarow In ds.Tables(0).Rows

                Dim str1 As String = row("TRM_DESCRIPTION")
                Dim str2 As String = row("TRM_ID")
                'Dim FLAG As Boolean = Mid(row("GRD_ID"), InStr(1, row("GRD_ID"), "|"), 1)
                chkPeriods2.Items.Add(New ListItem(str1, str2))

            Next



            For i As Integer = 0 To chkPeriods2.Items.Count - 1

                chkPeriods2.Items(i).Selected = True
            Next

            chkPeriods_SelectedIndexChnaged(chkPeriods2, Nothing)

        End If
    End Sub

    Sub BindTermBy_BSU_ACD_By_TermID(ByVal TermIDs As String(), ByVal SVC_ID As String)
        Dim str_conn As String = ConnectionManger.GetOASIS_SERVICESConnectionString
        Dim param(3) As SqlClient.SqlParameter
        chkPeriods2.ClearSelection()
        chkPeriods2.Items.Clear()

        param(0) = New SqlClient.SqlParameter("@BSU_ID", Me.hfGMABsuId.Value)
        param(1) = New SqlClient.SqlParameter("@ACD_ID", ddlGMAcademicYear.SelectedValue)
        param(2) = New SqlClient.SqlParameter("@SVC_ID", SVC_ID)
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GET_TERMS_BY_BSU_GEN_SERVICE", param)

        If ds.Tables(0).Rows.Count > 0 Then
            For Each row As datarow In ds.Tables(0).Rows

                Dim str1 As String = row("TRM_DESCRIPTION")
                Dim str2 As String = row("TRM_ID")
                'Dim FLAG As Boolean = Mid(row("GRD_ID"), InStr(1, row("GRD_ID"), "|"), 1)
                chkPeriods2.Items.Add(New ListItem(str1, str2))

            Next



            For i As Integer = 0 To chkPeriods2.Items.Count - 1
                For j As Integer = 0 To TermIDs.Length - 1
                    If chkPeriods2.Items(i).Value = TermIDs(j) Then
                        chkPeriods2.Items(i).Selected = True
                        ' no need to loop any further because we already found the matching checkbox

                    End If
                Next
            Next

            'For i As Integer = 0 To chkPeriods2.Items.Count - 1

            '    chkPeriods2.Items(i).Selected = True
            'Next

            chkPeriods_SelectedIndexChnaged(chkPeriods2, Nothing)

        End If
    End Sub

    Protected Sub chkPeriods_SelectedIndexChnaged(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim selectedTerms As String = String.Empty
        For Each li As ListItem In chkPeriods2.Items
            If li.Selected = True Then
                selectedTerms = selectedTerms + li.Value + ","
            End If


        Next
        selectedTerms = selectedTerms.TrimEnd(",")
        BindTermPeriodBy_TermsIds(selectedTerms)
        If chkAccepted.Checked Then
            'ScriptManager.RegisterClientScriptBlock(Page, Me.[GetType](), "CallJS", "enableSaveData();", True)
            btnSaveData1.Enabled = True
        Else
            btnSaveData1.Enabled = False
        End If

    End Sub
    Protected Sub chkAccepted_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If chkAccepted.Checked Then
            btnSaveData1.Enabled = True
        Else
            btnSaveData1.Enabled = False
        End If
    End Sub



    Sub BindTermPeriodBy_TermsIds(ByVal TermIDs As String)
        Dim str_conn As String = ConnectionManger.GetOASIS_SERVICESConnectionString
        Dim param(1) As SqlClient.SqlParameter


        param(0) = New SqlClient.SqlParameter("@TRM_IDs", TermIDs)


        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "SM.GET_TERM_PERIOD_BY_ID", param)

        If ds.Tables(0).Rows.Count > 0 Then
            For Each row As datarow In ds.Tables(0).Rows

                Dim StartDate As String = row("StartDate").ToString
                Dim EndDate As String = row("EndDate").ToString
                If StartDate <> "" Then
                    txtFrom.Value = Format(Convert.ToDateTime(row("StartDate")), "dd/MMM/yyyy")
                End If
                If EndDate <> "" Then
                    txtTo.Value = Format(Convert.ToDateTime(row("EndDate")), "dd/MMM/yyyy")
                End If

            Next


        End If
    End Sub
    Public Property Terms() As DataTable
        Get
            If ViewState("Terms") Is Nothing Then
                Dim dtTerms As New DataTable()
                dtTerms.Columns.Add("TRM_ID")
                dtTerms.Columns.Add("Term")
                dtTerms.Columns.Add("Amount")
                dtTerms.Columns.Add("StartDate")
                dtTerms.Columns.Add("EndDate")
                ViewState("Terms") = dtTerms
                Return DirectCast(ViewState("Terms"), DataTable)
            Else
                Return DirectCast(ViewState("Terms"), DataTable)
            End If
        End Get
        Set(ByVal value As DataTable)
            ViewState("Terms") = value
        End Set
    End Property
    Sub bindTerms()

        'For i As Integer = 1 To 3
        '    Dim dr As DataRow = Terms.NewRow()
        '    dr("TRM_ID") = i
        '    dr("Term") = "Term" + i.ToString()

        '    dr("Amount") = "600"
        '    dr("StartDate") = ""
        '    dr("EndDate") = ""
        '    Terms.Rows.Add(dr)
        'Next
        Dim ACDID_GRDID As String() = New String(9) {}
        ACDID_GRDID = ddlAcademicYear.SelectedValue.Split("_")
        Dim SVCID_SVGID As String() = New String(3) {}
        SVCID_SVGID = ddlServices.SelectedValue.Split("_")

        ''
        Dim count_sv As String = "0"
        If gvStudentServicesDetails.Items.Count > 0 Then
            Dim con As String = ConnectionManger.GetOASISConnectionString
            Dim param(6) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@ACD_ID", ACDID_GRDID(0))
            param(1) = New SqlClient.SqlParameter("@STU_ID", ddlStudent.SelectedValue)
            param(2) = New SqlClient.SqlParameter("@SSR_SVC_ID", SVCID_SVGID(0))
            param(3) = New SqlClient.SqlParameter("@bapproved", "0")
            count_sv = SqlHelper.ExecuteScalar(con, CommandType.StoredProcedure, "[ONLINE].[SERVICES_CHECK_SVC_AVAILD]", param)
        End If
        ''
        If count_sv = "0" Then
            If SVCID_SVGID.Length > 1 Then

                Dim con As String = ConnectionManger.GetOASISConnectionString
                Dim param(4) As SqlClient.SqlParameter
                param(0) = New SqlClient.SqlParameter("@SVG_ID", SVCID_SVGID(1))
                param(1) = New SqlClient.SqlParameter("@ACD_ID", ACDID_GRDID(0))
                param(2) = New SqlClient.SqlParameter("@SVC_ID", ddlServices.SelectedValue)
                Dim ds As New DataSet
                ds = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "[ONLINE].[SERVICES_GETSERVICES_TERMS]", param)
                ' Dim q = From DT In ds.Tables(0).AsEnumerable()
                Dim dtFiltered As DataTable = ds.Tables(0)
                If ds IsNot Nothing Then

                    If ds.Tables(0).Rows.Count > 0 Then

                        For Each dr As DataRow In ServicesRequested.Rows
                            Dim dvApprovalLevels As DataView = dtFiltered.DefaultView
                            dvApprovalLevels.RowFilter = "STM_ID<>'" + dr("ID") + "'"
                            dtFiltered = dvApprovalLevels.ToTable()
                            dtFiltered.AcceptChanges()
                        Next
                        For Each gr As GridDataItem In gvStudentServicesDetails.Items
                            Dim hf_STU_ID As HiddenField = DirectCast(gr.FindControl("hf_STU_ID0"), HiddenField)
                            Dim hf_STM_ID As HiddenField = DirectCast(gr.FindControl("hf_STM_ID0"), HiddenField)

                            Dim dvApprovalLevels As DataView = dtFiltered.DefaultView
                            If ddlStudent.SelectedValue = hf_STU_ID.Value Then
                                dvApprovalLevels.RowFilter = "STM_ID <>" + hf_STM_ID.Value + " "
                                dtFiltered = dvApprovalLevels.ToTable()
                                dtFiltered.AcceptChanges()
                            End If
                        Next
                    End If
                End If
                gvTerms.DataSource = dtFiltered
                gvTerms.DataBind()
            Else
                gvTerms.DataBind()
            End If
        Else
            gvTerms.DataBind()
            lblerror.Text = ""
        End If
    End Sub
    Public Property ServicesRequested() As DataTable
        Get
            If ViewState("ServicesRequested") Is Nothing Then
                Dim dtServicesRequested As New DataTable()
                dtServicesRequested.Columns.Add("STU_ID")
                dtServicesRequested.Columns.Add("STU_Name")
                dtServicesRequested.Columns.Add("ACADEMIC_YEAR")
                dtServicesRequested.Columns.Add("ACD_ID")
                dtServicesRequested.Columns.Add("SSC_ID")
                dtServicesRequested.Columns.Add("SERVICE")
                dtServicesRequested.Columns.Add("SVC_ID")
                dtServicesRequested.Columns.Add("SVG_ID")
                dtServicesRequested.Columns.Add("TERM")
                dtServicesRequested.Columns.Add("TRM_ID")
                dtServicesRequested.Columns.Add("AMOUNT")
                dtServicesRequested.Columns.Add("bAPPROVED")
                dtServicesRequested.Columns.Add("LEVEL")
                dtServicesRequested.Columns.Add("CurrentTeacher")
                dtServicesRequested.Columns.Add("OWNINSTRUMENT")
                dtServicesRequested.Columns.Add("ID")
                ViewState("ServicesRequested") = dtServicesRequested
                Return DirectCast(ViewState("ServicesRequested"), DataTable)
            Else
                Return DirectCast(ViewState("ServicesRequested"), DataTable)
            End If
        End Get
        Set(ByVal value As DataTable)
            ViewState("ServicesRequested") = value
        End Set

    End Property

    Sub GetServicesCount()
        Dim ACDID_GRDID As String() = New String(9) {}
        ACDID_GRDID = ddlAcademicYear.SelectedValue.Split("_")

        Dim con As String = ConnectionManger.GetOASISConnectionString
        Dim param(6) As SqlClient.SqlParameter
        param(0) = New SqlClient.SqlParameter("@ACD_ID", ACDID_GRDID(0))
        param(1) = New SqlClient.SqlParameter("@STU_ID", ddlStudent.SelectedValue)
        param(2) = New SqlClient.SqlParameter("@SSC_ID", ddlSVCategory.SelectedValue)

        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "[ONLINE].[SERVICES_GET_SVC_BSULIMIT]", param)
        If ds IsNot Nothing Then
            HF_svc_count.Value = ds.Tables(0).Rows(0)("STU_SERVICE_COUNT")
            HF_MAX_SVC.Value = ds.Tables(0).Rows(0)("MAX_SVC")
        End If

    End Sub

    Protected Sub btnRequestService_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRequestService.Click

        Dim ACDID_GRDID As String() = New String(9) {}
        ACDID_GRDID = ddlAcademicYear.SelectedValue.Split("_")
        Dim SVCID_SVGID As String() = New String(3) {}
        SVCID_SVGID = ddlServices.SelectedValue.Split("_")
        GetServicesCount()
        If ServicesRequested IsNot Nothing Then

            'Dim SvcReq = (From SR In ServicesRequested.AsEnumerable() _
            'Where(SR("STU_ID") = ddlStudents.SelectedValue And SR("ACD_ID") = ACDID_GRDID(0) And SR("SSC_ID") = ddlSVCategory.SelectedValue)
            'Select SR("SVC_ID")).Distinct
            ''
            Dim ServicesRequested_DT As DataTable = ServicesRequested
            Dim dvApprovalLevels As DataView = ServicesRequested_DT.DefaultView
            dvApprovalLevels.RowFilter = "STU_ID =" + ddlStudent.SelectedValue + " and  ACD_ID = " + ACDID_GRDID(0) + " and SSC_ID=" + ddlSVCategory.SelectedValue + ""
            ServicesRequested_DT = dvApprovalLevels.ToTable(True, "SVC_ID")
            ServicesRequested_DT.AcceptChanges()
            'ServicesRequested = ServicesRequested_DT

            Dim counter As Integer = 0
            Dim dr As DataRow
            If gvStudentServicesDetails.Items.Count > 0 Then
                'For Each d In SvcReq
                For Each dr In ServicesRequested_DT.Rows
                    Dim con As String = ConnectionManger.GetOASISConnectionString
                    Dim param(6) As SqlClient.SqlParameter
                    param(0) = New SqlClient.SqlParameter("@ACD_ID", ACDID_GRDID(0))
                    param(1) = New SqlClient.SqlParameter("@STU_ID", ddlStudent.SelectedValue)
                    param(2) = New SqlClient.SqlParameter("@SSR_SVC_ID", dr("SVC_ID"))
                    Dim count_sv As String = ""
                    count_sv = SqlHelper.ExecuteScalar(con, CommandType.StoredProcedure, "[ONLINE].[SERVICES_CHECK_SVC_AVAILD]", param)

                    counter = counter + IIf(count_sv = "1", 1, 0)
                Next
            End If
            ''
            Dim a As Integer = IIf(ServicesRequested_DT IsNot Nothing, ServicesRequested_DT.Rows.Count, 0)
            Dim count_F As Integer = IIf(HF_svc_count.Value <> "0", Convert.ToInt16(HF_svc_count.Value) - counter, counter)

            HF_svc_count.Value = Convert.ToInt16(a) + count_F
        End If

        If Convert.ToInt16(HF_svc_count.Value) < Convert.ToInt16(HF_MAX_SVC.Value) Then

            For Each gr As GridDataItem In gvTerms.Items

                Dim cbTerm As CheckBox = DirectCast(gr.FindControl("cbTerm"), CheckBox)
                Dim lblTRM_DESCRIPTION As Label = DirectCast(gr.FindControl("lblTRM_DESCRIPTION"), Label)
                Dim lblTERMFEES As Label = DirectCast(gr.FindControl("lblTERMFEES"), Label)
                Dim hf_TRMID As HiddenField = DirectCast(gr.FindControl("hf_TRMID"), HiddenField)
                Dim hf_STM_ID As HiddenField = DirectCast(gr.FindControl("hf_STM_ID"), HiddenField)
                If cbTerm.Checked Then

                    Dim status As Integer = 0
                    Dim ID As String = hf_STM_ID.Value  'ddlStudents.SelectedValue + "_" + ddlAcademicYear.SelectedValue + "_" + ddlServices.SelectedValue + "_" + lblTRM_DESCRIPTION.Text

                    For Each dr As DataRow In ServicesRequested.Rows
                        If ID = Convert.ToString(dr("ID")) Then
                            status = 1
                            Exit For
                        End If
                    Next

                    If status = 0 Then
                        Dim dr As DataRow = ServicesRequested.NewRow()
                        dr("STU_ID") = ddlStudent.SelectedValue
                        dr("STU_Name") = ddlStudent.SelectedItem.Text
                        dr("ACADEMIC_YEAR") = ddlAcademicYear.SelectedItem.Text
                        dr("ACD_ID") = ACDID_GRDID(0)
                        dr("SSC_ID") = ddlSVCategory.SelectedValue
                        dr("SERVICE") = ddlServices.SelectedItem.Text
                        dr("SVC_ID") = SVCID_SVGID(0)
                        dr("SVG_ID") = SVCID_SVGID(1)
                        dr("TERM") = lblTRM_DESCRIPTION.Text
                        dr("TRM_ID") = hf_TRMID.Value
                        dr("AMOUNT") = lblTERMFEES.Text
                        dr("LEVEL") = ddlLevels.SelectedValue
                        dr("CurrentTeacher") = txtCurrentTeacher.Text
                        dr("OWNINSTRUMENT") = IIf(rbOwnIns.SelectedValue = "1", "1", "0")

                        dr("bAPPROVED") = False
                        dr("ID") = hf_STM_ID.Value 'ddlStudents.SelectedValue + "_" + ddlAcademicYear.SelectedValue + "_" + ddlServices.SelectedValue + "_" + "TermID"
                        ServicesRequested.Rows.Add(dr)
                    Else
                        'lblmessage.Text = "This User is already added in the approval list..!!"
                    End If
                End If
            Next
            bindTerms()
        Else

            lblerror.Text = "Maximum limit for this Service Category Reached...!!"
        End If


        ServicesRequested.DefaultView.RowFilter = ""
        gvServicesRequested.DataSource = ServicesRequested
        gvServicesRequested.DataBind()
        enableButton()

        ddlLevels.SelectedIndex = -1
        txtCurrentTeacher.Text = ""
    End Sub
#Region "using Linq"
    'Protected Sub btnRequestService_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRequestService.Click

    '    Dim ACDID_GRDID As String() = New String(9) {}
    '    ACDID_GRDID = ddlAcademicYear.SelectedValue.Split("_")
    '    Dim SVCID_SVGID As String() = New String(3) {}
    '    SVCID_SVGID = ddlServices.SelectedValue.Split("_")

    '    If HF_svc_count.Value = "" Then
    '        GetServicesCount()
    '    End If
    '    If ServicesRequested IsNot Nothing Then

    '        Dim SvcReq = (From SR In ServicesRequested.AsEnumerable() _
    ' Where SR("STU_ID") = ddlStudents.SelectedValue And SR("ACD_ID") = ACDID_GRDID(0) And SR("SSC_ID") = ddlSVCategory.SelectedValue
    ' Select SR("SVC_ID")).Distinct
    '        ''
    '        Dim ServicesRequested_DT As DataTable = ServicesRequested
    '        Dim dvApprovalLevels As DataView = ServicesRequested_DT.DefaultView
    '        dvApprovalLevels.RowFilter = "STU_ID =" + ddlStudents.SelectedValue + " and  ACD_ID = " + ACDID_GRDID(0) + " and SSC_ID=" + ddlSVCategory.SelectedValue + ""
    '        ServicesRequested_DT = dvApprovalLevels.ToTable(True, "SVC_ID")
    '        ServicesRequested_DT.AcceptChanges()
    '        'ServicesRequested = ServicesRequested_DT

    '        Dim counter As Integer = 0
    '        If gvStudentServicesDetails.Items.Count > 0 Then
    '            For Each d In SvcReq
    '                Dim con As String = ConnectionManger.GetOASISConnectionString
    '                Dim param(6) As SqlClient.SqlParameter
    '                param(0) = New SqlClient.SqlParameter("@ACD_ID", ACDID_GRDID(0))
    '                param(1) = New SqlClient.SqlParameter("@STU_ID", ddlStudents.SelectedValue)
    '                param(2) = New SqlClient.SqlParameter("@SSR_SVC_ID", d)
    '                Dim count_sv As String = ""
    '                count_sv = SqlHelper.ExecuteScalar(con, CommandType.StoredProcedure, "[ONLINE].[SERVICES_CHECK_SVC_AVAILD]", param)

    '                counter = counter + If(count_sv = "1", 1, 0)
    '            Next
    '        End If
    '        ''
    '        Dim a As Integer = SvcReq.Count()
    '        HF_svc_count.Value = Convert.ToInt16(a) + Convert.ToInt16(HF_svc_count.Value) - counter
    '    End If






    '    If Convert.ToInt16(HF_svc_count.Value) < Convert.ToInt16(HF_MAX_SVC.Value) Then

    '        For Each gr As GridDataItem In gvTerms.Items

    '            Dim cbTerm As CheckBox = DirectCast(gr.FindControl("cbTerm"), CheckBox)
    '            Dim lblTRM_DESCRIPTION As Label = DirectCast(gr.FindControl("lblTRM_DESCRIPTION"), Label)
    '            Dim lblTERMFEES As Label = DirectCast(gr.FindControl("lblTERMFEES"), Label)
    '            Dim hf_TRMID As HiddenField = DirectCast(gr.FindControl("hf_TRMID"), HiddenField)
    '            Dim hf_STM_ID As HiddenField = DirectCast(gr.FindControl("hf_STM_ID"), HiddenField)
    '            If cbTerm.Checked Then

    '                Dim status As Integer = 0
    '                Dim ID As String = hf_STM_ID.Value  'ddlStudents.SelectedValue + "_" + ddlAcademicYear.SelectedValue + "_" + ddlServices.SelectedValue + "_" + lblTRM_DESCRIPTION.Text

    '                For Each dr As DataRow In ServicesRequested.Rows
    '                    If ID = Convert.ToString(dr("ID")) Then
    '                        status = 1
    '                        Exit For
    '                    End If
    '                Next

    '                If status = 0 Then
    '                    Dim dr As DataRow = ServicesRequested.NewRow()
    '                    dr("STU_ID") = ddlStudents.SelectedValue
    '                    dr("STU_Name") = ddlStudents.SelectedItem.Text
    '                    dr("ACADEMIC_YEAR") = ddlAcademicYear.SelectedItem.Text
    '                    dr("ACD_ID") = ACDID_GRDID(0)
    '                    dr("SSC_ID") = ddlSVCategory.SelectedValue
    '                    dr("SERVICE") = ddlServices.SelectedItem.Text
    '                    dr("SVC_ID") = SVCID_SVGID(0)
    '                    dr("SVG_ID") = SVCID_SVGID(1)
    '                    dr("TERM") = lblTRM_DESCRIPTION.Text
    '                    dr("TRM_ID") = hf_TRMID.Value
    '                    dr("AMOUNT") = lblTERMFEES.Text
    '                    dr("LEVEL") = ddlLevels.SelectedValue
    '                    dr("CurrentTeacher") = txtCurrentTeacher.Text
    '                    dr("OWNINSTRUMENT") = IIf(rbOwnIns.SelectedValue = "1", "1", "0")

    '                    dr("bAPPROVED") = False
    '                    dr("ID") = hf_STM_ID.Value 'ddlStudents.SelectedValue + "_" + ddlAcademicYear.SelectedValue + "_" + ddlServices.SelectedValue + "_" + "TermID"
    '                    ServicesRequested.Rows.Add(dr)
    '                Else
    '                    'lblmessage.Text = "This User is already added in the approval list..!!"
    '                End If
    '            End If
    '        Next
    '        gvServicesRequested.DataSource = ServicesRequested
    '        gvServicesRequested.DataBind()
    '        bindTerms()
    '    Else
    '        lblerror.Text = "Maximum limit for this Service Category Reached...!!"
    '    End If
    '    enableButton()
    'End Sub
    Sub SaveServiceRequest()

        Dim constring As String = ConnectionManger.GetOASISConnectionString
        Dim con As SqlConnection = New SqlConnection(constring)
        con.Open()
        Dim sqltran As SqlTransaction
        sqltran = con.BeginTransaction("trans")
        Dim bsaved As Integer = 0
        Dim bSSRID As String = ""
        Try
            For Each gr As GridViewRow In gvServicesRequested.Rows

                Dim HF_SVG_ID As HiddenField = DirectCast(gr.FindControl("HF_SVG_ID"), HiddenField)
                Dim hf_STU_ID As HiddenField = DirectCast(gr.FindControl("hf_STU_ID"), HiddenField)
                Dim hf_STM_ID As HiddenField = DirectCast(gr.FindControl("hf_STM_ID"), HiddenField)
                Dim HF_SVC_ID As HiddenField = DirectCast(gr.FindControl("HF_SVC_ID"), HiddenField)
                Dim HF_ACD_ID As HiddenField = DirectCast(gr.FindControl("HF_ACD_ID"), HiddenField)


                Dim ServicesRequested_DT1 As DataTable = ServicesRequested
                Dim dvApprovalLevels As DataView = ServicesRequested_DT1.DefaultView
                dvApprovalLevels.RowFilter = "STU_ID =" + hf_STU_ID.Value + " and  ACD_ID = " + HF_ACD_ID.Value + " and SVC_ID=" + HF_SVC_ID.Value + ""
                Dim ServicesRequested_DT As DataTable = dvApprovalLevels.ToTable()
                Dim SERVICE_TERM_XML As String = "<SERVICES>"
                Dim Terms_Service As String = ""

                For Each d As DataRow In ServicesRequested_DT.Rows

                    If Terms_Service <> "" Then
                        Terms_Service = Terms_Service + "|" + d("ID").ToString()
                    Else
                        Terms_Service = d("ID").ToString()
                    End If
                    SERVICE_TERM_XML = SERVICE_TERM_XML + "<TERM STM_ID='" + d("ID").ToString() + "' SRS_FEE='" + d("AMOUNT").ToString() + "' SRS_LEVEL='" + d("LEVEL").ToString() + "' SRS_TEACHER='" + d("CurrentTeacher").ToString() + "' SRS_OWN_INSTRUMENT='" + d("OWNINSTRUMENT").ToString() + "' STU_ID='" + d("STU_ID").ToString() + "' SVC_ID='" + d("SVC_ID").ToString() + "' ></TERM>"
                Next
                SERVICE_TERM_XML = SERVICE_TERM_XML + "</SERVICES>"

                Dim Terms_SVC As String() = New String(9) {}
                Terms_SVC = Terms_Service.Split("|")

                Dim param1(2) As SqlClient.SqlParameter
                param1(0) = New SqlClient.SqlParameter("@SSR_SVG_ID", HF_SVG_ID.Value)
                Dim ds As New DataSet
                ds = SqlHelper.ExecuteDataset(sqltran, CommandType.StoredProcedure, "[ONLINE].[SERVICES_GET_TRM_CONTINUES]", param1)
                Dim dt_TRM_CONTINUES As New DataTable
                dt_TRM_CONTINUES = ds.Tables(0)
                Dim flag As Integer = 0
                Dim j As Integer = 0
                Dim STM_ID_Save As String = ""
                Dim i As Integer = 0

                For Each row As DataRow In dt_TRM_CONTINUES.Rows
                    i = i + 1
                    If Terms_SVC.Length > j Then
                        If Terms_SVC(j) = row("STM_ID") Then

                            If STM_ID_Save <> "" Then
                                STM_ID_Save = STM_ID_Save + "|" + Terms_SVC(j).ToString()
                            Else
                                STM_ID_Save = Terms_SVC(j).ToString()
                            End If
                            flag = 0
                            j = j + 1
                        Else
                            flag = 1
                        End If
                    Else
                        flag = 1
                    End If


                    If flag = 1 Or i = dt_TRM_CONTINUES.Rows.Count Then
                        If STM_ID_Save <> "" Then
                            Dim lblAMOUNT As Label = DirectCast(gr.FindControl("lblAMOUNT"), Label)

                            Dim HF_Level As HiddenField = DirectCast(gr.FindControl("HF_Level"), HiddenField)
                            Dim HF_Teacher As HiddenField = DirectCast(gr.FindControl("HF_Teacher"), HiddenField)

                            Dim param(12) As SqlClient.SqlParameter
                            param(0) = New SqlClient.SqlParameter("@SSR_SVG_ID", HF_SVG_ID.Value)
                            param(1) = New SqlClient.SqlParameter("@SSR_STU_ID", hf_STU_ID.Value)
                            param(2) = New SqlClient.SqlParameter("@SRS_STM_ID", hf_STM_ID.Value)
                            param(3) = New SqlClient.SqlParameter("@SRS_FEE", lblAMOUNT.Text)
                            param(4) = New SqlClient.SqlParameter("@SRS_LEVEL", HF_Level.Value)
                            param(5) = New SqlClient.SqlParameter("@SRS_TEACHER", HF_Teacher.Value)
                            param(6) = New SqlClient.SqlParameter("@SRS_OWN_INSTRUMENT", "0")

                            param(7) = New SqlClient.SqlParameter("@SERVICE_TERM_XML", SERVICE_TERM_XML)
                            param(8) = New SqlClient.SqlParameter("@STM_IDS", STM_ID_Save) 'STM_ID_Save
                            param(9) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                            param(9).Direction = ParameterDirection.ReturnValue
                            param(10) = New SqlClient.SqlParameter("@MSG", SqlDbType.VarChar, 1000)
                            param(10).Direction = ParameterDirection.Output
                            param(11) = New SqlClient.SqlParameter("@SSR_REQ_USR", Session("username"))
                            SqlHelper.ExecuteNonQuery(sqltran, CommandType.StoredProcedure, "[ONLINE].[SAVE_SERVICES_REQUESTED_STUDENT]", param)
                            Dim ReturnFlag As Integer = param(9).Value


                            If ReturnFlag = 600 Then
                                Dim SSR_ID As String = param(10).Value
                                bSSRID = SSR_ID
                                lnkbtnProceed2Payment.PostBackUrl = "ServiceRequestPayment.aspx?SSRID=" + Encr_decrData.Encrypt(SSR_ID)

                                'Dim param2(7) As SqlClient.SqlParameter
                                'param2(0) = New SqlClient.SqlParameter("@SSR_ID", SSR_ID)
                                'param2(1) = New SqlClient.SqlParameter("@SRS_DISCOUNT", 0)
                                ''param(2) = New SqlClient.SqlParameter("@SRS_FROMDATE", txtfromdate.Text)
                                'param2(2) = New SqlClient.SqlParameter("@OPTION", 1)
                                'param2(3) = New SqlClient.SqlParameter("@MSG", SqlDbType.VarChar, 1000)
                                'param2(3).Direction = ParameterDirection.Output
                                'param2(4) = New SqlClient.SqlParameter("@SSR_REMARKS", "Auto-Approval")

                                'SqlHelper.ExecuteNonQuery(sqltran, CommandType.StoredProcedure, "[ONLINE].[SERVICES_APPROVE_SVC_REQUEST]", param2)
                                'lblerror.Text = param2(3).Value

                                'callSend_mail(hf_STU_ID.Value, SSR_ID, HF_SVC_ID.Value, sqltran, 4)
                            End If
                            STM_ID_Save = ""
                            flag = 0
                        End If
                    End If
                Next row
            Next
            sqltran.Commit()
            BindStudentServices()
            Delete_ServicesRequested("", "", "0")
            'lblerror.Text = "Service Saved Sucessfully...!!!!"

        Catch ex As Exception
            sqltran.Rollback()
            con.Close()
            lblerror.Text = ex.Message
        Finally
            ' BindStudentServices()
        End Try
        If bSSRID <> "" Then
            Response.Redirect("ServiceRequestPayment.aspx?SSRID=" + Encr_decrData.Encrypt(bSSRID))
        End If
    End Sub
#End Region

    Protected Sub ddlServices_SelectedIndexChanged(ByVal o As Object, ByVal e As System.EventArgs) Handles ddlServices.SelectedIndexChanged
        bindTerms()
        ddlLevels.SelectedValue = "0"
        HF_svc_count.Value = ""
        HF_MAX_SVC.Value = ""
    End Sub
    Sub hidingtables()
        Dim bTERMWISE As String = "0"
        Dim SSC_bSHOWFLD As String = "0"

        Dim ACDID_GRDID As String() = New String(9) {}
        ACDID_GRDID = ddlAcademicYear.SelectedValue.Split("_")
        Dim param1(9) As SqlClient.SqlParameter
        param1(0) = New SqlClient.SqlParameter("@SSC_ID", ddlSVCategory.SelectedValue)
        param1(1) = New SqlClient.SqlParameter("@ACD_ID", ACDID_GRDID(0))
        param1(2) = New SqlClient.SqlParameter("@STU_ID", ddlStudent.SelectedValue)

        Dim con As String = ConnectionManger.GetOASISConnectionString
        Using EmailHost_reader As SqlDataReader = SqlHelper.ExecuteReader(con, CommandType.StoredProcedure, "[ONLINE].[SERVICES_ISTERM_WISE]", param1)
            If EmailHost_reader.HasRows Then
                While EmailHost_reader.Read
                    bTERMWISE = Convert.ToString(EmailHost_reader("SVB_bTERMWISE"))
                    SSC_bSHOWFLD = Convert.ToString(EmailHost_reader("SSC_bSHOWFLD"))
                End While
            End If
        End Using


        bTERMWISE = "True"
        If bTERMWISE <> "True" Then
            ' tr_Terms.Attributes.Add(    
            RequiredFieldValidator1.Visible = True
            Comparevalidator1.Visible = False
            tr_Terms.Attributes.Add("style", "display:none")
            tr_CurrentTeacher.Attributes.Add("style", "display:none")
            tr_Level.Attributes.Add("style", "display:none")
            tr_Music.Attributes.Add("style", "display:none")
            tr_MusicServiceReq.Attributes.Add("style", "display:none")
            trOwnIns.Attributes.Add("style", "display:none")
            tr_ServiceStartdate.Visible = True '("style", "display:block")
            btn_SaveRequest.Enabled = True

            ServicesRequested.Rows.Clear()
            gvServicesRequested.DataSource = ServicesRequested
            gvServicesRequested.DataBind()
        Else
            Comparevalidator1.Visible = True
            RequiredFieldValidator1.Visible = False
            ' CompareValidator2.Visible = False
            tr_Terms.Attributes.Add("style", "display:block")
            tr_CurrentTeacher.Attributes.Add("style", "display:block")
            tr_Level.Attributes.Add("style", "display:block")
            tr_Music.Attributes.Add("style", "display:block")
            tr_MusicServiceReq.Attributes.Add("style", "display:block")
            trOwnIns.Attributes.Add("style", "display:block")
            tr_ServiceStartdate.Visible = False '("style", "display:none")
        End If

        If SSC_bSHOWFLD <> "True" Then
            tr_CurrentTeacher.Attributes.Add("style", "display:none")
            tr_Level.Attributes.Add("style", "display:none")
            Comparevalidator1.ValidationGroup = "save"
        Else
            tr_CurrentTeacher.Attributes.Add("style", "display:block")
            tr_Level.Attributes.Add("style", "display:block")
        End If
    End Sub
    Sub enableButton()
        If gvServicesRequested.Rows.Count > 0 Then 'Itemsa
            btn_SaveRequest.Enabled = True
        Else
            btn_SaveRequest.Enabled = False
        End If
    End Sub
    Protected Sub ddlSVCategory_SelectedIndexChanged(ByVal o As Object, ByVal e As System.EventArgs) Handles ddlSVCategory.SelectedIndexChanged
        ddlLevels.SelectedValue = "0"
        If gvServicesRequested.Rows.Count > 0 Then
            pnlSvcCatChange.Visible = True
            ViewState("SVCategory_Oldvalue") = ViewState("SVC_Value")
        Else
            BindServices()
            HF_svc_count.Value = ""
            HF_MAX_SVC.Value = ""
            'CompareValidator2.ValueToCompare = DateTime.Now.Date
        End If
        ViewState("SVC_Value") = ddlSVCategory.SelectedValue
    End Sub
    Protected Sub btn_Proceed_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Proceed.Click
        BindServices()
        HF_svc_count.Value = ""
        HF_MAX_SVC.Value = ""
        ServicesRequested.Rows.Clear()
        gvServicesRequested.DataSource = ServicesRequested
        gvServicesRequested.DataBind()
        pnlSvcCatChange.Visible = False
    End Sub

    Protected Sub btn_cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_cancel.Click
        If (ViewState("SVCategory_Oldvalue") <> "0") Then
            ddlSVCategory.SelectedValue = ViewState("SVCategory_Oldvalue")
        End If
        pnlSvcCatChange.Visible = False
    End Sub
    Protected Sub btn_SaveRequest_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_SaveRequest.Click

        If gvServicesRequested.Rows.Count > 0 Then
            SaveServiceRequest()
        Else
            If ddlServices.Items.Count > 0 Then
                saveNormalService()
            Else
                lblerror.Text = "No services avaliable to save"
            End If
        End If

    End Sub
    Sub saveNormalService()
        Dim SSRID As String = String.Empty
        Dim constring As String = ConnectionManger.GetOASISConnectionString
        Dim con As SqlConnection = New SqlConnection(constring)
        con.Open()
        Dim sqltran As SqlTransaction
        sqltran = con.BeginTransaction("trans")
        Try
            Dim ACDID_GRDID As String() = New String(9) {}
            ACDID_GRDID = ddlAcademicYear.SelectedValue.Split("_")

            Dim param(11) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@SSR_STU_ID", ddlStudent.SelectedValue)
            param(1) = New SqlClient.SqlParameter("@SSR_ACD_ID", ACDID_GRDID(0))
            param(2) = New SqlClient.SqlParameter("@SSR_SVC_ID", ddlServices.SelectedValue)
            param(3) = New SqlClient.SqlParameter("@SSR_SERVICE_STARTDT", txtfromdate.Text)

            param(4) = New SqlClient.SqlParameter("@MSG", SqlDbType.VarChar, 1000)
            param(4).Direction = ParameterDirection.Output

            param(5) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            param(5).Direction = ParameterDirection.ReturnValue

            param(6) = New SqlClient.SqlParameter("@SSC_ID", ddlSVCategory.SelectedValue)

            param(7) = New SqlClient.SqlParameter("@SRS_LEVEL", IIf(ddlLevels.SelectedValue <> "", ddlLevels.SelectedItem.Text, DBNull.Value))
            param(8) = New SqlClient.SqlParameter("@SRS_TEACHER", txtCurrentTeacher.Text)

            param(9) = New SqlClient.SqlParameter("@SSRID", SqlDbType.VarChar, 1000)
            param(9).Direction = ParameterDirection.Output

            param(10) = New SqlClient.SqlParameter("@SSR_REQ_USR", Session("username"))
            SqlHelper.ExecuteNonQuery(sqltran, CommandType.StoredProcedure, "[ONLINE].[SAVE_SERVICES_REQUESTED_GENERAL]", param)


            Dim ReturnFlag As Integer = param(5).Value
            If ReturnFlag = 600 Then
                SSRID = param(9).Value
                'callSend_mail(ddlStudents.SelectedValue, SSRID, ddlServices.SelectedValue, sqltran, 4)
                lblerror.Text = "Service Saved Sucessfully...!!!!"
                sqltran.Commit()
            Else
                lblerror.Text = param(4).Value
                sqltran.Rollback()
            End If
            BindStudentServices()
        Catch ex As Exception
            sqltran.Rollback()
            lblerror.Text = ex.Message
        Finally
            BindStudentServices()
        End Try
        If SSRID <> "" Then
            Response.Redirect("ServiceRequestPayment.aspx?SSRID=" + Encr_decrData.Encrypt(SSRID))
        End If

    End Sub

    'Sub SaveServiceRequest()
    '    Dim constring As String = ConnectionManger.GetOASISConnectionString
    '    Dim con As SqlConnection = New SqlConnection(constring)
    '    con.Open()
    '    Dim sqltran As SqlTransaction
    '    sqltran = con.BeginTransaction("trans")
    '    Dim bsaved As Integer = 0
    '    Dim bSSRID As String = ""
    '    Try
    '        For Each gr As GridViewRow In gvServicesRequested.Rows  ' GridDataItem    Items 

    '            Dim HF_SVG_ID As HiddenField = DirectCast(gr.FindControl("HF_SVG_ID"), HiddenField)
    '            Dim hf_STU_ID As HiddenField = DirectCast(gr.FindControl("hf_STU_ID"), HiddenField)
    '            Dim hf_STM_ID As HiddenField = DirectCast(gr.FindControl("hf_STM_ID"), HiddenField)
    '            Dim HF_SVC_ID As HiddenField = DirectCast(gr.FindControl("HF_SVC_ID"), HiddenField)
    '            Dim HF_ACD_ID As HiddenField = DirectCast(gr.FindControl("HF_ACD_ID"), HiddenField)

    '            Dim ServicesRequested_DT1 As DataTable = ServicesRequested
    '            Dim dvApprovalLevels As DataView = ServicesRequested_DT1.DefaultView
    '            dvApprovalLevels.RowFilter = "STU_ID =" + hf_STU_ID.Value + " and  ACD_ID = " + HF_ACD_ID.Value + " and SVC_ID=" + HF_SVC_ID.Value + ""
    '            Dim ServicesRequested_DT As DataTable = dvApprovalLevels.ToTable()
    '            Dim SERVICE_TERM_XML As String = "<SERVICES>"
    '            Dim Terms_Service As String = ""

    '            For Each d As DataRow In ServicesRequested_DT.Rows
    '                If Terms_Service <> "" Then
    '                    Terms_Service = Terms_Service + "|" + d("ID").ToString()
    '                Else
    '                    Terms_Service = d("ID").ToString()
    '                End If
    '                SERVICE_TERM_XML = SERVICE_TERM_XML + "<TERM STM_ID='" + d("ID").ToString() + "' SRS_FEE='" + d("AMOUNT").ToString() + "' SRS_LEVEL='" + d("LEVEL").ToString() + "' SRS_TEACHER='" + d("CurrentTeacher").ToString() + "' SRS_OWN_INSTRUMENT='" + d("OWNINSTRUMENT").ToString() + "' STU_ID='" + d("STU_ID").ToString() + "' SVC_ID='" + d("SVC_ID").ToString() + "' ></TERM>"
    '            Next

    '            SERVICE_TERM_XML = SERVICE_TERM_XML + "</SERVICES>"

    '            Dim Terms_SVC As String() = New String(9) {}
    '            Terms_SVC = Terms_Service.Split("|")

    '            Dim param1(2) As SqlClient.SqlParameter
    '            param1(0) = New SqlClient.SqlParameter("@SSR_SVG_ID", HF_SVG_ID.Value)
    '            Dim ds As New DataSet
    '            ds = SqlHelper.ExecuteDataset(sqltran, CommandType.StoredProcedure, "[ONLINE].[SERVICES_GET_TRM_CONTINUES]", param1)
    '            Dim dt_TRM_CONTINUES As New DataTable
    '            dt_TRM_CONTINUES = ds.Tables(0)
    '            Dim flag As Integer = 0
    '            Dim j As Integer = 0
    '            Dim STM_ID_Save As String = ""
    '            Dim i As Integer = 0
    '            Dim failure As Boolean = False
    '            For Each row As DataRow In dt_TRM_CONTINUES.Rows
    '                i = i + 1
    '                If (j < Terms_SVC.Length) Then


    '                    If Terms_SVC(j) = row("STM_ID") Then

    '                        If STM_ID_Save <> "" Then
    '                            STM_ID_Save = STM_ID_Save + "|" + Terms_SVC(j).ToString()
    '                        Else
    '                            STM_ID_Save = Terms_SVC(j).ToString()
    '                        End If
    '                        flag = 0
    '                        j = j + 1
    '                    Else
    '                        flag = 1
    '                        j = j + 1

    '                    End If

    '                    If flag = 1 Or i = dt_TRM_CONTINUES.Rows.Count Then
    '                        If STM_ID_Save <> "" Then
    '                            Dim lblAMOUNT As Label = DirectCast(gr.FindControl("lblAMOUNT"), Label)

    '                            Dim HF_Level As HiddenField = DirectCast(gr.FindControl("HF_Level"), HiddenField)
    '                            Dim HF_Teacher As HiddenField = DirectCast(gr.FindControl("HF_Teacher"), HiddenField)

    '                            Dim param(12) As SqlClient.SqlParameter
    '                            param(0) = New SqlClient.SqlParameter("@SSR_SVG_ID", HF_SVG_ID.Value)
    '                            param(1) = New SqlClient.SqlParameter("@SSR_STU_ID", hf_STU_ID.Value)
    '                            param(2) = New SqlClient.SqlParameter("@SRS_STM_ID", hf_STM_ID.Value)
    '                            param(3) = New SqlClient.SqlParameter("@SRS_FEE", lblAMOUNT.Text)
    '                            param(4) = New SqlClient.SqlParameter("@SRS_LEVEL", HF_Level.Value)
    '                            param(5) = New SqlClient.SqlParameter("@SRS_TEACHER", HF_Teacher.Value)
    '                            param(6) = New SqlClient.SqlParameter("@SRS_OWN_INSTRUMENT", "0")

    '                            param(7) = New SqlClient.SqlParameter("@SERVICE_TERM_XML", SERVICE_TERM_XML)
    '                            param(8) = New SqlClient.SqlParameter("@STM_IDS", STM_ID_Save) 'STM_ID_Save
    '                            param(9) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
    '                            param(9).Direction = ParameterDirection.ReturnValue
    '                            param(10) = New SqlClient.SqlParameter("@MSG", SqlDbType.VarChar, 1000)
    '                            param(10).Direction = ParameterDirection.Output
    '                            SqlHelper.ExecuteNonQuery(sqltran, CommandType.StoredProcedure, "[ONLINE].[SAVE_SERVICES_REQUESTED_STUDENT]", param)
    '                            Dim ReturnFlag As Integer = param(9).Value


    '                            If ReturnFlag = 600 Then
    '                                Dim SSR_ID As String = param(10).Value
    '                                bSSRID = SSR_ID
    '                                lnkbtnProceed2Payment.PostBackUrl = "ServiceRequestPayment.aspx?SSRID=" + Encr_decrData.Encrypt(SSR_ID)

    '                                'Dim param2(7) As SqlClient.SqlParameter
    '                                'param2(0) = New SqlClient.SqlParameter("@SSR_ID", SSR_ID)
    '                                'param2(1) = New SqlClient.SqlParameter("@SRS_DISCOUNT", 0)
    '                                ''param(2) = New SqlClient.SqlParameter("@SRS_FROMDATE", txtfromdate.Text)
    '                                'param2(2) = New SqlClient.SqlParameter("@OPTION", 1)
    '                                'param2(3) = New SqlClient.SqlParameter("@MSG", SqlDbType.VarChar, 1000)
    '                                'param2(3).Direction = ParameterDirection.Output
    '                                'param2(4) = New SqlClient.SqlParameter("@SSR_REMARKS", "Auto-Approval")

    '                                'SqlHelper.ExecuteNonQuery(sqltran, CommandType.StoredProcedure, "[ONLINE].[SERVICES_APPROVE_SVC_REQUEST]", param2)
    '                                'lblerror.Text = param2(3).Value

    '                                'callSend_mail(hf_STU_ID.Value, SSR_ID, HF_SVC_ID.Value, sqltran, 4)
    '                            End If
    '                        End If
    '                        STM_ID_Save = ""
    '                        flag = 0
    '                    End If
    '                End If
    '            Next row
    '        Next
    '        'sqltran.Rollback()
    '        sqltran.Commit()
    '        'pnlProceed2Payment.Visible = True
    '        'lblProceed2Payment.Text = "You have registered successfully for selected " + lblLanguage.Text + "(s)</br> for student " + ddlStudents.SelectedItem.Text + " and are liable to pay the application fee."
    '        bsaved = 1

    '        BindStudentServices()
    '        Delete_ServicesRequested("", "", "0")
    '        lblerror.Text = "Service Saved Sucessfully...!!!!"

    '    Catch ex As Exception
    '        sqltran.Rollback()
    '        con.Close()
    '        lblerror.Text = ex.Message
    '    Finally
    '        ' BindStudentServices()
    '    End Try
    '    If bsaved = 1 And bSSRID <> "" Then
    '        Response.Redirect("ServiceRequestPayment.aspx?SSRID=" + Encr_decrData.Encrypt(bSSRID))
    '    End If
    'End Sub
    'Sub SaveServiceRequest()
    '    Dim constring As String = ConnectionManger.GetOASISConnectionString
    '    Dim con As SqlConnection = New SqlConnection(constring)
    '    con.Open()
    '    Dim sqltran As SqlTransaction
    '    sqltran = con.BeginTransaction("trans")
    '    Dim bsaved As Integer = 0
    '    Dim bSSRID As String = ""
    '    Try
    '        For Each gr As GridViewRow In gvServicesRequested.Rows  ' GridDataItem    Items 

    '            Dim HF_SVG_ID As HiddenField = DirectCast(gr.FindControl("HF_SVG_ID"), HiddenField)
    '            Dim hf_STU_ID As HiddenField = DirectCast(gr.FindControl("hf_STU_ID"), HiddenField)
    '            Dim hf_STM_ID As HiddenField = DirectCast(gr.FindControl("hf_STM_ID"), HiddenField)
    '            Dim HF_SVC_ID As HiddenField = DirectCast(gr.FindControl("HF_SVC_ID"), HiddenField)
    '            Dim HF_ACD_ID As HiddenField = DirectCast(gr.FindControl("HF_ACD_ID"), HiddenField)

    '            Dim ServicesRequested_DT1 As DataTable = ServicesRequested
    '            Dim dvApprovalLevels As DataView = ServicesRequested_DT1.DefaultView
    '            dvApprovalLevels.RowFilter = "STU_ID =" + hf_STU_ID.Value + " and  ACD_ID = " + HF_ACD_ID.Value + " and SVC_ID=" + HF_SVC_ID.Value + ""
    '            Dim ServicesRequested_DT As DataTable = dvApprovalLevels.ToTable()
    '            Dim SERVICE_TERM_XML As String = "<SERVICES>"
    '            Dim Terms_Service As String = ""

    '            For Each d As DataRow In ServicesRequested_DT.Rows
    '                If Terms_Service <> "" Then
    '                    Terms_Service = Terms_Service + "|" + d("ID").ToString()
    '                Else
    '                    Terms_Service = d("ID").ToString()
    '                End If
    '                SERVICE_TERM_XML = SERVICE_TERM_XML + "<TERM STM_ID='" + d("ID").ToString() + "' SRS_FEE='" + d("AMOUNT").ToString() + "' SRS_LEVEL='" + d("LEVEL").ToString() + "' SRS_TEACHER='" + d("CurrentTeacher").ToString() + "' SRS_OWN_INSTRUMENT='" + d("OWNINSTRUMENT").ToString() + "' STU_ID='" + d("STU_ID").ToString() + "' SVC_ID='" + d("SVC_ID").ToString() + "' ></TERM>"
    '            Next

    '            SERVICE_TERM_XML = SERVICE_TERM_XML + "</SERVICES>"

    '            Dim Terms_SVC As String() = New String(9) {}
    '            Terms_SVC = Terms_Service.Split("|")

    '            Dim param1(2) As SqlClient.SqlParameter
    '            param1(0) = New SqlClient.SqlParameter("@SSR_SVG_ID", HF_SVG_ID.Value)
    '            Dim ds As New DataSet
    '            ds = SqlHelper.ExecuteDataset(sqltran, CommandType.StoredProcedure, "[ONLINE].[SERVICES_GET_TRM_CONTINUES]", param1)
    '            Dim dt_TRM_CONTINUES As New DataTable
    '            dt_TRM_CONTINUES = ds.Tables(0)
    '            Dim flag As Integer = 0
    '            Dim j As Integer = 0
    '            Dim STM_ID_Save As String = ""
    '            Dim i As Integer = 0
    '            Dim failure As Boolean = False
    '            For Each row As DataRow In dt_TRM_CONTINUES.Rows
    '                i = i + 1
    '                If (j < Terms_SVC.Length) Then

    '                    If Terms_SVC(j) = row("STM_ID") Then

    '                        If STM_ID_Save <> "" Then
    '                            STM_ID_Save = STM_ID_Save + "|" + Terms_SVC(j).ToString()
    '                        Else
    '                            STM_ID_Save = Terms_SVC(j).ToString()
    '                        End If
    '                        flag = 0
    '                        j = j + 1
    '                    Else
    '                        flag = 1
    '                        j = j + 1

    '                    End If
    '                End If

    '                If flag = 1 Or i = dt_TRM_CONTINUES.Rows.Count Then

    '                    If STM_ID_Save <> "" Then
    '                        Dim lblAMOUNT As Label = DirectCast(gr.FindControl("lblAMOUNT"), Label)

    '                        Dim HF_Level As HiddenField = DirectCast(gr.FindControl("HF_Level"), HiddenField)
    '                        Dim HF_Teacher As HiddenField = DirectCast(gr.FindControl("HF_Teacher"), HiddenField)

    '                        Dim param(12) As SqlClient.SqlParameter
    '                        param(0) = New SqlClient.SqlParameter("@SSR_SVG_ID", HF_SVG_ID.Value)
    '                        param(1) = New SqlClient.SqlParameter("@SSR_STU_ID", hf_STU_ID.Value)
    '                        param(2) = New SqlClient.SqlParameter("@SRS_STM_ID", hf_STM_ID.Value)
    '                        param(3) = New SqlClient.SqlParameter("@SRS_FEE", lblAMOUNT.Text)
    '                        param(4) = New SqlClient.SqlParameter("@SRS_LEVEL", HF_Level.Value)
    '                        param(5) = New SqlClient.SqlParameter("@SRS_TEACHER", HF_Teacher.Value)
    '                        param(6) = New SqlClient.SqlParameter("@SRS_OWN_INSTRUMENT", "0")

    '                        param(7) = New SqlClient.SqlParameter("@SERVICE_TERM_XML", SERVICE_TERM_XML)
    '                        param(8) = New SqlClient.SqlParameter("@STM_IDS", STM_ID_Save) 'STM_ID_Save
    '                        param(9) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
    '                        param(9).Direction = ParameterDirection.ReturnValue
    '                        param(10) = New SqlClient.SqlParameter("@MSG", SqlDbType.VarChar, 1000)
    '                        param(10).Direction = ParameterDirection.Output
    '                        SqlHelper.ExecuteNonQuery(sqltran, CommandType.StoredProcedure, "[ONLINE].[SAVE_SERVICES_REQUESTED_STUDENT]", param)
    '                        Dim ReturnFlag As Integer = param(9).Value


    '                        If ReturnFlag = 600 Then
    '                            Dim SSR_ID As String = param(10).Value
    '                            bSSRID = SSR_ID
    '                            lnkbtnProceed2Payment.PostBackUrl = "ServiceRequestPayment.aspx?SSRID=" + Encr_decrData.Encrypt(SSR_ID)

    '                            'Dim param2(7) As SqlClient.SqlParameter
    '                            'param2(0) = New SqlClient.SqlParameter("@SSR_ID", SSR_ID)
    '                            'param2(1) = New SqlClient.SqlParameter("@SRS_DISCOUNT", 0)
    '                            ''param(2) = New SqlClient.SqlParameter("@SRS_FROMDATE", txtfromdate.Text)
    '                            'param2(2) = New SqlClient.SqlParameter("@OPTION", 1)
    '                            'param2(3) = New SqlClient.SqlParameter("@MSG", SqlDbType.VarChar, 1000)
    '                            'param2(3).Direction = ParameterDirection.Output
    '                            'param2(4) = New SqlClient.SqlParameter("@SSR_REMARKS", "Auto-Approval")

    '                            'SqlHelper.ExecuteNonQuery(sqltran, CommandType.StoredProcedure, "[ONLINE].[SERVICES_APPROVE_SVC_REQUEST]", param2)
    '                            'lblerror.Text = param2(3).Value

    '                            'callSend_mail(hf_STU_ID.Value, SSR_ID, HF_SVC_ID.Value, sqltran, 4)
    '                        End If
    '                    End If
    '                    STM_ID_Save = ""
    '                    flag = 0
    '                End If
    '            Next row
    '        Next
    '        ' sqltran.Rollback()
    '        sqltran.Commit()
    '        'pnlProceed2Payment.Visible = True
    '        'lblProceed2Payment.Text = "You have registered successfully for selected " + lblLanguage.Text + "(s)</br> for student " + ddlStudents.SelectedItem.Text + " and are liable to pay the application fee."
    '        bsaved = 1

    '        BindStudentServices()
    '        Delete_ServicesRequested("", "", "0")
    '        'lblerror.Text = "Service Saved Sucessfully...!!!!"

    '    Catch ex As Exception
    '        sqltran.Rollback()
    '        con.Close()
    '        lblerror.Text = ex.Message
    '    Finally
    '        ' BindStudentServices()
    '    End Try
    '    If bsaved = 1 And bSSRID <> "" Then
    '        Response.Redirect("ServiceRequestPayment.aspx?SSRID=" + Encr_decrData.Encrypt(bSSRID))
    '    End If
    'End Sub
    Protected Sub lnkbtnProceed2Payment_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkbtnProceed2Payment.Click

    End Sub

    Protected Sub lnkViewServices_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkViewServices.Click
        Dim Enc As New Encryption64
        Response.Redirect("ECARequestView.aspx?Stu_Id=" & Enc.Encrypt(Me.ddlStudent.SelectedValue) & "&Acd_Id=" & Enc.Encrypt(ddlGMAcademicYear.SelectedValue) & "&Grd_Id=" & Enc.Encrypt(Me.ddlGrade.SelectedValue) & "&Bsu_Id=" & Enc.Encrypt(Me.hfGMABsuId.Value))

    End Sub
    Public Sub BindStudentServices()
        Dim con As String = ConnectionManger.GetOASISConnectionString
        Dim param(4) As SqlClient.SqlParameter
        param(0) = New SqlClient.SqlParameter("@OLU_ID", Session("OLU_ID"))
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "[ONLINE].[SERVICES_GETSTUDENT_REQUESTED_SVC]", param)
        gvStudentServicesDetails.DataSource = ds.Tables(0)
        gvStudentServicesDetails.DataBind()

    End Sub

    Protected Sub img_delete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)

        Dim hf_STU_ID As HiddenField = sender.parent.findcontrol("hf_STU_ID")
        Dim hf_STM_ID As HiddenField = sender.parent.findcontrol("hf_STM_ID")

        Delete_ServicesRequested(hf_STM_ID.Value, hf_STU_ID.Value, "1")
    End Sub
    Sub Delete_ServicesRequested(ByVal STM_ID As String, ByVal STU_ID As String, ByVal flag As String)
        Dim ServicesRequested_DT As DataTable = ServicesRequested
        Dim dvApprovalLevels As DataView = ServicesRequested_DT.DefaultView
        If flag = "0" Then
            dvApprovalLevels.RowFilter = "ID = 0"
        Else
            dvApprovalLevels.RowFilter = "ID <>" + STM_ID + " or STU_ID <> " + STU_ID + ""
        End If

        ServicesRequested_DT = dvApprovalLevels.ToTable()
        ServicesRequested_DT.AcceptChanges()
        ServicesRequested = ServicesRequested_DT


        gvServicesRequested.DataSource = ServicesRequested_DT
        gvServicesRequested.DataBind()
        bindTerms()
        enableButton()
    End Sub
    Sub callSend_mail(ByVal STU_ID As String, ByVal SSR_ID As String, ByVal lblSERVICE As String, ByVal sqltran As SqlTransaction, ByVal status As String)

        Dim htmlString As String = String.Empty
        Dim html As String = ScreenScrapeHtml(Server.MapPath("EmailContent\emailTemplate.htm"))
        Dim msg As New MailMessage

        Dim BSC_HOST As String = String.Empty
        Dim BSC_USERNAME As String = String.Empty
        Dim BSC_PASSWORD As String = String.Empty
        Dim SUBJECT As String = String.Empty
        Dim EMAIL_ADD As String = String.Empty
        Dim BSU_ID As String = String.Empty
        Dim ACD_ID As String = String.Empty


        Dim STUDENTNAME As String = String.Empty
        Dim STUDENTNO As String = String.Empty
        Dim SERVICE As String = String.Empty
        Dim BSU_NAME As String = String.Empty
        Dim SERVICE_CAT As String = String.Empty
        'html = html.Replace("@Content", htmlString)

        Dim pParms(4) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@STU_ID", STU_ID)
        pParms(2) = New SqlClient.SqlParameter("@SSR_ID", SSR_ID)
        pParms(1) = New SqlClient.SqlParameter("@OPTION", status)

        Using EmailHost_reader As SqlDataReader = SqlHelper.ExecuteReader(sqltran, CommandType.StoredProcedure, "[ONLINE].[SERVICES_EMAIL_TEXT]", pParms)
            If EmailHost_reader.HasRows Then
                While EmailHost_reader.Read
                    SUBJECT = Convert.ToString(EmailHost_reader("SUBJECT"))
                    SUBJECT = SUBJECT.Replace("@servicecategory", Convert.ToString(EmailHost_reader("SERVICE_CAT")))
                    SUBJECT = SUBJECT.Replace("@service", Convert.ToString(EmailHost_reader("SERVICE")))


                    htmlString = Convert.ToString(EmailHost_reader("EMAIL_MESSAGE"))
                    html = html.Replace("@Content", htmlString)
                    BSU_ID = Convert.ToString(EmailHost_reader("BSU_ID"))
                    html = html.Replace("@school", Convert.ToString(EmailHost_reader("BSU_NAME")))
                    html = html.Replace("@service", Convert.ToString(EmailHost_reader("SERVICE")))
                    html = html.Replace("@name ", Convert.ToString(EmailHost_reader("STUDENTNAME")))
                    html = html.Replace("@studentno ", Convert.ToString(EmailHost_reader("STUDENTNO")))


                    SERVICE_CAT = Convert.ToString(EmailHost_reader("SERVICE_CAT"))
                    STUDENTNO = Convert.ToString(EmailHost_reader("STUDENTNO"))
                    STUDENTNAME = Convert.ToString(EmailHost_reader("STUDENTNAME"))
                    SERVICE = Convert.ToString(EmailHost_reader("SERVICE"))
                    BSU_NAME = Convert.ToString(EmailHost_reader("BSU_NAME"))

                    EMAIL_ADD = Convert.ToString(EmailHost_reader("EMAIL_ID"))
                    ACD_ID = Convert.ToString(EmailHost_reader("ACD_ID"))
                End While
            End If
        End Using

        Dim param(9) As SqlClient.SqlParameter
        param(0) = New SqlClient.SqlParameter("@EML_BSU_ID", BSU_ID)
        param(1) = New SqlClient.SqlParameter("@EML_TYPE", IIf(status = "1", "SERVICE-A", "SERVICE-R"))
        param(2) = New SqlClient.SqlParameter("@EML_PROFILE_ID", SSR_ID)
        param(3) = New SqlClient.SqlParameter("@EML_TOEMAIL", EMAIL_ADD) 'EMAIL_ADD)
        param(4) = New SqlClient.SqlParameter("@EML_SUBJECT", SUBJECT)
        param(5) = New SqlClient.SqlParameter("@EML_MESSAGE", html)
        SqlHelper.ExecuteNonQuery(sqltran, CommandType.StoredProcedure, "dbo.InsertIntoEmailSendSchedule", param)



        html = ScreenScrapeHtml(Server.MapPath("EmailContent\emailTemplate.htm"))
        Dim param1(9) As SqlClient.SqlParameter
        param1(0) = New SqlClient.SqlParameter("@SBA_BSU_ID", BSU_ID)
        param1(1) = New SqlClient.SqlParameter("@SBA_ACD_ID", ACD_ID)
        param1(2) = New SqlClient.SqlParameter("@SBA_SVC_ID", lblSERVICE)
        param1(3) = New SqlClient.SqlParameter("@OPTION", status)
        Dim staI As Integer = 0
        EMAIL_ADD = ""
        Using EmailHost_reader As SqlDataReader = SqlHelper.ExecuteReader(sqltran, CommandType.StoredProcedure, "[ONLINE].[SERVICES_APPROVES_EMAIL]", param1)
            If EmailHost_reader.HasRows Then
                While EmailHost_reader.Read
                    SUBJECT = Convert.ToString(EmailHost_reader("SUBJECT"))
                    SUBJECT = SUBJECT.Replace("@servicecategory", SERVICE_CAT)
                    SUBJECT = SUBJECT.Replace("@service", SERVICE)

                    htmlString = Convert.ToString(EmailHost_reader("EMAIL_MESSAGE"))
                    html = html.Replace("@Content", htmlString)
                    html = html.Replace("@school", BSU_NAME)
                    html = html.Replace("@service", SERVICE)
                    html = html.Replace("@name ", STUDENTNAME)
                    html = html.Replace("@studentno ", STUDENTNO)
                    EMAIL_ADD = Convert.ToString(EmailHost_reader("EMD_EMAIL")) + ";" + EMAIL_ADD
                    staI = staI + 1
                End While
            End If
        End Using

        If (staI > 0) Then
            Dim EMAIL_ADD_TO As String() = New String(9) {}
            EMAIL_ADD_TO = EMAIL_ADD.Split(";")
            For Each toEmail As String In EMAIL_ADD_TO
                If (toEmail <> "") Then
                    param(0) = New SqlClient.SqlParameter("@EML_BSU_ID", BSU_ID)
                    param(1) = New SqlClient.SqlParameter("@EML_TYPE", IIf(status = "1", "SERVICE-A", "SERVICE-R"))
                    param(2) = New SqlClient.SqlParameter("@EML_PROFILE_ID", SSR_ID)
                    param(3) = New SqlClient.SqlParameter("@EML_TOEMAIL", toEmail) 'EMAIL_ADD)
                    param(4) = New SqlClient.SqlParameter("@EML_SUBJECT", SUBJECT)
                    param(5) = New SqlClient.SqlParameter("@EML_MESSAGE", html)
                    SqlHelper.ExecuteNonQuery(sqltran, CommandType.StoredProcedure, "dbo.InsertIntoEmailSendSchedule", param)
                End If
            Next
        End If
    End Sub
    Public Shared Function ScreenScrapeHtml(ByVal url As String) As String
        Dim objRequest As System.Net.WebRequest = System.Net.HttpWebRequest.Create(url)
        Dim sr As New StreamReader(objRequest.GetResponse().GetResponseStream())
        Dim result As String = sr.ReadToEnd()
        sr.Close()
        Return result
    End Function

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Dim dt As DataTable = Session("ECARequest_dtActivity")
        Dim tmpStr, tmpstr1 As String
        'Start validations
        'If Not ValidateData() Then Exit Sub
        If Not ValidateData2() Then Exit Sub

        'Add user selected activity to the grid
        Dim chk As CheckBox, lbl As String
        Dim row As DataRow = dt.NewRow
        row.Item("Category") = Me.ddlCategory.SelectedItem.Text
        row.Item("CategoryId") = Me.ddlCategory.SelectedValue
        row.Item("SubCategory") = Me.ddlSubCategoryMixed.SelectedItem.Text.Split("[")(0)
        'For Each item As ListItem In chkPeriods.Items
        '    If item.Selected = True Then
        '        tmpStr += item.Text.Split("(")(0) & " & "
        '        tmpstr1 += item.Value & "|"
        '    End If
        'Next
        For Each row1 As GridViewRow In Me.gvDuration.Rows
            chk = CType(row1.FindControl("chkDuration"), CheckBox)
            If chk.Checked Then
                tmpStr += CType(row1.FindControl("lblFromDate"), Label).Text & " to " & CType(row1.FindControl("lblToDate"), Label).Text & " & "
                tmpstr1 += CType(row1.FindControl("lblTermId"), Label).Text & "|"
            End If
        Next

        If Not tmpStr Is Nothing AndAlso tmpStr.EndsWith(" & ") Then
            tmpStr = tmpStr.Substring(0, tmpStr.Length - 3)
        End If
        If Not tmpstr1 Is Nothing AndAlso tmpstr1.EndsWith("|") Then
            tmpstr1 = tmpstr1.Substring(0, tmpstr1.Length - 1)
        End If
        tmpStr = tmpStr.Substring(0, 11) & " to " & tmpStr.Substring(tmpStr.Length - 11, 11)
        row.Item("Duration") = tmpStr
        row.Item("TermIds") = tmpstr1
        row.Item("SVGID") = Me.ddlSubCategoryMixed.SelectedValue
        dt.Rows.Add(row)
        dt.AcceptChanges()
        Session("ECARequest_dtActivity") = dt

        Me.gvActivity.DataSource = dt
        Me.gvActivity.DataBind()


    End Sub
    Protected Sub gvActivity_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvActivity.RowDeleting
        Dim lblId As String = CType(Me.gvActivity.Rows(e.RowIndex).FindControl("lblSVGId"), Label).Text
        Dim dt As DataTable = Session("ECARequest_dtActivity")
        Dim row() As DataRow = dt.Select("SVGID = " & lblId)
        If row.Length > 0 Then
            dt.Rows.Remove(row(0))
            dt.AcceptChanges()
            Session("ECARequest_dtActivity") = dt
            Me.gvActivity.DataSource = dt
            Me.gvActivity.DataBind()
        End If
        Me.lblerror.Text = Nothing
    End Sub


    Private Function ValidateData2() As Boolean
        ValidateData2 = False

        Try

            'Adding same activity again
            Dim tmpDT2 As DataTable = CType(Session("ECARequest_dtActivity"), DataTable)
            If tmpDT2.Select("CategoryId = " & Me.ddlCategory.SelectedValue & " And SVGID = " & Me.ddlSubCategoryMixed.SelectedValue).Length >= 1 Then
                lblerror.Text = "You have already added the selected activity. Kindly add some other activity"
                Return False
            End If
            Dim activity_count As Integer = 0

            'activity_count = SqlHelper.ExecuteScalar(ConnectionManger.GetOASIS_SERVICESConnectionString, CommandType.Text, "Select Count(Distinct SSA_ID) From Student_Service_Application Where ssa_stu_id = " & Session("stu_id") & " and ssa_grd_id = '" & Session("stu_grd_id") & "' and ssa_acd_id = " & Me.ddlAcademicYear.SelectedValue & " and ssa_ssc_id = " & Me.ddlCategory.SelectedValue & " and ssa_svg_id = " & Me.ddlSubCategoryMixed.SelectedValue & " and (ssa_appr_flag <> 'R' or ssa_appr_flag is null)")
            activity_count = SqlHelper.ExecuteScalar(ConnectionManger.GetOASIS_SERVICESConnectionString, CommandType.Text, "Select Count(Distinct SSR_ID) From Student_Services_Request INNER JOIN dbo.SERVICES_SYS_M ON dbo.STUDENT_SERVICES_REQUEST.SSR_SVC_ID = dbo.SERVICES_SYS_M.SVC_ID INNER JOIN dbo.SERVICES_CATEGORY ON dbo.SERVICES_SYS_M.SVC_SSC_ID = dbo.SERVICES_CATEGORY.SSC_ID Where ssr_stu_id = " & Me.ddlStudent.SelectedValue & " and ssr_acd_id = " & Me.ddlGMAcademicYear.SelectedValue & " and dbo.SERVICES_CATEGORY.SSC_ID = " & Me.ddlCategory.SelectedValue & " and ssr_svg_id = " & Me.ddlSubCategoryMixed.SelectedValue & " and (ssr_appr_flag <> 'R' or ssr_appr_flag = 'P' or ssr_appr_flag is null)")
            If activity_count >= 1 Then
                lblerror.Text = "You have already requested this activity in your previous request. Kindly try some other activity if you wish to"
                Return False
            End If

            'Empty Duration(s)
            'If Me.chkPeriods.Items.Count > 0 Then
            '    If Me.chkPeriods.SelectedItem Is Nothing Then
            '        Me.lblerror.Text = "Kindly select atleast one duration for the selected activity"
            '        Return False
            '    End If
            'End If

            Dim chk As CheckBox, CheckCount As Integer
            CheckCount = 0
            For Each row As GridViewRow In Me.gvDuration.Rows
                chk = CType(row.FindControl("chkDuration"), CheckBox)
                If chk.Checked Then
                    CheckCount += 1
                End If
            Next

            If CheckCount <= 0 Then
                Me.lblerror.Text = "Kindly select atleast one duration for the selected activity"
                Return False
            End If

            Dim PrevRow, CurrRow As Integer
            Dim FirstRowChecked As Boolean
            If Me.gvDuration.Rows.Count > 2 Then
                For Each row As GridViewRow In Me.gvDuration.Rows
                    chk = CType(row.FindControl("chkDuration"), CheckBox)
                    If chk.Checked Then
                        PrevRow = CurrRow
                        CurrRow = row.RowIndex
                        If CurrRow = 0 Then FirstRowChecked = True
                    End If
                    If (PrevRow <> 0 Or FirstRowChecked) And (CurrRow - PrevRow) > 1 Then
                        'If CurrRow - PrevRow > 1 Then
                        Me.lblerror.Text = "Skipping the term in not allowed. Kindly select consecutive terms if you are interested in multiple terms."
                        Return False
                    End If
                Next
            End If

            ''Applicable Groups
            'If Me.hfGender.Value = 1 Then 'Only male
            '    If Me.hfStudentGender.Value <> "M" Then
            '        lblerror.Text = "Selected activity is available only for male students"
            '        Return False
            '    End If
            'ElseIf Me.hfGender.Value = 2 Then 'Only female
            '    If Me.hfStudentGender.Value <> "F" Then
            '        lblerror.Text = "Selected activity is available only for female students"
            '        Return False
            '    End If
            'End If

            'Max Category
            ' ''If Not lblMaxCat.Text = Nothing Or Not Val(lblMaxCat.Text) = 0 Then
            ' ''    Dim tmpDT As DataTable = CType(Session("ECARequest_dtActivity"), DataTable)
            ' ''    If tmpDT.DefaultView.ToTable(True, New String() {"Category"}).Rows.Count >= Val(lblMaxCat.Text) Then
            ' ''        lblerror.Text = "Your student grade is allowed only max of " & Convert.ToInt32(lblMaxCat.Text) & " different categories"
            ' ''        Return False
            ' ''    End If
            ' ''    Dim stu_req_category_count As Integer = 0
            ' ''    'stu_req_category_count = SqlHelper.ExecuteScalar(ConnectionManger.GetOASIS_SERVICESConnectionString, CommandType.Text, "Select Count(Distinct SSA_SSC_ID) From Student_Service_Application Where ssa_stu_id = " & Session("stu_id") & " and ssa_grd_id = '" & Session("stu_grd_id") & "' and ssa_acd_id = " & Me.ddlAcademicYear.SelectedValue & " and (ssa_appr_flag <> 'R' or ssa_appr_flag is null)")
            ' ''    stu_req_category_count = SqlHelper.ExecuteScalar(ConnectionManger.GetOASIS_SERVICESConnectionString, CommandType.Text, "Select Count(Distinct SSR_ID) From Student_Services_Request INNER JOIN dbo.SERVICES_SYS_M ON dbo.STUDENT_SERVICES_REQUEST.SSR_SVC_ID = dbo.SERVICES_SYS_M.SVC_ID INNER JOIN dbo.SERVICES_CATEGORY ON dbo.SERVICES_SYS_M.SVC_SSC_ID = dbo.SERVICES_CATEGORY.SSC_ID Where ssr_stu_id = " & Me.ddlStudent.SelectedValue & " and ssr_acd_id = " & Me.ddlAcademicYear.SelectedValue & " and (ssr_appr_flag <> 'R' or ssr_appr_flag = 'P' or ssr_appr_flag is null)")
            ' ''    If stu_req_category_count >= Convert.ToInt32(lblMaxCat.Text) Then
            ' ''        lblerror.Text = "You have already requested similar/other category in your previous request. Your student grade is allowed only max of " & Convert.ToInt32(lblMaxCat.Text) & " different categories"
            ' ''        Return False
            ' ''    End If
            ' ''End If

            'Max Sub Category
            If Not lblMaxSubCat.Text = Nothing Or Not Val(lblMaxSubCat.Text) = 0 Then
                Dim tmpDT1 As DataTable = CType(Session("ECARequest_dtActivity"), DataTable)
                If tmpDT1.Select("CategoryId = " & Me.ddlCategory.SelectedValue).Length >= Convert.ToInt32(lblMaxSubCat.Text) Then
                    lblerror.Text = "Only " & Convert.ToInt32(lblMaxSubCat.Text) & " different sub category are allowed for the selected category"
                    Return False
                End If
                Dim stu_req_sub_category_count As Integer = 0
                'stu_req_sub_category_count = SqlHelper.ExecuteScalar(ConnectionManger.GetOASIS_SERVICESConnectionString, CommandType.Text, "Select Count(Distinct SSA_SVG_ID) From Student_Service_Application Where ssa_stu_id = " & Session("stu_id") & " and ssa_grd_id = '" & Session("stu_grd_id") & "' and ssa_acd_id = " & Me.ddlAcademicYear.SelectedValue & " and ssa_ssc_id = " & Me.ddlCategory.SelectedValue & " and (ssa_appr_flag <> 'R' or ssa_appr_flag is null)")
                stu_req_sub_category_count = SqlHelper.ExecuteScalar(ConnectionManger.GetOASIS_SERVICESConnectionString, CommandType.Text, "Select Count(Distinct SSR_SVG_ID) From Student_Services_Request INNER JOIN dbo.SERVICES_SYS_M ON dbo.STUDENT_SERVICES_REQUEST.SSR_SVC_ID = dbo.SERVICES_SYS_M.SVC_ID INNER JOIN dbo.SERVICES_CATEGORY ON dbo.SERVICES_SYS_M.SVC_SSC_ID = dbo.SERVICES_CATEGORY.SSC_ID Where ssr_stu_id = " & Me.ddlStudent.SelectedValue & " and ssr_acd_id = " & Me.ddlGMAcademicYear.SelectedValue & " and SERVICES_CATEGORY.SSC_ID = " & Me.ddlCategory.SelectedValue & " and (ssr_appr_flag <> 'R' or ssr_appr_flag = 'P' or ssr_appr_flag is null)")
                If stu_req_sub_category_count >= Convert.ToInt32(lblMaxCat.Text) Then
                    lblerror.Text = "You have already requested similar/other sub category in your previous request. Your student grade is allowed only max of " & Convert.ToInt32(lblMaxCat.Text) & " different sub category for the selected category"
                    Return False
                End If
            End If

            'Max Seat Capacity
            Dim taken_seat As Integer = 0
            'taken_seat = SqlHelper.ExecuteScalar(ConnectionManger.GetOASIS_SERVICESConnectionString, CommandType.Text, "Select Count(Distinct SSA_SVG_ID) From Student_Service_Application Where ssa_svg_id = " & Me.ddlSubCategoryMixed.SelectedValue & " and ssa_appr_flag = 'A'")
            taken_seat = SqlHelper.ExecuteScalar(ConnectionManger.GetOASIS_SERVICESConnectionString, CommandType.Text, "Select Count(Distinct SSR_SVG_ID) From Student_Services_Request Where ssr_svg_id = " & Me.ddlSubCategoryMixed.SelectedValue & " and ssr_appr_flag = 'A'")
            If taken_seat >= Convert.ToInt32(lblSeatCap.Text) Then
                lblerror.Text = "All the seats are already occupied for the selected activity. Kindly try some other activity if you wish to"
                Return False
            End If

            ''Mandatory category
            'Dim mandFlag As String
            'Dim dt As DataTable
            'For Each item As ListItem In Me.ddlSubCategoryMixed.Items
            '    mandFlag = SqlHelper.ExecuteScalar(ConnectionManger.GetOASIS_SERVICESConnectionString, CommandType.Text, "SELECT CASE WHEN svg.SVG_MANDATORY_MAIN IS NULL THEN '0' WHEN SVG_MANDATORY_MAIN = '1' THEN '1' ELSE '0' END AS MandFlag FROM dbo.SERVICES_GRD_M SVG WHERE SVG.SVG_ID = " & item.Value)
            '    If mandFlag = "1" Then
            '        dt = Session("ECARequest_dtActivity")
            '        If dt.Select("SVGID=" & item.Value).Length <= 0 Then
            '            lblerror.Text = item.Text & "is a mandatory activity and needs to be added to the request"
            '            Return False
            '        End If
            '    End If
            'Next

            Me.lblerror.Text = Nothing
            Return True
        Catch ex As Exception
            lblerror.Text = "There was an issue validating the data. Please try again"
            Return False
        End Try


    End Function

    Protected Sub btnSaveData1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveData1.Click
        Me.SaveData2()
    End Sub

    Private Function ValidateMandatoryActivity() As Boolean
        ValidateMandatoryActivity = False
        'trError.Visible = True
        Try
            'Mandatory category
            If ddlGMASVC.SelectedValue = 63 AndAlso ddlAdditionalLessons.Items.Count > 1 AndAlso ddlAdditionalLessons.SelectedValue = 0 Then
                lblErrorMsg.Text = "Please select an instrument or service requires Additional Lessons"
                ValidateMandatoryActivity = False
            Else
                ValidateMandatoryActivity = True
                'trError.Visible = False
            End If
        Catch ex As Exception
            lblErrorMsg.Text = "There was an issue validating the data. Please try again"
            Return False
        End Try
    End Function

    Private Sub SaveData2()

        If Not ValidateMandatoryActivity() Then Exit Sub

        Dim transaction As SqlTransaction

        'Dim tmpDT As DataTable = CType(Session("ECARequest_dtActivity"), DataTable)

        'If tmpDT.Rows.Count <= 0 Then
        '    Me.lblerror.Text = "Kindly select and add activities before trying to save the request"
        '    Exit Sub
        'End If

        Using conn As SqlConnection = ConnectionManger.GetOASIS_SERVICESConnection

            transaction = conn.BeginTransaction("SampleTransaction")
            Try

                ''added by nahyan on 13nov2013--commented 
                Dim trm_ids As String = String.Empty
                Dim status As Integer
                Dim pParms(16) As SqlClient.SqlParameter

                For Each item As ListItem In chkPeriods2.Items
                    If item.Selected = True Then
                        trm_ids += item.Value.ToString & "|"
                    End If

                Next

                pParms(0) = New SqlClient.SqlParameter("@ACD_ID", Me.ddlGMAcademicYear.SelectedValue)
                pParms(1) = New SqlClient.SqlParameter("@GRD_ID", 0)
                pParms(2) = New SqlClient.SqlParameter("@USR_ID", Session("sUsr_id"))
                pParms(3) = New SqlClient.SqlParameter("@STU_IDS", Me.ddlStudent.SelectedValue + "^|")
                pParms(4) = New SqlClient.SqlParameter("@BSU_ID", Me.hfGMABsuId.Value)
                pParms(5) = New SqlClient.SqlParameter("@SVC_ID", ddlGMASVC.SelectedValue)
                pParms(6) = New SqlClient.SqlParameter("@STM_IDS", trm_ids)

                pParms(7) = New SqlClient.SqlParameter("@FromDate", Convert.ToDateTime(txtFrom.Value))
                pParms(8) = New SqlClient.SqlParameter("@ToDate", Convert.ToDateTime(txtTo.Value))
                pParms(11) = New SqlClient.SqlParameter("@bOnline", True)
                pParms(12) = New SqlClient.SqlParameter("@sLevel", ddlLevel.SelectedValue)

                pParms(13) = New SqlClient.SqlParameter("@IsActive", chkApproval.Checked)
                pParms(14) = New SqlClient.SqlParameter("@SSR_ID", ViewState("ssr_id"))
                pParms(15) = New SqlClient.SqlParameter("@SSR_ADDITIONAL_LESSONS", GetSelectedAdditionalLesson)
                pParms(9) = New SqlClient.SqlParameter("@MSG_OUT", SqlDbType.VarChar, 500)
                pParms(9).Direction = ParameterDirection.Output

                pParms(10) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                pParms(10).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "ST.SAVESTUD_GMA_SERVICE_REQUEST", pParms)
                status = pParms(10).Value

                If pParms(9).Value.ToString <> "" Then
                    lblerror.Text = pParms(9).Value.ToString
                    status = -1

                End If

                ''nahyan code ends  



                'Dim status As Integer
                'Dim pParms(9) As SqlClient.SqlParameter
                'pParms(0) = New SqlClient.SqlParameter("@acd_ID", SqlDbType.BigInt)
                'pParms(1) = New SqlClient.SqlParameter("@Grd_ID", SqlDbType.BigInt)
                'pParms(2) = New SqlClient.SqlParameter("@usr_ID", SqlDbType.VarChar)
                'pParms(3) = New SqlClient.SqlParameter("@stu_IDs", SqlDbType.VarChar)
                'pParms(4) = New SqlClient.SqlParameter("@bsu_ID", SqlDbType.VarChar)
                'pParms(5) = New SqlClient.SqlParameter("@SVG_ID", SqlDbType.Int)
                'pParms(6) = New SqlClient.SqlParameter("@STM_IDS", SqlDbType.VarChar)
                'pParms(7) = New SqlClient.SqlParameter("@Msg_Out", SqlDbType.VarChar)
                'pParms(8) = New SqlClient.SqlParameter("@bOnline", SqlDbType.Bit)

                'For Each row As DataRow In tmpDT.Rows
                '    pParms(0).Value = Me.ddlGMAcademicYear.SelectedValue  'Session("stu_acd_id")
                '    pParms(1).Value = Me.ddlGrade.SelectedValue  'Session("stu_acd_id")
                '    pParms(2).Value = ""
                '    pParms(3).Value = Me.ddlStudent.SelectedValue & "^|"
                '    pParms(4).Value = Me.hfGMABsuId.Value  'Session("sBsuid")
                '    pParms(5).Value = Me.ddlSubCategoryMixed.SelectedValue
                '    pParms(6).Value = row.Item("TermIds")
                '    pParms(8).Value = "True"
                '    SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "ST.SAVESTUD_SERVICE_REQUEST", pParms)
                'Next

                'Send the email to the parent
                'Dim pParms1(3) As SqlClient.SqlParameter
                'pParms1(0) = New SqlClient.SqlParameter("@bsu_ID", Me.hfGMABsuId.Value) 'New SqlClient.SqlParameter("@bsu_ID", Session("sBsuid"))
                'pParms1(1) = New SqlClient.SqlParameter("@acd_ID", Me.ddlGMAcademicYear.SelectedValue) 'New SqlClient.SqlParameter("@acd_ID", Me.ddlAcademicYear.SelectedValue)
                'pParms1(2) = New SqlClient.SqlParameter("@stu_ID", Me.ddlStudent.SelectedValue)
                'pParms1(3) = New SqlClient.SqlParameter("@olu_ID", Session("OLU_ID"))
                'SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "SEND_ECA_PARENT_SERVICE_REQUEST_EMAIL", pParms1)
                Dim pEmail As String = String.Empty
                pEmail = getParentEmailAddress()
                If status = 0 Then
                    transaction.Commit()
                ElseIf status = -1 Then
                    transaction.Rollback()
                End If

                Dim dt As DataTable = CType(Session("ECARequest_dtActivity"), DataTable)
                dt.Rows.Clear()
                Session("ECARequest_dtActivity") = dt
                Me.gvActivity.DataSource = dt
                Me.gvActivity.DataBind()
                ' Me.lblerror.Text = "Your request has been saved successfully"


                pEmail = getParentEmailAddress()

                If chkApproval.Checked Then
                    lblPEmail.Text = " Thank you for submitting a service request for GEMS Music Academy lessons. Please check your email (" & pEmail & " ) for information regarding scheduling and payment. "
                Else
                    lblPEmail.Text = " Your Request has been saved as draft. You can change/amend the details and send for approval later ."
                End If

                Dim Encr As New Encryption64
                Dim stat = Encr.Encrypt("1")
                '' Response.Redirect("ViewStudentServices.aspx?suc=" + stat, False)
                If status = 0 Then
                    ScriptManager.RegisterClientScriptBlock(Page, Me.[GetType](), "CallJS", "SaveRequestDialog();", True)
                Else
                    'trError.Visible = True
                    Me.lblErrorMsg.Text = lblerror.Text
                    chkAccepted.Checked = False


                End If

                'btnSaveData1.Attributes.Add(" OnClientClick", "SaveRequestDialog()")
            Catch ex As Exception
                transaction.Rollback()
                UtilityObj.Errorlog(ex.Message)
                Me.lblerror.Text = "There was an issue saving your request"
            Finally

            End Try

        End Using
    End Sub

    Function GetSelectedAdditionalLesson() As String
        GetSelectedAdditionalLesson = ""
        If ddlGMASVC.SelectedValue = 63 AndAlso ddlAdditionalLessons.Items.Count > 1 Then
            GetSelectedAdditionalLesson = ddlAdditionalLessons.SelectedItem.Text
        End If
    End Function

    Protected Sub btnView0_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnView0.Click
        Dim Enc As New Encryption64
        Response.Redirect("ECARequestView.aspx?Stu_Id=" & Enc.Encrypt(Me.ddlStudent.SelectedValue) & "&Acd_Id=" & Enc.Encrypt(ddlGMAcademicYear.SelectedValue) & "&Grd_Id=" & Enc.Encrypt(Me.ddlGrade.SelectedValue) & "&Bsu_Id=" & Enc.Encrypt(Me.hfGMABsuId.Value))
    End Sub

    Protected Sub btnReset0_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset0.Click
        If Not Session("ECARequest_dtActivity") Is Nothing Then
            Dim dt As DataTable = CType(Session("ECARequest_dtActivity"), DataTable)
            dt.Rows.Clear()
            Session("ECARequest_dtActivity") = dt
        End If
        ddlGMASVC.SelectedValue = 0
        getGMASVC_color()
        Me.gvActivity.DataSource = Nothing
        Me.gvActivity.DataBind()
        If Me.ddlStudent.Items.Count > 1 Then
            'Me.lblStudentName.Visible = False
            Me.ddlStudent.Visible = True
        End If
        Me.lblerror.Text = Nothing
    End Sub
End Class

