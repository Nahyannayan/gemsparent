<%@ Page Language="VB" MasterPageFile="~/ParentMaster.master" AutoEventWireup="false"
    CodeFile="ViewStudentServices.aspx.vb" Inherits="Service_ViewStudentServices"
    Title="GEMS Education" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphParent" runat="Server">

    <script src="../Scripts/jquery-1.9.1.js" type="text/javascript"></script>

    <link rel="stylesheet" type="text/css" href="../Scripts/jQuery-ui-1.10.3.css" />

    <script type="text/javascript" src="../Scripts/jquery-ui.js"></script>

    <script type="text/javascript">
        function pageLoad() {

            $(function() {

                $(".datepicker").datepicker({
                    dateFormat: 'dd/M/yy'

                });
            }
    );
        }
        
    </script>

    <style type="text/css">
        .imgstyle
        {
            height: 15px;
            width: 15px;
        }
        .currMnuLinks
        {
            color: #10B3EA !important;
            font-weight: bold !important;
            font-size: 12px !important;
        }
    </style>
    <%--       <telerik:RadComboBox ID="ddlLevels" runat="server" EmptyMessage="<--Select One-->">
                            <Items>
                                <telerik:RadComboBoxItem runat="server" Text="<--Select One-->" Value="" />
                                <telerik:RadComboBoxItem runat="server" Text="Beginner" Value="1" />
                                <telerik:RadComboBoxItem runat="server" Text="Intermediate" Value="2" />
                                <telerik:RadComboBoxItem runat="server" Text="Advanced" Value="3" />
                            </Items>
                        </telerik:RadComboBox>
            <asp:Button ID="Button1" runat="server" Text="Button" />--%>
    

    
                <!-- Posts Block -->
    <div class="content margin-top30 margin-bottom60">
        <div class="container">
            <div class="row">

                <!-- Posts Block -->
                <div class="posts-block col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="my-account">
                     
           
                        <div>
                             <div >
                                <asp:LinkButton ID="lnkEnroll" runat="server" 
        PostBackUrl="~/Service/ServiceRequestMA.aspx"> Click here to enroll to GEMS Music Academy</asp:LinkButton><%--CssClass="currMnuLinks" Width="340px"--%>

                                    </div>
                               <div class="mainheading" style="display:none;">
                            <div>Services</div>
                           
                        </div>


                            <div class="title-box">
                            <h3> Services
                            <span class="profile-right">
                               <asp:label ID="lbChildName" runat="server" CssClass="lblChildNameCss"></asp:label><br />
                                     <asp:LinkButton id="lnkPayhist" forecolor="blue" PostBackUrl="~/Service/ServiceFeePaymentHistory.aspx"  runat="server">Payment History</asp:LinkButton>
                            </span></h3>
                        </div>
                            <div align="left">


    <table class="table table-striped table-bordered table-responsive text-left my-orders-table" width="100%" style="border-collapse:inherit !important;" >
      <%--  <tr>
            <td align="center" style="height: 25px">
                <div class="mainheading">
                    <div class="left">
                        Services</div>
                </div>
            </td>
        </tr>--%>
        <tr>
            <td class="" style="width: 90%">
                <asp:GridView ID="gvStudentServicesDetails" runat="server" AutoGenerateColumns="False" style="border-collapse:inherit !important;"
                    CssClass="table table-striped table-bordered table-responsive text-left my-orders-table" Width="100%" ShowFooter="false" AllowPaging="true" PageSize="25">
                    <Columns>
                        <%--<asp:TemplateField HeaderText="Name" ItemStyle-HorizontalAlign="Left"  >
                              <ItemTemplate>
                                    <asp:Label ID="lblStu_Name" runat="server" Text='<%# Bind("STU_Name") %>' ></asp:Label>
                                </ItemTemplate> 
                           </asp:TemplateField>--%>
                        <asp:TemplateField HeaderText="Requested Date" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Label ID="lblRequestedDate" runat="server" Text='<%# Bind("SSR_REQUESTED_DT") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Academic year" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Label ID="lblACADEMIC_YEAR" runat="server" Text='<%# Bind("ACADEMIC_YEAR") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Service" ItemStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="lblSERVICE" runat="server" Text='<%# Bind("SERVICE") %>'></asp:Label>
                                <asp:HiddenField ID="HF_SVC_ID" runat="server" Value='<%# Bind("SVC_ID") %>' />
                                <asp:HiddenField ID="HF_SSR_ID" runat="server" Value='<%# Bind("SSR_ID") %>' />
                                <asp:HiddenField ID="hf_SSV_ID" runat="server" Value='<%# Bind("SSV_ID") %>' />
                                <asp:HiddenField ID="HF_Stu_Name" runat="server" Value='<%# Bind("STU_Name") %>' />
                                <asp:HiddenField ID="HF_link" runat="server" Value='<%# Bind("link") %>' />
                                <asp:HiddenField ID="HF_DISCONTINUED" runat="server" Value='<%# Bind("DISCONTINUED") %>' />
                                <asp:HiddenField ID="HF_SSR_bPaid" runat="server" Value='<%# Bind("SSR_bPaid") %>' />
                                <asp:HiddenField ID="hf_IsPayable" runat="server" Value='<%# Bind("Ispayable") %>' />
                                <asp:HiddenField ID="hf_STU_NO" runat="server" Value='<%# Bind("STU_NO") %>' />
                                <asp:HiddenField ID="hf_bActive" runat="server" Value='<%# Bind("SSR_bACTIVE") %>' />
                                <asp:HiddenField ID="hf_bSource" runat="server"  Value='<%# Bind("bSource") %>' />
                                 <asp:HiddenField ID="hf_StartDate" runat="server"  Value='<%# Bind("SSV_FROMDATE") %>' />
                                 
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Period" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Label ID="lblStartDate" runat="server" Text='<%# Bind("SSR_SERVICE_STARTDT") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Status" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <%--      <asp:ImageButton  ID="img_Discontinue" runat="server" ImageUrl="~/ParentLogin/Images/DELETE.png"  Width="15px" Height="15px" OnClick="img_Discontinue_Click"  />--%>
                                <asp:LinkButton ID="lnk_paynow" runat="server" OnClick="lnk_paynow_Click" ForeColor="Blue"
                                    Font-Bold="true">Pay Now</asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Request" ItemStyle-HorizontalAlign="Center" >
                            <ItemTemplate>
                            <div >
                                <%--<asp:LinkButton ID="lnkEdits" runat="server"  >
                                    <asp:Image ID="img_Edit" runat="server" ImageUrl="Images/edit.jpg" Width="18px" Height="17px"
                                        AlternateText="Edit" CssClass="gridImage" /></asp:LinkButton>
                                <asp:LinkButton ID="lnk_DeleteReqs" runat="server"  ForeColor="Red"
                                    Font-Bold="true" Visible="false" >
                                    <asp:Image ID="img_Delete" runat="server" ImageUrl="~/Images/DELETE.png" Width="15px"
                                        Height="15px" AlternateText="DELETE" /></asp:LinkButton>--%>
                                         <asp:ImageButton ID="lnkEdit" runat="server" ImageUrl="Images/edit.jpg" Width="18px" Height="17px" OnClick="lnk_Edit_Click" AlternateText="EDIT"/>
                        <asp:ImageButton ID="lnk_DeleteReq" runat="server" ImageUrl="~/Images/DELETE.png" Width="18px" Height="17px" OnClick="lnk_DeleteReq_Click" Visible="false" AlternateText="DELETE"/>
                                <ajaxToolkit:ConfirmButtonExtender ID="cbe" runat="server" DisplayModalPopupID=""
                                    TargetControlID="lnk_DeleteReq" ConfirmText="Please confirm to Delete" />
                                <asp:LinkButton ID="lnk_Discontinue" runat="server" OnClick="lnk_Discontinue_Click"
                                    ForeColor="Red" Font-Bold="true" Visible="false">Discontinue</asp:LinkButton>
                              
                                    </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                       
                    </Columns>
                    <HeaderStyle CssClass="gridheader_new" Height="25px" HorizontalAlign="Center" VerticalAlign="Middle" />
                    <SelectedRowStyle BackColor="Wheat" />
                </asp:GridView>
                <br />
                *Discontinue request is subject to approval
            </td>
        </tr>
        <tr class="tdfields">
            <td>
                <asp:Label ID="lblerror" runat="server" EnableViewState="False" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr class="subheader_img" style="display: none">
            <td align="center" style="height: 25px">
                Services Log
            </td>
        </tr>
        <tr style="display: none">
            <td>
                <asp:GridView ID="gvPendingAppSvc" runat="server" AutoGenerateColumns="False" SkinID="GridViewNormal"
                    Width="100%" Style="text-indent: 5px" ShowFooter="false">
                    <Columns>
                        <%--  <asp:TemplateField HeaderText="Student Name" ItemStyle-HorizontalAlign="Left">
                            <ItemTemplate >
                              <asp:Label ID="lblSTU_Name0" runat="server" Text='<%# Bind("STU_Name") %>' ></asp:Label>
                                         
                            </ItemTemplate> 
                            </asp:TemplateField> --%>
                        <asp:TemplateField HeaderText="Requested Date" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Label ID="lblRequestedDate0" runat="server" Text='<%# Bind("SRS_REQUESTED_DATE") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Academic Year" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Label ID="lblACADEMIC_YEAR0" runat="server" Text='<%# Bind("ACADEMIC_YEAR") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Service" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Label ID="lblSERVICE0" runat="server" Text='<%# Bind("SERVICE") %>'></asp:Label>
                                <asp:HiddenField ID="hf_STU_ID0" runat="server" Value='<%# Bind("STU_ID") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Start Date" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Label ID="lblStartDate0" runat="server" Text='<%# Bind("SSR_SERVICE_STARTDT") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Amount" ItemStyle-HorizontalAlign="Right">
                            <ItemTemplate>
                                <asp:Label ID="lblAMOUNT0" runat="server" Text='<%# Bind("AMOUNT") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Status" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Label ID="lblAPP_Status" runat="server" Text='<%# Bind("APP_Status") %>' ForeColor="ButtonHighlight"></asp:Label>
                                <%--<asp:Image ID="IMG_STATUS" runat="server" ImageUrl='<%# Eval("APP_Status") %>'  Height="15px"  />        --%>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
                <%--<telerik:RadGrid ID="gvPendingAppSvc1" runat="server" 
                        AutoGenerateColumns="False" GridLines="None">
                      <MasterTableView GridLines="Both">
                           <Columns>
                                   <telerik:GridTemplateColumn HeaderText="Student Name" ItemStyle-HorizontalAlign="Left"   >
                                <ItemTemplate>
                                    <asp:Label ID="lblSTU_Name0" runat="server" Text='<%# Bind("STU_Name") %>' ></asp:Label>
                                     <asp:HiddenField ID="hf_STU_ID0" runat="server"  
                                        Value='<%# Bind("STU_ID") %>' />                                                                                                        
                                </ItemTemplate>                              
                            </telerik:GridTemplateColumn>

                                  <telerik:GridTemplateColumn HeaderText="Academic Year" ItemStyle-HorizontalAlign="Left" >
                                <ItemTemplate>
                                    <asp:Label ID="lblACADEMIC_YEAR0" runat="server" 
                                        Text='<%# Bind("ACADEMIC_YEAR") %>' ></asp:Label>
                                </ItemTemplate>                              
                            </telerik:GridTemplateColumn  >

                                     <telerik:GridTemplateColumn HeaderText="Service" ItemStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <asp:Label ID="lblSERVICE0" runat="server" Text='<%# Bind("SERVICE") %>' ></asp:Label>
                                </ItemTemplate>                              
                            </telerik:GridTemplateColumn>
                               <telerik:GridTemplateColumn HeaderText="Term">
                                <ItemTemplate>
                                    <asp:Label ID="lblTRM_DESCRIPTION0" runat="server" Text='<%# Bind("TERM") %>' ></asp:Label>
                                </ItemTemplate>                              
                            </telerik:GridTemplateColumn>
                              <telerik:GridTemplateColumn HeaderText="Amount" ItemStyle-HorizontalAlign="Right" >
                                <ItemTemplate>
                                    <asp:Label ID="lblAMOUNT0" runat="server" Text='<%# Bind("AMOUNT") %>' ></asp:Label>
                                </ItemTemplate> 
                                <HeaderStyle HorizontalAlign="Right" />                               
                            </telerik:GridTemplateColumn>

                                <telerik:GridTemplateColumn  HeaderText="Approval Status" >
                                <ItemTemplate>
                                    <asp:Image ID="IMG_STATUS" runat="server" ImageUrl='<%# Eval("APP_Status") %>'  Height="15px"  />                                  
                                </ItemTemplate>
                                
                                <HeaderStyle Width="10px" />
                            </telerik:GridTemplateColumn>
                            </Columns>  
                            
                            </MasterTableView>
                      </telerik:RadGrid>--%>
            </td>
        </tr>
        <%--   <tr>   <td align="right"  > <table class="style1">
                    <tr>
                            <td>
                                <img alt="" class="imgstyle" src="../Images/APPLY.png" /> Approved</td>
                            <td>
                                <img alt="s" class="imgstyle" src="../Images/PENDING.png" />Pending</td>
                                 <td>
                                <img alt="s" class="imgstyle" src="../Images/DELETE.png" />Rejected</td>
                        </tr>
                    
                    </table></td></tr> --%>
    </table>
    <asp:Panel ID="pnlDiscontinue" runat="server" CssClass="darkPanlvisible" Visible="false">
        <div class="panelQual">
            <div class="mainheading">
                <div style="float: left; width: 99%;">
                    <b>Discontinue Request</b></div>
                <div>  <%--style="float: right; vertical-align: top;"--%>
                    <asp:LinkButton ForeColor="red" ID="lbtnApplicantNotesClose" ToolTip="click here to close"
                        CssClass="" runat="server" Text="X" Font-Underline="false" CausesValidation="false">X</asp:LinkButton>
                </div>
            </div>
            <center>
                <table width="70%"  cellpadding="0" cellspacing="0" class="table table-striped table-bordered table-responsive text-left my-orders-table">
                    <tr>
                        <td colspan="2" align="center">
                            <asp:Label ID="lbldiserror" runat="server" Text="" ForeColor="Red"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <th class="tdfields" align="left">
                            Student Name
                        </th>
                        <td class="tdfields" align="left">
                            <asp:Label ID="lblStu_name_D" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <th class="tdfields" align="left">
                            Student No
                        </th>
                        <td class="tdfields" align="left">
                            <asp:Label ID="lblStuNo" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <th class="tdfields" align="left">
                            Service
                        </th>
                        <td class="tdfields" align="left">
                            <asp:Label ID="lblsvc" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <th class="tdfields" align="left">
                            Service Start Date
                        </th>
                        <td class="tdfields" align="left">
                            <asp:Label ID="lblfromdate_D" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr class="tdfields">
                        <th align="left">
                            Discontinue From <span style="color: Red">*</span>`
                        </th>
                        <td align="left">
                            <input type="text" id="txtfromdate" class="datepicker form-control" runat="server" />
                            <asp:ImageButton ID="imgFrom" runat="server" ImageUrl="~/Images/Common/PageBody/Calendar.png"
                                TabIndex="4" Visible="false" />
                            <br />
                            <%--   <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToValidate="txtfromdate"
                            ErrorMessage="Date should be greater or equal to service start date" Operator="GreaterThanEqual"
                            Type="Date" ValidationGroup="Save"  Display="Dynamic"></asp:CompareValidator>--%>
                            <br />
                            <%--  <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="txtfromdate"
                            ErrorMessage="Selected date should be greater or equal to todays date" Operator="GreaterThanEqual"
                            Type="Date" ValidationGroup="Save"></asp:CompareValidator>--%>
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="true"
                                ShowSummary="false" ValidationGroup="Save" />
                        </td>
                    </tr>
                    <tr>
                        <th class="tdfields" align="left">
                            Remarks<span style="color: Red">*</span>
                        </th>
                        <td class="tdfields" align="left">
                            <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine" SkinID="MultiText" CssClass="form-control"
                                Columns="32" Rows="6">
                            </asp:TextBox>
                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="txtRemarks"
                                ErrorMessage="Remarks Required" ValidationGroup="Save" Display="Dynamic">*</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2">
                            <asp:Button ID="btnSaveDisRequest" runat="server" Text="Save" ValidationGroup="Save"
                                CausesValidation="true" CssClass="btn btn-info" />&nbsp;&nbsp;
                            <asp:Button ID="btnCancelResetPassword" runat="server" Text="Cancel" CausesValidation="false"
                                CssClass="btn btn-info" />
                        </td>
                    </tr>
                </table>
            </center>
        </div>
    </asp:Panel>

                                  </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
