<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ServiceRequestPayment.aspx.vb"
    Inherits="ParentLogin_Service_ServiceRequestPayment" Debug="true" MasterPageFile="~/ParentMaster.master" %>

<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>

    <asp:Content ID="Content1" ContentPlaceHolderID="cphParent" runat="Server">

<div><asp:Label ID="lblPayMsg" runat="server" Font-Names="Verdana" ForeColor="Red" Font-Size="11pt"
                                EnableViewState="False"></asp:Label></div>
                <table  align="center" style="width: 98%; height: 110px;"  cellpadding="8" cellspacing="0" class="BlueTable_simple">
                                       <tr class="trSub_Header">
                        <td colspan="2" align="left" style="height: 18px">
                            Service Fee Online Payment
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="tdfields" width="20%">
                            Date
                        </td>
                        <td align="left" >
                            <asp:Label ID="lblDate" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="tdfields">
                            Student ID
                        </td>
                        <td align="left" >
                            <asp:Label ID="txtStdNo" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="tdfields">
                            Student Name
                        </td>
                        <td align="left" >
                            <asp:Label ID="txtStudentname" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="tdfields">
                            Academic Year
                        </td>
                        <td align="left" >
                            <asp:Label ID="lblacademicyear" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="tdfields">
                            Grade &amp; Section
                        </td>
                        <td align="left" >
                            <asp:Label ID="stugrd" runat="server"></asp:Label>
                            -<strong></strong>
                            <asp:Label ID="lblsct" runat="server"></asp:Label>
                            &nbsp; &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="tdfields">
                            School
                        </td>
                        <td align="left" >
                            <asp:Label ID="lblschool" runat="server"></asp:Label>
                        </td>
                    </tr>
                    </table>
                    <div>
                      <asp:Label ID="lblError" runat="server" EnableViewState="False" Font-Size="11pt"
                                ForeColor="Red" Font-Names="Verdana"></asp:Label>
                    </div>
                <table class="tableNoborder" align="center" style="width: 98%;" runat="server"
                    id="tbl_Feedetailsgrid">
                    <tr id="tr_AddFee" runat="server" class="trSub_Header">
                        <td align="left">
                            <asp:HiddenField ID="hfPaymentProvider" runat="server" />
                            Service Details</td>
                    </tr>
                    <tr>
                        <td class="tdfields" align="right">
                            <asp:GridView ID="gvFeeCollection" runat="server" AutoGenerateColumns="False" EmptyDataText="No Details Added"
                                SkinID="GridViewNormal" Width="100%">
                                <RowStyle Height="20px" VerticalAlign="Top" />
                                <Columns>
                                    <asp:BoundField DataField="SERVICE" HeaderText="Service">
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="SSR_RATE" HeaderText="Amount">
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="SSR_DISCOUNT" HeaderText="Discount">
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="NETAMOUNT" HeaderText="Net Amount">
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:BoundField>
                                     <asp:TemplateField HeaderText="Paying Now">
                            <ItemTemplate>
                                 <asp:HiddenField ID="HF_SSR_ID" runat="server" Value='<%# Bind("SSR_ID") %>' /> 
                                  <asp:HiddenField ID="HF_MINPAYAMOUNT" runat="server" Value='<%# Bind("FEE_MinPaymentAmount") %>' /> 
                                <asp:TextBox ID="txtAmountToPay" AutoCompleteType="Disabled" runat="server" AutoPostBack="True"
                                    onFocus="this.select();"  OnTextChanged="txtAmountToPay_TextChanged" Style="text-align: right"
                                    TabIndex="52" Text='<%# Bind("Amount", "{0:0.00}") %>' Width="100px"></asp:TextBox>
                               </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                            <ItemStyle HorizontalAlign="Right" Width="89px" />
                        </asp:TemplateField>
                        
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr runat="server">
                        <td align="right" class="tdfields" valign="middle">
                            Total :
                          <asp:TextBox ID="txtGridTotal" Style="text-align: right" runat="server" TabIndex="150"
                                Width="100px"></asp:TextBox></td>
                    </tr>
                    <tr id="Tr2" runat="server">
                        <td align="right" class="tdfields" valign="middle">
                            Processing Charge :
                          <asp:TextBox ID="txtProcessingCharge" Style="text-align: right" runat="server" TabIndex="150"
                                Width="100px"></asp:TextBox></td>
                    </tr>
                    <tr id="tr_DiscountTotal" runat="server">
                        <td align="right" class="tdfields" valign="middle">
                            <asp:HiddenField ID="hfCardCharges" runat="server" />
                            Net Total (<asp:Label ID="lblCurrency" runat="server"></asp:Label>
                            ):
                            <asp:TextBox ID="txtTotal" Style="text-align: right" runat="server" TabIndex="151"
                                Width="100px"></asp:TextBox>
                        </td>
                    </tr>
                </table>
                <table id="Table1" style="width: 98%" runat="server" class="tableNoborder">
                    <tr id="Tr1" runat="server">
                        <td align="center" style="font-size: 12px; color: #ff0000; height: 18px">
                            &nbsp;<asp:Button ID="btnSave" runat="server" CssClass="buttons" TabIndex="155" Text="Confirm & Proceed" width="130px"/>
                            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="buttons" width="130px"
                                Text="Cancel" TabIndex="158" />
                        </td>
                    </tr>
                </table>
          
</asp:Content>