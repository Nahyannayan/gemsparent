﻿Imports System.Data
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports Telerik.Web.UI

Imports System
Imports System.Data.SqlClient
Imports System.Web.Mail
Imports System.IO

Partial Class ParentLogin_Service_ServiceRequest
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("username") Is Nothing Then
            Response.Redirect("~\General\Home.aspx")
        End If
        'CompareValidator2.ValueToCompare = DateTime.Now.Date
        If Not IsPostBack Then
            trOwnIns.Visible = False
            tr_CurrentTeacher.Visible = False
            ViewState("SVCategory_Oldvalue") = "0"


            ' Session("OLU_ID") = "47521" '"243197"
            BindStudentServices()
            BindStudents()
            BindServiceCategory()

            AcademicYear()
            BindServices()
            bindTerms()
            BindStudentServices()
            hidingtables()
            enableButton()
            ViewState("SVC_Value") = ddlSVCategory.SelectedValue
        End If

    End Sub

    Sub BindStudents()
        Try
            Dim con As String = ConnectionManger.GetOASISConnectionString
            Dim param(4) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@OLU_ID", Session("OLU_ID"))
            param(1) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
            param(1).Direction = ParameterDirection.ReturnValue
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "[ONLINE].[SERVICES_GETSIBLINGS]", param)
            ddlStudents.DataSource = ds.Tables(0)
            ddlStudents.DataTextField = "StudName"
            ddlStudents.DataValueField = "stu_id"
            ddlStudents.DataBind()

            If Not ddlStudents.Items.FindByValue(Session("STU_ID")) Is Nothing Then
                ddlStudents.Items.FindByValue(Session("STU_ID")).Selected = True 'FindByValue
            End If
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub ddlStudents_SelectedIndexChanged(ByVal o As Object, ByVal e As System.EventArgs) Handles ddlStudents.SelectedIndexChanged
        BindServiceCategory()
        AcademicYear()
        BindServices()
        bindTerms()

        HF_svc_count.Value = ""
        HF_MAX_SVC.Value = ""
    End Sub
    Sub BindServiceCategory()
        Dim con As String = ConnectionManger.GetOASISConnectionString
        Dim param(4) As SqlClient.SqlParameter
        'param(0) = New SqlClient.SqlParameter("@OLU_ID", Session("OLU_ID"))
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "[ONLINE].[SERVICES_GETSERVICES_CATEGORY]", param)
        ddlSVCategory.DataSource = ds.Tables(0)
        ddlSVCategory.DataTextField = "SSC_DESC"
        ddlSVCategory.DataValueField = "SSC_ID"
        ddlSVCategory.DataBind()
        If ds.Tables(0).Rows.Count > 0 Then
            lblLanguage.Text = IIf(Convert.ToString(ds.Tables(0).Rows(0)("SSC_LBL_DESC")) <> "", Convert.ToString(ds.Tables(0).Rows(0)("SSC_LBL_DESC")), "Service")
        Else
            lblLanguage.Text = "Service"
        End If
    End Sub
    Sub BindServices()
        Try
            Dim ACDID_GRDID As String() = New String(9) {}
            ACDID_GRDID = ddlAcademicYear.SelectedValue.Split("_")
            Dim con As String = ConnectionManger.GetOASISConnectionString
            Dim param(4) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@SSC_ID", ddlSVCategory.SelectedValue)
            param(1) = New SqlClient.SqlParameter("@ACD_ID", ACDID_GRDID(0))
            param(2) = New SqlClient.SqlParameter("@GRD_ID", ACDID_GRDID(1))
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "[ONLINE].[SERVICES_GETSERVICES]", param)
            ddlServices.DataSource = ds
            ddlServices.DataTextField = "SVC_DESCRIPTION"
            ddlServices.DataValueField = "SVCID_SVGID"
            ddlServices.DataBind()
            If ds.Tables(0).Rows.Count > 0 Then
                lblLanguage.Text = IIf(Convert.ToString(ds.Tables(0).Rows(0)("SSC_LBL_DESC")) <> "", Convert.ToString(ds.Tables(0).Rows(0)("SSC_LBL_DESC")), "Service")
            Else
                lblLanguage.Text = "Service"
            End If

            bindTerms()
            hidingtables()
        Catch ex As Exception
            'ddlServices.
        End Try
      
    End Sub
    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal o As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        BindServiceCategory()
        BindServices()
        HF_svc_count.Value = ""
        HF_MAX_SVC.Value = ""
    End Sub
    Sub AcademicYear()
        Dim con As String = ConnectionManger.GetOASISConnectionString
        Dim param(4) As SqlClient.SqlParameter
        param(0) = New SqlClient.SqlParameter("@STU_ID", ddlStudents.SelectedValue)
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "[ONLINE].[SERVICES_GETACADEMICYEAR]", param)
        ddlAcademicYear.DataSource = ds.Tables(0)
        ddlAcademicYear.DataTextField = "ACY_DESCR"
        ddlAcademicYear.DataValueField = "ACDID_GRDID"
        ddlAcademicYear.DataBind()
    End Sub
    Public Property Terms() As DataTable
        Get
            If ViewState("Terms") Is Nothing Then
                Dim dtTerms As New DataTable()
                dtTerms.Columns.Add("TRM_ID")
                dtTerms.Columns.Add("Term")
                dtTerms.Columns.Add("Amount")
                dtTerms.Columns.Add("StartDate")
                dtTerms.Columns.Add("EndDate")
                ViewState("Terms") = dtTerms
                Return DirectCast(ViewState("Terms"), DataTable)
            Else
                Return DirectCast(ViewState("Terms"), DataTable)
            End If
        End Get
        Set(ByVal value As DataTable)
            ViewState("Terms") = value
        End Set
    End Property
    Sub bindTerms()

        'For i As Integer = 1 To 3
        '    Dim dr As DataRow = Terms.NewRow()
        '    dr("TRM_ID") = i
        '    dr("Term") = "Term" + i.ToString()

        '    dr("Amount") = "600"
        '    dr("StartDate") = ""
        '    dr("EndDate") = ""
        '    Terms.Rows.Add(dr)
        'Next
        Dim ACDID_GRDID As String() = New String(9) {}
        ACDID_GRDID = ddlAcademicYear.SelectedValue.Split("_")
        Dim SVCID_SVGID As String() = New String(3) {}
        SVCID_SVGID = ddlServices.SelectedValue.Split("_")

        ''
        Dim count_sv As String = "0"
        If gvStudentServicesDetails.Items.Count > 0 Then
            Dim con As String = ConnectionManger.GetOASISConnectionString
            Dim param(6) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@ACD_ID", ACDID_GRDID(0))
            param(1) = New SqlClient.SqlParameter("@STU_ID", ddlStudents.SelectedValue)
            param(2) = New SqlClient.SqlParameter("@SSR_SVC_ID", SVCID_SVGID(0))
            param(3) = New SqlClient.SqlParameter("@bapproved", "0")
            count_sv = SqlHelper.ExecuteScalar(con, CommandType.StoredProcedure, "[ONLINE].[SERVICES_CHECK_SVC_AVAILD]", param)
        End If
        ''
        If count_sv = "0" Then
            If SVCID_SVGID.Length > 1 Then

                Dim con As String = ConnectionManger.GetOASISConnectionString
                Dim param(4) As SqlClient.SqlParameter
                param(0) = New SqlClient.SqlParameter("@SVG_ID", SVCID_SVGID(1))
                param(1) = New SqlClient.SqlParameter("@ACD_ID", ACDID_GRDID(0))
                param(2) = New SqlClient.SqlParameter("@SVC_ID", ddlServices.SelectedValue)
                Dim ds As New DataSet
                ds = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "[ONLINE].[SERVICES_GETSERVICES_TERMS]", param)
                ' Dim q = From DT In ds.Tables(0).AsEnumerable()
                Dim dtFiltered As DataTable = ds.Tables(0)
                If ds IsNot Nothing Then

                    If ds.Tables(0).Rows.Count > 0 Then

                        For Each dr As DataRow In ServicesRequested.Rows
                            Dim dvApprovalLevels As DataView = dtFiltered.DefaultView
                            dvApprovalLevels.RowFilter = "STM_ID<>'" + dr("ID") + "'"
                            dtFiltered = dvApprovalLevels.ToTable()
                            dtFiltered.AcceptChanges()
                        Next
                        For Each gr As GridDataItem In gvStudentServicesDetails.Items
                            Dim hf_STU_ID As HiddenField = DirectCast(gr.FindControl("hf_STU_ID0"), HiddenField)
                            Dim hf_STM_ID As HiddenField = DirectCast(gr.FindControl("hf_STM_ID0"), HiddenField)

                            Dim dvApprovalLevels As DataView = dtFiltered.DefaultView
                            If ddlStudents.SelectedValue = hf_STU_ID.Value Then
                                dvApprovalLevels.RowFilter = "STM_ID <>" + hf_STM_ID.Value + " "
                                dtFiltered = dvApprovalLevels.ToTable()
                                dtFiltered.AcceptChanges()
                            End If
                        Next
                    End If
                End If
                gvTerms.DataSource = dtFiltered
                gvTerms.DataBind()
            Else
                gvTerms.DataBind()
            End If
        Else
            gvTerms.DataBind()
            lblerror.Text = ""
        End If
    End Sub
    Public Property ServicesRequested() As DataTable
        Get
            If ViewState("ServicesRequested") Is Nothing Then
                Dim dtServicesRequested As New DataTable()
                dtServicesRequested.Columns.Add("STU_ID")
                dtServicesRequested.Columns.Add("STU_Name")
                dtServicesRequested.Columns.Add("ACADEMIC_YEAR")
                dtServicesRequested.Columns.Add("ACD_ID")
                dtServicesRequested.Columns.Add("SSC_ID")
                dtServicesRequested.Columns.Add("SERVICE")
                dtServicesRequested.Columns.Add("SVC_ID")
                dtServicesRequested.Columns.Add("SVG_ID")
                dtServicesRequested.Columns.Add("TERM")
                dtServicesRequested.Columns.Add("TRM_ID")
                dtServicesRequested.Columns.Add("AMOUNT")
                dtServicesRequested.Columns.Add("bAPPROVED")
                dtServicesRequested.Columns.Add("LEVEL")
                dtServicesRequested.Columns.Add("CurrentTeacher")
                dtServicesRequested.Columns.Add("OWNINSTRUMENT")
                dtServicesRequested.Columns.Add("ID")
                ViewState("ServicesRequested") = dtServicesRequested
                Return DirectCast(ViewState("ServicesRequested"), DataTable)
            Else
                Return DirectCast(ViewState("ServicesRequested"), DataTable)
            End If
        End Get
        Set(ByVal value As DataTable)
            ViewState("ServicesRequested") = value
        End Set

    End Property

    Sub GetServicesCount()
        Dim ACDID_GRDID As String() = New String(9) {}
        ACDID_GRDID = ddlAcademicYear.SelectedValue.Split("_")

        Dim con As String = ConnectionManger.GetOASISConnectionString
        Dim param(6) As SqlClient.SqlParameter
        param(0) = New SqlClient.SqlParameter("@ACD_ID", ACDID_GRDID(0))
        param(1) = New SqlClient.SqlParameter("@STU_ID", ddlStudents.SelectedValue)
        param(2) = New SqlClient.SqlParameter("@SSC_ID", ddlSVCategory.SelectedValue)

        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "[ONLINE].[SERVICES_GET_SVC_BSULIMIT]", param)
        If ds IsNot Nothing Then
            HF_svc_count.Value = ds.Tables(0).Rows(0)("STU_SERVICE_COUNT")
            HF_MAX_SVC.Value = ds.Tables(0).Rows(0)("MAX_SVC")
        End If

    End Sub

    Protected Sub btnRequestService_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRequestService.Click

        Dim ACDID_GRDID As String() = New String(9) {}
        ACDID_GRDID = ddlAcademicYear.SelectedValue.Split("_")
        Dim SVCID_SVGID As String() = New String(3) {}
        SVCID_SVGID = ddlServices.SelectedValue.Split("_")
        GetServicesCount()
        If ServicesRequested IsNot Nothing Then

            'Dim SvcReq = (From SR In ServicesRequested.AsEnumerable() _
            'Where(SR("STU_ID") = ddlStudents.SelectedValue And SR("ACD_ID") = ACDID_GRDID(0) And SR("SSC_ID") = ddlSVCategory.SelectedValue)
            'Select SR("SVC_ID")).Distinct
            ''
            Dim ServicesRequested_DT As DataTable = ServicesRequested
            Dim dvApprovalLevels As DataView = ServicesRequested_DT.DefaultView
            dvApprovalLevels.RowFilter = "STU_ID =" + ddlStudents.SelectedValue + " and  ACD_ID = " + ACDID_GRDID(0) + " and SSC_ID=" + ddlSVCategory.SelectedValue + ""
            ServicesRequested_DT = dvApprovalLevels.ToTable(True, "SVC_ID")
            ServicesRequested_DT.AcceptChanges()
            'ServicesRequested = ServicesRequested_DT

            Dim counter As Integer = 0
            Dim dr As DataRow
            If gvStudentServicesDetails.Items.Count > 0 Then
                'For Each d In SvcReq
                For Each dr In ServicesRequested_DT.Rows
                    Dim con As String = ConnectionManger.GetOASISConnectionString
                    Dim param(6) As SqlClient.SqlParameter
                    param(0) = New SqlClient.SqlParameter("@ACD_ID", ACDID_GRDID(0))
                    param(1) = New SqlClient.SqlParameter("@STU_ID", ddlStudents.SelectedValue)
                    param(2) = New SqlClient.SqlParameter("@SSR_SVC_ID", dr("SVC_ID"))
                    Dim count_sv As String = ""
                    count_sv = SqlHelper.ExecuteScalar(con, CommandType.StoredProcedure, "[ONLINE].[SERVICES_CHECK_SVC_AVAILD]", param)

                    counter = counter + IIf(count_sv = "1", 1, 0)
                Next
            End If
            ''
            Dim a As Integer = IIf(ServicesRequested_DT IsNot Nothing, ServicesRequested_DT.Rows.Count, 0)
            Dim count_F As Integer = IIf(HF_svc_count.Value <> "0", Convert.ToInt16(HF_svc_count.Value) - counter, counter)

            HF_svc_count.Value = Convert.ToInt16(a) + count_F
        End If

        If Convert.ToInt16(HF_svc_count.Value) < Convert.ToInt16(HF_MAX_SVC.Value) Then

            For Each gr As GridDataItem In gvTerms.Items

                Dim cbTerm As CheckBox = DirectCast(gr.FindControl("cbTerm"), CheckBox)
                Dim lblTRM_DESCRIPTION As Label = DirectCast(gr.FindControl("lblTRM_DESCRIPTION"), Label)
                Dim lblTERMFEES As Label = DirectCast(gr.FindControl("lblTERMFEES"), Label)
                Dim hf_TRMID As HiddenField = DirectCast(gr.FindControl("hf_TRMID"), HiddenField)
                Dim hf_STM_ID As HiddenField = DirectCast(gr.FindControl("hf_STM_ID"), HiddenField)
                If cbTerm.Checked Then

                    Dim status As Integer = 0
                    Dim ID As String = hf_STM_ID.Value  'ddlStudents.SelectedValue + "_" + ddlAcademicYear.SelectedValue + "_" + ddlServices.SelectedValue + "_" + lblTRM_DESCRIPTION.Text

                    For Each dr As DataRow In ServicesRequested.Rows
                        If ID = Convert.ToString(dr("ID")) Then
                            status = 1
                            Exit For
                        End If
                    Next

                    If status = 0 Then
                        Dim dr As DataRow = ServicesRequested.NewRow()
                        dr("STU_ID") = ddlStudents.SelectedValue
                        dr("STU_Name") = ddlStudents.SelectedItem.Text
                        dr("ACADEMIC_YEAR") = ddlAcademicYear.SelectedItem.Text
                        dr("ACD_ID") = ACDID_GRDID(0)
                        dr("SSC_ID") = ddlSVCategory.SelectedValue
                        dr("SERVICE") = ddlServices.SelectedItem.Text
                        dr("SVC_ID") = SVCID_SVGID(0)
                        dr("SVG_ID") = SVCID_SVGID(1)
                        dr("TERM") = lblTRM_DESCRIPTION.Text
                        dr("TRM_ID") = hf_TRMID.Value
                        dr("AMOUNT") = lblTERMFEES.Text
                        dr("LEVEL") = ddlLevels.SelectedValue
                        dr("CurrentTeacher") = txtCurrentTeacher.Text
                        dr("OWNINSTRUMENT") = IIf(rbOwnIns.SelectedValue = "1", "1", "0")

                        dr("bAPPROVED") = False
                        dr("ID") = hf_STM_ID.Value 'ddlStudents.SelectedValue + "_" + ddlAcademicYear.SelectedValue + "_" + ddlServices.SelectedValue + "_" + "TermID"
                        ServicesRequested.Rows.Add(dr)
                    Else
                        'lblmessage.Text = "This User is already added in the approval list..!!"
                    End If
                End If
            Next
            bindTerms()
        Else

            lblerror.Text = "Maximum limit for this Service Category Reached...!!"
        End If


        ServicesRequested.DefaultView.RowFilter = ""
        gvServicesRequested.DataSource = ServicesRequested
        gvServicesRequested.DataBind()
        enableButton()

        ddlLevels.SelectedIndex = -1
        txtCurrentTeacher.Text = ""
    End Sub
#Region "using Linq"
    'Protected Sub btnRequestService_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRequestService.Click

    '    Dim ACDID_GRDID As String() = New String(9) {}
    '    ACDID_GRDID = ddlAcademicYear.SelectedValue.Split("_")
    '    Dim SVCID_SVGID As String() = New String(3) {}
    '    SVCID_SVGID = ddlServices.SelectedValue.Split("_")

    '    If HF_svc_count.Value = "" Then
    '        GetServicesCount()
    '    End If
    '    If ServicesRequested IsNot Nothing Then

    '        Dim SvcReq = (From SR In ServicesRequested.AsEnumerable() _
    ' Where SR("STU_ID") = ddlStudents.SelectedValue And SR("ACD_ID") = ACDID_GRDID(0) And SR("SSC_ID") = ddlSVCategory.SelectedValue
    ' Select SR("SVC_ID")).Distinct
    '        ''
    '        Dim ServicesRequested_DT As DataTable = ServicesRequested
    '        Dim dvApprovalLevels As DataView = ServicesRequested_DT.DefaultView
    '        dvApprovalLevels.RowFilter = "STU_ID =" + ddlStudents.SelectedValue + " and  ACD_ID = " + ACDID_GRDID(0) + " and SSC_ID=" + ddlSVCategory.SelectedValue + ""
    '        ServicesRequested_DT = dvApprovalLevels.ToTable(True, "SVC_ID")
    '        ServicesRequested_DT.AcceptChanges()
    '        'ServicesRequested = ServicesRequested_DT

    '        Dim counter As Integer = 0
    '        If gvStudentServicesDetails.Items.Count > 0 Then
    '            For Each d In SvcReq
    '                Dim con As String = ConnectionManger.GetOASISConnectionString
    '                Dim param(6) As SqlClient.SqlParameter
    '                param(0) = New SqlClient.SqlParameter("@ACD_ID", ACDID_GRDID(0))
    '                param(1) = New SqlClient.SqlParameter("@STU_ID", ddlStudents.SelectedValue)
    '                param(2) = New SqlClient.SqlParameter("@SSR_SVC_ID", d)
    '                Dim count_sv As String = ""
    '                count_sv = SqlHelper.ExecuteScalar(con, CommandType.StoredProcedure, "[ONLINE].[SERVICES_CHECK_SVC_AVAILD]", param)

    '                counter = counter + If(count_sv = "1", 1, 0)
    '            Next
    '        End If
    '        ''
    '        Dim a As Integer = SvcReq.Count()
    '        HF_svc_count.Value = Convert.ToInt16(a) + Convert.ToInt16(HF_svc_count.Value) - counter
    '    End If






    '    If Convert.ToInt16(HF_svc_count.Value) < Convert.ToInt16(HF_MAX_SVC.Value) Then

    '        For Each gr As GridDataItem In gvTerms.Items

    '            Dim cbTerm As CheckBox = DirectCast(gr.FindControl("cbTerm"), CheckBox)
    '            Dim lblTRM_DESCRIPTION As Label = DirectCast(gr.FindControl("lblTRM_DESCRIPTION"), Label)
    '            Dim lblTERMFEES As Label = DirectCast(gr.FindControl("lblTERMFEES"), Label)
    '            Dim hf_TRMID As HiddenField = DirectCast(gr.FindControl("hf_TRMID"), HiddenField)
    '            Dim hf_STM_ID As HiddenField = DirectCast(gr.FindControl("hf_STM_ID"), HiddenField)
    '            If cbTerm.Checked Then

    '                Dim status As Integer = 0
    '                Dim ID As String = hf_STM_ID.Value  'ddlStudents.SelectedValue + "_" + ddlAcademicYear.SelectedValue + "_" + ddlServices.SelectedValue + "_" + lblTRM_DESCRIPTION.Text

    '                For Each dr As DataRow In ServicesRequested.Rows
    '                    If ID = Convert.ToString(dr("ID")) Then
    '                        status = 1
    '                        Exit For
    '                    End If
    '                Next

    '                If status = 0 Then
    '                    Dim dr As DataRow = ServicesRequested.NewRow()
    '                    dr("STU_ID") = ddlStudents.SelectedValue
    '                    dr("STU_Name") = ddlStudents.SelectedItem.Text
    '                    dr("ACADEMIC_YEAR") = ddlAcademicYear.SelectedItem.Text
    '                    dr("ACD_ID") = ACDID_GRDID(0)
    '                    dr("SSC_ID") = ddlSVCategory.SelectedValue
    '                    dr("SERVICE") = ddlServices.SelectedItem.Text
    '                    dr("SVC_ID") = SVCID_SVGID(0)
    '                    dr("SVG_ID") = SVCID_SVGID(1)
    '                    dr("TERM") = lblTRM_DESCRIPTION.Text
    '                    dr("TRM_ID") = hf_TRMID.Value
    '                    dr("AMOUNT") = lblTERMFEES.Text
    '                    dr("LEVEL") = ddlLevels.SelectedValue
    '                    dr("CurrentTeacher") = txtCurrentTeacher.Text
    '                    dr("OWNINSTRUMENT") = IIf(rbOwnIns.SelectedValue = "1", "1", "0")

    '                    dr("bAPPROVED") = False
    '                    dr("ID") = hf_STM_ID.Value 'ddlStudents.SelectedValue + "_" + ddlAcademicYear.SelectedValue + "_" + ddlServices.SelectedValue + "_" + "TermID"
    '                    ServicesRequested.Rows.Add(dr)
    '                Else
    '                    'lblmessage.Text = "This User is already added in the approval list..!!"
    '                End If
    '            End If
    '        Next
    '        gvServicesRequested.DataSource = ServicesRequested
    '        gvServicesRequested.DataBind()
    '        bindTerms()
    '    Else
    '        lblerror.Text = "Maximum limit for this Service Category Reached...!!"
    '    End If
    '    enableButton()
    'End Sub
    Sub SaveServiceRequest()

        Dim constring As String = ConnectionManger.GetOASISConnectionString
        Dim con As SqlConnection = New SqlConnection(constring)
        con.Open()
        Dim sqltran As SqlTransaction
        sqltran = con.BeginTransaction("trans")
        Dim bsaved As Integer = 0
        Dim bSSRID As String = ""
        Try
            For Each gr As GridViewRow In gvServicesRequested.Rows

                Dim HF_SVG_ID As HiddenField = DirectCast(gr.FindControl("HF_SVG_ID"), HiddenField)
                Dim hf_STU_ID As HiddenField = DirectCast(gr.FindControl("hf_STU_ID"), HiddenField)
                Dim hf_STM_ID As HiddenField = DirectCast(gr.FindControl("hf_STM_ID"), HiddenField)
                Dim HF_SVC_ID As HiddenField = DirectCast(gr.FindControl("HF_SVC_ID"), HiddenField)
                Dim HF_ACD_ID As HiddenField = DirectCast(gr.FindControl("HF_ACD_ID"), HiddenField)


                Dim ServicesRequested_DT1 As DataTable = ServicesRequested
                Dim dvApprovalLevels As DataView = ServicesRequested_DT1.DefaultView
                dvApprovalLevels.RowFilter = "STU_ID =" + hf_STU_ID.Value + " and  ACD_ID = " + HF_ACD_ID.Value + " and SVC_ID=" + HF_SVC_ID.Value + ""
                Dim ServicesRequested_DT As DataTable = dvApprovalLevels.ToTable()
                Dim SERVICE_TERM_XML As String = "<SERVICES>"
                Dim Terms_Service As String = ""

                For Each d As DataRow In ServicesRequested_DT.Rows

                    If Terms_Service <> "" Then
                        Terms_Service = Terms_Service + "|" + d("ID").ToString()
                    Else
                        Terms_Service = d("ID").ToString()
                    End If
                    SERVICE_TERM_XML = SERVICE_TERM_XML + "<TERM STM_ID='" + d("ID").ToString() + "' SRS_FEE='" + d("AMOUNT").ToString() + "' SRS_LEVEL='" + d("LEVEL").ToString() + "' SRS_TEACHER='" + d("CurrentTeacher").ToString() + "' SRS_OWN_INSTRUMENT='" + d("OWNINSTRUMENT").ToString() + "' STU_ID='" + d("STU_ID").ToString() + "' SVC_ID='" + d("SVC_ID").ToString() + "' ></TERM>"
                Next
                SERVICE_TERM_XML = SERVICE_TERM_XML + "</SERVICES>"

                Dim Terms_SVC As String() = New String(9) {}
                Terms_SVC = Terms_Service.Split("|")

                Dim param1(2) As SqlClient.SqlParameter
                param1(0) = New SqlClient.SqlParameter("@SSR_SVG_ID", HF_SVG_ID.Value)
                Dim ds As New DataSet
                ds = SqlHelper.ExecuteDataset(sqltran, CommandType.StoredProcedure, "[ONLINE].[SERVICES_GET_TRM_CONTINUES]", param1)
                Dim dt_TRM_CONTINUES As New DataTable
                dt_TRM_CONTINUES = ds.Tables(0)
                Dim flag As Integer = 0
                Dim j As Integer = 0
                Dim STM_ID_Save As String = ""
                Dim i As Integer = 0

                For Each row As DataRow In dt_TRM_CONTINUES.Rows
                    i = i + 1
                    If Terms_SVC.Length > j Then
                        If Terms_SVC(j) = row("STM_ID") Then

                            If STM_ID_Save <> "" Then
                                STM_ID_Save = STM_ID_Save + "|" + Terms_SVC(j).ToString()
                            Else
                                STM_ID_Save = Terms_SVC(j).ToString()
                            End If
                            flag = 0
                            j = j + 1
                        Else
                            flag = 1
                        End If
                    Else
                        flag = 1
                    End If


                    If flag = 1 Or i = dt_TRM_CONTINUES.Rows.Count Then
                        If STM_ID_Save <> "" Then
                            Dim lblAMOUNT As Label = DirectCast(gr.FindControl("lblAMOUNT"), Label)

                            Dim HF_Level As HiddenField = DirectCast(gr.FindControl("HF_Level"), HiddenField)
                            Dim HF_Teacher As HiddenField = DirectCast(gr.FindControl("HF_Teacher"), HiddenField)

                            Dim param(12) As SqlClient.SqlParameter
                            param(0) = New SqlClient.SqlParameter("@SSR_SVG_ID", HF_SVG_ID.Value)
                            param(1) = New SqlClient.SqlParameter("@SSR_STU_ID", hf_STU_ID.Value)
                            param(2) = New SqlClient.SqlParameter("@SRS_STM_ID", hf_STM_ID.Value)
                            param(3) = New SqlClient.SqlParameter("@SRS_FEE", lblAMOUNT.Text)
                            param(4) = New SqlClient.SqlParameter("@SRS_LEVEL", HF_Level.Value)
                            param(5) = New SqlClient.SqlParameter("@SRS_TEACHER", HF_Teacher.Value)
                            param(6) = New SqlClient.SqlParameter("@SRS_OWN_INSTRUMENT", "0")

                            param(7) = New SqlClient.SqlParameter("@SERVICE_TERM_XML", SERVICE_TERM_XML)
                            param(8) = New SqlClient.SqlParameter("@STM_IDS", STM_ID_Save) 'STM_ID_Save
                            param(9) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                            param(9).Direction = ParameterDirection.ReturnValue
                            param(10) = New SqlClient.SqlParameter("@MSG", SqlDbType.VarChar, 1000)
                            param(10).Direction = ParameterDirection.Output
                            param(11) = New SqlClient.SqlParameter("@SSR_REQ_USR", Session("username"))
                            SqlHelper.ExecuteNonQuery(sqltran, CommandType.StoredProcedure, "[ONLINE].[SAVE_SERVICES_REQUESTED_STUDENT]", param)
                            Dim ReturnFlag As Integer = param(9).Value


                            If ReturnFlag = 600 Then
                                Dim SSR_ID As String = param(10).Value
                                bSSRID = SSR_ID
                                lnkbtnProceed2Payment.PostBackUrl = "ServiceRequestPayment.aspx?SSRID=" + Encr_decrData.Encrypt(SSR_ID)

                                'Dim param2(7) As SqlClient.SqlParameter
                                'param2(0) = New SqlClient.SqlParameter("@SSR_ID", SSR_ID)
                                'param2(1) = New SqlClient.SqlParameter("@SRS_DISCOUNT", 0)
                                ''param(2) = New SqlClient.SqlParameter("@SRS_FROMDATE", txtfromdate.Text)
                                'param2(2) = New SqlClient.SqlParameter("@OPTION", 1)
                                'param2(3) = New SqlClient.SqlParameter("@MSG", SqlDbType.VarChar, 1000)
                                'param2(3).Direction = ParameterDirection.Output
                                'param2(4) = New SqlClient.SqlParameter("@SSR_REMARKS", "Auto-Approval")

                                'SqlHelper.ExecuteNonQuery(sqltran, CommandType.StoredProcedure, "[ONLINE].[SERVICES_APPROVE_SVC_REQUEST]", param2)
                                'lblerror.Text = param2(3).Value

                                'callSend_mail(hf_STU_ID.Value, SSR_ID, HF_SVC_ID.Value, sqltran, 4)
                            End If
                            STM_ID_Save = ""
                            flag = 0
                        End If
                    End If
                Next row
            Next
            sqltran.Commit()
            BindStudentServices()
            Delete_ServicesRequested("", "", "0")
            'lblerror.Text = "Service Saved Sucessfully...!!!!"

        Catch ex As Exception
            sqltran.Rollback()
            con.Close()
            lblerror.Text = ex.Message
        Finally
            ' BindStudentServices()
        End Try
        If bSSRID <> "" Then
            Response.Redirect("ServiceRequestPayment.aspx?SSRID=" + Encr_decrData.Encrypt(bSSRID))
        End If
    End Sub
#End Region

    Protected Sub ddlServices_SelectedIndexChanged(ByVal o As Object, ByVal e As System.EventArgs) Handles ddlServices.SelectedIndexChanged
        bindTerms()
        ddlLevels.SelectedValue = "0"
        HF_svc_count.Value = ""
        HF_MAX_SVC.Value = ""
    End Sub
    Sub hidingtables()
        Dim bTERMWISE As String = "0"
        Dim SSC_bSHOWFLD As String = "0"

        Dim ACDID_GRDID As String() = New String(9) {}
        ACDID_GRDID = ddlAcademicYear.SelectedValue.Split("_")
        Dim param1(9) As SqlClient.SqlParameter
        param1(0) = New SqlClient.SqlParameter("@SSC_ID", ddlSVCategory.SelectedValue)
        param1(1) = New SqlClient.SqlParameter("@ACD_ID", ACDID_GRDID(0))
        param1(2) = New SqlClient.SqlParameter("@STU_ID", ddlStudents.SelectedValue)

        Dim con As String = ConnectionManger.GetOASISConnectionString
        Using EmailHost_reader As SqlDataReader = SqlHelper.ExecuteReader(con, CommandType.StoredProcedure, "[ONLINE].[SERVICES_ISTERM_WISE]", param1)
            If EmailHost_reader.HasRows Then
                While EmailHost_reader.Read
                    bTERMWISE = Convert.ToString(EmailHost_reader("SVB_bTERMWISE"))
                    SSC_bSHOWFLD = Convert.ToString(EmailHost_reader("SSC_bSHOWFLD"))
                End While
            End If
        End Using



        If bTERMWISE <> "True" Then
            ' tr_Terms.Attributes.Add(    
            RequiredFieldValidator1.Visible = True
            Comparevalidator1.Visible = False
            tr_Terms.Attributes.Add("style", "display:none")
            tr_CurrentTeacher.Attributes.Add("style", "display:none")
            tr_Level.Attributes.Add("style", "display:none")
            tr_Music.Attributes.Add("style", "display:none")
            tr_MusicServiceReq.Attributes.Add("style", "display:none")
            trOwnIns.Attributes.Add("style", "display:none")
            tr_ServiceStartdate.Visible = True '("style", "display:block")
            btn_SaveRequest.Enabled = True

            ServicesRequested.Rows.Clear()
            gvServicesRequested.DataSource = ServicesRequested
            gvServicesRequested.DataBind()
        Else
            Comparevalidator1.Visible = True
            RequiredFieldValidator1.Visible = False
            ' CompareValidator2.Visible = False
            tr_Terms.Attributes.Add("style", "display:block")
            tr_CurrentTeacher.Attributes.Add("style", "display:block")
            tr_Level.Attributes.Add("style", "display:block")
            tr_Music.Attributes.Add("style", "display:block")
            tr_MusicServiceReq.Attributes.Add("style", "display:block")
            trOwnIns.Attributes.Add("style", "display:block")
            tr_ServiceStartdate.Visible = False '("style", "display:none")
        End If

        If SSC_bSHOWFLD <> "True" Then
            tr_CurrentTeacher.Attributes.Add("style", "display:none")
            tr_Level.Attributes.Add("style", "display:none")
            Comparevalidator1.ValidationGroup = "save"
        Else
            tr_CurrentTeacher.Attributes.Add("style", "display:block")
            tr_Level.Attributes.Add("style", "display:block")
        End If
    End Sub
    Sub enableButton()
        If gvServicesRequested.Rows.Count > 0 Then 'Itemsa
            btn_SaveRequest.Enabled = True
        Else
            btn_SaveRequest.Enabled = False
        End If
    End Sub
    Protected Sub ddlSVCategory_SelectedIndexChanged(ByVal o As Object, ByVal e As System.EventArgs) Handles ddlSVCategory.SelectedIndexChanged
        ddlLevels.SelectedValue = "0"
        If gvServicesRequested.Rows.Count > 0 Then
            pnlSvcCatChange.Visible = True
            ViewState("SVCategory_Oldvalue") = ViewState("SVC_Value")
        Else
            BindServices()
            HF_svc_count.Value = ""
            HF_MAX_SVC.Value = ""
            'CompareValidator2.ValueToCompare = DateTime.Now.Date
        End If
        ViewState("SVC_Value") = ddlSVCategory.SelectedValue
    End Sub
    Protected Sub btn_Proceed_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Proceed.Click
        BindServices()
        HF_svc_count.Value = ""
        HF_MAX_SVC.Value = ""
        ServicesRequested.Rows.Clear()
        gvServicesRequested.DataSource = ServicesRequested
        gvServicesRequested.DataBind()
        pnlSvcCatChange.Visible = False
    End Sub

    Protected Sub btn_cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_cancel.Click
        If (ViewState("SVCategory_Oldvalue") <> "0") Then
            ddlSVCategory.SelectedValue = ViewState("SVCategory_Oldvalue")
        End If
        pnlSvcCatChange.Visible = False
    End Sub
    Protected Sub btn_SaveRequest_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_SaveRequest.Click

        If gvServicesRequested.Rows.Count > 0 Then
            SaveServiceRequest()
        Else
            If ddlServices.Items.Count > 0 Then
                saveNormalService()
            Else
                lblerror.Text = "No services avaliable to save"
            End If
        End If

    End Sub
    Sub saveNormalService()
        Dim SSRID As String = String.Empty
        Dim constring As String = ConnectionManger.GetOASISConnectionString
        Dim con As SqlConnection = New SqlConnection(constring)
        con.Open()
        Dim sqltran As SqlTransaction
        sqltran = con.BeginTransaction("trans")
        Try
            Dim ACDID_GRDID As String() = New String(9) {}
            ACDID_GRDID = ddlAcademicYear.SelectedValue.Split("_")

            Dim param(11) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@SSR_STU_ID", ddlStudents.SelectedValue)
            param(1) = New SqlClient.SqlParameter("@SSR_ACD_ID", ACDID_GRDID(0))
            param(2) = New SqlClient.SqlParameter("@SSR_SVC_ID", ddlServices.SelectedValue)
            param(3) = New SqlClient.SqlParameter("@SSR_SERVICE_STARTDT", txtfromdate.Text)

            param(4) = New SqlClient.SqlParameter("@MSG", SqlDbType.VarChar, 1000)
            param(4).Direction = ParameterDirection.Output

            param(5) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            param(5).Direction = ParameterDirection.ReturnValue

            param(6) = New SqlClient.SqlParameter("@SSC_ID", ddlSVCategory.SelectedValue)

            param(7) = New SqlClient.SqlParameter("@SRS_LEVEL", IIf(ddlLevels.SelectedValue <> "", ddlLevels.SelectedItem.Text, DBNull.Value))
            param(8) = New SqlClient.SqlParameter("@SRS_TEACHER", txtCurrentTeacher.Text)

            param(9) = New SqlClient.SqlParameter("@SSRID", SqlDbType.VarChar, 1000)
            param(9).Direction = ParameterDirection.Output

            param(10) = New SqlClient.SqlParameter("@SSR_REQ_USR", Session("username"))
            SqlHelper.ExecuteNonQuery(sqltran, CommandType.StoredProcedure, "[ONLINE].[SAVE_SERVICES_REQUESTED_GENERAL]", param)


            Dim ReturnFlag As Integer = param(5).Value
            If ReturnFlag = 600 Then
                SSRID = param(9).Value
                'callSend_mail(ddlStudents.SelectedValue, SSRID, ddlServices.SelectedValue, sqltran, 4)
                lblerror.Text = "Service Saved Sucessfully...!!!!"
                sqltran.Commit()
            Else
                lblerror.Text = param(4).Value
                sqltran.Rollback()
            End If
            BindStudentServices()
        Catch ex As Exception
            sqltran.Rollback()
            lblerror.Text = ex.Message
        Finally
            BindStudentServices()
        End Try
        If SSRID <> "" Then
            Response.Redirect("ServiceRequestPayment.aspx?SSRID=" + Encr_decrData.Encrypt(SSRID))
        End If

    End Sub

    'Sub SaveServiceRequest()
    '    Dim constring As String = ConnectionManger.GetOASISConnectionString
    '    Dim con As SqlConnection = New SqlConnection(constring)
    '    con.Open()
    '    Dim sqltran As SqlTransaction
    '    sqltran = con.BeginTransaction("trans")
    '    Dim bsaved As Integer = 0
    '    Dim bSSRID As String = ""
    '    Try
    '        For Each gr As GridViewRow In gvServicesRequested.Rows  ' GridDataItem    Items 

    '            Dim HF_SVG_ID As HiddenField = DirectCast(gr.FindControl("HF_SVG_ID"), HiddenField)
    '            Dim hf_STU_ID As HiddenField = DirectCast(gr.FindControl("hf_STU_ID"), HiddenField)
    '            Dim hf_STM_ID As HiddenField = DirectCast(gr.FindControl("hf_STM_ID"), HiddenField)
    '            Dim HF_SVC_ID As HiddenField = DirectCast(gr.FindControl("HF_SVC_ID"), HiddenField)
    '            Dim HF_ACD_ID As HiddenField = DirectCast(gr.FindControl("HF_ACD_ID"), HiddenField)

    '            Dim ServicesRequested_DT1 As DataTable = ServicesRequested
    '            Dim dvApprovalLevels As DataView = ServicesRequested_DT1.DefaultView
    '            dvApprovalLevels.RowFilter = "STU_ID =" + hf_STU_ID.Value + " and  ACD_ID = " + HF_ACD_ID.Value + " and SVC_ID=" + HF_SVC_ID.Value + ""
    '            Dim ServicesRequested_DT As DataTable = dvApprovalLevels.ToTable()
    '            Dim SERVICE_TERM_XML As String = "<SERVICES>"
    '            Dim Terms_Service As String = ""

    '            For Each d As DataRow In ServicesRequested_DT.Rows
    '                If Terms_Service <> "" Then
    '                    Terms_Service = Terms_Service + "|" + d("ID").ToString()
    '                Else
    '                    Terms_Service = d("ID").ToString()
    '                End If
    '                SERVICE_TERM_XML = SERVICE_TERM_XML + "<TERM STM_ID='" + d("ID").ToString() + "' SRS_FEE='" + d("AMOUNT").ToString() + "' SRS_LEVEL='" + d("LEVEL").ToString() + "' SRS_TEACHER='" + d("CurrentTeacher").ToString() + "' SRS_OWN_INSTRUMENT='" + d("OWNINSTRUMENT").ToString() + "' STU_ID='" + d("STU_ID").ToString() + "' SVC_ID='" + d("SVC_ID").ToString() + "' ></TERM>"
    '            Next

    '            SERVICE_TERM_XML = SERVICE_TERM_XML + "</SERVICES>"

    '            Dim Terms_SVC As String() = New String(9) {}
    '            Terms_SVC = Terms_Service.Split("|")

    '            Dim param1(2) As SqlClient.SqlParameter
    '            param1(0) = New SqlClient.SqlParameter("@SSR_SVG_ID", HF_SVG_ID.Value)
    '            Dim ds As New DataSet
    '            ds = SqlHelper.ExecuteDataset(sqltran, CommandType.StoredProcedure, "[ONLINE].[SERVICES_GET_TRM_CONTINUES]", param1)
    '            Dim dt_TRM_CONTINUES As New DataTable
    '            dt_TRM_CONTINUES = ds.Tables(0)
    '            Dim flag As Integer = 0
    '            Dim j As Integer = 0
    '            Dim STM_ID_Save As String = ""
    '            Dim i As Integer = 0
    '            Dim failure As Boolean = False
    '            For Each row As DataRow In dt_TRM_CONTINUES.Rows
    '                i = i + 1
    '                If (j < Terms_SVC.Length) Then


    '                    If Terms_SVC(j) = row("STM_ID") Then

    '                        If STM_ID_Save <> "" Then
    '                            STM_ID_Save = STM_ID_Save + "|" + Terms_SVC(j).ToString()
    '                        Else
    '                            STM_ID_Save = Terms_SVC(j).ToString()
    '                        End If
    '                        flag = 0
    '                        j = j + 1
    '                    Else
    '                        flag = 1
    '                        j = j + 1

    '                    End If

    '                    If flag = 1 Or i = dt_TRM_CONTINUES.Rows.Count Then
    '                        If STM_ID_Save <> "" Then
    '                            Dim lblAMOUNT As Label = DirectCast(gr.FindControl("lblAMOUNT"), Label)

    '                            Dim HF_Level As HiddenField = DirectCast(gr.FindControl("HF_Level"), HiddenField)
    '                            Dim HF_Teacher As HiddenField = DirectCast(gr.FindControl("HF_Teacher"), HiddenField)

    '                            Dim param(12) As SqlClient.SqlParameter
    '                            param(0) = New SqlClient.SqlParameter("@SSR_SVG_ID", HF_SVG_ID.Value)
    '                            param(1) = New SqlClient.SqlParameter("@SSR_STU_ID", hf_STU_ID.Value)
    '                            param(2) = New SqlClient.SqlParameter("@SRS_STM_ID", hf_STM_ID.Value)
    '                            param(3) = New SqlClient.SqlParameter("@SRS_FEE", lblAMOUNT.Text)
    '                            param(4) = New SqlClient.SqlParameter("@SRS_LEVEL", HF_Level.Value)
    '                            param(5) = New SqlClient.SqlParameter("@SRS_TEACHER", HF_Teacher.Value)
    '                            param(6) = New SqlClient.SqlParameter("@SRS_OWN_INSTRUMENT", "0")

    '                            param(7) = New SqlClient.SqlParameter("@SERVICE_TERM_XML", SERVICE_TERM_XML)
    '                            param(8) = New SqlClient.SqlParameter("@STM_IDS", STM_ID_Save) 'STM_ID_Save
    '                            param(9) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
    '                            param(9).Direction = ParameterDirection.ReturnValue
    '                            param(10) = New SqlClient.SqlParameter("@MSG", SqlDbType.VarChar, 1000)
    '                            param(10).Direction = ParameterDirection.Output
    '                            SqlHelper.ExecuteNonQuery(sqltran, CommandType.StoredProcedure, "[ONLINE].[SAVE_SERVICES_REQUESTED_STUDENT]", param)
    '                            Dim ReturnFlag As Integer = param(9).Value


    '                            If ReturnFlag = 600 Then
    '                                Dim SSR_ID As String = param(10).Value
    '                                bSSRID = SSR_ID
    '                                lnkbtnProceed2Payment.PostBackUrl = "ServiceRequestPayment.aspx?SSRID=" + Encr_decrData.Encrypt(SSR_ID)

    '                                'Dim param2(7) As SqlClient.SqlParameter
    '                                'param2(0) = New SqlClient.SqlParameter("@SSR_ID", SSR_ID)
    '                                'param2(1) = New SqlClient.SqlParameter("@SRS_DISCOUNT", 0)
    '                                ''param(2) = New SqlClient.SqlParameter("@SRS_FROMDATE", txtfromdate.Text)
    '                                'param2(2) = New SqlClient.SqlParameter("@OPTION", 1)
    '                                'param2(3) = New SqlClient.SqlParameter("@MSG", SqlDbType.VarChar, 1000)
    '                                'param2(3).Direction = ParameterDirection.Output
    '                                'param2(4) = New SqlClient.SqlParameter("@SSR_REMARKS", "Auto-Approval")

    '                                'SqlHelper.ExecuteNonQuery(sqltran, CommandType.StoredProcedure, "[ONLINE].[SERVICES_APPROVE_SVC_REQUEST]", param2)
    '                                'lblerror.Text = param2(3).Value

    '                                'callSend_mail(hf_STU_ID.Value, SSR_ID, HF_SVC_ID.Value, sqltran, 4)
    '                            End If
    '                        End If
    '                        STM_ID_Save = ""
    '                        flag = 0
    '                    End If
    '                End If
    '            Next row
    '        Next
    '        'sqltran.Rollback()
    '        sqltran.Commit()
    '        'pnlProceed2Payment.Visible = True
    '        'lblProceed2Payment.Text = "You have registered successfully for selected " + lblLanguage.Text + "(s)</br> for student " + ddlStudents.SelectedItem.Text + " and are liable to pay the application fee."
    '        bsaved = 1

    '        BindStudentServices()
    '        Delete_ServicesRequested("", "", "0")
    '        lblerror.Text = "Service Saved Sucessfully...!!!!"

    '    Catch ex As Exception
    '        sqltran.Rollback()
    '        con.Close()
    '        lblerror.Text = ex.Message
    '    Finally
    '        ' BindStudentServices()
    '    End Try
    '    If bsaved = 1 And bSSRID <> "" Then
    '        Response.Redirect("ServiceRequestPayment.aspx?SSRID=" + Encr_decrData.Encrypt(bSSRID))
    '    End If
    'End Sub
    'Sub SaveServiceRequest()
    '    Dim constring As String = ConnectionManger.GetOASISConnectionString
    '    Dim con As SqlConnection = New SqlConnection(constring)
    '    con.Open()
    '    Dim sqltran As SqlTransaction
    '    sqltran = con.BeginTransaction("trans")
    '    Dim bsaved As Integer = 0
    '    Dim bSSRID As String = ""
    '    Try
    '        For Each gr As GridViewRow In gvServicesRequested.Rows  ' GridDataItem    Items 

    '            Dim HF_SVG_ID As HiddenField = DirectCast(gr.FindControl("HF_SVG_ID"), HiddenField)
    '            Dim hf_STU_ID As HiddenField = DirectCast(gr.FindControl("hf_STU_ID"), HiddenField)
    '            Dim hf_STM_ID As HiddenField = DirectCast(gr.FindControl("hf_STM_ID"), HiddenField)
    '            Dim HF_SVC_ID As HiddenField = DirectCast(gr.FindControl("HF_SVC_ID"), HiddenField)
    '            Dim HF_ACD_ID As HiddenField = DirectCast(gr.FindControl("HF_ACD_ID"), HiddenField)

    '            Dim ServicesRequested_DT1 As DataTable = ServicesRequested
    '            Dim dvApprovalLevels As DataView = ServicesRequested_DT1.DefaultView
    '            dvApprovalLevels.RowFilter = "STU_ID =" + hf_STU_ID.Value + " and  ACD_ID = " + HF_ACD_ID.Value + " and SVC_ID=" + HF_SVC_ID.Value + ""
    '            Dim ServicesRequested_DT As DataTable = dvApprovalLevels.ToTable()
    '            Dim SERVICE_TERM_XML As String = "<SERVICES>"
    '            Dim Terms_Service As String = ""

    '            For Each d As DataRow In ServicesRequested_DT.Rows
    '                If Terms_Service <> "" Then
    '                    Terms_Service = Terms_Service + "|" + d("ID").ToString()
    '                Else
    '                    Terms_Service = d("ID").ToString()
    '                End If
    '                SERVICE_TERM_XML = SERVICE_TERM_XML + "<TERM STM_ID='" + d("ID").ToString() + "' SRS_FEE='" + d("AMOUNT").ToString() + "' SRS_LEVEL='" + d("LEVEL").ToString() + "' SRS_TEACHER='" + d("CurrentTeacher").ToString() + "' SRS_OWN_INSTRUMENT='" + d("OWNINSTRUMENT").ToString() + "' STU_ID='" + d("STU_ID").ToString() + "' SVC_ID='" + d("SVC_ID").ToString() + "' ></TERM>"
    '            Next

    '            SERVICE_TERM_XML = SERVICE_TERM_XML + "</SERVICES>"

    '            Dim Terms_SVC As String() = New String(9) {}
    '            Terms_SVC = Terms_Service.Split("|")

    '            Dim param1(2) As SqlClient.SqlParameter
    '            param1(0) = New SqlClient.SqlParameter("@SSR_SVG_ID", HF_SVG_ID.Value)
    '            Dim ds As New DataSet
    '            ds = SqlHelper.ExecuteDataset(sqltran, CommandType.StoredProcedure, "[ONLINE].[SERVICES_GET_TRM_CONTINUES]", param1)
    '            Dim dt_TRM_CONTINUES As New DataTable
    '            dt_TRM_CONTINUES = ds.Tables(0)
    '            Dim flag As Integer = 0
    '            Dim j As Integer = 0
    '            Dim STM_ID_Save As String = ""
    '            Dim i As Integer = 0
    '            Dim failure As Boolean = False
    '            For Each row As DataRow In dt_TRM_CONTINUES.Rows
    '                i = i + 1
    '                If (j < Terms_SVC.Length) Then

    '                    If Terms_SVC(j) = row("STM_ID") Then

    '                        If STM_ID_Save <> "" Then
    '                            STM_ID_Save = STM_ID_Save + "|" + Terms_SVC(j).ToString()
    '                        Else
    '                            STM_ID_Save = Terms_SVC(j).ToString()
    '                        End If
    '                        flag = 0
    '                        j = j + 1
    '                    Else
    '                        flag = 1
    '                        j = j + 1

    '                    End If
    '                End If

    '                If flag = 1 Or i = dt_TRM_CONTINUES.Rows.Count Then

    '                    If STM_ID_Save <> "" Then
    '                        Dim lblAMOUNT As Label = DirectCast(gr.FindControl("lblAMOUNT"), Label)

    '                        Dim HF_Level As HiddenField = DirectCast(gr.FindControl("HF_Level"), HiddenField)
    '                        Dim HF_Teacher As HiddenField = DirectCast(gr.FindControl("HF_Teacher"), HiddenField)

    '                        Dim param(12) As SqlClient.SqlParameter
    '                        param(0) = New SqlClient.SqlParameter("@SSR_SVG_ID", HF_SVG_ID.Value)
    '                        param(1) = New SqlClient.SqlParameter("@SSR_STU_ID", hf_STU_ID.Value)
    '                        param(2) = New SqlClient.SqlParameter("@SRS_STM_ID", hf_STM_ID.Value)
    '                        param(3) = New SqlClient.SqlParameter("@SRS_FEE", lblAMOUNT.Text)
    '                        param(4) = New SqlClient.SqlParameter("@SRS_LEVEL", HF_Level.Value)
    '                        param(5) = New SqlClient.SqlParameter("@SRS_TEACHER", HF_Teacher.Value)
    '                        param(6) = New SqlClient.SqlParameter("@SRS_OWN_INSTRUMENT", "0")

    '                        param(7) = New SqlClient.SqlParameter("@SERVICE_TERM_XML", SERVICE_TERM_XML)
    '                        param(8) = New SqlClient.SqlParameter("@STM_IDS", STM_ID_Save) 'STM_ID_Save
    '                        param(9) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
    '                        param(9).Direction = ParameterDirection.ReturnValue
    '                        param(10) = New SqlClient.SqlParameter("@MSG", SqlDbType.VarChar, 1000)
    '                        param(10).Direction = ParameterDirection.Output
    '                        SqlHelper.ExecuteNonQuery(sqltran, CommandType.StoredProcedure, "[ONLINE].[SAVE_SERVICES_REQUESTED_STUDENT]", param)
    '                        Dim ReturnFlag As Integer = param(9).Value


    '                        If ReturnFlag = 600 Then
    '                            Dim SSR_ID As String = param(10).Value
    '                            bSSRID = SSR_ID
    '                            lnkbtnProceed2Payment.PostBackUrl = "ServiceRequestPayment.aspx?SSRID=" + Encr_decrData.Encrypt(SSR_ID)

    '                            'Dim param2(7) As SqlClient.SqlParameter
    '                            'param2(0) = New SqlClient.SqlParameter("@SSR_ID", SSR_ID)
    '                            'param2(1) = New SqlClient.SqlParameter("@SRS_DISCOUNT", 0)
    '                            ''param(2) = New SqlClient.SqlParameter("@SRS_FROMDATE", txtfromdate.Text)
    '                            'param2(2) = New SqlClient.SqlParameter("@OPTION", 1)
    '                            'param2(3) = New SqlClient.SqlParameter("@MSG", SqlDbType.VarChar, 1000)
    '                            'param2(3).Direction = ParameterDirection.Output
    '                            'param2(4) = New SqlClient.SqlParameter("@SSR_REMARKS", "Auto-Approval")

    '                            'SqlHelper.ExecuteNonQuery(sqltran, CommandType.StoredProcedure, "[ONLINE].[SERVICES_APPROVE_SVC_REQUEST]", param2)
    '                            'lblerror.Text = param2(3).Value

    '                            'callSend_mail(hf_STU_ID.Value, SSR_ID, HF_SVC_ID.Value, sqltran, 4)
    '                        End If
    '                    End If
    '                    STM_ID_Save = ""
    '                    flag = 0
    '                End If
    '            Next row
    '        Next
    '        ' sqltran.Rollback()
    '        sqltran.Commit()
    '        'pnlProceed2Payment.Visible = True
    '        'lblProceed2Payment.Text = "You have registered successfully for selected " + lblLanguage.Text + "(s)</br> for student " + ddlStudents.SelectedItem.Text + " and are liable to pay the application fee."
    '        bsaved = 1

    '        BindStudentServices()
    '        Delete_ServicesRequested("", "", "0")
    '        'lblerror.Text = "Service Saved Sucessfully...!!!!"

    '    Catch ex As Exception
    '        sqltran.Rollback()
    '        con.Close()
    '        lblerror.Text = ex.Message
    '    Finally
    '        ' BindStudentServices()
    '    End Try
    '    If bsaved = 1 And bSSRID <> "" Then
    '        Response.Redirect("ServiceRequestPayment.aspx?SSRID=" + Encr_decrData.Encrypt(bSSRID))
    '    End If
    'End Sub
    Protected Sub lnkbtnProceed2Payment_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkbtnProceed2Payment.Click

    End Sub
    Public Sub BindStudentServices()
        Dim con As String = ConnectionManger.GetOASISConnectionString
        Dim param(4) As SqlClient.SqlParameter
        param(0) = New SqlClient.SqlParameter("@OLU_ID", Session("OLU_ID"))
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "[ONLINE].[SERVICES_GETSTUDENT_REQUESTED_SVC]", param)
        gvStudentServicesDetails.DataSource = ds.Tables(0)
        gvStudentServicesDetails.DataBind()

    End Sub

    Protected Sub img_delete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)

        Dim hf_STU_ID As HiddenField = sender.parent.findcontrol("hf_STU_ID")
        Dim hf_STM_ID As HiddenField = sender.parent.findcontrol("hf_STM_ID")

        Delete_ServicesRequested(hf_STM_ID.Value, hf_STU_ID.Value, "1")
    End Sub
    Sub Delete_ServicesRequested(ByVal STM_ID As String, ByVal STU_ID As String, ByVal flag As String)
        Dim ServicesRequested_DT As DataTable = ServicesRequested
        Dim dvApprovalLevels As DataView = ServicesRequested_DT.DefaultView
        If flag = "0" Then
            dvApprovalLevels.RowFilter = "ID = 0"
        Else
            dvApprovalLevels.RowFilter = "ID <>" + STM_ID + " or STU_ID <> " + STU_ID + ""
        End If

        ServicesRequested_DT = dvApprovalLevels.ToTable()
        ServicesRequested_DT.AcceptChanges()
        ServicesRequested = ServicesRequested_DT


        gvServicesRequested.DataSource = ServicesRequested_DT
        gvServicesRequested.DataBind()
        bindTerms()
        enableButton()
    End Sub
    Sub callSend_mail(ByVal STU_ID As String, ByVal SSR_ID As String, ByVal lblSERVICE As String, ByVal sqltran As SqlTransaction, ByVal status As String)

        Dim htmlString As String = String.Empty
        Dim html As String = ScreenScrapeHtml(Server.MapPath("EmailContent\emailTemplate.htm"))
        Dim msg As New MailMessage

        Dim BSC_HOST As String = String.Empty
        Dim BSC_USERNAME As String = String.Empty
        Dim BSC_PASSWORD As String = String.Empty
        Dim SUBJECT As String = String.Empty
        Dim EMAIL_ADD As String = String.Empty
        Dim BSU_ID As String = String.Empty
        Dim ACD_ID As String = String.Empty


        Dim STUDENTNAME As String = String.Empty
        Dim STUDENTNO As String = String.Empty
        Dim SERVICE As String = String.Empty
        Dim BSU_NAME As String = String.Empty
        Dim SERVICE_CAT As String = String.Empty
        'html = html.Replace("@Content", htmlString)

        Dim pParms(4) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@STU_ID", STU_ID)
        pParms(2) = New SqlClient.SqlParameter("@SSR_ID", SSR_ID)
        pParms(1) = New SqlClient.SqlParameter("@OPTION", status)

        Using EmailHost_reader As SqlDataReader = SqlHelper.ExecuteReader(sqltran, CommandType.StoredProcedure, "[ONLINE].[SERVICES_EMAIL_TEXT]", pParms)
            If EmailHost_reader.HasRows Then
                While EmailHost_reader.Read
                    SUBJECT = Convert.ToString(EmailHost_reader("SUBJECT"))
                    SUBJECT = SUBJECT.Replace("@servicecategory", Convert.ToString(EmailHost_reader("SERVICE_CAT")))
                    SUBJECT = SUBJECT.Replace("@service", Convert.ToString(EmailHost_reader("SERVICE")))


                    htmlString = Convert.ToString(EmailHost_reader("EMAIL_MESSAGE"))
                    html = html.Replace("@Content", htmlString)
                    BSU_ID = Convert.ToString(EmailHost_reader("BSU_ID"))
                    html = html.Replace("@school", Convert.ToString(EmailHost_reader("BSU_NAME")))
                    html = html.Replace("@service", Convert.ToString(EmailHost_reader("SERVICE")))
                    html = html.Replace("@name ", Convert.ToString(EmailHost_reader("STUDENTNAME")))
                    html = html.Replace("@studentno ", Convert.ToString(EmailHost_reader("STUDENTNO")))


                    SERVICE_CAT = Convert.ToString(EmailHost_reader("SERVICE_CAT"))
                    STUDENTNO = Convert.ToString(EmailHost_reader("STUDENTNO"))
                    STUDENTNAME = Convert.ToString(EmailHost_reader("STUDENTNAME"))
                    SERVICE = Convert.ToString(EmailHost_reader("SERVICE"))
                    BSU_NAME = Convert.ToString(EmailHost_reader("BSU_NAME"))

                    EMAIL_ADD = Convert.ToString(EmailHost_reader("EMAIL_ID"))
                    ACD_ID = Convert.ToString(EmailHost_reader("ACD_ID"))
                End While
            End If
        End Using

        Dim param(9) As SqlClient.SqlParameter
        param(0) = New SqlClient.SqlParameter("@EML_BSU_ID", BSU_ID)
        param(1) = New SqlClient.SqlParameter("@EML_TYPE", IIf(status = "1", "SERVICE-A", "SERVICE-R"))
        param(2) = New SqlClient.SqlParameter("@EML_PROFILE_ID", SSR_ID)
        param(3) = New SqlClient.SqlParameter("@EML_TOEMAIL", EMAIL_ADD) 'EMAIL_ADD)
        param(4) = New SqlClient.SqlParameter("@EML_SUBJECT", SUBJECT)
        param(5) = New SqlClient.SqlParameter("@EML_MESSAGE", html)
        SqlHelper.ExecuteNonQuery(sqltran, CommandType.StoredProcedure, "dbo.InsertIntoEmailSendSchedule", param)



        html = ScreenScrapeHtml(Server.MapPath("EmailContent\emailTemplate.htm"))
        Dim param1(9) As SqlClient.SqlParameter
        param1(0) = New SqlClient.SqlParameter("@SBA_BSU_ID", BSU_ID)
        param1(1) = New SqlClient.SqlParameter("@SBA_ACD_ID", ACD_ID)
        param1(2) = New SqlClient.SqlParameter("@SBA_SVC_ID", lblSERVICE)
        param1(3) = New SqlClient.SqlParameter("@OPTION", status)
        Dim staI As Integer = 0
        EMAIL_ADD = ""
        Using EmailHost_reader As SqlDataReader = SqlHelper.ExecuteReader(sqltran, CommandType.StoredProcedure, "[ONLINE].[SERVICES_APPROVES_EMAIL]", param1)
            If EmailHost_reader.HasRows Then
                While EmailHost_reader.Read
                    SUBJECT = Convert.ToString(EmailHost_reader("SUBJECT"))
                    SUBJECT = SUBJECT.Replace("@servicecategory", SERVICE_CAT)
                    SUBJECT = SUBJECT.Replace("@service", SERVICE)

                    htmlString = Convert.ToString(EmailHost_reader("EMAIL_MESSAGE"))
                    html = html.Replace("@Content", htmlString)
                    html = html.Replace("@school", BSU_NAME)
                    html = html.Replace("@service", SERVICE)
                    html = html.Replace("@name ", STUDENTNAME)
                    html = html.Replace("@studentno ", STUDENTNO)
                    EMAIL_ADD = Convert.ToString(EmailHost_reader("EMD_EMAIL")) + ";" + EMAIL_ADD
                    staI = staI + 1
                End While
            End If
        End Using

        If (staI > 0) Then
            Dim EMAIL_ADD_TO As String() = New String(9) {}
            EMAIL_ADD_TO = EMAIL_ADD.Split(";")
            For Each toEmail As String In EMAIL_ADD_TO
                If (toEmail <> "") Then
                    param(0) = New SqlClient.SqlParameter("@EML_BSU_ID", BSU_ID)
                    param(1) = New SqlClient.SqlParameter("@EML_TYPE", IIf(status = "1", "SERVICE-A", "SERVICE-R"))
                    param(2) = New SqlClient.SqlParameter("@EML_PROFILE_ID", SSR_ID)
                    param(3) = New SqlClient.SqlParameter("@EML_TOEMAIL", toEmail) 'EMAIL_ADD)
                    param(4) = New SqlClient.SqlParameter("@EML_SUBJECT", SUBJECT)
                    param(5) = New SqlClient.SqlParameter("@EML_MESSAGE", html)
                    SqlHelper.ExecuteNonQuery(sqltran, CommandType.StoredProcedure, "dbo.InsertIntoEmailSendSchedule", param)
                End If
            Next
        End If
    End Sub
    Public Shared Function ScreenScrapeHtml(ByVal url As String) As String
        Dim objRequest As System.Net.WebRequest = System.Net.HttpWebRequest.Create(url)
        Dim sr As New StreamReader(objRequest.GetResponse().GetResponseStream())
        Dim result As String = sr.ReadToEnd()
        sr.Close()
        Return result
    End Function

End Class

