﻿Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports iTextSharp.text
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Imports Microsoft.ApplicationBlocks.Data
Imports System
Imports System.Collections.Generic
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports System.Linq
Imports System.Net
Imports System.Web
Imports System.Web.Script.Services
Imports System.Web.Services
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Telerik.Web.UI


Partial Class Service_HifzTracker
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
        If Session("username") Is Nothing Then
            Response.Redirect("~\General\Home.aspx")
        End If
        If Not IsPostBack Then
            Page.Form.Attributes.Add("enctype", "multipart/form-data")
            lbChildName.Text = Session("STU_NAME")
            Dim islamicStudent As String = isIslamicStudent()
            If islamicStudent = "ISL" Then
                BindHifzTrackerGrid()
            Else
                lblError.Text = " This feature is not applicable to your child"
            End If

        End If
    End Sub
    'Protected Sub btnDownloadPdf_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        If Page.IsValid Then
    '            Dim pdfFileName = "HifzCertificate_" & Guid.NewGuid().ToString()

    '            Dim MyWebClient As WebClient = New WebClient()

    '            ' Read web page HTML to byte array
    '            Dim PageHTMLBytes() As Byte
    '            PageHTMLBytes = MyWebClient.DownloadData("http://localhost:49968/Service/HifzCertificate.aspx")

    '            ' Convert result from byte array to string
    '            ' and display it in TextBox txtPageHTML
    '            Dim oUTF8 As UTF8Encoding = New UTF8Encoding()
    '            Dim htmlContent = oUTF8.GetString(PageHTMLBytes)
    '            Html2PdfPreview(htmlContent, pdfFileName)
    '        End If
    '    Catch ex As Exception
    '        UtilityObj.Errorlog(ex.Message)
    '    End Try
    '    BindHifzTrackerGrid()
    'End Sub
    Private Sub BindHifzTrackerGrid()
        Try
            Dim ds As New DataSet
            ds = GetHifzTrackerData()

            If Not ds Is Nothing Then
                If Not ds.Tables(0) Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                    gv_HifzTracker.DataSource = GetHifzTrackerData()
                    gv_HifzTracker.DataBind()
                Else
                    lblError.Text = " This feature is not available to this grade/section"

                End If
            Else

                lblError.Text = " This feature is not available to this grade/section"
            End If
           
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Private Function GetHifzTrackerData() As DataSet
        Dim schoolId = Convert.ToString(HttpContext.Current.Session("sBsuid"))
        Dim gradeId = Convert.ToString(HttpContext.Current.Session("STU_GRD_ID"))
        Dim acdId = Convert.ToString(HttpContext.Current.Session("STU_ACD_ID"))
        Dim studentId = Convert.ToString(HttpContext.Current.Session("STU_ID"))
        Dim parameters As SqlParameter() = New SqlParameter(5) {}
        parameters(0) = New SqlParameter("@BSU_ID", schoolId)
        parameters(1) = New SqlParameter("@GradeId", gradeId)
        parameters(2) = New SqlParameter("@acdId", acdId)
        parameters(3) = New SqlParameter("@STU_ID", studentId)
        Return SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString(), CommandType.StoredProcedure, "dbo.GetHifzTrackerData", parameters)
    End Function

    Private Function isIslamicStudent() As String
        Dim studentId = Convert.ToString(HttpContext.Current.Session("STU_ID"))
        Dim parameters As SqlParameter() = New SqlParameter(2) {}
        parameters(0) = New SqlParameter("@STU_ID", studentId)
        Dim stuRelgn As String = String.Empty
        Try
            stuRelgn = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString(), CommandType.StoredProcedure, "dbo.IsIslamicStudent", parameters)
            Return stuRelgn
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Function

    Protected Sub gv_HifzTracker_ItemDataBound(sender As Object, e As Telerik.Web.UI.GridItemEventArgs)
        For Each item As GridDataItem In gv_HifzTracker.MasterTableView.Items
            Dim column As Literal = CType(item.FindControl("tblVersesHtml"), Literal)
            Dim trackerId As Integer = Convert.ToInt32(item.GetDataKeyValue("HifzTrackerId"))
            Dim verses As String = item.GetDataKeyValue("NoOfVerses").ToString()
            column.Text = PrepareVersesCheckboxes(trackerId, verses)
            Dim cell = item("DownloadCertificate")
            Dim isCertificateEnabled As Boolean = Convert.ToBoolean(item.GetDataKeyValue("IsCertificateEnabled"))
            If Not isCertificateEnabled Then
                cell.Text = "-"
            End If
            '' Dim lnkCertificatePdf As LinkButton = DirectCast(item.FindControl("DownloadCertificate"), LinkButton)
            ' ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(cell)
        Next
    End Sub
    Private Function PrepareVersesCheckboxes(ByVal trackerId As Integer, ByVal verses As String) As String
        Dim versesList = verses.Split("|"c).ToList()
        Dim allRowsHtml = ""
        For Each verse In versesList
            Dim result = IsVersesCompleted(trackerId, verse)
            allRowsHtml += CreateVerseCheckbox(trackerId, verse, result)
        Next
        Return allRowsHtml
    End Function

    Private Function CreateVerseCheckbox(ByVal trackerId As Integer, ByVal verse As String, ByVal verseData As Tuple(Of Integer?, Integer?, Boolean, Boolean)) As String
        Dim transactionId As Integer? = verseData.Item1
        Dim relationId As Integer? = verseData.Item2
        Dim isFileUploaded As Boolean = verseData.Item3
        Dim isTeacherApproved As Boolean = verseData.Item4
        Dim isCompleted = relationId.HasValue AndAlso relationId.Value > 0
        ' If checkbox is checked & file is not uploaded so we need to show upload icon
        Dim inputFieldCssClass = If(isCompleted Or isFileUploaded, "", "hidden")
        Dim teacherApprovalIcon = If(isTeacherApproved, "<i class='fa fa-check-square' title='Approved By Teacher'></i>", "")
        Dim iconTitle As String = If(isFileUploaded, "Download", "Upload") & " Audio/Video for verse " & verse
        Dim checkboxAttr As String = " data-verseno='" & verse & "' data-trackerid='" & trackerId & "' data-transactionid='" & transactionId & "'"
        Dim uploadIcon As String = "<i class='fa fa-upload upload-field' title='" & iconTitle & "' " & checkboxAttr & " ></i>"
        Dim downloadIcon As String = "<i class='fa fa-download download-field' title='" & iconTitle & "'  data-relationid='" & relationId & "' onclick='fileUploader.initializeDownloadFileRequest($(this));' ></i>"
        Dim deleteIcon As String = "<i class='fa fa-trash delete-attachment' title='Delete the file for verse " & verse & "' data-relationid='" & relationId & "'  " & checkboxAttr & "  ></i>"
        Dim downloadWithDeleteIcon As String = If(isTeacherApproved, downloadIcon, downloadIcon & " &nbsp;" & deleteIcon)
        ' Only if checkbox checked and file uploaded then show downloadicon
        Dim actionIcon As String = If(isCompleted AndAlso isFileUploaded, downloadWithDeleteIcon, uploadIcon)
        Dim checkbox = "<span Class='verse-field'><label><input type='checkbox'  class='verse-check' " & checkboxAttr & " " & If(isTeacherApproved, "disabled", "") & "  " & If(isCompleted, "checked", "") & "/> " & verse & "</label>&nbsp;<span class='file-field " & inputFieldCssClass & "' >" & actionIcon & " &nbsp;" & teacherApprovalIcon & "</span></span>"
        Return checkbox
    End Function


    Private Function UploadFileAndSaveHifzTransaction(ByVal file As FileUpload) As Integer
        Dim transactionId As Integer = 0
        If hdnTrackerId.Value IsNot Nothing Then
            Try
                Dim trackerId As Integer = Convert.ToInt32(hdnTrackerId.Value)
                transactionId = Convert.ToInt32(hdnTransactionId.Value)
                Dim verseNo As String = hdnVerseNo.Value
                Dim isCompleted As String = hdnIsCompleted.Value
                Dim dt As String = DateTime.Now.ToString("G").Replace("/", "").Replace(" ", "").Replace(":", "")
                Dim newFileName As String = "Verse_" & verseNo & "_" & dt & Path.GetExtension(file.FileName)

                transactionId = UpdateTransactionForHifzTracker(trackerId, verseNo, newFileName, isCompleted)
                If transactionId > 0 Then
                    SaveFile(file, newFileName, trackerId)
                End If
                Return transactionId
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
                Return transactionId
            End Try
        End If
        Return transactionId
    End Function

    Private Function SaveFile(ByVal file As FileUpload, ByVal newFileName As String, ByVal trackerId As Integer) As String
        If file IsNot Nothing AndAlso file.HasFile Then
            Dim folderPath As String = Convert.ToString(ConfigurationManager.AppSettings("HifzTrackerAudioFolderLocalPath"))
            Dim fullFilePath As String = Path.Combine(folderPath, newFileName)
            If Not Directory.Exists(folderPath) Then Directory.CreateDirectory(folderPath)
            If System.IO.File.Exists(fullFilePath) Then System.IO.File.Delete(fullFilePath)
            file.SaveAs(fullFilePath)
            Return fullFilePath
        End If
        Return Nothing
    End Function

    Private Function IsVersesCompleted(ByVal trackerId As Integer, ByVal verseNo As String) As Tuple(Of Integer?, Integer?, Boolean, Boolean)
        Dim studentId = Convert.ToString(HttpContext.Current.Session("STU_ID"))
        Dim schoolId = Convert.ToString(HttpContext.Current.Session("sBsuid"))
        Dim parameters As SqlParameter() = New SqlParameter(9) {}
        parameters(0) = New SqlParameter("@HifzTrackerId", trackerId)
        parameters(1) = New SqlParameter("@StudentId", studentId)
        parameters(2) = New SqlParameter("@VerseNo", verseNo)
        parameters(3) = New SqlParameter("@BSU_ID", schoolId)
        parameters(4) = New SqlParameter("@HifzRelationId", SqlDbType.Int)
        parameters(4).Direction = ParameterDirection.Output
        parameters(5) = New SqlParameter("@IsFileUploaded", SqlDbType.Int)
        parameters(5).Direction = ParameterDirection.Output
        parameters(6) = New SqlParameter("@IsApproved", SqlDbType.Int)
        parameters(6).Direction = ParameterDirection.Output
        parameters(7) = New SqlParameter("@UploadedFileName", SqlDbType.NVarChar, 150)
        parameters(7).Direction = ParameterDirection.Output
        parameters(8) = New SqlParameter("@HifzTransactionId", SqlDbType.Int)
        parameters(8).Direction = ParameterDirection.Output
        SqlHelper.ExecuteNonQuery(ConnectionManger.GetOASISConnectionString(), CommandType.StoredProcedure, "dbo.IsVersesCompleted", parameters)
        Dim hifzRelationId As Integer? = If(Not IsDBNull(parameters(4).Value), CInt(parameters(4).Value), Nothing)
        Dim isFileUploaded As Boolean = If(Not IsDBNull(parameters(5).Value), CInt(parameters(5).Value) > 0, False)
        Dim uploadedFileName As String = If(Not IsDBNull(parameters(7).Value), CStr(parameters(7).Value), String.Empty)
        Dim isTeacherApproved As Boolean = If(Not IsDBNull(parameters(6).Value), CInt(parameters(6).Value) > 0, False)
        Dim hifzTransactionId As Integer? = If(Not IsDBNull(parameters(8).Value), CInt(parameters(8).Value), Nothing)
        Return Tuple.Create(hifzTransactionId, hifzRelationId, isFileUploaded, isTeacherApproved)
    End Function

    Private Function UpdateTransactionForHifzTracker(ByVal trackerId As Integer, ByVal verseNo As String, ByVal fileName As String, Optional ByVal isCompleted As Boolean = True) As Integer
        Dim studentId = Convert.ToString(HttpContext.Current.Session("STU_ID"))
        Dim parameters As SqlParameter() = New SqlParameter(9) {}
        parameters(0) = New SqlParameter("@HifzTrackerId", trackerId)
        parameters(1) = New SqlParameter("@StudentId", studentId)
        parameters(2) = New SqlParameter("@VerseNo", verseNo)
        parameters(3) = New SqlParameter("@UploadedFileName", fileName)
        parameters(5) = New SqlParameter("@IsCompleted", isCompleted)
        parameters(4) = New SqlParameter("@output", SqlDbType.Int)
        parameters(4).Direction = ParameterDirection.Output
        SqlHelper.ExecuteNonQuery(ConnectionManger.GetOASISConnectionString(), CommandType.StoredProcedure, "dbo.UpdateTransactionForHifzTracker", parameters)
        Return CInt(parameters(4).Value)
    End Function

    Protected Sub btnHifzTrackerUpload_Click(sender As Object, e As EventArgs)
        Dim functionName As String = String.Empty
        If hifzFileUpload.HasFile Then
            Dim filePath = UploadFileAndSaveHifzTransaction(hifzFileUpload)
            If String.IsNullOrEmpty(filePath) Then
                Response.Redirect(Request.RawUrl & "?error=true")
            End If
            Response.Redirect(Request.RawUrl)
        End If
    End Sub

    <System.Web.Services.WebMethod()>
    Public Shared Function UpdateHifzTransaction(ByVal trackerId As Integer, ByVal verseNo As String, ByVal isCompleted As Boolean) As Integer
        Dim page = New Service_HifzTracker()
        Return page.UpdateTransactionForHifzTracker(trackerId, verseNo, String.Empty, isCompleted)
    End Function

    <System.Web.Services.WebMethod()>
    Public Shared Function DeleteHifzTransactionAttachment(ByVal relationId As Integer) As Boolean
        Try
            Dim query As String = "UPDATE [dbo].[Hifz_tracker_Trans_Reln] SET hft_uploaded_file = NULL WHERE hft_rel_id = " & relationId.ToString()
            Return SqlHelper.ExecuteNonQuery(ConnectionManger.GetOASISConnectionString(), CommandType.Text, query) > 0
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return False
        End Try
    End Function

    <System.Web.Services.WebMethod>
    Public Shared Function SetDownloadPath(ByVal relationId As Integer) As Boolean
        Dim str_query As String = "SELECT rel.hft_uploaded_file FROM [dbo].[Hifz_tracker_Trans_Reln] rel  WHERE hft_rel_id = " & relationId.ToString()
        Dim uploadedFileName As String = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString(), CommandType.Text, str_query)
        Dim objp As Page = New Page()
        objp.Session("HifzFileName") = uploadedFileName
        Return True
    End Function

    <System.Web.Services.WebMethod>
    Public Shared Function SetCertificateDownloadPath(ByVal trackerId As Integer) As Boolean
        Dim objp As Page = New Page()
        objp.Session("HifzTrackerId") = trackerId
        Return True
    End Function


    'Sub LoadReports(ByVal rptClass As rptClass)
    '    Dim rs As New ReportDocument
    '    Try
    '        Dim iRpt As New DictionaryEntry
    '        Dim i As Integer


    '        Dim rptStr As String = ""

    '        Dim crParameterDiscreteValue As ParameterDiscreteValue
    '        Dim crParameterFieldDefinitions As ParameterFieldDefinitions
    '        Dim crParameterFieldLocation As ParameterFieldDefinition
    '        Dim crParameterValues As ParameterValues


    '        With rptClass
    '            rs.Load(.reportPath)

    '            Dim myConnectionInfo As ConnectionInfo = New ConnectionInfo()
    '            myConnectionInfo.ServerName = .crInstanceName
    '            myConnectionInfo.DatabaseName = .crDatabase
    '            myConnectionInfo.UserID = .crUser
    '            myConnectionInfo.Password = .crPassword


    '            SetDBLogonForSubreports(myConnectionInfo, rs, .reportParameters)
    '            SetDBLogonForReport(myConnectionInfo, rs, .reportParameters)
    '            crParameterFieldDefinitions = rs.DataDefinition.ParameterFields
    '            If .reportParameters.Count <> 0 Then
    '                For Each iRpt In .reportParameters
    '                    crParameterFieldLocation = crParameterFieldDefinitions.Item(iRpt.Key.ToString)
    '                    crParameterValues = crParameterFieldLocation.CurrentValues
    '                    crParameterDiscreteValue = New CrystalDecisions.Shared.ParameterDiscreteValue
    '                    crParameterDiscreteValue.Value = iRpt.Value
    '                    crParameterValues.Add(crParameterDiscreteValue)
    '                    crParameterFieldLocation.ApplyCurrentValues(crParameterValues)
    '                Next
    '            End If
    '            If .selectionFormula <> "" Then
    '                rs.RecordSelectionFormula = .selectionFormula
    '            End If
    '            exportReport(rs, CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat)
    '            rs.Close()
    '            rs.Dispose()
    '        End With
    '    Catch ex As Exception
    '        rs.Close()
    '        rs.Dispose()
    '    End Try
    'End Sub

    'Sub LoadReports(ByVal rptClass)
    '    Dim rs As New ReportDocument
    '    Try
    '        Dim iRpt As New DictionaryEntry
    '        Dim i As Integer
    '        Dim newWindow As String

    '        Session("HFrpf_id") = Nothing
    '        Session("HFrsm_id") = Nothing
    '        Dim rptStr As String = ""


    '        '  UtilityObj.Errorlog("Loadreport", "Rajesh")

    '        Dim crParameterDiscreteValue As ParameterDiscreteValue
    '        Dim crParameterFieldDefinitions As ParameterFieldDefinitions
    '        Dim crParameterFieldLocation As ParameterFieldDefinition
    '        Dim crParameterValues As ParameterValues

    '        With rptClass





    '            'C:\Application\ParLogin\ParentLogin\ProgressReports\Rpt\rptTERM_REPORT_OOEHS_DUBAI_GRD05_08.rpt
    '            rs.Load(.reportPath)

    '            'UtilityObj.Errorlog(.reportPath, "Rajesh")

    '            Dim myConnectionInfo As ConnectionInfo = New ConnectionInfo()
    '            myConnectionInfo.ServerName = .crInstanceName
    '            myConnectionInfo.DatabaseName = .crDatabase
    '            myConnectionInfo.UserID = .crUser
    '            myConnectionInfo.Password = .crPassword

    '            ' UtilityObj.Errorlog("Connection", "Rajesh")

    '            SetDBLogonForSubreports(myConnectionInfo, rs, .reportParameters)
    '            SetDBLogonForReport(myConnectionInfo, rs, .reportParameters)

    '            ' UtilityObj.Errorlog("Logging", "Rajesh")
    '            'If .subReportCount <> 0 Then
    '            '    For i = 0 To .subReportCount - 1
    '            '       rs.Subreports(i).VerifyDatabase()
    '            '    Next
    '            'End If


    '            crParameterFieldDefinitions = rs.DataDefinition.ParameterFields
    '            If .reportParameters.Count <> 0 Then
    '                For Each iRpt In .reportParameters
    '                    crParameterFieldLocation = crParameterFieldDefinitions.Item(iRpt.Key.ToString)
    '                    crParameterValues = crParameterFieldLocation.CurrentValues
    '                    crParameterDiscreteValue = New CrystalDecisions.Shared.ParameterDiscreteValue
    '                    crParameterDiscreteValue.Value = iRpt.Value
    '                    crParameterValues.Add(crParameterDiscreteValue)
    '                    crParameterFieldLocation.ApplyCurrentValues(crParameterValues)
    '                Next
    '            End If

    '            ' rs.VerifyDatabase()
    '            '  UtilityObj.Errorlog("Verifying", "Rajesh")

    '            If .selectionFormula <> "" Then
    '                rs.RecordSelectionFormula = .selectionFormula
    '            End If
    '            ' Dim rs1 As New ReportDocument
    '            ' rs1 =rs


    '            'Response.ClearContent()
    '            'Response.ClearHeaders()
    '            'Response.ContentType = "application/pdf"
    '            'rs.ReportDocument.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, False, "Report")
    '            Try
    '                exportReport(rs, CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat)
    '            Catch ee As Exception
    '            End Try
    '            'rs.ReportDocument.Close()
    '            'rs.Dispose()
    '            'Response.Flush()
    '            'Response.Close()
    '        End With
    '    Catch ex As Exception
    '        UtilityObj.Errorlog(ex.Message, "HIFZTrackerLoad")
    '    Finally
    '        Try
    '            For Each table As CrystalDecisions.CrystalReports.Engine.Table In rs.Database.Tables
    '                table.Dispose()
    '            Next
    '            rs.Database.Dispose()
    '        Catch ex As Exception
    '            UtilityObj.Errorlog("Online rs.database.dispose--", ex.Message)
    '        End Try

    '        rs.Close()
    '        rs.Dispose()
    '        'Response.Flush()
    '        ' Response.Close()
    '    End Try
    '    'GC.Collect()
    'End Sub

    ''Protected Sub exportReport(ByVal selectedReport As CrystalDecisions.CrystalReports.Engine.ReportDocument, ByVal eft As CrystalDecisions.Shared.ExportFormatType)
    ''    selectedReport.ExportOptions.ExportFormatType = eft

    ''    Dim contentType As String = ""
    ''    ' Make sure asp.net has create and delete permissions in the directory
    ''    Dim tempDir As String = Web.Configuration.WebConfigurationManager.AppSettings("UploadExcelFile").ToString()
    ''    Dim tempFileName As String = HttpContext.Current.Session("susr_name") + Now.ToString.Replace("/", "_").Replace(":", "_").Replace(" ", "_") + "." ' HttpContext.Current.Session. HttpContext.Current.SessionID.ToString() & "."
    ''    Select Case eft
    ''        Case CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat
    ''            tempFileName += "pdf"
    ''            contentType = "application/pdf"
    ''            Exit Select
    ''        Case CrystalDecisions.[Shared].ExportFormatType.WordForWindows
    ''            tempFileName += "doc"
    ''            contentType = "application/msword"
    ''            Exit Select
    ''        Case CrystalDecisions.[Shared].ExportFormatType.Excel
    ''            tempFileName += "xls"
    ''            contentType = "application/vnd.ms-excel"
    ''            Exit Select
    ''        Case CrystalDecisions.[Shared].ExportFormatType.HTML32, CrystalDecisions.[Shared].ExportFormatType.HTML40
    ''            tempFileName += "htm"
    ''            contentType = "text/html"
    ''            Dim hop As New CrystalDecisions.Shared.HTMLFormatOptions()
    ''            hop.HTMLBaseFolderName = tempDir
    ''            hop.HTMLFileName = tempFileName
    ''            selectedReport.ExportOptions.FormatOptions = hop
    ''            Exit Select
    ''    End Select

    ''    Dim dfo As New CrystalDecisions.Shared.DiskFileDestinationOptions()
    ''    dfo.DiskFileName = tempDir + tempFileName
    ''    selectedReport.ExportOptions.DestinationOptions = dfo
    ''    selectedReport.ExportOptions.ExportDestinationType = CrystalDecisions.[Shared].ExportDestinationType.DiskFile
    ''    selectedReport.Export()
    ''    selectedReport.Close()

    ''    Dim tempFileNameUsed As String
    ''    If eft = CrystalDecisions.[Shared].ExportFormatType.HTML32 OrElse eft = CrystalDecisions.[Shared].ExportFormatType.HTML40 Then
    ''        Dim fp As String() = selectedReport.FilePath.Split("\".ToCharArray())
    ''        Dim leafDir As String = fp(fp.Length - 1)
    ''        ' strip .rpt extension
    ''        leafDir = leafDir.Substring(0, leafDir.Length - 4)
    ''        tempFileNameUsed = String.Format("{0}{1}\{2}", tempDir, leafDir, tempFileName)
    ''    Else
    ''        tempFileNameUsed = tempDir + tempFileName
    ''    End If

    ''    HttpContext.Current.Response.ContentType = "application/pdf"
    ''    HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" & System.IO.Path.GetFileName(tempFileNameUsed))
    ''    HttpContext.Current.Response.Clear()
    ''    HttpContext.Current.Response.WriteFile(tempFileNameUsed)
    ''    HttpContext.Current.Response.End()
    ''    System.IO.File.Delete(tempFileNameUsed)
    ''End Sub

    'Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument, ByVal reportParameters As Hashtable)
    '    Dim myTables As Tables = myReportDocument.Database.Tables
    '    Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
    '    Dim crParameterDiscreteValue As ParameterDiscreteValue

    '    For Each myTable In myTables
    '        Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
    '        myTableLogonInfo = myTable.LogOnInfo
    '        myTableLogonInfo.ConnectionInfo = myConnectionInfo
    '        myTable.ApplyLogOnInfo(myTableLogonInfo)
    '        myTable.Location = myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)

    '    Next
    '    myReportDocument.VerifyDatabase()
    'End Sub

    'Private Sub SetDBLogonForSubreports(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument, ByVal reportParameters As Hashtable)
    '    Dim mySections As Sections = myReportDocument.ReportDefinition.Sections
    '    Dim mySection As Section
    '    For Each mySection In mySections
    '        Dim myReportObjects As ReportObjects = mySection.ReportObjects
    '        Dim myReportObject As ReportObject
    '        For Each myReportObject In myReportObjects
    '            If myReportObject.Kind = ReportObjectKind.SubreportObject Then
    '                Dim mySubreportObject As SubreportObject = CType(myReportObject, SubreportObject)
    '                Dim subReportDocument As ReportDocument = mySubreportObject.OpenSubreport(mySubreportObject.SubreportName)

    '                Select Case subReportDocument.Name
    '                    Case "studphotos"
    '                        Dim dS As New dsImageRpt
    '                        Dim fs As New FileStream(HttpContext.Current.Session("StudentPhotoPath"), System.IO.FileMode.Open, System.IO.FileAccess.Read)
    '                        Dim Image As Byte() = New Byte(fs.Length - 1) {}
    '                        fs.Read(Image, 0, Convert.ToInt32(fs.Length))
    '                        fs.Close()
    '                        Dim dr As dsImageRpt.StudPhotoRow = dS.StudPhoto.NewStudPhotoRow
    '                        dr.Photo = Image
    '                        'Add the new row to the dataset
    '                        dS.StudPhoto.Rows.Add(dr)
    '                        subReportDocument.SetDataSource(dS)
    '                    Case "rptSubPhoto.rpt"
    '                        Dim newWindow As String = String.Empty
    '                        Dim vrptClass As rptClass
    '                        Dim rptStr As String = ""
    '                        If Not newWindow Is String.Empty Then
    '                            rptStr = "rptClass" + newWindow
    '                        Else
    '                            rptStr = "rptClass"
    '                        End If
    '                        vrptClass = HttpContext.Current.Session.Item(rptStr)
    '                        If vrptClass.Photos IsNot Nothing Then
    '                            SetPhotoToReport(subReportDocument, vrptClass.Photos)
    '                        End If
    '                    Case Else
    '                        SetDBLogonForReport(myConnectionInfo, subReportDocument, reportParameters)
    '                End Select

    '                ' subReportDocument.VerifyDatabase()
    '            End If
    '        Next
    '    Next

    'End Sub

    'Private Sub SetPhotoToReport(ByVal subReportDocument As ReportDocument, ByVal vPhotos As OASISPhotos)
    '    Dim arrphotos As ArrayList = vPhotos.IDs
    '    vPhotos = UpdatePhotoPath(vPhotos)
    '    Dim dS As New dsImageRpt
    '    Dim fs As FileStream = Nothing
    '    Dim Image As Byte() = Nothing
    '    Dim ienum As IDictionaryEnumerator = vPhotos.vHTPhoto_ID.GetEnumerator
    '    While (ienum.MoveNext)
    '        Try
    '            fs = New FileStream(ienum.Value, System.IO.FileMode.Open, System.IO.FileAccess.Read)
    '            Image = New Byte(fs.Length - 1) {}
    '            fs.Read(Image, 0, Convert.ToInt32(fs.Length))
    '        Catch
    '        Finally
    '            If Not fs Is Nothing Then
    '                fs.Close()
    '            End If
    '        End Try
    '        Dim dr As dsImageRpt.DSPhotosRow = dS.DSPhotos.NewDSPhotosRow
    '        dr.IMAGE = Image
    '        dr.ID = ienum.Key
    '        'Add the new row to the dataset
    '        dS.DSPhotos.Rows.Add(dr)
    '    End While
    '    subReportDocument.SetDataSource(dS)
    '    subReportDocument.VerifyDatabase()
    'End Sub
    'Protected Sub exportReport(ByVal selectedReport As CrystalDecisions.CrystalReports.Engine.ReportDocument, ByVal eft As CrystalDecisions.Shared.ExportFormatType)
    '    selectedReport.ExportOptions.ExportFormatType = eft

    '    Dim contentType As String = ""
    '    ' Make sure asp.net has create and delete permissions in the directory
    '    Dim tempDir As String = Server.MapPath("ReportDownloads/")
    '    Dim tempFileName As String = Session("STU_BSU_ID") + "_" + Session("username") + "_" + Now.ToString.Replace("/", "_").Replace(":", "_").Replace(" ", "_") + "." 'Session.SessionID.ToString() & "."
    '    Select Case eft
    '        Case CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat
    '            tempFileName += "pdf"
    '            contentType = "application/pdf"
    '            Exit Select
    '        Case CrystalDecisions.[Shared].ExportFormatType.WordForWindows
    '            tempFileName += "doc"
    '            contentType = "application/msword"
    '            Exit Select
    '        Case CrystalDecisions.[Shared].ExportFormatType.Excel
    '            tempFileName += "xls"
    '            contentType = "application/vnd.ms-excel"
    '            Exit Select
    '        Case CrystalDecisions.[Shared].ExportFormatType.HTML32, CrystalDecisions.[Shared].ExportFormatType.HTML40
    '            tempFileName += "htm"
    '            contentType = "text/html"
    '            Dim hop As New CrystalDecisions.Shared.HTMLFormatOptions()
    '            hop.HTMLBaseFolderName = tempDir
    '            hop.HTMLFileName = tempFileName
    '            selectedReport.ExportOptions.FormatOptions = hop
    '            Exit Select
    '    End Select

    '    Dim dfo As New CrystalDecisions.Shared.DiskFileDestinationOptions()
    '    dfo.DiskFileName = tempDir + tempFileName
    '    selectedReport.ExportOptions.DestinationOptions = dfo
    '    selectedReport.ExportOptions.ExportDestinationType = CrystalDecisions.[Shared].ExportDestinationType.DiskFile

    '    selectedReport.Export()
    '    selectedReport.Close()

    '    Dim tempFileNameUsed As String
    '    If eft = CrystalDecisions.[Shared].ExportFormatType.HTML32 OrElse eft = CrystalDecisions.[Shared].ExportFormatType.HTML40 Then
    '        Dim fp As String() = selectedReport.FilePath.Split("\".ToCharArray())
    '        Dim leafDir As String = fp(fp.Length - 1)
    '        ' strip .rpt extension
    '        leafDir = leafDir.Substring(0, leafDir.Length - 4)
    '        tempFileNameUsed = String.Format("{0}{1}\{2}", tempDir, leafDir, tempFileName)
    '    Else
    '        tempFileNameUsed = tempDir + tempFileName
    '    End If

    '    'Response.ClearContent()
    '    'Response.ClearHeaders()
    '    'Response.ContentType = contentType

    '    'Response.WriteFile(tempFileNameUsed)
    '    'Response.Flush()
    '    'Response.Close()


    '    ' HttpContext.Current.Response.ContentType = "application/octect-stream"
    '    HttpContext.Current.Response.ContentType = "application/pdf"
    '    HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" & System.IO.Path.GetFileName(tempFileNameUsed))
    '    HttpContext.Current.Response.Clear()
    '    HttpContext.Current.Response.WriteFile(tempFileNameUsed)
    '    ' HttpContext.Current.Response.Flush()
    '    '  HttpContext.Current.Response.Close()
    '    HttpContext.Current.Response.End()

    '    System.IO.File.Delete(tempFileNameUsed)
    'End Sub

    'Function UpdatePhotoPath(ByVal vPhotos As OASISPhotos) As OASISPhotos
    '    Select Case vPhotos.PhotoType
    '        Case OASISPhotoType.STUDENT_PHOTO
    '            Dim Virtual_Path As String = Web.Configuration.WebConfigurationManager.ConnectionStrings("EmpFilepath").ConnectionString()  'Replace(WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ConnectionString, "http:", "")
    '            Dim dtFilePath As DataTable = GETStud_photoPath(vPhotos.IDs) 'get the image
    '            vPhotos.vHTPhoto_ID = New Hashtable
    '            For Each dr As DataRow In dtFilePath.Rows
    '                If (dr("FILE_PATH") Is DBNull.Value) OrElse (dr("FILE_PATH") Is Nothing) OrElse (dr("FILE_PATH") = "") Then
    '                    vPhotos.vHTPhoto_ID(dr("STU_ID")) = Virtual_Path + "/NOIMG/no_image.jpg"
    '                Else
    '                    vPhotos.vHTPhoto_ID(dr("STU_ID")) = Virtual_Path + dr("FILE_PATH").ToString
    '                End If
    '            Next
    '    End Select
    '    Return vPhotos
    'End Function

    'Private Function GETStud_photoPath(ByVal arrSTU_IDs As ArrayList) As DataTable
    '    Dim comma As String = String.Empty
    '    Dim strStudIDs As String = String.Empty
    '    For Each ID As Object In arrSTU_IDs
    '        strStudIDs += comma + ID.ToString
    '        comma = ", "
    '    Next
    '    Dim sqlString As String = "SELECT DISTINCT isnull(STU_PHOTOPATH,'') FILE_PATH,STU_ID FROM STUDENT_M where  STU_ID in(" & strStudIDs & ")"
    '    Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, sqlString)
    '    If ds IsNot Nothing Then
    '        Return ds.Tables(0)
    '    End If
    '    Return Nothing

    'End Function



End Class
