﻿
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports System.Net
Imports iTextSharp.text
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Imports Microsoft.ApplicationBlocks.Data
Imports NReco.PdfGenerator


Partial Class Service_HifzCertificate
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
        If Session("username") Is Nothing Then
            Response.Redirect("~\General\Home.aspx")
        End If
        Dim trackerId = Convert.ToString(Session("HifzTrackerId"))
        If Not String.IsNullOrEmpty(trackerId) Then
            Session("HifzTrackerId") = ""
        End If
        If trackerId = "" Then Return
        DownloadHifzCertificate(trackerId)
    End Sub

    Private Sub DownloadHifzCertificate(ByVal trackerId As Integer)
        Try
            Dim certificateData = GetHifzCertificateData(trackerId)
            Dim certificateRow = certificateData.Rows(0)
            Dim pdfFileName = "HifzCertificate_" & Guid.NewGuid().ToString()

            Dim stream = New StreamReader(Server.MapPath("~/Service/certificate.html"))
            Dim htmlContent = stream.ReadToEnd

            'htmlContent = htmlContent.Replace("##lblCurrentDate##", DateTime.Now.ToString("dd MMM yyyy"))
            'htmlContent = htmlContent.Replace("##lblGrade##", certificateRow("GradeName"))
            htmlContent = htmlContent.Replace("##lblStudentName##", Convert.ToString(Session("STU_NAME")))
            htmlContent = htmlContent.Replace("##lblSurah##", certificateRow("Title"))
            'htmlContent = htmlContent.Replace("##lblUniqueNo##", Convert.ToString(Session("STU_NO")))
            'htmlContent = htmlContent.Replace("##lblApprovedBy##", certificateRow("ApprovedBy"))
            Html2PdfPreview(htmlContent, pdfFileName)
        Catch ex As Exception
            UtilityObj.Errorlog("hifzcertificate", ex.Message)
        End Try
    End Sub

    Private Function GetHifzCertificateData(ByVal trackerId As String) As DataTable
        Try
            Dim schoolId = Convert.ToString(HttpContext.Current.Session("sBsuid"))
            Dim studentId = Convert.ToString(HttpContext.Current.Session("STU_ID"))
            Dim parameters As SqlParameter() = New SqlParameter(4) {}
            parameters(0) = New SqlParameter("@BSU_ID", schoolId)
            parameters(1) = New SqlParameter("@HifzTrackerId", trackerId)
            parameters(2) = New SqlParameter("@StudentId", studentId)
            Return SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString(), CommandType.StoredProcedure, "dbo.GetHifzTrackerDataById", parameters).Tables(0)
        Catch ex As Exception
            UtilityObj.Errorlog("GetHifzCertificateData", ex.Message)
        End Try
    End Function


    Private Sub Html2PdfPreview(ByVal htmlContent As String, ByVal fileName As String)
        Try
            Dim document As New Document(iTextSharp.text.PageSize.A4, 10, 10, 10, 10)
            Dim output = New MemoryStream()
            HttpContext.Current.Response.ContentType = "application/pdf"
            Dim page As PdfPage
            Dim pdfWriter1 As PdfWriter = PdfWriter.GetInstance(document, HttpContext.Current.Response.OutputStream)
            pdfWriter1.PageEvent = page
            document.Open()
            Dim css As New StyleSheet()
            css.LoadTagStyle("HtmlTags.DIV", "HtmlTags.BORDER", "1")
            Dim htmlarraylist = HTMLWorker.ParseToList(New StringReader(htmlContent), Nothing)

            For k As Integer = 0 To htmlarraylist.Count - 1
                document.Add(DirectCast(htmlarraylist(k), IElement))
            Next

            document.Close()


            HttpContext.Current.Response.AddHeader("Content-Disposition", String.Format("attachment;filename={0}.pdf", fileName))
            HttpContext.Current.Response.BinaryWrite(output.ToArray)
            HttpContext.Current.Response.Flush()
            HttpContext.Current.Response.Close()

            '  HttpContext.Current.Response.End()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub



    Private Sub Html2PdfPreview2(ByVal htmlContent As String, ByVal fileName As String)
        Try
            Dim date_pdf As String = DateTime.Now.ToString("dd-MM-yy-hh-mm-ss")
            Dim Contract_name As String = fileName
            Dim htmlContenttest As String = htmlContent.Replace("width=""90%""", " ").Replace("<br>                          </br>                          <br>                          </br>                          <br>", " ")

            WriteDocument(Contract_name + "-" + date_pdf + ".pdf", "application/pdf", ConvertHtmlToPDF(htmlContenttest))
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Private Function ConvertHtmlToPDF(htmlContent As String) As Byte()
        Try


            Dim nRecohtmltoPdfObj As HtmlToPdfConverter = New HtmlToPdfConverter()
            nRecohtmltoPdfObj.Orientation = PageOrientation.Landscape
            'nRecohtmltoPdfObj.PageWidth = 2000
            'nRecohtmltoPdfObj.PageHeight = IsNothing()
            nRecohtmltoPdfObj.Size = NReco.PdfGenerator.PageSize.A4

            'nRecohtmltoPdfObj.Margins = New PageMargins { Top = 2, Bottom = 14, Left = 8, Right = 8 }
            nRecohtmltoPdfObj.Margins = New PageMargins With {
                .Top = 10,
                .Bottom = 14,
                .Left = 8,
                .Right = 8
            }

            nRecohtmltoPdfObj.PageFooterHtml = ""
            nRecohtmltoPdfObj.PageHeaderHtml = ""

            'nRecohtmltoPdfObj.CustomWkHtmlArgs = "--margin-top 25 --header-spacing 25 --margin-left 5 --margin-right 5";
            'return nRecohtmltoPdfObj.GeneratePdfFromFile(url, null);
            'Return nRecohtmltoPdfObj.GeneratePdf(CreatePDFScript() + htmlContent + "</html>")
            Return nRecohtmltoPdfObj.GeneratePdf(htmlContent)
            '' Return nRecohtmltoPdfObj.GeneratePdf(CreatePDFScript() + htmlContent + "<table><tr><td colspan=""3"" align=""left""  ><img src=""https://oasis.gemseducation.com/images/mhslogos_certificates/MHSFOOTER.jpg"" alt=""Logo"" width=""560"" height=""120"" /> </td></tr></table></body></body></html>")
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Function
    'Private Function CreatePDFScript() As String
    '    Return "<html lang=""en"" dir=""ltr""><head><meta http-equiv=""Content-Type"" content=""text/html; charset=utf-8""/>" &
    '        "<script>function substfooter() {document.getElementById(""footer"").style.pageBreakInside = ""avoid"";}</script>" &
    '         "<link href='https://fonts.googleapis.com/css?family=Kaushan+Script&display=swap' rel='stylesheet'>" &
    '        "<style></style>" &
    '        "</head><body style=""background-color: #fff; "" >"
    'End Function
    Private Sub WriteDocument(ByVal fileName As String, ByVal contentType As String, ByVal content As Byte())
        Try


            HttpContext.Current.Response.Clear()
            Response.ContentType = contentType
            Response.AddHeader("content-disposition", "attachment; filename=" & fileName)
            Response.CacheControl = "No-cache"
            Response.BinaryWrite(content)
            Response.Flush()
            Response.SuppressContent = True
            HttpContext.Current.ApplicationInstance.CompleteRequest()

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

End Class
