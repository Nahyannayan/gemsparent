﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="urcParentShortProfile.ascx.vb" Inherits="UserControl_urcParentShortProfile" %>

<%--<link href="../CSS/SiteStyle.css" rel="stylesheet" type="text/css" />--%>

<div id="divTitle" runat="server" class="mainheading">
    <div class="left">Employee's Basic Information </div>
</div>
<div>
    <asp:Repeater ID="repInfo" runat="server" OnItemCommand="R1_ItemCommand">
        <HeaderTemplate>
            <table border="0" class="childLink" style="border: none 0px !important;">
        </HeaderTemplate>
        <ItemTemplate>
            <tr class="trSub_Header" align="left" style="border: none 0px !important;">
                <td colspan="2" style="border: none 0px !important;">
                    <asp:Label ID="lbSName" runat="server" Text='<%# Bind("EMPNAME")%>'></asp:Label>
                </td>
                <td align="left" style="border: none 0px !important; width: 170px; padding-left: 10px;">

                </td>
            </tr>

            <tr align="left" style="border: none 0px !important;">
                <td valign="top" style="width: 20%; border: none 0px !important; padding-top: 10px;">
                    <div id="box">
                        <!--- box border -->
                        <div id="lb">
                            <div id="rb">
                                <div id="bb">
                                    <div id="blc">
                                        <div id="brc">
                                            <div id="tb">
                                                <div id="tlc">
                                                    <div id="trc">
                                                        <!--  -->

                                                        <div id="content2">
                                                            <asp:Image ID="imgEmpImage" runat="server" Height="132px" ToolTip='<%# Bind("EMPNAME") %>'
                                                                Width="120px" />
                                                        </div>

                                                        <!--- end of box border -->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- -->

                    </div>
                </td>
                <td valign="top" align="left" style="border: none 0px !important; width: 80%;">
                    <table class="tableNoborder" width="100%" border="0">
                        <tr>
                            <td class="tdfieldsHome" style="width: 25%;">Employee Id</td>
                            <td class="tdfieldsHomeValue" style="width: 75%;">
                                <asp:Literal ID="ltid" runat="server" Text='<%# Bind("EMPNO")%>'></asp:Literal></td>
                        </tr>
                        <tr>
                            <td class="tdfieldsHome">Full Name</td>
                            <td class="tdfieldsHomeValue">
                                <asp:Literal ID="Literal1" runat="server" Text='<%# Bind("EMPNAME")%>'></asp:Literal></td>
                        </tr>
                        <tr>
                            <td class="tdfieldsHome">Business Unit</td>
                            <td class="tdfieldsHomeValue">
                                <asp:Literal ID="Literal2" runat="server" Text='<%# Bind("WORK_BSU_NAME")%>'></asp:Literal></td>
                        </tr>
                        <tr>
                            <td class="tdfieldsHome">Department</td>
                            <td class="tdfieldsHomeValue">
                                <asp:Literal ID="Literal3" runat="server" Text='<%# Bind("EMP_DEPT_DESCR")%>'></asp:Literal></td>
                        </tr>
                        <tr>
                            <td class="tdfieldsHome">Designation</td>
                            <td class="tdfieldsHomeValue">
                                <asp:Literal ID="Literal4" runat="server" Text='<%# Bind("EMP_DES_DESCR")%>'></asp:Literal></td>
                        </tr>
                        <tr>
                            <td class="tdfieldsHome">Date Of Join(Group)</td>
                            <td class="tdfieldsHomeValue">
                                <asp:Literal ID="Literal5" runat="server" Text='<%# Bind("EMP_JOINDT", "{0:dd/MMM/yyyy}")%>'></asp:Literal></td>
                        </tr>
                        <tr>
                            <td class="tdfieldsHome"></td>
                            <td class="tdfieldsHomeValue">
                                <asp:Literal ID="Literal6" runat="server" Text=''></asp:Literal></td>
                        </tr>
                        <tr id="trStatus" runat="server" visible='true'>
                            <td class="tdfieldsHome"></td>
                            <td class="tdfieldsHomeValue">
                                <asp:Literal ID="Literal7" runat="server" Text=''></asp:Literal>
                            </td>
                        </tr>
                    </table>

                </td>
                <td valign="top" align="right" style="border: none 0px !important; padding-top: 10px;">
                    <table width="100%" class="tableNoborder" cellpadding="0" cellspacing="0" style="padding: 0px; margin: 0px; height: 180px;">
                        <tr>
                            <td>
                                <asp:Panel runat="server" ID="plChildLink" Width="173px" Height="130px" ScrollBars="auto"
                                    BorderStyle="solid"
                                    BorderColor="#aabec9" BorderWidth="1px" BackColor="#ffffff">
                                    <asp:Repeater ID="repChildInfo" runat="server" OnItemCommand="R1_ItemCommand">
                                        <HeaderTemplate>
                                            <table width="120px" class="tableNoborder">
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr align="left" style="border: none 0px !important;">
                                                <td style="border: none 0px !important; width: 120px;">
                                                    <asp:LinkButton ID="lbtnLinks" runat="server" CssClass="EasyAccessLink"
                                                        Text='<%# Bind("OPLM_DESCR") %>'
                                                        CommandName='EMP' CommandArgument='<%# Bind("LINK_ID") %>'></asp:LinkButton>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </table>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
    </asp:Repeater>
    <asp:HiddenField ID="h_EmpNo" runat="server" />
    <asp:HiddenField ID="h_empID" runat="server" />
</div>
