﻿Imports System.Web.Configuration
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports Lesnikowski.Barcode
Imports System.Data.SqlClient
Imports System.Drawing
Imports System.IO

Partial Class UserControl_urcBookSalePaidResult
    Inherits System.Web.UI.UserControl
    Private Property bBSUTaxable() As Boolean
        Get
            Return ViewState("bBSUTaxable")
        End Get
        Set(ByVal value As Boolean)
            ViewState("bBSUTaxable") = value
        End Set
    End Property
    Public ReceiptNos() As String
    Public Sub Gridbind_PayDetails(ByVal BSAH_BSAH_ID As Int64, ByVal bPaymentSuccess As Boolean)
        Try
            'Dim strPath As String = Convert.ToString(readerStudent_Detail("STU_PHOTOPATH"))
            Dim connPath As String = WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ConnectionString
            Dim noImagePath As String = "~/Images/Home/no_image.gif"
            Dim con As String = ConnectionManger.GetOASIS_PUR_INVConnectionString
            Dim param(4) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@OPTIONS", 1)
            param(1) = New SqlClient.SqlParameter("@BSAHO_BSU_ID", Session("sBsuid"))
            param(2) = New SqlClient.SqlParameter("@BSAHO_BSAHO_ID", BSAH_BSAH_ID)


            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "DBO.GET_BOOKSALES_PAYMENT_RESULT", param)
            bBSUTaxable = DirectCast(SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT ISNULL(BUS_bFEE_TAXABLE,0) FROM dbo.BUSINESSUNIT_SUB WITH(NOLOCK) WHERE BUS_BSU_ID='" & Session("sBsuid") & "'"), Boolean)
            repInfo.DataSource = ds.Tables(0)
            repInfo.DataBind()
        Catch ex As Exception
            repInfo.DataBind()
        End Try
    End Sub
    Dim ReptrSum As Double = 0

    Protected Sub repInfo_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles repInfo.ItemDataBound
        If e.Item.DataItem Is Nothing Then
            Dim lbl_Amount_F As New Label

            lbl_Amount_F = repInfo.Controls(repInfo.Controls.Count - 1).Controls(0).FindControl("lbl_Amount_F")
            If Not lbl_Amount_F Is Nothing Then
                lbl_Amount_F.Text = ReptrSum.ToString("#,##0.00")
            End If

            Return
        Else
            Dim lblReceipt As New LinkButton
            Dim ltAmount As New Literal
            Dim Encr_decrData As New Encryption64
            lblReceipt = DirectCast(e.Item.FindControl("lblReceipt"), LinkButton)
            ltAmount = DirectCast(e.Item.FindControl("ltAmount"), Literal)

            ReptrSum += IIf(Not ltAmount Is Nothing, CDbl(ltAmount.Text), 0)

            Dim PopForm As String = "BookSaleReceipt.aspx" 'IIf(bBSUTaxable, "FeeReceipt_TAX.aspx", "feereceipt.aspx")
            If Not lblReceipt Is Nothing Then
                Dim p_Receiptno As String = ""

                p_Receiptno = lblReceipt.Text.Trim
                Dim barcode As BaseBarcode
                barcode = BarcodeFactory.GetBarcode(Symbology.Code128)

                barcode.Number = p_Receiptno
                barcode.BackColor = Drawing.ColorTranslator.FromHtml("#FFFFFF")
                barcode.ChecksumAdd = True
                barcode.CustomText = p_Receiptno
                barcode.NarrowBarWidth = 3
                barcode.Height = 300
                barcode.FontHeight = 0.3F
                barcode.ForeColor = Drawing.Color.Black
                Dim b As Byte()
                ReDim b(barcode.Render(ImageType.Png).Length)
                b = barcode.Render(ImageType.Png)

                'b = PrintBarcode(p_Receiptno)
                Dim objConn As New SqlConnection(ConnectionManger.GetOASIS_PUR_INVConnectionString)
                objConn.Open()
                Dim stTrans As SqlTransaction = objConn.BeginTransaction
                Try
                    Dim ds5 As New DataSet
                    Dim STR_BSAH_ID As String = ""
                    ds5 = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT BSAH_ID FROM OASIS_PUR_INV.[dbo].[BOOK_SALE_H] WHERE BSAH_BSU_ID='" & Session("sBsuid") & "' AND BSAH_NO='" & p_Receiptno & "'")
                    STR_BSAH_ID = (ds5.Tables(0).Rows(0)("BSAH_ID").ToString())

                    Dim retvale As Integer = clsBookSalesOnline.UPDATE_BARCODE(1, STR_BSAH_ID, Session("sBsuid"), b, stTrans)
                    If retvale = 0 Then
                        stTrans.Commit()
                    Else
                        stTrans.Rollback()
                    End If
                Catch ex As Exception
                    stTrans.Rollback()
                Finally
                    If objConn.State = ConnectionState.Open Then
                        objConn.Close()
                    End If
                End Try

                lblReceipt.Attributes.Add("onClick", "return ShowSubWindowWithClose('" & PopForm & "?type=BRN&id=" & Encr_decrData.Encrypt(lblReceipt.Text.Trim) & "', '', '80%', '80%');")
            End If
        End If
    End Sub
    Protected Function PrintBarcode(ByVal p_Receiptno As String) As Byte()
        Dim byteImage As Byte()
        Using bitMap As New Bitmap(p_Receiptno.Length * 14, 80)
            Dim graphics As System.Drawing.Graphics = System.Drawing.Graphics.FromImage(bitMap)
            Dim oFont As Font = New Font("IDAutomationHC39M", 9)
            Dim point As PointF = New PointF(2.0F, 2.0F)
            Dim blackBrush As SolidBrush = New SolidBrush(Color.Black)
            Dim whiteBrush As SolidBrush = New SolidBrush(Color.White)
            graphics.FillRectangle(whiteBrush, 0, 0, bitMap.Width, bitMap.Height)
            graphics.DrawString("*" + p_Receiptno + "*", oFont, blackBrush, point)

            Using ms As New MemoryStream()
                bitMap.Save(ms, System.Drawing.Imaging.ImageFormat.Png)
                byteImage = ms.ToArray()
            End Using
        End Using

        Return byteImage
    End Function
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

    End Sub
End Class
