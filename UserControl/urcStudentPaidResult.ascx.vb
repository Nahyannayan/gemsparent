﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Partial Class UserControl_urcStudentPaidResult
    Inherits System.Web.UI.UserControl
    Private Property bBSUTaxable() As Boolean
        Get
            Return ViewState("bBSUTaxable")
        End Get
        Set(ByVal value As Boolean)
            ViewState("bBSUTaxable") = value
        End Set
    End Property
    Public ReceiptNos() As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("bPasswdChanged") = "False" Then
            Response.Redirect("~\UpdateInfo\ForcePasswordChange.aspx", False)
        ElseIf Session("bUpdateContactDetails") = "False" Then
            Response.Redirect(Session("ForceUpdate_stud"), False)
        End If
    End Sub

    Public Sub Gridbind_PayDetails(ByVal FCO_ID As Int64, ByVal bPaymentSuccess As Boolean)
        Try
            'Dim strPath As String = Convert.ToString(readerStudent_Detail("STU_PHOTOPATH"))
            Dim connPath As String = WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ConnectionString
            Dim noImagePath As String = "~/Images/Home/no_image.gif"
            Dim con As String = ConnectionManger.GetOASIS_FEESConnectionString
            Dim param(4) As SqlClient.SqlParameter

            param(0) = New SqlClient.SqlParameter("@OLU_ID", Session("OLU_ID").ToString)
            param(1) = New SqlClient.SqlParameter("@connPath", connPath)
            param(2) = New SqlClient.SqlParameter("@noImagePath", noImagePath)
            param(3) = New SqlClient.SqlParameter("@FCO_ID", FCO_ID)
            param(4) = New SqlClient.SqlParameter("@bPaymentSuccess", bPaymentSuccess)

            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "FEES.GET_SIBLINGS_PAYMENT_RESULT", param)
            bBSUTaxable = DirectCast(SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT ISNULL(BUS_bFEE_TAXABLE,0) FROM dbo.BUSINESSUNIT_SUB WHERE BUS_BSU_ID='" & Session("sBsuid") & "'"), Boolean)
            repInfo.DataSource = ds.Tables(0)
            repInfo.DataBind()
        Catch ex As Exception
            repInfo.DataBind()
        End Try
    End Sub
    Dim ReptrSum As Double = 0

    Protected Sub repInfo_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles repInfo.ItemDataBound
        If e.Item.DataItem Is Nothing Then
            Dim lbl_Amount_F As New Label
            If ReceiptNos Is Nothing Then
                ReDim ReceiptNos(-1)
            End If

            lbl_Amount_F = repInfo.Controls(repInfo.Controls.Count - 1).Controls(0).FindControl("lbl_Amount_F")
            If Not lbl_Amount_F Is Nothing Then
                lbl_Amount_F.Text = ReptrSum.ToString("#,##0.00")
            End If
            If ReceiptNos.Length <= 0 Then
                Me.divinfo.Visible = False
            ElseIf ReceiptNos(0) <> "" Then
                Me.divinfo.Visible = True
            End If
            Return
        Else
            Dim lblReceipt As New LinkButton
            Dim ltAmount As New Literal
            Dim Encr_decrData As New Encryption64
            lblReceipt = DirectCast(e.Item.FindControl("lblReceipt"), LinkButton)
            ltAmount = DirectCast(e.Item.FindControl("ltAmount"), Literal)

            ReptrSum += IIf(Not ltAmount Is Nothing, CDbl(ltAmount.Text), 0)

            Dim PopForm As String = IIf(bBSUTaxable, "FeeReceipt_TAX.aspx", "feereceipt.aspx")
            If Not lblReceipt Is Nothing Then
                ReDim Preserve ReceiptNos(UBound(ReceiptNos) + 1)
                ReceiptNos(UBound(ReceiptNos)) = lblReceipt.Text
                lblReceipt.Attributes.Add("onClick", "return ShowSubWindowWithClose('" & PopForm & "?type=REC&id=" & Encr_decrData.Encrypt(lblReceipt.Text.Trim) & "', '', '60%', '60%');")
            End If
        End If
    End Sub
End Class
