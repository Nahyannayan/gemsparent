<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ActionViewDetails.ascx.vb" Inherits="Students_BehaviorManagement_UserControl_bm_ActionViewDetails" %>

    <link rel="stylesheet" href="../css/bootstrap.css" type="text/css"  />
    <link rel="stylesheet" href="../css/bootstrap-theme.css" type="text/css"  />
 <div align="center">
 <%--<table border="1" bordercolor="#1b80b6" cellpadding="5" cellspacing="0" Width="700px" >
                        <tr>
                            <td class="subheader_img">
                                Primary Information&nbsp;<asp:ImageButton ID="ImagePrint" runat="server" ImageUrl="~/Images/tick.gif"
                                    OnClientClick="javascript:window.print(); return false;" ToolTip="Print Page" /></td>
                        </tr>
                        <tr>
                            <td align="left" >
<table width="100%">
    <tr>
        <td colspan="6" bgcolor="#003366">
            <strong><span style="font-size: 11pt; color: #ffffff; font-family: Arial">Student Information</span></strong></td>
    </tr>
    <tr>
        <td >
            Student Name</td>
        <td >
            :</td>
        <td >
            <asp:Label ID="lblstudentname" runat="server"></asp:Label></td>
        <td >
        </td>
        <td >
        </td>
        <td >
        </td>
    </tr>
    <tr>
        <td >
            Grade</td>
        <td >
            :</td>
        <td >
            <asp:Label ID="lblstudentgrade" runat="server"></asp:Label></td>
        <td >
            Shift
        </td>
        <td >
            :</td>
        <td >
            <asp:Label ID="lblstudentshift" runat="server"></asp:Label></td>
    </tr>
    <tr>
        <td >
            Section</td>
        <td >
            :</td>
        <td >
            <asp:Label ID="lblstudentsection" runat="server"></asp:Label></td>
        <td >
            Stream</td>
        <td >
            :</td>
        <td >
            <asp:Label ID="lblstudentstream" runat="server"></asp:Label></td>
    </tr>
    <tr>
        <td colspan="6" bgcolor="#003366"><strong><span style="font-size: 11pt; color: #ffffff; font-family: Arial">Parent Information</span></strong>
            </td>
    </tr>
    <tr>
        <td >
            Parent Name</td>
        <td >
            :</td>
        <td >
            <asp:Label ID="lblparentname" runat="server"></asp:Label></td>
        <td >
        </td>
        <td >
        </td>
        <td >
        </td>
    </tr>
    <tr>
        <td >
            Email ID</td>
        <td >
            :</td>
        <td >
            <asp:Label ID="lblparentemailid" runat="server"></asp:Label></td>
        <td >
        </td>
        <td >
        </td>
        <td >
        </td>
    </tr>
    <tr>
        <td >
            Contact Number</td>
        <td >
            :</td>
        <td >
            <asp:Label ID="lblparentcontactnumber" runat="server"></asp:Label></td>
        <td >
        </td>
        <td >
        </td>
        <td >
        </td>
    </tr>
</table>
 </td>
                    </tr>
               </table>--%>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
        <td class="sub-heading" >
           <h1> Incident Details-(<asp:Label ID="lbltype" runat="server" Text=""></asp:Label>
            )</h1><asp:ImageButton ID="ImagePrint" runat="server" ImageUrl="~/Images/Misc/print.gif"
                                    OnClientClick="javascript:window.print(); return false;" ToolTip="Print Page"  /></td>
    </tr>
    <tr>
        <td align="left" >
            <table width="100%" id="tabWitness" class="table table-bordered table-responsive text-left my-orders-table">
                <tr>
                    <td colspan="6" class="he" >
                   <h3><span >Reporting Informations</span></h3>
                        </td>
                </tr>
                <tr>
                    <td class="tdfie">
                        Reported
                        Date</td>
                  
                    <td colspan="4">
                        <asp:Label ID="lbltodaydate" runat="server" ></asp:Label></td>
                </tr>
                <tr>
                    <td class="tdfields">
                        Staff Reporting the Incident</td>
                   
                    <td colspan="4">
                        <asp:Label ID="lblreportingstaff" runat="server"  ></asp:Label></td>
                </tr>
                <tr>
                    <td class="tdfields">
                        Incident Date</td>
                   
                    <td colspan="4">
                        <asp:Label ID="lblincidentdate" runat="server"  ></asp:Label></td>
                </tr>
                <tr>
                    <td colspan="6" class="tdfields">
                  <b><u>Report on the incident</u></b>
                       </td>
                </tr>
                <tr>
                    <td colspan="6">
             
                        <asp:Label ID="txtReportonincident" runat="server"   ></asp:Label></td>
                </tr>
                <tr>
                    <td colspan="6" >
                 <strong><span >&nbsp;Witnesses
                     spoken to</span></strong>
                    </td>
                </tr>
            </table>
            <asp:Table ID="tabWitnessDet" runat="server" Width="100%" class="table table-bordered table-responsive text-left my-orders-table">
            </asp:Table>
        </td>
    </tr>
</table>
<table cellpadding="0" cellspacing="0" width="100%" class="table table-bordered table-responsive text-left my-orders-table">
    <tr>
        <td class="tite-box">
           <h3> Action Taken</h3></td>
    </tr>
    <tr>
        <td align="left">
            <table width="100%">
             <tr>
                    <td colspan="6" class="tdfields">
                     <strong><span >Parents Called
                         / Interviewed</span></strong>
                       </td>
                </tr>
                <tr>
                    <td>
                        Parent Called</td>
                   
                    <td colspan="2">
                        <asp:RadioButtonList ID="R1" runat="server"  RepeatDirection="Horizontal" Enabled="False">
                            <asp:ListItem Value="Yes">Yes</asp:ListItem><asp:ListItem Value="No">No</asp:ListItem>
                        </asp:RadioButtonList></td>
                      
                            <td><asp:TextBox ID="txtCalldate" runat="server" BorderWidth="0px" ReadOnly="True"></asp:TextBox></td>
                </tr>
                <tr>
                    <td colspan="6">
                       
                        <asp:Label ID="txtparentscalledsaid" runat="server"></asp:Label></td>
                </tr>
                <tr>
                    <td class="tdfields">
                        Parent Interviewed</td>
                  
                    <td colspan="2">
                        <asp:RadioButtonList ID="R2" runat="server" RepeatDirection="Horizontal" Enabled="False">
                            <asp:ListItem Value="Yes">Yes</asp:ListItem><asp:ListItem Value="No">No</asp:ListItem>
                        </asp:RadioButtonList></td>
                      
                            <td><asp:TextBox ID="TxtIntvDate" runat="server" BorderWidth="0px" ReadOnly="True"></asp:TextBox></td>
                </tr>
                <tr>
                    <td colspan="6">
                      
                        <asp:Label ID="txtparentsinterviewssaid" runat="server"></asp:Label></td>
                </tr>
                 <tr>
                    <td colspan="6" >
                     <strong><span >Other Details</span></strong>
                       </td>
                </tr>
                <tr>
                    <td class="tdfields">
                        Notes in student's planner</td>
                   
                    <td>
                        <asp:RadioButtonList ID="R3" runat="server" RepeatDirection="Horizontal" Enabled="False">
                            <asp:ListItem Value="Yes">Yes</asp:ListItem>
                            <asp:ListItem  Value="No">No</asp:ListItem>
                        </asp:RadioButtonList></td>
                    <td class="tdfields">
                        Date</td>
                   
                    <td>
                        <asp:TextBox ID="T1" runat="server" ReadOnly="True" BorderWidth="0px"></asp:TextBox>&nbsp;
                    </td>
                </tr>
                <tr>
                    <td class="tdfields">
                        Break detention given</td>
                   
                    <td>
                        <asp:RadioButtonList ID="R4" runat="server" RepeatDirection="Horizontal" Enabled="False">
                            <asp:ListItem Value="Yes">Yes</asp:ListItem>
                            <asp:ListItem  Value="No">No</asp:ListItem>
                        </asp:RadioButtonList></td>
                    <td class="tdfields">
                        Date</td>
                    
                    <td>
                        <asp:TextBox ID="T2" runat="server" ReadOnly="True" BorderWidth="0px"></asp:TextBox>&nbsp;
                    </td>
                </tr>
                <tr>
                    <td class="tdfields">
                        After school detention given</td>
                   
                    <td>
                        <asp:RadioButtonList ID="R5" runat="server" RepeatDirection="Horizontal" Enabled="False">
                            <asp:ListItem Value="Yes">Yes</asp:ListItem>
                            <asp:ListItem Value="No">No</asp:ListItem>
                        </asp:RadioButtonList></td>
                    <td class="tdfields">
                        Date</td>
                    
                    <td>
                        <asp:TextBox ID="T3" runat="server" ReadOnly="True" BorderWidth="0px"></asp:TextBox>&nbsp;
                    </td>
                </tr>
                <tr>
                    <td class="tdfields">
                        Suspension</td>
                   
                    <td>
                        <asp:RadioButtonList ID="R6" runat="server" RepeatDirection="Horizontal" Enabled="False">
                            <asp:ListItem Value="Yes">Yes</asp:ListItem>
                            <asp:ListItem Value="No">No</asp:ListItem>
                        </asp:RadioButtonList></td>
                    <td class="tdfields">
                        Date</td>
                   
                    <td>
                        <asp:TextBox ID="T4" runat="server" ReadOnly="True" BorderWidth="0px"></asp:TextBox>&nbsp;
                    </td>
                </tr>
                <tr>
                    <td class="tdfields">
                        Referred to students counsellor
                    </td>
                   
                    <td>
                        <asp:RadioButtonList ID="R7" runat="server" RepeatDirection="Horizontal" Enabled="False">
                            <asp:ListItem Value="Yes">Yes</asp:ListItem>
                            <asp:ListItem Value="No">No</asp:ListItem>
                        </asp:RadioButtonList></td>
                    <td class="tdfields">
                        Date</td>
                   
                    <td>
                        <asp:TextBox ID="T5" runat="server" ReadOnly="True" BorderWidth="0px"></asp:TextBox>&nbsp;
                    </td>
                </tr>
                <tr>
                    <td colspan="6" >
                     <strong><span >Any Notes/Followup</span></strong>
                        </td>
                </tr>
                <tr>
                    <td colspan="6">
                        <asp:Table ID="tabFollowUp" runat="server" Width="100%" class="table table-bordered table-responsive text-left my-orders-table">
                        </asp:Table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<asp:HiddenField ID="Hiddenstuid" runat="server" />
<asp:HiddenField ID="Hiddenincidentid" runat="server" />
     <asp:HiddenField ID="HidActionID" runat="server" />
</div>