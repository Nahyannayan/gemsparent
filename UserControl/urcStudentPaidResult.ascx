﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="urcStudentPaidResult.ascx.vb"
    Inherits="UserControl_urcStudentPaidResult" %>
<div>
    <div class="left" style="font-weight: bold !important;">
        Payment Summary</div>
</div>
<div>
    <div class="divinfo" id="divinfo" runat="server" style="width: auto">
        <div align="right" class="text-info">
            Click on Receipt No to view Receipt
        </div>
    </div>
    <asp:Repeater ID="repInfo" runat="server">
        <HeaderTemplate>
            <table  style="width: 100%;" >
        </HeaderTemplate>
        <ItemTemplate>
            <tr style="border: none 0px; border-color: inherit;">
                <td align="left" valign="top" style="display:none; width: 65px; border: none 0px !important; padding-top: 10px;">
                    <asp:Image ID="imgEmpImage" runat="server" Height="80px" ImageUrl='<%# Bind("PHOTOPATH") %>'
                        ToolTip='<%# Bind("STU_NAME") %>' Width="50px" />
                </td>
                <td valign="top" align="left" style="border: none 0px !important;" colspan="2" width="70%" >
                    <table class="table table-striped table-bordered table-responsive text-left my-orders-table" width="100%" border="0">
                        <tr>
                            <td class="tdfieldsHome" style="font-weight: bold !important; width:180px;">
                                Student Id
                            </td>
                            <td class="tdfieldsHomeValue">
                                <asp:Literal ID="ltStuno" runat="server" Text='<%# Bind("STU_NO") %>'></asp:Literal>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdfieldsHome" style="font-weight: bold !important;">
                                Name
                            </td>
                            <td class="tdfieldsHomeValue">
                                <asp:Label ID="lblStuName" runat="server" Text='<%# Bind("STU_NAME") %>'></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdfieldsHome" style="font-weight: bold !important;">
                                Grade & Section
                            </td>
                            <td class="tdfieldsHomeValue">
                                <asp:Literal ID="ltGrade" runat="server" Text='<%# Bind("GRD_DISPLAY") %>'></asp:Literal>
                            </td>
                        </tr>
                        <%--<tr>
                            <td class="tdfieldsHome" style="font-weight: bold !important; width:100px;">
                                Section
                            </td>
                            <td class="tdfieldsHomeValue">
                                <asp:Literal ID="ltSct" runat="server" Text='<%# Bind("SCT_DESCR") %>'></asp:Literal>
                            </td>
                        </tr>--%>
                    </table>
                </td>
                <td valign="top" align="left" style="border: none 0px !important; padding-top: 10px;">
                    <table class="tableNoborder" style="padding: 0px; margin: 0px; height: 80px !important;
                        width: 100%;">
                        <tr>
                            <td style="vertical-align: baseline !important; text-align: center !important; color: #283043;   font-weight: bold; ">   <%--color: #FFA500;   font-weight: bold; font-size: 11pt;--%>
                                <asp:Label ID="ltCurrency" ForeColor="#283043" runat="server"><%= IIf(Session("BSU_CURRENCY") Is Nothing, "", Session("BSU_CURRENCY"))%></asp:Label>  <%--ForeColor="#FF7F50" Font-Size="10pt"--%>
                                <br />
                                <asp:Literal ID="ltAmount" runat="server" Text='<%# Bind("AMOUNT", "{0:0.00}")%>'></asp:Literal><br />
                                <br />
                                <asp:LinkButton ID="lblReceipt" Style="color: #0061c3; text-decoration: underline !important;"
                                    runat="server" Text='<%# Bind("RECNO") %>'></asp:LinkButton>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr style="border: none 0px !important;">
                <td colspan="3" style="border: none 0px !important;">
                    <hr class="margin-bottom0 margin-top0"/>
                </td>
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            <tr style="border: none 0px; font-size: 11pt; font-weight: bold;">
                <td align="right" colspan="2" style="border: none 0px !important; padding-top: 10px;">
                    TOTAL :
                </td>
                
                <td style="border: none 0px !important; padding-top: 10px; text-align: center !important;">
                    <asp:Label ID="lbl_Amount_F" runat="server" Text="0.00" />
                </td>
            </tr>
            </table>
        </FooterTemplate>
    </asp:Repeater>
</div>
