﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="urcRef_History.ascx.vb" Inherits="UserControl_urcRef_History" %>
   <table  width="100%" cellpadding="8" cellspacing="0" class="BlueTable_simple">
   <tr class="trHeader"><td colspan="4">&nbsp;Search Filter</td>
                       </tr>
                       <tr class="tdblankAll">
                       <td class="tdfields">Referral Name
                       </td>
                       <td colspan="3"> <asp:TextBox ID="txtref_name" runat="server" Width="200px" CssClass="input" ></asp:TextBox> 
                       
                       </td>
                       </tr>
                    <tr class="tdblankAll">
                       <td class="tdfields">Email Address
                       </td>
                       <td><asp:TextBox ID="txtEmail_addr" runat="server" Width="200px" CssClass="input" ></asp:TextBox>
                       
                       </td>
                        <td class="tdfields">Mobile No
                       </td>
                       <td><asp:TextBox ID="txtMob_no" runat="server" Width="200px" CssClass="input" ></asp:TextBox>
                       
                       </td>
                       </tr>
                        <tr class="tdblankAll">
                       <td colspan="4" align="center">
                       <asp:Button ID="btnSearch" runat="server" CssClass="button" Text="Search"  
 Width="90px"/>
                       </td>
                       </tr>
                      <tr class="tdblankAll" >
                      <td  class="tdblankAll"  colspan="4">
   <asp:GridView ID="gvRef" AutoGenerateColumns="False"  runat="server"
                AllowPaging="True" EmptyDataText="No records available."
                PageSize="10"
                  Width="100%"   CssClass="gridView" BorderStyle="None" BorderWidth="0px" 
             HeaderStyle-HorizontalAlign="Center">
           
                   <EmptyDataRowStyle  Wrap="True" HorizontalAlign="Center" />
                    <columns>
                        <asp:TemplateField HeaderText="RF_ID" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lblRFM_ID" runat="server" Text='<%# Bind("RFM_ID") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:TemplateField HeaderText="Referral Name" >
                               <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                 <ItemStyle HorizontalAlign="left" VerticalAlign="Middle"></ItemStyle>
                                 <ItemTemplate>
                      <asp:Label id="lblRF_NAME" runat="server" Text='<%# Bind("RFM_NAME") %>' ></asp:Label>                                 
                    </ItemTemplate>
                    </asp:TemplateField>
                     <asp:TemplateField HeaderText="Email Address" >
                               <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                 <ItemStyle HorizontalAlign="left" VerticalAlign="Middle"></ItemStyle>
                                 <ItemTemplate>
                           <asp:Label ID="lblRF_EMAIL" runat="server" Text='<%# bind("RFM_EMAIL") %>'></asp:Label>                                
                    </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Mobile No" >
                               <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                 <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                 <ItemTemplate>
                           <asp:Label ID="lblRF_MOB_NO" runat="server" Text='<%# bind("RFM_MOB_NO") %>'></asp:Label>                                
                    </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Student Name" >
                               <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                 <ItemStyle HorizontalAlign="left" VerticalAlign="Middle"></ItemStyle>
                                 <ItemTemplate>
                          <asp:Label ID="lblStudentName" runat="server" 
            Text='<%# Bind("SNAME") %>'></asp:Label>                             
                    </ItemTemplate>
                    </asp:TemplateField> 
                   <asp:TemplateField HeaderText="School" >
                               <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                 <ItemStyle HorizontalAlign="left" VerticalAlign="Middle"></ItemStyle>
                                 <ItemTemplate>
                           <asp:Label ID="lblBSU_NAME" runat="server" 
            Text='<%# Bind("BSU_NAME") %>'></asp:Label>                          
                    </ItemTemplate>
                    </asp:TemplateField>     

<asp:TemplateField HeaderText="Reward" >
                               <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                 <ItemStyle HorizontalAlign="left" VerticalAlign="Middle"></ItemStyle>
                                 <ItemTemplate>
                          <asp:Label ID="lblReward" runat="server" 
                                Text='<%# bind("RFD_Reward") %>'></asp:Label>                         
                    </ItemTemplate>
                    </asp:TemplateField> 

</columns>
                 <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
          <PagerStyle CssClass="gridpager" />  
                </asp:GridView>
    
    </td></tr></table>
    <asp:HiddenField ID="hfRFM_ID" runat="server" />