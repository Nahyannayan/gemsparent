﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared

Partial Class UserControl_urcParentShortProfile
    Inherits System.Web.UI.UserControl
    Dim Encrypter As New Encryption64

    Public Property EMP_ID() As String
        Get
            Return h_empID.Value
        End Get
        Set(value As String)
            h_empID.Value = value
        End Set
    End Property

    Public Property EMP_NO() As String
        Get
            Return h_EmpNo.Value
        End Get
        Set(value As String)
            h_EmpNo.Value = value
        End Set
    End Property
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Session("bPasswdChanged") = "False" Then
            Response.Redirect("~\UpdateInfo\ForcePasswordChange.aspx", False)
        ElseIf Session("bUpdateContactDetails") = "False" Then
            Response.Redirect(Session("ForceUpdate_stud"), False)
        End If

        If Page.IsPostBack = False Then
            Me.divTitle.Visible = False
            Bind_Employee_Info()

        End If
    End Sub
    Public Sub Bind_Employee_Info()
        Try

            'Dim strPath As String = Convert.ToString(readerStudent_Detail("STU_PHOTOPATH"))
            Dim connPath As String = WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ConnectionString
            Dim noImagePath As String = "~/Images/Home/no_image.gif"



            Dim con As String = ConnectionManger.GetOASISConnectionString
            Dim qry As String = "SELECT EMP_ID,EMPNO,EMPNAME,WORK_BSU_NAME,EMP_DEPT_DESCR,EMP_DES_DESCR,EMP_JOINDT FROM dbo.vw_OSO_EMPLOYEEMASTERDETAILS_COMPACT WHERE"
            If EMP_NO <> "" Then
                qry &= " EMPNO='" & EMP_NO & "'"
            Else
                qry &= " 1=2"
            End If
            '

            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(con, CommandType.Text, qry)

            If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                EMP_NO = ds.Tables(0).Rows(0)("EMPNO").ToString
                EMP_ID = ds.Tables(0).Rows(0)("EMP_ID").ToString
                Me.divTitle.Visible = True
            Else
                Me.divTitle.Visible = False
            End If

            repInfo.DataSource = ds.Tables(0)
            repInfo.DataBind()

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub repInfo_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles repInfo.ItemDataBound
        If e.Item.ItemType = ListItemType.Item Then
            Dim repChildInfo As New Repeater
            Dim plChildLink As New Panel
            Dim imgEmpImage As Image = DirectCast(e.Item.FindControl("imgEmpImage"), Image)
            repChildInfo = DirectCast(e.Item.FindControl("repChildInfo"), Repeater)
            plChildLink = DirectCast(e.Item.FindControl("plChildLink"), Panel)
            If Not imgEmpImage Is Nothing Then
                Dim EMD_PHOTO As String = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT ISNULL(EMD_PHOTO,'')EMD_PHOTO FROM dbo.EMPLOYEE_D WITH(NOLOCK) WHERE EMD_EMP_ID=" & EMP_ID & "").ToString
                Dim strImagePath As String = WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ConnectionString & "/" & EMD_PHOTO
                imgEmpImage.ImageUrl = strImagePath & "?" & DateTime.Now.Ticks.ToString()
            End If
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim ds As New DataSet
            Dim param(3) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@STU_ID", EMP_ID)
            param(1) = New SqlClient.SqlParameter("@MNU_TYPE", "PARENT")
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[OPL].[GETUSR_MENUS_QUICK_ACCESS]", param)
            If ds.Tables(0).Rows.Count > 0 Then
                plChildLink.Visible = True
                repChildInfo.DataSource = ds.Tables(0)
                repChildInfo.DataBind()
            Else
                plChildLink.Visible = False
            End If
        End If
    End Sub
    Sub R1_ItemCommand(ByVal Sender As Object, ByVal e As RepeaterCommandEventArgs)
        If e.CommandName = "EMP" Then
            Dim ARR As String() = New String(2) {}
            Dim splitter As Char = "|"
            ARR = e.CommandArgument.ToString.Split(splitter)
            getEasyAccessLink(ARR(0), ARR(1))
        End If
    End Sub

    Private Sub getEasyAccessLink(ByVal OPLM_ID As String, ByVal STU_ID As String)
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim USRNAME As String = String.Empty
        Dim USRPASSWORD As String = String.Empty
        Dim param(3) As SqlClient.SqlParameter
        param(0) = New SqlClient.SqlParameter("@STU_ID", STU_ID)
        param(1) = New SqlClient.SqlParameter("@OPLM_ID", OPLM_ID)
        param(2) = New SqlClient.SqlParameter("@MNU_TYPE", "PARENT")
        Using DATAREADER As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "[OPL].[GETUSR_MENUS_QUICK_ACCESS]", param)
            While DATAREADER.Read
                Session("Active_tab") = Convert.ToString(DATAREADER("OPLM_GROUP_NAME"))
                Session("Site_Path") = ">" & Session("Active_tab") & ">" & Convert.ToString(DATAREADER("OPLM_DESCR"))
                Session("Active_menu") = Convert.ToString(DATAREADER("OPLM_DESCR"))
                Dim url As String = Convert.ToString(DATAREADER("OPLM_URL")) & "?EMP_ID=" & Encrypter.Encrypt(STU_ID)
                Response.Redirect(url)
            End While

        End Using


    End Sub
End Class
