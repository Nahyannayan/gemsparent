﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="urcBookSalePaidResult.ascx.vb" Inherits="UserControl_urcBookSalePaidResult" %>
<link href="../CSS/SiteStyle.css" rel="stylesheet" type="text/css" />
<link href="../CSS/MenuStyle.css" rel="stylesheet" />
<div class="mainheading">
    <div>
        Payment Summary
    </div>
</div>
<div>
    <div class="divinfo" id="divinfo" runat="server" style="width: auto">
        <div align="right" class="text-info">
            Click on Receipt No to view Receipt
        </div>
    </div>
    <asp:Repeater ID="repInfo" runat="server">
        <HeaderTemplate>
            <table style="width: 100%;">
        </HeaderTemplate>
        <ItemTemplate>
            <tr style="border: none 0px; border-color: inherit;">
                <td align="left" valign="top" style="display: none; width: 65px; border: none 0px !important; padding-top: 10px;">
                    <%--<asp:Image ID="imgEmpImage" runat="server" Height="80px" ImageUrl='<%# Bind("PHOTOPATH") %>'
                                                        ToolTip='<%# Bind("STU_NAME") %>' Width="50px" />--%>
                </td>
                <td valign="top" align="left" style="border: none 0px !important;" colspan="4">
                    <table class="table table-striped table-bordered table-responsive text-left my-orders-table" width="100%" border="0">
                        <tr>
                            <td class="tdfieldsHome" width="100px">Student Id
                            </td>
                            <td class="tdfieldsHomeValue" style="font-weight: bold !important;">
                                <asp:Literal ID="ltStuno" runat="server" Text='<%# Bind("STU_NO") %>'></asp:Literal>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdfieldsHome">Name
                            </td>
                            <td class="tdfieldsHomeValue">
                                <asp:Label ID="lblStuName" Font-Bold="true" runat="server" Text='<%# Bind("STU_NAME") %>'></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdfieldsHome">Grade
                            </td>
                            <td class="tdfieldsHomeValue" style="font-weight: bold !important;">
                                <asp:Literal ID="ltGrade" runat="server" Text='<%# Bind("GRM_DISPLAY") %>'></asp:Literal>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdfieldsHome">Section
                            </td>
                            <td class="tdfieldsHomeValue" style="font-weight: bold !important;">
                                <asp:Literal ID="ltSct" runat="server" Text='<%# Bind("SCT_DESCR") %>'></asp:Literal>
                            </td>
                        </tr>
                    </table>
                </td>
                <td valign="top" align="left" style="border: none 0px !important; padding-top: 10px;">
                    <table class="tableNoborder" style="padding: 0px; margin: 0px; height: 80px !important; width: 100%;">
                        <tr>
                            <td style="vertical-align: baseline !important; text-align: center !important; color: #FFA500; font-weight: bold; font-size: 11pt;"><%--color: #FFA500;   font-weight: bold; font-size: 11pt;--%>
                                <asp:Label ID="ltCurrency" ForeColor="#FF7F50" Font-Size="10pt" runat="server"><%= IIf(Session("BSU_CURRENCY") Is Nothing, "", Session("BSU_CURRENCY"))%></asp:Label>
                                <%--ForeColor="#FF7F50" Font-Size="10pt"--%>
                                <br />
                                <asp:Literal ID="ltAmount" runat="server" Text='<%# Bind("AMOUNT", "{0:0.00}")%>'></asp:Literal><br />
                                <br />
                                <asp:LinkButton ID="lblReceipt" Style="color: Blue; text-decoration: underline !important;"
                                    runat="server" Text='<%# Bind("RECNO") %>'></asp:LinkButton>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr style="border: none 0px !important;">
                <td colspan="5" style="border: none 0px !important;">
                    <hr />
                </td>
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            <tr style="border: none 0px; font-size: 11pt; font-weight: bold;">
                <td align="right" colspan="4" style="width: 65px; border: none 0px !important; padding-top: 10px;">TOTAL
                </td>

                <td style="border: none 0px !important; padding-top: 10px; text-align: center !important;">
                    <asp:Label ID="lbl_Amount_F" runat="server" Text="0.00" />
                </td>
            </tr>
            </table>
        </FooterTemplate>
    </asp:Repeater>
</div>
