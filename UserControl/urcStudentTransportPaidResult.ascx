﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="urcStudentTransportPaidResult.ascx.vb"
    Inherits="UserControl_urcStudentTransportPaidResult" %>
<%--<link href="../CSS/SiteStyle.css" rel="stylesheet" type="text/css" />
<link href="../CSS/MenuStyle.css" rel="stylesheet" />
<link href="../App_Themes/900500/title.css" rel="stylesheet" />--%>
<div>
    <div class="left" style="font-weight: bold !important;">
        Payment Summary
    </div>
</div>
<div>
    
    <asp:Repeater ID="repInfo" runat="server">
        <HeaderTemplate>
            <table border="0" cellspacing="0" style="width: 100%; padding: 0px;" >
        </HeaderTemplate>
        <ItemTemplate>
            <tr style="border: none 0px; border-color: inherit;">
               <%-- <td align="left" valign="top" style="width: 65px; border: none 0px !important; padding-top: 5px;">
                    <asp:Image ID="imgEmpImage" runat="server" class="img-circle" ImageUrl='<%# Bind("PHOTOPATH") %>'
                        ToolTip='<%# Bind("STU_NAME") %>' />
                </td>--%>
                <td valign="top" align="left" colspan="2" style="border: none 0px !important;">
                    <table class="table table-striped table-bordered table-responsive text-left my-orders-table" style="width: 100%; padding: 3px;">
                        <tr>
                            <td class="tdfieldsHome" style="font-weight: bold !important; width:150px;">Student Id
                            </td>
                            <td class="tdfieldsHomeValue">
                                <asp:Literal ID="ltStuno" runat="server" Text='<%# Bind("STU_NO") %>'></asp:Literal>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdfieldsHome" style="font-weight: bold !important;">Name
                            </td>
                            <td class="tdfieldsHomeValue">
                                <asp:Label ID="lblStuName" runat="server" Text='<%# Bind("STU_NAME") %>'></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdfieldsHome" style="font-weight: bold !important;">Grade & Section
                            </td>
                            <td class="tdfieldsHomeValue">
                                <asp:Literal ID="ltGrade" runat="server" Text='<%# Bind("GRD_DISPLAY") %>'></asp:Literal>
                            </td>
                        </tr>
                        <%--<tr>
                            <td class="tdfieldsHome">Section
                            </td>
                            <td class="tdfieldsHomeValue" style="font-weight: bold !important;">
                                <asp:Literal ID="ltSct" runat="server" Text='<%# Bind("SCT_DESCR") %>'></asp:Literal>
                            </td>
                        </tr>--%>
                    </table>
                </td>
                <td align="left" style="border: none 0px !important; padding-top: 10px; vertical-align:top;">
                    <table class="tableNoborder" style="padding: 0px; margin: 0px; height: 80px !important; width: 100%;">
                        <tr>
                            <td style="vertical-align: baseline !important; text-align: center !important; color: #283043; font-weight: bold;">
                                <asp:Label ID="ltCurrency" ForeColor="#283043" runat="server" Text='<%# Bind("BSUCURRENCY")%>'></asp:Label>
                                <br />
                                <asp:Literal ID="ltAmount" runat="server" Text='<%# Bind("AMOUNT", "{0:0.00}")%>'></asp:Literal><br />
                                <br />
                                <asp:Label ID="lblReceipt" runat="server" Text='<%# Bind("RECNO") %>'></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr style="border: none 0px !important;">
                <td colspan="3" style="border: none 0px !important;">
                    <hr class="margin-bottom0 margin-top0" />
                </td>
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            <tr style="border: none 0px; font-size: 11pt; font-weight: bold;">
                <td align="right" colspan="2" style=" border: none 0px !important; padding-top: 10px;">TOTAL :
                </td>
                
                <td style="border: none 0px !important; padding-top: 10px; text-align: center !important;">
                    <asp:Label ID="lbl_Amount_F" runat="server" Text="0.00" />
                </td>
            </tr>
            </table>
        </FooterTemplate>
    </asp:Repeater>
</div>
