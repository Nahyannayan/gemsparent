﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Partial Class UserControl_urcExtraProvisionPaidResult
    Inherits System.Web.UI.UserControl

    Private Property bBSUTaxable() As Boolean
        Get
            Return ViewState("bBSUTaxable")
        End Get
        Set(ByVal value As Boolean)
            ViewState("bBSUTaxable") = value
        End Set
    End Property
    Public ReceiptNos() As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("bPasswdChanged") = "False" Then
            Response.Redirect("~\UpdateInfo\ForcePasswordChange.aspx", False)
        ElseIf Session("bUpdateContactDetails") = "False" Then
            Response.Redirect(Session("ForceUpdate_stud"), False)
        End If
    End Sub
    Protected Sub RemoveInitiatedStatus()
        '[OASIS].[ACTIVITY_PAYMENT_INITIATION]
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim objConn As New SqlConnection(str_conn)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try
            Dim Param(1) As SqlParameter
            Param(0) = New SqlClient.SqlParameter("@APD_ID", Session("activity_apd_id"))
            Param(1) = New SqlClient.SqlParameter("@TYPE", "U")
            SqlHelper.ExecuteNonQuery(str_conn, "[OASIS].[ACTIVITY_PAYMENT_INITIATION]", Param)
            stTrans.Commit()
        Catch ex As Exception
            stTrans.Rollback()
        End Try
    End Sub
    Public Sub Gridbind_PayDetails(ByVal FCO_ID As Int64, ByVal bPaymentSuccess As Boolean, ByVal regMode As String)
        Try

            hidregMode.Value = regMode
            'Dim strPath As String = Convert.ToString(readerStudent_Detail("STU_PHOTOPATH"))
            ' Dim connPath As String = WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ConnectionString
            Dim noImagePath As String = "~/Images/Home/no_image.gif"
            Dim con As String = ConnectionManger.GetOASIS_SERVICESConnectionString
            Dim param(2) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@REF_ID", FCO_ID)
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "[FEES].[GET_FEE_RECEIPT_BY_ID]", param)
            Dim rowcount As Integer = ds.Tables(0).Rows.Count()
            If rowcount > 0 Then
                repinfo.DataSource = ds
                repinfo.DataBind()
            End If
            If Not (bPaymentSuccess) Then
                RemoveInitiatedStatus()
            End If
        Catch ex As Exception
            ' repInfo.DataBind()
        End Try
    End Sub



    Dim ReptrSum As Double = 0

    Protected Sub repInfo_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles repinfo.ItemDataBound
        'If e.Item.DataItem Is Nothing Then
        Dim Label_TxnRef As New Label
        Dim lblReceipt As New LinkButton
        Dim Encr_decrData As New Encryption64
        lblReceipt = DirectCast(e.Item.FindControl("lnkReceipt"), LinkButton)
        Label_TxnRef = DirectCast(e.Item.FindControl("Label_MerchTxnRef"), Label)

        Dim PopForm As String = "../Service/ServiceFeeReceipt.aspx"
        Dim Type As String = "REC"

        lblReceipt.Attributes.Add("onClick", "return ShowSubWindowWithClose('" & PopForm & "?BsuId=" & Encr_decrData.Encrypt(Session("sBsuid")) & "&type=" & Type & "&id=" & Encr_decrData.Encrypt(lblReceipt.Text.Trim) & "', '', '80%', '80%');")
        'End If

        'If e.Item.DataItem Is Nothing Then
        '    Dim lbl_Amount_F As New Label
        '    If ReceiptNos Is Nothing Then
        '        ReDim ReceiptNos(-1)
        '    End If

        '    lbl_Amount_F = repInfo.Controls(repInfo.Controls.Count - 1).Controls(0).FindControl("lbl_Amount_F")
        '    If Not lbl_Amount_F Is Nothing Then
        '        lbl_Amount_F.Text = ReptrSum.ToString("#,##0.00")
        '    End If
        '    If ReceiptNos.Length <= 0 Then
        '        Me.divinfo.Visible = False
        '    ElseIf ReceiptNos(0) <> "" Then
        '        Me.divinfo.Visible = True
        '    End If
        '    Return
        'Else
        '    If ReceiptNos Is Nothing Then
        '        ReDim ReceiptNos(-1)
        '    End If
        '    Dim lblReceipt As New LinkButton
        '    Dim trRecNum As New HtmlTableRow
        '    Dim TRreceipt As New HtmlTableRow
        '    Dim ltAmount As New Literal
        '    Dim Encr_decrData As New Encryption64
        '    Dim lbl_Amount_F As Label
        '    lblReceipt = DirectCast(e.Item.FindControl("lblReceipt"), LinkButton)
        '    ltAmount = DirectCast(e.Item.FindControl("ltAmount"), Literal)
        '    trRecNum = DirectCast(e.Item.FindControl("trRecNum"), HtmlTableRow)
        '    TRreceipt = DirectCast(e.Item.FindControl("TRreceipt"), HtmlTableRow)
        '    lbl_Amount_F = DirectCast(e.Item.FindControl("lbl_Amount_F"), Label)
        '    ReptrSum += IIf(Not ltAmount Is Nothing, CDbl(ltAmount.Text), 0)
        '    lbl_Amount_F.Text = ReptrSum
        '    If lblReceipt.Text = "" Then
        '        Me.divinfo.Visible = False
        '        trRecNum.Visible = False
        '        TRreceipt.Visible = False
        '    Else
        '        Dim PopForm As String = "../Fees/FeeReceipt.aspx"
        '        If Not lblReceipt Is Nothing Then
        '            ReDim Preserve ReceiptNos(UBound(ReceiptNos) + 1)
        '            ReceiptNos(UBound(ReceiptNos)) = lblReceipt.Text
        '            Dim Type As String = ""
        '            If hidregMode.Value = "FC" Then
        '                Type = "REC"
        '            ElseIf hidregMode.Value = "OC" Then
        '                Type = "OC"
        '            End If
        '            lblReceipt.Attributes.Add("onClick", "return ShowSubWindowWithClose('" & PopForm & "?type=" & Type & "&id=" & Encr_decrData.Encrypt(lblReceipt.Text.Trim) & "', '', '80%', '80%');")
        '        End If
        '    End If

        'End If
    End Sub
End Class
