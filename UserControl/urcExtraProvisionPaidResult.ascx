﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="urcExtraProvisionPaidResult.ascx.vb" Inherits="UserControl_urcExtraProvisionPaidResult" %>

<link href="../css/style.css" rel="stylesheet" type="text/css" />
<link href="../css/bootsrap.css" rel="stylesheet" />

<div class="mainheading">
    <div>Transaction Summary</div>
</div>
<div>
    <style>
        .td {
            width: 50%;
        }
    </style>

    <asp:Repeater ID="repinfo" runat="server">
        <%--<HeaderTemplate>
            <table class="childLink" style="width: 100%;">
        </HeaderTemplate>--%>
        <ItemTemplate>
            <tr style="border: none 0px; border-color: inherit;">
                <%--<td align="left" valign="top" >
                    <asp:Image ID="imgEmpImage" runat="server" Height="80px" ImageUrl='<%# Bind("PHOTOPATH") %>'
                        ToolTip='<%# Bind("APD_STU_NAME") %>' Width="50px" />
                </td>--%>
                <td valign="top" align="left">
                    <table width="100%" class="table table-striped table-bordered table-responsive text-left my-orders-table">
                        <tr class="tdfields">
                            <th align="left" width="30%">Date
                            </th>
                            <td align="left">
                                <asp:Label ID="lblDate" runat="server" CssClass="tdfields" Text='<%# Bind("DATE")%>' ></asp:Label>
                            </td>
                        </tr>
                        <tr class="tdfields">
                            <th align="left">Student ID 
                            </th>
                            <td align="left">
                                <asp:Label ID="lblStudentNo" runat="server" Text='<%# Bind("STU_NO") %>'></asp:Label>
                            </td>
                        </tr>
                        <tr class="tdfields">
                            <th align="left">Student Name
                            </th>
                            <td align="left">
                                <asp:Label ID="lblStudentName" runat="server" Text='<%# Bind("STU_NAME") %>'></asp:Label>
                            </td>
                        </tr>
                        <tr class="tdfields">
                            <th align="left">Transaction Reference
                            </th>
                            <td align="left" valign="middle">
                                <asp:Label ID="Label_MerchTxnRef" runat="server" Text='<%# Bind("REF_ID")%>' />
                            </td>
                        </tr>
                        <tr class="tdfields">
                            <th align="left">Amount
                            </th>
                            <td align="left" valign="middle">
                                <asp:Label ID="Label_Amount" runat="server" Text='<%# Bind("AMOUNT", "{0:0.00}")%>'  />
                            </td>
                        </tr>
                        <tr class="tdfields">
                            <th align="left">Receipt Number
                            </th>
                            <td align="left" valign="middle">&nbsp;<asp:LinkButton ID="lnkReceipt" runat="server" CssClass="MenuLink" Text='<%# Bind("RECNO") %>'></asp:LinkButton>
                                <asp:HiddenField ID="h_Recno" runat="server" />
                                <asp:HiddenField ID="hf_encr_bsuId" runat="server" />
                            </td>
                        </tr>

                        <tr>
                            <td align="left" style="font-weight: bold; font-size: 12px; color: #ff0000" colspan="2">
                                <asp:Label ID="lblPrintMsg" runat="server" EnableViewState="False" Visible="False">Please print the receipt (Click on the receipt number) for your reference.</asp:Label>
                            </td>
                        </tr>
                        <tr style="display: none;">
                            <td align="center" style="font-weight: bold; font-size: 12px; color: #ff0000" colspan="2">
                                <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="btn btn-info"
                                    Text="Cancel" TabIndex="158" Visible="false" />
                            </td>
                        </tr>
                    </table>
                </td>
                <%-- <td valign="top" align="left">
                    <table class="table table-striped table-bordered table-responsive text-left my-orders-table" width="100%">
                        
                    </table>
                </td>--%>
            </tr>
            <tr>
                <td colspan="3">
                    <hr />
                </td>
            </tr>
        </ItemTemplate>
        <%-- <FooterTemplate>           
            </table>
        </FooterTemplate>--%>
    </asp:Repeater>
    <div id="divinfo" runat="server">
        <div>
            <asp:Label runat="server" ID="lblrecepitprintmsg" Text="Click on Receipt No to view Receipt."></asp:Label>
        </div>
    </div>
    <asp:HiddenField ID="hidregMode" Value="" runat="server" />
</div>
