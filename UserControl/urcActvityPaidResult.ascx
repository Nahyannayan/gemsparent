﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="urcActvityPaidResult.ascx.vb" Inherits="UserControl_urcActvityPaidResult" %>

<%--<link href="../css/style.css" rel="stylesheet" type="text/css" />
<link href="../css/bootsrap.css" rel="stylesheet" />--%>

<div class="left" style="font-weight :bold !important;">
    <div>Transaction Summary</div>
</div>
<div>
   <%-- <style>
        .td{
            width:50%;
        }
    </style>--%>
   
    <asp:Repeater ID="repinfo" runat="server">
        <%--<HeaderTemplate>
            <table class="childLink" style="width: 100%;">
        </HeaderTemplate>--%>
        <ItemTemplate>
            <tr style="border: none 0px; border-color: inherit;">
                <%--<td align="left" valign="top" >
                    <asp:Image ID="imgEmpImage" runat="server" Height="80px" ImageUrl='<%# Bind("PHOTOPATH") %>'
                        ToolTip='<%# Bind("APD_STU_NAME") %>' Width="50px" />
                </td>--%>
                <td valign="top" align="left" >
                    <table class="table table-striped table-bordered table-responsive text-left my-orders-table" width="100%" border="0">
                        <tr>
                            <td  style="font-weight :bold !important; width:30%;">
                                Student Id
                            </td>
                            <td >
                                <asp:Literal ID="ltStuno" runat="server" Text='<%# Bind("STU_NO") %>'></asp:Literal>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight :bold !important;">
                                Name
                            </td>
                            <td >
                                <asp:Label ID="lblStuName" runat="server" Text='<%# Bind("APD_STU_NAME") %>'></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight :bold !important;">
                                Grade & Section
                            </td>
                            <td  >
                                <asp:Literal ID="ltGrade" runat="server" Text='<%# Bind("APD_GRADE_ID")%>'></asp:Literal> - 
                                <asp:Literal ID="ltSct" runat="server" Text='<%# Bind("SCT_DESCR") %>'></asp:Literal>
                            </td>
                        </tr>
                        <%--<tr>
                            <td >
                                
                            </td>
                            <td  >
                               
                            </td>
                        </tr>--%>
                         <tr>
                            <td style="font-weight :bold !important; " >
                                Activity Name 
                            </td>
                            <td  >
                                <asp:Literal ID="Literal1" runat="server" Text='<%# Bind("ALD_EVENT_NAME") %>'></asp:Literal>
                            </td>
                        </tr>
                         <tr>
                            <td style="font-weight :bold !important;">
                                Activity On 
                            </td>
                            <td  >
                                <asp:Literal ID="Literal2" runat="server" Text='<%# Bind("EVENT_DATE") %>'></asp:Literal>
                            </td>
                        </tr>
                        <tr >
                            <td align="left" style="font-weight :bold !important;">Total Amount
                            </td>

                            <td>
                                <asp:Label ID="lbl_Currency" runat="server" Text='<%# Bind("BSU_CURRENCY")%>' />&nbsp;&nbsp;<asp:Label ID="lbl_Amount_F" runat="server" Text="0.00" />
                                
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight :bold !important;">
                                Transaction Reference
                            </td>
                            <td>
                                 <asp:Literal ID="Literal3" runat="server" Text='<%# Bind("REF_ID")%>'></asp:Literal>
                            </td>
                        </tr>
                        <tr runat="server" id="TRreceipt">
                            <td style="font-weight :bold !important;">
                               Transaction Date
                            </td>
                            <td>
                                
                                 <asp:Literal ID="Literal5" runat="server" Text='<%# Bind("DATE")%>'></asp:Literal>
                            </td>
                        </tr>
                        <tr id="trRecNum" runat="server">
                            <td style="font-weight :bold !important;">
                                Receipt Number 
                            </td>
                            <td>
                           <%-- <td style="vertical-align: baseline !important; text-align: center !important; color: #FFA500;
                                font-weight: bold; ">--%>
                               <%-- <asp:Label ID="ltCurrency" ForeColor="#FF7F50"  runat="server"><%= IIf(Session("BSU_CURRENCY") Is Nothing, "", Session("BSU_CURRENCY"))%></asp:Label>
                                <br />--%>
                                <asp:Literal ID="ltAmount" runat="server" Text='<%# Bind("AMOUNT", "{0:0.00}")%>' Visible="false"></asp:Literal><br />
                               <%-- <br />--%>
                                <asp:LinkButton ID="lblReceipt" 
                                    runat="server" Text='<%# Bind("RECNO") %>'></asp:LinkButton>
                              
                            </td>
                        </tr>
                    </table>
                </td>
               <%-- <td valign="top" align="left">
                    <table class="table table-striped table-bordered table-responsive text-left my-orders-table" width="100%">
                        
                    </table>
                </td>--%>
            </tr>
            <tr >
                <td colspan="3" >
                    <hr />
                </td>
            </tr>
        </ItemTemplate>
       <%-- <FooterTemplate>           
            </table>
        </FooterTemplate>--%>
    </asp:Repeater>
    <%-- <div id="divinfo" runat="server">
        <div>
            <asp:Label runat="server" ID="lblrecepitprintmsg" text="Click on Receipt No to view Receipt."></asp:Label>
        </div>
    </div>--%>
      <asp:HiddenField ID="hidregMode" Value ="" runat="server"/>
</div>