Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Partial Class Students_BehaviorManagement_UserControl_bm_ActionViewDetails
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            Dim info As String = Request.QueryString("Info_id")
            Dim val As String() = info.Split(",")
            Hiddenstuid.Value = val(0) ''"51823"
            Hiddenincidentid.Value = val(1) '' "2"
            BindPrimaryInformation()
            IncidentDetails()
            GetWitNessDeatils()
            ActionTakendetails()
            Response.Cache.SetCacheability(HttpCacheability.Public)
            Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
            Response.Cache.SetAllowResponseInBrowserHistory(False)
        End If
    End Sub
    Public Sub BindPrimaryInformation()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString

        Dim str_query = "SELECT STU_ID, STU_NO, STUDENT_M.STU_FIRSTNAME , STUDENT_M.STU_LASTNAME,SCT_DESCR,GRM_DISPLAY,STM_DESCR,SHF_DESCR,STUDENT_M.STU_PRIMARYCONTACT ," & _
                        "(Case student_m.STU_PRIMARYCONTACT when 'F' then STUDENT_D.STS_FEMAIL  WHEN 'M' then STUDENT_D.STS_MEMAIL  when 'G' then STUDENT_D.STS_GEMAIL  end) as ParentEmail  ," & _
                        "(Case student_m.STU_PRIMARYCONTACT when 'F' then STUDENT_D.STS_FMOBILE  WHEN 'M' then STUDENT_D.STS_MMOBILE  when 'G' then STUDENT_D.STS_GMOBILE  end) as ParentMobile , " & _
                        "(Case student_m.STU_PRIMARYCONTACT when 'F' then STUDENT_D.STS_FFIRSTNAME + ' ' + STUDENT_D.STS_FLASTNAME when 'M' then STUDENT_D.STS_MFIRSTNAME + ' ' + STUDENT_D.STS_MLASTNAME when 'G' then STUDENT_D.STS_GFIRSTNAME + ' ' + STUDENT_D.STS_GLASTNAME end) as parent_name " & _
                        " FROM STUDENT_M " & _
                        "INNER JOIN SECTION_M ON   SECTION_M.SCT_ID =STUDENT_M.STU_SCT_ID  " & _
                        "INNER JOIN STREAM_M ON STREAM_M.STM_ID =STUDENT_M.STU_STM_ID " & _
                        "INNER JOIN SHIFTS_M ON SHIFTS_M.SHF_ID=STUDENT_M.STU_SHF_ID  " & _
                        "INNER JOIN GRADE_BSU_M ON GRADE_BSU_M.GRM_ID= STUDENT_M.STU_GRM_ID " & _
                        "INNER JOIN STUDENT_D ON  STUDENT_D.STS_STU_ID= STUDENT_M.STU_ID " & _
                        " WHERE STU_ID=" & Session("STU_ID") & ""

        '"INNER JOIN BM.BM_ACTION_MASTER ON BM_ACTION_MASTER.BM_STU_ID=STUDENT_M.STU_ID " & _
        'BM_ACTION_MASTER.BM_ID=" & Hiddenincidentid.Value & " AND

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ''Student Information
        'If ds.Tables(0).Rows.Count >= 1 Then
        '    lblstudentname.Text = ds.Tables(0).Rows(0).Item("STU_FIRSTNAME").ToString() & " " & ds.Tables(0).Rows(0).Item("STU_LASTNAME").ToString()
        '    lblstudentgrade.Text = ds.Tables(0).Rows(0).Item("grm_display").ToString()
        '    lblstudentshift.Text = ds.Tables(0).Rows(0).Item("shf_descr").ToString()
        '    lblstudentstream.Text = ds.Tables(0).Rows(0).Item("stm_descr").ToString()
        '    lblstudentsection.Text = ds.Tables(0).Rows(0).Item("sct_descr").ToString()

        '    ''Parent Information
        '    lblparentname.Text = ds.Tables(0).Rows(0).Item("parent_name").ToString()
        '    lblparentemailid.Text = ds.Tables(0).Rows(0).Item("ParentEmail").ToString()
        '    lblparentcontactnumber.Text = ds.Tables(0).Rows(0).Item("ParentMobile").ToString()
        'End If

    End Sub
    Public Sub IncidentDetails()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_query = "SELECT *, (EMP_FNAME+' ' +EMP_LNAME) AS STAFF FROM BM.BM_MASTER A  " & _
                        " INNER JOIN EMPLOYEE_M  B on  A.BM_REPORTING_STAFF_ID = B.emp_id " & _
                        " WHERE A.BM_ID=" & Hiddenincidentid.Value & ""
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        lbltype.Text = ds.Tables(0).Rows(0).Item("BM_INCIDENT_TYPE").ToString()
        If lbltype.Text = "FA" Then
            lbltype.Text = "For Action"
        Else
            lbltype.Text = "For Information"
        End If
        lbltodaydate.Text = Format(ds.Tables(0).Rows(0).Item("BM_ENTRY_DATE"), "dd-MMM-yyyy")

        lblreportingstaff.Text = ds.Tables(0).Rows(0).Item("STAFF").ToString()
        lblincidentdate.Text = Format(ds.Tables(0).Rows(0).Item("BM_INCIDENT_DATE"), "dd-MMM-yyyy")
        txtReportonincident.Text = ds.Tables(0).Rows(0).Item("BM_REPORT_ON_INCIDENT").ToString
    End Sub
    Public Function GetDesignation(ByVal DesigId As String) As String
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_query As String = "SELECT DES_ID,DES_DESCR,DES_FLAG FROM EMPDESIGNATION_M WHERE DES_ID=" & DesigId

            Dim dsCategory As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            If dsCategory.Tables(0).Rows.Count >= 1 Then
                Return IIf(IsDBNull(dsCategory.Tables(0).Rows(0).Item("DES_DESCR")), 0, dsCategory.Tables(0).Rows(0).Item("DES_DESCR"))
            End If
        Catch ex As Exception

        End Try
    End Function
    Private Sub GetWitNessDeatils()
        Try
            'AS VIEWID
            Dim blnHeader As Boolean = False

            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim str_query = "SELECT BM_WITNESS_ID,BM_ID,BM_WITNESSID,BM_WITNESS_TYPE,BM_WITNESS_SAID FROM BM.BM_INCIDENTWITNESS WHERE BM_ID=" & Hiddenincidentid.Value & ""

            Dim dsWitnesses As DataSet
            dsWitnesses = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

            If blnHeader = False Then ' Header Information Included
                Dim Headerrow_1 As New TableRow
                Headerrow_1.Style("font-weight") = "bold"
                Headerrow_1.Style("font-size") = "8pt"
                Headerrow_1.Style("text-align") = "center"
                Headerrow_1.Style("Height") = "5px"
                Headerrow_1.Style("font-family") = "verdana,timesroman,garamond"

                For intCols As Integer = 1 To 4
                    Dim HeaderCell_1 As New TableCell
                    HeaderCell_1.ColumnSpan = "0"
                    HeaderCell_1.Style("Height") = "5px"
                    HeaderCell_1.Wrap = True
                    HeaderCell_1.BorderStyle = BorderStyle.Solid
                    HeaderCell_1.BorderWidth = "1"
                    HeaderCell_1.Style("FONT-SIZE") = "12px"
                    HeaderCell_1.Style("FONT-FAMILY") = "Verdana"
                    HeaderCell_1.Style("Text-Align") = "center"
                    HeaderCell_1.Style("FONT-WEIGHT") = "bold"
                    If intCols = 1 Then
                        HeaderCell_1.Style("Width") = "50px"
                        HeaderCell_1.Text = "SlNo"
                    ElseIf intCols = 2 Then
                        HeaderCell_1.Style("Width") = "170px"
                        HeaderCell_1.Text = "Staff/Student"
                    ElseIf intCols = 3 Then
                        HeaderCell_1.Style("Width") = "170px"
                        HeaderCell_1.Text = "No"
                    ElseIf intCols = 4 Then
                        HeaderCell_1.Text = "Name"
                    End If
                    Headerrow_1.Cells.Add(HeaderCell_1)
                Next
                tabWitnessDet.Rows.Add(Headerrow_1)
            End If 'Ending Header If

            For IntCnt As Integer = 0 To dsWitnesses.Tables(0).Rows.Count - 1
                With dsWitnesses.Tables(0)
                    If .Rows(IntCnt).Item("BM_WITNESS_TYPE") = "STU" Then
                        Dim dssStudent As DataSet
                        dssStudent = GetStudentDetails(.Rows(IntCnt).Item("BM_WITNESSID"))

                        Dim Childrow_1 As New TableRow
                        Childrow_1.Style("font-weight") = "bold"
                        Childrow_1.Style("font-size") = "8pt"
                        Childrow_1.Style("text-align") = "center"
                        Childrow_1.Style("Height") = "5px"
                        Childrow_1.Style("font-family") = "verdana,timesroman,garamond"

                        For intCols As Integer = 1 To 4
                            Dim ChildCell_1 As New TableCell
                            ChildCell_1.ColumnSpan = "0"
                            ChildCell_1.Style("Height") = "5px"
                            ChildCell_1.Wrap = True
                            ChildCell_1.BorderStyle = BorderStyle.Solid
                            ChildCell_1.BorderWidth = "1"
                            ChildCell_1.Style("FONT-SIZE") = "12px"
                            ChildCell_1.Style("FONT-FAMILY") = "Verdana"
                            ChildCell_1.Style("text-align") = "center"
                            'HeaderCell_1.Style("FONT-WEIGHT") = "bold"
                            If intCols = 1 Then
                                ChildCell_1.Style("Width") = "50px"
                                ChildCell_1.Text = IntCnt + 1
                            ElseIf intCols = 2 Then
                                ChildCell_1.Style("Width") = "170px"
                                ChildCell_1.Text = "Student"
                            ElseIf intCols = 3 Then
                                ChildCell_1.Style("Width") = "170px"
                                ChildCell_1.Text = dssStudent.Tables(0).Rows(0).Item("STU_NO")
                            ElseIf intCols = 4 Then
                                ChildCell_1.Text = dssStudent.Tables(0).Rows(0).Item("STUDENTNAME")
                            End If
                            Childrow_1.Cells.Add(ChildCell_1)
                        Next
                        tabWitnessDet.Rows.Add(Childrow_1)
                        'Witnesss Comments Including
                        Dim CommentsRow As New TableRow
                        CommentsRow.Style("font-weight") = "bold"
                        CommentsRow.Style("font-size") = "8pt"
                        CommentsRow.Style("text-align") = "center"
                        CommentsRow.Style("Height") = "5px"
                        CommentsRow.Style("font-family") = "verdana,timesroman,garamond"
                        'Comment Cell
                        Dim CommentCell As New TableCell
                        CommentCell.ColumnSpan = "0"
                        CommentCell.Style("Height") = "5px"
                        CommentCell.ColumnSpan = 4
                        CommentCell.Wrap = True
                        CommentCell.BorderStyle = BorderStyle.Solid
                        CommentCell.BorderWidth = "1"
                        CommentCell.Style("FONT-SIZE") = "12px"
                        CommentCell.Style("FONT-FAMILY") = "Verdana"
                        CommentCell.Style("text-align") = "left"
                        CommentCell.Text = .Rows(IntCnt).Item("BM_WITNESS_SAID")
                        CommentsRow.Cells.Add(CommentCell)
                        tabWitnessDet.Rows.Add(CommentsRow)

                    Else
                        Dim dsStaffDet As DataSet
                        dsStaffDet = GetStaffDetails(.Rows(IntCnt).Item("BM_WITNESSID"))

                        Dim Childrow_1 As New TableRow
                        Childrow_1.Style("font-weight") = "bold"
                        Childrow_1.Style("font-size") = "8pt"
                        Childrow_1.Style("text-align") = "center"
                        Childrow_1.Style("Height") = "5px"
                        Childrow_1.Style("font-family") = "verdana,timesroman,garamond"
                        For intCols As Integer = 1 To 4
                            Dim ChildCell_1 As New TableCell
                            ChildCell_1.ColumnSpan = "0"
                            ChildCell_1.Style("Height") = "5px"
                            ChildCell_1.Wrap = True
                            ChildCell_1.BorderStyle = BorderStyle.Solid
                            ChildCell_1.BorderWidth = "1"
                            ChildCell_1.Style("FONT-SIZE") = "12px"
                            ChildCell_1.Style("FONT-FAMILY") = "Verdana"
                            ChildCell_1.Style("text-align") = "center"
                            'HeaderCell_1.Style("FONT-WEIGHT") = "bold"
                            If intCols = 1 Then
                                ChildCell_1.Style("Width") = "50px"
                                ChildCell_1.Text = IntCnt + 1
                            ElseIf intCols = 2 Then
                                ChildCell_1.Style("Width") = "170px"
                                ChildCell_1.Text = "Staff"
                            ElseIf intCols = 3 Then
                                ChildCell_1.Style("Width") = "170px"
                                ChildCell_1.Text = dsStaffDet.Tables(0).Rows(0).Item("EMPNO")
                            ElseIf intCols = 4 Then
                                ChildCell_1.Text = dsStaffDet.Tables(0).Rows(0).Item("EMPNAME")
                            End If
                            Childrow_1.Cells.Add(ChildCell_1)
                        Next
                        tabWitnessDet.Rows.Add(Childrow_1)
                        'Witnesss Comments Including
                        Dim CommentsRow As New TableRow
                        CommentsRow.Style("font-weight") = "bold"
                        CommentsRow.Style("font-size") = "8pt"
                        CommentsRow.Style("text-align") = "center"
                        CommentsRow.Style("Height") = "5px"
                        CommentsRow.Style("font-family") = "verdana,timesroman,garamond"
                        'Comment Cell
                        Dim CommentCell As New TableCell
                        CommentCell.ColumnSpan = "0"
                        CommentCell.Style("Height") = "5px"
                        CommentCell.ColumnSpan = 4
                        CommentCell.Wrap = True
                        CommentCell.BorderStyle = BorderStyle.Solid
                        CommentCell.BorderWidth = "1"
                        CommentCell.Style("FONT-SIZE") = "12px"
                        CommentCell.Style("FONT-FAMILY") = "Verdana"
                        CommentCell.Style("text-align") = "left"
                        CommentCell.Text = .Rows(IntCnt).Item("BM_WITNESS_SAID")
                        CommentsRow.Cells.Add(CommentCell)
                        tabWitnessDet.Rows.Add(CommentsRow)

                    End If

                End With
            Next
        Catch ex As Exception
        End Try
    End Sub

    Private Sub GetFollowUpDetails()

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        'Dim str_query As String = " SELECT BM_ID,BM_ACTION_ID,BM_ACTIONDATE,BM_COMMENTS,STAFF_ID,BM_FWDTO,EMPNO," & _
        '     "(ISNULL(EMP_FNAME,'') + ' '+ ISNULL(EMP_LNAME,'')) As EmpName, DES_DESCR " & _
        '     "FROM BM.BM_ACTIONDETAIL A INNER JOIN EMPLOYEE_M B ON A.STAFF_ID=B.EMP_ID " & _
        '     "LEFT OUTER JOIN EMPDESIGNATION_M C ON A.BM_DESIGID=C.DES_ID AND BM_ID=" & Hiddenincidentid.Value & " " & _
        '     "AND BM_ACTION_ID=" & HidActionID.Value

        Dim str_query As String = " SELECT BM_ID,BM_ACTION_ID,BM_ACTIONDATE,BM_COMMENTS,STAFF_ID,BM_FWDTO,B.EMPNO," & _
                  "(ISNULL(B.EMP_FNAME,'') + ' '+ ISNULL(B.EMP_LNAME,'')) As EmpName, C.DES_DESCR,T.DES_DESCR As ToName " & _
                  "FROM BM.BM_ACTIONDETAIL A INNER JOIN EMPLOYEE_M B ON A.STAFF_ID=B.EMP_ID " & _
                  "INNER JOIN EMPDESIGNATION_M C ON A.BM_DESIGID=C.DES_ID " & _
                  "LEFT JOIN EMPDESIGNATION_M T ON T.DES_ID=A.BM_FWDTO " & _
                  " WHERE BM_ID=" & Hiddenincidentid.Value & " " & _
                  " AND BM_ACTION_ID=" & HidActionID.Value & " ORDER BY BM_ACTIONDET_ID DESC"


        Dim dsFollowUp As DataSet
        dsFollowUp = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        If dsFollowUp.Tables(0).Rows.Count >= 1 Then
            For IntRow As Integer = 0 To dsFollowUp.Tables(0).Rows.Count - 1

                For intI As Integer = 0 To 1
                    Dim FollowUprow As New TableRow
                    FollowUprow.Style("font-weight") = "bold"
                    FollowUprow.Style("font-size") = "8pt"
                    FollowUprow.Style("text-align") = "left"
                    FollowUprow.Style("Height") = "5px"
                    FollowUprow.Style("font-family") = "verdana,timesroman,garamond"

                    'Create a Cell
                    Dim FollowUpCell As New TableCell
                    FollowUpCell.ColumnSpan = "0"
                    FollowUpCell.Style("Height") = "5px"
                    FollowUpCell.Wrap = True
                    FollowUpCell.BorderStyle = BorderStyle.Solid
                    FollowUpCell.BorderWidth = "1"
                    FollowUpCell.Style("FONT-SIZE") = "10px"
                    FollowUpCell.Style("FONT-FAMILY") = "Verdana"
                    FollowUpCell.Style("Text-Align") = "left"
                    FollowUpCell.Style("FONT-WEIGHT") = "bold"
                    With dsFollowUp.Tables(0).Rows(IntRow)
                        If intI = 0 Then
                            FollowUpCell.Style("COLOR") = "red"
                            If GetDesignation(.Item("BM_FWDTO")) = "" Then
                                FollowUpCell.Text = Format(CDate(.Item("BM_ACTIONDATE")), "dd-MMM-yyyy").ToString + " :  Action Taken By  :  " + .Item("DES_DESCR").ToString + "   " + "  Mr/Miss : " + .Item("EmpName").ToString()
                            Else
                                FollowUpCell.Text = Format(CDate(.Item("BM_ACTIONDATE")), "dd-MMM-yyyy").ToString + " -->" + .Item("DES_DESCR").ToString + "  Forwarded to --> " + IIf(GetDesignation(.Item("BM_FWDTO")) = "", " Action Taken By " + .Item("EmpName").ToString, GetDesignation(.Item("BM_FWDTO"))) + " --> Comments By  " + .Item("EmpName")
                            End If

                        Else
                            FollowUpCell.Text = .Item("BM_COMMENTS")
                        End If

                    End With

                    'Add Cell into Row
                    FollowUprow.Cells.Add(FollowUpCell)
                    'Add Rows into Table
                    tabFollowUp.Rows.Add(FollowUprow)
                Next 'Cells Forloop

            Next ' dATASET fOR lOOP
        End If
    End Sub

    Private Function GetStaffDetails(ByVal StaffId As String) As DataSet
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim strSQL As String

        strSQL = "SELECT EMP_ID, EMPNO ,(EMP_FNAME + ' ' + EMP_LNAME) As EMPNAME " & _
                    " FROM EMPLOYEE_M " & _
                    " WHERE EMP_ID=" & StaffId

        Dim dsStaffDet As DataSet
        dsStaffDet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSQL)
        Return dsStaffDet
    End Function

    Private Function GetStudentDetails(ByVal StudentId As String) As DataSet
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim strSQL As String
        strSQL = "SELECT STU_ID, STU_NO ,(STUDENT_M.STU_FIRSTNAME + ' ' +STUDENT_M.STU_LASTNAME)As STUDENTNAME," & _
                    " SCT_DESCR, GRM_DISPLAY, STM_DESCR, SHF_DESCR " & _
                    " FROM STUDENT_M " & _
                    " INNER JOIN SECTION_M ON   SECTION_M.SCT_ID =STUDENT_M.STU_SCT_ID " & _
                    " INNER JOIN STREAM_M ON STREAM_M.STM_ID =STUDENT_M.STU_STM_ID " & _
                    " INNER JOIN SHIFTS_M ON SHIFTS_M.SHF_ID=STUDENT_M.STU_SHF_ID " & _
                    " INNER JOIN GRADE_BSU_M ON GRADE_BSU_M.GRM_ID= STUDENT_M.STU_GRM_ID " & _
                    " WHERE STU_ID=" & StudentId & ""
        Dim dsStudentDet As DataSet
        dsStudentDet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, strSQL)

        Return dsStudentDet
    End Function

    Public Sub ActionTakendetails()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_query = "SELECT * from BM.BM_ACTION_MASTER WHERE BM_STU_ID=" & Hiddenstuid.Value & " AND BM_ID=" & Hiddenincidentid.Value & ""

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        If ds.Tables(0).Rows.Count >= 1 Then

            R1.SelectedValue = ds.Tables(0).Rows(0).Item("BM_PARENT_CALLED").ToString()
            R2.SelectedValue = ds.Tables(0).Rows(0).Item("BM_PARENT_INTERVIED").ToString()
            R3.SelectedValue = ds.Tables(0).Rows(0).Item("BM_NOTES_STU_PLANNER").ToString()
            R4.SelectedValue = ds.Tables(0).Rows(0).Item("BM_BREAK_DETENTION").ToString()
            R5.SelectedValue = ds.Tables(0).Rows(0).Item("BM_AFTER_SCHOOL_DETENTION").ToString()
            R6.SelectedValue = ds.Tables(0).Rows(0).Item("BM_SUSPENSION").ToString()
            R7.SelectedValue = ds.Tables(0).Rows(0).Item("BM_REF_STU_COUNSELLOR").ToString()
            T1.Text = Format(ds.Tables(0).Rows(0).Item("BM_NOTES_STU_PLANNER_DATE"), "dd-MMM-yyyy")
            If T1.Text.Trim() = "01-Jan-1900" Then T1.Text = ""
            T2.Text = Format(ds.Tables(0).Rows(0).Item("BM_BREAK_DETENTION_DATE"), "dd-MMM-yyyy")
            If T2.Text.Trim() = "01-Jan-1900" Then T2.Text = ""
            T3.Text = Format(ds.Tables(0).Rows(0).Item("BM_AFTER_SCHOOL_DETENTION_DATE"), "dd-MMM-yyyy")
            If T3.Text.Trim() = "01-Jan-1900" Then T3.Text = ""
            T4.Text = Format(ds.Tables(0).Rows(0).Item("BM_SUSPENSION_DATE"), "dd-MMM-yyyy")
            If T4.Text.Trim() = "01-Jan-1900" Then T4.Text = ""
            T5.Text = Format(ds.Tables(0).Rows(0).Item("BM_REF_STU_COUNSELLOR_DATE"), "dd-MMM-yyyy")
            If T5.Text.Trim() = "01-Jan-1900" Then T5.Text = ""
            txtCalldate.Text = Format(ds.Tables(0).Rows(0).Item("BM_PARENT_CALLED_DATE"), "dd-MMM-yyyy")
            If txtCalldate.Text.Trim() = "01-Jan-1900" Then txtCalldate.Text = ""
            TxtIntvDate.Text = Format(ds.Tables(0).Rows(0).Item("BM_PARENT_INTERVIED_DATE"), "dd-MMM-yyyy")
            If TxtIntvDate.Text.Trim() = "01-Jan-1900" Then TxtIntvDate.Text = ""

            txtparentscalledsaid.Text = ds.Tables(0).Rows(0).Item("BM_PARENT_CALLED_SAID").ToString()
            txtparentsinterviewssaid.Text = ds.Tables(0).Rows(0).Item("BM_PARENT_INTERVIED_SAID").ToString()
            HidActionID.Value = ds.Tables(0).Rows(0).Item("BM_ACTION_ID").ToString() '
            Hiddenincidentid.Value = ds.Tables(0).Rows(0).Item("BM_ID").ToString()
            GetFollowUpDetails()
        End If
    End Sub
End Class
