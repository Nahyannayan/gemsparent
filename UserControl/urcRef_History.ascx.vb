﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Partial Class UserControl_urcRef_History
    Inherits System.Web.UI.UserControl
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            bindRFM_ID()

            gridbind()

        Catch ex As Exception

        End Try


    End Sub

    Private Sub bindRFM_ID()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim PARAM(2) As SqlParameter
        PARAM(0) = New SqlParameter("@USR_NAME", Session("username"))
        Using DATAREADER As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "OPL.GETPARENT_REFERRAL_M", PARAM)
            If DATAREADER.HasRows = True Then
                While DATAREADER.Read

                    hfRFM_ID.Value = Convert.ToString(DATAREADER("RFM_ID"))
                End While
            Else
                hfRFM_ID.Value = 0
            End If

        End Using
      
    End Sub
    Public Sub gridbind(Optional ByVal p_sindex As Integer = -1)
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_Sql As String = ""
            Dim str_filter_RF_NAME As String = String.Empty
            Dim str_filter_RF_EMAIL As String = String.Empty
            Dim str_filter_RF_MOB_NO As String = String.Empty
            Dim str_filter_RF_CREATE_DT As String = String.Empty
            Dim str_filter_RF_APPR_STATUS As String = String.Empty
            Dim ds As New DataSet
            str_Sql = "SELECT * FROM( SELECT S.RFS_ID, M.RFM_ID, ISNULL(S.RFS_SALUTE,'') + ' ' + S.RFS_REF_FNAME + ' ' + ISNULL(S.RFS_REF_LNAME,'') AS RFM_NAME, S.RFS_REF_EMAIL as RFM_EMAIL," & _
 " S.RFS_REF_MOB_NO as RFM_MOB_NO,ISNULL((SELECT ISNULL(EQM_APPLFIRSTNAME,'') + ' ' + ISNULL(EQM_APPLLASTNAME,'') FROM dbo.ENQUIRY_M WHERE EQM_ENQID =D.REF_EQM_ENQID ),'') AS SNAME, " & _
"(SELECT BSU_NAME FROM BUSINESSUNIT_M WHERE BSU_ID=D.RFD_BSU_ID) AS BSU_NAME, " & _
            " CASE WHEN  D.RFD_Reward =0 THEN 'Just Referred' " & _
" WHEN D.RFD_Reward =1 THEN 'Student Applied' " & _
"  WHEN D.RFD_Reward =2  THEN 'Admission under process' " & _
" WHEN D.RFD_Reward =3  THEN 'Reward pending (Student Enrolled)' " & _
" WHEN D.RFD_Reward =4  THEN 'Rewarded' " & _
" WHEN D.RFD_Reward =5  THEN 'Reward not successful'  END  AS RFD_Reward  " & _
" FROM  REF.REFERRAL_M AS M INNER JOIN  REF.REFERRAL_S AS S ON S.RFS_RFM_ID = M.RFM_ID INNER JOIN " & _
" REF.REFERRAL_D AS D ON S.RFS_RFM_ID = D.RFD_RFM_ID AND S.RFS_ID = D.RFD_RFS_ID ) A WHERE  (RFM_ID = '" & hfRFM_ID.Value & "') "

            Dim txtSearch As New TextBox
            Dim str_search As String
            Dim str_RF_NAME As String = String.Empty
            Dim str_RF_EMAIL As String = String.Empty
            Dim str_RF_MOB_NO As String = String.Empty
            Dim str_RF_CREATE_DT As String = String.Empty
            Dim str_RF_APPR_STATUS As String = String.Empty

            If gvRef.Rows.Count > 0 Then




               
                str_filter_RF_NAME = " AND RFM_NAME LIKE '%" & txtref_name.Text.Trim & "%'"
                str_filter_RF_EMAIL = " AND RFM_EMAIL LIKE '%" & txtEmail_addr.Text.Trim & "%'"

                str_filter_RF_MOB_NO = " AND RFM_MOB_NO LIKE '%" & txtMob_no.Text.Trim & "%'"




            End If
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql & str_filter_RF_NAME & str_filter_RF_EMAIL & str_filter_RF_MOB_NO & " ORDER BY RFS_ID DESC ")
            If ds.Tables(0).Rows.Count > 0 Then
                gvRef.DataSource = ds.Tables(0)
                gvRef.DataBind()

            Else

                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                'start the count from 1 no matter gridcolumn is visible or not
                ds.Tables(0).Rows(0)(6) = True
                gvRef.DataSource = ds.Tables(0)
                Try
                    gvRef.DataBind()
                Catch ex As Exception
                End Try

                Dim columnCount As Integer = gvRef.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.

                gvRef.Rows(0).Cells.Clear()
                gvRef.Rows(0).Cells.Add(New TableCell)
                gvRef.Rows(0).Cells(0).ColumnSpan = columnCount
                gvRef.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvRef.Rows(0).Cells(0).Text = "No records available."
            End If

           
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub gvRef_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvRef.PageIndexChanging
        gvRef.PageIndex = e.NewPageIndex
        gridbind()
        Dim mpeRef As AjaxControlToolkit.ModalPopupExtender = Me.Parent.FindControl("mdlPopup")

        If Not mpeRef Is Nothing Then
            mpeRef.Show()
        End If
    End Sub
    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        gridbind()

        Dim mpeRef As AjaxControlToolkit.ModalPopupExtender = Me.Parent.FindControl("mdlPopup")

        If Not mpeRef Is Nothing Then
            mpeRef.Show()
        End If
    End Sub
End Class
