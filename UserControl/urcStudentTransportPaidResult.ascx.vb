﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO

Partial Class UserControl_urcStudentTransportPaidResult
    Inherits System.Web.UI.UserControl

    Public ReceiptNos() As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Public Sub Gridbind_PayDetails(ByVal FCO_ID As Int64, ByVal bPaymentSuccess As Boolean)
        Try
            'Dim strPath As String = Convert.ToString(readerStudent_Detail("STU_PHOTOPATH"))
            Dim connPath As String = WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ToString()
            Dim noImagePath As String = Server.MapPath("Images\no_image.gif")
            Dim con As String = ConnectionManger.GetOASISTRANSPORTConnectionString
            Dim param(4) As SqlClient.SqlParameter

            param(0) = New SqlClient.SqlParameter("@IMAGE_PATH", connPath)
            param(1) = New SqlClient.SqlParameter("@noImagePath", noImagePath)
            param(2) = New SqlClient.SqlParameter("@FCO_ID", FCO_ID)
            param(3) = New SqlClient.SqlParameter("@bPaymentSuccess", bPaymentSuccess)

            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "TRANSPORT.[GET_SIBLINGS_PAYMENT_RESULT]", param)

            repInfo.DataSource = ds.Tables(0)
            repInfo.DataBind()
        Catch ex As Exception
            repInfo.DataBind()
        End Try
    End Sub
    Dim ReptrSum As Double = 0

    Protected Sub repInfo_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles repInfo.ItemDataBound
        If e.Item.DataItem Is Nothing Then
            Dim lbl_Amount_F As New Label
            If ReceiptNos Is Nothing Then
                ReDim ReceiptNos(-1)
            End If

            lbl_Amount_F = repInfo.Controls(repInfo.Controls.Count - 1).Controls(0).FindControl("lbl_Amount_F")
            If Not lbl_Amount_F Is Nothing Then
                lbl_Amount_F.Text = ReptrSum.ToString("#,##0.00")
            End If
            'If ReceiptNos.Length <= 0 Then
            '    Me.divinfo.Visible = False
            'ElseIf ReceiptNos(0) <> "" Then
            '    Me.divinfo.Visible = True
            'End If
            Return
        Else
            Dim lblReceipt As New Label
            Dim ltAmount As New Literal
            'Dim Encr_decrData As New Encryption64
            lblReceipt = DirectCast(e.Item.FindControl("lblReceipt"), Label)
            ltAmount = DirectCast(e.Item.FindControl("ltAmount"), Literal)

            ReptrSum += IIf(Not ltAmount Is Nothing, CDbl(ltAmount.Text), 0)

            'Dim PopForm As String = "../Transport/feereceipt.aspx"
            If Not lblReceipt Is Nothing Then
                ReDim Preserve ReceiptNos(UBound(ReceiptNos) + 1)
                ReceiptNos(UBound(ReceiptNos)) = lblReceipt.Text
                '    lblReceipt.Attributes.Add("onClick", "return ShowSubWindowWithClose('" & PopForm & "?type=REC&id=" & Encr_decrData.Encrypt(lblReceipt.Text.Trim) & "', '', '80%', '80%');")
            End If
        End If
    End Sub
End Class
