﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Collections.Generic
Imports System.Collections
Partial Class ParentMaster
    Inherits System.Web.UI.MasterPage

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        CheckForSQLInjection(Me.Page)

        If Page.IsPostBack = False Then
            If Not Session("username") Is Nothing Then
                lblUser.Text = "Welcome : " & Session("USERDISPLAYNAME")
                lblTime.Text = "Last Login : " & Session("lastlogtime")
                bindStud_Curr_Photo()
                bindsiblings()
                'bind_menu()
                If Session("CLEV_MODULE") <> "DUESMS" Then
                    bind_htmlmenu()
                Else
                    desktop_menu.Visible = False
                    mobile_menu.Visible = False
                End If
            Else
                '----added by Jacob on 11/May/2020 to logout the application to same application folder
                Dim applicationFolder As String = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) & HttpRuntime.AppDomainAppVirtualPath
                Dim logoutPath As String = applicationFolder & "/Login.aspx"

                Response.Redirect(logoutPath, False)
                'Response.Redirect("~\Login.aspx", False)
            End If


            


            CheckPopConfig()
            If Session("IsNotif") = "1" Then
                If bind_notify() = "1" Then
                    Session("IsNotif") = "0"
                    ' popNotif.Attributes.Add("display", "block")
                    popNotif.Visible = True
                Else
                    Session("IsNotif") = "0"
                    popNotif.Visible = False
                End If
            Else
                'popNotif.Attributes.Add("display", "none")
                Session("IsNotif") = "0"
                popNotif.Visible = False
            End If
        End If

        ' If Session("CLEV_MODULE") <> "DUESMS" Then
        bind_badge()
        ' End If

        ''to display marquee for offline
        Dim strMArquee As String = String.Empty
        strMArquee = GetMArqueeText()
        If strMArquee.ToString <> "" Then
            lblMArquee.Visible = True
            lblMArquee.Text = strMArquee
        Else
            lblMArquee.Visible = True
        End If
        'ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "showNotify", "showNotify();", True)
        ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "HideSibs", "HideSiblings();", True)




    End Sub
    Public Sub bindsiblings()
        Try

            'Dim strPath As String = Convert.ToString(readerStudent_Detail("STU_PHOTOPATH"))
            'Dim connPath As String = WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ConnectionString
            'Dim strImagePath As String = String.Empty
            'If strPath <> "" Then
            '    strImagePath = connPath & strPath
            '    imgEmpImage.ImageUrl = strImagePath
            ' ~\Images\Common\PageBody\no_image.gif
            'End If
            Dim connPath1 As String = WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ConnectionString
            Dim noImagePath1 As String = "~/Images/Home/no_image.gif"

            Dim con As String = ConnectionManger.GetOASISConnectionString
            Dim param(5) As SqlClient.SqlParameter

            param(0) = New SqlClient.SqlParameter("@OLU_DEF_STU_ID", Session("STU_SIBLING_ID").ToString)
            param(1) = New SqlClient.SqlParameter("@STU_ID", Session("STU_ID").ToString)
            param(2) = New SqlClient.SqlParameter("@connPath", connPath1)
            param(3) = New SqlClient.SqlParameter("@noImagePath", noImagePath1)
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "[PROF].[GETCHILD_LIST_new]", param)

            If ds.Tables(0).Rows.Count >= 1 Then
                For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    If Session("IsSelected") Is Nothing Then
                        Session("STU_ID") = ds.Tables(0).Rows(i)("STU_ID")
                        Session("STU_NAME") = ds.Tables(0).Rows(i)("LISTNAME")
                        Session("IsSelected") = True
                        studdetails(ds.Tables(0).Rows(i)("STU_ID"))
                    End If
                    Exit For
                Next


                repSib.DataSource = ds.Tables(0)
                repSib.DataBind()
                'divsib.Visible = True
                Session("SibCount") = ds.Tables(0).Rows.Count
                ' lbtnSwitchSib.Visible = True

            Else
                divsib.Visible = False
                'lbtnSwitchSib.Visible = False
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Public Sub CheckPopConfig()
        Try
             
             
            Dim con As String = ConnectionManger.GetOASISConnectionString
            Dim param(2) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@STU_ID", Session("STU_ID").ToString)
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "[dbo].[Check_CONFIG_PARENT_BSU]", param)

            Dim param1(2) As SqlClient.SqlParameter
            param1(0) = New SqlClient.SqlParameter("@OLU_DEF_STU_ID", Session("STU_SIBLING_ID").ToString)
            Dim ds1 As New DataSet
            ds1 = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "[dbo].[GET_SIBLING_LIST]", param1)

            If ds.Tables(0).Rows.Count >= 1 Then
                If ds.Tables(0).Rows(0)("CPB_ENABLE_POP_EMAIL") = "1" And ds1.Tables(0).Rows.Count > 0 And Session("IsPopUpcheck") Is Nothing Then
                    'CHECK_EMAIL_UPDATES   
                    Session("IsPopUpcheck") = "1"
                    popNotif.Visible = False


                    ScriptManager.RegisterStartupScript(Me, Page.GetType, "Script", "openPOP();", True)
                    rptEMailPopup.DataSource = ds1.Tables(0)
                    rptEMailPopup.DataBind()
                ElseIf ds.Tables(0).Rows(0)("CPB_ENABLE_POP_IMG") = "1" And Session("IsPopUpcheck_1") Is Nothing Then
                    Session("IsPopUpcheck_1") = "1"
                    If bind_notify() = "1" Then
                        popNotif.Visible = True
                    Else
                        popNotif.Visible = False
                    End If
                Else
                    popNotif.Visible = False
                End If
            Else
                popNotif.Visible = False
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Private Sub bindStud_Curr_Photo()
        Try

            'Dim strPath As String = Convert.ToString(readerStudent_Detail("STU_PHOTOPATH"))
            Dim connPath As String = WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ConnectionString
            Dim noImagePath As String = "~/Images/Home/no_image.gif"



            Dim con As String = ConnectionManger.GetOASISConnectionString
            Dim param(2) As SqlClient.SqlParameter

            param(0) = New SqlClient.SqlParameter("@STU_ID", Session("STU_ID").ToString)
            param(1) = New SqlClient.SqlParameter("@connPath", connPath)
            param(2) = New SqlClient.SqlParameter("@noImagePath", noImagePath)


            Using DATAREADER As SqlDataReader = SqlHelper.ExecuteReader(con, CommandType.StoredProcedure, "[OPL].[GETSTU_CURR_PHOTO_V2]", param)
                If DATAREADER.HasRows = True Then
                    While DATAREADER.Read
                        imgStuImage.ImageUrl = Convert.ToString(DATAREADER("PHOTOPATH"))
                        imgStuImage.ToolTip = Convert.ToString(DATAREADER("SNAME"))
                        imgStuImage.AlternateText = Convert.ToString(DATAREADER("STU_NO"))
                        divstu_name.InnerText = Convert.ToString(DATAREADER("SNAME"))
                        ltrBsu.Text = Convert.ToString(DATAREADER("BSU_NAME"))
                        ' lbtnAlert_child.Text = "Alerts for " & StrConv(Convert.ToString(DATAREADER("FIRSTNAME")), VbStrConv.ProperCase)
                        Session("CURR_STU_FIRSTNAME") = StrConv(Convert.ToString(DATAREADER("FIRSTNAME")), VbStrConv.ProperCase)
                    End While

                Else
                    imgStuImage.ImageUrl = noImagePath
                End If
            End Using

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub repSib_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) _
 Handles repSib.ItemCommand

        Session("STU_ID") = e.CommandArgument
        Session("STU_NAME") = e.CommandName
        ' If Session("CLEV_MODULE") <> "DUESMS" Then
        studdetails(e.CommandArgument)
        get_url()
        getActive_menu()
        ' End If
    End Sub



    Private Sub studdetails(ByVal stu_id As String)

        Try

            Dim con As String = ConnectionManger.GetOASISConnectionString
            Dim param(4) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@STU_ID", stu_id)

            param(1) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
            param(1).Direction = ParameterDirection.ReturnValue

            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "[ONLINE].[STUDENTDETAILS_V2]", param)
            If param(1).Value = "0" Then
                If ds.Tables(0).Rows.Count > 0 Then

                    Session("STUID_S") = Convert.ToString(ds.Tables(0).Rows(0)("STU_ID"))
                    Session("STU_ID") = Convert.ToString(ds.Tables(0).Rows(0)("STU_ID"))
                    Session("STU_NAME") = Convert.ToString(ds.Tables(0).Rows(0)("StudName"))
                    Session("STU_NO") = Convert.ToString(ds.Tables(0).Rows(0)("StudNo"))
                    Session("STU_ACD_ID") = Convert.ToString(ds.Tables(0).Rows(0)("STU_ACD_ID"))
                    Session("sBsuid") = Convert.ToString(ds.Tables(0).Rows(0)("STU_BSU_ID"))
                    Session("STU_GRD_ID") = Convert.ToString(ds.Tables(0).Rows(0)("STU_GRD_ID"))
                    Session("STU_SCT_ID") = Convert.ToString(ds.Tables(0).Rows(0)("STU_SCT_ID"))
                    Session("ACD_CLM_ID") = Convert.ToString(ds.Tables(0).Rows(0)("ACD_CLM_ID"))
                    Session("STU_SIBLING_ID") = Convert.ToString(ds.Tables(0).Rows(0)("SIBLING_ID"))
                    Session("sReqRole") = Convert.ToString(ds.Tables(0).Rows(0)("StudName"))
                    Session("ACY_DESCR") = Convert.ToString(ds.Tables(0).Rows(0)("ACY_DESCR"))
                    Session("sroleid") = "1"
                    Session("STU_BSU_ID") = Convert.ToString(ds.Tables(0).Rows(0)("STU_BSU_ID"))
                    Session("SUser_Name") = Convert.ToString(ds.Tables(0).Rows(0)("StudName"))
                    Session("stu_section") = Convert.ToString(ds.Tables(0).Rows(0)("SCT_DESCR"))
                    Session("sModule") = "BB"
                    Session("BSU_NAME") = Convert.ToString(ds.Tables(0).Rows(0)("SchoolName"))
                    Session("Active") = Convert.ToString(ds.Tables(0).Rows(0)("ACTIVE"))
                    Session("STU_CURRSTATUS") = Convert.ToString(ds.Tables(0).Rows(0)("STU_CURRSTATUS"))
                    Session("CLM") = Convert.ToString(ds.Tables(0).Rows(0)("ACD_CLM_ID"))
                    Session("IsSelected") = True

                    '''''''''''''''
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Private Sub getActive_menu()


        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim PARAM(10) As SqlParameter
        Dim File_Path As String = String.Empty
        Dim Active_Tab As String = String.Empty
        Dim Active_menu As String = String.Empty
        PARAM(0) = New SqlParameter("@OPLM_GROUP_NAME", Session("Active_tab"))
        PARAM(1) = New SqlParameter("@STU_ID", Session("STU_ID"))
        PARAM(2) = New SqlParameter("@STU_GRD_ID", Session("STU_GRD_ID"))
        PARAM(3) = New SqlParameter("@STU_ACD_ID", Session("STU_ACD_ID"))
        PARAM(4) = New SqlParameter("@STU_BSU_ID", Session("STU_BSU_ID"))
        PARAM(5) = New SqlParameter("@STU_CLM_ID", Session("ACD_CLM_ID"))
        PARAM(6) = New SqlParameter("@STU_SCT_ID", Session("STU_SCT_ID"))
        PARAM(7) = New SqlParameter("@ACTIVE_MENU", Session("Active_menu"))
        PARAM(8) = New SqlParameter("@STU_ACTIVE", Session("Active"))
        Using dataread As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "OPL.GETUSR_MENUS_CURRENT_V2", PARAM)
            If dataread.HasRows = True Then
                While dataread.Read
                    Active_Tab = Convert.ToString(dataread("Active_Tab"))
                    Active_menu = Convert.ToString(dataread("Active_menu"))
                    File_Path = Convert.ToString(dataread("File_Path"))

                End While

            End If
        End Using

        Session("Active_tab") = Active_Tab
        If Active_menu = "Home" Then
            Session("Site_Path") = ""
            File_Path = "~\Home.aspx"
        Else
            Session("Site_Path") = ">" & Active_Tab & ">" & Active_menu
        End If

        Session("Active_menu") = Active_menu

        divMenupath.InnerText = "You are Now on: " + Session("Site_Path")

        Response.Redirect(File_Path, False)



    End Sub

    Sub bind_menu()
        Dim ds As New DataSet
        Dim moduleDecr As New Encryption64
        Dim connStr As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim bunit As String = Session("STU_BSU_ID")
        Dim grdid As String = Session("STU_GRD_ID")

        Dim sqlString As String = ""
        Dim PARAM(2) As SqlParameter



        PARAM(0) = New SqlParameter("@STU_BSU_ID", bunit)
        PARAM(1) = New SqlParameter("@STU_GRD_ID", grdid)

        ds = SqlHelper.ExecuteDataset(connStr, CommandType.StoredProcedure, "OPL.GET_MENUS_ONLINE", PARAM)
        'Dim da As SqlDataAdapter = New SqlDataAdapter(sqlString, conn)
        'da.Fill(ds)
        'da.Dispose()
        'SqlConnection.ClearPool(conn)

        ds.DataSetName = "MENURIGHTS"
        ds.Tables(0).TableName = "MENURIGHT"
        Dim relation As DataRelation = New DataRelation("ParentChild", ds.Tables("MENURIGHT").Columns("MNU_CODE"), ds.Tables("MENURIGHT").Columns("MNU_PARENTID"), True)
        relation.Nested = True
        ds.Relations.Add(relation)
        If ds.Tables(0).Rows.Count > 0 Then

            xmlDataSource.Data = ds.GetXml()
            xmlDataSource.DataBind()
        Else

            ' Response.Redirect("~/Modulelogin.aspx")
        End If
    End Sub

    Protected Sub Menu1_MenuItemClick(ByVal sender As Object, ByVal e As Telerik.Web.UI.RadMenuEventArgs)
        Try
            Dim MainMnu_code As String = e.Item.Value
            Dim strRedirect As String = "#"
            Dim menu_rights As String = ""

            Dim url As String = ""

            If Session("username") Is Nothing Then
                Response.Redirect("~/login.aspx")
            Else
                Using readerUserDetail As SqlDataReader = GetRedirectPage(MainMnu_code)

                    While readerUserDetail.Read()

                        strRedirect = Convert.ToString(readerUserDetail(1))
                        Session("Active_tab") = Convert.ToString(readerUserDetail(2))
                        Session("Active_menu") = Convert.ToString(readerUserDetail(0))
                    End While


                End Using

              

                    Dim datamode As String = "none"
                    Dim encrData As New Encryption64

                    url = strRedirect
                    If Trim(strRedirect) = "#" Then
                    Else
                        Response.Redirect(url, False)
                    End If
                End If

        Catch ex As Exception
            ' lblMainError.Text = "File can not be located"
        End Try
    End Sub

    Public Shared Function GetRedirectPage(ByVal MainMmnu_code As String) As SqlDataReader


        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim PARAM(2) As SqlParameter

        PARAM(0) = New SqlParameter("@mnu", MainMmnu_code)

        Dim dataread As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "OPL.GETMENU_CODE", PARAM)

        Return dataread
    End Function
    Public Sub bind_badge()
        Dim con As String = ConnectionManger.GetOASISConnectionString
        Dim param(4) As SqlClient.SqlParameter
        'data-badge
        param(0) = New SqlClient.SqlParameter("@STU_ID", Session("STU_ID").ToString)
        param(1) = New SqlClient.SqlParameter("@ACD_ID", Session("STU_ACD_ID"))
        param(2) = New SqlClient.SqlParameter("@BSU_ID", Session("STU_BSU_ID"))
        param(3) = New SqlClient.SqlParameter("@GRADE", Session("STU_GRD_ID"))
        param(4) = New SqlClient.SqlParameter("@SCT_ID", Session("STU_SCT_ID"))
        Dim count As Integer = SqlHelper.ExecuteScalar(con, "[OPL].[GET_NOTIFICATION_COUNT]", param)

       

        If (count > 0) Then
            If Session("CLEV_MODULE") = "DUESMS" Then
                navNotification.Attributes.Add("display", "none")
                navNotification_M.Attributes.Add("display", "none")

                navNotification.Attributes.Remove("data-badge")
                navNotification_M.Attributes.Remove("data-badge")
            Else
                navNotification.Attributes.Add("data-badge", count.ToString)
                navNotification_M.Attributes.Add("data-badge", count.ToString)
            End If
        Else
            navNotification.Attributes.Remove("data-badge")
            navNotification_M.Attributes.Remove("data-badge")
        End If


        If Session("CLEV_MODULE") = "DUESMS" Then
            navNotification.HRef = "#"
            navNotification_M.HRef = "#"
            navNotification.Attributes.Add("display", "none")
            navNotification_M.Attributes.Add("display", "none")

            navNotification.Attributes.Remove("data-badge")
            navNotification_M.Attributes.Remove("data-badge")
        End If
    End Sub
    Function bind_notify() As Integer
        Dim con As String = ConnectionManger.GetOASISConnectionString
        Dim param(1) As SqlClient.SqlParameter
        Dim i As Integer = 0
        param(0) = New SqlClient.SqlParameter("@BSU_ID", Session("STU_BSU_ID"))
        param(1) = New SqlClient.SqlParameter("@OLU_NAME", Session("username"))

        Dim ds As DataSet = SqlHelper.ExecuteDataset(con, "opl.get_Notify_message", param)
        If ds.Tables(0).Rows.Count > 0 Then
            imgNotify.Src = ds.Tables(0).Rows(0)("image_url")
            spHeader.InnerHtml = ds.Tables(0).Rows(0)("heading")
            spDescr.InnerHtml = ds.Tables(0).Rows(0)("descr")
            i = ds.Tables(0).Rows(0)("IsVisible")
        End If
        Return i
    End Function

    Sub bind_htmlmenu()
        Dim ds As New DataSet
        Dim moduleDecr As New Encryption64
        Dim connStr As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim bunit As String = Session("STU_BSU_ID")
        Dim stuid As String = Session("STU_ID")

        Dim sqlString As String = ""
        Dim PARAM(3) As SqlParameter

        PARAM(0) = New SqlParameter("@STU_BSU_ID", bunit)
        'PARAM(1) = New SqlParameter("@STU_GRD_ID", grdid)
        PARAM(1) = New SqlParameter("@PORTAL_FOLDER", HttpContext.Current.Request.ApplicationPath) 'added by Jacob on 19Aug/2019
        PARAM(2) = New SqlParameter("@STU_ID", stuid)
        ds = SqlHelper.ExecuteDataset(connStr, CommandType.StoredProcedure, "[OPL].[LOAD_MAIN_MENUS]", PARAM) 'changed SP name by Jacob on 19/Aug/2019 to resolved the menu path issue when hosted under subfolder.
        'ds = SqlHelper.ExecuteDataset(connStr, CommandType.StoredProcedure, "OPL.LoadParentMenu", PARAM)

        If ds.Tables(0).Rows.Count > 0 Then

            desktop_menu.InnerHtml = ds.Tables(0).Rows(0)(0)
            mobile_menu.InnerHtml = ds.Tables(0).Rows(0)(0)
        Else

            ' Response.Redirect("~/Modulelogin.aspx")
        End If
    End Sub
    Protected Sub repSib_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles repSib.ItemDataBound
        Dim spAct As New HtmlGenericControl
        Dim hfSTU_ID As New HiddenField
        Dim lnkSib As New LinkButton
        If e.Item.DataItem Is Nothing Then
            Return



        Else
            spAct = DirectCast(e.Item.FindControl("spActive"), HtmlGenericControl)
            hfSTU_ID = DirectCast(e.Item.FindControl("hdnStu"), HiddenField)
            lnkSib = DirectCast(e.Item.FindControl("lnkSib"), LinkButton)

            If hfSTU_ID.Value = Session("STU_ID") Then
                spAct.Visible = True
                lnkSib.Attributes.Add("class", "img-thumbnail img-thumbnail-active img-circle profile-thumb-top")
            Else
                spAct.Visible = False
                lnkSib.Attributes.Add("class", "img-thumbnail  img-circle profile-thumb-top")
            End If
        End If
    End Sub
    Private Function CheckForSQLInjection(ByVal myParentObj As Object) As Boolean
        Try
            Dim mObj As Object
            For Each mObj In myParentObj.Controls
                Try
                    If mObj.HasControls Then
                        CheckForSQLInjection(mObj)
                    End If
                Catch ex As Exception

                End Try
                If TypeOf (mObj) Is TextBox Then
                    If TypeOf (mObj.Parent) Is GridView Or TypeOf (mObj.Parent) Is DataControlFieldHeaderCell Then
                        mObj.Text = mainclass.cleanString(mObj.Text)
                    End If
                End If
            Next
        Catch ex As Exception

        End Try
    End Function
    Sub get_url()
        Dim url As String = Request.Url.AbsoluteUri
        url = url.Replace("http://oasis.gemseducation.com", "")

        ' UtilityObj.Errorlog(url, "Rajesh Parent Portal")

        Using readerUserDetail As SqlDataReader = geturlcode(url)

            While readerUserDetail.Read()
                lbHo(Convert.ToString(readerUserDetail(0)))
            End While
        End Using
    End Sub
    Public Shared Function geturlcode(ByVal url As String) As SqlDataReader


        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim PARAM(2) As SqlParameter

        PARAM(0) = New SqlParameter("@url", url)

        Dim dataread As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "OPL.get_parent_url", PARAM)

        Return dataread
    End Function
    Protected Sub lbHo(ByVal MainMnu_code As String)
        Try
            'Dim senderBox As HtmlAnchor = TryCast(sender, HtmlAnchor)
            'Dim MainMnu_code As String = senderBox.Name
            Dim strRedirect As String = "#"
            Dim menu_rights As String = ""

            Dim url As String = ""

            If Session("username") Is Nothing Then
                Response.Redirect("~/login.aspx")
            Else
                Using readerUserDetail As SqlDataReader = GetRedirectPage(MainMnu_code)

                    While readerUserDetail.Read()

                        strRedirect = Convert.ToString(readerUserDetail(1))
                        Session("Active_tab") = Convert.ToString(readerUserDetail(2))
                        Session("Active_menu") = Convert.ToString(readerUserDetail(0))
                    End While


                End Using
                Dim datamode As String = "none"
                Dim encrData As New Encryption64

                url = strRedirect
                If Trim(strRedirect) = "#" Then
                Else
                    Response.Redirect(url, False)
                End If
            End If
        Catch ex As Exception
            ' lblMainError.Text = "File can not be located"
        End Try
    End Sub

    Protected Sub lbSignOutTop_Click1(sender As Object, e As EventArgs) Handles lbSignOutTop.Click
        Session("username") = Nothing
        Session("Active_tab") = "Home"
        Session("Site_Path") = ""

        Session("hideDiv") = Nothing


        Session.RemoveAll()


        '----added by Jacob on 11/May/2020 to logout the application to same application folder
        Dim applicationFolder As String = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) & HttpRuntime.AppDomainAppVirtualPath
        Dim logoutPath As String = applicationFolder & "/Login.aspx"

        Response.Redirect(logoutPath, False)
        'Response.Redirect("/Login.aspx", False)
    End Sub




    Protected Sub LinkButton1_Click(sender As Object, e As EventArgs) Handles LinkButton1.Click
        Session("username") = Nothing
        Session("Active_tab") = "Home"
        Session("Site_Path") = ""

        Session("hideDiv") = Nothing


        Session.RemoveAll()


        '----added by Jacob on 11/May/2020 to logout the application to same application folder
        Dim applicationFolder As String = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) & HttpRuntime.AppDomainAppVirtualPath
        Dim logoutPath As String = applicationFolder & "/Login.aspx"

        Response.Redirect(logoutPath, False)
        'Response.Redirect("/Login.aspx", False)
    End Sub

    Protected Sub btnSaveEmail_Click1(sender As Object, e As EventArgs) Handles btnSaveEmail.Click

        For Each item As RepeaterItem In rptEMailPopup.Items

            Dim hdnSTU_ID As HiddenField = item.FindControl("hdnStuID")
            Dim txtStudemail As TextBox = item.FindControl("txtStudemail")

            Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim PARAM(4) As SqlParameter

            PARAM(0) = New SqlParameter("@STU_ID", hdnSTU_ID.Value)
            PARAM(1) = New SqlParameter("@EMAIL", txtStudemail.Text)
            PARAM(2) = New SqlParameter("@returnValue", 0)
            PARAM(2).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "dbo.UPDATE_STUDENT_DETAILS", PARAM)
        Next

        CheckPopConfig()

    End Sub
    Protected Sub btnClosePop_Click1(sender As Object, e As EventArgs) Handles btnClosePop.ServerClick
        Try


            Session("IsPopUpcheck") = "1"

            Dim con As String = ConnectionManger.GetOASISConnectionString
            Dim param(2) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@STU_ID", Session("STU_ID").ToString)
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "[dbo].[Check_CONFIG_PARENT_BSU]", param)


            If ds.Tables(0).Rows.Count >= 1 Then
                If ds.Tables(0).Rows(0)("CPB_ENABLE_POP_IMG") = "1" Then
                    If bind_notify() = "1" Then
                        popNotif.Visible = True
                    Else
                        popNotif.Visible = False
                    End If
                Else
                    popNotif.Visible = False
                End If
            Else
                popNotif.Visible = False
            End If


        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Function GetMArqueeText() As String
        Dim marqueeText As String = String.Empty
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = ""
        Dim ds As DataSet
        Dim param(3) As SqlClient.SqlParameter

        param(0) = New SqlClient.SqlParameter("@PORTAL", "GEMSPARENT")
        param(1) = New SqlClient.SqlParameter("@BSU_ID", Session("STU_BSU_ID"))

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "dbo.GetMarqueeForOfflineMessage", param)

        If ds.Tables(0).Rows.Count > 0 Then
            Dim Dt As DataTable = ds.Tables(0)

            If Dt.Rows.Count > 0 Then
                marqueeText = Dt.Rows("0")("MARQUEETEXT").ToString()
            End If

        End If
        Return marqueeText
    End Function

End Class

