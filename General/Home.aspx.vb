﻿
Partial Class General_Home
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

        If Session("TranReqType") <> "tran" Then
            Session("TranReqType") = Mainclass.cleanString(Request.QueryString("TranReqType"))
            Session("providerheader") = Mainclass.cleanString(Request.QueryString("header"))
        End If

        If Not Session("username") Is Nothing Then
            Response.Redirect("\Home.aspx")
        Else
            Response.Redirect("~\Login.aspx")
        End If


    End Sub
End Class
