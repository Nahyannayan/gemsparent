Imports Microsoft.ApplicationBlocks.Data
Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Text
Imports System.Net.Mail
Imports System.Security.Cryptography

Partial Class ParentLogin_Logindirect_CLEV
    Inherits System.Web.UI.Page
    Dim SessionFlag As Integer
    Private key() As Byte = {}
    Private IV() As Byte = {&H12, &H34, &H56, &H78, &H90, &HAB, &HCD, &HEF}


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        LoginDirect()
    End Sub
    Public Function Decrypt_CLEV(ByVal stringToDecrypt As String, _
              Optional ByVal sEncryptionKey As String = "!#$a54?CLEV") As String
        Dim inputByteArray(stringToDecrypt.Length) As Byte
        Try
            key = System.Text.Encoding.UTF8.GetBytes(Left(sEncryptionKey, 8))
            Dim des As New DESCryptoServiceProvider()
            inputByteArray = Convert.FromBase64String(stringToDecrypt)
            Dim ms As New MemoryStream()
            Dim cs As New CryptoStream(ms, des.CreateDecryptor(key, IV), _
                CryptoStreamMode.Write)
            cs.Write(inputByteArray, 0, inputByteArray.Length)
            cs.FlushFinalBlock()
            Dim encoding As System.Text.Encoding = System.Text.Encoding.UTF8
            Return encoding.GetString(ms.ToArray())
        Catch e As Exception
            Return e.Message
        End Try
    End Function
    Public Function Encrypt_CLEV(ByVal stringToEncrypt As String, _
              Optional ByVal SEncryptionKey As String = "!#$a54?CLEV") As String
        Try
            key = System.Text.Encoding.UTF8.GetBytes(Left(SEncryptionKey, 8))
            Dim des As New DESCryptoServiceProvider()
            Dim inputByteArray() As Byte = Encoding.UTF8.GetBytes( _
                stringToEncrypt)
            Dim ms As New MemoryStream()
            Dim cs As New CryptoStream(ms, des.CreateEncryptor(key, IV), _
                CryptoStreamMode.Write)
            cs.Write(inputByteArray, 0, inputByteArray.Length)
            cs.FlushFinalBlock()
            Return Convert.ToBase64String(ms.ToArray())
        Catch e As Exception
            Return e.Message
        End Try
    End Function
    Public Function FETCH_STU_ID(ByVal STU_NO As String) As String
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim pParms(3) As SqlClient.SqlParameter
        Dim lintStu_ID As String
        pParms(0) = New SqlClient.SqlParameter("@STU_NO", STU_NO)


        Using DefReader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "FETCH_STU_ID", pParms)
            While DefReader.Read
                lintStu_ID = DefReader("STU_ID")
            End While
        End Using
        Return lintStu_ID
    End Function

    Protected Sub LoginDirect()
        Try
            Session("CLEV_USERNAME") = Decrypt_CLEV(Request.QueryString("U").Replace(" ", "+"))  '"rajeshku.r$$$gems@2018" 
            'Session("CLEV_BSU") = Decrypt_CLEV(Request.QueryString("B").Replace(" ", "+")) 'Decrypt_CLEV("H9n0KjR9Xbo=") 
            Session("CLEV_MODULE") = Decrypt_CLEV(Request.QueryString("M").Replace(" ", "+")) 'Decrypt_CLEV("eGnga/EwHuI=")"FEE" '
            Session("CLEV_STUDENT_SEL") = FETCH_STU_ID(Request.QueryString("S"))


            'Dim IDs As String() = Session("CLEV_USERNAME").Split("$$$")
            'Session("GLGUser") = IDs(0)
            'Session("GLGPwd") = IDs(3)

            Dim IDs As String() = Split(Session("CLEV_USERNAME"), "$$$", , CompareMethod.Text) ' Session("CLEV_USERNAME").Split("$$$")
            Session("GLGUser") = IDs(0)
            Session("GLGPwd") = IDs(1)

            Dim Email_send As New Emailsending
            Dim passwordEncr As New Encryption64

            Dim username As String = Session("GLGUser") 'passwordEncr.Decrypt(Request.QueryString("GLGUser").Replace(" ", "+"))
            'Changed encryption with hash : Hrushikesh 
            '' Dim password As String = passwordEncr.EncryptWithHash(Session("GLGPwd")) 'Request.QueryString("GLGPwd").Replace(" ", "+")
            Dim password As String = passwordEncr.Encrypt(Session("GLGPwd")) 'Request.QueryString("GLGPwd").Replace(" ", "+")
            Dim con As String = ConnectionManger.GetOASISConnectionString
            Dim param(4) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@username", username)
            param(1) = New SqlClient.SqlParameter("@pass", password)
            param(2) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
            param(2).Direction = ParameterDirection.ReturnValue
            Dim ds As New DataSet
            ' ds = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "[ONLINE].[OASISGETONLINE_USERS_HASH]", param)
            ds = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "[ONLINE].[OASISGETONLINE_USERS]", param)
            If param(2).Value = "0" Then
                If ds.Tables(0).Rows.Count > 0 Then
                    StoreDatasForSession(ds)
                    'If Convert.ToInt64(Session("CLEV_STUDENT_SEL")) > 0 Then
                    '    GET_studdetails(Session("CLEV_STUDENT_SEL"))
                    'End If
                    If Session("bPasswdChanged") = "False" Then
                        Response.Redirect("~\UpdateInfo\ForcePasswordChange.aspx", False)
                    ElseIf Session("bUpdateContactDetails") = "False" Then
                        Dim isNewContact As Boolean = Is_New_Contact_Details(Session("sBsuid").ToString)
                        Session("isNewContact") = isNewContact

                        If isNewContact Then
                            Session("ForceUpdate_stud") = "~\StudProf\StudentMainDetails.aspx"
                            Response.Redirect("~\StudProf\StudentMainDetails.aspx", False)
                        Else
                            Session("ForceUpdate_stud") = "~\UpdateInfo\ForceUpdatecontactdetails.aspx"
                            Response.Redirect(Session("ForceUpdate_stud"), False)
                        End If
                    Else
                        RedirectToPage()
                        'Response.Redirect("~\general\Home.aspx", False)
                    End If
                    HttpContext.Current.ApplicationInstance.CompleteRequest()
                Else
                    lblError.Text = UtilityObj.getErrorMessage("560")
                End If
            Else
                Response.Redirect("~\General\Home.aspx")
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "Login PG")
            lblError.Text = UtilityObj.getErrorMessage("1000")
        End Try
    End Sub
    Private Function Is_New_Contact_Details(ByVal bsu_id As String) As Boolean
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim isNew As Boolean = False
        Dim param(2) As SqlParameter
        param(0) = New SqlParameter("@bsuId", bsu_id)
        param(1) = New SqlParameter("@OLU_ID", Session("OLU_ID"))
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "Get_Is_New_Contact_Details_By_bsu", param)
        If ds.Tables(0).Rows.Count > 0 Then
            Dim dt As New DataTable
            dt = ds.Tables(0)
            isNew = Convert.ToBoolean(dt.Rows(0)("bsu_contact_details_new").ToString)
        End If
        Return isNew
    End Function
    Private Sub StoreDatasForSession(ByVal ds As DataSet)
        For Each dr As DataRow In ds.Tables(0).Rows
            Session("USERDISPLAYNAME") = dr("OLU_DISPLAYNAME")
            Session("OLU_ID") = dr("OLU_ID")
            Session("username") = dr("OLU_NAME")
            Session("OLU_bsuid") = dr("OLU_BSU_ID")
            Session("BSU_NAME") = dr("BSU_NAME")
            Session("lastlogtime") = dr("lastlogtime")
            Session("logincount") = dr("OLU_LoginCount")
            Session("Login_STUID") = Convert.ToString(dr("STU_ID"))
            Session("STU_ID") = Convert.ToString(dr("STU_ID"))
            If Session("CLEV_STUDENT_SEL") <> "" Then
                Session("STU_ID") = Session("CLEV_STUDENT_SEL")
            End If
            Session("STU_NAME") = Convert.ToString(dr("STU_NAME"))
            Session("STU_NO") = Convert.ToString(dr("STU_NO"))
            Session("STU_ACD_ID") = Convert.ToString(dr("STU_ACD_ID"))
            Session("sBsuid") = Convert.ToString(dr("STU_BSU_ID"))
            Session("STU_GRD_ID") = Convert.ToString(dr("STU_GRD_ID"))
            Session("STU_SCT_ID") = Convert.ToString(dr("STU_SCT_ID"))
            Session("ACD_CLM_ID") = Convert.ToString(dr("ACD_CLM_ID"))
            Session("STU_SIBLING_ID") = Convert.ToString(dr("STU_SIBLING_ID"))
            Session("BSU_CURRENCY") = Convert.ToString(dr("BSU_CURRENCY"))
            Session("CUR_DENOMINATION") = Convert.ToString(dr("CUR_DENOMINATION"))
            Session("sReqRole") = Convert.ToString(dr("STU_NAME"))
            Session("ACY_DESCR") = Convert.ToString(dr("ACY_DESCR"))
            Session("sroleid") = "1"
            Session("STU_BSU_ID") = Convert.ToString(dr("STU_BSU_ID"))
            Session("SUser_Name") = Convert.ToString(dr("STU_NAME"))
            Session("sModule") = "FF"
            Session("bPasswdChanged") = Convert.ToString(dr("OLU_bPasswdChanged"))
            Session("bUpdateContactDetails") = Convert.ToString(dr("OLU_bUpdateContactDetails"))
            Session("Active") = Convert.ToString(dr("ACTIVE"))
            Session("STU_STM_ID") = Convert.ToString(dr("STU_STM_ID"))
            Session("IsSelected") = True
            Session("IsNotif") = "1"
        Next
    End Sub

    Private Sub RedirectToPage()
        Dim meta As New HtmlMeta()
        meta.HttpEquiv = "Refresh"
        meta.Content = "1;url=..\general\Home.aspx"
        Me.Page.Controls.Add(meta)
        'Response.Redirect("~\general\Home.aspx", False)
    End Sub
    Private Sub GET_studdetails(ByVal stu_id As String)

        Try

            Dim con As String = ConnectionManger.GetOASISConnectionString
            Dim param(4) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@STU_ID", stu_id)

            param(1) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
            param(1).Direction = ParameterDirection.ReturnValue

            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "[ONLINE].[STUDENTDETAILS]", param)
            If param(1).Value = "0" Then
                If ds.Tables(0).Rows.Count > 0 Then

                    Session("STUID_S") = Convert.ToString(ds.Tables(0).Rows(0)("STU_ID"))
                    Session("STU_ID") = Convert.ToString(ds.Tables(0).Rows(0)("STU_ID"))
                    Session("STU_NAME") = Convert.ToString(ds.Tables(0).Rows(0)("StudName"))
                    Session("STU_NO") = Convert.ToString(ds.Tables(0).Rows(0)("StudNo"))
                    Session("STU_ACD_ID") = Convert.ToString(ds.Tables(0).Rows(0)("STU_ACD_ID"))
                    Session("sBsuid") = Convert.ToString(ds.Tables(0).Rows(0)("STU_BSU_ID"))
                    Session("STU_GRD_ID") = Convert.ToString(ds.Tables(0).Rows(0)("STU_GRD_ID"))
                    Session("STU_SCT_ID") = Convert.ToString(ds.Tables(0).Rows(0)("STU_SCT_ID"))
                    Session("ACD_CLM_ID") = Convert.ToString(ds.Tables(0).Rows(0)("ACD_CLM_ID"))
                    Session("STU_SIBLING_ID") = Convert.ToString(ds.Tables(0).Rows(0)("SIBLING_ID"))
                    Session("sReqRole") = Convert.ToString(ds.Tables(0).Rows(0)("StudName"))
                    Session("ACY_DESCR") = Convert.ToString(ds.Tables(0).Rows(0)("ACY_DESCR"))
                    Session("sroleid") = "1"
                    Session("STU_BSU_ID") = Convert.ToString(ds.Tables(0).Rows(0)("STU_BSU_ID"))
                    Session("SUser_Name") = Convert.ToString(ds.Tables(0).Rows(0)("StudName"))
                    Session("stu_section") = Convert.ToString(ds.Tables(0).Rows(0)("SCT_DESCR"))
                    Session("sModule") = "BB"
                    Session("BSU_NAME") = Convert.ToString(ds.Tables(0).Rows(0)("SchoolName"))
                    Session("Active") = Convert.ToString(ds.Tables(0).Rows(0)("ACTIVE"))
                    Session("STU_CURRSTATUS") = Convert.ToString(ds.Tables(0).Rows(0)("STU_CURRSTATUS"))
                    Session("CLM") = Convert.ToString(ds.Tables(0).Rows(0)("ACD_CLM_ID"))
                    Session("IsSelected") = True
                    '''''''''''''''
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
End Class
