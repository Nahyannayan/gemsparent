Imports Microsoft.ApplicationBlocks.Data
Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Text
Imports System.Net.Mail

Partial Class ParentLogin_Login
    Inherits System.Web.UI.Page
    Dim SessionFlag As Integer


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Response.Redirect("~\Login.aspx", False)
    End Sub

    Protected Sub LoginDirect()
        Try
            Dim Email_send As New Emailsending
            Dim passwordEncr As New Encryption64

            Dim username As String = passwordEncr.Decrypt(Request.QueryString("GLGUser").Replace(" ", "+"))
            Dim password As String = Request.QueryString("GLGPwd").Replace(" ", "+")
            Dim con As String = ConnectionManger.GetOASISConnectionString
            Dim param(4) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@username", username)
            param(1) = New SqlClient.SqlParameter("@pass", password)
            param(2) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
            param(2).Direction = ParameterDirection.ReturnValue
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "[ONLINE].[OASISGETONLINE_USERS]", param)
            If param(2).Value = "0" Then
                If ds.Tables(0).Rows.Count > 0 Then
                    StoreDatasForSession(ds)
                    If Session("bPasswdChanged") = "False" Then
                        Response.Redirect("~\UpdateInfo\ForcePasswordChange.aspx", False)
                    ElseIf Session("bUpdateContactDetails") = "False" Then
                        Response.Redirect(Session("ForceUpdate_stud"), False)
                    Else
                        'If Session("Active") = "F" Then
                        '    Response.Redirect("Home_TC.aspx", False)
                        'Else


                        Response.Redirect("~\Login.aspx", False)
                        ' End If
                    End If
                    HttpContext.Current.ApplicationInstance.CompleteRequest()
                Else
                    lblError.Text = UtilityObj.getErrorMessage("560")
                End If
            Else
                Response.Redirect("~\Login.aspx")



            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "Login PG")
            lblError.Text = UtilityObj.getErrorMessage("1000")
        End Try
    End Sub

    Private Sub StoreDatasForSession(ByVal ds As DataSet)
        For Each dr As DataRow In ds.Tables(0).Rows
            Session("USERDISPLAYNAME") = dr("OLU_DISPLAYNAME")
            Session("OLU_ID") = dr("OLU_ID")
            Session("username") = dr("OLU_NAME")
            Session("OLU_bsuid") = dr("OLU_BSU_ID")
            Session("BSU_NAME") = dr("BSU_NAME")
            Session("lastlogtime") = dr("lastlogtime")
            Session("logincount") = dr("OLU_LoginCount")
            Session("Login_STUID") = Convert.ToString(dr("STU_ID"))
            Session("STU_ID") = Convert.ToString(dr("STU_ID"))
            Session("STU_NAME") = Convert.ToString(dr("STU_NAME"))
            Session("STU_NO") = Convert.ToString(dr("STU_NO"))
            Session("STU_ACD_ID") = Convert.ToString(dr("STU_ACD_ID"))
            Session("sBsuid") = Convert.ToString(dr("STU_BSU_ID"))
            Session("STU_GRD_ID") = Convert.ToString(dr("STU_GRD_ID"))
            Session("STU_SCT_ID") = Convert.ToString(dr("STU_SCT_ID"))
            Session("ACD_CLM_ID") = Convert.ToString(dr("ACD_CLM_ID"))
            Session("STU_SIBLING_ID") = Convert.ToString(dr("STU_SIBLING_ID"))
            Session("BSU_CURRENCY") = Convert.ToString(dr("BSU_CURRENCY"))
            Session("CUR_DENOMINATION") = Convert.ToString(dr("CUR_DENOMINATION"))
            Session("sReqRole") = Convert.ToString(dr("STU_NAME"))
            Session("ACY_DESCR") = Convert.ToString(dr("ACY_DESCR"))
            Session("sroleid") = "1"
            Session("STU_BSU_ID") = Convert.ToString(dr("STU_BSU_ID"))
            Session("SUser_Name") = Convert.ToString(dr("STU_NAME"))
            Session("sModule") = "FF"
            Session("bPasswdChanged") = Convert.ToString(dr("OLU_bPasswdChanged"))
            Session("bUpdateContactDetails") = Convert.ToString(dr("OLU_bUpdateContactDetails"))
            Session("Active") = Convert.ToString(dr("ACTIVE"))
            Session("STU_STM_ID") = Convert.ToString(dr("STU_STM_ID"))
            Session("STU_FEE_ID") = Convert.ToString(dr("STU_FEE_ID"))
        Next
    End Sub


End Class
