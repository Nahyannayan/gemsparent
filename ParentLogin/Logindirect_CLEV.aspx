﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Logindirect_CLEV.aspx.vb" Inherits="ParentLogin_Logindirect_CLEV" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>::GEMS Education | Sign In ::</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link media="screen" href="../ParentLogin/LoginStyle/style.css" type="text/css" rel="stylesheet" />
    <meta content="GEMS Education" name="description" />
    <meta content="GEMS Education" name="keywords" />
    <script src="../ParentLogin/LoginStyle/rotator.js" type="text/javascript"></script>
    <style type="text/css">
        .loader {
            position: fixed;
            left: 0px;
            top: 0px;
            width: 100%;
            height: 100%;
            z-index: 9999;
            background: url('../Images/Misc/loading1.gif') 50% 50% no-repeat rgb(249,249,249);
        }

        .center {
            position: fixed;
            left: 40%;
            top: 40%;
            width: 20%;
            height: 20%;
            z-index: 9999;
            background-color: none;
            color: navy;
            text-align: center;
            align-items: center;
            vertical-align: middle;
            font-family: Verdana, Geneva, Tahoma, sans-serif;
            font-weight: bold;
        }
    </style>
    <script language="javascript" type="text/javascript">
        function TermsOfUse(url) {
            var width = 600;
            var height = 400;
            var left = parseInt((screen.availWidth / 2) - (width / 2));
            var top = parseInt((screen.availHeight / 2) - (height / 2));
            var windowFeatures = "width=" + width + ",height=" + height + ",status,resizable,left=" + left + ",top=" + top + "screenX=" + left + ",screenY=" + top;
            myWindow = window.open(url, "subWind", windowFeatures);
            return false;
        }
        function TermsAndCondn() {
            if (document.getElementById('ChkTerms').checked)
                return true;
            else {
                alert('Please agree to the Terms and Conditions');
                return false;
            }
        }
        self.moveTo(0, 0); self.resizeTo(screen.availWidth, screen.availHeight);
        function CloseWindow() {
            window.open('', '_self', '');
            window.close();
        }
        function show() {
            if (document.getElementById('txtUsername').value != '' && document.getElementById('txtPassword').value != '')
                setTimeout('showGif()', 500);
        }
        function showGif() {
            //            setTimeout('showGif()',500);            
            //            document.getElementById('yourImage').style.display='block';           
        }
        function SetSize() {
            if (screen.width == 1024) {
                document.getElementById('SignIn').style.width = '1002px';
            }
            else if (screen.width == 1280) {
                document.getElementById('SignIn').style.width = '1259px';
            }
            if (screen.height == 768) {
                document.getElementById('SignIn').style.height = '560px';
            }
            else if (screen.height == 800) {
                document.getElementById('SignIn').style.height = '580px';
            }
        }
    </script>
</head>
<body>
    <div class="loader">
        <div class="center">Redirecting to PHOENIX...</div>

    </div>
    <form runat="server" id="SignIn">
        <div>
            <table align="center" width="90%" id="MainTable" cellpadding="0" border="0" cellspacing="0">
                <tr>
                    <td align="left" class="HeaderBG">
                        <img src="Images/loginImage/head.jpg" align="left" /></td>
                </tr>
                <tr>
                    <td class="ParentLogin">PHOENIX Parent Login
                            <asp:Button ID="btnPageLoad" runat="server" Text="Page Load"
                                
                                Style="display: none" UseSubmitBehavior="false" />
                    </td>
                </tr>
                <tr>
                    <td class="BodyBG">
                        <table cellpadding="0" cellspacing="0" border="0" align="center" height="400">
                            <tr align="left" valign="top">
                                <td width="70%">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td width="15%" id="coreValueImg" valign="top">
                                                <img src="" /></td>
                                            <td id="coreValue" valign="top">
                                                <p class="corevalue"></p>
                                                <p class="quote"></p>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td width="22%" align="center" style="padding-top: 10px;">
                                    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td width="16">
                                                <img src="LoginStyle/top_lef.gif" width="16" height="16"></td>
                                            <td height="16" background="LoginStyle/top_mid.gif">
                                                <img src="LoginStyle/top_mid.gif" width="16" height="16"></td>
                                            <td width="24">
                                                <img src="LoginStyle/top_rig.gif" width="24" height="16"></td>
                                        </tr>
                                        <tr>
                                            <td width="16" background="LoginStyle/cen_lef.gif">
                                                <img src="LoginStyle/cen_lef.gif" width="16" height="11"></td>
                                            <td bgcolor="#F7F8FB">
                                                <table width="250px" height="100%" border="0" cellpadding="2" cellspacing="2">
                                                    <tr>
                                                        <td colspan="2" align="right" valign="top" class="CaptionHead">Sign In &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" class="CaptionText">User ID
                                                        </td>
                                                        <td align="left">
                                                            <asp:TextBox ID="txtusername" runat="server" Width="150px"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" class="CaptionText">
                                                            <label for="Password">Password</label>
                                                        </td>
                                                        <td align="left">
                                                            <asp:TextBox ID="txtpass" TextMode="password" runat="server" Width="150px"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" colspan="2">&nbsp;<input id="ChkTerms" type="checkbox" />
                                                            <asp:LinkButton ID="lnkTermsCondns" runat="server">I agree to the Terms & Conditions</asp:LinkButton></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2" align="right">
                                                            <asp:Button ID="btnsignIn" runat="server" Text="Sign In" ValidationGroup="s" OnClientClick="return TermsAndCondn();" Height="24px" Width="70px" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td background="LoginStyle/cen_rig.gif">
                                                <img src="LoginStyle/cen_rig.gif" width="24" height="11"></td>
                                        </tr>
                                        <tr>
                                            <td width="16" height="16">
                                                <img src="LoginStyle/bot_lef.gif" width="16" height="16"></td>
                                            <td height="16" background="LoginStyle/bot_mid.gif">
                                                <img src="LoginStyle/bot_mid.gif" width="16" height="16"></td>
                                            <td width="24" height="16">
                                                <img src="LoginStyle/bot_rig.gif" width="24" height="16"></td>
                                        </tr>
                                    </table>
                                    <table style="width: 90%;">
                                        <tr align="left" style="font-family: Verdana; font-size: 9px; font-weight: bold;">
                                            <td>
                                                <asp:RequiredFieldValidator ID="RF_pass" runat="server" ControlToValidate="txtpass"
                                                    Display="None" ErrorMessage="Please Enter Password" SetFocusOnError="True" ValidationGroup="s"></asp:RequiredFieldValidator>
                                                <asp:RequiredFieldValidator ID="RF_userid" runat="server" ControlToValidate="txtusername"
                                                    Display="None" ErrorMessage="Please Enter User Name" SetFocusOnError="True" ValidationGroup="s"></asp:RequiredFieldValidator>
                                                <asp:ValidationSummary
                                                    ID="ValidationSummary1" runat="server" Font-Size="8pt" ValidationGroup="s" />
                                                <asp:Label ID="lblError" runat="server" EnableViewState="False" CssClass="Error" ForeColor="Red"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <table class="Footer" width="100%">
                            <tr>
                                <td align="center" class="Footer">Copyright &copy; 2009 GEMS EDUCATION.<br />
                                    (This site is best viewed with IE 6.0 or above at a minimum screen resolution of 1024x768)            </td>
                                <td align="right">
                                    <asp:Image ID="Image3" ImageAlign="Right" ImageUrl="~/ParentLogin/Images/openPage.png" runat="server" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </form>
    <script type="text/javascript">
        //setupLinks();
        //wl();
    </script>
</body>
</html>

