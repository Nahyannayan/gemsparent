﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ParentMaster.master" AutoEventWireup="false" CodeFile="Home.aspx.vb" Inherits="Home" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphParent" runat="Server">

    <div class="content margin-top30 margin-bottom30">
                    <div class="container">
                        <div class="row">
                            
                         <!-- Posts Block -->


    <!-- left Block -->
    <div class="posts-block col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <div class="my-account">
            
            <div class="bottom-padding">
                <div class="title-box">
                    <h3>Student Information <asp:Label ID="lblStatus" runat="server"  style="font-size:12px;"></asp:Label></h3>
                </div>
                <!-- Table  -->
                <div class="table-responsive">


                    <table align="center" style="width: 100%;" class="table table-responsive text-left my-orders-table">
                        <tbody>
                            <tr>
                                <td><strong>Student Id</strong>
                                </td>
                                <td>
                                   <asp:Literal ID="ltid" runat="server" ></asp:Literal>
                                </td>
                            </tr>
                            <tr>
                                <td><strong>Joining Date</strong></td>
                                <td>
                                     <asp:Literal ID="Literal9" runat="server" ></asp:Literal>
                                </td>
                            </tr>
                            <tr>
                                <td><strong>Grade & Section</strong>
                                </td>
                                <td>
                                   <asp:Literal ID="Literal1" runat="server" ></asp:Literal> - <asp:Literal ID="Literal2" runat="server" ></asp:Literal>
                                </td>
                            </tr>
                            
                            <tr>
                                <td><strong>Teacher Incharge</strong></td>
                                <td>
                                   <asp:Literal ID="Literal4" runat="server" ></asp:Literal>
                                </td>
                            </tr>
                            

                            


                        </tbody>
                    </table>


                </div>
                <!-- /Table  -->
            </div>

        </div>
    </div>
    <!-- /left Block -->
    
    <!-- Right Block -->
    <div class="posts-block col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <div class="my-account">
           
            <div class="bottom-padding">
                <div class="title-box">
                    <h3>&nbsp;<span class="profile-right">
                                <asp:Label ID="lbChildName" runat="server"></asp:Label>
                            </span>
</h3>
                </div>
                <!-- Table  -->
                <div class="table-responsive">


                    <table align="center" style="width: 100%;" class="table table-responsive text-left my-orders-table">
                        <tbody>
                            <tr>
                                <td><strong>Primary Contact</strong>
                                </td>
                                <td>
                                   <asp:Literal ID="Literal5" runat="server" ></asp:Literal>
                                </td>
                            </tr>
                            <tr>
                                <td><strong>Primary Contact Name</strong>
                                </td>
                                <td>
                                   <asp:Literal ID="Literal6" runat="server" ></asp:Literal>
                                </td>
                            </tr>

                            <tr>
                                <td><strong>Primary Contact Email</strong>
                                </td>
                                <td>
                                   <asp:Literal ID="Literal7" runat="server" ></asp:Literal>
                                </td>
                            </tr>
                            <tr>
                                <td><strong>Primary Contact Phone</strong>
                                </td>
                                <td>
                                    <asp:Literal ID="Literal8" runat="server" ></asp:Literal>
                                </td>
                            </tr>
                            
                           

                          
                        </tbody>
                    </table>


                </div>
                <!-- /Table  -->
            </div>

        </div>

    </div>
    <!-- /Right Block -->
                        <!-- Posts Block -->

                             
                </div>
                        <!-- Classroom link section added below -->
                        <div class="row" runat="server" id="divClassroom" visible="false">
                            <div class="col-lg-5">
                                <h3 class="" style="margin: 0; line-height: 52px;">Welcome to our all new Phoenix Classroom</h3>
                            </div>
                            <div class="col-lg-7">
                                <a href="https://classroom.gemseducation.com" target="_blank">
                                    <img src="img/classroom-logo.svg" height="40px"/></a>
                            </div>
                        </div>
                        <!-- Classroom link section added above -->
                        <div class="row">
    <div class="col-12 col-lg-12 col-md-12" style="font-weight: 700;">
        <%--<a href="https://school.gemsoasis.com/oasisfilesnew/PDFLINKS/Travel_Declaration_Form.pdf" target="_blank"><i class="fa fa-file" style="padding-right:6px;"></i>Download Travel Declaration Form</a>--%>
        <h3 class="" style="margin: 0; line-height: 52px; display:inline;"><span runat="server" id="spCovid"><i class="fa fa-file" style="padding-right:6px;"></i>COVID-19 Awareness eLearning Course <span style="font-size:14px;"><a href="https://school.gemsoasis.com/OASISFiles\Policies\COVID-19 Awareness eLearning Course/story.html" target="_blank"> English</a> | <a href="https://school.gemsoasis.com/OASISFiles\Policies\COVID-19 Awareness eLearning Course Arabic/story_html5.html" target="_blank">عربى </a></span> </span>
            </h3>

        <h3 class="" style="margin: 0; line-height: 52px; display:inline;"><span style="float:right;" runat="server" id="spfeepayplan"><a href="https://school.gemsoasis.com/oasisfiles/Policies/School%20Fee%20Delinquent%20Policy.pdf" target="_blank"><i class="fa fa-file" style="padding-right:6px;"></i>School fee delinquent policy</a></span>
            </h3>
        </div>
    </div>
</div>
            </div>

      <!--Slider Section-->
    <div class="content">
                    <div class="container">
                        <div class="row item content-slider margin-top0">

<asp:Repeater ID="rptInfo" runat="server">

    <ItemTemplate>
         <div class="col-sm-4"><div class='<%# Eval("INF_CLASS")%>'>
                                    <h3><i class='<%# Eval("INF_ICON")%>' style="font-size: 60px;"></i></h3>
                                    <h3><%# Eval("INF_TYPE")%></h3>
                                    <div><%# Eval("INF_HEADING")%></div>
                                   <div class='<%# Eval("INF_DIV_BG_CLASS")%>'> <div class='<%# Eval("INF_DIV_CLASS")%>' style='<%# Eval("INF_DIV_STYLE")%>' ><%# Eval("INF_VALUE")%> </div></div>
                                    <a class="btn btn-default btn-home" href='<%# Eval("INF_URL")%>'><%# Eval("INF_TEXT")%></a>
                                </div></div>
    </ItemTemplate>
</asp:Repeater>
 
  



                        </div>

                           

                    </div>	
        </div>
      <!--/Slider Section-->
             
    </div>


</asp:Content>

