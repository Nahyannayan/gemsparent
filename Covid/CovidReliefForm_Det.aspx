﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ParentMaster.master" AutoEventWireup="false" CodeFile="CovidReliefForm_Det.aspx.vb" Inherits="covid_CovidReliefForm_Det" %>



<asp:Content ID="Content1" ContentPlaceHolderID="cphParent" Runat="Server">

     <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"/>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500;600&display=swap" rel="stylesheet"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" />
    <link rel="stylesheet" href="css/jquery.mCustomScrollbar.css"/>
    <link rel="stylesheet" href="css/style.css?1=2"/>

    <script type="text/javascript">


        function reset_FResonRelief(e) {
           
            document.getElementById('<%= lblFReasonRelief.ClientID%>').innerText = "";


        }
        function reset_lblFNationID(e)
        {
           
            document.getElementById('<%= lblFNationID.ClientID%>').innerText = "";
        }
        function reset_lblFCompany(e) {
            document.getElementById('<%= lblFCompany.ClientID%>').innerText = "";
        }
        function reset_lblMReasonRelief(e)
        {
            document.getElementById('<%= lblMReasonRelief.ClientID%>').innerText = "";
        }

        function reset_lblMNationID(e) {

            document.getElementById('<%= lblMNationID.ClientID%>').innerText = "";
        }
        function reset_lblMCompany(e) {
            document.getElementById('<%= lblMCompany.ClientID%>').innerText = "";
        }

        function reset_lblFsalAllow(e) {
            
            document.getElementById('<%= lblFsalAllow.ClientID%>').innerText = "";
        }
        function reset_lblFsalAllow1(e) {
            
            document.getElementById('<%= lblMSalAllow.ClientID%>').innerText = "";
        }


        <%--function reset_lblMsalAllow(e) {
            alert('hi');
            document.getElementById('<%= lblMSalAllow.ClientID%>').innerText = "";
         }--%>
    </script>
    <div class="content margin-top30 margin-bottom60">
        <div class="container">
            <div class="row">

                <!-- Posts Block -->
                <div class="posts-block col-lg-12 col-md-12 col-sm-12 col-xs-12">


     <header class="py-3 bg-white">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 text-center">
                    <h1 class="m-0 text-primary">COVID-19 Relief Application Form</h1>
                </div>
            </div>
        </div>
    </header>
                   <span  style="padding-bottom:10px;" id="spMessage"  runat="server">   <asp:Label ID="lblMessage" runat="server"></asp:Label></span>
    <!-- header end -->
    <!-- form -->
    <main role="main" class="relief-form mb-3">
        <!-- personal details -->
        <section class="personal-info mt-3">
            <div class="container-fluid">
                <div class="row">
                    <!-- parent details -->
                    <div class="col-lg-6 col-md-6 col-12 mb-3 mb-sm-0">
                        <div class="card rounded-0 h-100">
                            <div class="card-body p-0">
                                <div class="row">
                                    <div class="col-12">
                                        <h2 class="section-title border-bottom py-2 px-3 m-0 bg-primary text-white">Parent Details</h2>
                                    </div>
                                </div>
                                <div class="column-padding pt-3 pb-3">
                                    <div class="row mb-4">
                                        <div class="col-sm-6 col-12">
                                            <h3 class="m-0">
                                                <span class="d-block">Father's Name</span>
                                                <asp:Label ID="lblFName" runat="server" class="label-value" ></asp:Label>
                                            </h3>
                                        </div>
                                        <div class="col-sm-6 col-12">
                                            <h3 class="m-0">
                                                <span class="d-block">Mother's Name</span>
                                                <asp:Label ID="lblMName" runat="server" class="label-value"></asp:Label>
                                            </h3>
                                        </div>
                                    </div>
                                    <div class="row mb-4">
                                        <div class="col-sm-6 col-12">
                                            <h3 class="m-0">
                                                <span class="d-block">Primary Contact Number</span>
                                                <asp:Label ID="lblMobNo" runat="server" class="label-value"></asp:Label>
                                            </h3>
                                        </div>
                                        <div class="col-sm-6 col-12">
                                            <h3 class="m-0">
                                                <span class="d-block">Primary Email Address</span>
                                               <asp:Label ID="lblEmail" runat="server" class="label-value"></asp:Label>
                                            </h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- parent details end -->
                    <!-- student details -->
                    <div class="col-lg-6 col-md-6 col-12">
                        <div class="card rounded-0">
                            <div class="card-body p-0">
                                <div class="row">
                                    <div class="col-12">
                                        <h2 class="section-title border-bottom py-2 px-3 m-0 bg-primary text-white">Student Details</h2>
                                    </div>
                                </div>
                                <div class="pb-0">
                                    <div class="studentInfo">
                                         <div class="accordion text-left" id="studentDetails">
                                        <asp:Repeater ID="rptStudDet" runat="server">
                                            <ItemTemplate>
                                                <asp:HiddenField id="hdnStu_id" runat="server" Value='<%# Eval("STU_ID")%>'/>
                                       <div class="card z-depth-0 m-0 border-bottom border-top-0 border-left-0 border-right-0">
                                                <div class="card-header pt-2 pb-2" id="student-1">
                                                    <h5 class="mb-0">
                                                        <button class="btn btn-link d-block w-100 text-left m-0 p-0 collapse" type="button" data-toggle="collapse" data-target="#student-info-1" aria-expanded="true" aria-controls="student-info-1">
                                                            <asp:Label ID="lblStuname" runat="server" Text= '<%# Eval("STU_FULLNAME")%>' class="label-value"></asp:Label>
                                                            <i class="fas fa-angle-down float-right m-0"></i>
                                                        </button>
                                                    </h5>
                                                </div>
                                                <div id="student-info-1" class="collapse show border-top" aria-labelledby="student-1" data-parent="#studentDetails">
                                                    <div class="card-body column-padding pt-2 pb-2">
                                                        <div class="content">
                                                            <div class="row">
                                                                <div class="col-12 col-md-12 col-lg-5">
                                                                    <h3 class="m-0 w-100 pt-2 pb-2">
                                                                        <span class="d-block mb-1">School</span><asp:Label ID="lblSchool" runat="server" Text= '<%# Eval("BSU_NAME")%>' class="label-value"></asp:Label>
                                                                    </h3>
                                                                </div>
                                                                <div class="col-12 col-md-12 col-lg-4">
                                                                    <h3 class="m-0 w-100 pt-2 pb-2">
                                                                        <span class="d-block mb-1">Grade</span><asp:Label ID="lblGrade" runat="server" Text= '<%# Eval("STU_GRADE")%>' class="label-value"></asp:Label>
                                                                    </h3>
                                                                </div>
                                                                <div class="col-12 col-md-12 col-lg-3">
                                                                    <h3 class="m-0 w-100 pt-2 pb-2">
                                                                        <span class="d-block mb-1">Date of Joining</span><asp:Label ID="lblDOJ" runat="server"  Text= '<%# Eval("STU_DOJ")%>' class="label-value"></asp:Label>
                                                                    </h3>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12 col-md-12 col-lg-5">
                                                                    <h3 class="m-0 w-100 pt-2 pb-2">
                                                                        <span class="d-block mb-1">Fees dues until Mar 20</span><asp:Label ID="lblDueMar" runat="server" Text= '<%# Eval("FeeDue_MARCH")%>' class="label-value"></asp:Label>
                                                                    </h3>
                                                                </div>
                                                                <div class="col-12 col-md-12 col-lg-4">
                                                                    <h3 class="m-0 w-100 pt-2 pb-2">
                                                                        <span class="d-block mb-1">Fees due for Apr to Jun 20</span>
                                                                        <asp:Label ID="lblDueJun" runat="server" Text= '<%# Eval("FeeDue_APRJUN")%>' class="label-value"></asp:Label>
                                                                    </h3>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                       
                                            
                                           
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- student details end -->
                </div>
            </div>
        </section>
        <!-- personal details -->

        <section class="salary-details mt-3">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card rounded-0 h-100">
                            <div class="card-body p-0">
                                <div class="row">
                                    <div class="col-12">
                                        <h2 class="section-title border-bottom py-2 px-3 m-0 bg-primary text-white">Employment Details<small class="d-block d-sm-inline-block text-white">( * Required Information )</small></h2>
                                    </div>
                                </div>
                                <div class="row">
                                    <!-- fathers monthly income -->
                                    <div class="col-12 col-md-6">
                                        <div class="row">
                                            <div class="col-12 pr-0 border-right">
                                                <div class="column-padding py-2 px-3 bg-light border-bottom">
                                                    <h3 class="m-0">Father</h3>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12 border-right">
                                                <div class="column-padding pt-3 pb-3">
                                                    <div class="row">
                                                        <div class="col-12 col-md-6">
                                                            <div class="form-group">
                                                                <label>Employment Type</label>
                                                                
                                                                <asp:DropDownList ID="ddlFempType" runat="server" class="form-control" AutoPostBack="true">
                                                                    <asp:ListItem Value="0" Text="--Select--"></asp:ListItem>
                                                                     <asp:ListItem Value="1" Text="Self Employed"></asp:ListItem>
                                                                    <asp:ListItem Value="2" Text="Salaried"></asp:ListItem>
                                                                    <asp:ListItem Value="3" Text="Not Working"></asp:ListItem>
                                                                </asp:DropDownList>
                                                                <asp:Label ID="lblFempType" runat="server" class="text-danger small"></asp:Label>
                                                            </div>
                                                        </div>
                                                        <div class="col-12 col-md-6">
                                                            <div class="form-group">
                                                                <label>Reason for Relief</label>

                                                                 <asp:DropDownList ID="ddlFReasonRelief" runat="server" class="form-control" onchange="reset_FResonRelief(event, this)"></asp:DropDownList>
                                                                 <asp:Label ID="lblFReasonRelief" runat="server" class="text-danger small"></asp:Label>
                                                               
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-12 col-md-6">
                                                            <div class="form-group">
                                                                <label>National ID</label>
                                                                 <asp:TextBox ID="txtFNationID" runat="server" class="form-control"  onkeypress="reset_lblFNationID(event,this)" ></asp:TextBox>
                                                                <ajaxToolkit:FilteredTextBoxExtender ID="ftbe" runat="server"
                                                                                                        TargetControlID="txtFNationID"
                                                                                                        FilterType="Custom, Numbers"
                                                                                                        ValidChars="-" />
                                                                <asp:Label ID="lblFNationID" runat="server" class="text-danger small"></asp:Label>
                                                            </div>
                                                        </div>
                                                        <div class="col-12 col-md-6">
                                                            <div class="form-group">
                                                                <label>Company Name</label>
                                                                 <asp:TextBox ID="txtFCompany" runat="server" class="form-control"  onkeypress="reset_lblFCompany(event,this)"></asp:TextBox>
                                                                  <asp:Label ID="lblFCompany" runat="server" class="text-danger small"></asp:Label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- fathers monthly income -->
                                    <!-- mothers monthly income -->
                                    <div class="col-12 col-md-6">
                                        <div class="row">
                                            <div class="col-12 pl-0">
                                                <div class="column-padding py-2 px-3 bg-light border-bottom">
                                                    <h3 class="m-0">Mother</h3>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="column-padding pt-3 pb-3">
                                                    <div class="row">
                                                        <div class="col-12 col-md-6">
                                                            <div class="form-group">
                                                                <label>Employment Type</label>
                                                               
                                                                <asp:DropDownList ID="ddlMEmpType" runat="server" class="form-control" AutoPostBack="true">
                                                                    <asp:ListItem Value="0" Text="--Select--"></asp:ListItem>
                                                                     <asp:ListItem Value="1" Text="Self Employed"></asp:ListItem>
                                                                    <asp:ListItem Value="2" Text="Salaried"></asp:ListItem>
                                                                    <asp:ListItem Value="3" Text="Not Working"></asp:ListItem>
                                                                </asp:DropDownList>
                                                                 <asp:Label ID="lblMempType" runat="server" class="text-danger small"></asp:Label>
                                                            </div>
                                                        </div>
                                                        <div class="col-12 col-md-6">
                                                            <div class="form-group">
                                                                <label>Reason for Relief</label>
                                                               <asp:DropDownList ID="ddlMReasonRelief" runat="server" class="form-control" onchange="reset_lblMReasonRelief(event, this)"></asp:DropDownList>
                                                                 <asp:Label ID="lblMReasonRelief" runat="server" class="text-danger small"></asp:Label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-12 col-md-6">
                                                            <div class="form-group">
                                                                <label>National ID</label>
                                                                 <asp:TextBox ID="txtMNationID" runat="server" class="form-control"  onkeypress="reset_lblMNationID(event,this)" ></asp:TextBox>
                                                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server"
                                                                                                        TargetControlID="txtMNationID"
                                                                                                        FilterType="Custom, Numbers"
                                                                                                        ValidChars="-" />
                                                                 <asp:Label ID="lblMNationID" runat="server" class="text-danger small"></asp:Label>
                                                            </div>
                                                        </div>
                                                        <div class="col-12 col-md-6">
                                                            <div class="form-group">
                                                                <label>Company Name</label>
                                                                <asp:TextBox ID="txtMCompany" runat="server" class="form-control" onkeypress="reset_lblMCompany(event,this)" ></asp:TextBox>
                                                                <asp:Label ID="lblMCompany" runat="server" class="text-danger small"></asp:Label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                   
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- mothers monthly income -->
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <!-- Salary details -->
        <section class="salary-details mt-3">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card rounded-0 h-100">
                            <div class="card-body p-0">
                                <div class="row">
                                    <div class="col-12">
                                        <h2 class="section-title border-bottom py-2 px-3 m-0 bg-primary text-white">Salary Details <small class="d-block d-sm-inline-block text-white">( * Required Information )</small></h2>
                                    </div>
                                </div>
                                <div class="row">
                                    <!-- fathers monthly income -->
                                    <div class="col-12 col-md-6">
                                        <div class="row">
                                            <div class="col-12 pr-0 border-right">
                                                <div class="column-padding py-2 px-3 bg-light border-bottom">
                                                    <h3 class="m-0">Father - Monthly Salary (AED)</h3>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12 border-right">
                                                <div class="column-padding pt-3 pb-3">
                                                    <div class="row">
                                                        <div class="col-12 col-md-6">
                                                            <div class="form-group">
                                                                <label>Salary and Allowance</label>
                                                                
                                                                <asp:TextBox ID="txtFsalAllow" runat="server" class="form-control" Text="0"  onkeypress="reset_lblFsalAllow(event,this)" ></asp:TextBox>
                                                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server"
                                                                                                        TargetControlID="txtFsalAllow"
                                                                                                        FilterType="Custom, Numbers"
                                                                                                        ValidChars="." />
                                                                <asp:Label ID="lblFsalAllow" runat="server" class="text-danger small"></asp:Label>
                                                            </div>
                                                        </div>
                                                        <div class="col-12 col-md-6">
                                                            <div class="form-group">
                                                                <label>Housing</label>
                                                                <asp:TextBox ID="txtFHousing" runat="server" class="form-control" Text="0" ></asp:TextBox>
                                                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server"
                                                                                                        TargetControlID="txtFHousing"
                                                                                                        FilterType="Custom, Numbers"
                                                                                                        ValidChars="." />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-12 col-md-6">
                                                            <div class="form-group">
                                                                <label>Tuition</label>
                                                                 <asp:TextBox ID="txtFTution" runat="server" class="form-control" Text="0" ></asp:TextBox>
                                                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server"
                                                                                                        TargetControlID="txtFTution"
                                                                                                        FilterType="Custom, Numbers"
                                                                                                        ValidChars="." />
                                                            </div>
                                                        </div>
                                                        <div class="col-12 col-md-6">
                                                            <div class="form-group">
                                                                <label>Others</label>
                                                                 <asp:TextBox ID="txtFOthers" runat="server" class="form-control" Text="0" ></asp:TextBox>
                                                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server"
                                                                                                        TargetControlID="txtFOthers"
                                                                                                        FilterType="Custom, Numbers"
                                                                                                        ValidChars="." />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row" style="display:none;">
                                                        <div class="col-12">
                                                            <h3 class="m-0">
                                                                <span class="d-block">Total salary monthly</span>
                                                                <asp:Label ID="lblFSalary" runat="server" Text="0"></asp:Label>
                                                            </h3>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- fathers monthly income -->
                                    <!-- mothers monthly income -->
                                    <div class="col-12 col-md-6">
                                        <div class="row">
                                            <div class="col-12 pl-0">
                                                <div class="column-padding py-2 px-3 bg-light border-bottom">
                                                    <h3 class="m-0">Mother - Monthly Salary (AED)</h3>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="column-padding pt-3 pb-3">
                                                    <div class="row">
                                                        <div class="col-12 col-md-6">
                                                            <div class="form-group">
                                                                <label>Salary and Allowance</label>
                                                               <asp:TextBox ID="txtMSalAllow" runat="server" class="form-control" Text="0"  onkeypress="reset_lblFsalAllow1(event,this)"></asp:TextBox>
                                                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server"
                                                                                                        TargetControlID="txtMSalAllow"
                                                                                                        FilterType="Custom, Numbers"
                                                                                                        ValidChars="." />
                                                                <asp:Label ID="lblMSalAllow" runat="server" class="text-danger small"></asp:Label>
                                                            </div>
                                                        </div>
                                                        <div class="col-12 col-md-6">
                                                            <div class="form-group">
                                                                <label>Housing</label>
                                                                <asp:TextBox ID="txtMHousing" runat="server" class="form-control" Text="0" ></asp:TextBox>
                                                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server"
                                                                                                        TargetControlID="txtMHousing"
                                                                                                        FilterType="Custom, Numbers"
                                                                                                        ValidChars="." />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-12 col-md-6">
                                                            <div class="form-group">
                                                                <label>Tuition</label>
                                                                 <asp:TextBox ID="txtMTution" runat="server" class="form-control" Text="0" ></asp:TextBox>
                                                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server"
                                                                                                        TargetControlID="txtMTution"
                                                                                                        FilterType="Custom, Numbers"
                                                                                                        ValidChars="." />
                                                            </div>
                                                        </div>
                                                        <div class="col-12 col-md-6">
                                                            <div class="form-group">
                                                                <label>Others</label>
                                                                <asp:TextBox ID="txtMOthers" runat="server" class="form-control" Text="0" ></asp:TextBox>
                                                                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server"
                                                                                                        TargetControlID="txtMOthers"
                                                                                                        FilterType="Custom, Numbers"
                                                                                                        ValidChars="." />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row" style="display:none;">
                                                        <div class="col-12">
                                                            <h3 class="m-0">
                                                                <span class="d-block">Total salary monthly</span>
                                                               <asp:Label ID="lblMSalary" runat="server" Text="0"></asp:Label>
                                                            </h3>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- mothers monthly income -->
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- salary details -->
        <!-- document section -->
        <section class="upload-documents mt-3">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card rounded-0 h-100">
                            <div class="card-body p-0">
                                <div class="row">
                                    <div class="col-12">
                                        <h2 class="section-title border-bottom py-2 px-3 m-0 bg-primary text-white">Upload relevant documents in order to process your application <small class="d-block d-sm-inline-block text-white">( *Each file to be uploaded should not exceed 10 MB )</small></h2>
                                    </div>
                                </div>
                                <!-- fathers documents -->
                                <div class="row">
                                    <div class="col-12">
                                        <div class="row d-none d-sm-flex">
                                            <div class="col-12">
                                                <div class="column-padding py-2 px-3 bg-light border-bottom">
                                                    <h3 class="m-0">Fathers Documents  <span class="text-danger" ><asp:TextBox ID="lblFDocError"    runat="server"  Style="border: none; width:75%;  background-color: transparent; float:right; text-align:right; " onfocus="this.blur();"></asp:TextBox>
                                                        </span>
                                                    
                                                    </h3>
                                                   
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <asp:Repeater ID="rptFDocument" runat="server" >

                                       <ItemTemplate>
                                           <asp:HiddenField  ID="hdnID" runat="server" value='<%# Eval("RDM_ID")%>'/>
                                            <div class="row mt-3">
                                            <div class="col-md-9 col-12 pr-0 d-flex align-items-center">
                                                <div class="column-padding pt-2 pb-2">
                                                    <h3 class="m-0 text-regular"><asp:Label ID="lblFDocname" runat="server" Text= '<%# Eval("RDM_DESC")%>'></asp:Label> </h3>
                                                </div>
                                            </div>
                                               
                                            <div class="col-md-3 col-12 pl-0 d-flex  justify-content-center flex-column">
                                                <div class="column-padding pt-2 pb-2 text-left" style="line-height:14px;">
                                                       
                                                    <telerik:RadAsyncUpload ID="uploadFile" RenderMode="Lightweight" MaxFileInputsCount="1" HideFileInput="true" MultipleFileSelection="Disabled"  
                                                        Width="100%" runat="server"  DropZones=".FileUploadZone">
                                                    </telerik:RadAsyncUpload>
                                                    <span style="font-size:smaller;"><%# Eval("FILEURL")%></span>
                                                    <asp:LinkButton ID="lblFileName" runat="server" Text='<%# Eval("FILEURL")%>' OnClick="lblFileName_Click" Visible="false"></asp:LinkButton>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                        <hr>
                                       </ItemTemplate>
                                        </asp:Repeater>

                                       
                                        

                                    </div>
                                </div>
                                <!-- fathers documents -->
                                <!-- mothers documents -->
                                <div class="row">
                                    <div class="col-12">
                                        <div class="row d-none d-sm-flex">
                                            <div class="col-12">
                                                <div class="column-padding py-2 px-3 border-top border-bottom bg-light">
                                                    <h3 class="m-0">Mothers Documents  
                                                         <span class="text-danger" ><asp:TextBox ID="lblMDocError"    runat="server"  Style="border: none; width:75%;  background-color: transparent; float:right; text-align:right; " onfocus="this.blur();"></asp:TextBox></span>
                                                       
                                                    </h3>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <asp:Repeater ID="rptMDocument" runat="server" >

                                       <ItemTemplate>
                                           <asp:HiddenField  ID="hdnID1" runat="server" value='<%# Eval("RDM_ID")%>'/>
                                            <div class="row mt-3">
                                            <div class="col-md-9 col-12 pr-0 d-flex align-items-center">
                                                <div class="column-padding pt-2 pb-2">
                                                    <h3 class="m-0 text-regular"><asp:Label ID="lblMDocName" runat="server" Text= '<%# Eval("RDM_DESC")%>'></asp:Label> </h3>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-12 pl-0 d-flex  justify-content-center flex-column">
                                                <div class="column-padding pt-2 pb-2 text-left" style="line-height:14px;">
                                                   
                                                      <telerik:RadAsyncUpload ID="uploadFile1" RenderMode="Lightweight" MaxFileInputsCount="1" HideFileInput="true"  MultipleFileSelection="Disabled" 
                                                        Width="100%" runat="server"  DropZones=".FileUploadZone">
                                                    </telerik:RadAsyncUpload>
                                                    <span style="font-size:smaller;"><%# Eval("FILEURL")%></span>
                                                    <asp:LinkButton ID="lblFileName1" runat="server" Text='<%# Eval("RRF_FILENAME")%>' OnClick="lblFileName_Click" Visible="false"></asp:LinkButton>
                                                   
                                                </div>
                                            </div>
                                        </div>
                                        <hr>
                                           </ItemTemplate>
                                            </asp:Repeater>

                                       
                                       

                                    </div>
                                </div>
                                <!-- mothers documents -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- document section -->
        <!-- notes for GEMS section -->
        <section class="user-message mt-3">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card rounded-0 h-100">
                            <div class="card-body p-0">
                                <div class="row">
                                    <div class="col-12">
                                        <h2 class="section-title border-bottom py-2 px-3 m-0 bg-primary text-white">Note to GEMS <small class="d-block d-sm-inline-block text-white">( Additional information if any )</small></h2>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="column-padding py-3">
                                            <div class="form-group">
                                                <textarea class="form-control" rows="4" id="txtRemarks" runat="server"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- notes for GEMS section -->
        <!-- disclaimer section -->
        <section class="disclaimer mt-3">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card rounded-0 h-100">
                            <div class="card-body p-0">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="column-padding py-3">
                                            <div class="custom-control custom-checkbox mb-3 custom-control-inline mr-0 mr-md-5">
                                               <input type="checkbox" class="custom-control-input" id="paymentPlanOption" checked="checked" disabled="disabled">
                                                <label class="custom-control-label"  for="paymentPlanOption">I hereby certify and declare that any information I provide here is true, complete, and not misleading in any way. I understand that by making such certification and declaration, that providing any false, incomplete or misleading information may be considered fraudulent, for which civil and criminal penalties may apply.</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row my-4">
                    <div class="col-12 text-center">
                       <span  style="padding-bottom:10px;" id="spError"  runat="server"> 
                          <%-- <asp:TextBox ID="lblDocError"    runat="server"  Style="border: none; width:75%;  background-color: transparent;  text-align:center; " onfocus="this.blur();"></asp:TextBox>--%>
                           <asp:Label ID="lblDocError" runat="server" ></asp:Label>
                       </span><br/>
                        <asp:Button id="btnSubmit"  runat="server" CssClass="btn btn-primary d-block d-sm-inline-block pb-4 pl-5 pr-5 pt-4 text-uppercase" Text="Submit"   ></asp:Button>
                          <%--<button id="btnSubmit"  runat="server" class="btn btn-primary d-block d-sm-inline-block pb-4 pl-5 pr-5 pt-4 text-uppercase"  style="display:none;">Submit</button>--%>
                    </div>
                </div>
            </div>
        </section>
        <!-- disclaimer section -->



        <!-- upload document modal popup -->
        <div class="modal fade" id="uploadDocument" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Upload Required Document</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="file-upload my-4">
                            <div class="file-select">
                                <div class="file-select-button" id="fileName">Choose File</div>
                                <div class="file-select-name" id="noFile">No file chosen...</div> 
                                <input type="file" name="chooseFile" class="chooseFile">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-primary pl-4 pr-4">Save</button>
                    </div>
                </div>
            </div>
        </div>          
        <!-- upload document modal popup end -->

        <!-- thank you modal popup -->
        <div id="divSuccess" runat="server" visible="false">
        <div class="modal fade" id="thankYou" tabindex="-1" role="dialog" aria-hidden="true" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <!-- <div class="modal-header">
                        <h5 class="modal-title">Thank you</h5>
                    </div> -->
                    <div class="modal-body text-center p-5">
                        <i class="fas fa-check-circle fa-4x mb-3 text-success"></i>
                        <h2 class="mb-4">Thank You</h2>
                        <p class="pb-2">Dear Parent , <br>The application for COVID-19 relief is Submitted. A member of school staff would contact you for further processing.</p>
                        <p>Thanks and Regards<br><strong>GEMS Education</strong></p>
                    </div>
                    <!-- <div class="modal-footer">
                        <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-primary pl-4 pr-4">Save</button>
                    </div> -->
                </div>
            </div>
        </div>   
            </div>       
        <!-- thank you modal popup end -->
    </main>
</div>
                <!-- /Posts Block -->
            </div>
        </div>
    </div>
   <script>

       function validateTerms() {
           if (!this.form.paymentPlanOption.checked) {
               alert('You must agree to the terms first.');
               return false;
           }
       }


       function checkBoxFun() {
         
           if (document.getElementById("paymentPlanOption").checked == true) {
             
               document.getElementById('<%= btnSubmit.ClientID%>').disabled = false;
           }
           else {
               
               
               document.getElementById('<%= btnSubmit.ClientID%>').disabled = true;
              
           }
       }
       
   </script>
</asp:Content>

