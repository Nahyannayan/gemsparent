﻿Imports Microsoft.ApplicationBlocks.Data
Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Net.Mail
Imports Telerik.Web.UI


Partial Class covid_CovidReliefForm_Det
    Inherits System.Web.UI.Page
    Dim encr_decr As New Encryption64
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Session("username") Is Nothing Then
            Session("Active_tab") = "Home"
            Session("Site_Path") = ""
            Response.Redirect("~\login.aspx")
        ElseIf Session("bPasswdChanged") = "False" Then
            Response.Redirect("~\UpdateInfo\ForcePasswordChange.aspx", False)
        ElseIf Session("bUpdateContactDetails") = "False" Then
            Response.Redirect(Session("ForceUpdate_stud"), False)
        End If



        If Page.IsPostBack = False Then

            If Session("Covid_Exceptional") = "1" Then

           

            ViewState("RRH_ID") = 0
            ViewState("mode") = "add"
            disable_ctrl()
            If Request.QueryString("RRHID") IsNot Nothing AndAlso Request.QueryString("RRHID").Trim <> "" Then
                ViewState("RRH_ID") = GetIntegerVal(encr_decr.Decrypt(Request.QueryString("RRHID").Replace(" ", "+")))
                GetSavedRequest()
                ViewState("mode") = "edit"
            Else

                ''added on 16th 

                If Session("validuser") Is Nothing Then
                    ' Response.Redirect("/covid/CovidReliefLanding.aspx", False)
                    Response.Redirect("/home.aspx", False)
                Else
                    Session("validuser") = Nothing
                End If

                IsCovidRequestExists(Session("username"))
            End If

            bind_covid_parent(Session("username"))
            bind_covid_Student(Session("username"))

            ddlFReasonRelief.Items.Insert(0, New ListItem("--Select--", "0"))
                ddlMReasonRelief.Items.Insert(0, New ListItem("--Select--", "0"))
            Else
                Response.Redirect("~\home.aspx", False)

            End If
        End If
    End Sub

    Public Sub IsCovidRequestExists(ByVal username As String)
        Dim conn As String = ConnectionManger.GetOASISConnectionString
        Dim param(5) As SqlParameter
        param(0) = New SqlParameter("@USR_NAME", username)
        param(1) = New SqlParameter("@OPTION", 2)
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "ONLINE.CHECK_COVID_RELIEF_V2", param)
        If ds.Tables(0).Rows.Count > 0 Then
            If Convert.ToString(ds.Tables(0).Rows(0)("Active")) <> "1" And Convert.ToString(ds.Tables(0).Rows(0)("Active")) <> "4" Then
                Response.Redirect("/covid/CovidReliefLanding.aspx", False)
            End If

        End If
    End Sub


    Sub clear_values()
        txtFNationID.Enabled = False
        txtFNationID.Attributes.Add("class", "form-control")
        txtFCompany.Enabled = False
        txtFCompany.Attributes.Add("class", "form-control")
        txtFHousing.Enabled = False
        txtFHousing.Attributes.Add("class", "form-control")
        txtFOthers.Enabled = False
        txtFOthers.Attributes.Add("class", "form-control")
        txtFsalAllow.Enabled = False
        txtFsalAllow.Attributes.Add("class", "form-control")
        txtFTution.Enabled = False
        txtFTution.Attributes.Add("class", "form-control")

        ddlMReasonRelief.Enabled = False
        ddlMReasonRelief.Attributes.Add("class", "form-control")
        txtMNationID.Enabled = False
        txtMNationID.Attributes.Add("class", "form-control")
        txtMCompany.Enabled = False
        txtMCompany.Attributes.Add("class", "form-control")
        txtMHousing.Enabled = False
        txtMHousing.Attributes.Add("class", "form-control")
        txtMOthers.Enabled = False
        txtMOthers.Attributes.Add("class", "form-control")
        txtMSalAllow.Enabled = False
        txtMSalAllow.Attributes.Add("class", "form-control")
        txtMTution.Enabled = False
        txtMTution.Attributes.Add("class", "form-control")
    End Sub


    Sub disable_ctrl()
        ddlFReasonRelief.Enabled = False
        ddlFReasonRelief.Attributes.Add("class", "form-control")
        txtFNationID.Enabled = False
        txtFNationID.Attributes.Add("class", "form-control")
        txtFCompany.Enabled = False
        txtFCompany.Attributes.Add("class", "form-control")
        txtFHousing.Enabled = False
        txtFHousing.Attributes.Add("class", "form-control")
        txtFOthers.Enabled = False
        txtFOthers.Attributes.Add("class", "form-control")
        txtFsalAllow.Enabled = False
        txtFsalAllow.Attributes.Add("class", "form-control")
        txtFTution.Enabled = False
        txtFTution.Attributes.Add("class", "form-control")

        ddlMReasonRelief.Enabled = False
        ddlMReasonRelief.Attributes.Add("class", "form-control")
        txtMNationID.Enabled = False
        txtMNationID.Attributes.Add("class", "form-control")
        txtMCompany.Enabled = False
        txtMCompany.Attributes.Add("class", "form-control")
        txtMHousing.Enabled = False
        txtMHousing.Attributes.Add("class", "form-control")
        txtMOthers.Enabled = False
        txtMOthers.Attributes.Add("class", "form-control")
        txtMSalAllow.Enabled = False
        txtMSalAllow.Attributes.Add("class", "form-control")
        txtMTution.Enabled = False
        txtMTution.Attributes.Add("class", "form-control")
    End Sub

    Public Sub bind_covid_parent(ByVal username As String)
        Dim s As String = ""
        Dim conn As String = ConnectionManger.GetOASISConnectionString
        Dim param(5) As SqlParameter
        param(0) = New SqlParameter("@OLU_NAME", username)
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "COV.GET_PARENT_INFO", param)
        If ds.Tables(0).Rows.Count > 0 Then
            lblFName.Text = ds.Tables(0).Rows(0)("F_NAME").ToString
            lblMName.Text = ds.Tables(0).Rows(0)("M_NAME").ToString
            lblEmail.Text = ds.Tables(0).Rows(0)("F_EMAIL").ToString
            lblMobNo.Text = ds.Tables(0).Rows(0)("F_MOBILE").ToString
        End If
    End Sub

    Public Sub bind_covid_Student(ByVal username As String)
        Dim s As String = ""
        Dim conn As String = ConnectionManger.GetOASISConnectionString
        Dim param(5) As SqlParameter
        param(0) = New SqlParameter("@OLU_NAME", username)
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "COV.GET_HEADFER_INFO", param)
        If ds.Tables(0).Rows.Count > 0 Then
            rptStudDet.DataSource = ds.Tables(0)
            rptStudDet.DataBind()
        End If
    End Sub


    Public Sub bind_covid_FDocument(ByVal id As String)
        Dim s As String = ""
        Dim conn As String = ConnectionManger.GetOASISConnectionString
        Dim param(5) As SqlParameter
        param(0) = New SqlParameter("@EMPL_TYPES", id)
        param(1) = New SqlParameter("@PARENT_TYPE", "F")
        param(2) = New SqlParameter("@RRH_ID", ViewState("RRH_ID"))
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "COV.GET_APPLICABLE_DOCTYPES", param)
        If ds.Tables(0).Rows.Count > 0 Then
            rptFDocument.DataSource = ds.Tables(0)
            rptFDocument.DataBind()
        Else
            rptFDocument.DataSource = Nothing
            rptFDocument.DataBind()
        End If
        bind_covid_FReason(id)

    End Sub
    Public Sub bind_covid_FReason(ByVal id As String)
        Dim s As String = ""
        ddlFReasonRelief.Items.Clear()
        Dim conn As String = ConnectionManger.GetOASISConnectionString
        Dim param(5) As SqlParameter
        param(0) = New SqlParameter("@EMPL_TYPES", id)
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "COV.GET_APPLICABLE_REASONS", param)

        If ds.Tables(0).Rows.Count > 0 Then
            ddlFReasonRelief.DataSource = ds
            ddlFReasonRelief.DataValueField = "RSN_ID"
            ddlFReasonRelief.DataTextField = "RSN_DESC"
            ddlFReasonRelief.DataBind()
        End If
        ddlFReasonRelief.Items.Insert(0, New ListItem("--Select--", "0"))
    End Sub


    Public Sub bind_covid_MDocument(ByVal id As String)
        Dim s As String = ""
        Dim conn As String = ConnectionManger.GetOASISConnectionString
        Dim param(5) As SqlParameter
        param(0) = New SqlParameter("@EMPL_TYPES", id)
        param(1) = New SqlParameter("@PARENT_TYPE", "M")
        param(2) = New SqlParameter("@RRH_ID", ViewState("RRH_ID"))
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "COV.GET_APPLICABLE_DOCTYPES", param)
        If ds.Tables(0).Rows.Count > 0 Then
            rptMDocument.DataSource = ds.Tables(0)
            rptMDocument.DataBind()
        Else
            rptMDocument.DataSource = Nothing
            rptMDocument.DataBind()
        End If
        bind_covid_MReason(id)

    End Sub
    Public Sub bind_covid_MReason(ByVal id As String)
        Dim s As String = ""
        ddlMReasonRelief.Items.Clear()
        Dim conn As String = ConnectionManger.GetOASISConnectionString
        Dim param(5) As SqlParameter
        param(0) = New SqlParameter("@EMPL_TYPES", id)
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "COV.GET_APPLICABLE_REASONS", param)

        If ds.Tables(0).Rows.Count > 0 Then
            ddlMReasonRelief.DataSource = ds

            ddlMReasonRelief.DataValueField = "RSN_ID"
            ddlMReasonRelief.DataTextField = "RSN_DESC"

            ddlMReasonRelief.DataBind()
        End If
        ddlMReasonRelief.Items.Insert(0, New ListItem("--Select--", "0"))
    End Sub

    Protected Sub ddlFempType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlFempType.SelectedIndexChanged
        bind_covid_FDocument(ddlFempType.SelectedValue)

        lblFempType.Text = ""
        lblFNationID.Text = ""
        lblFCompany.Text = ""
        lblFsalAllow.Text = ""
        lblFDocError.Text = ""
        lblDocError.Text = ""
        lblFReasonRelief.Text = ""
        spError.Attributes.Add("class", "")

        If ddlFempType.SelectedValue = 1 Then
            ddlFReasonRelief.Enabled = True
            txtFNationID.Enabled = True
            txtFCompany.Enabled = True
            txtFHousing.Enabled = False
            txtFOthers.Enabled = False
            txtFsalAllow.Enabled = False
            txtFTution.Enabled = False
            txtFHousing.Text = "0"
            txtFOthers.Text = "0"
            txtFsalAllow.Text = "0"
            txtFTution.Text = "0"
        ElseIf ddlFempType.SelectedValue = 2 Then
            ddlFReasonRelief.Enabled = True
            txtFNationID.Enabled = True
            txtFCompany.Enabled = True
            txtFHousing.Enabled = True
            txtFOthers.Enabled = True
            txtFsalAllow.Enabled = True
            txtFTution.Enabled = True
        Else
            ddlFReasonRelief.Enabled = False
            txtFNationID.Enabled = False
            txtFCompany.Enabled = False
            txtFHousing.Enabled = False
            txtFOthers.Enabled = False
            txtFsalAllow.Enabled = False
            txtFTution.Enabled = False

            txtFNationID.Text = ""
            txtFCompany.Text = ""
            txtFHousing.Text = "0"
            txtFOthers.Text = "0"
            txtFsalAllow.Text = "0"
            txtFTution.Text = "0"


        End If

    End Sub

    Protected Sub ddlMEmpType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlMEmpType.SelectedIndexChanged
        bind_covid_MDocument(ddlMEmpType.SelectedValue)


        lblMNationID.Text = ""
        lblMCompany.Text = ""
        lblMSalAllow.Text = ""
        lblMDocError.Text = ""
        lblMempType.Text = ""
        lblMReasonRelief.Text = ""
        lblDocError.Text = ""
        spError.Attributes.Add("class", "")

        If ddlMEmpType.SelectedValue = 1 Then
            ddlMReasonRelief.Enabled = True
            txtMNationID.Enabled = True
            txtMCompany.Enabled = True
            txtMHousing.Enabled = False
            txtMOthers.Enabled = False
            txtMSalAllow.Enabled = False
            txtMTution.Enabled = False

            txtMHousing.Text = "0"
            txtMOthers.Text = "0"
            txtMSalAllow.Text = "0"
            txtMTution.Text = "0"

        ElseIf ddlMEmpType.SelectedValue = 2 Then
            ddlMReasonRelief.Enabled = True
            txtMNationID.Enabled = True
            txtMCompany.Enabled = True
            txtMHousing.Enabled = True
            txtMOthers.Enabled = True
            txtMSalAllow.Enabled = True
            txtMTution.Enabled = True
        Else
            ddlMReasonRelief.Enabled = False
            txtMNationID.Enabled = False
            txtMCompany.Enabled = False
            txtMHousing.Enabled = False
            txtMOthers.Enabled = False
            txtMSalAllow.Enabled = False
            txtMTution.Enabled = False

            txtMNationID.Text = ""
            txtMCompany.Text = ""
            txtMHousing.Text = "0"
            txtMOthers.Text = "0"
            txtMSalAllow.Text = "0"
            txtMTution.Text = "0"

        End If
    End Sub

    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        Dim exi As Integer = 0

        lblFempType.Text = ""
        lblFNationID.Text = ""
        lblFCompany.Text = ""
        lblFsalAllow.Text = ""
        lblMNationID.Text = ""
        lblMCompany.Text = ""
        lblMSalAllow.Text = ""
        lblFDocError.Text = ""
        lblMDocError.Text = ""
        lblMempType.Text = ""
        lblFReasonRelief.Text = ""
        lblMReasonRelief.Text = ""

        txtFsalAllow.Text = txtFsalAllow.Text.Replace("-", "").Replace(",", "")
        txtFHousing.Text = txtFHousing.Text.Replace("-", "").Replace(",", "")
        txtFTution.Text = txtFTution.Text.Replace("-", "").Replace(",", "")
        txtFOthers.Text = txtFOthers.Text.Replace("-", "").Replace(",", "")
        txtMSalAllow.Text = txtMSalAllow.Text.Replace("-", "").Replace(",", "")
        txtMHousing.Text = txtMHousing.Text.Replace("-", "").Replace(",", "")
        txtMTution.Text = txtMTution.Text.Replace("-", "").Replace(",", "")
        txtMOthers.Text = txtMOthers.Text.Replace("-", "").Replace(",", "")

        txtFNationID.Text = txtFNationID.Text.Replace("-", "").Replace(",", "").Replace("'", "")
        txtMNationID.Text = txtMNationID.Text.Replace("-", "").Replace(",", "").Replace("'", "")
        txtRemarks.InnerText = txtRemarks.InnerText.Replace("'", "")

        If txtFsalAllow.Text.Replace(" ", "") = "" Then txtFsalAllow.Text = "0"
        If txtFHousing.Text.Replace(" ", "") = "" Then txtFHousing.Text = "0"
        If txtFTution.Text.Replace(" ", "") = "" Then txtFTution.Text = "0"
        If txtFOthers.Text.Replace(" ", "") = "" Then txtFOthers.Text = "0"
        If txtMSalAllow.Text.Replace(" ", "") = "" Then txtMSalAllow.Text = "0"
        If txtMHousing.Text.Replace(" ", "") = "" Then txtMHousing.Text = "0"
        If txtMTution.Text.Replace(" ", "") = "" Then txtMTution.Text = "0"
        If txtMOthers.Text.Replace(" ", "") = "" Then txtMOthers.Text = "0"



        If ((txtMSalAllow.Enabled = True) And (txtMSalAllow.Text <= 0)) Then
            lblMSalAllow.Text = "Mandatory information missing"
            txtMSalAllow.Focus()
            exi = 1
        End If


        If ((txtMCompany.Enabled = True) And (txtMCompany.Text = "")) Then
            lblMCompany.Text = "Mandatory information missing"
            txtMCompany.Focus()
            exi = 1
        End If
        If ((txtMNationID.Enabled = True) And (txtMNationID.Text = "")) Then
            lblMNationID.Text = "Mandatory information missing "
            txtMNationID.Focus()
            exi = 1
        End If
        If ((txtFsalAllow.Enabled = True) And (txtFsalAllow.Text <= 0)) Then
            lblFsalAllow.Text = "Mandatory information missing"
            txtFsalAllow.Focus()
            exi = 1
        End If
        If ((txtFCompany.Enabled = True) And (txtFCompany.Text = "")) Then
            lblFCompany.Text = "Mandatory information missing"
            txtFCompany.Focus()
            exi = 1
        End If
        If ((txtFNationID.Enabled = True) And (txtFNationID.Text = "")) Then
            lblFNationID.Text = "Mandatory information missing "
            txtFNationID.Focus()
            ' txtFNationID.Attributes.Add("class", "border-danger")
            exi = 1
        End If
        If (((ddlMEmpType.SelectedValue = 1) Or (ddlMEmpType.SelectedValue = 2)) And (ddlMReasonRelief.SelectedValue = 0)) Then
            lblMReasonRelief.Text = "Mandatory information missing "
            ddlMReasonRelief.Focus()
            exi = 1
        End If
        If (((ddlFempType.SelectedValue = 1) Or (ddlFempType.SelectedValue = 2)) And (ddlFReasonRelief.SelectedValue = 0)) Then
            lblFReasonRelief.Text = "Mandatory information missing "
            ddlFReasonRelief.Focus()
            exi = 1
        End If
        If ((ddlFempType.SelectedValue = 0) And (ddlMEmpType.SelectedValue = 0)) Then

            lblFempType.Text = "Mandatory information missing "
            lblMempType.Text = "Mandatory information missing "
            ddlFempType.Focus()
            exi = 1
        End If
        If exi = 1 Then
            Exit Sub
        End If



        'If txtFNationID.Enabled = False Then
        '    RequiredFieldValidator1.Enabled = False
        '    RequiredFieldValidator1.Visible = False

        'End If
        'If txtFCompany.Enabled = False Then
        '    RequiredFieldValidator2.Enabled = False
        '    RequiredFieldValidator2.Visible = False
        'End If
        'If txtMNationID.Enabled = False Then
        '    RequiredFieldValidator4.Enabled = False
        '    RequiredFieldValidator4.Visible = False
        'End If
        'If txtMCompany.Enabled = False Then
        '    RequiredFieldValidator3.Enabled = False
        '    RequiredFieldValidator3.Visible = False
        'End If


        'If Page.IsValid Then


        Dim connection As String = ConnectionManger.GetOASISConnectionString
        Dim con As SqlConnection = New SqlConnection(connection)
        con.Open()
        Dim sqltran As SqlTransaction

        sqltran = con.BeginTransaction("trans")
        Try
            SAVE_RELIEF_REQUEST_H(sqltran)
            SAVE_RELIEF_REQUEST_D(sqltran)

            If rptFDocument.Items.Count > 0 Then
                SAVE_RELIEF_REQUEST_FILES(sqltran, "FATHER", rptFDocument)
            End If
            If rptMDocument.Items.Count > 0 Then
                SAVE_RELIEF_REQUEST_FILES(sqltran, "MOTHER", rptMDocument)
            End If

            Dim err = validate_attachment(sqltran, ViewState("RRH_ID"))


            If err = "" Then
                sqltran.Commit()
                'divSuccess.Visible = True
                btnSubmit.Visible = False
                Response.Redirect("/others/covid19gems.aspx", False)

                lblMessage.Text = "The application for COVID-19 relief is Submitted. A member of school staff would contact you for further processing."

            Else
                lblMessage.Text = err
                sqltran.Rollback()
                If ViewState("mode") = "add" Then
                    ViewState("RRH_ID") = "0"
                    ViewState("RRD_ID") = "0"
                End If
                divSuccess.Visible = False
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "covid Submit")
            lblMessage.Text = "Record can not be saved, please make sure the entered data is in the correct format"
            lblDocError.Text = lblMessage.Text
            If lblDocError.Text <> "" Then
                spError.Attributes.Add("class", "text-danger form-control blink")

            Else
                spError.Attributes.Add("class", "")
            End If
            If lblMessage.Text <> "" Then
                spMessage.Attributes.Add("class", "text-danger form-control blink")
            Else
                spMessage.Attributes.Add("class", "")
            End If
            sqltran.Rollback()
        End Try
        'Else

        ' End If
    End Sub


    Private Function validate_attachment(sqltran As SqlTransaction, ByVal id As String) As String
        Dim pParm(5) As SqlClient.SqlParameter

        pParm(0) = New SqlClient.SqlParameter("@RRH_ID", id)



        pParm(1) = New SqlClient.SqlParameter("@RETVAL", SqlDbType.VarChar, 500)
        pParm(1).Direction = ParameterDirection.Output

        pParm(2) = New SqlClient.SqlParameter("@RETFVAL", SqlDbType.VarChar, 500)
        pParm(2).Direction = ParameterDirection.Output

        pParm(3) = New SqlClient.SqlParameter("@RETMVAL", SqlDbType.VarChar, 500)
        pParm(3).Direction = ParameterDirection.Output

        SqlHelper.ExecuteNonQuery(sqltran, CommandType.StoredProcedure, "[COV].[VALIDATE_RELIEF_REQUESTS]", pParm)
        Dim RETVAL As String = Convert.ToString(pParm(1).Value)
        Dim RETFVAL As String = Convert.ToString(pParm(2).Value)
        Dim RETMVAL As String = Convert.ToString(pParm(3).Value)


        If RETMVAL <> "" Then
            lblMDocError.Text = RETMVAL
            ' lblMDocError.Focus()

        End If
        If RETFVAL <> "" Then
            lblFDocError.Text = RETFVAL
            lblFDocError.Focus()
        End If
        lblDocError.Text = RETMVAL + " , " + RETFVAL
        If lblDocError.Text <> "" Then
            spError.Attributes.Add("class", "text-danger form-control blink")
            btnSubmit.Focus()
        Else
            spError.Attributes.Add("class", "")
        End If


        Return RETVAL

    End Function


    Private Sub SAVE_RELIEF_REQUEST_H(sqltran As SqlTransaction)
        Dim pParm(26) As SqlClient.SqlParameter

        


        pParm(0) = New SqlClient.SqlParameter("@RRH_DEF_BSU_ID", Session("STU_BSU_ID"))
        pParm(1) = New SqlClient.SqlParameter("@RRH_DEF_STU_ID", Session("STU_ID"))
        pParm(2) = New SqlClient.SqlParameter("@RRH_F_NAME", lblFName.Text)
        pParm(3) = New SqlClient.SqlParameter("@RRH_F_EMPLOYMENT_TYPE", ddlFempType.SelectedValue)
        pParm(4) = New SqlClient.SqlParameter("@RRH_F_RELIEF_REASON", ddlFReasonRelief.SelectedValue)
        pParm(5) = New SqlClient.SqlParameter("@RRH_F_NATIONAL_ID", txtFNationID.Text)
        pParm(6) = New SqlClient.SqlParameter("@RRH_F_COMPANY_NAME", txtFCompany.Text)
        pParm(7) = New SqlClient.SqlParameter("@RRH_F_SALARY_ALLOWANCE", txtFsalAllow.Text)
        pParm(8) = New SqlClient.SqlParameter("@RRH_F_HOUSING", txtFHousing.Text)
        pParm(9) = New SqlClient.SqlParameter("@RRH_F_TUITION", txtFTution.Text)
        pParm(10) = New SqlClient.SqlParameter("@RRH_F_OTHERS", txtFOthers.Text)
        pParm(11) = New SqlClient.SqlParameter("@RRH_F_TOT_SALARY", lblFSalary.Text)
        pParm(12) = New SqlClient.SqlParameter("@RRH_M_NAME", lblMName.Text)
        pParm(13) = New SqlClient.SqlParameter("@RRH_M_EMPLOYMENT_TYPE", ddlMEmpType.SelectedValue)
        pParm(14) = New SqlClient.SqlParameter("@RRH_M_RELIEF_REASON", ddlMReasonRelief.SelectedValue)
        pParm(15) = New SqlClient.SqlParameter("@RRH_M_NATIONAL_ID", txtMNationID.Text)
        pParm(16) = New SqlClient.SqlParameter("@RRH_M_COMPANY_NAME", txtMCompany.Text)
        pParm(17) = New SqlClient.SqlParameter("@RRH_M_SALARY_ALLOWANCE", txtMSalAllow.Text)
        pParm(18) = New SqlClient.SqlParameter("@RRH_M_HOUSING", txtMHousing.Text)
        pParm(19) = New SqlClient.SqlParameter("@RRH_M_TUITION", txtMTution.Text)
        pParm(20) = New SqlClient.SqlParameter("@RRH_M_OTHERS", txtMOthers.Text)
        pParm(21) = New SqlClient.SqlParameter("@RRH_M_TOT_SALARY", lblMSalary.Text)
        pParm(22) = New SqlClient.SqlParameter("@RRH_REMARKS", txtRemarks.InnerText)
        pParm(23) = New SqlClient.SqlParameter("@RRH_LOG_PAR_USER_ID", Session("username"))

        pParm(24) = New SqlClient.SqlParameter("@RRH_ID", SqlDbType.Int) With
                    {.Direction = ParameterDirection.InputOutput,
                     .Value = ViewState("RRH_ID")
                      }

        SqlHelper.ExecuteNonQuery(sqltran, CommandType.StoredProcedure, "[COV].[SAVE_RELIEF_REQUEST_H]", pParm)
        'Dim RRH_ID As String = Convert.ToString(pParm(24).Value)
        ViewState("RRH_ID") = GetIntegerVal(pParm(24).Value)
    End Sub

    Private Sub SAVE_RELIEF_REQUEST_D(sqltran As SqlTransaction)
        Dim lblDueMar As Label
        Dim lblDueJun As Label
        Dim hdnStu_id As HiddenField
        Dim pParm(11) As SqlClient.SqlParameter

        For i = 0 To rptStudDet.Items.Count - 1

            With rptStudDet.Items(i)
                lblDueMar = .FindControl("lblDueMar")
                lblDueJun = .FindControl("lblDueJun")
                hdnStu_id = .FindControl("hdnStu_id")
                pParm(0) = New SqlClient.SqlParameter("@RRD_RRH_ID", ViewState("RRH_ID"))
                pParm(1) = New SqlClient.SqlParameter("@RRD_BSU_ID", Session("STU_BSU_ID"))
                pParm(2) = New SqlClient.SqlParameter("@RRD_STU_ID", hdnStu_id.Value)
                pParm(3) = New SqlClient.SqlParameter("@RRD_ACD_ID", 0)
                pParm(4) = New SqlClient.SqlParameter("@RRD_PREV_TRM_DUE", lblDueMar.Text)
                pParm(5) = New SqlClient.SqlParameter("@RRD_CURR_TRM_DUE", lblDueJun.Text)
                pParm(6) = New SqlClient.SqlParameter("@RRD_CURR_TRM_CONC", 0)
                pParm(7) = New SqlClient.SqlParameter("@RRD_CONC_TYPE", 0)
                pParm(8) = New SqlClient.SqlParameter("@RRD_REMARKS", "")
                pParm(9) = New SqlClient.SqlParameter("@RRD_ID", SqlDbType.Int)
                pParm(9).Direction = ParameterDirection.Output

                SqlHelper.ExecuteNonQuery(sqltran, CommandType.StoredProcedure, "[COV].[SAVE_RELIEF_REQUEST_D]", pParm)
                'Dim RRD_ID As String = Convert.ToString(pParm(9).Value)

                ViewState("RRD_ID") = GetIntegerVal(pParm(9).Value)
            End With
        Next
    End Sub

    Private Sub SAVE_RELIEF_REQUEST_FILES(sqltran As SqlTransaction, ByVal type As String, ByVal irpt As Repeater)
        Dim uploadFile As New RadAsyncUpload
        Dim hdnID As New HiddenField

        Dim pParm(8) As SqlClient.SqlParameter
        For i = 0 To irpt.Items.Count - 1
            With irpt.Items(i)
                If type = "FATHER" Then
                    uploadFile = .FindControl("uploadFile")
                    hdnID = .FindControl("hdnID")
                ElseIf type = "MOTHER" Then
                    uploadFile = .FindControl("uploadFile1")
                    hdnID = .FindControl("hdnID1")
                End If

                'Dim j As Integer
                For j As Integer = 0 To uploadFile.UploadedFiles.Count - 1
                    pParm(0) = New SqlClient.SqlParameter("@RRF_ID", 0)
                    pParm(1) = New SqlClient.SqlParameter("@RRF_RRH_ID", ViewState("RRH_ID"))
                    pParm(2) = New SqlClient.SqlParameter("@RRF_PARENT_TYP", type)
                    pParm(3) = New SqlClient.SqlParameter("@RRF_RDM_ID", hdnID.Value)
                    pParm(4) = New SqlClient.SqlParameter("@RRF_FILETYPE", uploadFile.UploadedFiles(j).ContentType)
                    pParm(5) = New SqlClient.SqlParameter("@RRF_FILENAME", Replace(uploadFile.UploadedFiles(j).FileName, " ", ""))
                    pParm(6) = New SqlClient.SqlParameter("@RRF_FILEPATH", UploadEvidence(uploadFile, 0, type))

                    SqlHelper.ExecuteNonQuery(sqltran, CommandType.StoredProcedure, "[COV].[SAVE_RELIEF_REQUEST_FILES]", pParm)
                Next
            End With
        Next
    End Sub

    Sub chk_click(sender As Object, e As EventArgs)

        'If paymentPlanOption.Checked Then
        '    btnSubmit.Visible = True
        'Else
        '    btnSubmit.Visible = False
        'End If
    End Sub

    Protected Sub rptFDocument_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptFDocument.ItemDataBound
        ' Dim fu As New FileUpload


        If (e.Item.ItemType = ListItemType.Item) Or (e.Item.ItemType = ListItemType.AlternatingItem) Then
            If e.Item.DataItem Is Nothing Then
                Return



            Else
                'fu = DirectCast(e.Item.FindControl("fileUpload"), FileUpload)


                'Dim ScriptManager1 As ScriptManager = DirectCast(Master.FindControl("ScriptManager1"), ScriptManager)
                'ScriptManager1.RegisterPostBackControl(fu)



                Dim lblFileName As LinkButton = e.Item.FindControl("lblFileName")


                Dim ScriptManager1 As ScriptManager = DirectCast(Master.FindControl("ScriptManager1"), ScriptManager)
                ScriptManager1.RegisterPostBackControl(lblFileName)

            End If
        End If




    End Sub
    Protected Sub lblFileName_Click(sender As Object, e As EventArgs)
        Dim lblFile As LinkButton = CType(sender, LinkButton)
        'DownloadFile(lblFile.ToolTip)
    End Sub
    Function UploadEvidence(uploadFile As RadAsyncUpload, ByVal k As Integer, ByVal p As String) As String
        Dim str As String = ""
        Try
            If k = 0 Then


                Dim STU_ID As String = Session("STU_ID")
                Dim STU_BSU_ID As String = Session("sBsuid")
                Dim PHOTO_PATH As String = String.Empty
                Dim ConFigPath As String = Convert.ToString(ConfigurationManager.AppSettings("covidreleif"))


                If Not Directory.Exists(ConFigPath & "\" & STU_BSU_ID & "\" & STU_ID & "\" & p & "\") Then
                    Directory.CreateDirectory(ConFigPath & "\" & STU_BSU_ID & "\" & STU_ID & "\" & p & "\")
                End If

                If uploadFile.UploadedFiles.Count > 0 Then
                    Dim tempDir As String = ConFigPath & "\" & STU_BSU_ID & "\" & STU_ID & "\" & p & "\"

                    Dim tempFileName As String = Replace(uploadFile.UploadedFiles(0).GetName, " ", "")
                    Dim tempFileNameUsed As String = tempDir + tempFileName
                    If uploadFile.UploadedFiles.Count Then
                        If File.Exists(tempFileNameUsed) Then
                            File.Delete(tempFileNameUsed)
                        End If
                        Try
                            uploadFile.UploadedFiles(0).SaveAs(tempFileNameUsed)

                        Catch ex As Exception

                        End Try
                        Try
                        Catch ex As Exception
                            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)

                        End Try
                    End If
                    str = "\" & STU_BSU_ID & "\" & STU_ID & "\" & p & "\" & tempFileName
                End If
            End If
            Return str
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "Covid Relief")
        End Try
        Return str
    End Function

    Protected Sub rptMDocument_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptMDocument.ItemDataBound
        If (e.Item.ItemType = ListItemType.Item) Or (e.Item.ItemType = ListItemType.AlternatingItem) Then
            If e.Item.DataItem Is Nothing Then
                Return



            Else
                'fu = DirectCast(e.Item.FindControl("fileUpload"), FileUpload)


                'Dim ScriptManager1 As ScriptManager = DirectCast(Master.FindControl("ScriptManager1"), ScriptManager)
                'ScriptManager1.RegisterPostBackControl(fu)




                Dim lblFileName1 As LinkButton = e.Item.FindControl("lblFileName1")

                Dim ScriptManager1 As ScriptManager = DirectCast(Master.FindControl("ScriptManager1"), ScriptManager)

                ScriptManager1.RegisterPostBackControl(lblFileName1)
            End If
        End If
    End Sub
    Public Function GetIntegerVal(ByVal Value As Object) As Integer
        GetIntegerVal = 0
        Try
            If IsNumeric(Value) Then
                GetIntegerVal = Convert.ToInt32(Value)
            End If
        Catch ex As Exception
            GetIntegerVal = 0
        End Try
    End Function
    Private Sub GetSavedRequest()
        If GetIntegerVal(ViewState("RRH_ID")) > 0 Then
            Dim pParm(0) As SqlClient.SqlParameter
            pParm(0) = New SqlClient.SqlParameter("@RRH_ID", SqlDbType.Int) With
                       {.Value = ViewState("RRH_ID")}
            Dim dsReq As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, "[COV].[GET_COV_RELIEF_REQUEST_H]", pParm)
            If Not dsReq Is Nothing AndAlso dsReq.Tables(0).Rows.Count > 0 Then
                '----------Father info
                ddlFempType.SelectedValue = dsReq.Tables(0).Rows(0)("RRH_F_EMPLOYMENT_TYPE")
                ddlFempType_SelectedIndexChanged(Nothing, Nothing)
                ddlFReasonRelief.SelectedValue = dsReq.Tables(0).Rows(0)("RRH_F_RELIEF_REASON")
                txtFNationID.Text = dsReq.Tables(0).Rows(0)("RRH_F_NATIONAL_ID")
                txtFCompany.Text = dsReq.Tables(0).Rows(0)("RRH_F_COMPANY_NAME")
                txtFsalAllow.Text = dsReq.Tables(0).Rows(0)("RRH_F_SALARY_ALLOWANCE")
                txtFHousing.Text = dsReq.Tables(0).Rows(0)("RRH_F_HOUSING")
                txtFTution.Text = dsReq.Tables(0).Rows(0)("RRH_F_TUITION")
                txtFOthers.Text = dsReq.Tables(0).Rows(0)("RRH_F_OTHERS")
                lblFSalary.Text = dsReq.Tables(0).Rows(0)("RRH_F_TOT_SALARY")
                '----------Mother info
                ddlMEmpType.SelectedValue = dsReq.Tables(0).Rows(0)("RRH_M_EMPLOYMENT_TYPE")
                ddlMEmpType_SelectedIndexChanged(Nothing, Nothing)
                ddlMReasonRelief.SelectedValue = dsReq.Tables(0).Rows(0)("RRH_M_RELIEF_REASON")
                txtMNationID.Text = dsReq.Tables(0).Rows(0)("RRH_M_NATIONAL_ID")
                txtMCompany.Text = dsReq.Tables(0).Rows(0)("RRH_M_COMPANY_NAME")
                txtMSalAllow.Text = dsReq.Tables(0).Rows(0)("RRH_M_SALARY_ALLOWANCE")
                txtMHousing.Text = dsReq.Tables(0).Rows(0)("RRH_M_HOUSING")
                txtMTution.Text = dsReq.Tables(0).Rows(0)("RRH_M_TUITION")
                txtMOthers.Text = dsReq.Tables(0).Rows(0)("RRH_M_OTHERS")
                lblMSalary.Text = dsReq.Tables(0).Rows(0)("RRH_M_TOT_SALARY")
                '----------oTHER INFO
                txtRemarks.InnerText = dsReq.Tables(0).Rows(0)("RRH_REMARKS")
                'GetUploadedFiles()
            End If
        End If
    End Sub
    Sub GetUploadedFiles()
        Dim pParm(0) As SqlClient.SqlParameter
        pParm(0) = New SqlClient.SqlParameter("@RRH_ID", SqlDbType.Int) With
                   {.Value = ViewState("RRH_ID")}
        Dim dsFiles As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, "[COV].[GET_COV_RELIEF_REQUEST_FILES]", pParm)
        If Not dsFiles Is Nothing AndAlso dsFiles.Tables(0).Rows.Count > 0 Then
            Dim dvF As DataView = New DataView(dsFiles.Tables(0))
            dvF.RowFilter = "PARENT_TYPE = 'F'"
            Dim dtFFiles As DataTable = dvF.ToTable
            If Not dtFFiles Is Nothing AndAlso dtFFiles.Rows.Count > 0 Then
                rptFDocument.DataSource = dtFFiles
                rptFDocument.DataBind()
            End If
            Dim dvM As DataView = New DataView(dsFiles.Tables(0))
            dvM.RowFilter = "PARENT_TYPE = 'M'"
            Dim dtMFiles As DataTable = dvM.ToTable
            If Not dtMFiles Is Nothing AndAlso dtMFiles.Rows.Count > 0 Then
                rptMDocument.DataSource = dtMFiles
                rptMDocument.DataBind()
            End If
        End If
    End Sub
End Class
