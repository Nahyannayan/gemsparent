﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Partial Class Covid_CovidReliefLanding
    Inherits System.Web.UI.Page

    Dim encr_decr As New Encryption64
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Session("CLEV_MODULE") = Nothing

        If Session("Covid_Exceptional") = "1" Then
            IsCovidRequestExists(Session("username"))
        Else
            Response.Redirect("~\home.aspx", False)

        End If

    End Sub

    Public Sub IsCovidRequestExists(ByVal username As String)
        Dim conn As String = ConnectionManger.GetOASISConnectionString
        Dim param(5) As SqlParameter
        param(0) = New SqlParameter("@USR_NAME", username)
        param(1) = New SqlParameter("@OPTION", 2)
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "ONLINE.CHECK_COVID_RELIEF_V2", param)
        If ds.Tables(0).Rows.Count > 0 Then

            If Convert.ToString(ds.Tables(0).Rows(0)("Active")) = "1" Then
                divlanding.Visible = True
                divExisting.Visible = False
                divApplyNew.Visible = False
                divFeePEnding.Visible = False
            ElseIf Convert.ToString(ds.Tables(0).Rows(0)("Active")) = "2" Then
                divExisting.Visible = True
                divlanding.Visible = False
                divApplyNew.Visible = False
                divFeePEnding.Visible = False
                Dim rrhId As String = String.Empty
                If Convert.ToString(ds.Tables(0).Rows(0)("STATUS")) <> "" Then

                    rrhId = encr_decr.Encrypt(Convert.ToString(ds.Tables(0).Rows(0)("STATUS")))
                    Dim strurl As String = String.Empty
                    strurl = "https://oasis.gemseducation.com/covid/CovidReliefForm_Det.aspx?RRHID=" & rrhId
                    'lnkPending.PostBackUrl = "CovidReliefForm_Det.aspx?RRHID=" & rrhId
                    lnkPending.Visible = False
                    lblExisting.Text = "We have reviewed your application, kindly  <a href='" & strurl & "'><b>click here</b></a> to complete the pending informations."


                End If

            ElseIf Convert.ToString(ds.Tables(0).Rows(0)("Active")) = "3" Then
                divExisting.Visible = True
                divlanding.Visible = False
                lnkPending.Visible = False
                divApplyNew.Visible = False
                divFeePEnding.Visible = False
                If Convert.ToString(ds.Tables(0).Rows(0)("STATUS")) <> "" Then
                    lblExisting.Text = Convert.ToString(ds.Tables(0).Rows(0)("STATUS"))
                Else
                    lblExisting.Text = "You have already submitted the application and it is under processing."

                End If

            ElseIf Convert.ToString(ds.Tables(0).Rows(0)("Active")) = "4" Or Convert.ToString(ds.Tables(0).Rows(0)("Active")) = "5" Then
                divExisting.Visible = False
                divlanding.Visible = False
                lnkPending.Visible = False
                divApplyNew.Visible = True
                divFeePEnding.Visible = False

                ViewState("hdnStatus") = Convert.ToString(ds.Tables(0).Rows(0)("Active"))
                lblFeePending.Text = Convert.ToString(ds.Tables(0).Rows(0)("STATUS"))
                'If Convert.ToString(ds.Tables(0).Rows(0)("STATUS")) <> "" Then
                '    lblApplyNew.Text = Convert.ToString(ds.Tables(0).Rows(0)("STATUS"))
                'Else
                '    lblApplyNew.Text = "Do you want to apply for releif " & lnkYesToReleif.Text & "/" & lnkNoToReleif.Text

                'End If

            ElseIf Convert.ToString(ds.Tables(0).Rows(0)("Active")) = "5" Then
                divExisting.Visible = False
                divlanding.Visible = False
                lnkPending.Visible = False
                divApplyNew.Visible = False
                divFeePEnding.Visible = True
                lblFeePending.Text = Convert.ToString(ds.Tables(0).Rows(0)("STATUS"))
                'If Convert.ToString(ds.Tables(0).Rows(0)("STATUS")) <> "" Then
                '    lblExisting.Text = "Kindly clear your fee dues " & Convert.ToString(ds.Tables(0).Rows(0)("STATUS"))
                '    lblFeePending.Text = "Kindly clear your fee dues " & Convert.ToString(ds.Tables(0).Rows(0)("STATUS"))
                'Else
                '    lblFeePending.Text = "Kindly clear your fee dues " & lnkPayFees.Text & "/" & lnkPayLater.Text

                'End If
                ''You are already submitted the application. A member of school staff would contact you for further processing.
            End If
        Else
            divExisting.Visible = False
        End If

    End Sub

    Protected Sub lnkYesToReleif_Click(sender As Object, e As EventArgs)


        If (ViewState("hdnStatus") = "4") Then
            divExisting.Visible = False
            divlanding.Visible = True
            lnkPending.Visible = False
            divApplyNew.Visible = False
            divFeePEnding.Visible = False
            spAgree.Visible = True

            ''added on 16

            Session("validuser") = "1"
        ElseIf (ViewState("hdnStatus") = "5") Then

            'divExisting.Visible = False
            'divlanding.Visible = False
            'lnkPending.Visible = False
            'divApplyNew.Visible = False
            lnkYesToReleif.Visible = False
            divFeePEnding.Visible = True
            lnkPayFees.Visible = True
            spAgree.Visible = False
            'Dim conn As String = ConnectionManger.GetOASISConnectionString
            'Dim param(5) As SqlParameter
            'param(0) = New SqlParameter("@USR_NAME", Session("username"))
            'param(1) = New SqlParameter("@OPTION", 2)
            'Dim ds As New DataSet
            'ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "ONLINE.CHECK_COVID_RELIEF_V2", param)
            'If ds.Tables(0).Rows.Count > 1 Then
            '    gvFeeDetails.DataSource = ds.Tables(1)
            '    gvFeeDetails.DataBind()
            'End If
        End If

    End Sub

    Protected Sub lnkNoToReleif_Click(sender As Object, e As EventArgs)
        Response.Redirect("/Home.aspx", False)
    End Sub

    Protected Sub lnkPayFees_Click(sender As Object, e As EventArgs)
        Response.Redirect("/fees/FeeCollectionOnlineSibling.aspx", False)
    End Sub

    Protected Sub lnkPayLater_Click(sender As Object, e As EventArgs)
        Response.Redirect("/Home.aspx", False)
    End Sub
End Class
