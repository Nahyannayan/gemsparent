﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ParentMaster.master" AutoEventWireup="false" CodeFile="CovidReliefLanding.aspx.vb" Inherits="Covid_CovidReliefLanding" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphParent" runat="Server">
    <style>
        .btn:focus {
            background-color:#4cae4c !important;
        }
    </style>
    <div class="content margin-top30 margin-bottom60">
        <div class="container">
            <div class="row" id="divlanding" runat="server">



                <main class="relief-form mb-3" role="main">
        
        <section class="salary-details mt-3">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card rounded-0 h-100">
                            <div class="card-body p-0">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="section-title border-bottom p-3 m-0 text-medium bg-primary text-white" style="font-family:'open sans',helvetica,arial,sans-serif;background:#3772be !important;">
                                            <h3 class="text-center" style="font-family:'open sans',helvetica,arial,sans-serif;color:#fff;padding-top:10px;">GEMS COVID-19 Relief Package Application
                                            <a href="https://school.gemsoasis.com/oasisfiles/CovidReliefFAQ/GEMS_Covid_19_Relief_Package_FAQs.pdf?1=2" class="pull-right" style="color:white;padding-right:20px;" target="_blank">[ FAQ's ]
                                            </a></h3>
                                            
                                        </div>                                    
                                    </div>
                                </div>
                                <div class="row">                                    
                                    <div class="col-12 col-md-12">
                                        <div class="column-padding pt-3 pb-3" style="padding: 15px;">
                                            <p>The GEMS COVID-19 Relief Package has been established to help alleviate the adverse economic impact that COVID-19 &#8211; and the corresponding shut down &#8211; has had on GEMS families. </p>
                                            <p>The funds allocated to this program are intended for those directly financially affected, and as such, eligibility for such relief is limited to students who have had at least one (1) parent who has been terminated, laid off, furloughed, placed on unpaid leave, or received a pay cut since 1st February 2020 or is self-employed. By completing this application, you hereby confirm that we may, if required, contact your employer to verify this change in your employment status.</p>
                                            <p>Any award of relief will be strictly conditional on all outstanding fees up from previous terms and for the remainder of the fees for the current term owed, post-relief, being fully paid.</p>
                                            <p>You are not required to apply for relief or to provide any information requested here. However, to be eligible for relief, you must complete the information in its entirety.</p>
                                            <p>Please note that a completed application does not entitle any applicant to relief, and that GEMS reserves the right, in its sole discretion, without ascribing any reason, to approve or reject any relief of any kind.</p>
                                            <p>Any relief granted will only be applicable for current term of 2020.</p>
                                            <p>We note that we treat your privacy very seriously, and we will hold your information in strict confidence. We will only use the information to process your application, and otherwise will not disclose such information to third parties outside of the GEMS group of companies  unless required by applicable law or regulation. You hereby consent to our collection, storing and processing of the data that you submit in this application in accordance with the  GEMS privacy Policy <a href="https://www.gemseducation.com/privacy-policy" target="_blank">https://www.gemseducation.com/privacy-policy</a>  .  You are entitled to withdraw your consent by contacting us at<a href="#"> privacy@gemseducation.com</a> , but by doing so you agree to forfeit any discount you have been provided on and from the date that the consent is withdrawn.</p>
                                        </div>
                                    </div>
                                </div>
                            <%--    <div class="row">
                                    <div class="col-12">
                                        <div class="column-padding pt-3 pb-3 bg-light" id="mainDiv" style="margin-top:-25px">
                                            <div class="custom-inline" id="mainDiv2" style="padding-top:25px;padding-bottom:20px;" >
                                                <input class="custom-control-input" id="paymentPlanOption" onclick="checkBoxFun();" type="checkbox"/>
                                                <label class="custom-control-label" for="paymentPlanOption"  style="padding-left:30px;padding-right:30px;">I hereby agree to keep the results of your application and any relief granted or withheld in strict confidence, and I agree that failure to maintain such confidence may result in the withdrawal of any relief provided.</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>--%>
                                <div class="row">
                                 <div class="col-1 col-xs-1"></div>
                                    <div class="col-xs-10 col-10">
                                        <div class="column-padding pt-3 pb-3 bg-light" id="mainDiv" style="margin-top: -25px; border: solid rgb(248, 249, 250);">
                                            <div class="custom-control custom-checkbox mb-3 custom-control-inline mr-0 mr-md-5" id="mainDiv2" style="padding: 10px;">
                                                <input class="custom-control-input pos-absolute" id="paymentPlanOption" onclick="checkBoxFun();" type="checkbox">
                                                <label class="custom-control-label" for="paymentPlanOption" style="padding-left: 30px;padding-right: 0px;">I hereby agree to keep the results of your application and any relief granted or withheld in strict confidence, and I agree that failure to maintain such confidence may result in the withdrawal of any relief provided.</label>
                                            </div>
                                        </div>
                                    </div>
                                 <div class="col-1 col-xs-1"></div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="column-padding pt-3 pb-3">
                                        <center>
                                            <button class="btn btn-success text-center w-80" id="btnSubmit" onclick="confirmFun();" style="background: #28a745;padding: 5px 12px;font-weight: 100;padding: 100;" type="button">CONFIRM AGREEMENT WITH ALL TERMS OUTLINED ABOVE</button>
                                        </center>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
        

    </main>
            </div>

            <div class="row" id="divExisting" runat="server">

                <!-- Posts Block -->
                <div class="posts-block col-lg-12 col-md-12 col-sm-12 col-xs-12">
                   
                   <div id="divCovid" runat="server" >
                     <h3 class="text-center" style="font-family:'open sans',helvetica,arial,sans-serif">GEMS Covid-19 Relief Package</h3>
                       <%-- <p style="font-size: 130%;">You can apply from tomorrow morning 9.00 AM <strong>UAE time</strong> Wednesday 8th April, if you have lost your job, been put on unpaid leave, suffered a salary cut or are self-employed and receive immediate tuition fee relief.</p>--%>
                   
                        <p style="font-size: 130%;line-height:3rem;"><asp:Label ID="lblExisting" runat="server"></asp:Label>

                            <asp:LinkButton ID="lnkPending" runat="server" Text="Here"></asp:LinkButton>
                        </p>
                        </div>
                </div>
                <!-- /Posts Block -->
            </div>

            <div class="row" id="divApplyNew" runat="server">

                <!-- Posts Block -->
                <div class="posts-block col-lg-12 col-md-12 col-sm-12 col-xs-12">
                   
                   <div id="div2" runat="server" >
                     <h3 class="text-center" style="font-family:'open sans',helvetica,arial,sans-serif">GEMS Covid-19 Relief Package</h3>
                       <p style="font-size: 130%;line-height:3rem;">Relief Package applications are now closed following the deadline of Wednesday 15th April. 
We are pleased that we are able to provide relief to over 20,000 of our students, supporting our families who have been directly impacted by the Covid-19 outbreak.</p>

 <p style="font-size: 130%;line-height:3rem;">

An extended deadline of 31 May 2020 is available for those who suffer a salary cut, unpaid leave or job loss after 15 April 2020.    All fees due should have been cleared before application is submitted. If you are eligible and are awarded relief, the discounted fee amount will be applied towards next term fees. Relief amount is subject to approvals.</p>

                <span id="spAgree" runat="server">   <p style="font-size: 130%;line-height:3rem;">If you agree to the above kindly click the <b>Apply</b> button to proceed</p></span>
                        <p style="font-size: 130%; line-height:3rem;"><asp:Label ID="lblApplyNew" runat="server"></asp:Label></p>

                            <div class="text-center" ><asp:LinkButton ID="lnkYesToReleif" runat="server" Text="Apply" OnClick="lnkYesToReleif_Click" CssClass="btn btn-success text-center " style="font-size: 130%;"></asp:LinkButton></div>
                             <asp:LinkButton ID="lnkNoToReleif" runat="server" Text="NO" OnClick="lnkNoToReleif_Click" Visible="false"></asp:LinkButton>
                        
                        </div>
                </div>
                <!-- /Posts Block -->
            </div>

              <div class="row" id="divFeePEnding" runat="server">

                <!-- Posts Block -->
                <div class="posts-block col-lg-12 col-md-12 col-sm-12 col-xs-12">
                   
                   <div id="div3" runat="server" >
                       <p style="font-size: 130%;line-height:3rem;"><asp:Label ID="lblFeePending" runat="server"></asp:Label></p>

                            <div class="text-center" ><asp:LinkButton ID="lnkPayFees" runat="server" Text="Proceed to payment" OnClick="lnkPayFees_Click" Visible="false" CssClass="btn btn-success text-center" style="font-size: 130%;"></asp:LinkButton></div>
                             <asp:LinkButton ID="lnkPayLater" runat="server" Text="NO" OnClick="lnkPayLater_Click" Visible="false" ></asp:LinkButton>
                        

                       <%--<asp:GridView ID="gvFeeDetails" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                                            EmptyDataText="No Records Available"
                                                            PageSize="15" Width="100%"
                                                            CssClass="table table-striped table-bordered table-responsive text-left my-orders-table" BorderStyle="None" BorderWidth="0px"
                                                            HeaderStyle-HorizontalAlign="Center">
                                                            <EmptyDataRowStyle Wrap="True" HorizontalAlign="Center" />
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="STUNO" Visible="False">
                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                    <ItemTemplate>
                                                                        
                                                                        <asp:Label ID="lblStudent" runat="server" Text='<%# Eval("STU_NO")%>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Name">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblStudentName" runat="server" Text='<%# Eval("STUNAME")%>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                           <asp:TemplateField HeaderText="Amount">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblAmount" runat="server" Text='<%# Eval("Amount")%>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                              
                                                            </Columns>

                                                        </asp:GridView>--%>
                        </div>
                </div>
                <!-- /Posts Block -->
            </div>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <script>
        function confirmFun() {
            if (document.getElementById("paymentPlanOption").checked != true) {
                document.getElementById("mainDiv").style.border = "solid #3772be";
                document.getElementById("mainDiv2").style.background = "White !important";

            } else {
                window.location.href = "https://oasis.gemseducation.com/covid/CovidReliefForm_Det.aspx";
            }

        }
        function checkBoxFun() {
            if (document.getElementById("paymentPlanOption").checked == true) {
                document.getElementById("mainDiv").style.border = "solid #f8f9fa";
            }

        }
    </script>
</asp:Content>

