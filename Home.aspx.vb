﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Partial Class Home
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then

            If Not Session("username") Is Nothing Then
                bindChildInfo_Home(Session("STU_ID").ToString)
                bind_Info(Session("STU_ID").ToString)
                bind_classroom(Session("STU_ID").ToString)
                bind_covidform(Session("username"))
                bind_feepayplan(Session("STU_ID").ToString)
                Session("STU_NAME") = StrConv(Session("STU_NAME"), VbStrConv.ProperCase)
                lbChildName.Text = Session("STU_NAME")
                If Not Session("CLEV_MODULE") Is Nothing Then
                    CLEV_REDIRECT(Session("CLEV_MODULE"))
                End If

            Else
                Response.Redirect("~\Login.aspx", False)
            End If



        End If

    End Sub
    Sub CLEV_REDIRECT(ByVal modu As String)
        If modu = "DUESMS" Then
            Response.Redirect("/Fees/FeeCollectionOnlineSibling.aspx", False)
        ElseIf modu = "FEE" Then
            Response.Redirect("/Fees/FeeCollectionOnlineSibling.aspx", False)
        ElseIf modu = "CLM" Then
            Response.Redirect("/Curriculum/CurrReport.aspx?type=student", False)
        ElseIf modu = "OTH" Then
            Response.Redirect("/Others/Add_referral.aspx", False)
        ElseIf modu = "CRAF" Then
            '' Response.Redirect("/Others/CovidReliefForm.aspx", False)
            ' Response.Redirect("/Others/Covid19Gems.aspx", False)
            Response.Redirect("/covid/CovidReliefLanding.aspx", False)
        End If
    End Sub

    Public Sub bindChildInfo_Home(ByVal stu_id As String)
        Try

            'Dim strPath As String = Convert.ToString(readerStudent_Detail("STU_PHOTOPATH"))


            Dim con As String = ConnectionManger.GetOASISConnectionString
            Dim param(2) As SqlClient.SqlParameter

            param(0) = New SqlClient.SqlParameter("@STU_ID", stu_id)


            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "[ONLINE].[STUDENTDETAILS_new]", param)
            If ds.Tables(0).Rows.Count > 0 Then

                ltid.Text = Convert.ToString(ds.Tables(0).Rows(0)("StudNo"))
                Literal1.Text = Convert.ToString(ds.Tables(0).Rows(0)("DisplayGrade"))
                Literal2.Text = Convert.ToString(ds.Tables(0).Rows(0)("SCT_DESCR"))
                Literal4.Text = Convert.ToString(ds.Tables(0).Rows(0)("ENAME"))
                Literal5.Text = Convert.ToString(ds.Tables(0).Rows(0)("STU_PRIMARYCONTACT"))
                Literal6.Text = Convert.ToString(ds.Tables(0).Rows(0)("STU_PRIMARYCONTACT_name"))
                Literal7.Text = Convert.ToString(ds.Tables(0).Rows(0)("STU_PRIMARYCONTACT_email"))
                Literal8.Text = Convert.ToString(ds.Tables(0).Rows(0)("STU_PRIMARYCONTACT_mob"))
                Literal9.Text = Convert.ToString(ds.Tables(0).Rows(0)("STU_DOJ"))

                If Convert.ToString(ds.Tables(0).Rows(0)("STU_CURRSTATUS")) <> "EN" Then
                    lblStatus.Text = " (Student is not Active)"
                    lblStatus.Attributes.Add("class", "blink")
                Else
                    lblStatus.Text = ""
                    lblStatus.Attributes.Add("class", "")
                End If
                'Literal10.Text = Convert.ToString(ds.Tables(0).Rows(0)("FEE"))
            End If
        Catch ex As Exception

        End Try
    End Sub
    Public Sub bind_Info(ByVal stu_id As String)
        Dim conn As String = ConnectionManger.GetOASISConnectionString
        Dim param(5) As SqlParameter
        param(0) = New SqlParameter("@STU_ID", stu_id)

        Using datareader As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "ONLINE.ONLINE_GET_INFO_M", param)
            rptInfo.DataSource = datareader
            rptInfo.DataBind()
        End Using
    End Sub
    Public Sub bind_classroom(ByVal stu_id As String)
        Dim conn As String = ConnectionManger.GetOASISConnectionString
        Dim param(5) As SqlParameter
        param(0) = New SqlParameter("@STU_ID", stu_id)
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "ONLINE.CHECK_CLASSROOM_VALID_BSU", param)
        If ds.Tables(0).Rows.Count > 0 Then

            If Convert.ToString(ds.Tables(0).Rows(0)("Active")) = "1" Then
                divClassroom.Visible = True
            Else
                divClassroom.Visible = False
            End If
        Else
            divClassroom.Visible = False
        End If

    End Sub
    Public Sub bind_feepayplan(ByVal stu_id As String)
        Dim conn As String = ConnectionManger.GetOASISConnectionString
        Dim param(5) As SqlParameter
        param(0) = New SqlParameter("@STU_ID", stu_id)
        param(1) = New SqlParameter("@OLU_NAME", Session("username"))
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "ONLINE.CHECK_FEEPAYPLAN", param)
        If ds.Tables(0).Rows.Count > 0 Then

            If Convert.ToString(ds.Tables(0).Rows(0)("Active")) = "1" Then
                spfeepayplan.Visible = True
            Else
                spfeepayplan.Visible = False
            End If
        Else
            spfeepayplan.Visible = False
        End If

    End Sub

    Public Sub bind_covidform(ByVal username As String)
        Dim conn As String = ConnectionManger.GetOASISConnectionString
        Dim param(5) As SqlParameter
        param(0) = New SqlParameter("@USR_NAME", username)
        param(1) = New SqlParameter("@OPTION", 1)
        Dim ds As New DataSet

        'ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "ONLINE.CHECK_COVID_RELIEF_V2", param)
        'If ds.Tables(0).Rows.Count > 0 Then

        '    If Convert.ToString(ds.Tables(0).Rows(0)("Active")) = "1" Then
        '        spCovid.Visible = True
        '        Session("Covid_Exceptional") = "1"
        '    Else
        '        spCovid.Visible = False
        '        Session("Covid_Exceptional") = "0"
        '    End If
        'Else
        '    spCovid.Visible = False
        'End If

        ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "ONLINE.CHECK_ADHOC_LINKS_SHOW", param)
        If ds.Tables(0).Rows.Count > 0 Then

            If Convert.ToString(ds.Tables(0).Rows(0)("Active")) = "1" Then
                spCovid.Visible = True
                'Session("Covid_Exceptional") = "1"
            Else
                spCovid.Visible = False
                'Session("Covid_Exceptional") = "0"
            End If
        Else
            spCovid.Visible = False
        End If

    End Sub
End Class
