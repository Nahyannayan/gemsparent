﻿Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data.SqlTypes
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data
Partial Class Fees_feePaymentResultSibling
    Inherits System.Web.UI.Page
    Private Property bBSUTaxable() As Boolean
        Get
            Return ViewState("bBSUTaxable")
        End Get
        Set(ByVal value As Boolean)
            ViewState("bBSUTaxable") = value
        End Set
    End Property
    Dim debugData As String = String.Empty
    '
    'Version 3.1
    '
    '---------------- Disclaimer --------------------------------------------------
    '
    'Copyright 2004 Dialect Solutions Holdings.  All rights reserved.
    '
    'This document is provided by Dialect Holdings on the basis that you will treat
    'it as confidential.
    '
    'No part of this document may be reproduced or copied in any form by any means
    'without the written permission of Dialect Holdings.  Unless otherwise
    'expressly agreed in writing, the information contained in this document is
    'subject to change without notice and Dialect Holdings assumes no
    'responsibility for any alteration to, or any error or other deficiency, in
    'this document.
    '
    'All intellectual property rights in the Document and in all extracts and
    'things derived from any part of the Document are owned by Dialect and will be
    'assigned to Dialect on their creation. You will protect all the intellectual
    'property rights relating to the Document in a manner that is equal to the
    'protection you provide your own intellectual property.  You will notify
    'Dialect immediately, and in writing where you become aware of a breach of
    'Dialect's intellectual property rights in relation to the Document.
    '
    'The names "Dialect", "QSI Payments" and all similar words are trademarks of
    'Dialect Holdings and you must not use that name or any similar name.
    '
    'Dialect may at its sole discretion terminate the rights granted in this
    'document with immediate effect by notifying you in writing and you will
    'thereupon return (or destroy and certify that destruction to Dialect) all
    'copies and extracts of the Document in its possession or control.
    '
    'Dialect does not warrant the accuracy or completeness of the Document or its
    'content or its usefulness to you or your merchant customers.   To the extent
    'permitted by law, all conditions and warranties implied by law (whether as to
    'fitness for any particular purpose or otherwise) are excluded.  Where the
    'exclusion is not effective, Dialect limits its liability to $100 or the
    'resupply of the Document (at Dialect's option).
    '
    'Data used in examples and sample data files are intended to be fictional and
    'any resemblance to real persons or companies is entirely coincidental.
    '
    'Dialect does not indemnify you or any third party in relation to the content
    'or any use of the content as contemplated in these terms and conditions.
    '
    'Mention of any product not owned by Dialect does not constitute an endorsement
    'of that product.
    '
    'This document is governed by the laws of New South Wales, Australia and is
    'intended to be legally binding.
    '
    'Author: Dialect Solutions Group Pty Ltd
    '
    '------------------------------------------------------------------------------


    '
    '<summary>ASP.NET C# 3-Party example for the Virtual Payment Client</summary>
    '<remarks>
    '
    '<para>
    'This example assumes that a transaction GET response has been sent to this 
    'example from the Payment Server with the required fields via a cardholder's 
    'browser redirect. The example then checks the value of an MD5 signature to 
    'ensure the data has not been changed during transmission.
    '</para>
    '
    '<para>
    'The to instantiate the MD5 signature check, the MD5 seed must be saved in the 
    'SECURE_SECRET value which is first parameter in the PageLoad() class. The 
    'SECURE_SECRET value can be found in Merchant Administration/Setup page on the 
    'Payment Server.
    '</para>
    '
    '</remarks>
    '


    ' _____________________________________________________________________________

    ' Declare the global variables
    'private string debugData = "";

    ' _____________________________________________________________________________


    Private Function displayAVSResponse(ByVal vAVSResultCode As String) As String
        '
        '    <summary>Maps the vpc_AVSResultCode to a relevant description</summary>
        '    <param name="vAVSResultCode">The vpc_AVSResultCode returned by the transaction.</param>
        '    <returns>The corresponding description for the vpc_AVSResultCode.</returns>
        '  
        Dim result As String = "Unknown"

        If vAVSResultCode.Length > 0 Then
            If vAVSResultCode.Equals("Unsupported") Then
                result = "AVS not supported or there was no AVS data provided"
            Else
                Select Case vAVSResultCode
                    Case "X"
                        result = "Exact match - address and 9 digit ZIP/postal code"
                        Exit Select
                    Case "Y"
                        result = "Exact match - address and 5 digit ZIP/postal code"
                        Exit Select
                    Case "S"
                        result = "Service not supported or address not verified (international transaction)"
                        Exit Select
                    Case "G"
                        result = "Issuer does not participate in AVS (international transaction)"
                        Exit Select
                    Case "A"
                        result = "Address match only"
                        Exit Select
                    Case "W"
                        result = "9 digit ZIP/postal code matched, Address not Matched"
                        Exit Select
                    Case "Z"
                        result = "5 digit ZIP/postal code matched, Address not Matched"
                        Exit Select
                    Case "R"
                        result = "Issuer system is unavailable"
                        Exit Select
                    Case "U"
                        result = "Address unavailable or not verified"
                        Exit Select
                    Case "E"
                        result = "Address and ZIP/postal code not provided"
                        Exit Select
                    Case "N"
                        result = "Address and ZIP/postal code not matched"
                        Exit Select
                    Case "0"
                        result = "AVS not requested"
                        Exit Select
                    Case Else
                        result = "Unable to be determined"
                        Exit Select
                End Select
            End If
        End If
        Return result
    End Function

    Private Function displayCSCResponse(ByVal vCSCResultCode As String) As String        '
        '    <summary>Maps the vpc_CSCResultCode to a relevant description</summary>
        '    <param name="vCSCResultCode">The vpc_CSCResultCode returned by the transaction.</param>
        '    <returns>The corresponding description for the vpc_CSCResultCode.</returns>
        Dim result As String = "Unknown"
        If vCSCResultCode.Length > 0 Then
            If vCSCResultCode.Equals("Unsupported") Then
                result = "CSC not supported or there was no CSC data provided"
            Else

                Select Case vCSCResultCode
                    Case "M"
                        result = "Exact code match"
                        Exit Select
                    Case "S"
                        result = "Merchant has indicated that CSC is not present on the card (MOTO situation)"
                        Exit Select
                    Case "P"
                        result = "Code not processed"
                        Exit Select
                    Case "U"
                        result = "Card issuer is not registered and/or certified"
                        Exit Select
                    Case "N"
                        result = "Code invalid or not matched"
                        Exit Select
                    Case Else
                        result = "Unable to be determined"
                        Exit Select
                End Select
            End If
        End If
        Return result
    End Function

    '______________________________________________________________________________

    Private Function splitResponse(ByVal rawData As String) As System.Collections.Hashtable
        '
        '    * <summary>This function parses the content of the VPC response
        '    * <para>This function parses the content of the VPC response to extract the
        '    * individual parameter names and values. These names and values are then
        '    * returned as a Hashtable.</para>
        '    *
        '    * <para>The content returned by the VPC is a HTTP POST, so the content will
        '    * be in the format "parameter1=value&parameter2=value&parameter3=value".
        '    * i.e. key/value pairs separated by ampersands "&".</para>
        '    *
        '    * <param name="RawData"> data string containing the raw VPC response content
        '    * <returns> responseData - Hashtable containing the response data
        '    

        Dim responseData As New System.Collections.Hashtable()
        Try
            ' Check if there was a response containing parameters
            If rawData.IndexOf("=") > 0 Then
                ' Extract the key/value pairs for each parameter
                For Each pair As String In rawData.Split("&"c)
                    Dim equalsIndex As Integer = pair.IndexOf("=")
                    If equalsIndex > 1 AndAlso pair.Length > equalsIndex Then
                        Dim paramKey As String = System.Web.HttpUtility.UrlDecode(pair.Substring(0, equalsIndex))
                        Dim paramValue As String = System.Web.HttpUtility.UrlDecode(pair.Substring(equalsIndex + 1))
                        responseData.Add(paramKey, paramValue)
                    End If
                Next
            Else
                ' There were no parameters so create an error
                responseData.Add("vpc_Message", "The data contained in the response was not a valid receipt.<br/>" & vbLf & "The data was: <pre>" & rawData & "</pre><br/>" & vbLf)
            End If
            Return responseData
        Catch ex As Exception
            ' There was an exception so create an error
            responseData.Add("vpc_Message", (vbLf & "The was an exception parsing the response data.<br/>" & vbLf & "The data was: <pre>" & rawData & "</pre><br/>" & vbLf & "<br/>" & vbLf & "Exception: ") + ex.ToString() & "<br/>" & vbLf)
            Return responseData
        End Try
    End Function

    ' _________________________________________________________________________

   

    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("username") Is Nothing Then
            Session("Active_tab") = "Home"
            Session("Site_Path") = ""
            Response.Redirect("~\General\Home.aspx")
        ElseIf Session("bPasswdChanged") = "False" Then
            Response.Redirect("~\UpdateInfo\ForcePasswordChange.aspx", False)
        ElseIf Session("bUpdateContactDetails") = "False" Then
            Response.Redirect(Session("ForceUpdate_stud"), False)
        End If

        Dim SECURE_SECRET As String = FeeCollectionOnline.GetSECURE_SECRET(Session("sBsuid").ToString())
        Dim CPM_GATEWAY_TYPE As String = ""
        If Not Session("CPM_GATEWAY_TYPE") Is Nothing Then
            CPM_GATEWAY_TYPE = Session("CPM_GATEWAY_TYPE").ToString
        End If
        Panel_Debug.Visible = False
        Panel_Receipt.Visible = False
        Panel_StackTrace.Visible = False

        Dim message As String = ""
        Dim errorExists As Boolean = False
        Dim txnResponseCode As String = ""
        Dim MyPaymentGatewayClass As New PaymentGatewayClass(Session("CPS_ID"))

        Dim rawHashData As String = SECURE_SECRET
        MyPaymentGatewayClass.vpc_Amount = Session("vpc_Amount")
        MyPaymentGatewayClass.vpc_MerchantID = Session("vpc_OrderInfo")
        Dim PageResponseValues As System.Collections.Specialized.NameValueCollection
        If MyPaymentGatewayClass.ResponseType = "POST" Then
            PageResponseValues = Request.Form
        Else
            PageResponseValues = Page.Request.QueryString
        End If
        MyPaymentGatewayClass.SetResponseCodeFromPaymentGateway(PageResponseValues)
        ' Initialise the Local Variables
        Label_HashValidation.Text = "<font color='orange'><b>NOT CALCULATED</b></font>"
        Dim hashValidated As Boolean = True

        Try
#If DEBUG Then
    'lblError.Text=MyPaymentGatewayClass.getParameterValues
    debugData += "<br/><u>Start of Debug Data</u><br/><br/>"
#End If
            Dim signature As String = ""
            If MyPaymentGatewayClass.vpc_SecureHashCode.Length > 0 Then
                ' collect debug information
#If DEBUG Then
            debugData += "<u>Data from Payment Server</u><br/>"
#End If
                If MyPaymentGatewayClass.CPM_GATEWAY_TYPE = "MIGS" Then
                    rawHashData = ""
                    Dim seperator As String = ""
                    For Each item As String In Page.Request.QueryString
                        If Not item.Equals("vpc_SecureHash") AndAlso Not item.Equals("vpc_SecureHashType") Then
                            If item.StartsWith("vpc_") Or item.StartsWith("user_") Then
                                rawHashData &= seperator & item & "=" & null2unknown(Page.Request.QueryString(item))
                                seperator = "&"
                            End If
                        End If
#If DEBUG Then
                debugData += (item & "=") + PageResponseValues(item) & "<br/>"
#End If
                    Next
                    If SECURE_SECRET.Length > 0 Then
                        signature = MyPaymentGatewayClass.CreateMIGS_SHA256Signature(rawHashData, SECURE_SECRET)
                    End If
                ElseIf MyPaymentGatewayClass.CPM_GATEWAY_TYPE = "EGHL" Or CPM_GATEWAY_TYPE = "EGHL" Then
                    rawHashData = MyPaymentGatewayClass.GetPageResponseValues
                Else
                    For Each item As String In PageResponseValues
                        ' collect debug information
#If DEBUG Then
                debugData += (item & "=") + PageResponseValues(item) & "<br/>"
#End If

                        If SECURE_SECRET.Length > 0 AndAlso Not item.Equals(MyPaymentGatewayClass.MySecueHashCodeName) Then
                            rawHashData += Mainclass.cleanString(PageResponseValues(item))
                        End If
                    Next
                    If SECURE_SECRET.Length > 0 Then
                        signature = MyPaymentGatewayClass.CreateMD5Signature(rawHashData)
                    End If
                End If
            End If

            If SECURE_SECRET.Length > 0 Then
                ' Collect debug information
#If DEBUG Then
            debugData += ("<br/><u>Hash Data Input</u>: " & rawHashData & "<br/><br/><u>Signature Created</u>: ") + signature & "<br/>"
#End If
                ' Validate the Secure Hash
                If MyPaymentGatewayClass.vpc_SecureHashCode.Equals(signature) Then
                    Label_HashValidation.Text = "<font color='#00AA00'><b>CORRECT</b></font>"
                Else
                    Label_HashValidation.Text = "<font color='#FF0066'><b>INVALID HASH</b></font>"
                    hashValidated = False
                End If
            End If
            ' Get the standard receipt data from the parsed response
            txnResponseCode = IIf(MyPaymentGatewayClass.vpc_TxnResponseCode.Length > 0, MyPaymentGatewayClass.vpc_TxnResponseCode, "Unknown")
            Label_TxnResponseCode.Text = txnResponseCode
            Label_TxnResponseCodeDesc.Text = MyPaymentGatewayClass.vpc_TxnResponseCodeDescr   'MyPaymentGatewayClass.getResponseDescription(txnResponseCode)
#If DEBUG Then
            debugData += ("<br/><u>Hash Data Input</u>: " & rawHashData & "<br/><br/><u>Signature Created</u>: ") + signature & "<br/>"
            'lblMessage.Text &= MyPaymentGatewayClass.getParameterValues
#End If

            Label_Command.Text = IIf(MyPaymentGatewayClass.vpc_Command.ToString.Length > 0, MyPaymentGatewayClass.vpc_Command.ToString, "Unknown")
            Label_Version.Text = IIf(MyPaymentGatewayClass.vpc_Version.ToString.Length > 0, MyPaymentGatewayClass.vpc_Version.ToString, "Unknown")
            Label_OrderInfo.Text = IIf(MyPaymentGatewayClass.vpc_OrderInfo.ToString.Length > 0, MyPaymentGatewayClass.vpc_OrderInfo.ToString, "Unknown")
            Label_MerchantID.Text = IIf(MyPaymentGatewayClass.vpc_MerchantID.ToString.Length > 0, MyPaymentGatewayClass.vpc_MerchantID.ToString, "Unknown")
            Dim recno As String = "", msgServer As String = "", retval As String = "", MerchTxnRef As String = ""
            Try
                If Not Session("vpc_MerchTxnRef") Is Nothing AndAlso Convert.ToString(Session("vpc_MerchTxnRef")) <> "" Then
                    MerchTxnRef = Convert.ToString(Session("vpc_MerchTxnRef"))
                End If
            Catch ex As Exception
                MerchTxnRef = ""
            End Try
            If MyPaymentGatewayClass.vpc_MerchTxnRef.ToString = MerchTxnRef Then
                retval = FeeCollectionOnline.F_SaveFEECOLLECTION_H_ONLINE_PAYMENT_FEES_MULTI(MerchTxnRef, _
                          MyPaymentGatewayClass.vpc_TxnResponseCode.ToString, MyPaymentGatewayClass.vpc_TxnResponseCodeDescr.ToString, MyPaymentGatewayClass.vpc_Message.ToString, MyPaymentGatewayClass.vpc_ReceiptNo.ToString, MyPaymentGatewayClass.vpc_TransactionNo.ToString, _
                          MyPaymentGatewayClass.vpc_AcqResponseCode.ToString, MyPaymentGatewayClass.vpc_AuthorizeID.ToString, MyPaymentGatewayClass.vpc_BatchNo.ToString, MyPaymentGatewayClass.vpc_CardType.ToString, hashValidated.ToString(), MyPaymentGatewayClass.vpc_Amount.ToString, _
                          MyPaymentGatewayClass.vpc_OrderInfo.ToString, MyPaymentGatewayClass.vpc_MerchantID.ToString, MyPaymentGatewayClass.vpc_Command.ToString, MyPaymentGatewayClass.vpc_Version.ToString, _
                          MyPaymentGatewayClass.vpc_VerType.ToString & "|" + MyPaymentGatewayClass.vpc_VerStatus.ToString.ToString & "|" + MyPaymentGatewayClass.vpc_Token.ToString & "|" + MyPaymentGatewayClass.vpc_VerSecurLevel.ToString & "|" + MyPaymentGatewayClass.vpc_Enrolled.ToString & "|" + MyPaymentGatewayClass.vpc_Xid.ToString & "|" + MyPaymentGatewayClass.vpc_AcqECI.ToString & "|" + MyPaymentGatewayClass.vpc_AuthStatus.ToString, _
                          recno, msgServer)
            End If
            Try
                FeeCollectionOnline.SAVE_ONLINE_PAYMENT_AUDIT("FEES", "RESPONSE", _
               Microsoft.Security.Application.Encoder.UrlEncode(Page.Request.Url.ToString), Session("vpc_MerchTxnRef").ToString())
            Catch ex As Exception
                'lblMessage.Text &= "ex.message - " & ex.Message
            End Try
            bBSUTaxable = DirectCast(SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT ISNULL(BUS_bFEE_TAXABLE,0) FROM dbo.BUSINESSUNIT_SUB WHERE BUS_BSU_ID='" & Session("sBsuid") & "'"), Boolean)
            hfTaxable.Value = IIf(bBSUTaxable, "1", "0")
            If retval = 0 And recno.Trim <> "" Then
                'lblError.CssClass = "divsuccess"
                lblError.CssClass = "alert alert-success"
                urcStudentPaidResult1.Gridbind_PayDetails(Session("vpc_MerchTxnRef").ToString, True)
                Dim EmlStatus As String = "" 'SendEmail(recno, Session("vpc_MerchTxnRef").ToString)
                lblError.Text = IIf(EmlStatus.Trim <> "", EmlStatus.Trim & "</br>", "")
                lblError.CssClass = "alert alert-success"
                lblError.Text = msgServer.ToString + " - Reference No:" + MyPaymentGatewayClass.vpc_MerchTxnRef
            Else
                'lblMessage.CssClass = "diverrorPopUp"
                If MyPaymentGatewayClass.vpc_TxnResponseCodeDescr.ToString <> "" Then
                    lblMessage.CssClass = "text-danger"
                End If

                'lblMessage.Text = MyPaymentGatewayClass.vpc_TxnResponseCodeDescr.ToString
                lblMessage.Text = ""
                urcStudentPaidResult1.Gridbind_PayDetails(Session("vpc_MerchTxnRef").ToString, False)
                lblError.CssClass = "alert alert-danger"
                'lblError.Text &= msgServer.ToString
                lblError.Text = MyPaymentGatewayClass.vpc_TxnResponseCodeDescr.ToString + " - Reference No:" + MyPaymentGatewayClass.vpc_MerchTxnRef 'error not showing only shows "Transaction Cancelled" message
            End If
            Session("vpc_MerchTxnRef") = ""
            'lblError.CssClass = "alert alert-warning"
            'lblError.Text &= msgServer.ToString
            Label_MerchTxnRef.Text = MyPaymentGatewayClass.vpc_MerchTxnRef
            Dim Encr_decrData As New Encryption64
            lblDate.Text = Format(Date.Now, "dd/MMM/yyyy")
            If message.Length = 0 Then
                message = IIf(MyPaymentGatewayClass.vpc_Message.ToString.Length > 0, MyPaymentGatewayClass.vpc_Message.ToString, "Unknown")
            End If
        Catch ex As Exception
            message = "(51) Exception encountered. " & ex.Message
            UtilityObj.Errorlog(ex.Message)
            UtilityObj.Errorlog(ex.ToString())
            If ex.StackTrace.Length > 0 Then
                Label_StackTrace.Text = ex.ToString()
                Panel_StackTrace.Visible = True
            End If
            errorExists = True
            'lblError.CssClass = "diverrorPopUp"
            lblError.CssClass = "alert alert-warning"
            lblError.Text = ex.Message
        End Try
        ' output the message field
        Label_Message.Text = message.ToString
        ' Create a link to the example's HTML order page
        Label_AgainLink.Text = "<a href=""" & MyPaymentGatewayClass.AgainLink & """>Another Transaction</a>"
        ' Determine the appropriate title for the receipt page
        Label_Title.Text = IIf((errorExists OrElse txnResponseCode.Equals("7") OrElse txnResponseCode.Equals("Unknown") OrElse hashValidated = False), MyPaymentGatewayClass.Title & " Error Page", MyPaymentGatewayClass.Title & " Receipt Page")
        MyPaymentGatewayClass = Nothing
        ' output debug data to the screen
#If DEBUG Then
    debugData += "<br/><u>End of debug information</u><br/>"
    'Label_Debug.Text = debugData
#End If
        Panel_Debug.Visible = True
    End Sub
    Private Function SendEmail(ByVal RecNo As String, ByVal FCO_ID As Long) As String
        SendEmail = ""
        'UtilityObj.Errorlog("SendEmailFn, RecNo=" & RecNo & ",BSU_ID=" & Session("sBsuid"), "GEMSPARENT")
        Dim rs As New CrystalDecisions.Web.CrystalReportSource
        Dim RptFile As String
        Dim str As String = "SELECT ISNULL(FCL_FCL_ID,0)AS FCL_FCL_ID FROM FEES.FEECOLLECTION_H WITH ( NOLOCK ) WHERE FCL_RECNO='" & RecNo & "' AND FCL_BSU_ID='" & Session("sBsuid") & "'"
        Dim FCL_FCL_ID As Long = SqlHelper.ExecuteScalar(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, str)
        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim param As New Hashtable
        Dim rptClass As New rptClass
        param.Add("UserName", "SYSTEM")
        param.Add("IMG_TYPE", "LOGO")
        param.Add("@FCL_FCL_ID", FCL_FCL_ID)
        If bBSUTaxable Then
            RptFile = Server.MapPath("../Reports/FeeReceipt/rptFeeReceipt_Tax.rpt")
        Else
            RptFile = Server.MapPath("../Reports/FeeReceipt/rptFeeReceipt.rpt")
        End If
        'UtilityObj.Errorlog("reportPath:" & RptFile, "GEMSPARENT")
        rptClass.reportPath = RptFile
        rptClass.reportParameters = param
        rptClass.crDatabase = ConnectionManger.GetOASIS_FEESConnection.Database '= "OASIS_FEES"
        Dim rptDownload As New DownloadEmailReceipt
        rptDownload.LogoPath = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT ISNULL(BUS_BSU_GROUP_LOGO,'https://oasis.gemseducation.com/Images/Misc/TransparentLOGO.gif')GROUP_LOGO FROM dbo.BUSINESSUNIT_SUB WHERE BUS_BSU_ID='" & Session("sBsuid") & "'")
        rptDownload.BSU_ID = Session("sBsuId")
        rptDownload.FCL_FCL_ID = FCL_FCL_ID
        rptDownload.FCO_ID = FCO_ID
        'UtilityObj.Errorlog("BeforeLoadReports()", "GEMSPARENT")
        rptDownload.LoadReports(rptClass, rs)
        SendEmail = rptDownload.EmailStatus
        rptDownload = Nothing
    End Function
    Private Shared Function null2unknown(ByVal req As Object) As String
        Try
            If req Is Nothing Then
                Return String.Empty
            Else
                Return Microsoft.Security.Application.Encoder.HtmlEncode(req.ToString())
            End If
        Catch ex As Exception
            Return String.Empty
        End Try
    End Function
End Class

