﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ParentMaster.master" AutoEventWireup="false" CodeFile="RewardsRedemption.aspx.vb" Inherits="Fees_RewardsRedemption" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphParent" runat="Server">
    <script type="text/javascript" src="../Scripts/jquery-1.10.2.min.js"></script>
    <link href="../Scripts/JQuery-ui-1.10.3.css" rel="stylesheet" />
    <script type="text/javascript" src="../Scripts/Fancybox/jquery.fancybox.js?v=2.1.5"></script>
    <script type="text/javascript" src="../Scripts/Fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
    <link rel="stylesheet" href="../Scripts/Fancybox/jquery.fancybox.css" type="text/css"
        media="screen" />
    <script type="text/javascript" src="../Scripts/PopupJQuery.js"></script>
    <link href="../CSS/Popup.css" rel="stylesheet" />

    <div class="content margin-top30 margin-bottom60">
                    <div class="container">
                        <div class="row">
    <!-- Posts Block -->
    
    <div class="posts-block col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="my-account">
            
            <div class="bottom-padding">
                <div class="title-box">
                    <h3> GEMS Rewards Points Redemption 
                        <span class="profile-right">
                                <asp:Label ID="lbChildNameTop" runat="server"></asp:Label>
                            </span>
                    </h3>
                </div>
                <!-- Table  -->
                <div class="table-responsive">
                    

<%--    <div class="mainheading">
        <div class="left">
            GEMS Rewards Points Redemption
        </div>
    </div>--%>
    <table align="center" cellpadding="0" cellspacing="0" style="width: 100%;" class="table table-striped table-bordered table-responsive text-left my-orders-table">
        <tr>
            <td align="center" colspan="2">
                <asp:Image ID="imgEmpImage" runat="server" ImageUrl="~/Images/Fees/GEMS REWARDS.JPG" Width="100%" Height="253px" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Label ID="lblError" runat="server" width="100%" EnableViewState="False"></asp:Label>
            </td>
        </tr>
        <tr>
            <th align="left" class="tdfields" style="width: 25%">Registered Email Id
            </th>
            <td align="left">
                <asp:Label ID="lblEmailId" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <th align="left" class="tdfields">Points Summary</th>
            <td align="left">
                <table class="table table-striped table-bordered table-responsive text-left my-orders-table">
                    <tr>
                        <td align="center" class="tdfields">Available Points</td>
                        <td align="center" class="tdfields">Redeemable Points</td>
                        <td align="center" class="tdfields">Redeemable Amount <%= IIf(Session("BSU_CURRENCY") Is Nothing, "", "(" & Session("BSU_CURRENCY") & ")")%></td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Label ID="lblAvailablePoints" runat="server" Text="0.00"></asp:Label></td>
                        <td align="center">
                            <asp:Label ID="lblRedeemablePoints" runat="server" Text="0.00"></asp:Label></td>
                        <td align="center">
                            <asp:Label ID="lblRedeemableAmt" runat="server" Text="0.00"></asp:Label></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="left" class="tdfields">&nbsp;</td>
            <td align="left">Click
                <asp:LinkButton ID="lbtnHistory" runat="server" Font-Bold="True"  Font-Underline="True"
                    Text="here">
                </asp:LinkButton>
                to view Redemption history</td>
        </tr>
        <tr class="trSub_Header">
            <th align="left" colspan="2">Fee Details</th>
        </tr>
          <tr style="display:none;" visible="false"><td colspan="2">        </td>
                            </tr>
        <tr>
            <td align="left" colspan="2">
                <table cellpadding="0" cellspacing="0" border="0" width="100%" >
                    <tr>
                        <td align="center" valign="bottom">
                            <asp:Repeater ID="repInfo" runat="server">
                                <HeaderTemplate>
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding: 0;">
                                </HeaderTemplate>
                                <ItemTemplate>
                                     <tr><td colspan="3">&nbsp;</td></tr>
                                    <tr class="trSub_Header_Small">
                                        <th colspan="2">
                                            <asp:Label ID="lbSName" runat="server" Text='<%# Bind("SNAME") %>'></asp:Label>&nbsp;[<asp:Label ID="lbSNo" runat="server" Text='<%# Bind("STU_NO")%>'></asp:Label>]
                                        </th>
                                        <th align="right"  class="text-right">
                                            <div style="clear: both;">
                                                <asp:Label ID="lbGrade" runat="server" Text='<%# Bind("GRM_DISPLAY")%>'></asp:Label>-<asp:Label ID="lbSection" runat="server" Text='<%# Bind("SCT_DESCR")%>'></asp:Label>&nbsp;-
                                                <asp:LinkButton ID="lnkViewFeeSetup" Text="View Fee Schedule" CssClass="linkFee small_text" runat="server"></asp:LinkButton>
                                                <asp:HiddenField ID="hfSTU_ID" Value='<%# Bind("STU_ID") %>' runat="server" />
                                                <asp:HiddenField ID="hfSTU_FEE_ID" Value='<%# Bind("STU_FEE_ID")%>' runat="server" />
                                                <asp:HiddenField ID="hfSTU_ACD_ID" Value='<%# Bind("STU_ACD_ID")%>' runat="server" />
                                                <asp:HiddenField ID="hfSTU_BSU_ID" Value='<%# Bind("STU_BSU_ID")%>' runat="server" />
                                            </div>
                                        </th>
                                    </tr>
                                     <tr><td colspan="3">&nbsp;</td></tr>
                                    <tr id="trgvrerror" runat="server" visible="false">
                                        <td colspan="3" align="center">
                                            <asp:Label ID="lblGvrError" CssClass="linkText" Text="" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr align="left" style="border: none 0px !important;">
                                        <td valign="top" colspan="3" align="left">
                                            <table cellpadding="0" width="100%" cellspacing="0" border="0" 
                                                style="border-collapse: collapse !important; border-spacing: 0 !important; padding: 0 !important; border: none 0px !important;">
                                                <tr>
                                                    <td valign="top" align="left" width="10%">  <%--style="width: 62px;"--%>
                                                        <asp:Image ID="imgEmpImage" runat="server" Height="80px" ImageUrl='<%# Bind("PHOTOPATH") %>' ToolTip='<%# Bind("SNAME") %>'
                                                            Width="60px" />
                                                    </td>
                                                    <td valign="top"  width="90%">
                                                        <asp:GridView ID="gvFee" runat="server" AutoGenerateColumns="False" EmptyDataText="No Details" class="table table-striped table-bordered table-responsive text-left my-orders-table"
                                                            Style="border: 1pt  !important;" Width="100%" OnRowDataBound="gvFeeCollection_RowDataBound" ShowFooter="true">
                                                            <RowStyle Height="20px" VerticalAlign="Top" />
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="FSR_FEE_ID" Visible="False">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblFSR_FEE_ID" runat="server" Text='<%# Bind("FEE_ID") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Fee">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblFeeDescr" runat="server" Text='<%# Bind("FEE_DESCR") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                    <FooterTemplate>
                                                                        <asp:Label ID="lblTotals" runat="server" Text='TOTAL'></asp:Label>
                                                                    </FooterTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Due">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblAmount" runat="server" Text='<%# Bind("CLOSING", "{0:0.00}") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                    <FooterTemplate>
                                                                        <asp:Label ID="lblAmountF" runat="server" Text=""></asp:Label>
                                                                    </FooterTemplate>
                                                                    <ItemStyle HorizontalAlign="Right" Width="89px" />
                                                                    <FooterStyle HorizontalAlign="Right" />
                                                                    <HeaderStyle HorizontalAlign="Right" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Discount" Visible="false">
                                                                    <ItemTemplate>
                                                                        <span class="ui-icon ui-icon-info" id="spanInfo" runat="server" visible="false" style="float: left; margin: 0 15px 10px 0;"></span>
                                                                        <ajaxToolkit:HoverMenuExtender ID="pceDiscountDetail" runat="server" PopupControlID="pnlDiscntDtl"
                                                                            PopupPosition="Top" TargetControlID="spanInfo" OffsetX="-300">
                                                                        </ajaxToolkit:HoverMenuExtender>
                                                                        <asp:Label ID="lblDiscount" runat="server" Text="0.00"></asp:Label>
                                                                        <asp:Panel ID="pnlDiscntDtl" Style="display: none" runat="server">
                                                                            <table style="border: 1pt solid navy !important; width: 400px;" cellpadding="0" cellspacing="0" align="center">
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:Repeater ID="gvDiscDt" runat="server" EnableViewState="false">
                                                                                            <HeaderTemplate>
                                                                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                                                    <tr class="trSub_Header_Small">
                                                                                                        <th style="width: 250px">Month
                                                                                                        </th>
                                                                                                        <th style="width: 20%">Fee
                                                                                                        </th>
                                                                                                        <th style="width: 20%">Disc(%)
                                                                                                        </th>
                                                                                                        <th style="width: 20%">Discount
                                                                                                        </th>
                                                                                                        <th style="width: 20%">Net
                                                                                                        </th>
                                                                                                    </tr>
                                                                                            </HeaderTemplate>
                                                                                            <AlternatingItemTemplate>
                                                                                                <tr style="background-color: #DFE0E5; color: #1B80B6; border: 1pt; border-color: #1b80b6;">
                                                                                                    <td align="left">
                                                                                                        <%#Container.DataItem("MONTH")%>
                                                                                                    </td>
                                                                                                    <td align="right">
                                                                                                        <%# Math.Round(Convert.ToDecimal(Container.DataItem("FEE_AMOUNT")), 2)%>
                                                                                                    </td>
                                                                                                    <td align="right">
                                                                                                        <%# Math.Round(Convert.ToDecimal(Container.DataItem("DISC_PERC")), 2)%>
                                                                                                    </td>
                                                                                                    <td align="right">
                                                                                                        <%# Math.Round(Convert.ToDecimal(Container.DataItem("DISC_AMOUNT")), 2)%>
                                                                                                    </td>
                                                                                                    <td align="right">
                                                                                                        <%# Math.Round(Convert.ToDecimal(Container.DataItem("NET_AMOUNT")), 2)%>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </AlternatingItemTemplate>
                                                                                            <ItemTemplate>
                                                                                                <tr style="background-color: #ffffff; border: 1pt; color: #1B80B6; border-color: #1b80b6;">
                                                                                                    <td align="left">
                                                                                                        <%#Container.DataItem("MONTH")%>
                                                                                                    </td>
                                                                                                    <td align="right">
                                                                                                        <%# Math.Round(Convert.ToDecimal(Container.DataItem("FEE_AMOUNT")), 2)%>
                                                                                                    </td>
                                                                                                    <td align="right">
                                                                                                        <%# Math.Round(Convert.ToDecimal(Container.DataItem("DISC_PERC")), 2)%>
                                                                                                    </td>
                                                                                                    <td align="right">
                                                                                                        <%# Math.Round(Convert.ToDecimal(Container.DataItem("DISC_AMOUNT")), 2)%>
                                                                                                    </td>
                                                                                                    <td align="right">
                                                                                                        <%# Math.Round(Convert.ToDecimal(Container.DataItem("NET_AMOUNT")), 2)%>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </ItemTemplate>
                                                                                            <FooterTemplate>
                                                                                                </table>
                                                                                            </FooterTemplate>
                                                                                        </asp:Repeater>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </asp:Panel>
                                                                    </ItemTemplate>
                                                                    <FooterTemplate>
                                                                        <asp:Label ID="lblDiscountF" runat="server" Text="0.00"></asp:Label>
                                                                    </FooterTemplate>
                                                                    <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                                                    <FooterStyle HorizontalAlign="Right" />
                                                                    <ItemStyle HorizontalAlign="Right" Width="89px" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Redeem (AED)">
                                                                    <ItemTemplate >
                                                                        <asp:TextBox ID="txtAmountToPay" AutoCompleteType="Disabled" runat="server" AutoPostBack="True"
                                                                            onFocus="this.select();" OnTextChanged="txtAmountToPay_TextChanged" Style="text-align: right"
                                                                            TabIndex="52" Text='<%# Bind("Amount", "{0:0.00}") %>' Width="100px" CssClass="form-control"></asp:TextBox>
                                                                        <ajaxToolkit:FilteredTextBoxExtender ID="ftbe" runat="server"
                                                                            TargetControlID="txtAmountToPay"
                                                                             FilterType="Custom, Numbers"
                                                                                                        ValidChars="." />

                                                                        <asp:LinkButton ID="lbCancel" Visible="false" runat="server">Cancel</asp:LinkButton>
                                                                        <asp:HiddenField ID="h_OtherCharge" runat="server" />
                                                                        <asp:HiddenField ID="h_Discount" runat="server" />
                                                                        <asp:HiddenField ID="h_BlockPayNow" runat="server" Value='<%# Bind("BlockPayNow") %>' />
                                                                        <asp:HiddenField ID="h_ProcessingCharge" runat="server" />
                                                                    </ItemTemplate>
                                                                    <FooterTemplate>
                                                                        <asp:Label ID="lblAmounttoPayF" runat="server" Text="0.00"></asp:Label>
                                                                        <asp:HiddenField ID="h_ProcessingChargeF" runat="server" />
                                                                    </FooterTemplate>
                                                                    <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                                                    <FooterStyle HorizontalAlign="Right" />
                                                                    <ItemStyle HorizontalAlign="Right" Width="89px" />
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <FooterStyle Font-Bold="true" />
                                                        </asp:GridView>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </td>
                    </tr>
                    <tr id="tr_TaxMessage" runat="server" visible="false" class="tdblankAll">
                        <td align="center" class="tdfields" valign="middle">
                            <asp:Label ID="lblalert" CssClass="alert-warning alert" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr class="tdblankAll">
                        <td align="right" valign="middle" style="font-size: medium;">Total :
                            <asp:Label ID="txtGridTotal" runat="server" CssClass="gridlabel" Text="0.00" Font-Size="Medium"></asp:Label>
                        </td>
                    </tr>
                    <tr class="tdblankAll">
                        <td align="right" valign="middle" style="font-size: medium;">Net Amount :
                            <asp:Label ID="txtTotal" runat="server" CssClass="gridlabel" Text="0.00" Font-Size="Medium"></asp:Label>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="center" colspan="2">
                <asp:Button ID="btnRedeem" runat="server" CssClass="btn btn-info" TabIndex="155" Text="Redeem"
                   />
                &nbsp;<asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="btn btn-info"
                    Text="Cancel" TabIndex="158"  />
            </td>
        </tr>
    </table>
    <div id="testpopup" class="darkPanelM anim" runat="server" style="display: none; z-index: 3000 !important;">
        <div id="divboxpanelconfirm" runat="server" style="height: 50%; width: 40%;">
            <div class="holderInner" style="height: 90%; width: 98%;">
                <center>
                    <table cellpadding="0" cellspacing="0" border="0" style="height: 90% !important; width: 100% !important; overflow-y: scroll;"
                        class="tableNoborder">
                        <tr class="trSub_Header">
                            <td>
                                <span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 50px 0;"></span>
                                Confirm & Proceed
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <span class="ui-icon ui-icon-info" ></span>
                                <asp:Label ID="lblMsg" runat="server"></asp:Label><br />
                                <br />
                                <br />
                                <span class="ui-icon ui-icon-notice" ></span>
                                &nbsp
                                <asp:Label ID="lblMsg2" runat="server" ></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <asp:Button ID="btnProceed" runat="server" CausesValidation="False" CssClass="btn btn-info"
                                    Text="Confirm & Proceed" />
                                <asp:Button ID="btnSkip" runat="server" CausesValidation="False" CssClass="btn btn-info"
                                    Text="Cancel" />
                            </td>
                        </tr>
                    </table>
                </center>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hfACLID" runat="server" Value="0" />
    <asp:HiddenField ID="hfCurrency" runat="server" Value="" />

  </div>
                <!-- /Table  -->
            </div>

        </div>
    </div>
        
    <!-- /Posts Block -->
                        </div>
                    </div>
        </div>

    <script lang="javascript" type="text/javascript">
        function ValidateSave() {
            var ElblAmt = parseFloat($('#<%=lblRedeemableAmt.ClientID%>').text().replace(",", ""));
            var gridTotal = parseFloat($('#<%=txtGridTotal.ClientID%>').text().replace(",", ""));
            $('#<%=lblError.ClientID%>').removeClass();
            var currency = $('#<%=hfCurrency.ClientID%>').val();

            if ($.isNumeric(ElblAmt) == true) {
                if (ElblAmt <= 0) {
                    $('#<%=lblError.ClientID%>').addClass('alert-warning alert');
                    $('#<%=lblError.ClientID%>').text('Insufficient balance to redeem points!');
                    return false;
                }
                else if (ElblAmt > 0 && ElblAmt < 1) {
                    $('#<%=lblError.ClientID%>').addClass('alert-warning alert');
                    $('#<%=lblError.ClientID%>').text('Minimum of ' + currency + ' 1 is required for redemption!');
                    return false;
                }
        }
        else {
            $('#<%=lblError.ClientID%>').addClass('alert-warning alert');
                $('#<%=lblError.ClientID%>').text('Insufficient balance to redeem points!');
                return false;
            }
            if ($.isNumeric(gridTotal) == true) {
                if (gridTotal <= 0) {
                    $('#<%=lblError.ClientID%>').addClass('alert-warning alert');
                    $('#<%=lblError.ClientID%>').text('Enter the amount for redemption!');
                    return false;
                }
                else if (gridTotal > 0 && gridTotal < 1) {
                    $('#<%=lblError.ClientID%>').addClass('alert-warning alert');
                    $('#<%=lblError.ClientID%>').text('Minimum redemption amount is ' + currency + ' 1!');
                    return false;
                }
                <%--else if ((gridTotal % 10) != 0) {
                    $('#<%=lblError.ClientID%>').addClass('alert-warning alert');
                    $('#<%=lblError.ClientID%>').text('The total redemption amount should be multiple of 10');
                    return false;
                }--%>
    }
            //SaveAPICallLog();
    return true;
}

    </script>
</asp:Content>

