﻿<%@ Page Language="VB"  MasterPageFile="~/ParentMaster.master" AutoEventWireup="false" CodeFile="FeeRefundOnline.aspx.vb" Inherits="Fees_FeeRefundOnline" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphParent" runat="Server">
                      
    <script src="../Scripts/jquery-1.9.1.js" type="text/javascript"></script>

    <link rel="stylesheet" type="text/css" href="../Scripts/jQuery-ui-1.10.3.css" />
    <link href="../css/Popup.css" rel="stylesheet" />
    <!-- Add fancyBox -->

    <script type="text/javascript" src="../Scripts/Fancybox/jquery.fancybox.js?v=2.1.5"></script>

    <script type="text/javascript" src="../Scripts/Fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>

    <link rel="stylesheet" href="../Scripts/Fancybox/jquery.fancybox.css" type="text/css"
        media="screen" />

    <script type="text/javascript" src="../Scripts/PopupJQuery.js"></script>

    <script type="text/javascript" language="javascript">
        function ShowInfoW(w, h) {
            $.fancybox({
                href: '#testpopup',
                maxHeight: 600,
                fitToView: true,
                width: w,
                height: h,
                padding: 0,
                'titleShow': false,
                'transitionIn': 'elastic',
                'transitionOut': 'elastic',
                closeEffect: 'fade',
                helpers: {
                    overlay: { closeClick: false } // prevents closing when clicking OUTSIDE fancybox 
                },
            });

            return false;
        }

    </script>
    

    <div class="content margin-top30 margin-bottom60">
                  <div class="container">
                        <div class="row">


    <!-- Posts Block -->
    <div class="posts-block col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="my-account">
            
            <div class="bottom-padding">
                <div class="title-box">
                    <h3> Online Fee Refund 
                        <span class="profile-right">
                                <asp:Label ID="lbChildNameTop" runat="server"></asp:Label>
                            </span>
                    </h3>
                </div>
                <!-- Table  -->
                <div class="table-responsive">
                    
  
    <%--      <ajaxToolkit:ToolkitScriptManager runat="server" ID="ScriptManager1" AsyncPostBackTimeout="600" >            
        </ajaxToolkit:ToolkitScriptManager>--%>

        <div>
            <%If lblError.Text <> "" Then%>        
                <asp:Label ID="lblError" runat="server" CssClass="alert-danger alert margin-bottom0" width="100%" EnableViewState="False"></asp:Label>
            <%End If%>
        </div>

    <table align="center" cellpadding="0" cellspacing="0" style="width: 100%;" class="table table-striped table-bordered table-responsive text-left my-orders-table">
        
        <tr>
            <th align="left" class="tdfields" width="20%">Date
            </th>
            <td align="left" class="tdfields">
                <asp:Label ID="lblDate" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <th align="left" class="tdfields">School
            </th>
            <td align="left" class="tdfields">
                <asp:Label ID="lblschool" runat="server"></asp:Label>
            </td>
        </tr>

        <tr id="tr_AddFee" runat="server">
            <th align="left" colspan="2" class="trSub_Header">Fee Refund Details
                <%= IIf(Session("BSU_CURRENCY") Is Nothing, "", "(Amount In " & Session("BSU_CURRENCY") & ")")%>
               
            </th>
        </tr>
        
        <tr>
            <td align="left" colspan="2">
                <table cellpadding="0" cellspacing="0" border="0" width="100%" >
                    <tr>
                        <td align="left">
                            <%--<asp:UpdatePanel ID="UpdatePanel1" runat="server" class="width-100">
          <ContentTemplate>--%>
                            <asp:Repeater ID="repInfo" runat="server">
                                <HeaderTemplate>
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding: 0;">
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr><td colspan="3">&nbsp;</td></tr>
                                    <tr class="trSub_Header_Small">
                                        <th colspan="2" class="margin-bottom5 margin-top10">
                                            <asp:Label ID="lbSName" runat="server" Text='<%# Bind("SNAME") %>'></asp:Label>&nbsp;[<asp:Label ID="lbSNo" runat="server" Text='<%# Bind("STU_NO")%>'></asp:Label>]
                                        </th>
                                        <th align="right" class="text-right">
                                            <div style="clear: both;">
                                                <asp:Label ID="lbGrade" runat="server" Text='<%# Bind("GRM_DISPLAY")%>'></asp:Label>-<asp:Label ID="lbSection" runat="server" Text='<%# Bind("SCT_DESCR")%>'></asp:Label>
                                                <%-- &nbsp;-<asp:LinkButton ID="lnkViewFeeSetup" Text="View Fee Schedule" CssClass="linkFee" runat="server" Font-Size="9px"></asp:LinkButton>--%>
                                                <asp:HiddenField ID="hfSTU_ID" Value='<%# Bind("STU_ID") %>' runat="server" />
                                                <asp:HiddenField ID="hfSTU_FEE_ID" Value='<%# Bind("STU_FEE_ID")%>' runat="server" />
                                                <asp:HiddenField ID="hfSTU_ACD_ID" Value='<%# Bind("STU_ACD_ID")%>' runat="server" />
                                                <asp:HiddenField ID="hfSTU_CURRSTATUS" Value='<%# Bind("STU_CURRSTATUS")%>' runat="server" />
                                            </div>
                                        </th>
                                    </tr>
                                    <tr><td colspan="3">&nbsp;</td></tr>
                                    <tr id="trgvrerror" runat="server" visible="false">
                                        <td colspan="3" align="center">
                                            <asp:Label ID="lblGvrError" CssClass="alert-danger alert margin-bottom0" width="100%" Text="" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr align="left">
                                        <td valign="top" colspan="3" align="left">
                                            <table cellpadding="0" width="100%" cellspacing="0" border="0" style="border-collapse: collapse !important; border-spacing: 0 !important; padding: 0 !important; border: none 0px !important;">
                                                <tr>
                                                    <td valign="top" align="left" width="10%"><%--style="width: 62px;"--%>
                                                        <asp:Image ID="imgEmpImage" runat="server" Height="80px" ImageUrl='<%# Bind("PHOTOPATH") %>' ToolTip='<%# Bind("SNAME") %>'
                                                            Width="60px" />
                                                    </td>
                                                    <td valign="top" width="90%">
                                                        <asp:GridView ID="gvFee" runat="server" AutoGenerateColumns="False" EmptyDataText="No Details"
                                                            cssclass="table table-striped table-bordered table-responsive text-left my-orders-table" Width="100%" OnRowDataBound="gvFeeCollection_RowDataBound"  ShowFooter="true">
                                                            <RowStyle Height="20px" VerticalAlign="Top" />
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="SELECT" Visible="true" >
                                                                    <ItemTemplate >
                                                                        <asp:CheckBox ID="lblFSR_FEE_CHK_ID" Visible="true" runat="server" OnCheckedChanged="rdSelect_CheckedChanged" AutoPostBack="true" />
                                                                    </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" Width="10%" />
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="FEE_ID" Visible="False">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblFSR_FEE_ID" runat="server" Text='<%# Bind("FEE_ID") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Fee" ItemStyle-Wrap="true" >
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblFeeDescr" runat="server" Text='<%# Bind("FEE_DESCR")%>'></asp:Label>
                                                                    </ItemTemplate>
                                                                    <FooterTemplate>
                                                                        <asp:Label ID="lblTotals" runat="server" Text='TOTAL'></asp:Label>
                                                                    </FooterTemplate>
                                                                    <ItemStyle HorizontalAlign="Left" Width="60%" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Balance">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblAmount" runat="server" Text='<%# Bind("Amount", "{0:0.00}")%>'></asp:Label>
                                                                    </ItemTemplate>
                                                                    <FooterTemplate>
                                                                        <asp:Label ID="lblAmountF" runat="server" Text=""></asp:Label>
                                                                    </FooterTemplate>
                                                                    <ItemStyle HorizontalAlign="Right" Width="15%" />  <%--Width="89px" --%>
                                                                    <FooterStyle HorizontalAlign="Right" />
                                                                    <HeaderStyle HorizontalAlign="Right" />
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Refund Amt">
                                                                    <ItemTemplate >
                                                                        <asp:TextBox ID="txtAmountToRefund" CssClass="form-control" AutoCompleteType="Disabled" runat="server" AutoPostBack="True" Enabled="false"
                                                                            onFocus="this.select();" OnTextChanged="txtAmountToPay_TextChanged" Text="0" Style="text-align: right"
                                                                            TabIndex="52" ></asp:TextBox>
                                                                        <ajaxToolkit:FilteredTextBoxExtender ID="ftbe" runat="server"
                                                                            TargetControlID="txtAmountToRefund"
                                                                            FilterType="Custom, Numbers"
                                                                            ValidChars="." />
                                                                    </ItemTemplate>
                                                                    <FooterTemplate>
                                                                        <asp:Label ID="lblReFAmountF" runat="server" Text=""></asp:Label>
                                                                    </FooterTemplate>
                                                                    <ItemStyle HorizontalAlign="Right" Width="15%" /><%--Width="89px" --%>
                                                                    <FooterStyle HorizontalAlign="Right" />
                                                                    <HeaderStyle HorizontalAlign="Right" />
                                                                </asp:TemplateField>


                                                            </Columns>
                                                            <FooterStyle Font-Bold="true" />
                                                        </asp:GridView>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                            <%--     </ContentTemplate>
                </asp:UpdatePanel>--%>
                        </td>
                    </tr>
                    <tr id="tr_TaxMessage" runat="server" class="tdblankAll">
                        <td align="center" class="tdfields" valign="middle">
                            <asp:Label ID="lblalert" CssClass="alert-danger alert margin-bottom0" Width="100%" runat="server"></asp:Label>
                        </td>
                    </tr>

                             <tr >
            <td align="left" colspan="2" >
                <table cellpadding="0" cellspacing="0" border="0" width="100%" >
           <tr id="tr1" runat="server" class="tdblankAll">
                         <th align="left"  width="10%">Reason
            </th>
                        <td align="center" valign="middle"> 
                        <textarea  id="txt_Remarks2" class="form-control"  rows="3" style="width:100%"  runat="server"/> 
                            </td>
                    </tr>

                      <tr >
            <td  >&nbsp;
                      </td>
                    </tr>
             
                  
                    <tr class="tdblankAll">
                        <td  align="right" valign="middle" class="tdfields" colspan="2"> <h4>Net Refund :
            <asp:TextBox ID="txtTotal" CssClass="form-control text-right" runat="server" TabIndex="152">
            </asp:TextBox></h4>
                        </td>
                    </tr>
                    </table>
 </td>
                    </tr>
                 
                </table>
            </td>
        </tr>
         

        <tr class="tdblankAll">
            <td align="center" colspan="2" class="tdblankAll">
                <asp:Button ID="btnSave" runat="server"  CssClass="btn btn-info" TabIndex="155" Text="Submit"/>
                <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="btn btn-info"
                    Text="Cancel" TabIndex="158" />
            </td>
        </tr>


    </table>
    <asp:ObjectDataSource ID="odsGetFEETYPE_M" runat="server" OldValuesParameterFormatString="original_{0}"
        SelectMethod="GetFEES_M" TypeName="FeeCommon">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="123004" Name="BSU_ID" SessionField="BSU_ID" Type="String" />
            <asp:SessionParameter DefaultValue="0" Name="ACD_ID" SessionField="ACD_ID" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <span class="anim">
        <label id="test" runat="server">
        </label>
    </span>

      <div id="divNote" runat="server"  title="Click on the message box to drag it up and down" visible="false" ClientIDMode="Static">
                   <span class="msgInfoclose">
                       <asp:Button ID="btn" type="button" ValidationGroup="none" runat="server"  Text="X"
                CausesValidation="false"></asp:Button>
                   </span>                                   
                        <asp:Label ID="Label1" runat="server" EnableViewState="false"></asp:Label>      </div>

    <asp:UpdateProgress ID="upProgGv" runat="server">
        <ProgressTemplate>
            <asp:Panel ID="pnlProgress" runat="server">
                <br />
                <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/loading1.gif" /><br />
                Please Wait....
            </asp:Panel>
            <ajaxToolkit:AlwaysVisibleControlExtender ID="avcProgress" runat="server" HorizontalOffset="10"
                HorizontalSide="Center" ScrollEffectDuration=".1" TargetControlID="pnlProgress"
                VerticalOffset="10" VerticalSide="Middle">
            </ajaxToolkit:AlwaysVisibleControlExtender>
        </ProgressTemplate>
    </asp:UpdateProgress>

                      </div>
                <!-- /Table  -->
            </div>

        </div>
    </div>

                        </div>
                    </div>
        </div>
  


  

</asp:Content>
