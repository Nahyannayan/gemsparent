<%@ Page Title="" Language="VB" MasterPageFile="~/ParentMaster.master" AutoEventWireup="false"
    CodeFile="feePaymentResultSibling.aspx.vb" Inherits="Fees_feePaymentResultSibling" Debug="false" %>

<%@ Register Src="../UserControl/urcStudentPaidResult.ascx" TagName="urcStudentPaidResult" TagPrefix="ucs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphParent" runat="Server">

    <script src="../Scripts/jquery-1.9.1.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.10.2.min.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-ui-1.10.2.min.js" type="text/javascript"></script>
    <!-- Add fancyBox -->
    <script type="text/javascript" src="../Scripts/Fancybox/jquery.fancybox.js?v=2.1.5"></script>
    <script type="text/javascript" src="../Scripts/Fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
    <link rel="stylesheet" href="../Scripts/Fancybox/jquery.fancybox.css" type="text/css" media="screen" />
    <script type="text/javascript" src="../Scripts/PopupJQuery.js"></script>
    <script type="text/javascript" language="javascript">
        function CheckForPrint() {
            <%--if (document.getElementById('<%=h_Recno.ClientID %>').value != '') {
                var frmReceipt = "feereceipt.aspx";
                if ($("#<%=hfTaxable.ClientID%>").val() == "1")
                    frmReceipt = "FeeReceipt_TAX.aspx";
                else
                    frmReceipt = "feereceipt.aspx";
                showModelessDialog(frmReceipt + '?type=REC&id=' + document.getElementById('<%=h_Recno.ClientID %>').value, '', "dialogWidth: 800px; dialogHeight: 700px; help: no; resizable: no; scroll: yes; status: no; unadorned: no;");
                return false;
            }
            else
                alert('Sorry. There is some problem with the transaction');--%>
        }
    </script>
          <div class="content margin-top30 margin-bottom60">
        <div class="container">
            <div class="row">

                <!-- Posts Block -->
                
                <div class="posts-block col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <table align="center" cellpadding="0" cellspacing="0" class="BlueTable" width="90%">
        <tr class="subheader_img">
            <td style="height: 19px" align="left">
                <asp:HiddenField ID="hfTaxable" runat="server" Value="0" />
                <asp:Label ID="lblError" runat="server" EnableViewState="False" CssClass="alert alert-warning"></asp:Label>
                <asp:Label ID="lblMessage" runat="server" EnableViewState="true" CssClass="text-danger"></asp:Label></td>
        </tr>
        <tr class="matters" >
            <th align="left" class="tdfields" ><%--Transaction Reference :--%>
                <asp:Label ID="Label_MerchTxnRef" runat="server"  Visible="False" />
                <asp:Label ID="lblDate" runat="server" CssClass="matters" Visible="False"></asp:Label>
            </th>
        </tr>
        <tr>
            <td align="left">

                <ucs:urcStudentPaidResult ID="urcStudentPaidResult1" runat="server" />

            </td>
        </tr>
    </table>
    <br />
    <table align="center" border="0" width="70%" style="display: none">
        <tr class="title">
            <td colspan="2">
                <iframe id="frame1" scrolling="auto" runat="server"></iframe>
                <p>
                    <strong>&nbsp;Transaction Receipt Fields</strong>
                </p>
                <asp:Label ID="Label_Title" runat="server" />
            </td>
        </tr>
        <tr>
            <td align="right">
                <strong><i>VPC API Version: </i></strong>
            </td>
            <td>
                <asp:Label ID="Label_Version" runat="server" /></td>
        </tr>
        <tr class='shade'>
            <td align="right">
                <strong><i>Command: </i></strong>
            </td>
            <td>
                <asp:Label ID="Label_Command" runat="server" /></td>
        </tr>
        <tr>
            <td align="right">
                <strong><em>MerchTxnRef: </em></strong>
            </td>
            <td></td>
        </tr>
        <tr class="shade">
            <td align="right">
                <strong><em>Merchant ID: </em></strong>
            </td>
            <td>
                <asp:Label ID="Label_MerchantID" runat="server" /></td>
        </tr>
        <tr>
            <td align="right">
                <strong><em>OrderInfo: </em></strong>
            </td>
            <td>
                <asp:Label ID="Label_OrderInfo" runat="server" /></td>
        </tr>
        <tr class="shade">
            <td align="right">
                <strong><em>Transaction Amount: </em></strong>
            </td>
            <td></td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <div class='bl'>
                    Fields above are the primary request values.<hr>
                    Fields below are receipt data fields.
                </div>
            </td>
        </tr>
        <tr class="shade">
            <td align="right">
                <strong><em>Transaction Response Code: </em></strong>
            </td>
            <td>
                <asp:Label ID="Label_TxnResponseCode" runat="server" /></td>
        </tr>
        <tr>
            <td align="right">
                <strong><em>QSI Response Code Description: </em></strong>
            </td>
            <td>
                <asp:Label ID="Label_TxnResponseCodeDesc" runat="server" /></td>
        </tr>
        <tr class='shade'>
            <td align="right">
                <strong><i>Message: </i></strong>
            </td>
            <td>
                <asp:Label ID="Label_Message" runat="server" /></td>
        </tr>
        <asp:Panel ID="Panel_Receipt" runat="server" Visible="false">
            <!-- only display these next fields if not an error -->
            <tr>
                <td align="right">
                    <strong><em>Shopping Transaction Number: </em></strong>
                </td>
                <td>
                    <asp:Label ID="Label_TransactionNo" runat="server" /></td>
            </tr>
            <tr class="shade">
                <td align="right">
                    <strong><em>Batch Number for this transaction: </em></strong>
                </td>
                <td>
                    <asp:Label ID="Label_BatchNo" runat="server" /></td>
            </tr>
            <tr>
                <td align="right">
                    <strong><em>Acquirer Response Code: </em></strong>
                </td>
                <td>
                    <asp:Label ID="Label_AcqResponseCode" runat="server" /></td>
            </tr>
            <tr class="shade">
                <td align="right">
                    <strong><em>Receipt Number: </em></strong>
                </td>
                <td>
                    <asp:Label ID="Label_ReceiptNo" runat="server" /></td>
            </tr>
            <tr>
                <td align="right">
                    <strong><em>Authorization ID: </em></strong>
                </td>
                <td>
                    <asp:Label ID="Label_AuthorizeID" runat="server" /></td>
            </tr>
            <tr class="shade">
                <td align="right">
                    <strong><em>Card Type: </em></strong>
                </td>
                <td>
                    <asp:Label ID="Label_CardType" runat="server" /></td>
            </tr>
        </asp:Panel>
        <tr>
            <td colspan="2" style="height: 32px">
                <hr />
                &nbsp;</td>
        </tr>
        <tr class="title">
            <td colspan="2" height="25">
                <p>
                    <strong>&nbsp;Hash Validation</strong>
                </p>
            </td>
        </tr>
        <tr>
            <td align="right">
                <strong><em>Hash Validated Correctly: </em></strong>
            </td>
            <td>
                <asp:Label ID="Label_HashValidation" runat="server" /></td>
        </tr>
        <asp:Panel ID="Panel_StackTrace" runat="server">
            <!-- only display these next fields if an stacktrace output exists-->
            <tr>
                <td colspan="2">&nbsp;</td>
            </tr>
            <tr class="title">
                <td colspan="2">
                    <p>
                        <strong>&nbsp;Exception Stack Trace</strong>
                    </p>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Label ID="Label_StackTrace" runat="server" /></td>
            </tr>
        </asp:Panel>
        <tr>
            <td width="50%">&nbsp;</td>
            <td width="50%">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <asp:Label ID="Label_AgainLink" runat="server" /></td>
        </tr>
        <tr>
            <td align="center" colspan="2">
                <asp:Panel ID="Panel_Debug" runat="server" Visible="false">
                    <!-- only display these next fields if debug enabled -->
                    <table>
                        <tr>
                            <td>
                                <asp:Label ID="Label_Debug" runat="server" /></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label_DigitalOrder" runat="server" /></td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
    </table>
                       </div>
                 </div>
             </div>
               </div>
</asp:Content>
