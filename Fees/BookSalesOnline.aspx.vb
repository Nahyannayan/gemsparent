﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Imports System.Collections.Generic
Imports Lesnikowski.Barcode

Partial Class Fees_BookSalesOnline
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Public Shared dtDiscount As DataTable
    Dim MainObj As Mainclass = New Mainclass()
    Dim SAL_GRD As DataTable = New DataTable()

    Private Property bPaymentConfirmMsgShown() As Boolean
        Get
            Return ViewState("bPaymentConfirmMsgShown")
        End Get
        Set(ByVal value As Boolean)
            ViewState("bPaymentConfirmMsgShown") = value
            If value Then
                btnSave.Text = "Confirm & Proceed"
            Else
                btnSave.Text = "Proceed"
            End If
        End Set
    End Property
    Private Property SALGRD() As DataTable
        Get
            Return ViewState("SALGRD")
        End Get
        Set(ByVal value As DataTable)
            ViewState("SALGRD") = value
        End Set
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim BSU_ID As String = Session("sBsuid")
        If Session("username") Is Nothing Then
            Response.Redirect("~\Login.aspx")
        End If
        If Page.IsPostBack = False Then

            Dim pParms1(2) As SqlClient.SqlParameter
            pParms1(0) = New SqlClient.SqlParameter("@OPTIONS", SqlDbType.Int)
            pParms1(0).Value = 1
            pParms1(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.Int)
            pParms1(1).Value = Session("sBSUID")
            Dim dsM As New DataSet
            dsM = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_PUR_INVConnectionString, CommandType.StoredProcedure, "[dbo].[GET_BOOK_COLLECTION_MESSAGE]", pParms1)
            If Not dsM Is Nothing AndAlso dsM.Tables(0).Rows.Count >= 1 Then
                If Convert.ToBoolean(dsM.Tables(0).Rows(0)("BCM_COLP")) Then
                    rbl_deliverytype.Items.Add(New ListItem("Collect the Books from School Personally", "0"))
                    rbl_deliverytype.SelectedValue = 0 'Change in Delivery type
                    rbl_deliverytype_SelectedIndexChanged(Nothing, Nothing)
                End If
                If Convert.ToBoolean(dsM.Tables(0).Rows(0)("BCM_COLC")) Then
                    rbl_deliverytype.Items.Add(New ListItem("Deliver Books To Child", "1"))
                    rbl_deliverytype.SelectedValue = 1 'Change in Delivery type
                End If
                If Convert.ToBoolean(dsM.Tables(0).Rows(0)("BCM_COLD")) Then
                    rbl_deliverytype.Items.Add(New ListItem("Deliver Books By Courier", "2"))
                    rbl_deliverytype.SelectedValue = 2 'Change in Delivery type
                    rbl_deliverytype_SelectedIndexChanged(Nothing, Nothing)
                End If
            End If
            'rbl_deliverytype.SelectedValue = 1 'Change in Delivery type
            If SALGRD Is Nothing Then

                SALGRD = GetTable()
            End If

            If rbl_deliverytype.SelectedValue = 0 Then
                id_courier.Visible = False
                id_colMessage.Visible = True
            ElseIf rbl_deliverytype.SelectedValue = 1 Then
                id_courier.Visible = False
                id_colMessage.Visible = False
            ElseIf rbl_deliverytype.SelectedValue = 2 Then
                id_courier.Visible = True
                id_colMessage.Visible = True
            End If



            bPaymentConfirmMsgShown = False
            BindPaymentProviders(Session("sBsuid"))
            Page.Title = "::GEMS Education |Book Sale::"
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            ViewState("datamode") = "add"
            Dim CurUsr_id As String = Session("STU_ID")
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("STU_NAME")
            InitialiseCompnents()

            If USR_NAME = "" Or CurBsUnit = "" Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else

                ViewState("STU_NO") = Session("STU_NO")
                ViewState("STU_NAME") = Session("STU_NAME")
                ViewState("STU_GRD_ID") = Session("STU_GRD_ID")
                ViewState("stu_section") = Session("stu_section")
                ViewState("BSU_NAME") = Session("BSU_NAME")

                rblPaymentGateway.DataBind()
                lblschool.Text = ViewState("BSU_NAME")

                If rblPaymentGateway.Items.Count > 0 And rblPaymentGateway.SelectedIndex = -1 Then
                    rblPaymentGateway.SelectedIndex = 0
                    rblPaymentGateway_SelectedIndexChanged(sender, e)
                Else
                    Gridbind_StuDetails()
                End If
                lMoreInfo.Attributes.Add("onClick", "return ShowSubWindowWithClose('whatsThis.htm', '', '50%', '50%');")
            End If
            'Dim obj As Object = sender.parent
            'Dim GridSelBook As Label = DirectCast(obj.FindControl("txtItmSelect"), Label)
            'GridSelBook.Visible = True







        End If
    End Sub


    Private Function CheckIfStudentAllowedForOnlinePayment(ByVal STU_ID As Integer) As Boolean
        Try

            Dim dsData As DataSet = FeeCommon.CheckStudentAllowedForOnlinePayment(STU_ID)
            If Not dsData Is Nothing AndAlso dsData.Tables.Count > 0 AndAlso dsData.Tables(0).Rows.Count > 0 Then
                CheckIfStudentAllowedForOnlinePayment = True
            Else
                CheckIfStudentAllowedForOnlinePayment = False
            End If
        Catch ex As Exception

        End Try
        CheckIfStudentAllowedForOnlinePayment = False

    End Function
    Public Sub BindPaymentProviders(ByVal BSU_ID As String)
        HFCardCharge.Value = ""
        Dim pParms(1) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(0).Value = BSU_ID
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISFINConnectionString, _
         CommandType.StoredProcedure, "GetMerchantDetails_BSU_ID", pParms)
        If Not dsData Is Nothing AndAlso dsData.Tables.Count > 0 Then
            For Each row As DataRow In dsData.Tables(0).Rows
                HFCardCharge.Value = HFCardCharge.Value & IIf(HFCardCharge.Value = "", "", "|") & row("CPS_ID") & "=" & row("CRR_CLIENT_RATE")
                'Dim lst As New ListItem(String.Format("<img src='{0}' alt='{1}' align='absmiddle' onclick='javascript:return this.parentNode.previousSibling.checked=true;' class='feeImg' />", row("CPM_IMAGEPATH").ToString(), row("CPM_DESCR").ToString()), row("CPS_ID").ToString())
                Dim lst As New ListItem(String.Format("<img src='{0}' alt='{1}'  title='{2}' align='absmiddle' onclick='javascript:return this.parentNode.previousSibling.checked=true;' class='feeImg' />", row("CPM_IMAGEPATH").ToString(), row("CPM_DESCR").ToString(), row("CPM_REMARKS").ToString()), row("CPS_ID").ToString())
                rblPaymentGateway.Items.Add(lst)
            Next
        End If
    End Sub
    Protected Sub repInfo_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles repInfo.ItemDataBound
        Dim hfSTU_ID As New HiddenField
        Dim hfSTU_ACD_ID As New HiddenField
        Dim GridSelBook As New Panel
        Dim lblGvrError As New Label
        Dim grdSAL As New GridView
        Dim DivHeaderRow As New HtmlGenericControl
        Dim DivMainContent As New HtmlGenericControl
        Dim DivFooterRow As New HtmlGenericControl
        Dim SelectItem As New HtmlGenericControl

        'Dim btnMinus As New ImageButton
        'Dim btnPlus As New ImageButton
        If e.Item.DataItem Is Nothing Then
            Return
        Else
            'Dim rblist As RadioButtonList = DirectCast(e.Item.FindControl("rdbBookSets"), RadioButtonList)
            Dim rblist As CheckBoxList = DirectCast(e.Item.FindControl("rdbBookSets"), CheckBoxList)
            GridSelBook = DirectCast(e.Item.FindControl("GridScroll"), Panel)
            grdSAL = DirectCast(e.Item.FindControl("grdSAL"), GridView)
            DivHeaderRow = DirectCast(e.Item.FindControl("DivHeaderRow"), HtmlGenericControl)
            DivMainContent = DirectCast(e.Item.FindControl("DivMainContent"), HtmlGenericControl)
            DivFooterRow = DirectCast(e.Item.FindControl("DivFooterRow"), HtmlGenericControl)
            SelectItem = DirectCast(e.Item.FindControl("SelectItem"), HtmlGenericControl)
            'btnMinus = DirectCast(e.Item.FindControl("btnMinus"), ImageButton)
            'btnPlus = DirectCast(e.Item.FindControl("btnPlus"), ImageButton)

            Dim hfSTU_GRD_ID As HiddenField = DirectCast(e.Item.FindControl("hfSTU_GRD_ID"), HiddenField)
            'rblist.Items.Add(New ListItem("Science", "1"))
            Dim str_conn As String = ConnectionManger.GetOASIS_PUR_INVConnectionString
            Dim str_Sql As String = " SELECT BSH_ID,BSH_DESCR,ISNULL(BSH_bMANDATORY,0) BSH_bMANDATORY FROM [dbo].[BOOK_SET_H] WITH(NOLOCK) WHERE ISNULL([BSH_bSHOWONLINE],0) =1 AND ISNULL([BSH_DELETED],0)=0 AND " _
                                     & " [BSH_BSU_ID]  ='" + Session("SBSUID") + "' AND [BSH_GRD_ID]= '" + hfSTU_GRD_ID.Value + "'  ORDER BY [BSH_ID] "
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            rblist.DataSource = ds
            rblist.DataTextField = "BSH_DESCR"
            rblist.DataValueField = "BSH_ID"
            rblist.DataBind()

            'check mandatory set 
            'Dim str_Sql2 As String = " SELECT TOP 1 * FROM [dbo].[BOOK_SET_H] H INNER JOIN [dbo].[BOOK_SET_D] D ON H.BSH_ID=D.BSD_BSH_ID   WHERE ISNULL([BSH_bSHOWONLINE],0) =1 AND ISNULL([BSH_DELETED],0)=0 AND BSD_bLOCKQTY=1 AND " _
            '                       & " [BSH_BSU_ID]  ='" + Session("SBSUID") + "' AND [BSH_GRD_ID]= '" + hfSTU_GRD_ID.Value + "'  ORDER BY [BSH_ID] "
            'Dim ds2 As DataSet
            'Dim Mandatory_Set As Integer = "-1"
            'ds2 = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql2)
            If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                For i As Integer = 0 To rblist.Items.Count - 1
                    Dim dtView As DataView = New DataView(ds.Tables(0))
                    dtView.RowFilter = "BSH_ID=" & rblist.Items(i).Value & " AND BSH_bMANDATORY=1"
                    If dtView.ToTable().Rows.Count > 0 Then
                        rblist.Items(i).Selected = True
                        'rdbBookSets_SelectedIndexChanged(rblist, Nothing)
                        rblist.Items(i).Enabled = False
                    End If
                Next
            End If



            If SALGRD Is Nothing Then
                GridSelBook.Visible = False
                'grdSAL.Visible = False
            Else

                GridSelBook.Visible = True
                'grdSAL.Visible = True
            End If

            rdbBookSets_SelectedIndexChanged(rblist, Nothing)

            'id_show_add_items
            Dim id_show_add_items As Boolean = 0
            Dim pParms(2) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@OPTIONS", SqlDbType.Int)
            pParms(0).Value = 2
            pParms(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.Int)
            pParms(1).Value = Session("sBSUID")
            Dim ds0 As New DataSet
            ds0 = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_PUR_INVConnectionString, CommandType.StoredProcedure, "[dbo].[GET_SHOW_ADD_MORE_ITEMS]", pParms)
            id_show_add_items = Convert.ToBoolean(ds0.Tables(0).Rows(0)("IS_SHOW"))
            If id_show_add_items = True Then
                SelectItem.Visible = True
            Else
                SelectItem.Visible = False
            End If

        End If
    End Sub

    Sub InitialiseCompnents()
        lblDate.Text = Format(Date.Now, "dd/MMM/yyyy")
        txtTotal.Attributes.Add("readonly", "readonly")

    End Sub
    Sub clear_All()

        HFCardCharge.Value = ""
        tr_comment.Visible = False

        txtTotal.Text = ""
        lblTotal.Text = ""

        ClearStudentData()
    End Sub
    Private Function TrimStudentNo(ByVal STUNO As String) As String
        TrimStudentNo = STUNO
        If STUNO.Length >= 14 Then
            TrimStudentNo = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT CAST(REPLACE('" & STUNO & "',SUBSTRING('" & STUNO & "',1,6),'')AS INTEGER)AS STUNO")
        End If
    End Function
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click

        Dim BSU_ID As String = Session("sBsuid")
        Dim Qty_Error As String = ""
        Dim Qty_Error_Check As Integer = 0

        If bPaymentConfirmMsgShown = True Then
            btnSave.Enabled = False
            PaymentRedirect() 'Response.Redirect("BookSalePaymentRedirectNew.aspx")
            Exit Sub
        End If

        Dim vpc_OrderInfo As String = String.Empty
        Dim vpc_Amount As Decimal = 0.0
        'Dim vpc_ReturnURL As String = String.Empty
        Dim boolPaymentInitiated As Boolean = False
        Dim str_error As String = ""
        If rblPaymentGateway.SelectedIndex = -1 Then
            If str_error <> "" Then
                str_error = str_error & "<br /><br />"
                str_error = str_error & "Please select a payment gateway"
            Else
                str_error = str_error & "Please select a payment gateway"
            End If
        End If
        If rbl_deliverytype.SelectedItem.Value = 2 Then
            If txt_contactno1.Text = "" Or txt_address.Text = "" Then
                If str_error <> "" Then
                    str_error = str_error & "<br /><br />"
                    str_error = str_error & "Please enter Primary Contact No and Address"
                Else
                    str_error = str_error & "Please enter Primary Contact No and Address"
                End If
            End If
        End If
        If Not IsDate(lblDate.Text) Then

            If str_error <> "" Then
                str_error = str_error & "<br /><br />"
                str_error = str_error & "Invalid date"
            Else
                str_error = str_error & "Invalid date"
            End If
        End If
        If Not IsNumeric(Session("STU_ID")) Then

            If str_error <> "" Then
                str_error = str_error & "<br /><br />"
                str_error = str_error & "Please select student"
            Else
                str_error = str_error & "Please select student"
            End If
        End If
        Dim dblTotal As Decimal
        dblTotal = CDbl(txtTotal.Text)
        If dblTotal <= 0 Then

            If str_error <> "" Then
                str_error = str_error & "<br /><br />"
                str_error = str_error & "Please enter a valid amount"
            Else
                str_error = str_error & "Please enter a valid amount"
            End If
        End If
        If str_error <> "" Then
            lblError.Text = str_error
            lblError.CssClass = "alert alert-warning"
            Exit Sub
        Else
            lblError.CssClass = ""
        End If

        'checking the available quantity on save click
        'Dim dtt As DataTable = SALGRD
        'For Each gvrow As DataRow In dtt.Rows
        '    Dim FNL_QTY As Integer = 0
        '    Dim dt0 As New DataTable
        '    Dim filter_str As String = ""
        '    Dim dtView As DataView = New DataView(SALGRD)
        '    filter_str = " ITEM_ID=" & gvrow("ITEM_ID") & ""
        '    dtView.RowFilter = filter_str
        '    dt0 = dtView.ToTable
        '    Dim ds5 As New DataSet
        '    Dim AVAIL_QTY As Integer = 0
        '    ds5 = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_PUR_INVConnectionString, CommandType.Text, "SELECT [dbo].[GET_BAL_ITEM_QTY] ('" & Session("sBSUID") & "'," & gvrow("ITEM_ID") & ") AS AVAIL_QTY")
        '    AVAIL_QTY = CInt((ds5.Tables(0).Rows(0)("AVAIL_QTY")))
        '    If dt0.Rows.Count > 0 Then
        '        For Each gvrow2 As DataRow In dt0.Rows
        '            FNL_QTY = FNL_QTY + CDbl(gvrow("QTY"))
        '        Next
        '    End If
        '    If FNL_QTY > AVAIL_QTY Then
        '        lblError.Text = "Item (" & gvrow("ITEM_DESCR") & ") quantity higher than available quantity. Available quantity is " & AVAIL_QTY
        '        lblError.CssClass = "alert alert-warning"
        '        Exit Sub
        '    End If
        'Next




        Dim dtFrom As DateTime
        dtFrom = CDate(lblDate.Text)
        'Dim str_new_FCL_ID As Long
        'Dim str_NEW_FCL_RECNO As String = ""
        Dim StuNos As String = ""
        Dim str_conn As String = ConnectionManger.GetOASIS_PUR_INVConnectionString
        Dim objConn As New SqlConnection(str_conn) '
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try
            Dim retval As Integer = 0
            Dim STR_TYPE As Char = "S"
            Dim BSAHO_ID As Int64 = 0
            Dim BOL_SAVE_COURIER As Int64 = 0
            Dim BSAHO_BSAHO_ID As Int64 = 0
            Dim BSAHO_MERCHANT_ID As String = "0"
            Dim LoopTotal As Double = 0
            Dim courier_amount As Decimal = 0.0
            Dim courier_amt As Decimal = 0

            Dim pParms(2) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@OPTIONS", SqlDbType.Int)
            pParms(0).Value = 1
            pParms(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.Int)
            pParms(1).Value = Session("sBSUID")
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_PUR_INVConnectionString, CommandType.StoredProcedure, "[dbo].[GET_COURIER_FEE]", pParms)
            courier_amount = Convert.ToDecimal(ds.Tables(0).Rows(0)("COURIER_FEE").ToString())

            For Each repitm As RepeaterItem In repInfo.Items
                Dim GridSelBook As New GridView

                GridSelBook = DirectCast(repitm.FindControl("grdSAL"), GridView)
                Dim hfSTU_ID As HiddenField = CType(repitm.FindControl("hfSTU_ID"), HiddenField)
                Dim hfSTU_FEE_ID As HiddenField = CType(repitm.FindControl("hfSTU_FEE_ID"), HiddenField)
                Dim hfSTU_ACD_ID As HiddenField = CType(repitm.FindControl("hfSTU_ACD_ID"), HiddenField)
                Dim lblStuNo As Label = CType(repitm.FindControl("lbSNo"), Label)
                Dim lblStuName As Label = CType(repitm.FindControl("lbSName"), Label)
                Dim lblGrade As Label = CType(repitm.FindControl("lbGrade"), Label)
                Dim lblSection As Label = CType(repitm.FindControl("lbSection"), Label)
                'Dim lblAmounttoPayF As Label = CType(gvfee.FooterRow.FindControl("lblAmounttoPayF"), Label)
                Dim lblAmounttoPayF As Label = DirectCast(repitm.FindControl("txtGrandTotal"), Label) 'gives total amount paying now of current student
                'Dim h_ProcessingChargeF As HiddenField = DirectCast(repitm.FindControl("h_ProcessingChargeF"), HiddenField) 'gives total processing charge of current student

                If Convert.ToDouble(lblAmounttoPayF.Text) > 0 Then
                    vpc_Amount = vpc_Amount + Convert.ToDouble(lblAmounttoPayF.Text)
                    StuNos = StuNos & IIf(StuNos = "", TrimStudentNo(hfSTU_FEE_ID.Value), "," & TrimStudentNo(hfSTU_FEE_ID.Value))

                    'Dim ds5 As New DataSet
                    Dim F_Year As String = "2019"
                    'ds5 = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT FYR_ID,FYR_DESCR,FYR_FROMDT  ,FYR_TODT from FINANCIALYEAR_S WHERE  bDefault=1")
                    'F_Year = (ds5.Tables(0).Rows(0)("FYR_ID").ToString())


                    Dim BSAH_NO As String = "NEW"
                    'SAVING HEADER DATA
                    retval = clsBookSalesOnline.SAVE_BOOK_SALE_H_ONLINE(BSAHO_ID, BSAHO_BSAHO_ID, BSAH_NO, Session("sBsuid"), _
                    lblDate.Text, "S", Session("username"), hfSTU_ID.Value, lblStuNo.Text, lblStuName.Text, lblGrade.Text, lblSection.Text, _
                     F_Year, "", lblAmounttoPayF.Text, "", "", 0, 0, lblAmounttoPayF.Text, _
                    lblAmounttoPayF.Text, 0, "", 0, _
                    "", 0, lblAmounttoPayF.Text, rblPaymentGateway.SelectedItem.Value, rbl_deliverytype.SelectedValue, hf_CollectionMessageID.Value, stTrans)
                    If BSAHO_BSAHO_ID = 0 Then
                        BSAHO_BSAHO_ID = BSAHO_ID
                        BSAHO_MERCHANT_ID = "BKS" & BSAHO_ID
                    End If
                    If BSAHO_ID = 0 Then
                        BSAHO_ID = BSAHO_ID
                    End If

                    ''Dim ds7 As New DataSet
                    'Dim p_Receiptno As String = ""
                    ''ds7 = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_PUR_INVConnectionString, CommandType.Text, "SELECT BSAH_NO from [dbo].[BOOK_SALE_H] WHERE  BSAH_ID=" & BSAHO_ID)
                    ''p_Receiptno = (ds7.Tables(0).Rows(0)("BSAH_NO").ToString())
                    'p_Receiptno = BSAH_NO
                    'Dim barcode As BaseBarcode
                    'barcode = BarcodeFactory.GetBarcode(Symbology.Code128)

                    'barcode.Number = p_Receiptno
                    'barcode.BackColor = Drawing.ColorTranslator.FromHtml("#FFFFFF")
                    'barcode.ChecksumAdd = True
                    'barcode.CustomText = p_Receiptno
                    'barcode.NarrowBarWidth = 3
                    'barcode.Height = 300
                    'barcode.FontHeight = 0.3F
                    'barcode.ForeColor = Drawing.Color.Black
                    'Dim b As Byte()
                    'ReDim b(barcode.Render(ImageType.Png).Length)
                    'b = barcode.Render(ImageType.Png)


                    'retval = clsBookSalesOnline.UPDATE_BARCODE(1, BSAHO_ID, Session("sBsuid"), b, stTrans)
                    Dim RETURN_MESSGAE_VALUE As String = ""
                    If retval = 0 Then

                        If rbl_deliverytype.SelectedItem.Value = 2 And BOL_SAVE_COURIER = 0 Then
                            Dim rblist As New CheckBoxList
                            Dim rbvalue As String = ""

                            rblist = DirectCast(repitm.FindControl("rdbBookSets"), CheckBoxList)
                            For i As Integer = 0 To rblist.Items.Count - 1
                                If rblist.Items(i).Selected Then

                                    Dim dt00 As New DataTable
                                    Dim filter_str0 As String = ""
                                    Dim dtView0 As DataView = New DataView(SALGRD)
                                    filter_str0 = " BSH_ID=" & rblist.Items(i).Value & " AND QTY <> ORG_QTY "
                                    dtView0.RowFilter = filter_str0
                                    dt00 = dtView0.ToTable
                                    If dt00.Rows.Count >= 1 Then
                                    Else
                                        rbvalue = rbvalue + rblist.Items(i).Value + "|"
                                    End If
                                    'rbvalue = rbvalue + rblist.Items(i).Value + "|"
                                End If
                            Next


                            If rbvalue = "" Then
                                courier_amt = courier_amount
                            Else
                                courier_amt = 0
                            End If

                            BOL_SAVE_COURIER = 1
                            retval = clsBookSalesOnline.SAVE_BOOK_SALE_COURIER_DETAILS(0, BSAHO_BSAHO_ID, txt_address.Text, txt_contactno1.Text, txt_contactno2.Text, txt_contactperson.Text, txt_cityname.Text, courier_amt, stTrans)
                            If retval <> 0 Then
                                'stTrans.Rollback()
                                Exit For
                            End If
                        End If

                        Dim dtView As DataView = New DataView(SALGRD)
                        SAL_GRD = Nothing
                        SAL_GRD = dtView.ToTable
                        SAL_GRD.Columns.Remove("ORG_QTY")
                        SAL_GRD.Columns.Remove("TAX_DESCR")
                        retval = clsBookSalesOnline.SAVE_BOOK_SALE_ONLINE_DTLS(BSAHO_ID, SAL_GRD, lblStuNo.Text.ToString, Session("sBSUID").ToString, RETURN_MESSGAE_VALUE, stTrans)
                        If retval = -1 Then
                            'usrMessageBar2.ShowNotification("Item (" & gvrow("ITEM_DESCR") & ") quantity higher than available quantity. Available quantity is " & AVAIL_QTY, UserControls_usrMessageBar.WarningType.Danger)
                            Qty_Error = RETURN_MESSGAE_VALUE '"Item quantity higher than available quantity. Available quantity"
                            'retval = -1
                            Qty_Error_Check = 1
                            Exit For
                        End If


                        'Dim dt As New DataTable '= SALGRD
                        'If Not SALGRD Is Nothing Then
                        '    Try
                        '        Dim query As IEnumerable(Of DataRow) = From order In SALGRD.AsEnumerable() Where order.Field(Of String)("STU_NO") = lblStuNo.Text.ToString Select order
                        '        dt = query.CopyToDataTable()
                        '    Catch ex As Exception
                        '    End Try
                        'Else
                        '    'dt = SALGRD
                        'End If
                        'If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                        '    'Dim dbl_net_amt As Double
                        '    For Each gvrow As DataRow In dt.Rows
                        '        If lblStuNo.Text.ToString = gvrow("STU_NO").ToString Then
                        '            If CDbl(gvrow("NET_AMOUNT")) > 0 Then

                        '                'If GET_QTY(gvrow("ITEM_ID").ToString) <= 0 Then
                        '                '    Qty_Error = Qty_Error & gvrow("BOOK_NAME").ToString & "</br>"
                        '                '    retval = -1
                        '                '    Qty_Error_Check = 1
                        '                '    Exit For
                        '                'End If

                        '                'checking the quantity before saving line item
                        '                Dim FNL_QTY As Integer = 0
                        '                Dim dt0 As New DataTable
                        '                Dim filter_str As String = ""
                        '                Dim dtView As DataView = New DataView(SALGRD)
                        '                filter_str = " ITEM_ID=" & gvrow("ITEM_ID") & ""
                        '                dtView.RowFilter = filter_str
                        '                dt0 = dtView.ToTable
                        '                Dim ds50 As New DataSet
                        '                Dim AVAIL_QTY As Integer = 0
                        '                ds50 = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_PUR_INVConnectionString, CommandType.Text, "SELECT [dbo].[GET_BAL_ITEM_QTY] ('" & Session("sBSUID") & "'," & gvrow("ITEM_ID") & ") AS AVAIL_QTY")
                        '                AVAIL_QTY = CInt((ds50.Tables(0).Rows(0)("AVAIL_QTY")))
                        '                If dt0.Rows.Count > 0 Then
                        '                    For Each gvrow2 As DataRow In dt0.Rows
                        '                        FNL_QTY = FNL_QTY + CDbl(gvrow2("QTY"))
                        '                    Next
                        '                End If
                        '                If FNL_QTY > AVAIL_QTY Then
                        '                    'usrMessageBar2.ShowNotification("Item (" & gvrow("ITEM_DESCR") & ") quantity higher than available quantity. Available quantity is " & AVAIL_QTY, UserControls_usrMessageBar.WarningType.Danger)
                        '                    Qty_Error = "Item (" & gvrow("ITEM_DESCR") & ") quantity higher than available quantity. Available quantity is " & AVAIL_QTY
                        '                    retval = -1
                        '                    Qty_Error_Check = 1
                        '                    Exit For
                        '                End If

                        '                'SAVING CHILD DATA
                        '                retval = clsBookSalesOnline.SAVE_BOOK_SALE_ONLINE_D(0, BSAHO_ID, CInt(gvrow("BSH_ID")), CDbl(gvrow("ITEM_ID")), _
                        '                       gvrow("ITEM_DESCR"), (gvrow("BOOK_NAME")), CDbl(gvrow("QTY")), CDbl(gvrow("PRICE")),
                        '                        CDbl(0), CDbl(gvrow("NET_AMOUNT")), CStr(gvrow("TAX_CODE")), CDbl(gvrow("TAX_AMOUNT")), stTrans)
                        '                If retval <> 0 Then
                        '                    'stTrans.Rollback()
                        '                    Exit For
                        '                End If
                        '            End If
                        '        End If

                        '    Next
                        'End If
                    Else
                        'stTrans.Rollback()
                        Exit For
                    End If

                End If

                BSAHO_ID = 0

            Next
            If retval = 0 And Qty_Error_Check = 0 Then
                stTrans.Commit()
                'btnSave.Enabled = False
                'SendEmailNotification(2, BSAHO_BSAHO_ID, Session("sBsuid"), "BOOKSALE", "GEMS")

                If rbl_deliverytype.SelectedItem.Value = 2 Then
                    vpc_Amount = vpc_Amount + courier_amt
                End If
                Session("vpc_Amount") = CStr(vpc_Amount * 100).Split(".")(0)
                '  vpc_OrderInfo = "Payment for " & StuNos
                vpc_OrderInfo = "RefNo-" & BSAHO_MERCHANT_ID.ToString & "," & "Payment for " & StuNos
                If vpc_OrderInfo.Length > 34 Then
                    vpc_OrderInfo = Left(vpc_OrderInfo, 34)
                End If
                ' vpc_ReturnURL = Request.Url.ToString.Replace("FeeCollectionOnlineNew.aspx", "feePaymentResultPage.aspx")
                Session("vpc_ReturnURL") = Request.Url.ToString.ToLower.Replace("booksalesonline.aspx", "BookSalePaymentResult.aspx")
                'Session("vpc_ReturnURL") = UtilityObj.StringReplace(Session("vpc_ReturnURL"), "http://", "https://")
                Session("vpc_MerchTxnRef") = BSAHO_MERCHANT_ID
                Session("CPS_ID") = rblPaymentGateway.SelectedItem.Value
                'Session("FeeCollection").rows.clear()
                ' gvFeeCollection.DataSource = Session("FeeCollection")
                Session("vpc_OrderInfo") = vpc_OrderInfo
                ' gvFeeCollection.DataBind()
                Dim flagAudit As Integer = UtilityObj.operOnAudiTable("Book Sale Online Payment", BSAHO_BSAHO_ID, "Insert", Page.User.Identity.Name.ToString, Me.Page, Server.MachineName)
                'lblError.Text = "<br />" & "Please note reference no. <B>" & str_new_FCL_ID & "</B><br />" & "Now press Proceed to continue.you are about to pay a amount of<B>" & Session("BSU_CURRENCY") & "." & dblTotal & "</B> "
                'lblError.CssClass = "divsuccess"
                Dim message1 = "Click on Confirm & Proceed to continue with this payment for " & IIf(Session("BSU_CURRENCY") Is Nothing, "", Session("BSU_CURRENCY")) & " " & Format(vpc_Amount, Session("BSU_DataFormatString")) & ""
                Dim message2 = "Please note reference no. <STRONG> " & BSAHO_MERCHANT_ID & "</STRONG> of this transaction for any future communication."
                Dim message3 = "Please do not close this browser or Log off until you get the final receipt."
                If Request.Browser.Type.ToUpper.Contains("IE") Then
                    Me.divboxpanelconfirm.Attributes("class") = "darkPanelIETop"
                Else
                    Me.divboxpanelconfirm.Attributes("class") = "darkPanelMTop"
                End If
                Me.btnSave.Visible = False
                Me.testpopup.Style.Item("display") = "block"
                lblMsg.Text = message1 & "<br />" & message2
                lblMsg2.Text = message3
                'lblPayMsg.Text = "Please do not close this window or Log off until you get the final receipt."
                'Me.tr_comment.Visible = True
                'Panel1.Visible = False
                bPaymentConfirmMsgShown = True
                boolPaymentInitiated = True
            Else
                stTrans.Rollback()


                If Qty_Error_Check = 1 Then
                    'Qty_Error = "Below Items has Zero Quantity </br>" & Qty_Error
                    lblError.Text = Qty_Error
                    lblError.CssClass = "alert alert-warning"
                Else
                    lblError.Text = getErrorMessage(retval)
                    lblError.CssClass = "alert alert-warning"

                End If

            End If
        Catch ex As Exception
            stTrans.Rollback()
            lblError.Text = getErrorMessage(1000)
            lblError.CssClass = "alert alert-warning"
            Errorlog(ex.Message)


        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub
    Public Shared Function SendEmailNotification(ByVal OPTIONS As Integer, ByVal STU_ID As String, ByVal BSU_ID As String, ByVal EML_TYPE As String, ByVal COMPANY As String) As Integer
        Dim pParms(5) As SqlClient.SqlParameter

        pParms(0) = New SqlClient.SqlParameter("@OPTION", SqlDbType.Int)
        pParms(0).Value = OPTIONS
        pParms(1) = New SqlClient.SqlParameter("@STU_ID", SqlDbType.VarChar, 1000)
        pParms(1).Value = STU_ID
        pParms(2) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 50)
        pParms(2).Value = BSU_ID
        pParms(3) = New SqlClient.SqlParameter("@EML_TYPE", SqlDbType.VarChar, 100)
        pParms(3).Value = EML_TYPE
        pParms(4) = New SqlClient.SqlParameter("@COMPANY", SqlDbType.VarChar, 50)
        pParms(4).Value = COMPANY

        Dim ReturnFlag As Integer = SqlHelper.ExecuteNonQuery(ConnectionManger.GetOASISConnection, _
         CommandType.StoredProcedure, "OASIS.[DBO].[BULK_EMAIL_INSERT_SCHEDULE_JOB]", pParms)
        Return ReturnFlag

    End Function
    Protected Function GET_QTY(ByVal STR_ITEM_ID As String) As Integer
        Dim QTY As Integer = 0
        Dim pParms(2) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@OPTIONS", SqlDbType.Int)
        pParms(0).Value = 1
        pParms(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.Int)
        pParms(1).Value = Session("sBSUID")
        pParms(2) = New SqlClient.SqlParameter("@BIM_ID", SqlDbType.VarChar, 20)
        pParms(2).Value = STR_ITEM_ID
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_PUR_INVConnectionString, CommandType.StoredProcedure, "[dbo].[GET_ITEM_QTY]", pParms)
        If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
            QTY = CInt(ds.Tables(0).Rows(0)("QTY"))
        End If
        Return QTY
    End Function
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        bPaymentConfirmMsgShown = False
        Response.Redirect("~\Home.aspx")
    End Sub
    Protected Sub rdbBookSets_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) 'Handles rdbBookSets.SelectedIndexChanged
        Dim obj As Object = sender.parent
        Dim GridSelBook As New GridView
        Dim DivHeaderRow As New HtmlGenericControl
        Dim DivMainContent As New HtmlGenericControl
        Dim DivFooterRow As New HtmlGenericControl
        Dim rblist As New CheckBoxList
        Dim txtTotal As New Label

        Dim Qty_Error As String = ""
        GridSelBook = DirectCast(obj.FindControl("grdSAL"), GridView)
        DivHeaderRow = DirectCast(obj.FindControl("DivHeaderRow"), HtmlGenericControl)
        DivMainContent = DirectCast(obj.FindControl("DivMainContent"), HtmlGenericControl)
        DivFooterRow = DirectCast(obj.FindControl("DivFooterRow"), HtmlGenericControl)
        rblist = DirectCast(obj.FindControl("rdbBookSets"), CheckBoxList)
        txtTotal = DirectCast(obj.FindControl("txtGrandTotal"), Label)


        Dim lbSNo As New Label
        lbSNo = DirectCast(obj.FindControl("lbSNo"), Label)
        Dim lblStuNo As Label = DirectCast(obj.FindControl("lbSNo"), Label)

        Dim dt0 As New DataTable '= SALGRD
        Dim dt1 As New DataTable '= SALGRD
        If Not SALGRD Is Nothing Then

            Try
                'Dim query0 As IEnumerable(Of DataRow) = From order In SALGRD.AsEnumerable() Where order.Field(Of String)("STU_NO") = lblStuNo.Text.ToString And order.Field(Of String)("BSH_ID") <> "-1" Select order
                '   dt0 = query0.CopyToDataTable()               

                Dim filter_str As String = ""
                Dim dtView As DataView = New DataView(SALGRD)
                filter_str = "BSH_ID = -1 AND STU_NO=" & lblStuNo.Text.ToString
                dtView.RowFilter = filter_str
                dt0 = dtView.ToTable
                Dim incr As Integer = 1
                If Not dt0 Is Nothing AndAlso dt0.Rows.Count > 0 Then
                    For Each gvrow As DataRow In dt0.Rows
                        gvrow("ID") = incr
                        incr = incr + 1
                    Next
                End If


                dtView = New DataView(SALGRD)
                filter_str = " STU_NO <>" & lblStuNo.Text.ToString
                dtView.RowFilter = filter_str
                dt1 = dtView.ToTable
                SALGRD = dt0
                SALGRD.Merge(dt1)


            Catch ex As Exception
            End Try

            'add to salgrd   
            Dim rbvalue As String = "" '= rblist.SelectedValue
            For i As Integer = 0 To rblist.Items.Count - 1
                If rblist.Items(i).Selected Then
                    rbvalue = rbvalue + rblist.Items(i).Value + "|"
                End If
            Next

            If Not SALGRD Is Nothing And rbvalue <> "" Then

                Dim dt As DataTable = GetTable()
                'Dim dt2 As DataTable = GetTable()

                GridSelBook.Visible = True
                GridSelBook.ShowFooter = False


                Dim pParms(2) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@OPTIONS", SqlDbType.Int)
                pParms(0).Value = 1
                pParms(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
                pParms(1).Value = Session("sBSUID")
                pParms(2) = New SqlClient.SqlParameter("@BSH_ID", SqlDbType.VarChar, 200)
                pParms(2).Value = rbvalue

                Dim ds As New DataSet
                ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_PUR_INVConnectionString, CommandType.StoredProcedure, "dbo.[GET_BOOK_SET_ITEM_M]", pParms)
                If Not SALGRD Is Nothing Then
                    Try
                        Dim query As IEnumerable(Of DataRow) = From order In SALGRD.AsEnumerable() Where order.Field(Of String)("STU_NO") = lbSNo.Text.ToString Select order
                        dt = query.CopyToDataTable()

                    Catch ex As Exception
                    End Try
                    'dt = SALGRD
                    If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                        Dim Row_Count As Integer = dt.Rows.Count + 1
                        For Each gvrow As DataRow In ds.Tables(0).Rows
                            Dim dr As DataRow
                            dr = dt.NewRow()

                            'If GET_QTY(gvrow("ITEM_ID").ToString) <= 0 Then
                            '    Qty_Error = Qty_Error & gvrow("BOOK_NAME").ToString & "</br>"
                            'End If
                            Dim FNL_QTY As Integer = 0
                            Dim ds5 As New DataSet
                            Dim AVAIL_QTY As Integer = 0
                            ds5 = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_PUR_INVConnectionString, CommandType.Text, "SELECT [dbo].[GET_BAL_ITEM_QTY] ('" & Session("sBSUID") & "'," & gvrow("ITEM_ID").ToString & ") AS AVAIL_QTY")
                            AVAIL_QTY = CInt((ds5.Tables(0).Rows(0)("AVAIL_QTY")))
                            'in plus, check item multiple times in salgrd
                            Dim dt00 As New DataTable
                            Dim filter_str0 As String = ""
                            Dim dtView0 As DataView = New DataView(SALGRD)
                            filter_str0 = " ITEM_ID=" & gvrow("ITEM_ID").ToString & ""
                            dtView0.RowFilter = filter_str0
                            dt00 = dtView0.ToTable
                            If dt00.Rows.Count > 0 Then
                                For Each gvrow2 As DataRow In dt00.Rows
                                    FNL_QTY = FNL_QTY + CDbl(gvrow2("QTY"))
                                Next
                            End If
                            FNL_QTY = FNL_QTY + 1
                            If FNL_QTY > AVAIL_QTY Then
                                'usrMessageBar2.ShowNotification("Item (" & lbl_BOOK_NAME.Text & ") quantity higher than available quantity. Available quantity is " & AVAIL_QTY, UserControls_usrMessageBar.WarningType.Danger)
                                'Exit Sub
                            Else

                                dr("ID") = Row_Count 'gvrow("ID").ToString
                                dr("BSH_ID") = gvrow("BSH_ID").ToString
                                dr("ITEM_ID") = gvrow("ITEM_ID").ToString
                                dr("ITEM_DESCR") = gvrow("ITEM_DESCR").ToString
                                dr("BOOK_NAME") = gvrow("BOOK_NAME").ToString
                                dr("ISBN") = gvrow("ISBN").ToString
                                dr("PRICE") = gvrow("PRICE").ToString
                                dr("QTY") = gvrow("QTY").ToString
                                dr("TAX_CODE") = gvrow("TAX_CODE").ToString
                                dr("TAX_AMOUNT") = gvrow("TAX_AMOUNT").ToString
                                dr("NET_AMOUNT") = gvrow("NET_AMOUNT").ToString
                                dr("STU_NO") = lbSNo.Text

                                dr("MINUS_BTN_CSS") = gvrow("MINUS_BTN_CSS").ToString
                                dr("PLUS_BTN_CSS") = gvrow("PLUS_BTN_CSS").ToString
                                dr("TAX_DESCR") = gvrow("TAX_DESCR").ToString
                                dr("ORG_QTY") = gvrow("QTY").ToString
                                dt.Rows.Add(dr)
                                SALGRD.ImportRow(dr)
                                Row_Count = Row_Count + 1
                            End If
                        Next
                    End If

                Else
                    'dt = SALGRD
                    If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                        For Each gvrow As DataRow In ds.Tables(0).Rows
                            Dim dr As DataRow
                            dr = dt.NewRow()

                            'If GET_QTY(gvrow("ITEM_ID").ToString) <= 0 Then
                            '    Qty_Error = Qty_Error & gvrow("BOOK_NAME").ToString & "</br>"
                            'End If
                            Dim FNL_QTY As Integer = 0
                            Dim ds5 As New DataSet
                            Dim AVAIL_QTY As Integer = 0
                            ds5 = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_PUR_INVConnectionString, CommandType.Text, "SELECT [dbo].[GET_BAL_ITEM_QTY] ('" & Session("sBSUID") & "'," & gvrow("ITEM_ID").ToString & ") AS AVAIL_QTY")
                            AVAIL_QTY = CInt((ds5.Tables(0).Rows(0)("AVAIL_QTY")))
                            'in plus, check item multiple times in salgrd
                            Dim dt00 As New DataTable
                            Dim filter_str0 As String = ""
                            Dim dtView0 As DataView = New DataView(SALGRD)
                            filter_str0 = " ITEM_ID=" & gvrow("ITEM_ID").ToString & ""
                            dtView0.RowFilter = filter_str0
                            dt00 = dtView0.ToTable
                            If dt00.Rows.Count > 0 Then
                                For Each gvrow2 As DataRow In dt00.Rows
                                    FNL_QTY = FNL_QTY + CDbl(gvrow2("QTY"))
                                Next
                            End If
                            FNL_QTY = FNL_QTY + 1
                            If FNL_QTY > AVAIL_QTY Then
                                'usrMessageBar2.ShowNotification("Item (" & lbl_BOOK_NAME.Text & ") quantity higher than available quantity. Available quantity is " & AVAIL_QTY, UserControls_usrMessageBar.WarningType.Danger)
                                'Exit Sub
                            Else

                                dr("ID") = gvrow("ID").ToString
                                dr("BSH_ID") = gvrow("BSH_ID").ToString
                                dr("ITEM_ID") = gvrow("ITEM_ID").ToString
                                dr("ITEM_DESCR") = gvrow("ITEM_DESCR").ToString
                                dr("BOOK_NAME") = gvrow("BOOK_NAME").ToString
                                dr("ISBN") = gvrow("ISBN").ToString
                                dr("PRICE") = gvrow("PRICE").ToString
                                dr("QTY") = gvrow("QTY").ToString
                                dr("TAX_CODE") = gvrow("TAX_CODE").ToString
                                dr("TAX_AMOUNT") = gvrow("TAX_AMOUNT").ToString
                                dr("NET_AMOUNT") = gvrow("NET_AMOUNT").ToString
                                dr("STU_NO") = lbSNo.Text

                                dr("MINUS_BTN_CSS") = gvrow("MINUS_BTN_CSS").ToString
                                dr("PLUS_BTN_CSS") = gvrow("PLUS_BTN_CSS").ToString
                                dr("TAX_DESCR") = gvrow("TAX_DESCR").ToString
                                dr("ORG_QTY") = gvrow("QTY").ToString
                                dt.Rows.Add(dr)
                            End If
                        Next
                    End If
                    SALGRD = dt

                End If


                'GridSelBook.Height = 240
                GridSelBook.DataSource = dt
                GridSelBook.DataBind()
                CalculateTotal()
            Else

                Dim filter_str As String = ""
                Dim dtView As DataView = New DataView(SALGRD)
                filter_str = "STU_NO=" & lblStuNo.Text.ToString
                dtView.RowFilter = filter_str
                dt0 = dtView.ToTable
                'GridSelBook.Height = 240
                GridSelBook.DataSource = dt0
                GridSelBook.DataBind()
                CalculateTotal()
                'If dt0.Rows.Count <> 0 Then
                '    'Try
                '    'Dim dt As DataTable = GetTable()
                '    'Dim query As IEnumerable(Of DataRow) = From order In SALGRD.AsEnumerable() Where order.Field(Of String)("STU_NO") = lbSNo.Text.ToString Select order
                '    'dt = query.CopyToDataTable()
                '    'Catch ex As Exception
                '    'End Try
                '    GridSelBook.Visible = True
                '    GridSelBook.ShowFooter = False
                'Else
                '    GridSelBook.Visible = False
                '    GridSelBook.ShowFooter = False
                'End If
            End If

        Else
            'dt = SALGRD


            Dim dt As DataTable = GetTable()
            'Dim dt2 As DataTable = GetTable()

            GridSelBook.Visible = True
            GridSelBook.ShowFooter = False
            Dim rbvalue As String = "" '= rblist.SelectedValue

            For i As Integer = 0 To rblist.Items.Count - 1
                If rblist.Items(i).Selected Then
                    rbvalue = rbvalue + rblist.Items(i).Value + "|"
                End If
            Next

            Dim pParms(2) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@OPTIONS", SqlDbType.Int)
            pParms(0).Value = 1
            pParms(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            pParms(1).Value = Session("sBSUID")
            pParms(2) = New SqlClient.SqlParameter("@BSH_ID", SqlDbType.VarChar, 200)
            pParms(2).Value = rbvalue

            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_PUR_INVConnectionString, CommandType.StoredProcedure, "dbo.[GET_BOOK_SET_ITEM_M]", pParms)
            If Not SALGRD Is Nothing Then
                Try
                    Dim query As IEnumerable(Of DataRow) = From order In SALGRD.AsEnumerable() Where order.Field(Of String)("STU_NO") = lbSNo.Text.ToString Select order
                    dt = query.CopyToDataTable()

                Catch ex As Exception
                End Try
                'dt = SALGRD
                If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                    Dim Row_Count As Integer = dt.Rows.Count + 1
                    For Each gvrow As DataRow In ds.Tables(0).Rows
                        Dim dr As DataRow
                        dr = dt.NewRow()

                        'If GET_QTY(gvrow("ITEM_ID").ToString) <= 0 Then
                        '    Qty_Error = Qty_Error & gvrow("BOOK_NAME").ToString & "</br>"
                        'End If
                        Dim FNL_QTY As Integer = 0
                        Dim ds5 As New DataSet
                        Dim AVAIL_QTY As Integer = 0
                        ds5 = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_PUR_INVConnectionString, CommandType.Text, "SELECT [dbo].[GET_BAL_ITEM_QTY] ('" & Session("sBSUID") & "'," & gvrow("ITEM_ID").ToString & ") AS AVAIL_QTY")
                        AVAIL_QTY = CInt((ds5.Tables(0).Rows(0)("AVAIL_QTY")))
                        'in plus, check item multiple times in salgrd
                        Dim dt00 As New DataTable
                        Dim filter_str0 As String = ""
                        Dim dtView0 As DataView = New DataView(SALGRD)
                        filter_str0 = " ITEM_ID=" & gvrow("ITEM_ID").ToString & ""
                        dtView0.RowFilter = filter_str0
                        dt00 = dtView0.ToTable
                        If dt00.Rows.Count > 0 Then
                            For Each gvrow2 As DataRow In dt00.Rows
                                FNL_QTY = FNL_QTY + CDbl(gvrow2("QTY"))
                            Next
                        End If
                        FNL_QTY = FNL_QTY + 1
                        If FNL_QTY > AVAIL_QTY Then
                            'usrMessageBar2.ShowNotification("Item (" & lbl_BOOK_NAME.Text & ") quantity higher than available quantity. Available quantity is " & AVAIL_QTY, UserControls_usrMessageBar.WarningType.Danger)
                            'Exit Sub
                        Else
                            dr("ID") = Row_Count 'gvrow("ID").ToString
                            dr("BSH_ID") = gvrow("BSH_ID").ToString
                            dr("ITEM_ID") = gvrow("ITEM_ID").ToString
                            dr("ITEM_DESCR") = gvrow("ITEM_DESCR").ToString
                            dr("BOOK_NAME") = gvrow("BOOK_NAME").ToString
                            dr("ISBN") = gvrow("ISBN").ToString
                            dr("PRICE") = gvrow("PRICE").ToString
                            dr("QTY") = gvrow("QTY").ToString
                            dr("TAX_CODE") = gvrow("TAX_CODE").ToString
                            dr("TAX_AMOUNT") = gvrow("TAX_AMOUNT").ToString
                            dr("NET_AMOUNT") = gvrow("NET_AMOUNT").ToString
                            dr("STU_NO") = lbSNo.Text

                            dr("MINUS_BTN_CSS") = gvrow("MINUS_BTN_CSS").ToString
                            dr("PLUS_BTN_CSS") = gvrow("PLUS_BTN_CSS").ToString
                            dr("TAX_DESCR") = gvrow("TAX_DESCR").ToString
                            dr("ORG_QTY") = gvrow("QTY").ToString
                            dt.Rows.Add(dr)
                            SALGRD.ImportRow(dr)
                            Row_Count = Row_Count + 1
                        End If
                    Next
                End If
                'Dim dr1 As DataRow = dt.NewRow()
                'dr1("ID") = ds.Tables(0).Rows(0)("ID").ToString
                'dr1("BSH_ID") = ds.Tables(0).Rows(0)("BSH_ID").ToString
                'dr1("ITEM_DESCR") = ds.Tables(0).Rows(0)("ITEM_DESCR").ToString
                'dr1("ISBN") = ds.Tables(0).Rows(0)("ISBN").ToString
                'dr1("PRICE") = ds.Tables(0).Rows(0)("PRICE").ToString
                'dr1("QTY") = ds.Tables(0).Rows(0)("QTY").ToString
                'dr1("TAX_AMOUNT") = ds.Tables(0).Rows(0)("TAX_AMOUNT").ToString
                'dr1("NET_AMOUNT") = ds.Tables(0).Rows(0)("NET_AMOUNT").ToString
                'dt.Rows.Add(dr1)
                'SALGRD = dt
                'SALGRD.Merge(dt2)
            Else
                'dt = SALGRD
                If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                    For Each gvrow As DataRow In ds.Tables(0).Rows
                        Dim dr As DataRow
                        dr = dt.NewRow()

                        'If GET_QTY(gvrow("ITEM_ID").ToString) <= 0 Then
                        '    Qty_Error = Qty_Error & gvrow("BOOK_NAME").ToString & "</br>"
                        'End If
                        Dim FNL_QTY As Integer = 0
                        Dim ds5 As New DataSet
                        Dim AVAIL_QTY As Integer = 0
                        ds5 = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_PUR_INVConnectionString, CommandType.Text, "SELECT [dbo].[GET_BAL_ITEM_QTY] ('" & Session("sBSUID") & "'," & gvrow("ITEM_ID").ToString & ") AS AVAIL_QTY")
                        AVAIL_QTY = CInt((ds5.Tables(0).Rows(0)("AVAIL_QTY")))
                        'in plus, check item multiple times in salgrd
                        Dim dt00 As New DataTable
                        Dim filter_str0 As String = ""
                        Dim dtView0 As DataView = New DataView(SALGRD)
                        filter_str0 = " ITEM_ID=" & gvrow("ITEM_ID").ToString & ""
                        dtView0.RowFilter = filter_str0
                        dt00 = dtView0.ToTable
                        If dt00.Rows.Count > 0 Then
                            For Each gvrow2 As DataRow In dt00.Rows
                                FNL_QTY = FNL_QTY + CDbl(gvrow2("QTY"))
                            Next
                        End If
                        FNL_QTY = FNL_QTY + 1
                        If FNL_QTY > AVAIL_QTY Then
                            'usrMessageBar2.ShowNotification("Item (" & lbl_BOOK_NAME.Text & ") quantity higher than available quantity. Available quantity is " & AVAIL_QTY, UserControls_usrMessageBar.WarningType.Danger)
                            'Exit Sub
                        Else
                            dr("ID") = gvrow("ID").ToString
                            dr("BSH_ID") = gvrow("BSH_ID").ToString
                            dr("ITEM_ID") = gvrow("ITEM_ID").ToString
                            dr("ITEM_DESCR") = gvrow("ITEM_DESCR").ToString
                            dr("BOOK_NAME") = gvrow("BOOK_NAME").ToString
                            dr("ISBN") = gvrow("ISBN").ToString
                            dr("PRICE") = gvrow("PRICE").ToString
                            dr("QTY") = gvrow("QTY").ToString
                            dr("TAX_CODE") = gvrow("TAX_CODE").ToString
                            dr("TAX_AMOUNT") = gvrow("TAX_AMOUNT").ToString
                            dr("NET_AMOUNT") = gvrow("NET_AMOUNT").ToString
                            dr("STU_NO") = lbSNo.Text
                            dr("TAX_DESCR") = gvrow("TAX_DESCR").ToString
                            dr("ORG_QTY") = gvrow("QTY").ToString
                            dt.Rows.Add(dr)
                        End If
                    Next
                End If
                SALGRD = dt

            End If


            'GridSelBook.Height = 240
            GridSelBook.DataSource = dt
            GridSelBook.DataBind()
            CalculateTotal()



        End If

        If Qty_Error <> "" Then

            'Qty_Error = "Below Items has Zero Quantity </br>" & Qty_Error
            lblError.Text = Qty_Error
            lblError.CssClass = "alert alert-warning"
        End If
        'Dim k As String = GridSelBook.ClientID
        'Dim et As String = k(k.Length - 1)
        'If et = "0" Then
        '    ScriptManager.RegisterStartupScript(Page, Me.GetType(), "Key", "<script>MakeStaticHeader0('" & GridSelBook.ClientID & "','" & DivHeaderRow.ClientID & "','" & DivMainContent.ClientID & "','" & DivFooterRow.ClientID & "', 200, 1000 , 50 ,false); </script>", False)
        'ElseIf et = "1" Then
        '    ScriptManager.RegisterStartupScript(Page, Me.GetType(), "Key", "<script>MakeStaticHeader1('" & GridSelBook.ClientID & "','" & DivHeaderRow.ClientID & "','" & DivMainContent.ClientID & "','" & DivFooterRow.ClientID & "', 200, 1000 , 50 ,false); </script>", False)

        'End If
        ScriptManager.RegisterStartupScript(Page, Me.GetType(), "Key", "<script>MakeStaticHeader('" & GridSelBook.ClientID & "','" & DivHeaderRow.ClientID & "','" & DivMainContent.ClientID & "','" & DivFooterRow.ClientID & "', 200, 1000 , 50 ,false); </script>", False)

    End Sub
    Sub Gridbind_StuDetails()
        Try
            'Dim strPath As String = Convert.ToString(readerStudent_Detail("STU_PHOTOPATH"))
            Dim connPath As String = WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ConnectionString
            Dim noImagePath As String = "~/Images/Home/no_image.gif"
            Dim con As String = ConnectionManger.GetOASISConnectionString
            Dim param(3) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@OLU_ID", Session("OLU_ID").ToString)
            param(1) = New SqlClient.SqlParameter("@STU_BSU_ID", Session("STU_BSU_ID").ToString)
            param(2) = New SqlClient.SqlParameter("@connPath", connPath)
            param(3) = New SqlClient.SqlParameter("@noImagePath", noImagePath)
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "[OPL].[GET_BOOKSALE_CHILDINFO]", param)
            repInfo.DataSource = ds.Tables(0)
            repInfo.DataBind()

        Catch ex As Exception
            repInfo.DataBind()

        End Try
    End Sub
    Private Function checkIfCobrand(ByVal GatewayID As Int16) As Boolean
        checkIfCobrand = False
        Dim pParms(1) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(0).Value = Session("sBsuid")
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISFINConnectionString, _
          CommandType.StoredProcedure, "GetMerchantDetails_BSU_ID", pParms)
        If Not dsData Is Nothing AndAlso dsData.Tables.Count > 0 Then
            For Each row As DataRow In dsData.Tables(0).Rows
                If row("CPS_ID") = GatewayID And row("CPS_CPM_ID") = 3 Then
                    checkIfCobrand = True
                    Exit Function
                End If
            Next
            checkIfCobrand = False
            Exit Function
        End If
    End Function


    Sub ClearStudentData()
        Gridbind_StuDetails()
    End Sub
    Sub ClearDetail()
        'txtAmountAdd.Text = ""
    End Sub
    Protected Sub txtAmountToPay_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim myCell, myRow As Object
            myCell = sender.parent
            If Not myCell Is Nothing Then
                myRow = myCell.parent
                If Not myRow Is Nothing Then
                    Dim h_Discount As HiddenField = CType(myRow.FindControl("h_Discount"), HiddenField)
                    If Not h_Discount Is Nothing Then
                        h_Discount.Value = 0
                    End If
                End If
            End If
            bPaymentConfirmMsgShown = False

        Catch ex As Exception
        End Try
    End Sub

    Protected Sub lnkSetArea_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Gridbind_StuDetails()
    End Sub
    Protected Sub lbApply_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim dtDiscountGrid As DataTable
        dtDiscountGrid = FeeCommon.DIS_SelectDiscount(lblDate.Text, Session("STU_ID"), "S", Session("sBsuid"), "", False)

        Dim lblFee_ID As Label = sender.Parent.Parent.Findcontrol("lblFee_ID")
        Dim lblFds_ID As Label = sender.Parent.Parent.Findcontrol("lblFds_ID")
        Dim dtDiscountData As DataTable = FeeCollectionOnline.DIS_GetDiscount(lblFds_ID.Text, Session("STU_ID"), "S", lblDate.Text)
        If Not dtDiscountData Is Nothing AndAlso dtDiscountData.Rows.Count > 0 Then
        End If

    End Sub
    Protected Sub lbCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    End Sub
    Protected Sub rblPaymentGateway_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rblPaymentGateway.SelectedIndexChanged
        Gridbind_StuDetails()


    End Sub

    Protected Sub btnShow_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnShow.Click
        'Me.testpopup.Visible = True
        Me.testpopup.Style.Item("display") = "block"
        Me.lblMsg.Text = "Click on Confirm & Proceed to continue with this payment AED : 0.00" & "<br />" & "Please note reference no. 12345 of this transaction for any future communication."
    End Sub
    Protected Sub btnSkip_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSkip.Click
        Me.testpopup.Style.Item("display") = "none"
        Me.btnSave.Visible = True
        bPaymentConfirmMsgShown = False
    End Sub
    Protected Sub btnProceed_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnProceed.Click
        PaymentRedirect() 'Response.Redirect("BookSalePaymentRedirectNew.aspx")
    End Sub
    Protected Sub btnMinus_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim obj As Object = sender.parent
        Dim GridSelBook As New GridView
        'Dim rblist As New RadioButtonList
        Dim rblist As New CheckBoxList
        Dim txtTotal As New Label
        Dim lbSNo As New Label
        lbSNo = DirectCast(obj.FindControl("lbl_PID"), Label)
        GridSelBook = DirectCast(obj.FindControl("grdSAL"), GridView)
        Dim rowIndex = (CType((CType(sender, Control)).NamingContainer, GridViewRow)).RowIndex
        'GridSelBook.Rows(GridSelBook.SelectedIndex).Cells(1).Text = "7"
        'GridSelBook.Rows(0).Cells(3).Text = "7"
        Dim lbl1 As Label = DirectCast(obj.FindControl("lbl_QTY"), Label) '(Label)GridSelBook.FindControl("lblQuestion_Scripting");
        Dim lbl_ID As Label = DirectCast(obj.FindControl("lbl_ID"), Label)
        Dim Cur_Qty As Integer = CInt(lbl1.Text)
        Dim Cur_Net_Amt As Double = 0.0

        'id_show_add_items
        Dim id_show_add_items As Boolean = 0
        Dim pParms0(2) As SqlClient.SqlParameter
        pParms0(0) = New SqlClient.SqlParameter("@OPTIONS", SqlDbType.Int)
        pParms0(0).Value = 2
        pParms0(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.Int)
        pParms0(1).Value = Session("sBSUID")
        Dim ds0 As New DataSet
        ds0 = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_PUR_INVConnectionString, CommandType.StoredProcedure, "[dbo].[GET_SHOW_ADD_MINUS_ITEMS]", pParms0)
        id_show_add_items = Convert.ToBoolean(ds0.Tables(0).Rows(0)("IS_SHOW"))
        If id_show_add_items = True Then


            If Cur_Qty >= 1 Then
                Cur_Qty = Cur_Qty - 1
                lbl1.Text = "" & Cur_Qty

                Dim lbl2 As Label = DirectCast(obj.FindControl("lbl_PRICE"), Label)
                Dim lbl3 As Label = DirectCast(obj.FindControl("lbl_TAX_AMOUNT"), Label)
                Dim lbl4 As Label = DirectCast(obj.FindControl("lbl_NET_AMOUNT"), Label)
                Dim Cur_Price As Double = CDbl(lbl2.Text)
                Dim Cur_Tax_Amt As Double = CDbl(lbl3.Text)
                Cur_Net_Amt = CDbl(lbl4.Text)
                Cur_Net_Amt = (Cur_Price + Cur_Tax_Amt) * Cur_Qty
                lbl4.Text = "" & Cur_Net_Amt

                ''TAX AMOUNT SHOWS AS ZERO FOR ZERO QTY
                'If Cur_Qty = 0 Then
                '    lbl3.Text = "0.00"
                'End If

            End If

            For Each dr As DataRow In SALGRD.Rows

                If dr("ID") = lbl_ID.Text.ToString And lbSNo.Text.ToString = dr("STU_NO") Then
                    dr("QTY") = "" & Cur_Qty
                    dr("NET_AMOUNT") = "" & Cur_Net_Amt
                End If
            Next

            CalculateTotal()
        End If
    End Sub
    Protected Sub btnPlus_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim obj As Object = sender.parent
        Dim GridSelBook As New GridView
        'Dim rblist As New RadioButtonList
        Dim rblist As New CheckBoxList
        Dim txtTotal As New Label
        Dim lbSNo As New Label
        lbSNo = DirectCast(obj.FindControl("lbl_PID"), Label)
        'GridSelBook = DirectCast(Me.FindControl("grdSAL"), GridView)
        'Dim rowIndex = (CType((CType(sender, Control)).NamingContainer, GridViewRow)).RowIndex
        Dim lbl1 As Label = DirectCast(obj.FindControl("lbl_QTY"), Label) '(Label)GridSelBook.FindControl("lblQuestion_Scripting");
        Dim lbl_ID As Label = DirectCast(obj.FindControl("lbl_ID"), Label)
        Dim lbl_ITMID As Label = DirectCast(obj.FindControl("lbl_ITMID"), Label)
        Dim lbl_BOOK_NAME As Label = DirectCast(obj.FindControl("lbl_BOOK_NAME"), Label)
        Dim Cur_Qty As Integer = CInt(lbl1.Text)
        Dim Cur_Net_Amt As Double = 0.0

        Dim FNL_QTY As Integer = 0
        Dim ds5 As New DataSet
        Dim AVAIL_QTY As Integer = 0
        ds5 = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_PUR_INVConnectionString, CommandType.Text, "SELECT [dbo].[GET_BAL_ITEM_QTY] ('" & Session("sBSUID") & "'," & lbl_ITMID.Text & ") AS AVAIL_QTY")
        AVAIL_QTY = CInt((ds5.Tables(0).Rows(0)("AVAIL_QTY")))

        'in plus, check item multiple times in salgrd
        Dim dt00 As New DataTable
        Dim filter_str0 As String = ""
        Dim dtView0 As DataView = New DataView(SALGRD)
        filter_str0 = " ITEM_ID=" & lbl_ITMID.Text & ""
        dtView0.RowFilter = filter_str0
        dt00 = dtView0.ToTable



        If dt00.Rows.Count > 0 Then
            For Each gvrow2 As DataRow In dt00.Rows
                FNL_QTY = FNL_QTY + CDbl(gvrow2("QTY"))
            Next
        End If

        FNL_QTY = FNL_QTY + 1
        If FNL_QTY > AVAIL_QTY Then
            lblError.Text = "Item (" & lbl_BOOK_NAME.Text & ") quantity higher than available quantity. Available quantity is " & AVAIL_QTY
            lblError.CssClass = "alert alert-warning"

            Exit Sub
        End If


        'id_show_add_items
        Dim id_show_add_items As Boolean = 0
        Dim pParms0(2) As SqlClient.SqlParameter
        pParms0(0) = New SqlClient.SqlParameter("@OPTIONS", SqlDbType.Int)
        pParms0(0).Value = 2
        pParms0(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.Int)
        pParms0(1).Value = Session("sBSUID")
        Dim ds0 As New DataSet
        ds0 = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_PUR_INVConnectionString, CommandType.StoredProcedure, "[dbo].[GET_SHOW_ADD_MINUS_ITEMS]", pParms0)
        id_show_add_items = Convert.ToBoolean(ds0.Tables(0).Rows(0)("IS_SHOW"))
        If id_show_add_items = True And Cur_Qty < AVAIL_QTY Then 'not inside if 2<2

            If Cur_Qty >= 0 And Cur_Qty < 99 Then
                Cur_Qty = Cur_Qty + 1
                lbl1.Text = "" & Cur_Qty

                Dim lbl2 As Label = DirectCast(obj.FindControl("lbl_PRICE"), Label)
                Dim lbl3 As Label = DirectCast(obj.FindControl("lbl_TAX_AMOUNT"), Label)
                Dim lbl4 As Label = DirectCast(obj.FindControl("lbl_NET_AMOUNT"), Label)
                Dim Cur_Price As Double = CDbl(lbl2.Text)
                Dim Cur_Tax_Amt As Double = CDbl(lbl3.Text)
                Cur_Net_Amt = CDbl(lbl4.Text)
                Cur_Net_Amt = (Cur_Price + Cur_Tax_Amt) * Cur_Qty
                lbl4.Text = "" & Cur_Net_Amt
            End If

            For Each dr As DataRow In SALGRD.Rows

                If dr("ID") = lbl_ID.Text.ToString And lbSNo.Text.ToString = dr("STU_NO") Then

                    dr("QTY") = "" & Cur_Qty
                    dr("NET_AMOUNT") = "" & Cur_Net_Amt
                End If
            Next

            CalculateTotal()
        End If
    End Sub
    Private Sub CalculateTotal()
        Dim Cur_Net_Amt As Double
        Dim courier_amount As Decimal
        Dim Cur_VAT_Amt As Double
        Dim Cur_SubTotal_Amt As Double
        Dim Net_Pay_Amt As Double
        Dim rbvalue As String = "" '= rblist.SelectedValue
        For Each repitm As RepeaterItem In repInfo.Items
            Dim GridSelBook As New GridView
            Dim GridScroll As New Panel
            Dim lbSNo As New Label
            Dim DivHeaderRow As New HtmlGenericControl
            Dim DivMainContent As New HtmlGenericControl
            Dim DivFooterRow As New HtmlGenericControl

            Dim rblist As New CheckBoxList
            'Dim btnMinus1 As New ImageButton
            'Dim btnPlus1 As New ImageButton

            lbSNo = DirectCast(repitm.FindControl("lbSNo"), Label)
            GridSelBook = DirectCast(repitm.FindControl("grdSAL"), GridView)
            GridScroll = DirectCast(repitm.FindControl("GridScroll"), Panel)
            Dim lbl0 As Label = DirectCast(repitm.FindControl("txtGrandTotal"), Label)
            Dim lblSubTotal As Label = DirectCast(repitm.FindControl("txtSubTotal"), Label)
            Dim lblSubVATTotal As Label = DirectCast(repitm.FindControl("txtSubVATTotal"), Label)
            DivHeaderRow = DirectCast(repitm.FindControl("DivHeaderRow"), HtmlGenericControl)
            DivMainContent = DirectCast(repitm.FindControl("DivMainContent"), HtmlGenericControl)
            DivFooterRow = DirectCast(repitm.FindControl("DivFooterRow"), HtmlGenericControl)
            'btnMinus1 = DirectCast(repitm.FindControl("btnMinus"), ImageButton)
            'btnPlus1 = DirectCast(repitm.FindControl("btnPlus"), ImageButton)
            rblist = DirectCast(repitm.FindControl("rdbBookSets"), CheckBoxList)
            'For Each gvRow As GridViewRow In GridSelBook.Rows

            '    Dim lbl4 As Label = (CType(gvRow.FindControl("lbl_NET_AMOUNT"), Label))
            '    lbl4.Visible = False

            'Next
            Dim id_show_price_details As Boolean = 0
            Dim pParms0(2) As SqlClient.SqlParameter
            pParms0(0) = New SqlClient.SqlParameter("@OPTIONS", SqlDbType.Int)
            pParms0(0).Value = 1
            pParms0(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.Int)
            pParms0(1).Value = Session("sBSUID")
            Dim ds0 As New DataSet
            ds0 = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_PUR_INVConnectionString, CommandType.StoredProcedure, "[dbo].[GET_SHOW_PRICE_DETAILS]", pParms0)
            id_show_price_details = Convert.ToBoolean(ds0.Tables(0).Rows(0)("IS_SHOW"))
            If id_show_price_details = False Then
                GridSelBook.Columns(6).Visible = False
                GridSelBook.Columns(7).Visible = False
                GridSelBook.Columns(8).Visible = False
                GridSelBook.Columns(9).Visible = False
                GridSelBook.Columns(10).Visible = False
                GridSelBook.Columns(11).Visible = False
            End If
            'Dim lbl10 As Label = DirectCast(repitm.FindControl("lblTotal"), Label)
            'For Each gvRow As GridViewRow In GridSelBook.Rows

            '    Dim lbl4 As Label = (CType(gvRow.FindControl("lbl_NET_AMOUNT"), Label))
            '    Cur_Net_Amt = Cur_Net_Amt + CDbl(lbl4.Text)

            'Next
            Dim dt As New DataTable '= SALGRD
            If Not SALGRD Is Nothing Then
                Try
                    Dim query As IEnumerable(Of DataRow) = From order In SALGRD.AsEnumerable() Where order.Field(Of String)("STU_NO") = lbSNo.Text.ToString Select order
                    dt = query.CopyToDataTable()
                Catch ex As Exception
                End Try
            Else
                'dt = SALGRD
            End If


            If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                'Dim dbl_net_amt As Double

                'GridSelBook.Visible = True
                If dt.Rows.Count <= 4 Then

                    DivMainContent.Attributes("class") = "main_content_grid0"
                    GridScroll.CssClass = "grid_scroll0"
                Else
                    DivMainContent.Attributes("class") = "main_content_grid"
                    GridScroll.CssClass = "grid_scroll"

                End If
                GridScroll.Visible = True

                For Each gvrow As DataRow In dt.Rows
                    If lbSNo.Text = gvrow("STU_NO").ToString Then
                        Cur_Net_Amt = Cur_Net_Amt + CDbl(gvrow("NET_AMOUNT"))
                        Cur_SubTotal_Amt = Cur_SubTotal_Amt + (CDbl(gvrow("PRICE")) * CDbl(gvrow("QTY")))
                        Cur_VAT_Amt = Cur_VAT_Amt + (CDbl(gvrow("TAX_AMOUNT")) * CDbl(gvrow("QTY")))
                    End If

                Next
            Else
                GridScroll.Visible = False
            End If
            lbl0.Text = "" & Convert.ToDouble(Cur_Net_Amt).ToString("#,###,##0.00") '"" & Cur_Net_Amt
            lblSubTotal.Text = "" & Convert.ToDouble(Cur_SubTotal_Amt).ToString("#,###,##0.00") '"" & Cur_SubTotal_Amt
            lblSubVATTotal.Text = "" & Convert.ToDouble(Cur_VAT_Amt).ToString("#,###,##0.00") '"" & Cur_VAT_Amt



            Net_Pay_Amt = Net_Pay_Amt + Cur_Net_Amt
            Cur_Net_Amt = 0.0
            Cur_SubTotal_Amt = 0.0
            Cur_VAT_Amt = 0.0
            lblTotal.Text = "" & Convert.ToDouble(Net_Pay_Amt).ToString("#,###,##0.00") '"" & Net_Pay_Amt
            txtTotal.Text = "" & Convert.ToDouble(Net_Pay_Amt).ToString("#,###,##0.00") '"" & Net_Pay_Amt


            For i As Integer = 0 To rblist.Items.Count - 1
                If rblist.Items(i).Selected Then

                    Dim dt00 As New DataTable
                    Dim filter_str0 As String = ""
                    Dim dtView0 As DataView = New DataView(SALGRD)
                    filter_str0 = " BSH_ID=" & rblist.Items(i).Value & " AND  QTY <> ORG_QTY "
                    dtView0.RowFilter = filter_str0
                    dt00 = dtView0.ToTable
                    If dt00.Rows.Count >= 1 Then
                    Else
                        rbvalue = rbvalue + rblist.Items(i).Value + "|"
                    End If

                End If
            Next

        Next


        'COURIER AMOUNT ADDING SECTION
        Dim pParms(2) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@OPTIONS", SqlDbType.Int)
        pParms(0).Value = 1
        pParms(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.Int)
        pParms(1).Value = Session("sBSUID")
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_PUR_INVConnectionString, CommandType.StoredProcedure, "[dbo].[GET_COURIER_FEE]", pParms)
        courier_amount = Convert.ToDecimal(ds.Tables(0).Rows(0)("COURIER_FEE").ToString())



        If rbl_deliverytype.SelectedItem.Value = 2 Then
            If Net_Pay_Amt > 0 Then
                If rbvalue = "" Then
                    Net_Pay_Amt = Net_Pay_Amt + courier_amount
                    lblTotal.Text = Net_Pay_Amt
                    txt_courier_charge.Text = courier_amount
                Else
                    Net_Pay_Amt = Net_Pay_Amt + 0
                    lblTotal.Text = Net_Pay_Amt
                    txt_courier_charge.Text = 0
                End If

            Else
                'rbl_deliverytype.SelectedValue = 1 'Change in Delivery type ' commented on 21AUG2020
                rbl_deliverytype.SelectedValue = 2 'Change in Delivery type
                'id_courier.Visible = False  ' commented on 21AUG2020
                id_courier.Visible = True
            End If
        End If

    End Sub

    Public Function GetTable() As DataTable
        Dim dt As DataTable = New DataTable()
        dt.Columns.Add("ID")
        dt.Columns.Add("BSH_ID")
        dt.Columns.Add("ITEM_ID")
        dt.Columns.Add("ITEM_DESCR")
        dt.Columns.Add("BOOK_NAME")
        dt.Columns.Add("ISBN")
        dt.Columns.Add("PRICE")
        dt.Columns.Add("QTY")
        dt.Columns.Add("TAX_CODE")
        dt.Columns.Add("TAX_AMOUNT")
        dt.Columns.Add("NET_AMOUNT")
        dt.Columns.Add("STU_NO")
        dt.Columns.Add("MINUS_BTN_CSS")
        dt.Columns.Add("PLUS_BTN_CSS")
        dt.Columns.Add("TAX_DESCR")
        dt.Columns.Add("ORG_QTY")
        Return dt
    End Function
    Protected Sub btnAdd_Click(sender As Object, e As EventArgs)
        Dim obj As Object = sender.parent

        Dim hf_Item_ID As New HiddenField
        hf_Item_ID = DirectCast(obj.FindControl("hf_Item_ID"), HiddenField)
        Dim STR_ITEM_ID As String = hf_Item_ID.Value

        Dim GridSelBook As New GridView
        Dim DivHeaderRow As New HtmlGenericControl
        Dim DivMainContent As New HtmlGenericControl
        Dim DivFooterRow As New HtmlGenericControl

        GridSelBook = DirectCast(obj.FindControl("grdSAL"), GridView)
        DivHeaderRow = DirectCast(obj.FindControl("DivHeaderRow"), HtmlGenericControl)
        DivMainContent = DirectCast(obj.FindControl("DivMainContent"), HtmlGenericControl)
        DivFooterRow = DirectCast(obj.FindControl("DivFooterRow"), HtmlGenericControl)

        'GridSelBook.AllowPaging = False
        Dim lbSNo As New Label
        lbSNo = DirectCast(obj.FindControl("lbSNo"), Label)

        Dim pParms(2) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@OPTIONS", SqlDbType.Int)
        pParms(0).Value = 2
        pParms(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.Int)
        pParms(1).Value = Session("sBSUID")
        pParms(2) = New SqlClient.SqlParameter("@BIM_ID", SqlDbType.VarChar, 20)
        pParms(2).Value = STR_ITEM_ID


        Dim dt0 As New DataTable
        Dim filter_str As String = ""
        Dim dtView As DataView = New DataView(SALGRD)
        filter_str = " BSH_ID=-1 AND ITEM_ID=" & STR_ITEM_ID & " AND STU_NO='" & lbSNo.Text & "'"
        dtView.RowFilter = filter_str
        dt0 = dtView.ToTable
        If dt0.Rows.Count > 0 Then
            lblError.Text = "Cannot add same item master"
            lblError.CssClass = "alert alert-warning"
        Else

            'add more item, check the item already (multiple times) in salgrd
            Dim dtt As DataTable = SALGRD
            Dim FNL_QTY As Integer = 0
            For Each gvrow As DataRow In dtt.Rows
                Dim dt00 As New DataTable
                Dim filter_str0 As String = ""
                Dim dtView0 As DataView = New DataView(SALGRD)
                filter_str0 = " ITEM_ID=" & gvrow("ITEM_ID") & ""
                dtView0.RowFilter = filter_str0
                dt00 = dtView0.ToTable

                Dim ds5 As New DataSet
                Dim AVAIL_QTY As Integer = 0
                ds5 = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_PUR_INVConnectionString, CommandType.Text, "SELECT [dbo].[GET_BAL_ITEM_QTY] ('" & Session("sBSUID") & "'," & gvrow("ITEM_ID") & ") AS AVAIL_QTY")
                AVAIL_QTY = CInt((ds5.Tables(0).Rows(0)("AVAIL_QTY")))

                If dt00.Rows.Count > 0 Then
                    For Each gvrow2 As DataRow In dt00.Rows
                        FNL_QTY = FNL_QTY + CDbl(gvrow2("QTY"))
                    Next
                End If
                FNL_QTY = FNL_QTY + 1 'we are incrementing because we have to add one item to the list
                If FNL_QTY > AVAIL_QTY Then
                    lblError.Text = "Item (" & gvrow("ITEM_DESCR") & ") quantity higher than available quantity. Available quantity is " & AVAIL_QTY
                    lblError.CssClass = "alert alert-warning"

                    Exit Sub
                End If

            Next


            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_PUR_INVConnectionString, CommandType.StoredProcedure, "dbo.[GET_BOOK_ITEM_M]", pParms)


            Dim dt As DataTable = GetTable()

            If Not SALGRD Is Nothing Then '
                'dt = SALGRD
                Try
                    Dim query As IEnumerable(Of DataRow) = From order In SALGRD.AsEnumerable() Where order.Field(Of String)("STU_NO") = lbSNo.Text.ToString Select order
                    dt = query.CopyToDataTable()

                Catch ex As Exception
                End Try
                If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then

                    FNL_QTY = 0
                    Dim ds5 As New DataSet
                    Dim AVAIL_QTY As Integer = 0
                    ds5 = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_PUR_INVConnectionString, CommandType.Text, "SELECT [dbo].[GET_BAL_ITEM_QTY] ('" & Session("sBSUID") & "'," & ds.Tables(0).Rows(0)("ITEM_ID") & ") AS AVAIL_QTY")
                    AVAIL_QTY = CInt((ds5.Tables(0).Rows(0)("AVAIL_QTY")))

                    FNL_QTY = FNL_QTY + 1 'we are incrementing because we have to add one item to the list
                    If FNL_QTY > AVAIL_QTY Then
                        lblError.Text = "Item (" & ds.Tables(0).Rows(0)("BOOK_NAME") & ") quantity higher than available quantity. Available quantity is " & AVAIL_QTY
                        lblError.CssClass = "alert alert-warning"

                        Exit Sub
                    End If

                    Dim dr1 As DataRow = dt.NewRow()
                    ''Dim dr2 As DataRow = dt2.NewRow()
                    dr1("ID") = dt.Rows.Count + 1 'ds.Tables(0).Rows(0)("ID").ToString
                    dr1("BSH_ID") = "-1" 'ds.Tables(0).Rows(0)("BSH_ID").ToString
                    dr1("ITEM_ID") = ds.Tables(0).Rows(0)("ITEM_ID").ToString
                    dr1("ITEM_DESCR") = ds.Tables(0).Rows(0)("ITEM_DESCR").ToString
                    dr1("BOOK_NAME") = ds.Tables(0).Rows(0)("BOOK_NAME").ToString
                    dr1("ISBN") = ds.Tables(0).Rows(0)("ISBN").ToString
                    dr1("PRICE") = ds.Tables(0).Rows(0)("PRICE").ToString
                    dr1("QTY") = ds.Tables(0).Rows(0)("QTY").ToString
                    dr1("TAX_CODE") = ds.Tables(0).Rows(0)("TAX_CODE").ToString
                    dr1("TAX_AMOUNT") = ds.Tables(0).Rows(0)("TAX_AMOUNT").ToString
                    dr1("NET_AMOUNT") = ds.Tables(0).Rows(0)("NET_AMOUNT").ToString
                    dr1("STU_NO") = lbSNo.Text
                    dr1("TAX_DESCR") = ds.Tables(0).Rows(0)("TAX_DESCR").ToString
                    dr1("ORG_QTY") = ds.Tables(0).Rows(0)("QTY").ToString
                    dt.Rows.Add(dr1)
                    SALGRD.ImportRow(dr1)
                    'dt2.Rows.Add(dr2)
                End If
                ''SALGRD = dt
                'SALGRD.Merge(dt2)
            Else
                'dt = SALGRD
                If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                    For Each gvrow As DataRow In ds.Tables(0).Rows

                        FNL_QTY = 0
                        Dim ds5 As New DataSet
                        Dim AVAIL_QTY As Integer = 0
                        ds5 = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_PUR_INVConnectionString, CommandType.Text, "SELECT [dbo].[GET_BAL_ITEM_QTY] ('" & Session("sBSUID") & "'," & gvrow("ITEM_ID") & ") AS AVAIL_QTY")
                        AVAIL_QTY = CInt((ds5.Tables(0).Rows(0)("AVAIL_QTY")))

                        FNL_QTY = FNL_QTY + 1 'we are incrementing because we have to add one item to the list
                        If FNL_QTY > AVAIL_QTY Then
                            lblError.Text = "Item (" & gvrow("ITEM_DESCR") & ") quantity higher than available quantity. Available quantity is " & AVAIL_QTY
                            lblError.CssClass = "alert alert-warning"

                            Exit Sub
                        End If

                        Dim dr As DataRow
                        dr = dt.NewRow()

                        dr("ID") = gvrow("ID").ToString
                        dr("BSH_ID") = "-1" 'gvrow("BSH_ID").ToString
                        dr("ITEM_ID") = gvrow("ITEM_ID").ToString
                        dr("ITEM_DESCR") = gvrow("ITEM_DESCR").ToString
                        dr("BOOK_NAME") = gvrow("BOOK_NAME").ToString
                        dr("ISBN") = gvrow("ISBN").ToString
                        dr("PRICE") = gvrow("PRICE").ToString
                        dr("QTY") = gvrow("QTY").ToString
                        dr("TAX_CODE") = gvrow("TAX_CODE").ToString
                        dr("TAX_AMOUNT") = gvrow("TAX_AMOUNT").ToString
                        dr("NET_AMOUNT") = gvrow("NET_AMOUNT").ToString
                        dr("STU_NO") = lbSNo.Text
                        dr("TAX_DESCR") = gvrow("TAX_DESCR").ToString
                        dr("ORG_QTY") = gvrow("QTY").ToString
                        dt.Rows.Add(dr)
                    Next
                End If

                SALGRD = dt
            End If
            'GridSelBook.Height = 240
            GridSelBook.DataSource = dt
            GridSelBook.DataBind()
            CalculateTotal()
            ScriptManager.RegisterStartupScript(Page, Me.GetType(), "Key", "<script>MakeStaticHeader('" & GridSelBook.ClientID & "','" & DivHeaderRow.ClientID & "','" & DivMainContent.ClientID & "','" & DivFooterRow.ClientID & "', 200, 1000 , 50 ,false); </script>", False)
        End If
        'ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FREEZE", "FreezeGrid('" & GridSelBook.ClientID & "');", True)
        'ScriptManager.RegisterStartupScript(Page, Me.GetType(), "Key", "<script>MakeStaticHeader('" & GridSelBook.ClientID & "', 200, 1000 , 40 ,false); </script>", False)
        'ScriptManager.RegisterStartupScript(Page, Me.GetType(), "Key", "<script>MakeStaticHeader('" & GridSelBook.ClientID & "', 200, 1000 , 50 ,false); </script>", False)

    End Sub
    <System.Web.Services.WebMethod()>
    Public Shared Function GetItmDescr(ByVal prefixText As String, ByVal contextKey As String) As String()

        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
        Dim StrGrade As String = "00"
        Dim StrSQL As String
        'If contextKey Is Nothing Then contextKey = ""
        If Not contextKey Is Nothing Then
            'contextKey = ""
            Dim StrSQL2 = "select STU_GRD_ID from [OASIS].[DBO].[STUDENT_M] WITH (NOLOCK) where [STU_BSU_ID]='" & HttpContext.Current.Session("sBSUID") & "' AND [STU_ID] = '" & contextKey & "'"

            Dim ds2 As DataSet
            ds2 = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, StrSQL2)
            StrGrade = ds2.Tables(0).Rows(0)("STU_GRD_ID").ToString()

        End If

        StrSQL = "select top 10 DESCRIPTION,BIM_ID from (select [BIM_BOOK_NAME] DESCRIPTION,BIM_ID from [BOOK_ITEM_M] WITH (NOLOCK) where [BIM_BSU_ID]='" & HttpContext.Current.Session("sBSUID") & "' AND [BIM_bDELETE]=0 AND [BIM_bAPPROVE]=1 AND BIM_GRD_ID LIKE '%" & StrGrade & "%' ) a where 1=1 and DESCRIPTION like '%" & prefixText & "%'"

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, StrSQL)

        Dim intRows As Int32 = ds.Tables(0).Rows.Count
        Dim items As New List(Of String)(intRows)
        Dim i2 As Integer

        For i2 = 0 To intRows - 1
            Dim item As String = AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(ds.Tables(0).Rows(i2)("DESCRIPTION").ToString, ds.Tables(0).Rows(i2)("BIM_ID").ToString & "|" & ds.Tables(0).Rows(i2)("DESCRIPTION").ToString)
            items.Add(item)
        Next
        Return items.ToArray()
    End Function
    Protected Sub btnSelectBook_Click(sender As Object, e As EventArgs)

        Dim obj As Object = sender.parent
        Dim GridSelBook As New GridView
        'Dim rblist As New RadioButtonList
        Dim DivHeaderRow As New HtmlGenericControl
        Dim DivMainContent As New HtmlGenericControl
        Dim DivFooterRow As New HtmlGenericControl
        Dim rblist As New CheckBoxList
        Dim txtTotal As New Label
        GridSelBook = DirectCast(obj.FindControl("grdSAL"), GridView)
        'rblist = DirectCast(obj.FindControl("rdbBookSets"), RadioButtonList)
        DivHeaderRow = DirectCast(obj.FindControl("DivHeaderRow"), HtmlGenericControl)
        DivMainContent = DirectCast(obj.FindControl("DivMainContent"), HtmlGenericControl)
        DivFooterRow = DirectCast(obj.FindControl("DivFooterRow"), HtmlGenericControl)
        rblist = DirectCast(obj.FindControl("rdbBookSets"), CheckBoxList)
        txtTotal = DirectCast(obj.FindControl("txtGrandTotal"), Label)
        Dim lbSNo As New Label
        lbSNo = DirectCast(obj.FindControl("lbSNo"), Label)

        Dim dt As DataTable = GetTable()
        'Dim dt2 As DataTable = GetTable()

        GridSelBook.Visible = True
        GridSelBook.ShowFooter = False
        Dim rbvalue As String = "" '= rblist.SelectedValue

        For i As Integer = 0 To rblist.Items.Count - 1
            If rblist.Items(i).Selected Then
                rbvalue = rbvalue + rblist.Items(i).Value + "|"
            End If
        Next

        Dim pParms(2) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@OPTIONS", SqlDbType.Int)
        pParms(0).Value = 1
        pParms(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(1).Value = Session("sBSUID")
        pParms(2) = New SqlClient.SqlParameter("@BSH_ID", SqlDbType.VarChar, 200)
        pParms(2).Value = rbvalue

        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_PUR_INVConnectionString, CommandType.StoredProcedure, "dbo.[GET_BOOK_SET_ITEM_M]", pParms)
        If Not SALGRD Is Nothing Then
            Try
                Dim query As IEnumerable(Of DataRow) = From order In SALGRD.AsEnumerable() Where order.Field(Of String)("STU_NO") = lbSNo.Text.ToString Select order
                dt = query.CopyToDataTable()

            Catch ex As Exception
            End Try
            'dt = SALGRD
            If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                Dim Row_Count As Integer = dt.Rows.Count + 1
                For Each gvrow As DataRow In ds.Tables(0).Rows
                    Dim dr As DataRow
                    dr = dt.NewRow()


                    dr("ID") = Row_Count 'gvrow("ID").ToString
                    dr("BSH_ID") = gvrow("BSH_ID").ToString
                    dr("ITEM_ID") = gvrow("ITEM_ID").ToString
                    dr("ITEM_DESCR") = gvrow("ITEM_DESCR").ToString
                    dr("BOOK_NAME") = gvrow("BOOK_NAME").ToString
                    dr("ISBN") = gvrow("ISBN").ToString
                    dr("PRICE") = gvrow("PRICE").ToString
                    dr("QTY") = gvrow("QTY").ToString
                    dr("TAX_CODE") = gvrow("TAX_CODE").ToString
                    dr("TAX_AMOUNT") = gvrow("TAX_AMOUNT").ToString
                    dr("NET_AMOUNT") = gvrow("NET_AMOUNT").ToString
                    dr("STU_NO") = lbSNo.Text
                    dr("TAX_DESCR") = gvrow("TAX_CODE").ToString 'NOT USING OTHER WISE HERE TAX_DESCR '21AUG2020
                    dr("ORG_QTY") = gvrow("QTY").ToString

                    dt.Rows.Add(dr)
                    SALGRD.ImportRow(dr)
                    Row_Count = Row_Count + 1
                Next
            End If
            'Dim dr1 As DataRow = dt.NewRow()
            'dr1("ID") = ds.Tables(0).Rows(0)("ID").ToString
            'dr1("BSH_ID") = ds.Tables(0).Rows(0)("BSH_ID").ToString
            'dr1("ITEM_DESCR") = ds.Tables(0).Rows(0)("ITEM_DESCR").ToString
            'dr1("ISBN") = ds.Tables(0).Rows(0)("ISBN").ToString
            'dr1("PRICE") = ds.Tables(0).Rows(0)("PRICE").ToString
            'dr1("QTY") = ds.Tables(0).Rows(0)("QTY").ToString
            'dr1("TAX_AMOUNT") = ds.Tables(0).Rows(0)("TAX_AMOUNT").ToString
            'dr1("NET_AMOUNT") = ds.Tables(0).Rows(0)("NET_AMOUNT").ToString
            'dt.Rows.Add(dr1)
            'SALGRD = dt
            'SALGRD.Merge(dt2)
        Else
            'dt = SALGRD
            If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                For Each gvrow As DataRow In ds.Tables(0).Rows
                    Dim dr As DataRow
                    dr = dt.NewRow()

                    dr("ID") = gvrow("ID").ToString
                    dr("BSH_ID") = gvrow("BSH_ID").ToString
                    dr("ITEM_ID") = gvrow("ITEM_ID").ToString
                    dr("ITEM_DESCR") = gvrow("ITEM_DESCR").ToString
                    dr("BOOK_NAME") = gvrow("BOOK_NAME").ToString
                    dr("ISBN") = gvrow("ISBN").ToString
                    dr("PRICE") = gvrow("PRICE").ToString
                    dr("QTY") = gvrow("QTY").ToString
                    dr("TAX_CODE") = gvrow("TAX_CODE").ToString
                    dr("TAX_AMOUNT") = gvrow("TAX_AMOUNT").ToString
                    dr("NET_AMOUNT") = gvrow("NET_AMOUNT").ToString
                    dr("STU_NO") = lbSNo.Text
                    dr("TAX_DESCR") = gvrow("TAX_CODE").ToString 'NOT USING OTHER WISE HERE TAX_DESCR '21AUG2020
                    dr("ORG_QTY") = gvrow("QTY").ToString
                    dt.Rows.Add(dr)

                Next
            End If
            SALGRD = dt

        End If


        GridSelBook.Height = 240
        GridSelBook.DataSource = dt
        GridSelBook.DataBind()
        CalculateTotal()

        Dim id_as_string As String = GridSelBook.ClientID
        Dim last_char As String = id_as_string(id_as_string.Length - 1)
        If last_char = "0" Then
            ScriptManager.RegisterStartupScript(Page, Me.GetType(), "Key", "<script>MakeStaticHeader('" & GridSelBook.ClientID & "','" & DivHeaderRow.ClientID & "','" & DivMainContent.ClientID & "','" & DivFooterRow.ClientID & "', 200, 1000 , 50 ,false); </script>", False)
        ElseIf last_char = "1" Then
            ScriptManager.RegisterStartupScript(Page, Me.GetType(), "Key", "<script>MakeStaticHeader('" & GridSelBook.ClientID & "','" & DivHeaderRow.ClientID & "','" & DivMainContent.ClientID & "','" & DivFooterRow.ClientID & "', 200, 1000 , 50 ,false); </script>", False)

        End If
        'ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "FREEZE", "FreezeGrid('" & GridSelBook.ClientID & "');", True)
        'ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), "Key", "<script>MakeStaticHeader('" & GridSelBook.ClientID & "','" & DivHeaderRow.ClientID & "','" & DivMainContent.ClientID & "','" & DivFooterRow.ClientID & "', 200, 1000 , 50 ,false); </script>", False)

    End Sub
    Protected Sub grdSAL_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) 'Handles grdSAL.PageIndexChanging
        '    grdSAL.PageIndex = e.NewPageIndex
        Dim gvOrders As GridView = TryCast(sender, GridView)
        gvOrders.PageIndex = e.NewPageIndex
        Dim obj As Object = sender.parent
        Dim GridSelBook As New GridView
        'Dim rblist As New RadioButtonList
        Dim rblist As New CheckBoxList
        Dim txtTotal As New Label
        Dim lbSNo As New Label
        lbSNo = DirectCast(obj.FindControl("lbSNo"), Label)
        GridSelBook = DirectCast(obj.FindControl("grdSAL"), GridView)
        'rblist = DirectCast(obj.FindControl("rdbBookSets"), RadioButtonList)
        rblist = DirectCast(obj.FindControl("rdbBookSets"), CheckBoxList)
        txtTotal = DirectCast(obj.FindControl("txtGrandTotal"), Label)
        'Dim lbSNo As New Label
        'lbSNo = DirectCast(obj.FindControl("lbSNo"), Label)
        Dim dt As DataTable = GetTable()
        If Not SALGRD Is Nothing Then
            Try
                Dim query As IEnumerable(Of DataRow) = From order In SALGRD.AsEnumerable() Where order.Field(Of String)("STU_NO") = lbSNo.Text.ToString Select order
                dt = query.CopyToDataTable()

            Catch ex As Exception
            End Try
            'dt = SALGRD
        End If
        GridSelBook.Visible = True
        GridSelBook.ShowFooter = False
        GridSelBook.DataSource = dt
        GridSelBook.DataBind()
        CalculateTotal()


    End Sub
    Protected Sub rbl_deliverytype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbl_deliverytype.SelectedIndexChanged
        Dim net_total As Decimal = 0.0
        Dim courier_amount As Decimal = 0.0

        Dim pParms(2) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@OPTIONS", SqlDbType.Int)
        pParms(0).Value = 1
        pParms(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.Int)
        pParms(1).Value = Session("sBSUID")
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_PUR_INVConnectionString, CommandType.StoredProcedure, "[dbo].[GET_COURIER_FEE]", pParms)
        courier_amount = Convert.ToDecimal(ds.Tables(0).Rows(0)("COURIER_FEE").ToString())
        If rbl_deliverytype.SelectedItem.Value = 2 Then


            If Convert.ToDouble(lblTotal.Text) > 0 Then
                id_courier.Visible = True
                'txt_courier_charge.Text = courier_amount
                'net_total = Convert.ToDouble(lblTotal.Text) + courier_amount
                'lblTotal.Text = net_total

                CalculateTotal()
            Else
                'rbl_deliverytype.SelectedValue = 1 'Change in delivery type ' Commented on 21AUG2020
                rbl_deliverytype.SelectedValue = 2 'Change in delivery type
                id_courier.Visible = True
                lblError.Text = "Please select items"
                lblError.CssClass = "alert alert-warning"
            End If
        Else
            id_courier.Visible = False
            If Convert.ToDouble(lblTotal.Text) > 0 Then
                'net_total = Convert.ToDouble(lblTotal.Text) '- courier_amount
                'lblTotal.Text = net_total
                CalculateTotal()
            Else
                lblError.Text = "Please select items"
                lblError.CssClass = "alert alert-warning"
            End If

        End If

        If rbl_deliverytype.SelectedItem.Value = 0 Or rbl_deliverytype.SelectedItem.Value = 2 Then
            id_colMessage.Visible = True

            Dim pParms1(2) As SqlClient.SqlParameter
            pParms1(0) = New SqlClient.SqlParameter("@OPTIONS", SqlDbType.Int)
            pParms1(0).Value = 1
            pParms1(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.Int)
            pParms1(1).Value = Session("sBSUID")
            Dim dsM As New DataSet
            dsM = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_PUR_INVConnectionString, CommandType.StoredProcedure, "[dbo].[GET_BOOK_COLLECTION_MESSAGE]", pParms1)
            If Not dsM Is Nothing AndAlso dsM.Tables(0).Rows.Count >= 1 Then
                lblCollectionMessage.Text = dsM.Tables(0).Rows(0)("BCM_MESSAGE").ToString()
                hf_CollectionMessageID.Value = dsM.Tables(0).Rows(0)("BCM_ID").ToString()
            Else
                hf_CollectionMessageID.Value = "0"
            End If

        Else
            id_colMessage.Visible = False
            hf_CollectionMessageID.Value = "0"
        End If

    End Sub

    Sub PaymentRedirect()
        Dim GATEWAY_TYPE As String = "", GATEWAY_URL As String = ""
        PaymentGatewayClass.GetGatewayParameters(Session("sBsuId"), Session("CPS_ID"), GATEWAY_TYPE, GATEWAY_URL)
        Session("CPM_GATEWAY_TYPE") = GATEWAY_TYPE
        Select Case GATEWAY_TYPE
            Case "MPGS"
                Dim APP_RESULT_PAGE As String = Session("vpc_ReturnURL").ToString().ToLower().Replace("booksalepaymentresult.aspx", "PaymentResultPage.aspx").Split("?")(0).Replace("http://", IIf(Request.Url.Host.Contains("localhost"), "http://", "https://"))
                Dim redirectUrl As String = GATEWAY_URL & "?SRC=" & Encr_decrData.Encrypt("BOOKSALES") & "&ID=" & Encr_decrData.Encrypt(Session("vpc_MerchTxnRef")) & "&APP=" & Encr_decrData.Encrypt("GEMSPARENT") & "&ARP=" & Encr_decrData.Encrypt(APP_RESULT_PAGE) & "&BSU_ID=" & Encr_decrData.Encrypt(Session("sBsuId"))
                'Dim redirectUrl As String = GATEWAY_URL & "?SRC=" & Encr_decrData.Encrypt("BOOKSALES") & "&ID=" & Encr_decrData.Encrypt(Session("vpc_MerchTxnRef")) & "&APP=" & Encr_decrData.Encrypt("GEMSPARENT") & "&BSU_ID=" & Encr_decrData.Encrypt(Session("sBsuId"))
                Response.Redirect(redirectUrl, True)
            Case Else
                Response.Redirect("BookSalePaymentRedirectNew.aspx")
        End Select
    End Sub
End Class