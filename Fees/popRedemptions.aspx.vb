﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports UtilityObj
Partial Class ParentLogin_popRedemptions
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("username") & "" = "" Then
            Response.Redirect("~/login.aspx")
        End If

        If Not Page.IsPostBack Then
            GET_REWARDS_REDEMPTION_RECEIPTS()
        End If
    End Sub

    Sub GET_REWARDS_REDEMPTION_RECEIPTS()
        Dim pParms(2) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(0).Value = Session("sBsuid")
        pParms(1) = New SqlClient.SqlParameter("@OLU_ID", SqlDbType.Int)
        pParms(1).Value = Session("OLU_ID")
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, _
          CommandType.StoredProcedure, "FEES.GET_REWARDS_REDEMPTION_RECEIPTS", pParms)
        gvRedemptionHistory.DataSource = dsData
        gvRedemptionHistory.DataBind()

        'If Not dsData Is Nothing AndAlso dsData.Tables.Count > 0 AndAlso dsData.Tables(0).Rows.Count > 0 Then
        '    lblgrade.Text = "Grade - " & dsData.Tables(0).Rows(0)("GRD_ID")
        'End If

    End Sub

End Class
