﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ParentMaster.master" AutoEventWireup="false" CodeFile="PaymentResultPage.aspx.vb" Inherits="Fees_PaymentResultPage" %>

<%@ Register Src="~/UserControl/urcStudentPaidResult.ascx" TagPrefix="uc1" TagName="urcStudentPaidResult" %>
<%@ Register Src="~/UserControl/urcStudentTransportPaidResult.ascx" TagPrefix="uc1" TagName="urcStudentTransportPaidResult" %>
<%@ Register Src="~/UserControl/urcActvityPaidResult.ascx" TagPrefix="uc1" TagName="urcActvityPaidResult" %>
<%@ Register Src="~/UserControl/urcBookSalePaidResult.ascx" TagPrefix="uc1" TagName="urcBookSalePaidResult" %>
<%@ Register Src="~/UserControl/urcExtraProvisionPaidResult.ascx" TagPrefix="uc1" TagName="urcExtraProvisionPaidResult" %>



<asp:Content ID="Content1" ContentPlaceHolderID="cphParent" runat="Server">
    <script type="text/javascript" src="../Scripts/Fancybox/jquery.fancybox.js?v=2.1.5"></script>
    <script type="text/javascript" src="../Scripts/Fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
    <link rel="stylesheet" href="../Scripts/Fancybox/jquery.fancybox.css" type="text/css" media="screen" />
    <script type="text/javascript" src="../Scripts/PopupJQuery.js"></script>

    <div class="content margin-top30 margin-bottom60">
        <div class="container">
            <div class="row">
                <div class="posts-block col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <table style="width: 90%;">
                        <tr>
                            <td>
                                <asp:Label ID="lblMessage" runat="server" EnableViewState="false"></asp:Label>
                            </td>
                        </tr>
                        <tr runat="server" id="trSchool">
                            <td>
                                <uc1:urcStudentPaidResult runat="server" ID="urcStudentPaidResult" />
                            </td>
                        </tr>
                        <tr runat="server" id="trTransport">
                            <td>
                                <uc1:urcStudentTransportPaidResult runat="server" ID="urcStudentTransportPaidResult" />
                            </td>
                        </tr>
                        <tr runat="server" id="trActivity">
                            <td>
                                <uc1:urcActvityPaidResult runat="server" ID="urcActvityPaidResult" />
                            </td>
                        </tr>
                        <tr runat="server" id="trBooksales">
                            <td>
                                <uc1:urcBookSalePaidResult runat="server" ID="urcBookSalePaidResult" />
                            </td>
                        </tr>
                        <tr runat="server" id="trExtraProvResult">
                            <td>
                                <uc1:urcExtraProvisionPaidResult runat="server" ID="urcExtraProvisionPaidResult" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

