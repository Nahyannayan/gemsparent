﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports UtilityObj
Partial Class Fees_PopupShowDataFeeSetup
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("username") & "" = "" Then
            Response.Redirect("~/login.aspx")
        End If
        If Not Page.IsPostBack Then
            If Not Request.QueryString("ACD_ID") Is Nothing And Not Request.QueryString("ACD_ID") Is Nothing Then
                hf_acdid.Value = Request.QueryString("ACD_ID")
                hf_stuid.Value = Request.QueryString("STU_ID")
            End If
            GridViewShowDetails.Attributes.Add("bordercolor", "#E0DBD7")
            gridbind()
        End If
    End Sub

    Sub gridbind()
        Try
            Dim str_Transport_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
            Select Case Request.QueryString("id")
                Case "FEESETUPMULTI"
                    Page.Title = "Schedule of Fees"
                    GetFeeSetupForAcademicyear()
            End Select
        Catch ex As Exception
            Errorlog(ex.Message)
        End Try
    End Sub

    Sub GetFeeSetupForAcademicyear()
        Dim pParms(5) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@ACD_ID", SqlDbType.BigInt)
        'pParms(0).Value = Session("STU_ACD_ID")
        pParms(0).Value = Convert.ToInt32(hf_acdid.Value)
        pParms(1) = New SqlClient.SqlParameter("@STU_ID", SqlDbType.BigInt)
        ' pParms(1).Value = Session("STU_ID")
        pParms(1).Value = Convert.ToInt32(hf_stuid.Value)
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, _
          CommandType.StoredProcedure, "FEES.GetFeeSetupForAcademicyear", pParms)
        GridViewShowDetails.DataSource = dsData
        GridViewShowDetails.DataBind()

        If Not dsData Is Nothing AndAlso dsData.Tables.Count > 0 AndAlso dsData.Tables(0).Rows.Count > 0 Then
            lblgrade.Text = "Grade - " & dsData.Tables(0).Rows(0)("GRD_ID")
        End If

    End Sub

End Class
