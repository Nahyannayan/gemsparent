﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="popRedemptions.aspx.vb"
    Inherits="ParentLogin_popRedemptions" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <%--<link media="screen" href="../CSS/SiteStyle.css" type="text/css" rel="stylesheet" />--%>
     <link rel="stylesheet" href="../css/bootstrap.css" type="text/css"  />
    <link rel="stylesheet" href="../css/bootstrap-theme.css" type="text/css"  />
    <title></title>
</head>
<body class="matters">
    <form id="form1" runat="server">
        <div class="title-box">
            <h3>
                GEMS Rewards Points redemptions
            </h3>
        </div>
        <table style="width: 100%; border-collapse: inherit !important; "
            border="0" >
            <tr>
                <td>
                    <asp:GridView ID="gvRedemptionHistory" runat="server" Width="100%" style="border-collapse: inherit !important" 
                        EmptyDataText="No Details" AutoGenerateColumns="False" class="table table-striped table-bordered table-responsive text-left my-orders-table">
                        <Columns>
                            <asp:BoundField DataField="RefNo" HeaderText="Reference No.">
                                <ItemStyle HorizontalAlign="Center" />
                                <HeaderStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="FCL_DATE" HeaderText="Date">
                                <ItemStyle HorizontalAlign="Center" />
                                <HeaderStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="FCL_RECNO" HeaderText="Receipt No.">
                                <ItemStyle HorizontalAlign="Center" />
                                <HeaderStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="FCL_AMOUNT" HeaderText="Amount" DataFormatString="{0:0.00}">
                                <HeaderStyle HorizontalAlign="Right" />
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="STU_NO" HeaderText="Student Id">
                                <ItemStyle HorizontalAlign="Left" />
                                <HeaderStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="NAME" HeaderText="Student Name">
                                <ItemStyle HorizontalAlign="Left" />
                                <HeaderStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>

        </table>
    </form>
</body>
</html>
