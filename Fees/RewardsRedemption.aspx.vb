﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Imports System.Web.Services
Imports System.Net
Imports Newtonsoft.Json.Linq
Imports RestSharp

Partial Class Fees_RewardsRedemption
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Private Property bPaymentConfirmMsgShown() As Boolean
        Get
            Return ViewState("bPaymentConfirmMsgShown")
        End Get
        Set(ByVal value As Boolean)
            ViewState("bPaymentConfirmMsgShown") = value
        End Set
    End Property
    Private Property dDueTotal() As Double
        Get
            Return ViewState("dDueTotal")
        End Get
        Set(ByVal value As Double)
            ViewState("dDueTotal") = value
        End Set
    End Property
    Private Property dDiscountTotal() As Double
        Get
            Return ViewState("dDiscountTotal")
        End Get
        Set(ByVal value As Double)
            ViewState("dDiscountTotal") = value
        End Set
    End Property
    Private Property dPayAmountTotal() As Double
        Get
            Return ViewState("dPayAmountTotal")
        End Get
        Set(ByVal value As Double)
            ViewState("dPayAmountTotal") = value
        End Set
    End Property

    Private Property dProcessingChargeTotal() As Double
        Get
            Return ViewState("dProcessingChargeTotal")
        End Get
        Set(ByVal value As Double)
            ViewState("dProcessingChargeTotal") = value
        End Set
    End Property

    'Private Property DiscountBreakUpXML() As String
    '    Get
    '        Return ViewState("DiscountBreakUpXML")
    '    End Get
    '    Set(ByVal value As String)
    '        ViewState("DiscountBreakUpXML") = value
    '    End Set
    'End Property

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Session("username") Is Nothing Then
            Response.Redirect("~\login.aspx")
        End If
        If Not IsPostBack Then
            bPaymentConfirmMsgShown = False
            Page.Title = "::GEMS Education |GEMS Rewards Redemption::"
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            ViewState("datamode") = "add"
            Dim CurUsr_id As String = Session("STU_ID")
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("STU_NAME")
            ClearAll()
            InitialiseCompnents()
            lbChildNameTop.Text = Session("STU_NAME")
            If USR_NAME = "" Or CurBsUnit = "" Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                Gridbind_Feedetails()
                GET_POINTS_SUMMARY_CALL()
            End If
        End If
    End Sub
    Sub ClearAll()
        lblAvailablePoints.Text = "0"
        lblRedeemablePoints.Text = "0"
        lblRedeemableAmt.Text = "0.00"
        lblEmailId.Text = ""
        ViewState("PRO_PRO_ID") = 0
        ViewState("dblTotalAmt") = 0
        ViewState("CUSTOMER_ID") = 0
        'lblPId.Text = ""
        'lblUserId.Text = ""
        txtTotal.Text = "0.00"
        txtGridTotal.Text = "0.00"
        'lblDate.Text = Format(Date.Now, OASISConstants.DateFormat)
        dDiscountTotal = 0
        dProcessingChargeTotal = 0
        dDueTotal = 0
        dPayAmountTotal = 0
        'DiscountBreakUpXML = ""
    End Sub
    Sub InitialiseCompnents()
        hfCurrency.Value = Session("BSU_CURRENCY")
        btnRedeem.Attributes.Add("onClick", "return ValidateSave();")
        lbtnHistory.Attributes.Add("onClick", "return ShowSubWindowWithClose('popRedemptions.aspx', '', '50%', '50%');")
    End Sub
    Sub Gridbind_Feedetails()
        Try
            'Dim strPath As String = Convert.ToString(readerStudent_Detail("STU_PHOTOPATH"))
            Dim connPath As String = WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ConnectionString
            Dim noImagePath As String = "~/Images/Home/no_image.gif"
            Dim con As String = ConnectionManger.GetOASISConnectionString
            Dim param(4) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@OLU_ID", Session("OLU_ID").ToString)
            param(1) = New SqlClient.SqlParameter("@connPath", connPath)
            param(2) = New SqlClient.SqlParameter("@noImagePath", noImagePath)
            param(3) = New SqlClient.SqlParameter("@STU_BSU_ID", Session("STU_BSU_ID")) '@
            param(4) = New SqlClient.SqlParameter("@SELECTED_STU_ID", Session("STU_ID"))

            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "[OPL].[GETCHILD_HOME_INFO]", param)

            repInfo.DataSource = ds.Tables(0)
            repInfo.DataBind()
            Set_GridTotal(False)
        Catch ex As Exception
            repInfo.DataBind()
            Set_GridTotal(False)
        End Try
    End Sub
    Sub Set_GridTotal(ByVal DonotCalulateDisc As Boolean)
        Dim dAmount As Decimal = 0
        Dim dDiscount As Decimal = 0
        For Each rptr As RepeaterItem In repInfo.Items
            Dim gvfee As New GridView
            gvfee = DirectCast(rptr.FindControl("gvFee"), GridView)
            Dim hfSTU_ID As HiddenField = CType(rptr.FindControl("hfSTU_ID"), HiddenField)
            Dim lblGvrError As New Label
            lblGvrError = DirectCast(rptr.FindControl("lblGvrError"), Label)
            Dim trgvrerror As New HtmlTableRow
            trgvrerror = DirectCast(rptr.FindControl("trgvrerror"), HtmlTableRow)

            dDiscountTotal = 0
            dDueTotal = 0
            dPayAmountTotal = 0
            dProcessingChargeTotal = 0

            For Each gvr As GridViewRow In gvfee.Rows
                Dim txtAmountToPay As TextBox = CType(gvr.FindControl("txtAmountToPay"), TextBox)
                Dim lblAmount As Label = CType(gvr.FindControl("lblAmount"), Label)

                If Not txtAmountToPay Is Nothing Then
                    txtAmountToPay.Text = GetDoubleVal(txtAmountToPay.Text)
                    If lblGvrError.Text <> "" Then
                        trgvrerror.Visible = True
                        txtAmountToPay.Text = 0
                    Else
                        trgvrerror.Visible = False
                    End If
                    If IsNumeric(txtAmountToPay.Text) = False Then
                        lblGvrError.Text = "Invalid Amount..!"
                        txtAmountToPay.Text = "0"
                        txtTotal.Text = "0"
                        Exit Sub
                    End If

                    dAmount = dAmount + GetDoubleVal(txtAmountToPay.Text)
                End If
                If gvr.RowType = DataControlRowType.DataRow Then
                    dDueTotal = dDueTotal + IIf(Not lblAmount Is Nothing, GetDoubleVal(lblAmount.Text), 0)
                    dPayAmountTotal = dPayAmountTotal + IIf(Not txtAmountToPay Is Nothing, GetDoubleVal(txtAmountToPay.Text), 0)
                End If
            Next
            If gvfee.Rows.Count > 0 Then
                Dim lblAmountF As New Label
                Dim lblDiscountF As New Label
                Dim lblAmounttoPayF As New Label
                Dim h_ProcessingChargeF As New HiddenField
                lblAmountF = CType(gvfee.FooterRow.FindControl("lblAmountF"), Label)
                lblDiscountF = CType(gvfee.FooterRow.FindControl("lblDiscountF"), Label)
                lblAmounttoPayF = CType(gvfee.FooterRow.FindControl("lblAmounttoPayF"), Label)
                h_ProcessingChargeF = CType(gvfee.FooterRow.FindControl("h_ProcessingChargeF"), HiddenField)

                lblAmountF.Text = dDueTotal.ToString("#,##0.00")
                lblDiscountF.Text = dDiscountTotal.ToString("#,##0.00")
                lblAmounttoPayF.Text = dPayAmountTotal.ToString("#,##0.00")
                h_ProcessingChargeF.Value = dProcessingChargeTotal.ToString("#,##0.00")
            End If

            dDiscountTotal = 0
            dDueTotal = 0
            dPayAmountTotal = 0
            dProcessingChargeTotal = 0
        Next
        txtGridTotal.Text = Format(dAmount, "0.00")
        txtTotal.Text = Format(GetDoubleVal(txtGridTotal.Text), "0.00")
    End Sub
    Public Function GetDoubleVal(ByVal Value As Object) As Double
        GetDoubleVal = 0
        Try
            If IsNumeric(Value) Then
                GetDoubleVal = Convert.ToDouble(Value)
            End If
        Catch ex As Exception
            GetDoubleVal = 0
        End Try
    End Function

    Sub ShowMessage(ByVal Message As String, Optional ByVal bError As Boolean = True)
        If Message <> "" Then
            If bError Then
                lblError.CssClass = "alert-danger alert margin-bottom0"
            Else
                lblError.CssClass = "alert-success alert margin-bottom0"
            End If
        Else
            lblError.CssClass = ""
        End If
        lblError.Text = Message
    End Sub
    Private Function VALIDATE_SAVE() As Boolean
        VALIDATE_SAVE = False
        Try
            If Session("sBsuId") Is Nothing OrElse Not IsNumeric(Session("sBsuId")) Then
                ShowMessage("Your login session expired, please login again and continue.")
                Exit Function
            End If
            If Not IsNumeric(Session("STU_ID")) Then
                ShowMessage("Please select Student")
                Exit Function
            End If
            If GetDoubleVal(lblRedeemableAmt.Text) < 1 Then
                ShowMessage("There should be a minimum of " & Session("BSU_CURRENCY") & " 1.00 is required for redemption ")
                Exit Function
            End If
            If GetDoubleVal(txtTotal.Text) < 1 Then
                ShowMessage("The minimum redemption amount is " & Session("BSU_CURRENCY") & " 1.00")
                Exit Function
            End If
            'If (GetDoubleVal(txtTotal.Text) Mod 10) <> 0 Then
            '    ShowMessage("The Amount should be multiple of 10")
            '    Exit Function
            'End If
            If GetDoubleVal(txtTotal.Text) > GetDoubleVal(lblRedeemableAmt.Text) Then
                ShowMessage("Amount exceeded the eligible redemption amount")
                Exit Function
            End If
            VALIDATE_SAVE = True
        Catch ex As Exception

        End Try
    End Function
    <WebMethod()> _
    Public Shared Function SAVE_ENTERTAINER_API_CALL_LOG(ByVal ACL_ID As Long, ByVal ACL_BSU_ID As String, ByVal ACL_DATE As DateTime, _
                                                         ByVal ACL_ACTION As String, ByVal ACL_URL As String, ByVal ACL_METHOD As String, _
                                                         ByVal ACL_PERSISTENT_ID As String, ByVal ACL_USER As String) As String()

        Return clsRewards.SAVE_ENTERTAINER_API_CALL_LOG(ACL_ID, ACL_BSU_ID, ACL_DATE, ACL_ACTION, ACL_URL, ACL_METHOD, ACL_PERSISTENT_ID, ACL_USER)

    End Function
    Private Function TrimStudentNo(ByVal STUNO As String) As String
        TrimStudentNo = STUNO
        If STUNO.Length >= 14 Then
            TrimStudentNo = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT CAST(REPLACE('" & STUNO & "',SUBSTRING('" & STUNO & "',1,6),'')AS INTEGER)AS STUNO")
        End If
    End Function
    Private Sub SAVE_COLLECTION_INITIATED()
        Dim PRO_DATE As String
        PRO_DATE = Format(DateTime.Now, OASISConstants.DataBaseDateFormat)
        Dim NEW_PRO_ID As Long = 0
        'Dim NEW_FCL_RECNO As String = ""
        Dim StuNos As String = ""
        ViewState("PRO_PRO_ID") = 0
        ViewState("dblTotalAmt") = 0
        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim objConn As New SqlConnection(str_conn) '
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try
            Dim PRO_PRO_ID As Int64 = 0
            Dim LoopTotal As Double = 0, retval As String = "0"
            For Each repitm As RepeaterItem In repInfo.Items
                Dim gvfee As New GridView
                gvfee = DirectCast(repitm.FindControl("gvFee"), GridView)
                Dim hfSTU_ID As HiddenField = CType(repitm.FindControl("hfSTU_ID"), HiddenField)
                Dim hfSTU_FEE_ID As HiddenField = CType(repitm.FindControl("hfSTU_FEE_ID"), HiddenField)
                Dim hfSTU_ACD_ID As HiddenField = CType(repitm.FindControl("hfSTU_ACD_ID"), HiddenField)
                Dim lblStuNo As Label = CType(repitm.FindControl("lbSNo"), Label)
                Dim lblAmounttoPayF As New Label
                If gvfee.Rows.Count > 0 Then
                    lblAmounttoPayF = CType(gvfee.FooterRow.FindControl("lblAmounttoPayF"), Label) 'gives total amount paying now of current student
                Else
                    lblAmounttoPayF.Text = "0.00"
                End If
                NEW_PRO_ID = 0
                If GetDoubleVal(lblAmounttoPayF.Text) > 0 And retval = "0" Then
                    StuNos = TrimStudentNo(hfSTU_FEE_ID.Value)
                    Dim objclsrewards As New clsRewards
                    objclsrewards.PRO_ID = 0
                    objclsrewards.PRO_PRO_ID = PRO_PRO_ID
                    objclsrewards.PRO_BSU_ID = Session("sBsuId")
                    objclsrewards.PRO_ACD_ID = hfSTU_ACD_ID.Value
                    objclsrewards.PRO_DATE = Format(DateTime.Now.Date, OASISConstants.DateFormat)
                    objclsrewards.PRO_SOURCE = "INTERNET"
                    objclsrewards.PRO_STU_TYPE = "S"
                    objclsrewards.PRO_STU_ID = hfSTU_ID.Value
                    objclsrewards.PRO_DRCR = "CR"
                    objclsrewards.PRO_AMOUNT = GetDoubleVal(lblAmounttoPayF.Text)
                    objclsrewards.PRO_NARRATION = "GEMS Rewards Points redemption for " & StuNos
                    objclsrewards.PRO_STATUS = "INITIATED"
                    objclsrewards.PRO_LOG_USERNAME = Session("username")
                    objclsrewards.PRO_API_DATA_REDEEMABLE_POINTS = GetDoubleVal(lblRedeemablePoints.Text)
                    objclsrewards.PRO_API_DATA_AVAILABLE_POINTS = GetDoubleVal(lblAvailablePoints.Text)
                    objclsrewards.PRO_API_DATA_REDEEMABLE_AMOUNT = GetDoubleVal(lblRedeemableAmt.Text)
                    objclsrewards.PERSISTENT_ID = ViewState("CUSTOMER_ID")
                    objclsrewards.API_CMD = ""

                    If objclsrewards.SAVE_POINTS_REDEMPTION_ONLINE(stTrans) Then
                        'stTrans.Commit()
                        NEW_PRO_ID = objclsrewards.PRO_ID
                        If PRO_PRO_ID = 0 Then
                            PRO_PRO_ID = NEW_PRO_ID
                        End If
                        If retval = "0" Then
                            For Each gvr As GridViewRow In gvfee.Rows
                                Dim txtAmountToPay As TextBox = CType(gvr.FindControl("txtAmountToPay"), TextBox)
                                Dim lblFSR_FEE_ID As Label = CType(gvr.FindControl("lblFSR_FEE_ID"), Label)
                                'Dim lblDiscount As Label = CType(gvr.FindControl("lblDiscount"), Label)
                                Dim lblAmount As Label = CType(gvr.FindControl("lblAmount"), Label)
                                If Not txtAmountToPay Is Nothing Then
                                    If GetDoubleVal(txtAmountToPay.Text) > 0 Then
                                        objclsrewards.PSS_ID = 0
                                        objclsrewards.PSS_FEE_ID = lblFSR_FEE_ID.Text
                                        objclsrewards.PSS_ORG_AMOUNT = GetDoubleVal(lblAmount.Text)
                                        objclsrewards.PSS_AMOUNT = GetDoubleVal(txtAmountToPay.Text)
                                        objclsrewards.PSS_TAX_AMOUNT = 0
                                        objclsrewards.PSS_CANCEL_AMOUNT = 0
                                        objclsrewards.PSS_CURRENCY_AMOUNT = 0
                                        If Not objclsrewards.SAVE_POINTS_REDEMPTION_SUB_ONLINE(stTrans) Then
                                            retval = "1"
                                            Exit For
                                        End If

                                    End If
                                End If
                            Next
                        End If
                    Else
                        retval = "1"
                        'stTrans.Rollback()
                        Exit For
                    End If
                    LoopTotal = LoopTotal + GetDoubleVal(lblAmounttoPayF.Text) 'Gets the total amount of all students paying now
                End If
            Next
            If retval = "0" Then
                stTrans.Commit()
                Dim message1 = "Click on <STRONG>Confirm & Proceed</STRONG> to continue with this GEMS Rewards Points redemption for " & IIf(Session("BSU_CURRENCY") Is Nothing, "", Session("BSU_CURRENCY")) & " " & Format(txtTotal.Text, Session("BSU_DataFormatString")) & ""
                Dim message2 = "Please note reference no. <STRONG>" & PRO_PRO_ID & "</STRONG> of this transaction for any future communication."
                Dim message3 = "Please do not close this browser or Log off until you get the final receipt."
                If Request.Browser.Type.ToUpper.Contains("IE") Then
                    Me.divboxpanelconfirm.Attributes("class") = "alert-danger alert margin-bottom0"
                Else
                    Me.divboxpanelconfirm.Attributes("class") = "alert-danger alert margin-bottom0"
                End If
                Me.btnRedeem.Visible = False
                Me.testpopup.Style.Item("display") = "block"
                lblMsg.Text = message1 & "<br />" & message2
                lblMsg2.Text = message3
                'lblPayMsg.Text = "Please do not close this window or Log off until you get the final receipt."
                'Me.tr_comment.Visible = True
                bPaymentConfirmMsgShown = True
                ViewState("PRO_PRO_ID") = PRO_PRO_ID
                ViewState("dblTotalAmt") = LoopTotal
                'REDEEM_POINTS_CALL(PRO_PRO_ID, LoopTotal)
            Else
                stTrans.Rollback()
            End If
        Catch ex As Exception
            stTrans.Rollback()
            ShowMessage(getErrorMessage(1000), True)
            Errorlog(ex.Message)
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub

    Private Sub SAVE_COLLECTION_SUCCESS(ByVal PRO_ID As Long)
        Dim StuNos As String = ""
        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim objConn As New SqlConnection(str_conn)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try
            Dim objcls As New clsRewards
            objcls.PRO_ID = PRO_ID
            If objcls.SAVE_FEECOLLECTION_H(stTrans) Then
                stTrans.Commit()
                ShowMessage(getErrorMessage(360), False)
                Gridbind_Feedetails()
                GET_POINTS_SUMMARY_CALL()
                'updating latest points after successfull points redemption
                UPDATE_COLLECTION_INITIATED(PRO_ID, "", "", "", "", "", "", "", "", True, GetDoubleVal(lblRedeemableAmt.Text), GetDoubleVal(lblRedeemablePoints.Text), GetDoubleVal(lblAvailablePoints.Text))
                Me.testpopup.Style.Item("display") = "none"
                Me.btnRedeem.Visible = True
                bPaymentConfirmMsgShown = False
                'Redirecting the page to result page with receipt details
                'Response.Redirect("RewardsRedemptionResult.aspx?ID=" & Encr_decrData.Encrypt(PRO_ID))
            Else
                stTrans.Rollback()
            End If
        Catch ex As Exception
            stTrans.Rollback()
            ShowMessage(getErrorMessage(359), True)
            Errorlog(ex.Message)
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub
    Private Sub GET_POINTS_SUMMARY_CALL()
        If Not Session("STU_ID") Is Nothing AndAlso Convert.ToInt64(Session("STU_ID")) > 0 Then
            Dim objcls As New clsRewards
            objcls.FETCH_REWARDS_INFO(Convert.ToInt64(Session("STU_ID")))

            'objcls.CUSTOMER_ID = 2049708

            If objcls.CUSTOMER_ID.Trim <> "" And objcls.CUSTOMER_ID.Trim <> "0" Then
                Dim ACL_ID As Long = 0, GENERATED_TOKEN As String = "", responseData As String = ""
                ViewState("CUSTOMER_ID") = objcls.CUSTOMER_ID
                lblEmailId.Text = objcls.PARENT_EMAIL
                If objcls.GENERATE_ACCESS_TOKEN(objcls, GENERATED_TOKEN) Then
                    objcls.GET_API_CALL_PARAMETERS("GETPOINTSBALANCE") 'function gets the web api call parameters
                    Dim str = clsRewards.SAVE_ENTERTAINER_API_CALL_LOG(ACL_ID, Session("sBsuid"), DateTime.Now, "GETPOINTSBALANCE", objcls.API_URI, objcls.API_METHOD, objcls.CUSTOMER_ID, Session("username"))
                    Const contentType As String = "application/x-www-form-urlencoded"

                    Dim client = New RestClient(New Uri(objcls.API_URI).AbsoluteUri)
                    Dim APIRequest = New RestRequest(clsRewards.GetAPIMethod(objcls.API_METHOD))
                    APIRequest.AddHeader("Content-Type", contentType)
                    APIRequest.AddHeader("CC_TOKEN", GENERATED_TOKEN)
                    APIRequest.AddHeader("TP_APPLICATION_KEY", objcls.API_TOKEN)
                    APIRequest.AddParameter("customer_id", objcls.CUSTOMER_ID)
                    APIRequest.AddParameter("type", "parent")
                    Dim Response As IRestResponse = client.Execute(APIRequest)
                    responseData = Response.Content
                    Dim bool = clsRewards.UPDATE_ENTERTAINER_API_CALL_LOG(ACL_ID, Left(responseData, 2000))
                End If

                'Dim responseReader As StreamReader
                'responseReader = Nothing
                Try
                    Dim jsonPointSummary = JObject.Parse(responseData)
                    If Not jsonPointSummary("status") Is Nothing AndAlso jsonPointSummary("status").ToObject(Of String)().ToLower = "true" Then
                        Dim jsonValues = JObject.Parse(jsonPointSummary("values").ToString())
                        Dim ParentName As String = ""
                        lblAvailablePoints.Text = ValidateValue(jsonValues.Property("point_balance")) ' json2("available_points").ToObject(Of String)()
                        lblRedeemablePoints.Text = ValidateValue(jsonValues.Property("tentative_points")) 'json2("redeemable_points").ToObject(Of String)()
                        lblRedeemableAmt.Text = Format(GetDoubleVal(ValidateValue(jsonValues.Property("point_balance"))) / 10, "#,##0.00")
                        ParentName = ValidateValue(jsonValues.Property("first_name")) & " " & ValidateValue(jsonValues.Property("last_name"))
                        lblEmailId.Text = ParentName & ", " & objcls.PARENT_EMAIL
                    End If
                    'responseReader = New StreamReader(webRequest.GetResponse().GetResponseStream())
                    'Dim responseData As String = responseReader.ReadToEnd()
                    'Dim bool = clsRewards.UPDATE_ENTERTAINER_API_CALL_LOG(ACL_ID, Left(responseData, 2000))
                    'Dim json1 = JObject.Parse(responseData)
                    'If Not json1("success") Is Nothing AndAlso json1("success").ToObject(Of String)().ToLower = "true" Then 'if web api return with success=true message
                    '    Dim json2 = JObject.Parse(json1("data").ToString())
                    '    lblAvailablePoints.Text = ValidateValue(json2.Property("available_points")) ' json2("available_points").ToObject(Of String)()
                    '    lblRedeemablePoints.Text = ValidateValue(json2.Property("redeemable_points")) 'json2("redeemable_points").ToObject(Of String)()
                    '    lblRedeemableAmt.Text = Format(GetDoubleVal(ValidateValue(json2.Property("redeemable_amount"))), "#,##0.00") 'Format(GetDoubleVal(json2("redeemable_amount").ToObject(Of String)()), "#,##0.00")
                    'End If
                Catch ex As WebException
                    Errorlog(ex.Message & "Rewards - PointSummary - WebApi call", "GEMSPARENT")
                End Try
            Else
                ShowMessage(getErrorMessage("343"))
                lblEmailId.Text = ""
            End If
        Else
            ShowMessage("Your session expired! Unable to fetch the GEMS Rewards data.")
        End If
    End Sub
    Private Sub REDEEM_POINTS_CALL(ByVal GEMSTransactionID As Long, ByVal RedeemingAmount As Double)
        If Not Session("STU_ID") Is Nothing AndAlso Convert.ToInt64(Session("STU_ID")) > 0 Then
            Dim objcls As New clsRewards
            objcls.FETCH_REWARDS_INFO(Convert.ToInt64(Session("STU_ID")))

            'objcls.CUSTOMER_ID = 2049708

            If objcls.CUSTOMER_ID.Trim <> "" And objcls.CUSTOMER_ID.Trim <> "0" Then
                Try
                    Dim ACL_ID As Long = 0, GENERATED_TOKEN As String = "", responseData As String = ""
                    If objcls.GENERATE_ACCESS_TOKEN(objcls, GENERATED_TOKEN) Then
                        objcls.GET_API_CALL_PARAMETERS("BURNPOINTS") 'api to burn points
                        Dim postString As String = String.Format("customer_id={0}&type={1}&activity={2}&description={3}&burn_points={4}", objcls.CUSTOMER_ID, "parent", "4", "burning points with refId." & GEMSTransactionID.ToString & "", (RedeemingAmount * 10))
                        Const ContType As String = "application/x-www-form-urlencoded"
                        Dim str = clsRewards.SAVE_ENTERTAINER_API_CALL_LOG(ACL_ID, Session("sBsuid"), DateTime.Now, "BURNPOINTS", objcls.API_URI, objcls.API_METHOD, objcls.CUSTOMER_ID, Session("username"))

                        Dim webRequest As HttpWebRequest = TryCast(Net.WebRequest.Create(objcls.API_URI), HttpWebRequest)
                        webRequest.Method = objcls.API_METHOD
                        webRequest.ContentType = ContType
                        webRequest.ContentLength = postString.Length
                        webRequest.Headers.Add("TP_APPLICATION_KEY", objcls.API_TOKEN)
                        webRequest.Headers.Add("CC_TOKEN", GENERATED_TOKEN)
                        Dim requestWriter As New StreamWriter(webRequest.GetRequestStream())
                        requestWriter.Write(postString)
                        requestWriter.Close()
                        Dim responseReader As StreamReader
                        responseReader = Nothing
                        responseReader = New StreamReader(webRequest.GetResponse().GetResponseStream())
                        responseData = responseReader.ReadToEnd()
                        Dim bool = clsRewards.UPDATE_ENTERTAINER_API_CALL_LOG(ACL_ID, Left(responseData, 2000))
                        Dim jsonBurnResponse = JObject.Parse(responseData)
                        Dim transaction_id As String = "", status_code As String = "", message As String = ""
                        If Not jsonBurnResponse("status") Is Nothing AndAlso jsonBurnResponse("status").ToObject(Of String)().ToLower = "true" Then
                            ShowMessage("Points redeemed successfully for the requested amount", False)
                            transaction_id = "" : status_code = "" : message = ""
                            If Not jsonBurnResponse("status_code") Is Nothing Then
                                status_code = jsonBurnResponse("status_code").ToObject(Of String)()
                            End If
                            If Not jsonBurnResponse("message") Is Nothing Then
                                message = jsonBurnResponse("message").ToObject(Of String)()
                            End If
                            If Not jsonBurnResponse("values") Is Nothing Then
                                Dim jsonBurnResponse_values = JObject.Parse(jsonBurnResponse("values").ToString())
                                transaction_id = ValidateValue(jsonBurnResponse_values.Property("transaction_id"))
                            End If
                            UPDATE_COLLECTION_INITIATED(GEMSTransactionID, objcls.API_URI, "true", "200OK", status_code, status_code, message, transaction_id, "success") 'updating the api call response
                            SAVE_COLLECTION_SUCCESS(GEMSTransactionID)
                        ElseIf Not jsonBurnResponse("status") Is Nothing AndAlso jsonBurnResponse("status").ToObject(Of String)().ToLower = "false" Then
                            transaction_id = "" : status_code = "" : message = ""
                            If Not jsonBurnResponse("message") Is Nothing Then
                                message = jsonBurnResponse("message").ToObject(Of String)()
                            End If
                            UPDATE_COLLECTION_INITIATED(GEMSTransactionID, objcls.API_URI, "false", "200OK", status_code, status_code, message, transaction_id, "")
                        End If
                    End If
                    'Try
                    '    'responseReader = New StreamReader(WebRequest.GetResponse().GetResponseStream())
                    '    'Dim responseData As String = responseReader.ReadToEnd()
                    '    'Dim bool = clsRewards.UPDATE_ENTERTAINER_API_CALL_LOG(ACL_ID, Left(responseData, 2000))
                    '    Dim json1 = JObject.Parse(responseData)
                    '    If Not json1("success") Is Nothing AndAlso json1("success").ToObject(Of String)().ToLower = "true" Then 'if web api return with success=true message
                    '        ShowMessage("Points redeemed successfully for the requested amount", False)
                    '        Dim http_response As String = ValidateValue(json1.Property("http_response"))
                    '        Dim code As String = ValidateValue(json1.Property("code"))
                    '        Dim json2 = JObject.Parse(json1("data").ToString())

                    '        Dim data_code = ValidateValue(json2.Property("code"))
                    '        Dim data_message = ValidateValue(json2.Property("message"))
                    '        Dim data_transaction_id = ValidateValue(json2.Property("transaction_id"))

                    '        Dim data_status = ValidateValue(json2.Property("status"))
                    '        UPDATE_COLLECTION_INITIATED(GEMSTransactionID, "/web/v1/gems/points_summary", "true", http_response, code, data_code, data_message, data_transaction_id, data_status) 'updating the api call response
                    '        SAVE_COLLECTION_SUCCESS(GEMSTransactionID)
                    '    ElseIf Not json1("success") Is Nothing AndAlso json1("success").ToObject(Of String)().ToLower = "false" Then
                    '        Dim http_response As String = ValidateValue(json1.Property("http_response"))
                    '        Dim code = ValidateValue(json1.Property("code"))
                    '        Dim json2 = JObject.Parse(json1("data").ToString())

                    '        Dim data_code = ValidateValue(json2.Property("code"))
                    '        Dim data_message = ValidateValue(json2.Property("message"))
                    '        Dim data_transaction_id = ValidateValue(json2.Property("transaction_id"))
                    '        UPDATE_COLLECTION_INITIATED(GEMSTransactionID, "/web/v1/gems/points_summary", "false", http_response, code, data_code, data_message, data_transaction_id, "")
                    '    End If
                    'Catch ex As WebException
                    '    Errorlog(ex.Message & "Rewards - PointRedeem - WebApi call", "GEMSPARENT")
                    '    Dim errResp As WebResponse = ex.Response
                    '    Using respStream As Stream = errResp.GetResponseStream()
                    '        Dim reader As New StreamReader(respStream)
                    '        Dim errorMessage As String = reader.ReadToEnd()
                    '        If errorMessage <> "" Then
                    '            Dim bool = clsRewards.UPDATE_ENTERTAINER_API_CALL_LOG(ACL_ID, Left(errorMessage, 2000))
                    '            Dim json1 = JObject.Parse(errorMessage)
                    '            If Not json1("success") Is Nothing AndAlso json1("success").ToObject(Of String)().ToLower = "false" Then 'if web api return with success=false message
                    '                Dim json2 = JObject.Parse(json1("data").ToString())
                    '                Dim data_code = ValidateValue(json2.Property("code")) 'IIf(json2("code") Is Nothing, "", json2("code").ToObject(Of String)())
                    '                Dim data_message = ValidateValue(json2.Property("message")) 'IIf(json2("message") Is Nothing, "", json2("message").ToObject(Of String)())
                    '                Select Case data_code
                    '                    Case "41" 'Invalid Authentication
                    '                        ShowMessage(getErrorMessage("341"))
                    '                    Case "42" 'Invalid token
                    '                        ShowMessage(getErrorMessage("342"))
                    '                    Case "43" 'Invalid persistent User ID
                    '                        ShowMessage(getErrorMessage("343"))
                    '                    Case "51" 'Invalid Authentication
                    '                        ShowMessage(getErrorMessage("351"))
                    '                    Case "52" 'Invalid token
                    '                        ShowMessage(getErrorMessage("352"))
                    '                    Case "54" 'Invalid persistent User ID
                    '                        ShowMessage(getErrorMessage("354"))
                    '                    Case "55" 'Invalid Authentication
                    '                        ShowMessage(getErrorMessage("355"))
                    '                    Case "61" 'Error encountered while processing transaction!
                    '                        ShowMessage(getErrorMessage("361"))
                    '                End Select
                    '                UPDATE_COLLECTION_INITIATED(GEMSTransactionID, "/web/v1/gems/points_summary", "false", "", "", data_code, data_message, GEMSTransactionID, "")
                    '                Errorlog(data_code & "- " & data_message & " - /web/v1/gems/points_summary", "GEMSPARENT")
                    '            End If
                    '        End If
                    '    End Using
                    'End Try
                Catch ex As Exception
                    ShowMessage(getErrorMessage(1000), True)
                    Errorlog(ex.Message)
                End Try

            Else 'If persistent id not found
                ShowMessage(getErrorMessage("343"))
            End If
        Else
            ShowMessage("Your session expired! Unable to fetch the GEMS Rewards data.")
        End If
    End Sub
    Private Function ValidateValue(ByVal parameter As JProperty) As String
        ValidateValue = ""
        Try
            Dim msgProperty = parameter
            If msgProperty IsNot Nothing Then
                ValidateValue = msgProperty.Value
            End If
        Catch ex As Exception

        End Try
    End Function
    Private Sub UPDATE_COLLECTION_INITIATED(ByVal PRO_ID As Long, ByVal API_CMD As String, ByVal API_SUCCESS As String, ByVal API_HTTP_RESPONSE As String, _
                                            ByVal API_CODE As String, ByVal API_DATA_CODE As String, ByVal API_DATA_MESSAGE As String, _
                                            ByVal API_DATA_TRANSACTION_ID As String, ByVal API_DATA_STATUS As String, _
                                            Optional ByVal bPointUpdate As Boolean = False, Optional ByVal REDEEMABLE_AMOUNT As Double = 0, _
                                            Optional ByVal REDEEMABLE_POINTS As Double = 0, Optional ByVal AVAILABLE_POINTS As Double = 0)
        Dim objclsrewards As New clsRewards
        With objclsrewards
            .PRO_ID = PRO_ID
            .API_CMD = API_CMD
            .PRO_API_SUCCESS = API_SUCCESS
            .PRO_API_HTTP_RESPONSE = API_HTTP_RESPONSE
            .PRO_API_CODE = API_CODE
            .PRO_API_DATA_CODE = API_DATA_CODE
            .PRO_API_DATA_MESSAGE = API_DATA_MESSAGE
            .PRO_API_DATA_TRANSACTION_ID = API_DATA_TRANSACTION_ID
            .PRO_API_DATA_STATUS = API_DATA_STATUS
            .bUpdate_Points = bPointUpdate
            .PRO_API_DATA_REDEEMABLE_AMOUNT = REDEEMABLE_AMOUNT
            .PRO_API_DATA_REDEEMABLE_POINTS = REDEEMABLE_POINTS
            .PRO_API_DATA_AVAILABLE_POINTS = AVAILABLE_POINTS
            .UPDATE_POINTS_REDEMPTION_ONLINE() 'function to update the web api call response fields
        End With
    End Sub

    Protected Sub repInfo_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles repInfo.ItemDataBound
        Dim hfSTU_ID As New HiddenField
        Dim hfSTU_ACD_ID As New HiddenField
        Dim hfSTU_BSU_ID As New HiddenField
        Dim gvFee As New GridView
        Dim lnkViewFeeSetup As New LinkButton
        Dim lblGvrError As New Label
        If e.Item.DataItem Is Nothing Then
            Return
        Else
            gvFee = DirectCast(e.Item.FindControl("gvFee"), GridView)
            hfSTU_ID = DirectCast(e.Item.FindControl("hfSTU_ID"), HiddenField)
            hfSTU_ACD_ID = DirectCast(e.Item.FindControl("hfSTU_ACD_ID"), HiddenField)
            hfSTU_BSU_ID = DirectCast(e.Item.FindControl("hfSTU_BSU_ID"), HiddenField)
            lnkViewFeeSetup = DirectCast(e.Item.FindControl("lnkViewFeeSetup"), LinkButton)
            lblGvrError = DirectCast(e.Item.FindControl("lblGvrError"), Label)

            If IsDate(Format(Date.Now, OASISConstants.DateFormat)) Then
                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim dt As New DataTable
                dt = FeeCommon.F_GetFeeDetailsFOrCollection_online(Format(Date.Now, OASISConstants.DateFormat), hfSTU_ID.Value, "S", hfSTU_BSU_ID.Value)
                If dt.Rows.Count > 0 Then
                    lnkViewFeeSetup.Attributes.Add("onClick", "return ShowSubWindowWithClose('PopupShowDataFeeSetup.aspx?ID=FEESETUPMULTI&STU_ID=" & hfSTU_ID.Value & "&ACD_ID=" & hfSTU_ACD_ID.Value & "', '', '50%', '50%');")
                    gvFee.DataSource = dt
                    gvFee.DataBind()
                    If Not FeeCollectionOnline.CheckIfStudentAllowedForOnlinePayment(hfSTU_ID.Value) Then
                        lblGvrError.Text = "Please contact the school – Fee payments will be accepted at the school Fee counter only."
                        gvFee.Enabled = False
                    Else
                        lblGvrError.Text = ""
                        btnRedeem.Enabled = True
                        gvFee.Enabled = True
                    End If
                    gvFee.DataSource = dt
                    gvFee.DataBind()
                    Set_GridTotal(False)
                Else
                    gvFee.DataBind()
                    Set_GridTotal(False)
                End If
            End If
        End If
    End Sub
    Protected Sub gvFeeCollection_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        Try
            If e.Row.RowType = DataControlRowType.Header Then
                dDiscountTotal = 0
                dDueTotal = 0
                dPayAmountTotal = 0
                dProcessingChargeTotal = 0
            ElseIf e.Row.RowType = DataControlRowType.DataRow Then
                Dim h_BlockPayNow As HiddenField, txtAmountToPay As TextBox
                Dim lblAmount As New Label
                Dim lblDiscount As New Label
                lblAmount = CType(e.Row.FindControl("lblAmount"), Label)
                lblDiscount = CType(e.Row.FindControl("lblDiscount"), Label)
                h_BlockPayNow = CType(e.Row.FindControl("h_BlockPayNow"), HiddenField)
                txtAmountToPay = CType(e.Row.FindControl("txtAmountToPay"), TextBox)
                Dim h_ProcessingCharge As HiddenField = CType(e.Row.FindControl("h_ProcessingCharge"), HiddenField)

                If h_BlockPayNow IsNot Nothing And txtAmountToPay IsNot Nothing Then
                    If h_BlockPayNow.Value = "1" Or h_BlockPayNow.Value.ToLower = "true" Then
                        txtAmountToPay.Enabled = False
                    Else
                        txtAmountToPay.Enabled = True
                    End If
                End If
                dDiscountTotal = dDiscountTotal + IIf(Not lblDiscount Is Nothing, GetDoubleVal(lblDiscount.Text), 0)
                dDueTotal = dDueTotal + IIf(Not lblAmount Is Nothing, GetDoubleVal(lblAmount.Text), 0)
                dPayAmountTotal = dPayAmountTotal + IIf(Not txtAmountToPay Is Nothing, GetDoubleVal(txtAmountToPay.Text), 0)
                dProcessingChargeTotal = dProcessingChargeTotal + IIf(Not h_ProcessingCharge Is Nothing, GetDoubleVal(h_ProcessingCharge.Value), 0)
            ElseIf e.Row.RowType = DataControlRowType.Footer Then
                Dim lblAmountF As New Label
                Dim lblDiscountF As New Label
                Dim lblAmounttoPayF As New Label
                Dim h_ProcessingChargeF As New HiddenField
                lblAmountF = CType(e.Row.FindControl("lblAmountF"), Label)
                lblDiscountF = CType(e.Row.FindControl("lblDiscountF"), Label)
                lblAmounttoPayF = CType(e.Row.FindControl("lblAmounttoPayF"), Label)
                h_ProcessingChargeF = CType(e.Row.FindControl("h_ProcessingChargeF"), HiddenField)

                lblAmountF.Text = dDueTotal
                lblDiscountF.Text = dDiscountTotal
                lblAmounttoPayF.Text = dPayAmountTotal
                h_ProcessingChargeF.Value = dProcessingChargeTotal

                dDiscountTotal = 0
                dDueTotal = 0
                dPayAmountTotal = 0
                dProcessingChargeTotal = 0
            End If
        Catch ex As Exception

        End Try

    End Sub
    Protected Sub txtAmountToPay_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            'Dim myCell, myRow As Object
            'myCell = sender.parent
            'If Not myCell Is Nothing Then
            '    myRow = myCell.parent
            '    If Not myRow Is Nothing Then
            '        Dim h_Discount As HiddenField = CType(myRow.FindControl("h_Discount"), HiddenField)
            '        If Not h_Discount Is Nothing Then
            '            h_Discount.Value = 0
            '        End If
            '    End If
            'End If
            bPaymentConfirmMsgShown = False
            Set_GridTotal(False)
        Catch ex As Exception

        End Try

    End Sub

    Protected Sub btnRedeem_Click(sender As Object, e As EventArgs) Handles btnRedeem.Click
        If VALIDATE_SAVE() Then
            Set_GridTotal(True)
            SAVE_COLLECTION_INITIATED()
        End If
    End Sub
    Protected Sub btnSkip_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSkip.Click
        Me.testpopup.Style.Item("display") = "none"
        Me.btnRedeem.Visible = True
        bPaymentConfirmMsgShown = False
    End Sub
    Protected Sub btnProceed_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnProceed.Click
        If Not ViewState("PRO_PRO_ID") Is Nothing AndAlso Not ViewState("dblTotalAmt") Is Nothing AndAlso IsNumeric(ViewState("PRO_PRO_ID")) AndAlso IsNumeric(ViewState("dblTotalAmt")) Then
            Me.testpopup.Style.Item("display") = "none"
            Me.btnRedeem.Visible = True
            bPaymentConfirmMsgShown = False
            REDEEM_POINTS_CALL(ViewState("PRO_PRO_ID"), ViewState("dblTotalAmt"))
        End If
    End Sub
End Class
