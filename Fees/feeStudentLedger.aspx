﻿<%@ Page Language="VB"  MasterPageFile="~/ParentMaster.master" AutoEventWireup="false" CodeFile="feeStudentLedger.aspx.vb" Inherits="Fees_feeStudentLedger" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphParent" runat="Server">

      <script src="../Scripts/jquery-1.9.1.js" type="text/javascript"></script>
    <script type="text/javascript">
        Sys.Application.add_load(
        function CheckForPrint() {
            if ($("#<%=h_print.ClientID%>").val() != "") {
                //alert('Hi');
                var html = $("#<%=h_print.ClientID%>").val();
                var printWin = window.open('', '', 'left=0,top=0,width=1000,height=1000,status=0');
                printWin.document.write(html);
                printWin.document.close();
                printWin.focus();
                printWin.print();
                printWin.close();
                $("#<%=h_print.ClientID%>").val('');
            }
        });
    </script>
    <style type="text/css">
        div.RadComboBoxDropDown_Default .rcbImage {
            vertical-align: middle;
            max-height: 40px !important;
            max-width: 30px !important;
            border-radius: 50% 50% 50% 50% !important;
        }
    </style>

        <div class="content margin-top30 margin-bottom60">
                    <div class="container">
                        <div class="row">


    <!-- Posts Block -->
    <div class="posts-block col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="my-account">
            
            <div class="bottom-padding">
                <div class="title-box">
                    <h3> Statement Of Accounts <%= IIf(Session("BSU_CURRENCY") Is Nothing, "", "(Currency " & Session("BSU_CURRENCY") & ")")%> 
                    <span class="profile-right">
                                <asp:Label ID="lbChildNameTop" runat="server"></asp:Label>
                            </span>
                    </h3>
                </div>
                <!-- Table  -->
                <div class="table-responsive">
  
    <asp:Label ID="lblerror" runat="server" CssClass="error"></asp:Label>
 <%--   <div class="mainheading">
        <div class="left">
           
        </div>
    </div>--%>
    <asp:HiddenField ID="h_print" runat="server" />
    <table width="100%" class="table table-striped table-bordered table-responsive text-left my-orders-table">
        <tr class="matters">
            <td class="tdfields" colspan="6">
                <asp:Label ID="lblMessage" runat="server" EnableViewState="true"></asp:Label>
            </td>
        </tr>
        <tr class="matters">
            <td class="tdfields">Student</td>
            <td colspan="3">
                <telerik:RadComboBox ID="radCmbSiblings" CssClass="form-control" AutoPostBack="true" Visible="false" runat="server" HighlightTemplatedItems="true"></telerik:RadComboBox>
                <asp:DropDownList ID="ddlSiblings" CssClass="form-control" Width="50%" AutoPostBack="true" runat="server">
                </asp:DropDownList>
            </td>
            <td colspan="2" align="right">
                <table width="auto" style="align-self: center; border: none !important;" border="0">
                    <tr>
                        <td>
                            <asp:ImageButton ID="btnEmail" Width="20px" runat="server" AlternateText="Email" ToolTip="Email Statement"
                                ImageUrl="~/Images/email.png" />
                        </td>
                        <td>
                            <asp:ImageButton ID="btnPrint" Width="20px" runat="server" AlternateText="Print" ToolTip="Print Statement"
                                ImageUrl="~/Images/print.png" />
                            <%--<img alt="Print" src="../Images/Fees/Receipt/print.gif" height="29" width="29" onclick="PrintReceipt();" style="cursor: hand" />--%>
                        </td>
                        <td>
                            <asp:ImageButton ID="btnPDF" Width="20px" runat="server" AlternateText="Download" ToolTip="Download as PDF"
                                ImageUrl="~/Images/pdf.png" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr class="tdfields">
            <td class="tdfields">From Date
            </td>
            <td>
                <asp:TextBox ID="txtFrom" CssClass="form-control" runat="server"></asp:TextBox>
            </td>
            <td>To Date
            </td>
            <td>
                <asp:TextBox ID="txtTo" runat="server" CssClass="form-control"></asp:TextBox>
            </td>
            <td>
                <asp:RadioButton ID="RdDetails" runat="server" Checked="True" GroupName="Rd" Text="Details"
                    AutoPostBack="True" />
                <asp:RadioButton ID="RdSummary" runat="server" GroupName="Rd" Text="Summary" AutoPostBack="True" />
            </td>
            <td>
                <asp:Button ID="btnsearch" runat="server" CssClass="btn btn-info" Text="Show"
                    />
            </td>
        </tr>
        <tr id="trHeader" runat="server" visible="false">
            <td colspan="6">
                <table id="tblHeader" runat="server" style="border: 1pt solid #000000; display: block;" width="100%">
                    <tr>
                        <%--<td align="center" style="font-weight: bold; width: 40px;" rowspan="4">
                            <div>
                                <asp:Image ID="imgBSULOGO" runat="server" />
                            </div>
                        </td>--%>
                        <td colspan="4" align="center" style="font-weight: bold;">
                            <asp:Label ID="lblBSU" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center">
                            <asp:Label ID="lblReportname" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center">
                            <asp:Label ID="lblPeriod" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" style="font-weight: bold;">Student Id :
                        </td>
                        <td align="left">
                            <asp:Label ID="lblStNo" runat="server" Text=""></asp:Label>
                        </td>
                        <td align="right" style="font-weight: bold;">Name :
                        </td>
                        <td align="left">
                            <asp:Label ID="lblName" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr class="matters">
            <td colspan="6">
                <asp:GridView ID="gvLedger" runat="server" AutoGenerateColumns="False" SkinID="GridViewNormal" CssClass="table table-striped table-bordered table-responsive text-left my-orders-table"
                    Width="100%" Style="text-indent: 5px" ShowFooter="True" AllowPaging="True">
                    <Columns>
                        <asp:BoundField DataField="ACY_DESCR" Visible="False">
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField DataField="DOCDATE" HeaderText="Date" DataFormatString="{0:dd/MMM/yyyy}"
                            HtmlEncode="False">
                            <ItemStyle HorizontalAlign="Center" />
                            <HeaderStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="RECNO" HeaderText="Ref.No">
                            <ItemStyle HorizontalAlign="Left" />
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField DataField="FEE_DESCR" HeaderText="Fees">
                            <ItemStyle HorizontalAlign="Left" />
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField DataField="FCL_NARRATION" HeaderText="Narration">
                            <ItemStyle HorizontalAlign="Left" />
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField DataField="DEBIT" HeaderText="Debit" DataFormatString="{0:0.00}">
                            <ItemStyle HorizontalAlign="Right" />
                            <FooterStyle HorizontalAlign="Right" />
                            <HeaderStyle HorizontalAlign="Right" />
                        </asp:BoundField>
                        <asp:BoundField DataField="CREDIT" HeaderText="Credit" DataFormatString="{0:0.00}">
                            <ItemStyle HorizontalAlign="Right" />
                            <FooterStyle HorizontalAlign="Right" />
                            <HeaderStyle HorizontalAlign="Right" />
                        </asp:BoundField>
                    </Columns>
                    <PagerStyle CssClass="gridpager" />
                </asp:GridView>
                <asp:GridView ID="gvSummary" runat="server" AutoGenerateColumns="False" Style="text-indent: 5px"
                    SkinID="GridViewNormal" Width="100%" Visible="False" ShowFooter="True" AllowPaging="True"  CssClass="table table-striped table-bordered table-responsive text-left my-orders-table">
                    <Columns>
                        <asp:BoundField DataField="ACY_DESCR" HeaderText="Academic Year">
                            <HeaderStyle HorizontalAlign="left" />
                            <ItemStyle HorizontalAlign="left" />
                        </asp:BoundField>
                        <asp:BoundField DataField="FEE_DESCR" HeaderText="Fee Details">
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField DataField="DEBIT" HeaderText="Debit" DataFormatString="{0:0.00}">
                            <FooterStyle HorizontalAlign="Right" />
                            <ItemStyle HorizontalAlign="Right" />
                            <HeaderStyle HorizontalAlign="Right" />
                        </asp:BoundField>
                        <asp:BoundField DataField="CREDIT" HeaderText="Credit" DataFormatString="{0:0.00}">
                            <FooterStyle HorizontalAlign="Right" />
                            <ItemStyle HorizontalAlign="Right" />
                            <HeaderStyle HorizontalAlign="Right" />
                        </asp:BoundField>
                    </Columns>
                    <PagerStyle CssClass="gridpager" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td align="right" colspan="6">
                <asp:Label ID="lblTotal" runat="server" CssClass="error">&nbsp;&nbsp;</asp:Label>
            </td>
        </tr>
    </table>
    <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd/MMM/yyyy"
        TargetControlID="txtTo">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MMM/yyyy"
        TargetControlID="txtFrom">
    </ajaxToolkit:CalendarExtender>


                      </div>
                <!-- /Table  -->
            </div>

        </div>
    </div>
  
                        </div>
                </div>
        </div>

</asp:Content>
