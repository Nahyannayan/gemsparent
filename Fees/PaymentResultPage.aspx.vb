﻿Imports System
Imports System.Linq
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data

Partial Class Fees_PaymentResultPage
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If Session("username") Is Nothing Then
                Session("Active_tab") = "Home"
                Session("Site_Path") = ""
                Response.Redirect("~\General\Home.aspx")
            ElseIf Session("bPasswdChanged") = "False" Then
                Response.Redirect("~\UpdateInfo\ForcePasswordChange.aspx", False)
            ElseIf Session("bUpdateContactDetails") = "False" Then
                Response.Redirect(Session("ForceUpdate_stud"), False)
            End If
            Dim FCO_FCO_ID As String = "0", SRC As String = "", TYPE As String = "", bPaymentSuccess As Boolean = False
            If Not Request.QueryString("ID") Is Nothing AndAlso Request.QueryString("ID") <> "" Then
                FCO_FCO_ID = Encr_decrData.Decrypt(Request.QueryString("ID").Replace(" ", "+"))
            End If
            If Not Request.QueryString("SRC") Is Nothing AndAlso Request.QueryString("SRC") <> "" Then
                SRC = Encr_decrData.Decrypt(Request.QueryString("SRC").Replace(" ", "+"))
            End If
            If Not Request.QueryString("TYPE") Is Nothing AndAlso Request.QueryString("TYPE") <> "" Then
                TYPE = Encr_decrData.Decrypt(Request.QueryString("TYPE").Replace(" ", "+"))
            End If
            If Not IsNumeric(FCO_FCO_ID) Then 'removes alphabets from the viariable and keeps the numeric part only
                FCO_FCO_ID = Integer.Parse(New String(FCO_FCO_ID.Where(AddressOf Char.IsDigit).ToArray))
            End If

            urcActvityPaidResult.Visible = False
            urcStudentPaidResult.Visible = False
            urcStudentTransportPaidResult.Visible = False
            urcBookSalePaidResult.Visible = False
            If TYPE = "SUCCESS" Then
                bPaymentSuccess = True
            End If
            SetMessage(TYPE)
            Select Case SRC
                Case "TRANSPORT"
                    urcStudentTransportPaidResult.Visible = True
                    urcStudentTransportPaidResult.Gridbind_PayDetails(FCO_FCO_ID, bPaymentSuccess)
                Case "SCHOOL"
                    urcStudentPaidResult.Visible = True
                    urcStudentPaidResult.Gridbind_PayDetails(FCO_FCO_ID, bPaymentSuccess)
                Case "ACTIVITY_FC", "ACTIVITY"
                    urcActvityPaidResult.Visible = True
                    urcActvityPaidResult.Gridbind_PayDetails(FCO_FCO_ID, bPaymentSuccess, "FC")
                Case "ACTIVITY_OC"
                    urcActvityPaidResult.Visible = True
                    urcActvityPaidResult.Gridbind_PayDetails(FCO_FCO_ID, bPaymentSuccess, "OC")
                Case "BOOKSALES"
                    urcBookSalePaidResult.Visible = True
                    urcBookSalePaidResult.Gridbind_PayDetails(FCO_FCO_ID, bPaymentSuccess)
                Case "SERVICES"
                    trExtraProvResult.Visible = True
                    urcExtraProvisionPaidResult.Gridbind_PayDetails(FCO_FCO_ID, bPaymentSuccess, "FC")
            End Select
            If SRC = "BOOKSALES" And bPaymentSuccess Then
                Dim ds5 As New DataSet
                Dim BSAH_IDD As String = "0"
                ds5 = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_PUR_INVConnectionString, CommandType.Text, "SELECT DISTINCT O.BSAHO_BSAH_ID FROM [OASIS_PUR_INV].[dbo].[BOOK_SALE_ONLINE_H] O WITH(NOLOCK) INNER JOIN [OASIS_PUR_INV].[dbo].[BOOK_SALE_H] C WITH(NOLOCK) ON O.BSAHO_BSAH_ID=C.BSAH_BSAH_ID WHERE O.BSAHO_BSAHO_ID=" & FCO_FCO_ID)
                BSAH_IDD = CInt((ds5.Tables(0).Rows(0)("BSAHO_BSAH_ID")))
                SendEmailNotification(2, BSAH_IDD, Session("sBsuid"), "BOOKSALE", "GEMS")
            End If
        End If
    End Sub

    Private Sub SetMessage(ByVal pTYPE As String)
        Select Case pTYPE
            Case "NPF" '-------No query string parameters found
                lblMessage.CssClass = "alert alert-danger"
                lblMessage.Text = "Unable to find necessary parameters"
            Case "PSW" '-------Status for the provided id is not "INITIATED"
                lblMessage.CssClass = "alert alert-danger"
                lblMessage.Text = "The Payment request is invalid"
            Case "UPP" '-------Unable to continue payment due to technical error
                lblMessage.CssClass = "alert alert-danger"
                lblMessage.Text = "Unable to proceed to the payment portal due to a technical issue, Please try after some time."
            Case "PAID_NORECEIPT" '-------If payment is successful
                lblMessage.CssClass = "alert alert-success"
                lblMessage.Text = "The payment was successful and the receipt will be emailed shortly"
            Case "CANCEL" '-------Payment has been cancelled by the user
                lblMessage.CssClass = "alert alert-info"
                lblMessage.Text = "The Payment request has been cancelled"
            Case "SUCCESS" '-------Payment has been cancelled by the user
                lblMessage.CssClass = "alert alert-success"
                lblMessage.Text = "The Payment processed successfully, the receipt will be emailed shortly"
            Case "FAILED" '-------Payment was not successful
                lblMessage.CssClass = "alert alert-danger"
                lblMessage.Text = "The Payment transaction was not successful"
            Case "INITIATED" '-------Payment was not successful
                lblMessage.CssClass = "alert alert-success"
                lblMessage.Text = "The payment request has been initiated"
            Case "GUARANTEED" '-------Payment was successful, but reciept not generated.
                lblMessage.CssClass = "alert alert-success"
                lblMessage.Text = "The payment is under processing, will send the receipt once amount is captured."
            Case "DELIVERED"
                lblMessage.CssClass = "alert alert-success"
                lblMessage.Text = "The payment processed successfully."
        End Select
    End Sub

    Public Shared Function SendEmailNotification(ByVal OPTIONS As Integer, ByVal STU_ID As String, ByVal BSU_ID As String, ByVal EML_TYPE As String, ByVal COMPANY As String) As Integer
        Dim pParms(5) As SqlClient.SqlParameter

        pParms(0) = New SqlClient.SqlParameter("@OPTION", SqlDbType.Int)
        pParms(0).Value = OPTIONS
        pParms(1) = New SqlClient.SqlParameter("@STU_ID", SqlDbType.VarChar, 1000)
        pParms(1).Value = STU_ID
        pParms(2) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 50)
        pParms(2).Value = BSU_ID
        pParms(3) = New SqlClient.SqlParameter("@EML_TYPE", SqlDbType.VarChar, 100)
        pParms(3).Value = EML_TYPE
        pParms(4) = New SqlClient.SqlParameter("@COMPANY", SqlDbType.VarChar, 50)
        pParms(4).Value = COMPANY

        Dim ReturnFlag As Integer = SqlHelper.ExecuteNonQuery(ConnectionManger.GetOASISConnection, _
         CommandType.StoredProcedure, "OASIS.[DBO].[BULK_EMAIL_INSERT_SCHEDULE_JOB]", pParms)
        Return ReturnFlag

    End Function
End Class
