﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="PopupShowDataFeeSetup.aspx.vb" Inherits="Fees_PopupShowDataFeeSetup" %>


<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <base target="_self" />
 <%--   <link media="screen" href="../CSS/SiteStyle.css" type="text/css" rel="stylesheet" />--%>
      <link rel="stylesheet" href="../css/bootstrap.css" type="text/css"  />
    <link rel="stylesheet" href="../css/bootstrap-theme.css" type="text/css"  />
    <style type="text/css">
        BODY
        {
            padding-right: 0px;
            padding-left: 0px;
            background: url(images/background1.jpg) #ffffff repeat;
            padding-bottom: 0px;
            margin: 0px;
            font: 12px "Verdana" , sans-serif;
            padding-top: 0px;
        }
    </style>

    <script language="javascript" type="text/javascript">
        function printme() {
            document.getElementById('img_Print').style.display = 'none';
            window.print();
            document.getElementById('img_Print').style.display = 'inline';
        }
    </script>

</head>
<body class="matters" bottommargin="0" leftmargin="0" rightmargin="0" topmargin="0">
    <form id="form1" runat="server"> 
    <div class="title-box">
       <h3>
            Schedule of Fees</h3>
        <asp:HiddenField ID="hf_acdid" runat="server" Visible="true" Value="" />
        <asp:HiddenField ID="hf_stuid" runat="server" Visible="true" Value="" />

    </div>
    <table width="100%" align="center" cellpadding="0" cellspacing="0" style="border-collapse: inherit !important"
        border="1" bordercolor="#E0DBD7" class="table table-bordered table-responsive text-left"> 
        <tr>
            <td align="center">
                <table width="100%">
                    <tr>
                        <td align="left" class="matterscontactus">
                            <asp:Label ID="lblgrade" runat="server" Font-Bold="True"></asp:Label>
                            <span style="color: #FF0000; font-weight: bold; font-size: 10px">(To proceed with the
                                payment process, please close this window )</span>
                        </td>
                        <td align="right">
                            <img id='img_Print' align="absMiddle" onclick="printme();" src="../Images/Fees/Receipt/print.gif"
                                style="cursor: hand;" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:GridView ID="GridViewShowDetails" runat="server" Width="100%" style="border-collapse: inherit !important"
                    EmptyDataText="No Details" AutoGenerateColumns="False"  class="table table-striped table-bordered table-responsive text-left my-orders-table">
                    <Columns>
                        <asp:BoundField DataField="FEE_DESCR" HeaderText="Fee">
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField DataField="FDD_DATE" HeaderText="Due Date">
                            <ItemStyle HorizontalAlign="Center" />
                            <HeaderStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Amount" HeaderText="Amount">
                            <HeaderStyle HorizontalAlign="Right" />
                            <ItemStyle HorizontalAlign="Right" />
                        </asp:BoundField>
                    </Columns>
                </asp:GridView>
            </td>
        </tr> 
        <tr style="display: none">
            <td align="right" class="matters_small">
                * Terms and conditions apply
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
