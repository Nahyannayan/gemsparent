﻿<%@ Page Language="VB"  MasterPageFile="~/ParentMaster.master"  AutoEventWireup="false" CodeFile="FeeCollectionOnlineNew.aspx.vb" Inherits="Fees_FeeCollectionOnlineNew" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphParent" runat="Server">

    
    <script src="../Scripts/jquery-1.9.1.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="../Scripts/jQuery-ui-1.10.3.css" />
    <link href="../css/Popup.css" rel="stylesheet" />
    <!-- Add fancyBox -->
    <script type="text/javascript" src="../Scripts/Fancybox/jquery.fancybox.js?v=2.1.5"></script>
    <script type="text/javascript" src="../Scripts/Fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
    <link rel="stylesheet" href="../Scripts/Fancybox/jquery.fancybox.css" type="text/css"  media="screen" />
    <script type="text/javascript" src="../Scripts/PopupJQuery.js"></script>

    <script type="text/javascript" language="javascript">
        function CheckAmount(e) {
            alert(e);
            var amt = parseFloat(document.getElementById(e).value);
            alert(amt);
            //amt=parseFloat(e.value)
            if (isNaN(amt))
                amt = 0;
            e.value = amt.toFixed(2);
            return true;
        }

        function Numeric_Only() {
            if (event.keyCode < 48 || event.keyCode > 57 || (event.keyCode > 90 & event.keyCode < 97)) {
                if (event.keyCode == 13 || event.keyCode == 45)
                { return false; }
                event.keyCode = 0
            }
        }

        function HideAddFee() {
            if (document.getElementById('TbAddFee').style.display == 'none')
                document.getElementById('TbAddFee').style.display = 'block';
            else
                document.getElementById('TbAddFee').style.display = 'none';
            return false;
        }

        function ShowFeeSetup() {
            var sFeatures, url;
            sFeatures = "dialogWidth:800px; dialogHeight: 600px; help: no; resizable: no; scroll: yes; status: no; unadorned: no; ";
            url = "PopupShowDataFeeSetup.aspx?id=FEESETUP";
            result = window.showModalDialog(url, "", sFeatures);
            return false;
        }

        function ShowWhatsThis() {
            var sFeatures, url;
            sFeatures = "dialogWidth:650px; dialogHeight: 250px; help: no; resizable: no; scroll: yes; status: no; unadorned: no; ";
            url = "whatsThis.htm";
            result = window.showModalDialog(url, "", sFeatures);
            return false;
        }
    </script>
    <link rel="stylesheet" type="text/css" href="../Scripts/jQuery-ui-1.10.3.css" />
    


      <div class="content margin-top30 margin-bottom60">
                    <div class="container">
                        <div class="row">

        <!-- Posts Block -->
    <div class="posts-block col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="my-account">
            
            <div class="bottom-padding">
                <div class="title-box">
                    <h3>Fee Online Payment </h3>
                </div>
                <!-- Table  -->
                <div class="table-responsive">

    <asp:Label ID="lblPayMsg" runat="server" ForeColor="Red"
        EnableViewState="False"></asp:Label>
    <asp:Label ID="lblError" runat="server" EnableViewState="False"
          ForeColor="Red"></asp:Label>

<%--    <div class="mainheading">
        <div class="left">
            Fee Online Payment
        </div>
    </div>--%>
    <table align="center" style="width: 100%;" class="table table-striped table-bordered table-responsive text-left my-orders-table">
        <tr>
            <th align="left"  width="20%">Date
            </th>
            <td align="left" >
                <asp:Label ID="lblDate" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <th align="left" >Student ID
            </th>
            <td align="left" >
                <asp:Label ID="txtStdNo" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <th align="left" >Student Name
            </th>
            <td align="left" >
                <asp:Label ID="txtStudentname" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <th align="left" >Grade &amp; Section
            </th>
            <td align="left" >
                <asp:Label ID="stugrd" runat="server"></asp:Label>
                -<strong></strong>
                <asp:Label ID="lblsct" runat="server"></asp:Label>
                &nbsp; &nbsp;
                <asp:LinkButton ID="lnkShowFeeSetup" Text="View Fee Schedule" runat="server" Font-Bold="True"
                     Font-Underline="True">View Fee Schedule</asp:LinkButton>
               <%-- <asp:Panel ID="pnlFeesetup" runat="server" CssClass="darkPanlvisible" Visible="false">
                    <div class="panelFeeSetup">
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td align="right" height="10px">
                                    <asp:LinkButton ID="lbPnlJobClose" CssClass="closebtnFee" runat="server" OnClick="lbPnlJobClose_Click">Close</asp:LinkButton>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <iframe src="PopupShowDataFeeSetup.aspx?id=FEESETUP" frameborder="0" style="margin: -1px;"
                                        scrolling="no" width="800" height="400" id="IfRefClose"></iframe>
                                </td>
                            </tr>
                        </table>
                    </div>
                </asp:Panel>--%>
            </td>
        </tr>
        <tr>
            <th align="left" >School
            </th>
            <td align="left" >
                <asp:Label ID="lblschool" runat="server" CssClass="font-small-mob"></asp:Label>
            </td>
        </tr>
        <tr id="tr_Discount" runat="server">
            <th align="left" >Discount
            </th>
            <td align="left" >
                <asp:ImageButton ID="ImgMore" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/selectdiscount.jpg"
                    OnClientClick="return false;" TabIndex="8" />
                <ajaxToolkit:PopupControlExtender ID="pceMoreDetails" runat="server" PopupControlID="Panel1"
                    Position="Bottom" TargetControlID="ImgMore">
                </ajaxToolkit:PopupControlExtender>
            </td>
        </tr>
        <tr>
            <th >Select a Payment Gateway
                <br />
                <asp:LinkButton ID="lMoreInfo" runat="server" Font-Bold="True" Font-Underline="True"
                    Text="More Info" ForeColor="Blue">
                </asp:LinkButton>
               <%-- <asp:Panel ID="pnlMoreInfo" runat="server" CssClass="darkPanlvisible" Visible="false">
                    <div class="panelFeeSetup">
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td align="right" height="10px">
                                    <asp:LinkButton ID="LinkButton2" CssClass="closebtnFee" runat="server" OnClick="lbPnlJobClose_Click">Close</asp:LinkButton>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <iframe src="whatsThis.htm" frameborder="0" style="margin: -1px;" scrolling="no"
                                        width="800" height="400" id="Iframe1"></iframe>
                                </td>
                            </tr>
                        </table>
                    </div>
                </asp:Panel>--%>
            </th>
            <td align="left"  colspan="1" style="height: 21px" valign="middle">
                <asp:RadioButtonList ID="rblPaymentGateway" runat="server" RepeatDirection="Horizontal"
                    RepeatLayout="Flow" AutoPostBack="True" RepeatColumns="2">
                </asp:RadioButtonList>
                <br />
                <asp:HiddenField ID="HFCardCharge" runat="server" />
                <asp:RadioButtonList ID="rdlmasterpass" runat="server" RepeatDirection="Horizontal" Visible="false" Style="margin-left: 40%;">
                    <asp:ListItem Value="1" Text="GEMS NBAD Co-Branded"></asp:ListItem>
                    <asp:ListItem Value="2" Text="Other" Selected="True"></asp:ListItem>
                </asp:RadioButtonList>

            </td>
        </tr>
        <tr id="tr_AddFee" runat="server" class="tdblankAll">
            <th align="left" colspan="2" class="trSub_Header">Fee Details
                <%= IIf(Session("BSU_CURRENCY") Is Nothing, "", "(Amount In " & Session("BSU_CURRENCY") & ")")%>
                <%--<asp:label id="lblCurrency" runat="server" Text="" ></asp:label> --%>
                <div>
                    <asp:LinkButton ID="lnkMoreFee" runat="server" OnClientClick="HideAddFee(); return false;"
                        ForeColor="White" Visible="False">Add More Fee Head(s)</asp:LinkButton>&nbsp;
                </div>
            </th>
        </tr>
       
         <tr>
            <td align="left" colspan="2">
                <table cellpadding="0" cellspacing="0" border="0" width="100%" >
                     <tr class="tdblankAll">
            <td align="right" colspan="2" class="tdblankAll">
                <asp:GridView ID="gvFeeCollection" runat="server" AutoGenerateColumns="False" EmptyDataText="No Details"
                    SkinID="GridViewNormal" Width="100%" cssclass="table table-striped table-bordered table-responsive text-left my-orders-table">
                    <RowStyle Height="20px" VerticalAlign="Top" />
                    <Columns>
                        <asp:TemplateField HeaderText="FSR_FEE_ID" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lblFSR_FEE_ID" runat="server" Text='<%# Bind("FEE_ID") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="FEE_DESCR" HeaderText="Fee">
                            <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" Width="50%" />
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="Due">
                            <ItemTemplate>
                                <asp:Label ID="lblAmount" runat="server" Text='<%# Bind("CLOSING", "{0:0.00}") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Right" Width="10%" />
                            <HeaderStyle HorizontalAlign="Right" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Discount">
                            <ItemTemplate>
                                <span class="ui-icon ui-icon-info" id="spanInfo" runat="server" visible="false" style="float: left; margin: 0 15px 10px 0;"></span>
                                <ajaxToolkit:HoverMenuExtender ID="pceDiscountDetail" runat="server" PopupControlID="pnlDiscntDtl"
                                    PopupPosition="Top" TargetControlID="spanInfo" OffsetX="-300">
                                </ajaxToolkit:HoverMenuExtender>
                                <asp:Label ID="lblDiscount" runat="server" Text="0.00"></asp:Label>
                                <asp:Panel ID="pnlDiscntDtl" Style="display: none" runat="server">
                                    <table width="100%;" class="table table-striped table-bordered table-responsive text-left my-orders-table" cellpadding="0" cellspacing="0" align="center">
                                        <tr>
                                            <td>
                                                <asp:Repeater ID="gvDiscDt" runat="server" EnableViewState="false">
                                                    <HeaderTemplate>
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                            <tr class="trSub_Header_Small">
                                                                <th style="width: 250px">Month
                                                                </th>
                                                                <th style="width: 20%">Fee
                                                                </th>
                                                                <th style="width: 20%">Disc(%)
                                                                </th>
                                                                <th style="width: 20%">Discount
                                                                </th>
                                                                <th style="width: 20%">Net
                                                                </th>
                                                            </tr>
                                                    </HeaderTemplate>
                                                    <AlternatingItemTemplate>
                                                        <tr style="background-color: #DFE0E5; color: #1B80B6; border: 1pt; border-color: #1b80b6;">
                                                            <td align="left">
                                                                <%#Container.DataItem("MONTH")%>
                                                            </td>
                                                            <td align="right">
                                                                <%# Math.Round(Convert.ToDecimal(Container.DataItem("FEE_AMOUNT")), 2)%>
                                                            </td>
                                                            <td align="right">
                                                                <%# Math.Round(Convert.ToDecimal(Container.DataItem("DISC_PERC")), 2)%>
                                                            </td>
                                                            <td align="right">
                                                                <%# Math.Round(Convert.ToDecimal(Container.DataItem("DISC_AMOUNT")), 2)%>
                                                            </td>
                                                            <td align="right">
                                                                <%# Math.Round(Convert.ToDecimal(Container.DataItem("NET_AMOUNT")), 2)%>
                                                            </td>
                                                        </tr>
                                                    </AlternatingItemTemplate>
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td align="left">
                                                                <%#Container.DataItem("MONTH")%>
                                                            </td>
                                                            <td align="right">
                                                                <%# Math.Round(Convert.ToDecimal(Container.DataItem("FEE_AMOUNT")), 2)%>
                                                            </td>
                                                            <td align="right">
                                                                <%# Math.Round(Convert.ToDecimal(Container.DataItem("DISC_PERC")), 2)%>
                                                            </td>
                                                            <td align="right">
                                                                <%# Math.Round(Convert.ToDecimal(Container.DataItem("DISC_AMOUNT")), 2)%>
                                                            </td>
                                                            <td align="right">
                                                                <%# Math.Round(Convert.ToDecimal(Container.DataItem("NET_AMOUNT")), 2)%>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                        </table>
                                                    </FooterTemplate>
                                                </asp:Repeater>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                            <ItemStyle HorizontalAlign="Right" Width="10%" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Paying Now">
                            <ItemTemplate>
                                <asp:TextBox ID="txtAmountToPay" CssClass="form-control" AutoCompleteType="Disabled" runat="server" AutoPostBack="True"
                                    onFocus="this.select();" OnTextChanged="txtAmountToPay_TextChanged" Style="text-align: right; width:100%"
                                    TabIndex="52" Text='<%# Bind("Amount", "{0:0.00}") %>' ></asp:TextBox>
                                <asp:LinkButton ID="lbCancel" Visible="false" runat="server" OnClick="lbCancel_Click">Cancel</asp:LinkButton>
                                <asp:HiddenField ID="h_OtherCharge" runat="server" />
                                <asp:HiddenField ID="h_Discount" runat="server" />
                                <asp:HiddenField ID="h_BlockPayNow" runat="server" Value='<%# Bind("BlockPayNow") %>' />
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                            <ItemStyle HorizontalAlign="Right" width="20%" />
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
        <tr class="tdblankAll">
            <td align="center" colspan="2" class="tdblankAll">
                <p><asp:Label ID="lblalert" CssClass="alert-warning alert margin-bottom0" runat="server"></asp:Label></p>
            </td>
        </tr>
        <tr id="tr_DiscountTotal" runat="server" class="tdblankAll">
            <td align="right"  valign="middle" colspan="2">Total :
                <asp:TextBox ID="txtDiscountTotal" CssClass="form-control" Style="text-align: right" runat="server" TabIndex="150"
                    >
                </asp:TextBox>
            </td>
        </tr>
        <tr id="id_notusing" runat="server" visible="false" class="tdblankAll">
            <td align="left" colspan="2" class="tdblankAll">
                <asp:TextBox ID="txtRemarks" CssClass="form-control" runat="server" Height="52px" SkinID="TextBoxMultiTextLarge"
                    TabIndex="100" TextMode="MultiLine" Width="84%" ReadOnly="True" Visible="False">
                </asp:TextBox>
                <asp:DropDownList ID="ddlFeeType" CssClass="form-control" runat="server" DataSourceID="odsGetFEETYPE_M" DataTextField="FEE_DESCR"
                    DataValueField="FEE_ID" SkinID="DropDownListNormal">
                </asp:DropDownList>
                <asp:TextBox ID="txtAmountAdd" CssClass="form-control" runat="server" onblur="CheckNumber(this)" AutoCompleteType="Disabled"
                    TabIndex="45">
                </asp:TextBox>
                <asp:Button ID="btnAddDetails" runat="server" Text="Add" CausesValidation="False"
                    TabIndex="50" />
            </td>
        </tr>
        <tr class="tdblankAll">
            <td width="50%"></td>
            <td align="right" valign="middle" colspan="0" class="padding-bottom20">
                <div>
                <div class="col-sm-6 align-items-center"><strong>Total :</strong></div>
                <div class="col-sm-6 align-items-center">
                    <asp:TextBox ID="txtGridTotal" CssClass="form-control text-field-mob" Style="text-align: right" runat="server" TabIndex="150" Font-Bold="True" Width="100%">
                    </asp:TextBox>
                </div>
                </div>
            </td>
        </tr>
        <tr class="tdblankAll">
            <td width="50%"></td>
            <td align="right" valign="middle" colspan="0" class="padding-bottom20">
                <div>
                <div class="col-sm-6 align-items-center"><strong>Processing Charge :</strong></div>
                <div class="col-sm-6 align-items-center">
                <asp:TextBox ID="txtCardCharge" CssClass="form-control text-field-mob" Style="text-align: right" runat="server" TabIndex="151" Font-Bold="True" Width="100%">
                </asp:TextBox>
                </div>
                </div>
            </td>
        </tr>
        <tr class="tdblankAll">
            <td width="50%"></td>
            <td align="right" valign="middle" colspan="0" class="padding-bottom20">
                <div>
                <div class="col-sm-6 align-items-center the_price"><strong>Net Payable :</strong></div>
                <div class="col-sm-6 align-items-center">
                <asp:TextBox ID="txtTotal" CssClass="form-control text-field-mob" Style="text-align: right" runat="server" TabIndex="152" Font-Bold="True" Width="100%">
                </asp:TextBox>
                </div>
                </div>
            </td>
        </tr>
        <tr class="tdblankAll">
            <td align="center" colspan="2" class="tdblankAll">
                <asp:Button ID="btnSave" runat="server" CssClass="btn btn-info" TabIndex="155" Text="Confirm & Proceed"
                     />
                <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="btn btn-info"
                    Text="Cancel" TabIndex="158" />
            </td>
        </tr>
       

                     </table>
 </td>
        </tr>
         <tr id="tr_comment" runat="server" class=" alert-info alert">
            <td align="left" colspan="2"
                class="tdblankAll">Please enter the amount paying now and Click Proceed.</td>
        </tr>
    </table>
    <asp:ObjectDataSource ID="odsGetFEETYPE_M" runat="server" OldValuesParameterFormatString="original_{0}"
        SelectMethod="GetFEES_M" TypeName="FeeCommon">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="123004" Name="BSU_ID" SessionField="BSU_ID" Type="String" />
            <asp:SessionParameter DefaultValue="0" Name="ACD_ID" SessionField="ACD_ID" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:Panel ID="Panel1" runat="server">
        <table cellpadding="3" cellspacing="0" class="BlueTable_simple">
            <tr class="tdblankAll">
                <td class="tdblankAll" align="left">
                    <asp:GridView ID="gvDiscount" runat="server" SkinID="GridViewNormal" Width="100%"
                        EmptyDataText="No Data Found" AutoGenerateColumns="False">
                        <Columns>
                            <asp:BoundField DataField="Scheme" HeaderText="Scheme" Visible="False">
                                <HeaderStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Priod" HeaderText="Valid period">
                                <HeaderStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Slab" HeaderText="Fee Terms">
                                <HeaderStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Fees" HeaderText="Fees">
                                <HeaderStyle HorizontalAlign="Right" />
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="PayNow" HeaderText="Pay Now">
                                <HeaderStyle HorizontalAlign="Right" />
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:BoundField DataField="YouSave" HeaderText="You Save">
                                <HeaderStyle HorizontalAlign="Right" />
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="Select">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbApply" runat="server" OnClick="lbApply_Click">Select</asp:LinkButton>
                                    <asp:Label ID="lblFee_ID" runat="server" Text='<%# Bind("FEE_ID") %>' Visible="false"></asp:Label>
                                    <asp:Label ID="lblFds_ID" runat="server" Text='<%# Bind("FDS_ID") %>' Visible="false"></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>

                    
                      </div>
                <!-- /Table  -->
            </div>

        </div>
    </div>
        <!-- /Posts Block -->


                        </div>
                    </div>
        </div>

</asp:Content>
