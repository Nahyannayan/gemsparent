Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj

Partial Class ParentLogin_FeeReceiptEMail
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Overloads Overrides Sub Render(ByVal writer As HtmlTextWriter)
        Using msOur As New System.IO.MemoryStream()
            Using swOur As New System.IO.StreamWriter(msOur)
                Dim ourWriter As New HtmlTextWriter(swOur)
                MyBase.Render(ourWriter)
                ourWriter.Flush()
                msOur.Position = 0
                Dim RECNO As String = Encr_decrData.Decrypt(Request.QueryString("id").Replace(" ", "+"))
                Dim BSU_ID As String = Encr_decrData.Decrypt(Request.QueryString("bsu_id").Replace(" ", "+"))

                Using oReader As New System.IO.StreamReader(msOur)
                    Dim sTxt As String = oReader.ReadToEnd()
                    Dim ds As New DataSet
                    ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, _
                    "EXEC ONLINE.GetMailDetals_OASIS '" & Encr_decrData.Decrypt(Request.QueryString("user").Replace(" ", "+")) & _
                    "', '" & Encr_decrData.Decrypt(Request.QueryString("bsu_id").Replace(" ", "+")) & "','" & RECNO & "'")
                    If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 AndAlso Not Session("username") Is Nothing Then
                        If ds.Tables(0).Rows(0)("Emailed") = 0 Then
                            Try
                                eMailReceipt.SendNewsLetters(ds.Tables(0).Rows(0)("SYS_EMAIL_FEEONLINE"), ds.Tables(0).Rows(0)("OLU_Email"), _
                                  "GEMS Online Fee Payment receipt", _
                                  eMailReceipt.convertToEmbedResource(sTxt, Encr_decrData.Decrypt(Request.QueryString("bsu_id").Replace(" ", "+"))), _
                                  ds.Tables(0).Rows(0)("SYS_USERNAME_FEEONLINE"), ds.Tables(0).Rows(0)("SYS_PASSWORD_FEEONLINE"), _
                                  ds.Tables(0).Rows(0)("SYS_EMAIL_HOST"), ds.Tables(0).Rows(0)("SYS_EMAIL_PORT"))

                                'eMailReceipt.SendNewsLetters(ds.Tables(0).Rows(0)("SYS_EMAIL_FEEONLINE"), "shakeel.shakkeer@gemseducation.com", _
                                ' "GEMS Online Fee Payment receipt", _
                                '   eMailReceipt.convertToEmbedResource(sTxt, Encr_decrData.Decrypt(Request.QueryString("bsu_id").Replace(" ", "+"))), _
                                '   ds.Tables(0).Rows(0)("SYS_USERNAME_FEEONLINE"), ds.Tables(0).Rows(0)("SYS_PASSWORD_FEEONLINE"), _
                                '   ds.Tables(0).Rows(0)("SYS_EMAIL_HOST"), ds.Tables(0).Rows(0)("SYS_EMAIL_PORT"))

                                Dim sqlStr As String
                                sqlStr = "UPDATE FEES.FEECOLLECTION_H SET FCL_bEMAILED=1 WHERE FCL_BSU_ID='" & BSU_ID & "' and FCL_RECNO='" & RECNO & "'"
                                SqlHelper.ExecuteNonQuery(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, sqlStr)

                            Catch ex As Exception

                            End Try
                        End If
                    End If
                    'Response.Write(sTxt)
                    oReader.Close()
                End Using
            End Using
        End Using
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Page.IsPostBack = False Then
                'Page.Title = FeeCollectionOnlineBB.GetTransportTitle()7bad10
                'gvFeeDetails.Attributes.Add("bordercolor", "#000095")            
                Select Case Request.QueryString("type")
                    Case "REC"
                        If Request.QueryString("id") <> "" Then
                            PrintReceipt(Encr_decrData.Decrypt(Request.QueryString("id").Replace(" ", "+")), _
                            Encr_decrData.Decrypt(Request.QueryString("bsu_id").Replace(" ", "+")), _
                            Encr_decrData.Decrypt(Request.QueryString("user").Replace(" ", "+")))
                        End If
                End Select
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub PrintReceipt(ByVal p_Receiptno As String, ByVal p_BSU_ID As String, ByVal p_USER As String)
        Dim str_Sql, strFilter As String
        strFilter = "  FCL_RECNO='" & p_Receiptno & "' AND FCL_BSU_ID='" & p_BSU_ID & "' "
        str_Sql = "select * FROM [FEES].[VW_OSO_FEES_FEERECEIPT] WHERE " + strFilter
        Dim cmd As New SqlCommand
        cmd.CommandText = str_Sql
        cmd.CommandType = Data.CommandType.Text
        ' check whether Data Exits
        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim ds As New DataSet
        SqlHelper.FillDataset(str_conn, CommandType.Text, cmd.CommandText, ds, Nothing)
        If Not ds Is Nothing Or ds.Tables(0).Rows.Count > 0 Then
            imgLogo.ImageUrl = "GetLogo.aspx?BSU_ID=" & p_BSU_ID
            lblHeader1.Text = ds.Tables(0).Rows(0)("BSU_HEADER1").ToString().ToLower()
            lblHeader2.Text = ds.Tables(0).Rows(0)("BSU_HEADER2")
            lblHeader3.Text = ds.Tables(0).Rows(0)("BSU_HEADER3")
            lblSchool.Text = ds.Tables(0).Rows(0)("BSU_NAME")
            ' lblBusno.Text = IIf(ds.Tables(0).Rows(0)("BNO_DESCR") Is System.DBNull.Value, "", ds.Tables(0).Rows(0)("BNO_DESCR"))
            lblDate.Text = Format(ds.Tables(0).Rows(0)("FCL_DATE"), "dd/MMM/yyyy")
            lblGrade.Text = IIf(ds.Tables(0).Rows(0)("GRD_DISPLAY") Is System.DBNull.Value, "", ds.Tables(0).Rows(0)("GRD_DISPLAY"))
            lblRecno.Text = ds.Tables(0).Rows(0)("FCL_RECNO")

            lblStudentNo.Text = ds.Tables(0).Rows(0)("STU_NO")
            lblStudentName.Text = ds.Tables(0).Rows(0)("STU_NAME").ToString().ToUpper()
            lbluserloggedin.Text = p_USER & " (Issued Time: " + CDate(ds.Tables(0).Rows(0)("FCL_LOGDATE")).ToString("hh:mm tt") & ")"
            lblPrintTime.Text = "Printed Time: " + Now.ToString("dd/MMM/yyyy hh:mm tt") & ""
            Dim Narration As String = IIf(ds.Tables(0).Rows(0)("FCL_NARRATION").ToString = "", "", "Narration : ")
            lblNarration.Text = Narration & ds.Tables(0).Rows(0)("FCL_NARRATION").ToString
            'lblAmount.Text = Mainclass.SpellNumber(ds.Tables(0).Rows(0)("FCL_AMOUNT"))
            Dim str_paymnts As String = " exec fees.GetReceiptPrint_Online @FCL_RECNO ='" & p_Receiptno & "', @FCL_BSU_ID  = '" & Session("sBsuid") & "'"
            If IsNumeric(ds.Tables(0).Rows(0)("FCL_BALANCE")) AndAlso ds.Tables(0).Rows(0)("FCL_BALANCE") <> 0 Then
                If ds.Tables(0).Rows(0)("FCL_BALANCE") > 0 Then
                    lblBalance.Text = "Due : " & ds.Tables(0).Rows(0)("BSU_CURRENCY") & " " & Format(ds.Tables(0).Rows(0)("FCL_BALANCE"), "0.00")
                Else
                    lblBalance.Text = "Advance : " & ds.Tables(0).Rows(0)("BSU_CURRENCY") & " " & Format(ds.Tables(0).Rows(0)("FCL_BALANCE") * -1, "0.00")
                End If
            End If
            Dim ds1 As New DataSet
            SqlHelper.FillDataset(str_conn, CommandType.Text, str_paymnts, ds1, Nothing)
            gvFeeDetails.DataSource = ds1.Tables(0)
            gvFeeDetails.DataBind()
            gvFeeDetails.HeaderRow.Cells(1).Text = "Amount (" & ds.Tables(0).Rows(0)("BSU_CURRENCY") & ")"

            lblPaymentDetals.Text = "Received with thanks <b>" & ds.Tables(0).Rows(0)("BSU_CURRENCY") & " " & Format(ds.Tables(0).Rows(0)("FCL_AMOUNT"), "0.00") & _
                    "</b> (" & Mainclass.SpellNumber(ds.Tables(0).Rows(0)("FCL_AMOUNT")) & _
                    ") through " & ds1.Tables(0).Rows(0)("CPM_DESCR") & "'s Payment Gateway (Ref.No. " & ds.Tables(0).Rows(0)("FCO_ID") & " ) over the Internet towards the School Fees. "

            lblProviderMessage.Text = ds1.Tables(0).Rows(0)("CPS_PAYMENT_MESSAGE")
            gvFeeDetails.Rows(gvFeeDetails.Rows.Count - 1).Font.Bold = True
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, "EXEC FEES.Save_EMAIL_ONLINEPAYMENT_LOG '" & p_BSU_ID & "','" & p_USER & "','" & p_Receiptno & "' ")
        End If
    End Sub

    Protected Sub gvFeeDetails_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvFeeDetails.RowDataBound
        For Each tc As TableCell In e.Row.Cells
            tc.Attributes("style") = "border-left: #7f83ee 0pt solid; border-right: #7f83ee 1pt dotted; border-top: #7f83ee 1pt dotted; border-bottom: #7f83ee 1pt dotted;"
        Next
        If e.Row.Cells(0).Text = "Total" Then
            e.Row.Font.Bold = True
        End If
        If e.Row.Cells(0).Text.ToLower.Contains("discount") Then
            lblDiscount.Visible = True
        End If
    End Sub

End Class

