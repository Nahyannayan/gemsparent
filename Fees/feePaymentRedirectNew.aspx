<%@ Page Language="VB" AutoEventWireup="false" CodeFile="feePaymentRedirectNew.aspx.vb" Inherits="feePaymentRedirectNew" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
        <div class="container-fluid mt-5">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <p class="text-center text-capitalize">Please wait while you are being redirected to the payment gateway. It might take a few seconds.</p>
                </div>
                <div class="col-lg-12 text-center">
                    <p class="text-center text-capitalize">Please do not refresh the page or click the �<strong>Back</strong>� or �<strong>Close</strong>� button of your browser.</p>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 text-center">
                    <img src="../img/loading.gif" width="500" alt=""/>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
