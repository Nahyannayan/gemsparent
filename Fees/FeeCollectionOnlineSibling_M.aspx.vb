﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Imports System.Collections.Generic

Partial Class Fees_FeeCollectionOnlineSibling
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim objutil As New clsRewards
    Public Shared dtDiscount As DataTable
    Private Property VSgridSlider() As DataTable
        Get
            Return ViewState("SliderDS")
        End Get
        Set(ByVal value As DataTable)
            ViewState("SliderDS") = value
        End Set
    End Property
    Private Property bPaymentConfirmMsgShown() As Boolean
        Get
            Return ViewState("bPaymentConfirmMsgShown")
        End Get
        Set(ByVal value As Boolean)
            ViewState("bPaymentConfirmMsgShown") = value
            If value Then
                btnSave.Text = "Confirm & Proceed"
            Else
                btnSave.Text = "Proceed"
            End If
        End Set
    End Property
    Private Property bApplyDiscountScheme() As Boolean
        Get
            Return ViewState("bApplyDiscountScheme")
        End Get
        Set(ByVal value As Boolean)
            ViewState("bApplyDiscountScheme") = value
        End Set
    End Property
    Private Property dDueTotal() As Double
        Get
            Return ViewState("dDueTotal")
        End Get
        Set(ByVal value As Double)
            ViewState("dDueTotal") = value
        End Set
    End Property
    Private Property dDiscountTotal() As Double
        Get
            Return ViewState("dDiscountTotal")
        End Get
        Set(ByVal value As Double)
            ViewState("dDiscountTotal") = value
        End Set
    End Property
    Private Property dPayAmountTotal() As Double
        Get
            Return ViewState("dPayAmountTotal")
        End Get
        Set(ByVal value As Double)
            ViewState("dPayAmountTotal") = value
        End Set
    End Property
    Private Property dProcessingChargeTotal() As Double
        Get
            Return ViewState("dProcessingChargeTotal")
        End Get
        Set(ByVal value As Double)
            ViewState("dProcessingChargeTotal") = value
        End Set
    End Property
    Private Property DiscountBreakUpXML() As String
        Get
            Return ViewState("DiscountBreakUpXML")
        End Get
        Set(ByVal value As String)
            ViewState("DiscountBreakUpXML") = value
        End Set
    End Property

    Private Property DiscountXMLDict() As Dictionary(Of String, String)
        Get
            Return ViewState("DiscountXMLDict")
        End Get
        Set(ByVal value As Dictionary(Of String, String))
            ViewState("DiscountXMLDict") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim BSU_ID As String = Session("STU_BSU_ID")
        If Session("username") Is Nothing Then
            Response.Redirect("~\Login.aspx")
        End If
        If Page.IsPostBack = False Then
            bPaymentConfirmMsgShown = False
            BindPaymentProviders(Session("STU_BSU_ID"))

            'Dim k As String
            'k = k.ToString

            'Dim sqlStr As String
            'sqlStr = "SELECT ISNULL(BSU_FEE_OTH_DISC,0) AS BSU_FEE_OTH_DISC,ISNULL(BSU_bAPPLYDISCOUNTSCHEME,0) AS BSU_bAPPLYDISCOUNTSCHEME FROM dbo.BUSINESSUNIT_M WHERE BSU_ID='" & Session("STU_BSU_ID") & "'"
            'Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, _
            ' CommandType.Text, sqlStr)
            Dim dsData As DataSet = FeeCommon.GETDiscountScheme(BSU_ID)
            If Not dsData Is Nothing AndAlso dsData.Tables.Count > 0 AndAlso dsData.Tables(0).Rows.Count > 0 Then
                ViewState("OthDiscEnabled") = True
                bApplyDiscountScheme = Convert.ToBoolean(dsData.Tables(0).Rows(0)("BSU_bAPPLYDISCOUNTSCHEME"))
            Else
                ViewState("OthDiscEnabled") = False
                bApplyDiscountScheme = False
            End If
            'Me.lblError.Text = Request.Browser.Type
            Page.Title = "::GEMS Education |Fee Payment::"
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            'txtAmountAdd.Attributes.Add("Onkeypress", "return Numeric_Only()")
            ViewState("datamode") = "add"
            Dim CurUsr_id As String = Session("STU_ID")
            Dim CurBsUnit As String = Session("STU_BSU_ID")
            Dim USR_NAME As String = Session("STU_NAME")
            InitialiseCompnents()
            SetNarration()
            If USR_NAME = "" Or CurBsUnit = "" Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                'Gridbind_Feedetails()
                ViewState("STU_NO") = Session("STU_NO")
                ViewState("STU_NAME") = Session("STU_NAME")
                ViewState("STU_GRD_ID") = Session("STU_GRD_ID")
                ViewState("stu_section") = Session("stu_section")
                ViewState("BSU_NAME") = Session("BSU_NAME")
                'lblCurrency.Text = IIf(Session("BSU_CURRENCY") Is Nothing, "Fee Details", "Fee Details (Amount In " & Session("BSU_CURRENCY") & ")")
                'tr_DiscountTotal.Visible = bApplyDiscountScheme
                rblPaymentGateway.DataBind()
                lblschool.Text = ViewState("BSU_NAME")
                lbChildName.Text = Session("STU_NAME")
                If rblPaymentGateway.Items.Count > 0 And rblPaymentGateway.SelectedIndex = -1 Then
                    rblPaymentGateway.SelectedIndex = 0
                    rblPaymentGateway_SelectedIndexChanged(sender, e)
                Else
                    Gridbind_Feedetails()
                    SetAdvanceFeeBreakupInControls()
                End If
                'SetDiscountData()
                lMoreInfo.Attributes.Add("onClick", "return ShowSubWindowWithClose('whatsThis.htm?1=2', '', '50%', '50%');")
            End If
            If Convert.ToDouble(Session("SibCount").ToString()) <= 1 Then
                chkLoadSiblings.Checked = True
                chkLoadSiblings.Visible = False
            Else
                chkLoadSiblings.Visible = True
            End If
            If Session("BSU_CURRENCY") = "AED" Then
                lMoreInfo.Visible = True
                lblalert.Text = getErrorMessage("647")
            Else
                lMoreInfo.Visible = False
                lblalert.Visible = False
            End If
        End If
    End Sub
    Private Function CheckIfStudentAllowedForOnlinePayment(ByVal STU_ID As Integer) As Boolean
        Try
            'Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, _
            '  CommandType.Text, "SELECT * FROM VW_StudentAllowedForOnlinePayment WHERE STU_ID=" & STU_ID)
            Dim dsData As DataSet = FeeCommon.CheckStudentAllowedForOnlinePayment(STU_ID)
            If Not dsData Is Nothing AndAlso dsData.Tables.Count > 0 AndAlso dsData.Tables(0).Rows.Count > 0 Then
                CheckIfStudentAllowedForOnlinePayment = True
            Else
                CheckIfStudentAllowedForOnlinePayment = False
            End If
        Catch ex As Exception
            CheckIfStudentAllowedForOnlinePayment = False
        End Try
    End Function
    Public Sub BindPaymentProviders(ByVal BSU_ID As String)
        HFCardCharge.Value = ""
        Dim pParms(1) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(0).Value = BSU_ID
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISFINConnectionString,
          CommandType.StoredProcedure, "GetMerchantDetails_BSU_ID", pParms)
        If Not dsData Is Nothing AndAlso dsData.Tables.Count > 0 Then
            For Each row As DataRow In dsData.Tables(0).Rows
                HFCardCharge.Value = HFCardCharge.Value & IIf(HFCardCharge.Value = "", "", "|") & row("CPS_ID") & "=" & row("CRR_CLIENT_RATE")
                'Dim lst As New ListItem(String.Format("<img src='{0}' alt='{1}' align='absmiddle' onclick='javascript:return this.parentNode.previousSibling.checked=true;' class='feeImg' />", row("CPM_IMAGEPATH").ToString(), row("CPM_DESCR").ToString()), row("CPS_ID").ToString())
                Dim lst As New ListItem(String.Format("<img src='{0}' alt='{1}'  title='{2}' align='absmiddle' onclick='javascript:return this.parentNode.previousSibling.checked=true;' class='feeImg' />", row("CPM_IMAGEPATH").ToString(), row("CPM_DESCR").ToString(), row("CPM_REMARKS").ToString()), row("CPS_ID").ToString())
                rblPaymentGateway.Items.Add(lst)
            Next
        End If
    End Sub
    Protected Sub repInfo_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles repInfo.ItemDataBound
        Dim hfSTU_ID As New HiddenField
        Dim hfSTU_ACD_ID As New HiddenField
        Dim gvFee As New GridView
        Dim lnkViewFeeSetup As New LinkButton
        Dim lblGvrError As New Label
        Dim lblNacdFeetext As New Label
        Dim nxtAcaFee As New CheckBox

        If e.Item.DataItem Is Nothing Then
            Return
        Else
            gvFee = DirectCast(e.Item.FindControl("gvFee"), GridView)
            hfSTU_ID = DirectCast(e.Item.FindControl("hfSTU_ID"), HiddenField)
            hfSTU_ACD_ID = DirectCast(e.Item.FindControl("hfSTU_ACD_ID"), HiddenField)
            lnkViewFeeSetup = DirectCast(e.Item.FindControl("lnkViewFeeSetup"), LinkButton)
            lblGvrError = DirectCast(e.Item.FindControl("lblGvrError"), Label)
            lblNacdFeetext = DirectCast(e.Item.FindControl("lblNacdFeetext"), Label)
            nxtAcaFee = DirectCast(e.Item.FindControl("nxtAcaFee"), CheckBox)

            If Not Session("BSU_bFPSEnabled") Is Nothing AndAlso Convert.ToBoolean(Session("BSU_bFPSEnabled")) = True Then
                Dim StoreProcedureQry As String = "dbo.GET_NEXTACADEMICPAYMENT_LIST @BSU_ID = '" & Session("sBsuid") & "',@STU_ID = '" & hfSTU_ID.Value & "',@STU_TYPE = 'S',@STU_ACD_ID = '" & hfSTU_ACD_ID.Value & "'"
                Dim connection As SqlConnection = ConnectionManger.GetOASISConnection()
                Dim command As SqlCommand = New SqlCommand(StoreProcedureQry, connection)
                command.CommandType = CommandType.Text

                Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
                While reader.Read
                    If reader("SHOWCHECK") = 1 Then
                        nxtAcaFee.Visible = True
                        lblNacdFeetext.Text = reader("DISPLAYMESSAGE")
                        lblNacdFeetext.Visible = False
                    Else
                        nxtAcaFee.Visible = False
                        lblNacdFeetext.Text = reader("DISPLAYMESSAGE")
                        lblNacdFeetext.Visible = True
                    End If
                End While
                reader.Close()
            End If

            If IsDate(lblDate.Text) Then
                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim dt As New DataTable
                dt = FeeCommon.F_GetFeeDetailsFOrCollection_online(lblDate.Text, hfSTU_ID.Value, "S", Session("STU_BSU_ID"), nxtAcaFee.Checked)
                If dt.Rows.Count > 0 Then
                    lnkViewFeeSetup.Attributes.Add("onClick", "return ShowSubWindowWithClose('PopupShowDataFeeSetup.aspx?ID=FEESETUPMULTI&STU_ID=" & hfSTU_ID.Value & "&ACD_ID=" & hfSTU_ACD_ID.Value & "', '', '50%', '50%');")
                    gvFee.DataSource = dt
                    gvFee.DataBind()
                    If Not CheckIfStudentAllowedForOnlinePayment(hfSTU_ID.Value) Then
                        lblGvrError.Text = getErrorMessage("4042")
                        lblGvrError.CssClass = "alert alert-danger font-small-mob"
                        gvFee.Enabled = False
                    Else
                        lblGvrError.Text = ""
                        lblGvrError.CssClass = ""
                        btnSave.Enabled = True
                        gvFee.Enabled = True
                    End If
                    gvFee.DataSource = dt
                    gvFee.DataBind()
                    Set_GridTotal(False)
                Else
                    gvFee.DataBind()
                    Set_GridTotal(False)
                End If
            End If
        End If
    End Sub
    Sub SetDiscountData(ByVal STUID As Int64, ByRef gvDsct As GridView)
        Dim BSU_ID As String = Session("STU_BSU_ID")
        If bApplyDiscountScheme Then
            Dim dtDiscount As DataTable
            dtDiscount = FeeCommon.DIS_SelectDiscount(lblDate.Text, STUID, "S", BSU_ID, "", False)
            gvDsct.DataSource = dtDiscount
            gvDsct.DataBind()
        End If
    End Sub
    Sub InitialiseCompnents()
        lblDate.Text = Format(Date.Now, "dd/MMM/yyyy")
        txtTotal.Attributes.Add("readonly", "readonly")
        txtCardCharge.Attributes.Add("readonly", "readonly")
        txtGridTotal.Attributes.Add("readonly", "readonly")
        dDiscountTotal = 0
        dDueTotal = 0
        dPayAmountTotal = 0
        dProcessingChargeTotal = 0
    End Sub
    Sub clear_All()
        'gvFeeCollection.DataBind()
        HFCardCharge.Value = ""
        tr_comment.Visible = False
        'lnkMoreFee.Visible = False
        txtTotal.Text = ""
        lblTotal.Text = ""
        txtCardCharge.Text = "0"
        lblCardCharge.Text = "0"
        txtGridTotal.Text = "0"
        lblGridTotal.Text = "0"
        txtAmountAdd.Text = 0
        dDiscountTotal = 0
        dProcessingChargeTotal = 0
        dDueTotal = 0
        dPayAmountTotal = 0
        DiscountBreakUpXML = ""
        SetNarration()
        ClearStudentData()
    End Sub
    Private Function TrimStudentNo(ByVal STUNO As String) As String
        TrimStudentNo = ""
        If STUNO.Length >= 14 Then
            TrimStudentNo = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT CAST(REPLACE('" & STUNO & "',SUBSTRING('" & STUNO & "',1,6),'')AS INTEGER)AS STUNO")
        Else
            TrimStudentNo = STUNO
        End If
    End Function
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim BSU_ID As String = Session("STU_BSU_ID")
        If bPaymentConfirmMsgShown = True Then
            btnSave.Enabled = False
            'Response.Redirect("feePaymentRedirectNew.aspx")
            PaymentRedirect()
            Exit Sub
        End If
        Set_GridTotal(True)
        Dim vpc_OrderInfo As String = String.Empty
        'Dim vpc_ReturnURL As String = String.Empty
        Dim boolPaymentInitiated As Boolean = False
        Dim str_error As String = ""
        If rblPaymentGateway.SelectedIndex = -1 Then


            If str_error <> "" Then
                str_error = str_error & "<br /><br />"
                str_error = str_error & "Please select a payment gateway"
            Else
                str_error = str_error & "Please select a payment gateway"
            End If
        End If
        If Not IsDate(lblDate.Text) Then

            If str_error <> "" Then
                str_error = str_error & "<br /><br />"
                str_error = str_error & "Invalid date"
            Else
                str_error = str_error & "Invalid date"
            End If
        End If
        If Not IsNumeric(Session("STU_ID")) Then

            If str_error <> "" Then
                str_error = str_error & "<br /><br />"
                str_error = str_error & "Please select student"
            Else
                str_error = str_error & "Please select student"
            End If
        End If
        Dim dblTotal As Decimal
        dblTotal = CDbl(txtTotal.Text)
        If dblTotal <= 0 Then

            If str_error <> "" Then
                str_error = str_error & "<br /><br />"
                str_error = str_error & "Please enter a valid amount"
            Else
                str_error = str_error & "Please enter a valid amount"
            End If
        End If
        If str_error <> "" Then
            lblError.Text = str_error
            lblError.CssClass = "alert alert-warning"
            Exit Sub
        Else
            lblError.CssClass = ""
        End If
        Dim dtFrom As DateTime
        dtFrom = CDate(lblDate.Text)
        Dim str_new_FCL_ID As Long
        Dim str_NEW_FCL_RECNO As String = ""
        Dim StuNos As String = ""
        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim objConn As New SqlConnection(str_conn) '
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try
            Dim retval As String = "0"
            Dim STR_TYPE As Char = "S"
            Dim FCO_FCO_ID As Int64 = 0
            Dim LoopTotal As Double = 0
            For Each repitm As RepeaterItem In repInfo.Items
                Dim gvfee As New GridView
                gvfee = DirectCast(repitm.FindControl("gvFee"), GridView)
                Dim hfSTU_ID As HiddenField = CType(repitm.FindControl("hfSTU_ID"), HiddenField)
                Dim hfSTU_FEE_ID As HiddenField = CType(repitm.FindControl("hfSTU_FEE_ID"), HiddenField)
                Dim hfSTU_ACD_ID As HiddenField = CType(repitm.FindControl("hfSTU_ACD_ID"), HiddenField)

                Dim lblStuNo As Label = CType(repitm.FindControl("lbSNo"), Label)
                Dim nxtAcaFee As CheckBox = CType(repitm.FindControl("nxtAcaFee"), CheckBox)

                'Dim lblAmounttoPayF As Label = CType(gvfee.FooterRow.FindControl("lblAmounttoPayF"), Label) 'gives total amount paying now of current student
                'Dim h_ProcessingChargeF As HiddenField = CType(gvfee.FooterRow.FindControl("h_ProcessingChargeF"), HiddenField) 'gives total processing charge of current student
                Dim lblAmounttoPayF As New Label, h_ProcessingChargeF As New HiddenField
                If gvfee.Rows.Count > 0 Then
                    lblAmounttoPayF = CType(gvfee.FooterRow.FindControl("lblAmounttoPayF"), Label) 'gives total amount paying now of current student
                    h_ProcessingChargeF = CType(gvfee.FooterRow.FindControl("h_ProcessingChargeF"), HiddenField) 'gives total processing charge of current student
                Else
                    lblAmounttoPayF.Text = "0.00"
                    h_ProcessingChargeF.Value = 0
                End If
                str_new_FCL_ID = 0
                str_NEW_FCL_RECNO = ""
                If Convert.ToDouble(lblAmounttoPayF.Text) > 0 And retval = "0" Then
                    StuNos = StuNos & IIf(StuNos = "", TrimStudentNo(hfSTU_FEE_ID.Value), "," & TrimStudentNo(hfSTU_FEE_ID.Value))
                    DiscountBreakUpXML = IIf(DiscountBreakUpXML Is Nothing, "", DiscountBreakUpXML)
                    Dim DiscountXML As String = String.Empty
                    If Not DiscountXMLDict Is Nothing AndAlso DiscountXMLDict.ContainsKey(hfSTU_ID.Value) Then
                        DiscountXML = DiscountXMLDict(hfSTU_ID.Value)
                    End If
                    retval = FeeCollectionOnline.F_SaveFEECOLLECTION_H_ONLINE_MULTI(0, "Online", lblDate.Text,
                    hfSTU_ACD_ID.Value, hfSTU_ID.Value, "S", lblAmounttoPayF.Text, False, str_new_FCL_ID,
                    BSU_ID, "Online Fee Collection", "CR", str_NEW_FCL_RECNO,
                    Request.UserHostAddress.ToString, rblPaymentGateway.SelectedItem.Value, FCO_FCO_ID, stTrans, DiscountXML, nxtAcaFee.Checked)
                    If retval = "0" Then
                        For Each gvr As GridViewRow In gvfee.Rows
                            Dim txtAmountToPay As TextBox = CType(gvr.FindControl("txtAmountToPay"), TextBox)
                            Dim lblFSR_FEE_ID As Label = CType(gvr.FindControl("lblFSR_FEE_ID"), Label)
                            Dim lblDiscount As Label = CType(gvr.FindControl("lblDiscount"), Label)
                            Dim lblAmount As Label = CType(gvr.FindControl("lblAmount"), Label)
                            If Not txtAmountToPay Is Nothing Then
                                If CDbl(txtAmountToPay.Text > 0) Then
                                    retval = FeeCollectionOnline.F_SaveFEECOLLSUB_ONLINE(0, str_new_FCL_ID, lblFSR_FEE_ID.Text,
                                            CDbl(txtAmountToPay.Text) + CDbl(lblDiscount.Text), -1, lblAmount.Text, lblDiscount.Text, stTrans)
                                    If retval <> 0 Then
                                        Exit For
                                    End If
                                End If
                            End If
                        Next
                        '--------------------Added by Jacob on 23/oct/2013--------------------
                        If retval = "0" And CDbl(h_ProcessingChargeF.Value) > 0 Then ' credit card processing charge
                            retval = FeeCollectionOnline.F_SaveFEECOLLSUB_ONLINE(0, str_new_FCL_ID, 162,
                                        CDbl(h_ProcessingChargeF.Value), -1, CDbl(h_ProcessingChargeF.Value), 0, stTrans)
                        End If
                        If Convert.ToDouble(lblAmounttoPayF.Text) > 0 And retval = "0" Then 'cash  here
                            retval = FeeCollectionOnline.F_SaveFEECOLLSUB_D_ONLINE(0, str_new_FCL_ID, COLLECTIONTYPE.CASH,
                            lblAmounttoPayF.Text, "", lblDate.Text, 0, "", "", "", stTrans, CDbl(h_ProcessingChargeF.Value))
                        End If
                        If retval = "0" Then
                            retval = FEECOLLECTION_H_ONLINE_Validation(str_new_FCL_ID, stTrans)
                        End If
                    End If
                End If
            Next
            If retval = "0" Then
                stTrans.Commit()
                'btnSave.Enabled = False
                Session("vpc_Amount") = CStr(dblTotal * 100).Split(".")(0)
                '  vpc_OrderInfo = "Payment for " & StuNos
                vpc_OrderInfo = "" & FCO_FCO_ID.ToString & "," & "for " & StuNos
                If vpc_OrderInfo.Length > 34 Then
                    vpc_OrderInfo = Left(vpc_OrderInfo, 34)
                End If
                ' vpc_ReturnURL = Request.Url.ToString.Replace("FeeCollectionOnlineNew.aspx", "feePaymentResultPage.aspx")
                Session("vpc_ReturnURL") = Request.Url.ToString.Replace("FeeCollectionOnlineSibling_M.aspx", "feePaymentResultSibling.aspx")
                'Session("vpc_ReturnURL") = UtilityObj.StringReplace(Session("vpc_ReturnURL"), "http://", "https://")
                Session("vpc_MerchTxnRef") = FCO_FCO_ID
                Session("CPS_ID") = rblPaymentGateway.SelectedItem.Value
                'Session("FeeCollection").rows.clear()
                ' gvFeeCollection.DataSource = Session("FeeCollection")
                Session("vpc_OrderInfo") = vpc_OrderInfo
                ' gvFeeCollection.DataBind()
                Dim flagAudit As Integer = UtilityObj.operOnAudiTable("Fee Online Payment Sibling", FCO_FCO_ID, "Insert", Page.User.Identity.Name.ToString, Me.Page, Server.MachineName)
                'lblError.Text = "<br />" & "Please note reference no. <B>" & str_new_FCL_ID & "</B><br />" & "Now press Proceed to continue.you are about to pay a amount of<B>" & Session("BSU_CURRENCY") & "." & dblTotal & "</B> "
                'lblError.CssClass = "divsuccess"
                Dim message1 = "Click on Confirm & Proceed to continue with this payment for " & IIf(Session("BSU_CURRENCY") Is Nothing, "", Session("BSU_CURRENCY")) & " " & Format(txtTotal.Text, Session("BSU_DataFormatString")) & ""
                Dim message2 = "Please note reference no. <STRONG>" & FCO_FCO_ID & "</STRONG> of this transaction for any future communication."
                Dim message3 = "Please do not close this browser or Log off until you get the final receipt."
                If Request.Browser.Type.ToUpper.Contains("IE") Then
                    Me.divboxpanelconfirm.Attributes("class") = "darkPanelIETop"
                Else
                    Me.divboxpanelconfirm.Attributes("class") = "darkPanelMTop"
                End If
                Me.btnSave.Visible = False
                Me.testpopup.Style.Item("display") = "block"
                lblMsg.Text = message1 & "<br />" & message2
                lblMsg2.Text = message3
                'lblPayMsg.Text = "Please do not close this window or Log off until you get the final receipt."
                'Me.tr_comment.Visible = True
                Panel1.Visible = False
                bPaymentConfirmMsgShown = True
                boolPaymentInitiated = True
            Else
                stTrans.Rollback()
                lblError.Text = getErrorMessage(retval)
                lblError.CssClass = "alert alert-warning"
            End If
        Catch ex As Exception
            stTrans.Rollback()
            lblError.Text = getErrorMessage(1000)
            lblError.CssClass = "alert alert-warning"
            Errorlog(ex.Message)
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub
    Public Shared Function FEECOLLECTION_H_ONLINE_Validation(ByVal FCO_ID As Integer, ByVal p_stTrans As SqlTransaction) As String
        Dim pParms(2) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@FCO_ID", SqlDbType.Int)
        pParms(0).Value = FCO_ID
        pParms(1) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(1).Direction = ParameterDirection.ReturnValue
        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "FEES.FEECOLLECTION_H_ONLINE_Validation", pParms)
        FEECOLLECTION_H_ONLINE_Validation = pParms(1).Value
    End Function
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        bPaymentConfirmMsgShown = False
        Response.Redirect("~\Home.aspx")
    End Sub
    Sub Gridbind_Feedetails()
        Try
            'Dim strPath As String = Convert.ToString(readerStudent_Detail("STU_PHOTOPATH"))
            Dim connPath As String = WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ConnectionString
            Dim noImagePath As String = "~/Images/Home/no_image.gif"
            Dim con As String = ConnectionManger.GetOASISConnectionString
            Dim param(4) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@OLU_ID", Session("OLU_ID").ToString)
            param(1) = New SqlClient.SqlParameter("@connPath", connPath)
            param(2) = New SqlClient.SqlParameter("@noImagePath", noImagePath)
            param(3) = New SqlClient.SqlParameter("@STU_BSU_ID", Session("STU_BSU_ID")) '@
            param(4) = New SqlClient.SqlParameter("@SELECTED_STU_ID", IIf(chkLoadSiblings.Checked, 0, Session("STU_ID")))
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "[OPL].[GETCHILD_HOME_INFO]", param)
            repInfo.DataSource = ds.Tables(0)
            repInfo.DataBind()
            Set_GridTotal(False)
        Catch ex As Exception
            repInfo.DataBind()
            Set_GridTotal(False)
        End Try
    End Sub
    Private Function checkIfCobrand(ByVal GatewayID As Int16) As Boolean
        checkIfCobrand = False
        Dim pParms(1) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(0).Value = Session("STU_BSU_ID")
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISFINConnectionString,
          CommandType.StoredProcedure, "GetMerchantDetails_BSU_ID", pParms)
        If Not dsData Is Nothing AndAlso dsData.Tables.Count > 0 Then
            For Each row As DataRow In dsData.Tables(0).Rows
                If row("CPS_ID") = GatewayID And row("CPS_CPM_ID") = 3 Then
                    checkIfCobrand = True
                    Exit Function
                End If
            Next
            checkIfCobrand = False
            Exit Function
        End If
    End Function
    Sub Set_GridTotal(ByVal DonotCalulateDisc As Boolean)
        Dim dAmount As Decimal = 0
        Dim dDiscount As Decimal = 0
        '--------checking for advance enrolment or admission paid
        Dim EnrollFee, AdmissionFee, OtherCharge As Double
        Dim IsCobrandCard As Boolean = False
        EnrollFee = 0 : AdmissionFee = 0 : OtherCharge = 0
        For Each rptr As RepeaterItem In repInfo.Items
            Dim gvfee As New GridView
            gvfee = DirectCast(rptr.FindControl("gvFee"), GridView)
            Dim hfSTU_ID As HiddenField = CType(rptr.FindControl("hfSTU_ID"), HiddenField)
            Dim lblGvrError As New Label
            lblGvrError = DirectCast(rptr.FindControl("lblGvrError"), Label)
            Dim trgvrerror As New HtmlTableRow
            trgvrerror = DirectCast(rptr.FindControl("trgvrerror"), HtmlTableRow)
            If rblPaymentGateway.SelectedIndex <> -1 Then
                IsCobrandCard = checkIfCobrand(rblPaymentGateway.SelectedItem.Value)
                If IsCobrandCard Then
                    For Each gvro As GridViewRow In gvfee.Rows
                        Dim FSR_FEE_ID As Label = CType(gvro.FindControl("lblFSR_FEE_ID"), Label)
                        Dim tAmountToPay As TextBox = CType(gvro.FindControl("txtAmountToPay"), TextBox)
                        If Not FSR_FEE_ID.Text Is Nothing AndAlso (FSR_FEE_ID.Text = "145") Then
                            AdmissionFee += Val(tAmountToPay.Text)
                        End If
                        If Not FSR_FEE_ID.Text Is Nothing AndAlso (FSR_FEE_ID.Text = "145" Or FSR_FEE_ID.Text = "146") Then
                            EnrollFee += Val(tAmountToPay.Text)
                        End If
                    Next
                End If
            End If
            '----------------------------------------------------------
            dDiscountTotal = 0
            dDueTotal = 0
            dPayAmountTotal = 0
            dProcessingChargeTotal = 0
            dtDiscount = Nothing
            For Each gvr As GridViewRow In gvfee.Rows
                Dim lblDiscount As Label = CType(gvr.FindControl("lblDiscount"), Label)
                Dim spanInfo As HtmlGenericControl = CType(gvr.FindControl("spanInfo"), HtmlGenericControl)
                Dim txtAmountToPay As TextBox = CType(gvr.FindControl("txtAmountToPay"), TextBox)
                Dim lblFSR_FEE_ID As Label = CType(gvr.FindControl("lblFSR_FEE_ID"), Label)
                Dim lbCancel As LinkButton = CType(gvr.FindControl("lbCancel"), LinkButton)
                Dim h_OtherCharge As HiddenField = CType(gvr.FindControl("h_OtherCharge"), HiddenField)
                Dim h_Discount As HiddenField = CType(gvr.FindControl("h_Discount"), HiddenField) '
                Dim h_ProcessingCharge As HiddenField = CType(gvr.FindControl("h_ProcessingCharge"), HiddenField)
                Dim lblAmount As Label = CType(gvr.FindControl("lblAmount"), Label)
                Dim gvDiscDt As Repeater = DirectCast(gvr.FindControl("gvDiscDt"), Repeater)
                txtAmountToPay.Text = Val(txtAmountToPay.Text)
                If Not txtAmountToPay Is Nothing Then
                    If lblGvrError.Text <> "" Then
                        trgvrerror.Visible = True
                        txtAmountToPay.Text = 0
                    Else
                        trgvrerror.Visible = False
                    End If
                    If IsNumeric(txtAmountToPay.Text) = False Then
                        lblGvrError.Text = "Invalid Amount..!"
                        txtAmountToPay.Text = "0"
                        txtTotal.Text = "0"
                        Exit Sub
                    End If
                    If lblFSR_FEE_ID.Text.Trim = "5" And Not lbCancel.Visible Then
                        If DonotCalulateDisc = False Then
                            Dim decDiscountamt As Decimal = 0
                            FeeCollectionOnline.DIS_FindDiscount(hfSTU_ID.Value, "S", lblDate.Text, Session("STU_BSU_ID"), "", txtAmountToPay.Text, decDiscountamt)
                            lblDiscount.Text = Format(decDiscountamt, "0.00")
                        End If
                    End If
                    If rblPaymentGateway.SelectedIndex <> -1 Then
                        If lblFSR_FEE_ID.Text.Trim = "5" And Not lbCancel.Visible Then
                            If DonotCalulateDisc = False Then
                                Dim decDiscountamt As Decimal = 0
                                Dim p_Amount As Double = 0
                                p_Amount = Val(txtAmountToPay.Text) + EnrollFee + Val(h_Discount.Value)
                                If IsCobrandCard Then
                                    GetDiscountedFeesPayable_COBRAND(hfSTU_ID.Value, "S", rblPaymentGateway.SelectedItem.Value,
                                                             COLLECTIONTYPE.CREDIT_CARD, lblFSR_FEE_ID.Text, p_Amount, decDiscountamt, AdmissionFee, 0, 3)
                                Else
                                    GetDiscountedFeesPayable_COBRAND(hfSTU_ID.Value, "S", rblPaymentGateway.SelectedItem.Value,
                                          COLLECTIONTYPE.CREDIT_CARD, lblFSR_FEE_ID.Text, p_Amount, decDiscountamt, AdmissionFee, OtherCharge, 0)
                                End If

                                If lblFSR_FEE_ID.Text = "5" Then '-------discount calculations only for tuition fee
                                    If Not DiscountXMLDict Is Nothing AndAlso DiscountXMLDict.ContainsKey(hfSTU_ID.Value) Then
                                        DiscountXMLDict.Remove(hfSTU_ID.Value)
                                    End If
                                    If DiscountXMLDict Is Nothing Then
                                        DiscountXMLDict = New Dictionary(Of String, String)
                                    End If
                                    DiscountXMLDict.Add(hfSTU_ID.Value, DiscountBreakUpXML)
                                End If

                                If ((Convert.ToDouble(txtAmountToPay.Text) - decDiscountamt) + OtherCharge + Val(h_Discount.Value)) < 0 Then
                                    txtAmountToPay.Text = 0
                                    decDiscountamt = 0
                                Else
                                    txtAmountToPay.Text = Format(Convert.ToDouble(txtAmountToPay.Text) - decDiscountamt, Session("BSU_DataFormatString")) + OtherCharge + Val(h_Discount.Value)
                                End If
                                lblDiscount.Text = Format(decDiscountamt, "0.00")
                                h_OtherCharge.Value = OtherCharge
                                h_Discount.Value = decDiscountamt
                                gvDiscDt.DataSource = dtDiscount
                                gvDiscDt.DataBind()
                                If decDiscountamt <> 0 Then
                                    spanInfo.Visible = True
                                    gvfee.Columns(3).Visible = True
                                Else
                                    spanInfo.Visible = False
                                    gvfee.Columns(3).Visible = False
                                End If
                            End If
                        End If
                    Else
                        DiscountBreakUpXML = ""
                    End If
                    h_ProcessingCharge.Value = Format(Math.Ceiling(GetCardProcessingCharge(Val(Me.rblPaymentGateway.SelectedValue)) * CDbl(txtAmountToPay.Text)), "0.00")
                    dDiscount = dDiscount + CDbl(lblDiscount.Text)
                    dAmount = dAmount + CDbl(txtAmountToPay.Text)
                End If
                If gvr.RowType = DataControlRowType.DataRow Then
                    dDiscountTotal = dDiscountTotal + IIf(Not lblDiscount Is Nothing, Convert.ToDouble(lblDiscount.Text), 0)
                    dDueTotal = dDueTotal + IIf(Not lblAmount Is Nothing, Convert.ToDouble(lblAmount.Text), 0)
                    dPayAmountTotal = dPayAmountTotal + IIf(Not txtAmountToPay Is Nothing, Convert.ToDouble(txtAmountToPay.Text), 0)
                    dProcessingChargeTotal = dProcessingChargeTotal + IIf(Not h_ProcessingCharge Is Nothing, Convert.ToDouble(h_ProcessingCharge.Value), 0)
                End If
            Next
            If gvfee.Rows.Count > 0 Then
                Dim lblAmountF As New Label
                Dim lblDiscountF As New Label
                Dim lblAmounttoPayF As New Label
                Dim h_ProcessingChargeF As New HiddenField
                lblAmountF = CType(gvfee.FooterRow.FindControl("lblAmountF"), Label)
                lblDiscountF = CType(gvfee.FooterRow.FindControl("lblDiscountF"), Label)
                lblAmounttoPayF = CType(gvfee.FooterRow.FindControl("lblAmounttoPayF"), Label)
                h_ProcessingChargeF = CType(gvfee.FooterRow.FindControl("h_ProcessingChargeF"), HiddenField)
                lblAmountF.Text = dDueTotal.ToString("#,##0.00")
                lblDiscountF.Text = dDiscountTotal.ToString("#,##0.00")
                lblAmounttoPayF.Text = dPayAmountTotal.ToString("#,##0.00")
                h_ProcessingChargeF.Value = dProcessingChargeTotal.ToString("#,##0.00")
            End If

            dDiscountTotal = 0
            dDueTotal = 0
            dPayAmountTotal = 0
            dProcessingChargeTotal = 0
        Next
        Dim CPS_ID As Integer = Val(Me.rblPaymentGateway.SelectedValue)
        txtGridTotal.Text = Format(dAmount, "0.00")
        lblGridTotal.Text = Format(dAmount, "0.00")
        txtCardCharge.Text = Format(Math.Ceiling(GetCardProcessingCharge(CPS_ID) * dAmount), "0.00")
        lblCardCharge.Text = Format(Math.Ceiling(GetCardProcessingCharge(CPS_ID) * dAmount), "0.00")
        txtTotal.Text = Format(Val(txtGridTotal.Text) + Val(txtCardCharge.Text), "0.00")
        lblTotal.Text = Format(Val(lblGridTotal.Text) + Val(lblCardCharge.Text), "0.00")
        'txtTotal.Text = Format(dAmount, "0.00")
        If dDiscount <> 0 Then
            tr_DiscountTotal.Visible = True
            txtDiscountTotal.Text = Format(dDiscount, "0.00")
            lblDiscountTotal.Text = Format(dDiscount, "0.00")
        End If
    End Sub
    Public Function GetCardProcessingCharge(ByVal CPS_ID As Integer) As Double
        Dim CardCharge As Double = 0
        If HFCardCharge.Value <> "" And CPS_ID <> 0 Then
            Dim Temp As String()
            Temp = HFCardCharge.Value.Split("|")
            For i As Int16 = 0 To Temp.Length - 1
                If Temp(i).Split("=")(0) = CPS_ID Then
                    CardCharge = Temp(i).Split("=")(1) / 100
                    Exit For
                End If
            Next
        End If
        Return CardCharge
    End Function
    Sub ClearStudentData()
        Gridbind_Feedetails()
    End Sub
    Sub ClearDetail()
        txtAmountAdd.Text = ""
    End Sub
    Protected Sub txtAmountToPay_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim myCell, myRow As Object
            myCell = sender.parent
            If Not myCell Is Nothing Then
                myRow = myCell.parent
                If Not myRow Is Nothing Then
                    Dim h_Discount As HiddenField = CType(myRow.FindControl("h_Discount"), HiddenField)
                    If Not h_Discount Is Nothing Then
                        h_Discount.Value = 0
                    End If
                End If
            End If
            bPaymentConfirmMsgShown = False
            Set_GridTotal(False)
        Catch ex As Exception
        End Try
    End Sub
    Sub SetNarration()
        If IsDate(lblDate.Text) Then
            txtRemarks.Text = FeeCollectionOnline.F_GetFeeNarration(lblDate.Text, Session("STU_ACD_ID"), Session("STU_BSU_ID"))
        End If
        If txtRemarks.Text.Trim = "" Then
            txtRemarks.Text = "Online School Fees Payment"
        End If
    End Sub
    Protected Sub lnkSetArea_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Gridbind_Feedetails()
    End Sub
    Protected Sub lbApply_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        gvDiscount.SelectedIndex = sender.Parent.Parent.RowIndex
        Dim dtDiscountGrid As DataTable
        dtDiscountGrid = FeeCommon.DIS_SelectDiscount(lblDate.Text, Session("STU_ID"), "S", Session("STU_BSU_ID"), "", False)
        gvDiscount.DataSource = dtDiscountGrid
        gvDiscount.DataBind()
        Dim lblFee_ID As Label = sender.Parent.Parent.Findcontrol("lblFee_ID")
        Dim lblFds_ID As Label = sender.Parent.Parent.Findcontrol("lblFds_ID")
        Dim dtDiscountData As DataTable = FeeCollectionOnline.DIS_GetDiscount(lblFds_ID.Text, Session("STU_ID"), "S", lblDate.Text)
        If Not dtDiscountData Is Nothing AndAlso dtDiscountData.Rows.Count > 0 Then
        End If
        Set_GridTotal(True)
    End Sub
    Protected Sub lbCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    End Sub
    Protected Sub rblPaymentGateway_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rblPaymentGateway.SelectedIndexChanged
        Gridbind_Feedetails()
        SetAdvanceFeeBreakupInControls()
        Set_GridTotal(False)
    End Sub
    Public Function GetDiscountedFeesPayable_COBRAND(ByVal p_STU_ID As String, ByVal p_STU_TYPE As String,
              ByVal p_CRR_ID As String, ByVal p_CLT_ID As String, ByVal p_FEE_ID As String,
              ByVal p_AMOUNT As Decimal, ByRef p_Discount As Decimal, Optional ByVal AdmissionFee As Decimal = 0,
              Optional ByRef OtherCharge As Decimal = 0, Optional ByVal CPM_ID As Integer = 0) As String
        Dim pParms(14) As SqlClient.SqlParameter
        Dim dtDiscnt As New DataSet
        pParms(0) = New SqlClient.SqlParameter("@STU_ID", SqlDbType.BigInt) '
        pParms(0).Value = p_STU_ID
        pParms(1) = New SqlClient.SqlParameter("@STU_TYPE", SqlDbType.VarChar, 2)
        pParms(1).Value = p_STU_TYPE
        pParms(2) = New SqlClient.SqlParameter("@CRR_ID", SqlDbType.Int)
        pParms(2).Value = p_CRR_ID
        pParms(3) = New SqlClient.SqlParameter("@CLT_ID", SqlDbType.Int)
        pParms(3).Value = p_CLT_ID
        pParms(4) = New SqlClient.SqlParameter("@FEE_ID", SqlDbType.BigInt)
        pParms(4).Value = p_FEE_ID
        pParms(5) = New SqlClient.SqlParameter("@AMOUNT", SqlDbType.Decimal, 21)
        pParms(5).Value = p_AMOUNT
        pParms(6) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(6).Direction = ParameterDirection.ReturnValue
        pParms(7) = New SqlClient.SqlParameter("@Discount", SqlDbType.Decimal, 21)
        pParms(7).Direction = ParameterDirection.Output
        pParms(8) = New SqlClient.SqlParameter("@NEW_ADMISSION_FEE", SqlDbType.Decimal, 21)
        pParms(8).Value = AdmissionFee
        pParms(9) = New SqlClient.SqlParameter("@SPLITUP_XML", SqlDbType.Xml, 100000)
        pParms(9).Direction = ParameterDirection.Output
        pParms(10) = New SqlClient.SqlParameter("@OTHER_CHARGE", SqlDbType.Decimal, 21)
        pParms(10).Direction = ParameterDirection.Output
        pParms(11) = New SqlClient.SqlParameter("@CPM_ID", SqlDbType.Int)
        pParms(11).Value = CPM_ID
        SqlHelper.ExecuteNonQuery(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.StoredProcedure, "GetDiscountedFeesPayable_COBRAND_NEW", pParms)
        If pParms(6).Value = 0 Then
            If Not pParms(7).Value Is Nothing And pParms(7).Value Is System.DBNull.Value Then
                p_Discount = 0
            Else
                p_Discount = pParms(7).Value
            End If
            If Not pParms(10).Value Is Nothing And pParms(10).Value Is System.DBNull.Value Then
                OtherCharge = 0
            Else
                OtherCharge = pParms(10).Value
            End If
        End If
        If Not pParms(9).Value Is DBNull.Value Then
            Dim readXML As StringReader = New StringReader(HttpUtility.HtmlDecode(pParms(9).Value))
            dtDiscnt.ReadXml(readXML)
            dtDiscount = dtDiscnt.Tables(0).Copy
        End If
        DiscountBreakUpXML = IIf(pParms(9).Value Is DBNull.Value, "", pParms(9).Value)
        GetDiscountedFeesPayable_COBRAND = pParms(6).Value
    End Function
    Protected Sub SetAdvanceFeeBreakupInControls()
        Dim gvFee As New GridView
        Dim hfSTU_ID As New HiddenField
        Dim myRow As GridViewRow
        Dim myItem As RepeaterItem
        Dim lblFSR_FEE_ID As Label
        Dim btnSlider As New LinkButton
        Dim divslider As New HtmlGenericControl
        Dim hid_sli_stu_id As HiddenField
        For Each myItem In repInfo.Items
            gvFee = DirectCast(myItem.FindControl("gvFee"), GridView)
            hfSTU_ID = DirectCast(myItem.FindControl("hfSTU_ID"), HiddenField)
            If Not gvFee Is Nothing Then
                For Each myRow In gvFee.Rows
                    lblFSR_FEE_ID = CType(myRow.FindControl("lblFSR_FEE_ID"), Label)
                    divslider = CType(myRow.FindControl("divslider"), HtmlGenericControl)
                    btnSlider = CType(myRow.FindControl("btnSlider"), LinkButton)
                    hid_sli_stu_id = CType(myRow.FindControl("hid_sli_stu_id"), HiddenField)
                    hid_sli_stu_id.Value = hfSTU_ID.Value
                    If (lblFSR_FEE_ID.Text = "5") Then
                        'Creating the controls                       
                        Dim txtNetAmt As Label = CType(myRow.FindControl("txtNetAmt"), Label)
                        'Calling SP to fetch Details
                        Dim ds As DataSet = gridbind_slider(hfSTU_ID.Value)
                        If Not ds Is Nothing And Not ds.Tables(0) Is Nothing Then
                            Dim dt As DataTable = ds.Tables(0)
                            If dt.Rows.Count > 0 Then
                                dt.Columns.Add("STU_ID", GetType(Int64))
                                For Each drow In dt.Rows
                                    drow("STU_ID") = hfSTU_ID.Value
                                Next
                                If VSgridSlider Is Nothing Then
                                    VSgridSlider = dt
                                Else
                                    VSgridSlider.Merge(dt)
                                End If
                            End If
                        End If
                        'Creating slider and binding with dataset
                        Dim RadSlider_Ticks As Telerik.Web.UI.RadSlider = CType(myRow.FindControl("RadSlider_Ticks"), Telerik.Web.UI.RadSlider)
                        RadSlider_Ticks.DataSource = ds
                        RadSlider_Ticks.DataTextField = "F_MONTH"
                        RadSlider_Ticks.DataValueField = "ID"
                        RadSlider_Ticks.DataBind()
                        'finding max id 
                        Dim max_id As Integer
                        max_id = ds.Tables(0).Rows(ds.Tables(0).Rows.Count - 1)("ID")
                        'index for locating current position
                        Dim index As Integer = 0
                        Try
                            Dim foundRows(), dueRows() As Data.DataRow
                            foundRows = ds.Tables(0).Select("SOURCE = 'CURRENT'")
                            dueRows = ds.Tables(0).Select("SOURCE = 'OUTSTANDING'")
                            If foundRows.Length > 0 Then
                                btnSlider.Visible = True
                                index = Convert.ToInt32(foundRows(0)("ID"))
                                RadSlider_Ticks.SelectedIndex = index - 1
                                RadSlider_Ticks.MinimumValue = index - 1
                                RadSlider_Ticks.SelectionStart = index - 1
                                'txtNetAmt.Text = "0.00"
                            Else
                                divslider.Visible = False
                                btnSlider.Visible = False
                            End If
                            Dim CurrentDue As Double = 0
                            If Not dueRows Is Nothing AndAlso dueRows.Length > 0 Then
                                CurrentDue = objutil.GetDoubleVal(dueRows(0)("FEE_AMOUNT"))
                            End If
                            txtNetAmt.Text = Format(CurrentDue, "#,##0.00")
                        Catch ex As ArgumentOutOfRangeException
                            divslider.Visible = False
                            btnSlider.Visible = False
                        Catch ex2 As Exception
                        End Try
                        ' Populating value on load
                    Else
                        btnSlider.Visible = False
                        divslider.Visible = False
                    End If
                Next
            End If
        Next
    End Sub
    Protected Sub gvFeeCollection_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        Try
            If e.Row.RowType = DataControlRowType.Header Then
                dDiscountTotal = 0
                dDueTotal = 0
                dPayAmountTotal = 0
                dProcessingChargeTotal = 0
            ElseIf e.Row.RowType = DataControlRowType.DataRow Then
                Dim lblAmount As New Label
                Dim lblDiscount As New Label
                lblAmount = CType(e.Row.FindControl("lblAmount"), Label)
                lblDiscount = CType(e.Row.FindControl("lblDiscount"), Label)
                Dim h_BlockPayNow As HiddenField, txtAmountToPay As TextBox
                txtAmountToPay = CType(e.Row.FindControl("txtAmountToPay"), TextBox)
                h_BlockPayNow = CType(e.Row.FindControl("h_BlockPayNow"), HiddenField)
                Dim h_ProcessingCharge As HiddenField = CType(e.Row.FindControl("h_ProcessingCharge"), HiddenField)
                If h_BlockPayNow IsNot Nothing And txtAmountToPay IsNot Nothing Then
                    If h_BlockPayNow.Value = "1" Or h_BlockPayNow.Value.ToLower = "true" Then
                        txtAmountToPay.Enabled = False
                    Else
                        txtAmountToPay.Enabled = True
                    End If
                End If
                dDiscountTotal = dDiscountTotal + IIf(Not lblDiscount Is Nothing, Convert.ToDouble(lblDiscount.Text), 0)
                dDueTotal = dDueTotal + IIf(Not lblAmount Is Nothing, Convert.ToDouble(lblAmount.Text), 0)
                dPayAmountTotal = dPayAmountTotal + IIf(Not txtAmountToPay Is Nothing, Convert.ToDouble(txtAmountToPay.Text), 0)
                If h_ProcessingCharge Is Nothing Or h_ProcessingCharge.Value = "" Then
                    h_ProcessingCharge.Value = 0
                End If
                dProcessingChargeTotal = dProcessingChargeTotal + IIf(Not h_ProcessingCharge Is Nothing, Convert.ToDouble(h_ProcessingCharge.Value), 0)
            ElseIf e.Row.RowType = DataControlRowType.Footer Then
                Dim lblAmountF As New Label
                Dim lblDiscountF As New Label
                Dim lblAmounttoPayF As New Label
                Dim h_ProcessingChargeF As New HiddenField
                lblAmountF = CType(e.Row.FindControl("lblAmountF"), Label)
                lblDiscountF = CType(e.Row.FindControl("lblDiscountF"), Label)
                lblAmounttoPayF = CType(e.Row.FindControl("lblAmounttoPayF"), Label)
                h_ProcessingChargeF = CType(e.Row.FindControl("h_ProcessingChargeF"), HiddenField)
                lblAmountF.Text = dDueTotal
                lblDiscountF.Text = dDiscountTotal
                lblAmounttoPayF.Text = dPayAmountTotal
                h_ProcessingChargeF.Value = dProcessingChargeTotal
                dDiscountTotal = 0
                dDueTotal = 0
                dPayAmountTotal = 0
                dProcessingChargeTotal = 0
            End If
        Catch ex As Exception
        End Try
    End Sub
    Protected Sub btnShow_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnShow.Click
        'Me.testpopup.Visible = True
        Me.testpopup.Style.Item("display") = "block"
        Me.lblMsg.Text = "Click on Confirm & Proceed to continue with this payment AED : 0.00" & "<br />" & "Please note reference no. 12345 of this transaction for any future communication."
    End Sub
    Protected Sub btnSkip_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSkip.Click
        Me.testpopup.Style.Item("display") = "none"
        Me.btnSave.Visible = True
        bPaymentConfirmMsgShown = False
    End Sub
    Protected Sub btnProceed_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnProceed.Click
        'Response.Redirect("feePaymentRedirectNew.aspx")
        PaymentRedirect()
    End Sub
    Protected Sub RadSlider_Ticks_ValueChanged(sender As Object, e As EventArgs)
        Try
            Dim dt As DataTable = VSgridSlider
            Dim foundRows(), duerows() As Data.DataRow
            If Not dt Is Nothing Then
                Dim Startindex As Integer
                Dim obj As Object = sender.parent
                Dim RadSlider_Ticks As Telerik.Web.UI.RadSlider = CType(obj.FindControl("RadSlider_Ticks"), Telerik.Web.UI.RadSlider)
                Dim hid_sli_stu_id As HiddenField = CType(obj.FindControl("hid_sli_stu_id"), HiddenField)
                Dim dr As DataRow() = dt.Select("STU_ID =" & hid_sli_stu_id.Value)
                foundRows = dt.Select("SOURCE = 'CURRENT' AND STU_ID =" & hid_sli_stu_id.Value)
                duerows = dt.Select("SOURCE = 'OUTSTANDING' AND STU_ID =" & hid_sli_stu_id.Value)
                Dim txtAmountToPay As TextBox = CType(obj.FindControl("txtAmountToPay"), TextBox)
                Dim txtNetAmt As Label = CType(obj.FindControl("txtNetAmt"), Label)
                Dim lblmessage As Label = CType(obj.FindControl("lblmessage"), Label)
                Dim str As String = txtAmountToPay.Text
                Dim feeamount_total As Double = 0, current_due As Double = 0
                Dim index As Integer = RadSlider_Ticks.Value
                If Not duerows Is Nothing AndAlso duerows.Length > 0 AndAlso Not duerows(0)("FEE_AMOUNT") Is Nothing Then
                    feeamount_total = objutil.GetDoubleVal(duerows(0)("FEE_AMOUNT"))
                End If
                If Not foundRows(0)("ID") Is Nothing Then
                    Startindex = Convert.ToInt32(foundRows(0)("ID"))
                Else
                    Startindex = 0
                End If
                If (index = 0) Then
                    txtNetAmt.Text = feeamount_total.ToString("#,##0.00")
                    txtAmountToPay.Text = "0.00"
                Else
                    If Startindex > index And 1 = 2 Then
                        lblmessage.Visible = True
                        lblmessage.Text = "You Have Already Paid For This Month"
                        txtNetAmt.Text = "0.00"
                        txtAmountToPay.Text = "0.00"
                    Else
                        For Value As Integer = Startindex To index
                            feeamount_total = feeamount_total + dr(Value)("FEE_AMOUNT")
                        Next
                        lblmessage.Visible = False
                        txtAmountToPay.Text = feeamount_total.ToString()
                        txtNetAmt.Text = feeamount_total.ToString("#,##0.00") + "  "
                    End If
                End If
                txtAmountToPay_TextChanged(sender, e)
            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Function gridbind_slider(ByVal hfSTU_ID As Integer) As DataSet
        Dim Iscobrandcard As Boolean = checkIfCobrand(rblPaymentGateway.SelectedItem.Value)
        Dim pParms(1) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@STU_iD", SqlDbType.VarChar, 50)
        pParms(0).Value = hfSTU_ID
        If Iscobrandcard Then
            pParms(1) = New SqlClient.SqlParameter("@CPM_ID", SqlDbType.VarChar, 50)
            pParms(1).Value = 3
        Else
            pParms(1) = New SqlClient.SqlParameter("@CPM_ID", SqlDbType.VarChar, 50)
            pParms(1).Value = 2
        End If
        Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.StoredProcedure, "FEES.GET_ADVANCE_PAYMENT_DETAILS", pParms)
        Return ds
    End Function

    Protected Sub chkLoadSiblings_CheckedChanged(sender As Object, e As EventArgs) Handles chkLoadSiblings.CheckedChanged
        Gridbind_Feedetails()
        SetAdvanceFeeBreakupInControls()
    End Sub
    Sub PaymentRedirect()
        '-------------------code added by Jacob on 29/jul/2019 for MPGS integration
        Dim GATEWAY_TYPE As String = "", GATEWAY_URL As String = ""
        PaymentGatewayClass.GetGatewayParameters(Session("sBsuId"), Session("CPS_ID"), GATEWAY_TYPE, GATEWAY_URL)
        Session("CPM_GATEWAY_TYPE") = GATEWAY_TYPE
        Select Case GATEWAY_TYPE
            Case "MPGS", "DIRECPAY", "EBPG", "FLYWIRE", "QLESS"
                Dim APP_RESULT_PAGE As String = Session("vpc_ReturnURL").ToString().ToLower().Replace("feepaymentresultsibling.aspx", "PaymentResultPage.aspx").Split("?")(0).Replace("http://", IIf(Request.Url.Host.Contains("localhost"), "http://", "https://"))
                Dim redirectUrl As String = GATEWAY_URL & "?SRC=" & Encr_decrData.Encrypt("SCHOOL") & "&ID=" & Encr_decrData.Encrypt(Session("vpc_MerchTxnRef")) & "&APP=" & Encr_decrData.Encrypt("GEMSPARENT") & "&ARP=" & Encr_decrData.Encrypt(APP_RESULT_PAGE) & "&BSU_ID=" & Encr_decrData.Encrypt(Session("sBsuId"))
                Response.Redirect(redirectUrl, True)
            Case Else
                Response.Redirect("feePaymentRedirectNew.aspx")
        End Select
    End Sub
    Protected Sub Load_Next_Acd_Fee_Details()
        'Gridbind_Feedetails()
        Dim gvFee As New GridView
        Dim hfSTU_ID As New HiddenField
        Dim nxtAcaFee As New CheckBox

        Dim myItem As RepeaterItem

        Dim btnSlider As New LinkButton
        Dim divslider As New HtmlGenericControl

        Dim lnkViewFeeSetup As New LinkButton
        Dim lblGvrError As New Label

        Dim hfSTU_ACD_ID As New HiddenField
        For Each myItem In repInfo.Items
            gvFee = DirectCast(myItem.FindControl("gvFee"), GridView)
            hfSTU_ID = DirectCast(myItem.FindControl("hfSTU_ID"), HiddenField)
            hfSTU_ACD_ID = DirectCast(myItem.FindControl("hfSTU_ACD_ID"), HiddenField)
            'lnkViewFeeSetup = DirectCast(myItem.FindControl("lnkViewFeeSetup"), LinkButton)
            lblGvrError = DirectCast(myItem.FindControl("lblGvrError"), Label)
            nxtAcaFee = DirectCast(myItem.FindControl("nxtAcaFee"), CheckBox)
            If IsDate(lblDate.Text) Then
                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim dt As New DataTable
                If nxtAcaFee.Checked = True Then
                    dt = FeeCommon.F_GetFeeDetailsFOrCollection_online(lblDate.Text, hfSTU_ID.Value, "S", Session("STU_BSU_ID"), 1)
                Else
                    dt = FeeCommon.F_GetFeeDetailsFOrCollection_online(lblDate.Text, hfSTU_ID.Value, "S", Session("STU_BSU_ID"), 0)
                End If

                If dt.Rows.Count > 0 Then
                    '                Session("FeeCollection") = dt
                    'lnkViewFeeSetup.Attributes.Add("onClick", "return ShowSubWindowWithClose('PopupShowDataFeeSetup.aspx?ID=FEESETUPMULTI&STU_ID=" & hfSTU_ID.Value & "&ACD_ID=" & hfSTU_ACD_ID.Value & "', '', '50%', '50%');")
                    gvFee.DataSource = dt
                    gvFee.DataBind()
                    If Not CheckIfStudentAllowedForOnlinePayment(hfSTU_ID.Value) Then
                        lblGvrError.Text = "Please contact the school – Fee payments will be accepted at the school Fee counter only."
                        gvFee.Enabled = False
                    Else
                        lblGvrError.Text = ""
                        btnSave.Enabled = True
                        gvFee.Enabled = True
                    End If
                    gvFee.DataSource = dt
                    gvFee.DataBind()
                    Set_GridTotal(False)
                Else
                    gvFee.DataBind()
                    Set_GridTotal(False)
                End If
                SetAdvanceFeeBreakupInControls()
            End If


        Next

    End Sub
End Class