﻿Imports Microsoft.ApplicationBlocks.Data
Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Text
Imports Telerik.Web.UI
Imports iTextSharp.text
Imports iTextSharp.text.pdf
Imports iTextSharp.text.html.simpleparser
Imports System.Net
Partial Class Fees_feeStudentLedger
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("username") Is Nothing Then
            Response.Redirect("~\ParentLogin\Login.aspx")
        End If
        Dim smScriptManager As New ScriptManager
        smScriptManager = Master.FindControl("ScriptManager1")
        smScriptManager.RegisterPostBackControl(btnPDF)
        Me.lblMessage.CssClass = ""
        Me.lblMessage.Text = ""
        If Page.IsPostBack = False Then
            txtFrom.Text = Format(Date.Now.AddMonths(-1), "dd/MMM/yyyy")
            txtTo.Text = Format(Date.Now, "dd/MMM/yyyy")
            GetSiblings()
            Dim sibling_value As String = radCmbSiblings.SelectedItem.Value
            getLedgerTransport(sibling_value) 'radCmbSiblings.SelectedItem.Value
            SetHeader()
            h_print.Value = ""
        End If
    End Sub

    Protected Sub btnsearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsearch.Click
        Dim sibling_value As String = radCmbSiblings.SelectedItem.Value
        getLedgerTransport(sibling_value) 'radCmbSiblings.SelectedItem.Value
        h_print.Value = ""
    End Sub

    Sub getLedgerTransport(ByVal STUID As Int64)
        Dim Debit As Double = 0, Credit As Double = 0
        Dim con As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim param(7) As SqlClient.SqlParameter
        param(0) = New SqlClient.SqlParameter("@STU_IDS", UtilityObj.GenerateXML(STUID, XMLType.STUDENT))
        param(1) = New SqlClient.SqlParameter("@BSU_ID", Session("sBsuid"))
        'param(2) = New SqlClient.SqlParameter("@STU_BSU_ID", Session("STU_BSU_ID"))
        param(3) = New SqlClient.SqlParameter("@STU_TYPE", "S")
        param(4) = New SqlClient.SqlParameter("@FromDT", CDate(txtFrom.Text))
        param(5) = New SqlClient.SqlParameter("@ToDT", CDate(txtTo.Text))
        param(6) = New SqlClient.SqlParameter("@bSuppressAdvInv", True)
        Dim ds As New DataSet
        If RdDetails.Checked Then
            gvLedger.Visible = True
            gvSummary.Visible = False
            ds = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "FEES.F_rptStudentLedger", param)
            gvLedger.DataSource = ds.Tables(0)

            Dim helper As GridViewHelper
            helper = New GridViewHelper(gvLedger, True)
            helper.RegisterSummary("DEBIT", SummaryOperation.Sum)
            helper.RegisterSummary("CREDIT", SummaryOperation.Sum)
            'helper.RegisterGroup("ACY_DESCR", True, True)
            gvLedger.DataBind()
            If gvLedger.Rows.Count > 0 Then
                Try
                    Debit = Convert.ToDouble(ds.Tables(0).Compute("SUM(DEBIT)", String.Empty))
                    Credit = Convert.ToDouble(ds.Tables(0).Compute("SUM(CREDIT)", String.Empty))
                Catch ex As Exception
                    Debit = 0
                    Credit = 0
                End Try
                lblTotal.Text = Format(Debit - Credit, "0.00")
            End If
        Else
            gvLedger.Visible = False
            gvSummary.Visible = True
            ds = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "[FEES].[F_rptStudentLedgerSummary]", param)
            gvSummary.DataSource = ds.Tables(0)
            Dim helper As GridViewHelper
            helper = New GridViewHelper(gvSummary, True)
            helper.RegisterSummary("DEBIT", SummaryOperation.Sum)
            helper.RegisterSummary("CREDIT", SummaryOperation.Sum)
            gvSummary.DataBind()
            If gvLedger.Rows.Count > 0 Then
                Try
                    Debit = Convert.ToDouble(ds.Tables(0).Compute("SUM(DEBIT)", String.Empty))
                    Credit = Convert.ToDouble(ds.Tables(0).Compute("SUM(CREDIT)", String.Empty))
                Catch ex As Exception
                    Debit = 0
                    Credit = 0
                End Try
                lblTotal.Text = Format(Debit - Credit, "0.00")
            End If
        End If
        If IsNumeric(lblTotal.Text) AndAlso Convert.ToDecimal(lblTotal.Text) <= 0 Then
            lblTotal.Text = Format(Math.Abs(Convert.ToDecimal(lblTotal.Text)), "0.00")
            lblTotal.ForeColor = Drawing.Color.Green
            lblTotal.Text = "Advance : " & lblTotal.Text
        Else
            lblTotal.ForeColor = Drawing.Color.Navy
            lblTotal.Text = "Due : " & lblTotal.Text
        End If
    End Sub

    Protected Sub RdSummary_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RdSummary.CheckedChanged
        getLedgerTransport(radCmbSiblings.SelectedItem.Value)
        h_print.Value = ""
    End Sub

    Protected Sub RdDetails_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RdDetails.CheckedChanged
        getLedgerTransport(radCmbSiblings.SelectedItem.Value)
        h_print.Value = ""
    End Sub

    Sub GetSiblings()
        Try
            Dim connPath As String = WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ConnectionString
            Dim noImagePath As String = "~/Images/Home/no_image.gif"
            Dim con As String = ConnectionManger.GetOASISConnectionString
            Dim param(2) As SqlClient.SqlParameter

            param(0) = New SqlClient.SqlParameter("@OLU_ID", Session("OLU_ID").ToString)
            param(1) = New SqlClient.SqlParameter("@connPath", connPath)
            param(2) = New SqlClient.SqlParameter("@noImagePath", noImagePath)

            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "[OPL].[GETCHILD_HOME_INFO]", param)
            If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                For Each dr As DataRow In ds.Tables(0).Rows
                    Dim rditm As New Telerik.Web.UI.RadComboBoxItem
                    rditm.ImageUrl = dr("PHOTOPATH")
                    rditm.Text = dr("SNAME")
                    rditm.Value = dr("STU_ID")
                    radCmbSiblings.Items.Add(rditm)
                Next
                ddlSiblings.DataTextField = "SNAME"
                ddlSiblings.DataValueField = "STU_ID"
                ddlSiblings.DataSource = ds.Tables(0)
                ddlSiblings.DataBind()
                radCmbSiblings.SelectedItem.Value = Session("STU_ID")
            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub radCmbSiblings_SelectedIndexChanged(o As Object, e As RadComboBoxSelectedIndexChangedEventArgs) Handles radCmbSiblings.SelectedIndexChanged
        getLedgerTransport(radCmbSiblings.SelectedItem.Value)
    End Sub
    Protected Sub ddlSiblings_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlSiblings.SelectedIndexChanged
        Dim STU_ID As Integer = ddlSiblings.SelectedValue
        SetHeader()
        getLedgerTransport(STU_ID)
        h_print.Value = ""
    End Sub
    Private Sub SetHeader()
        Dim BSU_ID As String = Session("sBsuId")
        Dim STU_ID As Integer = ddlSiblings.SelectedValue
        'Dim BSUName As String = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT BSU_NAME FROM dbo.BUSINESSUNIT_M WITH(NOLOCK) WHERE BSU_ID='" & Session("sBsuId") & "'")
        Dim BSUName As String = FeeCommon.GET_BSU_NAME(BSU_ID)
        lblBSU.Text = BSUName
        'lblStNo.Text = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT STU_NO FROM dbo.STUDENT_M WITH(NOLOCK) WHERE STU_ID='" & STU_ID & "'")
        lblStNo.Text = FeeCommon.GET_STUDENT_NO(STU_ID)
        lblName.Text = ddlSiblings.SelectedItem.Text
        lblReportname.Text = "Statement of Accounts"
        lblPeriod.Text = "For the period " & txtFrom.Text & " to " & txtTo.Text
        'imgBSULOGO.ImageUrl = "GetLogo.aspx?BSU_ID=" & Session("sBsuid")
    End Sub
    Protected Sub btnPrint_Click(sender As Object, e As ImageClickEventArgs) Handles btnPrint.Click
        PrintAllPages(sender, e)
    End Sub
    Public Overloads Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)

    End Sub
    Protected Sub PrintAllPages(ByVal sender As Object, ByVal e As EventArgs)
        gvLedger.AllowPaging = False
        gvSummary.AllowPaging = False
        getLedgerTransport(radCmbSiblings.SelectedItem.Value)

        Dim sw As New StringWriter()
        Dim hw As New HtmlTextWriter(sw)

        tblHeader.RenderControl(hw)
        Dim headerHTML As String = sw.ToString().Replace("""", "'") _
             .Replace(System.Environment.NewLine, "")
        If RdDetails.Checked Then
            gvLedger.Style.Add("font-size", "10px")
            gvLedger.RenderControl(hw)
            gvLedger.Style.Remove("font-size")
            gvLedger.AllowPaging = True
            gvLedger.PageSize = 10
        Else
            gvSummary.Style.Add("font-size", "10px")
            gvSummary.RenderControl(hw)
            gvSummary.Style.Remove("font-size")
            gvSummary.AllowPaging = True
            gvSummary.PageSize = 10
        End If

        Dim gridHTML As String = sw.ToString().Replace("""", "'") _
             .Replace(System.Environment.NewLine, "")

        h_print.Value = gridHTML

        getLedgerTransport(radCmbSiblings.SelectedItem.Value)
    End Sub
    Protected Sub gvLedger_PageIndexChanging(sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvLedger.PageIndexChanging
        gvLedger.PageIndex = e.NewPageIndex
        getLedgerTransport(radCmbSiblings.SelectedItem.Value)
    End Sub
    Protected Sub gvSummary_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvSummary.PageIndexChanging
        gvSummary.PageIndex = e.NewPageIndex
        getLedgerTransport(radCmbSiblings.SelectedItem.Value)
    End Sub
    Protected Sub btnPDF_Click(sender As Object, e As ImageClickEventArgs) Handles btnPDF.Click
        DownloadPDF()
    End Sub
    Protected Sub btnEmail_Click(sender As Object, e As ImageClickEventArgs) Handles btnEmail.Click
        ExportToPDF()
    End Sub
    Private Sub ExportToPDF()
        Dim document As New Document(PageSize.A4, 30, 30, 60, 35)
        document.SetPageSize(PageSize.A4.Rotate)
        Dim strData As New StringBuilder(String.Empty)
        Dim tempDir As String = Web.Configuration.WebConfigurationManager.AppSettings.Item("OnlineFeeReceipt")
        Dim tempFileName As String = HttpContext.Current.Session("sBsuId") & "_" & CType(Date.Now.Hour, String) & CType(Date.Now.Minute, String) & CType(Date.Now.Second, String)
        Dim strmyHTML As String = tempDir & tempFileName & ".html"
        Dim strmyPDF As String = tempDir & tempFileName & ".pdf"
        Dim strHTMLpath As String = strmyHTML 'Server.MapPath(strmyHTML)
        Dim strPDFpath As String = strmyPDF 'Server.MapPath(strmyPDF)

        Try
            Dim sw As New StringWriter
            Dim htw As New HtmlTextWriter(sw)
            tblHeader.RenderControl(htw)
            gvLedger.AllowPaging = False
            gvSummary.AllowPaging = False
            getLedgerTransport(ddlSiblings.SelectedValue)
            If RdDetails.Checked Then
                gvLedger.Style.Add("font-size", "9px")
                gvLedger.RenderControl(htw)
                gvLedger.Style.Remove("font-size")
                gvLedger.AllowPaging = True
                gvLedger.PageSize = 10
            Else
                gvSummary.Style.Add("font-size", "9px")
                gvSummary.RenderControl(htw)
                gvSummary.Style.Remove("font-size")
                gvSummary.AllowPaging = True
                gvSummary.PageSize = 10
            End If
            getLedgerTransport(ddlSiblings.SelectedValue)
            Using strWriter As New StreamWriter(strHTMLpath, False, Encoding.UTF8)
                strWriter.Write("" + htw.InnerWriter.ToString() + "")
                strWriter.Close()
                strWriter.Dispose()
            End Using
            Dim styles As New iTextSharp.text.html.simpleparser.StyleSheet()
            'styles.LoadTagStyle("ol", "leading", "16,0")
            PdfWriter.GetInstance(document, New FileStream(strPDFpath, FileMode.Create))
            document.Open()

            Dim htmlarraylist As System.Collections.Generic.List(Of IElement) = HTMLWorker.ParseToList(New StreamReader(strHTMLpath, Encoding.Default), styles)
            For k As Integer = 0 To htmlarraylist.Count - 1
                document.Add(DirectCast(htmlarraylist(k), IElement))
            Next
            document.Close()
            EmailFeeReceipt(strPDFpath)
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Sub

    Public Sub EmailFeeReceipt(ByVal pdfFilePath As String)
        Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, _
                  "EXEC [ONLINE].[GET_EMAILINFO] @STU_ID=" & ddlSiblings.SelectedValue & ",@BSU_ID='" & Session("sBsuId") & "'")
        Dim subject As String = "Statement Of Accounts"
        Dim ContactName, BSU_Name, StudName, EMailID, FromEmailID, Password, EmailHost, EmailPort As String, Emailed As Boolean, EmailStatus As String, LogoPath As String
        ContactName = ds.Tables(0).Rows(0).Item("TO_EMAIL_NAME").ToString
        BSU_Name = ds.Tables(0).Rows(0).Item("BSU_Name").ToString
        StudName = ds.Tables(0).Rows(0).Item("STU_NAME").ToString
        FromEmailID = ds.Tables(0).Rows(0)("SYS_EMAIL_FEEONLINE").ToString
        EMailID = ds.Tables(0).Rows(0).Item("TO_EMAIL_ID").ToString
        Password = ds.Tables(0).Rows(0).Item("SYS_PASSWORD_FEEONLINE").ToString
        EmailHost = ds.Tables(0).Rows(0).Item("SYS_EMAIL_HOST").ToString
        EmailPort = ds.Tables(0).Rows(0).Item("SYS_EMAIL_PORT").ToString
        Emailed = ds.Tables(0).Rows(0).Item("Emailed").ToString
        LogoPath = ds.Tables(0).Rows(0).Item("BSU_LOGO_PATH").ToString

        If Not Emailed Or 1 = 1 Then
            Dim sb As New StringBuilder '
            sb.Append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>")
            sb.Append("<html xmlns='http://www.w3.org/1999/xhtml'>")
            sb.Append("<head id='Head1' runat='server'><title>Untitled Page</title></head><body>")
            sb.Append("<form id='form1' runat='server'><div style='width: 100%;'><center>")
            sb.Append("<table border='0' style='border-top-style: none; font-family: Verdana; border-right-style: none; border-left-style: none; background-color: white; border-bottom-style: none;font-size: 10pt;'>")
            sb.Append("<tr><td align='left'><div ><img src='" & LogoPath & "' alt='GEMS'/></div></td></tr>")
            sb.Append("<tr><td><hr /></td></tr>")
            sb.Append("<tr><td style='font-weight: bold; font-size: 12pt; color: white; background-color: gray'>Dear " & ContactName & ", </td></tr>")
            sb.Append("<tr><td style='color:#00476B; font-style:italic;'><br />GREETINGS from the <strong>" & BSU_Name & "</strong><br /></td></tr>")
            sb.Append("<tr><td  style='color:#80B2CC;'><br />Please find the attached statement for your child<br /></td></tr>")
            sb.Append("<tr><td ><br />Thanks & regards </td></tr>")
            sb.Append("<tr><td >Accounts </td></tr>")
            sb.Append("<tr><td ><strong>" & BSU_Name & "</strong><br /></td></tr>")
            sb.Append("<tr><td></td></tr>")
            sb.Append("<tr></tr><tr></tr>")
            sb.Append("</table></center></div></form></body></htm>")
            EmailStatus = eMailReceipt.SendNewsLetters(FromEmailID, EMailID, subject, sb.ToString, _
                           FromEmailID, Password, EmailHost, EmailPort, pdfFilePath)
            If EmailStatus.ToString.ToUpper.Contains("SUCCESS") Then
                lblMessage.CssClass = "divsuccess"
                lblMessage.Text = "Receipt successfully emailed to " & EMailID
            Else
                lblMessage.CssClass = "diverrorPopUp"
                lblMessage.Text = "Email Sending Failed"
            End If

            Try
                If System.IO.File.Exists(pdfFilePath) Then
                    System.IO.File.Delete(pdfFilePath)
                End If
            Catch ex As Exception

            End Try
        End If
    End Sub

    Private Sub DownloadPDF()
        Dim strData As New StringBuilder(String.Empty)
        Dim tempDir As String = Web.Configuration.WebConfigurationManager.AppSettings.Item("OnlineFeeReceipt")
        Dim tempFileName As String = HttpContext.Current.Session("sBsuId") & "_" & CType(Date.Now.Hour, String) & CType(Date.Now.Minute, String) & CType(Date.Now.Second, String)
        Dim strmyHTML As String = tempDir & tempFileName & ".html"
        Dim strmyPDF As String = tempDir & tempFileName & ".pdf"
        Dim strHTMLpath As String = strmyHTML 'Server.MapPath(strmyHTML)
        Dim strPDFpath As String = strmyPDF 'Server.MapPath(strmyPDF)

        Try
            Dim sw As New StringWriter
            Dim htw As New HtmlTextWriter(sw)
            tblHeader.RenderControl(htw)
            gvLedger.AllowPaging = False
            gvSummary.AllowPaging = False
            getLedgerTransport(ddlSiblings.SelectedValue)
            If RdDetails.Checked Then
                gvLedger.Style.Add("font-size", "9px")
                gvLedger.RenderControl(htw)
                gvLedger.Style.Remove("font-size")
                gvLedger.AllowPaging = True
                gvLedger.PageSize = 10
            Else
                gvSummary.Style.Add("font-size", "9px")
                gvSummary.RenderControl(htw)
                gvSummary.Style.Remove("font-size")
                gvSummary.AllowPaging = True
                gvSummary.PageSize = 10
            End If
            getLedgerTransport(ddlSiblings.SelectedValue)
            Using strWriter As New StreamWriter(strHTMLpath, False, Encoding.UTF8)
                strWriter.Write("" + htw.InnerWriter.ToString() + "")
                strWriter.Close()
                strWriter.Dispose()
            End Using
            Dim styles As New iTextSharp.text.html.simpleparser.StyleSheet()
            Using ms As New MemoryStream
                Using document As New Document(PageSize.A4, 30, 30, 60, 35)
                    document.SetPageSize(PageSize.A4.Rotate)
                    PdfWriter.GetInstance(document, ms)
                    document.Open()
                    Dim htmlarraylist As System.Collections.Generic.List(Of IElement) = HTMLWorker.ParseToList(New StreamReader(strHTMLpath, Encoding.Default), styles)
                    For k As Integer = 0 To htmlarraylist.Count - 1
                        document.Add(DirectCast(htmlarraylist(k), IElement))
                    Next
                    document.Close()
                End Using

                Response.Clear()
                Response.ContentType = "application/octet-stream"
                Response.AddHeader("content-disposition", "attachment;filename=" & tempFileName & ".pdf")
                Response.Buffer = True
                Response.Clear()
                Dim bytes = ms.ToArray
                Response.OutputStream.Write(bytes, 0, bytes.Length)
                Response.OutputStream.Flush()

            End Using
        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Sub

End Class
