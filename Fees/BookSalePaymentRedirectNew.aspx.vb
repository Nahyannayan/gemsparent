﻿Imports System.Data
Imports Microsoft.ApplicationBlocks.Data

Partial Class Fees_BookSalePaymentRedirectNew
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        PaymentRedirect()
    End Sub

    Sub PaymentRedirect()
        Dim SRC As String = "BOOKSALES"
        Dim MyPaymentGateway As New PaymentGatewayClass(Session("CPS_ID"))
        Dim UrlRef As String = Request.UrlReferrer.ToString()
        If Not Request.QueryString("SRC") Is Nothing AndAlso Request.QueryString("SRC") <> "" Then
            SRC = Encr_decrData.Decrypt(Request.QueryString("SRC").Replace(" ", "+"))
        End If
        '-------------------code added by Jacob on 29/jul/2019 for MPGS integration
        If (MyPaymentGateway.CPM_GATEWAY_TYPE = "MPGS" Or MyPaymentGateway.CPM_GATEWAY_TYPE = "INDUS") Then
            Dim MPGSdata As New NameValueCollection()
            Session("CPM_GATEWAY_TYPE") = MyPaymentGateway.CPM_GATEWAY_TYPE
            MPGSdata("SRC") = Encr_decrData.Encrypt("BOOKSALES")
            MPGSdata("ID") = Encr_decrData.Encrypt(Session("vpc_MerchTxnRef"))
            MPGSdata("APP") = Encr_decrData.Encrypt("GEMSPARENT")
            MPGSdata("APP_RESULT_PAGE") = Session("vpc_ReturnURL").ToString().ToLower().Replace("fees/booksalepaymentresult.aspx", "Fees/PaymentResultPage.aspx").Split("?")(0).Replace("http://", IIf(Request.Url.Host.Contains("localhost"), "http://", "https://"))
            HttpHelper.RedirectAndPOST(Me.Page, MyPaymentGateway.PAYMENT_GATEWAY_URL, MPGSdata)
        Else '-------------------------------------------------------------------------
            'Dim MyPaymentGateway As New PaymentGatewayClass(Session("CPS_ID"))
            Dim data As New NameValueCollection()
            data("vpc_Command") = "pay"
            data("vpc_Version") = "1"
            data("vpc_ReturnURL") = Session("vpc_ReturnURL").ToString().ToLower().Replace("http://", IIf(Request.Url.Host.Contains("localhost"), "http://", "https://"))
            data("vpc_Locale") = "en"
            data("vpc_OrderInfo") = Session("vpc_OrderInfo")
            data("Title") = "ASP VPC 3-Party"
            Session("vpc_Amount") = MyPaymentGateway.GetAmountInPaymentGatewayFormat(Session("vpc_Amount"))
            HttpHelper.RedirectAndPOST(Me.Page, "CS_VPC_3Party_DO.aspx", data)
        End If
    End Sub

End Class
