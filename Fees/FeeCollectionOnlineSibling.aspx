﻿<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/ParentMaster.master" CodeFile="FeeCollectionOnlineSibling.aspx.vb" Inherits="Fees_FeeCollectionOnlineSibling" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphParent" runat="Server">

    <script src="../Scripts/jquery-1.9.1.js" type="text/javascript"></script>

    <link rel="stylesheet" type="text/css" href="../Scripts/jQuery-ui-1.10.3.css" />
    <link href="../CSS/Popup.css" rel="stylesheet" />
    <!-- Add fancyBox -->
    <style>
        @media (min-width:980px) {
            .PopUpMessageStyle {
                /*background-image: linear-gradient(to left, #eff1fd 0%, #dbe0ff 100%) !important;*/
                left: 15px !important;
                border: 1px solid #d5dbff !important;
                border-radius: 4px !important;
                padding: 5px !important;
                text-align: -webkit-center !important;
                box-shadow: 4px 6px 13px #cdc;
                background-color: #f1f1f1;
            }
        }

        @media (max-width:980px) {
            .PopUpMessageStyle {
                /*background-image: linear-gradient(to left, #eff1fd 0%, #dbe0ff 100%) !important;*/
                left: 15px !important;
                border: 1px solid #d5dbff !important;
                padding: 5px !important;
                text-align: -webkit-center !important;
                top: 100px !important;
                box-shadow: 4px 6px 13px #cdc;
                background-color: #f1f1f1;
            }
        }


        /*.RadSlider_Metro .rslHorizontal .rslItem {
            background-image:none!important;            
        }*/

        .RadSlider .rslHorizontal .rslItem, .RadSlider .rslHorizontal .rslLargeTick, .RadSlider .rslHorizontal .rslSmallTick {
            background-position: center !important;
        }

        .RadSlider_Silk {
            height: 80px !important;
        }

        .imgStyle {
            float: left;
        }

        .feeImg {
            /*max-width:50%;*/
            max-width: 120px;
            max-height: 60px;
        }
    </style>
    <script type="text/javascript" src="../Scripts/Fancybox/jquery.fancybox.js?v=2.1.5"></script>
    <script type="text/javascript" src="../Scripts/Fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
    <link rel="stylesheet" href="../Scripts/Fancybox/jquery.fancybox.css" type="text/css"
        media="screen" />
    <link rel="stylesheet" type="text/css" href="../css/slider.css" />
    <script type="text/javascript" src="../Scripts/PopupJQuery.js"></script>

    <script type="text/javascript" language="javascript">

        function sliderEndSlide(sender, args) {
            var sliderMin;
            //var sliderMax;

            sliderMin = sender.get_element().getAttribute('sliderClientMinValue');
            // sliderMax = sender.get_element().getAttribute('sliderClientMaxValue');

            if (sender.get_selectionStart() < sliderMin) {
                sender.set_selectionStart(sliderMin);
            }

            //if (sender.get_selectionEnd() > sliderMax) {
            //    sender.set_slectionEnd(sliderMax);
            //}
        }


        function OnClientLoaded(sender, args) {
            var slider = sender;
            if ($(window).width() < 979) {
                slider.set_orientation(1);
                slider.set_width(50);
                slider.set_height(600);
            }
        }


        function ShowInfoW(w, h) {
            $.fancybox({
                href: '#testpopup',
                maxHeight: 600,
                fitToView: true,
                width: w,
                height: h,
                padding: 0,
                'titleShow': false,
                'transitionIn': 'elastic',
                'transitionOut': 'elastic',
                closeEffect: 'fade',
                helpers: {
                    overlay: { closeClick: false } // prevents closing when clicking OUTSIDE fancybox 
                },
            });

            return false;
        }



    </script>


    <script>
        if ($(window).width() < 979) {
            if ($(location).attr("href").indexOf("FeeCollectionOnlineSibling_M.aspx") == -1) {
                window.location = "\\fees\\FeeCollectionOnlineSibling_M.aspx";
            }
        }
        if ($(window).width() > 979) {
            if ($(location).attr("href").indexOf("FeeCollectionOnlineSibling.aspx") == -1) {
                window.location = "\\fees\\FeeCollectionOnlineSibling.aspx";
            }
        }
    </script>


    <div class="content margin-top30 margin-bottom60">
        <div class="container">
            <div class="row">

                <!-- Posts Block -->
                <div class="posts-block col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="my-account">

                        <div class="bottom-padding">
                            <div class="title-box">
                                <h3>Fee Online Payment 
                                    <span class="profile-right">
                                        <asp:Label ID="lbChildName" runat="server"></asp:Label>
                                    </span></h3>
                            </div>
                            <!-- Table  -->
                            <div class="table-responsive">

                                <%If lblError.Text <> "" Then%>
                                <asp:Label ID="lblError" runat="server" EnableViewState="False"></asp:Label>
                                <asp:HiddenField ID="hidAmountTotal" runat="server" />
                                <%End If%>

                                <table align="center" cellpadding="0" cellspacing="0" style="width: 100%;" class="table table-striped table-bordered table-responsive text-left my-orders-table">



                                    <tr>
                                        <th align="left" class="tdfields" width="20%">Date
                                        </th>
                                        <td align="left" class="tdfields">
                                            <asp:Label ID="lblDate" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th align="left" class="tdfields">School
                                        </th>
                                        <td align="left" class="tdfields">
                                            <asp:Label ID="lblschool" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th class="tdfields">Select a Payment Gateway
                <br />
                                            <asp:LinkButton ID="lMoreInfo" runat="server" Font-Bold="True" Font-Underline="True"
                                                Text="More Info" ForeColor="Blue" Visible="false">
                                            </asp:LinkButton>
                                        </th>
                                        <td align="left" colspan="1" style="height: 21px" valign="middle">
                                            <asp:RadioButtonList ID="rblPaymentGateway" runat="server" RepeatDirection="Horizontal"
                                                RepeatLayout="Flow" AutoPostBack="True" RepeatColumns="2">
                                            </asp:RadioButtonList>
                                            <asp:HiddenField ID="HFCardCharge" runat="server" />
                                        </td>
                                    </tr>
                                    <tr id="tr_AddFee" runat="server">
                                        <th align="left" colspan="2" class="trSub_Header">Fee Details
                <%= IIf(Session("BSU_CURRENCY") Is Nothing, "", "(Amount In " & Session("BSU_CURRENCY") & ")")%>
                                            <%--<asp:label id="lblCurrency" runat="server" Text="" ></asp:label> --%>
                                            <%--<div>
                    <asp:LinkButton ID="lnkMoreFee" runat="server" OnClientClick="HideAddFee(); return false;"
                        ForeColor="White" Visible="False">Add More Fee Head(s)</asp:LinkButton>&nbsp;
                </div>--%>
                                            <span class="profile-right">
                                                <asp:CheckBox ID="chkLoadSiblings" runat="server" Text="Pay for Sibling(s)" Checked="true" AutoPostBack="true" /></span>
                                        </th>
                                    </tr>
                                    <tr style="display: none;" visible="false">
                                        <td colspan="2"></td>
                                    </tr>
                                    <tr>
                                        <td align="left" colspan="2">
                                            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                <tr>
                                                    <td align="left">
                                                        <asp:Repeater ID="repInfo" runat="server">
                                                            <HeaderTemplate>
                                                                <table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding: 0;">
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <tr>
                                                                    <td colspan="3">&nbsp;</td>
                                                                </tr>
                                                                <tr class="trSub_Header_Small">
                                                                    <th colspan="2">
                                                                        <asp:Label ID="lbSName" runat="server" Text='<%# Bind("SNAME") %>'></asp:Label>&nbsp;[<asp:Label ID="lbSNo" runat="server" Text='<%# Bind("STU_NO")%>'></asp:Label>]
                                                                    </th>
                                                                    <th align="right" class="text-right">
                                                                        <div style="clear: both;">
                                                                            <asp:CheckBox ID="nxtAcaFee" runat="server" OnCheckedChanged="Load_Next_Acd_Fee_Details" AutoPostBack="True" Visible="false" Text="Next Academic Year Fee" CssClass="tdfields" />&nbsp;&nbsp
                                                                            <asp:Label ID="lblNacdFeetext" runat="server" Visible="false" CssClass="text-danger"></asp:Label>
                                                                            ;&nbsp;;&nbsp;

                                                                            <asp:Label ID="lbGrade" runat="server" Text='<%# Bind("GRM_DISPLAY")%>'></asp:Label>-<asp:Label ID="lbSection" runat="server" Text='<%# Bind("SCT_DESCR")%>'></asp:Label>&nbsp;-
                                                <asp:LinkButton ID="lnkViewFeeSetup" Text="View Fee Schedule" CssClass="linkFee" runat="server"></asp:LinkButton>
                                                                            <asp:HiddenField ID="hfSTU_ID" Value='<%# Bind("STU_ID") %>' runat="server" />
                                                                            <asp:HiddenField ID="hfSTU_FEE_ID" Value='<%# Bind("STU_FEE_ID")%>' runat="server" />
                                                                            <asp:HiddenField ID="hfSTU_ACD_ID" Value='<%# Bind("STU_ACD_ID")%>' runat="server" />
                                                                        </div>
                                                                    </th>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="3">&nbsp;</td>
                                                                </tr>
                                                                <tr id="trgvrerror" runat="server" visible="false">
                                                                    <td colspan="3" align="center">
                                                                        <asp:Label ID="lblGvrError" Text="" runat="server"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr align="left" style="border: none 0px !important;">
                                                                    <td valign="top" colspan="3" align="left">
                                                                        <table cellpadding="0" width="100%" cellspacing="0" border="0" style="border-collapse: collapse !important; border-spacing: 0 !important; padding: 0 !important; border: none 0px !important;">
                                                                            <tr>
                                                                                <td valign="top" align="left" width="10%">
                                                                                    <asp:Image ID="imgEmpImage" runat="server" Height="80px" ImageUrl='<%# Bind("PHOTOPATH") %>' ToolTip='<%# Bind("SNAME") %>'
                                                                                        Width="60px" />
                                                                                </td>
                                                                                <td valign="top" width="90%">
                                                                                    <asp:GridView ID="gvFee" runat="server" AutoGenerateColumns="False" EmptyDataText="No Details"
                                                                                        CssClass="table table-striped table-bordered table-responsive text-left my-orders-table" Width="100%" OnRowDataBound="gvFeeCollection_RowDataBound" ShowFooter="true">
                                                                                        <RowStyle Height="20px" VerticalAlign="Top" />
                                                                                        <Columns>
                                                                                            <asp:TemplateField HeaderText="FSR_FEE_ID" Visible="False">
                                                                                                <ItemTemplate>
                                                                                                    <asp:Label ID="lblFSR_FEE_ID" runat="server" Text='<%# Bind("FEE_ID") %>'></asp:Label>
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderText="Fee">
                                                                                                <ItemTemplate>
                                                                                                    <asp:Label ID="lblFeeDescr" runat="server" Text='<%# Bind("FEE_DESCR") %>'></asp:Label>&nbsp;&nbsp;
                                                                          <asp:LinkButton runat="server" ID="btnSlider" Visible="false" OnClientClick="return false;" ToolTip="Click here to select advance payment" Text="Click here to select advance payment" />
                                                                                                    <%--<a runat="server" id="btnSlider" onclientclick="return false;" tooltip="Click Here To Select Advance Payment">(Click Here for Advance Payment Option)</a>--%>
                                                                                                    <asp:Panel ID="RadSliders_Wrapper" runat="server" CssClass="sliderView horizontalSliderView">
                                                                                                        <div id="divslider" runat="server" class="PopUpMessageStyle">
                                                                                                            <asp:Label ID="lblmessage" runat="server" ForeColor="Red" />
                                                                                                            <telerik:RadSlider RenderMode="Lightweight" ID="RadSlider_Ticks" runat="server" Height="80px" Width="700px" ItemType="Item"
                                                                                                                AnimationDuration="400" ThumbsInteractionMode="Free" OnValueChanged="RadSlider_Ticks_ValueChanged" AutoPostBack="true" Skin="Silk" TrackPosition="Center" OnClientLoaded="OnClientLoaded">
                                                                                                                <ItemBinding TextField="F_MONTH" ValueField="ID" />
                                                                                                            </telerik:RadSlider>
                                                                                                            <asp:HiddenField ID="hid_sli_stu_id" runat="server" />
                                                                                                            <div class="text-right">
                                                                                                                <asp:Label ID="lblonetamt" runat="server" Text="Total amount : " ForeColor="#333333" Font-Bold="true" />
                                                                                                                <asp:Label ID="txtNetAmt" runat="server" ForeColor="#333333" Style="padding-right: 25px;" />
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </asp:Panel>
                                                                                                    <ajaxToolkit:PopupControlExtender ID="pcet_Lib_Iss" runat="server"
                                                                                                        OffsetX="-750" PopupControlID="divslider"
                                                                                                        Position="Bottom" TargetControlID="btnSlider">
                                                                                                    </ajaxToolkit:PopupControlExtender>
                                                                                                </ItemTemplate>
                                                                                                <FooterTemplate>
                                                                                                    <asp:Label ID="lblTotals" runat="server" Text='TOTAL'></asp:Label>
                                                                                                </FooterTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderText="Due">
                                                                                                <ItemTemplate>
                                                                                                    <asp:Label ID="lblAmount" runat="server" Text='<%# Bind("CLOSING", "{0:0.00}") %>'></asp:Label>
                                                                                                </ItemTemplate>
                                                                                                <FooterTemplate>
                                                                                                    <asp:Label ID="lblAmountF" runat="server" Text=""></asp:Label>
                                                                                                </FooterTemplate>
                                                                                                <ItemStyle HorizontalAlign="Right" />
                                                                                                <FooterStyle HorizontalAlign="Right" />
                                                                                                <HeaderStyle HorizontalAlign="Right" Width="10%" />
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderText="Discount" Visible="false">
                                                                                                <ItemTemplate>
                                                                                                    <span class="ui-icon ui-icon-info" id="spanInfo" runat="server" visible="false" style="float: left; margin: 0 15px 10px 0;"></span>
                                                                                                    <ajaxToolkit:HoverMenuExtender ID="pceDiscountDetail" runat="server" PopupControlID="pnlDiscntDtl"
                                                                                                        PopupPosition="Top" TargetControlID="spanInfo" OffsetX="-300">
                                                                                                    </ajaxToolkit:HoverMenuExtender>
                                                                                                    <asp:Label ID="lblDiscount" runat="server" Text="0.00"></asp:Label>
                                                                                                    <asp:Panel ID="pnlDiscntDtl" Style="display: none" runat="server">
                                                                                                        <table width="100%" class="table table-striped table-bordered table-responsive text-left my-orders-table" cellpadding="0" cellspacing="0" align="center">
                                                                                                            <tr>
                                                                                                                <td>
                                                                                                                    <asp:Repeater ID="gvDiscDt" runat="server" EnableViewState="false">
                                                                                                                        <HeaderTemplate>
                                                                                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                                                                                <tr class="trSub_Header_Small">
                                                                                                                                    <th style="width: 250px">Month
                                                                                                                                    </th>
                                                                                                                                    <th style="width: 20%">Fee
                                                                                                                                    </th>
                                                                                                                                    <th style="width: 20%">Disc(%)
                                                                                                                                    </th>
                                                                                                                                    <th style="width: 20%">Discount
                                                                                                                                    </th>
                                                                                                                                    <th style="width: 20%">Net
                                                                                                                                    </th>
                                                                                                                                </tr>
                                                                                                                        </HeaderTemplate>
                                                                                                                        <AlternatingItemTemplate>
                                                                                                                            <tr>
                                                                                                                                <td align="left">
                                                                                                                                    <%#Container.DataItem("MONTH")%>
                                                                                                                                </td>
                                                                                                                                <td align="right">
                                                                                                                                    <%# Math.Round(Convert.ToDecimal(Container.DataItem("FEE_AMOUNT")), 2)%>
                                                                                                                                </td>
                                                                                                                                <td align="right">
                                                                                                                                    <%# Math.Round(Convert.ToDecimal(Container.DataItem("DISC_PERC")), 2)%>
                                                                                                                                </td>
                                                                                                                                <td align="right">
                                                                                                                                    <%# Math.Round(Convert.ToDecimal(Container.DataItem("DISC_AMOUNT")), 2)%>
                                                                                                                                </td>
                                                                                                                                <td align="right">
                                                                                                                                    <%# Math.Round(Convert.ToDecimal(Container.DataItem("NET_AMOUNT")), 2)%>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </AlternatingItemTemplate>
                                                                                                                        <ItemTemplate>
                                                                                                                            <tr style="background-color: #ffffff; border: 1pt; color: #1B80B6; border-color: #1b80b6;">
                                                                                                                                <td align="left">
                                                                                                                                    <%#Container.DataItem("MONTH")%>
                                                                                                                                </td>
                                                                                                                                <td align="right">
                                                                                                                                    <%# Math.Round(Convert.ToDecimal(Container.DataItem("FEE_AMOUNT")), 2)%>
                                                                                                                                </td>
                                                                                                                                <td align="right">
                                                                                                                                    <%# Math.Round(Convert.ToDecimal(Container.DataItem("DISC_PERC")), 2)%>
                                                                                                                                </td>
                                                                                                                                <td align="right">
                                                                                                                                    <%# Math.Round(Convert.ToDecimal(Container.DataItem("DISC_AMOUNT")), 2)%>
                                                                                                                                </td>
                                                                                                                                <td align="right">
                                                                                                                                    <%# Math.Round(Convert.ToDecimal(Container.DataItem("NET_AMOUNT")), 2)%>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </ItemTemplate>
                                                                                                                        <FooterTemplate>
                                                                                                                            </table>
                                                                                                                        </FooterTemplate>
                                                                                                                    </asp:Repeater>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </asp:Panel>
                                                                                                </ItemTemplate>
                                                                                                <FooterTemplate>
                                                                                                    <asp:Label ID="lblDiscountF" runat="server" Text="0.00"></asp:Label>
                                                                                                </FooterTemplate>
                                                                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                                                                                <FooterStyle HorizontalAlign="Right" />
                                                                                                <ItemStyle HorizontalAlign="Right" />
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderText="Paying Now">
                                                                                                <ItemTemplate>

                                                                                                    <asp:TextBox ID="txtAmountToPay" CssClass="form-control" AutoCompleteType="Disabled" runat="server" AutoPostBack="True"
                                                                                                        onFocus="this.select();" OnTextChanged="txtAmountToPay_TextChanged" Style="text-align: right" Width="80%"
                                                                                                        TabIndex="52" Text='<%# Bind("Amount", "{0:0.00}") %>'></asp:TextBox>
                                                                                                    <ajaxToolkit:FilteredTextBoxExtender ID="ftbe" runat="server"
                                                                                                        TargetControlID="txtAmountToPay"
                                                                                                        FilterType="Custom, Numbers"
                                                                                                        ValidChars="." />
                                                                                                    <asp:LinkButton ID="lbCancel" Visible="false" runat="server" OnClick="lbCancel_Click">Cancel</asp:LinkButton>
                                                                                                    <asp:HiddenField ID="h_OtherCharge" runat="server" />
                                                                                                    <asp:HiddenField ID="h_Discount" runat="server" />
                                                                                                    <asp:HiddenField ID="h_BlockPayNow" runat="server" Value='<%# Bind("BlockPayNow") %>' />
                                                                                                    <asp:HiddenField ID="h_ProcessingCharge" runat="server" />
                                                                                                </ItemTemplate>

                                                                                                <FooterTemplate>
                                                                                                    <asp:Label ID="lblAmounttoPayF" runat="server" Text="0.00"></asp:Label>
                                                                                                    <asp:HiddenField ID="h_ProcessingChargeF" runat="server" />
                                                                                                </FooterTemplate>
                                                                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                                                                                <FooterStyle HorizontalAlign="Right" />
                                                                                                <ItemStyle HorizontalAlign="Right" Width="20%" />
                                                                                            </asp:TemplateField>
                                                                                        </Columns>
                                                                                        <FooterStyle Font-Bold="true" />
                                                                                    </asp:GridView>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                </table>
                                                            </FooterTemplate>
                                                        </asp:Repeater>
                                                    </td>
                                                </tr>
                                                <tr id="tr_TaxMessage" runat="server" class="tdblankAll">
                                                    <td align="center" class="tdfields" valign="middle">
                                                        <asp:Label ID="lblalert" CssClass="alert-warning alert" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr id="tr_DiscountTotal" runat="server" class="tdblankAll" visible="false">
                                                    <td align="right" class="tdfields" valign="middle">
                                                        <h4>Discount Total :
            <asp:TextBox ID="txtDiscountTotal" CssClass="form-control" Style="text-align: right" runat="server" TabIndex="150" Visible="false">
            </asp:TextBox>
                                                            <asp:Label ID="lblDiscountTotal" CssClass="form-control" Style="text-align: right" runat="server" TabIndex="150" Width="20%">
                                                            </asp:Label>
                                                        </h4>
                                                    </td>
                                                </tr>
                                                <tr id="id_notusing" runat="server" visible="false" class="tdblankAll">
                                                    <td align="left" class="tdblankAll">
                                                        <asp:TextBox ID="txtRemarks" CssClass="form-control" runat="server" Height="52px" SkinID="TextBoxMultiTextLarge"
                                                            TabIndex="100" TextMode="MultiLine" Width="84%" ReadOnly="True" Visible="False">
                                                        </asp:TextBox>
                                                        <asp:DropDownList ID="ddlFeeType" CssClass="form-control" runat="server" DataSourceID="odsGetFEETYPE_M" DataTextField="FEE_DESCR"
                                                            DataValueField="FEE_ID" SkinID="DropDownListNormal">
                                                        </asp:DropDownList>
                                                        <asp:TextBox ID="txtAmountAdd" CssClass="form-control" runat="server" onblur="CheckNumber(this)" AutoCompleteType="Disabled"
                                                            TabIndex="45">
                                                        </asp:TextBox>
                                                        <asp:Button ID="btnAddDetails" runat="server" Text="Add" CausesValidation="False"
                                                            TabIndex="50" />
                                                    </td>
                                                </tr>
                                                <tr class="tdblankAll">
                                                    <td align="right" valign="middle" class="tdfields">
                                                        <h4>Total :
            <asp:TextBox ID="txtGridTotal" CssClass="form-control" Style="text-align: right" runat="server" TabIndex="150" Visible="false">
            </asp:TextBox>
                                                            <asp:Label ID="lblGridTotal" CssClass="form-control text-right" runat="server" TabIndex="150" Width="20%">       </asp:Label>
                                                        </h4>
                                                    </td>
                                                </tr>
                                                <tr class="tdblankAll">
                                                    <td align="right" valign="middle" class="tdfields">
                                                        <h4>Processing Charge :
            <asp:TextBox ID="txtCardCharge" CssClass="form-control" Style="text-align: right" runat="server" TabIndex="151" Visible="false">
            </asp:TextBox>
                                                            <asp:Label ID="lblCardCharge" CssClass="form-control text-right" runat="server" TabIndex="151" Width="20%">    </asp:Label>
                                                        </h4>
                                                    </td>
                                                </tr>
                                                <tr class="tdblankAll">
                                                    <td align="right" valign="middle" class="tdfields">
                                                        <h4>Net Payable :
            <asp:TextBox ID="txtTotal" CssClass="form-control" Style="text-align: right" runat="server" TabIndex="152" Visible="false">
            </asp:TextBox>
                                                            <asp:Label ID="lblTotal" CssClass="form-control text-right" runat="server" TabIndex="152" Width="20%">    </asp:Label>
                                                        </h4>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr class="tdblankAll">
                                        <td align="center" colspan="2" class="tdblankAll">
                                            <asp:Button ID="btnSave" runat="server" CssClass="btn btn-info" TabIndex="155" Text="Confirm & Proceed" />
                                            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="btn btn-info"
                                                Text="Cancel" TabIndex="158" />
                                            <asp:Button ID="btnShow" runat="server" CausesValidation="False" Visible="false"
                                                CssClass="btn btn-info" Text="Show" TabIndex="158" />
                                    </tr>
                                    <tr class="tdblankAll" id="tr_continue" runat="server" visible="false">
                                        <td align="center" colspan="2" class="tdblankAll">
                                            <span class="ui-icon ui-icon-info" style="float: left; margin: 0 7px 7px 0;"></span>
                                            <asp:Label ID="lblPayMsg" runat="server"
                                                EnableViewState="False"></asp:Label><br />
                                            <br />
                                            <br />
                                            <span class="ui-icon ui-icon-notice" style="float: left; margin: 0 7px 7px 0;"></span>
                                            &nbsp
            <asp:Label ID="lblPayMsgIE" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr id="tr_comment" runat="server" class="tdblankAll" visible="false">
                                        <td align="left" style="height: 18px; font-size: 13px; color: #0026ff;" colspan="2"
                                            class="tdblankAll">Please enter the amount paying now and Click Proceed.</td>
                                    </tr>
                                </table>
                                <asp:ObjectDataSource ID="odsGetFEETYPE_M" runat="server" OldValuesParameterFormatString="original_{0}"
                                    SelectMethod="GetFEES_M" TypeName="FeeCommon">
                                    <SelectParameters>
                                        <asp:SessionParameter DefaultValue="123004" Name="BSU_ID" SessionField="BSU_ID" Type="String" />
                                        <asp:SessionParameter DefaultValue="0" Name="ACD_ID" SessionField="ACD_ID" Type="String" />
                                    </SelectParameters>
                                </asp:ObjectDataSource>
                                <span class="anim">
                                    <label id="test" runat="server">
                                    </label>
                                </span>
                                <asp:Panel ID="Panel1" runat="server">
                                    <table cellpadding="3" cellspacing="0" class="BlueTable_simple">
                                        <tr class="tdblankAll">
                                            <td class="tdblankAll" align="left">
                                                <asp:GridView ID="gvDiscount" runat="server" SkinID="GridViewNormal" Width="100%"
                                                    EmptyDataText="No Data Found" AutoGenerateColumns="False">
                                                    <Columns>
                                                        <asp:BoundField DataField="Scheme" HeaderText="Scheme" Visible="False">
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="Priod" HeaderText="Valid period">
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="Slab" HeaderText="Fee Terms">
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="Fees" HeaderText="Fees">
                                                            <HeaderStyle HorizontalAlign="Right" />
                                                            <ItemStyle HorizontalAlign="Right" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="PayNow" HeaderText="Pay Now">
                                                            <HeaderStyle HorizontalAlign="Right" />
                                                            <ItemStyle HorizontalAlign="Right" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="YouSave" HeaderText="You Save">
                                                            <HeaderStyle HorizontalAlign="Right" />
                                                            <ItemStyle HorizontalAlign="Right" />
                                                        </asp:BoundField>
                                                        <asp:TemplateField HeaderText="Select">
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="lbApply" runat="server" OnClick="lbApply_Click">Select</asp:LinkButton>
                                                                <asp:Label ID="lblFee_ID" runat="server" Text='<%# Bind("FEE_ID") %>' Visible="false"></asp:Label>
                                                                <asp:Label ID="lblFds_ID" runat="server" Text='<%# Bind("FDS_ID") %>' Visible="false"></asp:Label>
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Center" />
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <div id="testpopup" class="darkPanelM anim" runat="server" style="display: none; z-index: 3000 !important;">
                                    <div id="divboxpanelconfirm" runat="server" style="height: 50%; width: 45%; margin-left: 30%; margin-top: 10%;" class="darkPanelMTop">
                                        <div class="holderInner" style="height: 90%; width: 98%;">
                                            <center>
                            <table cellpadding="5" cellspacing="2" border="0" style="height: 90% !important; width: 100% !important; overflow-y: scroll;"
                                class="tableNoborder">
                                <tr class="trSub_Header">
                                    <td>
                                        <span style="float: left; margin: 0 7px 50px 10px;"></span>
                                        <h4>Confirm & Proceed</h4>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top">
                                        <span  style="float: left; margin: 0 7px 50px 10px;"></span>
                                        <asp:Label ID="lblMsg"  runat="server"></asp:Label><br />   <%--Style="font-family: Verdana, Geneva, Tahoma, sans-serif; font-size: 12px;"--%>
                                        <br />
                                        <br />
                                        
                                <asp:Label ID="lblMsg2" runat="server" class="alert-success alert" style="float: left; margin: 0 7px 50px 10px;"></asp:Label>  <%--BackColor="Beige" Font-Names="Verdana" ForeColor="Navy"  Font-Size="10pt"--%>
                                         <br />
                                        <br />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">
                                        <asp:Button ID="btnProceed" runat="server" CausesValidation="False" CssClass="btn btn-info"
                                            Text="Confirm & Proceed" />
                                        <asp:Button ID="btnSkip" runat="server" CausesValidation="False" CssClass="btn btn-info"
                                            Text="Cancel"  />
                                    </td>
                                </tr>
                            </table>
                        </center>
                                        </div>
                                    </div>
                                </div>


                            </div>
                            <!-- /Table  -->
                        </div>

                    </div>
                </div>
                <!-- /Posts Block -->

            </div>
        </div>
    </div>


</asp:Content>
