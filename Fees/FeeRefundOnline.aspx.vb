﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Imports Telerik.Web.UI
Partial Class Fees_FeeRefundOnline
    Inherits System.Web.UI.Page
    Const CASH_ACCOUNT As Integer = 24301001
    Dim Encr_decrData As New Encryption64
    Private Property dBalanceTotal() As Double
        Get
            Return ViewState("dBalanceTotal")
        End Get
        Set(ByVal value As Double)
            ViewState("dBalanceTotal") = value
        End Set
    End Property
    Private Property dPayAmountTotal() As Double
        Get
            Return ViewState("dPayAmountTotal")
        End Get
        Set(ByVal value As Double)
            ViewState("dPayAmountTotal") = value
        End Set
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("username") Is Nothing Then
            Response.Redirect("~\login.aspx")
        End If
        If Page.IsPostBack = False Then

            Page.Title = "::GEMS Education | Fee Refund::"
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If

            ViewState("datamode") = "add"
            Dim CurUsr_id As String = Session("STU_ID")
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("STU_NAME")
            InitialiseCompnents()
            lbChildNameTop.Text = Session("STU_NAME")
            If USR_NAME = "" Or CurBsUnit = "" Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else

                ViewState("STU_NO") = Session("STU_NO")
                ViewState("STU_NAME") = Session("STU_NAME")
                ViewState("STU_GRD_ID") = Session("STU_GRD_ID")
                ViewState("stu_section") = Session("stu_section")
                ViewState("BSU_NAME") = Session("BSU_NAME")

                lblschool.Text = ViewState("BSU_NAME")
                Gridbind_Feedetails()
                txtTotal.Text = New Double().ToString("#,##0.00")

            End If


            Dim isVal As Integer = FeeCommon.GetNotApprovedFeeRefundRequest(CurBsUnit, CurUsr_id)
            If isVal >= 1 Then
                btnSave.Visible = False
                ShowMessage("Cannot create a new request. A pending fee request exists. Please contact school finance.", True)
                disableCheckbox()
                txt_Remarks2.Disabled = True
            End If

            tr_TaxMessage.Visible = False
        End If

    End Sub

    Protected Sub repInfo_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles repInfo.ItemDataBound
        Dim hfSTU_ID As New HiddenField
        Dim hfSTU_ACD_ID As New HiddenField
        Dim gvFee As New GridView
        Dim lnkViewFeeSetup As New LinkButton
        Dim lblGvrError As New Label
        If e.Item.DataItem Is Nothing Then
            Return
        Else
            gvFee = DirectCast(e.Item.FindControl("gvFee"), GridView)
            hfSTU_ID = DirectCast(e.Item.FindControl("hfSTU_ID"), HiddenField)
            hfSTU_ACD_ID = DirectCast(e.Item.FindControl("hfSTU_ACD_ID"), HiddenField)
            lnkViewFeeSetup = DirectCast(e.Item.FindControl("lnkViewFeeSetup"), LinkButton)
            lblGvrError = DirectCast(e.Item.FindControl("lblGvrError"), Label)
            Dim retval As String = "0"
            If IsDate(lblDate.Text) Then
                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim dt As New DataTable
                Try
                    retval = FeeCommon.Resettlement(Session("sBsuid"), hfSTU_ID.Value, "S")
                Catch ex As Exception
                    retval = "1000"
                End Try
                If retval = "0" Then
                    dt = FeeCommon.GetOnlineRefundData(hfSTU_ID.Value, "S", Session("sBsuid"))
                    If dt.Rows.Count > 0 Then
                        gvFee.DataSource = dt
                        gvFee.DataBind()

                        gvFee.DataSource = dt
                        gvFee.DataBind()

                    Else
                        gvFee.DataBind()

                    End If
                End If
            End If
            End If
    End Sub

    Sub InitialiseCompnents()
        lblDate.Text = Format(Date.Now, "dd/MMM/yyyy")
        txtTotal.Attributes.Add("readonly", "readonly")

    End Sub

    Sub clear_All()
        txtTotal.Text = ""

        ClearStudentData()
    End Sub

    Private Function TrimStudentNo(ByVal STUNO As String) As String
        TrimStudentNo = ""
        If STUNO.Length >= 14 Then
            TrimStudentNo = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT CAST(REPLACE('" & STUNO & "',SUBSTRING('" & STUNO & "',1,6),'')AS INTEGER)AS STUNO")
        End If
    End Function

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click

        Dim str_error As String = ""

        If Not IsDate(lblDate.Text) Then
            str_error = str_error & "Invalid date <br />"
        End If
        If Not IsNumeric(Session("STU_ID")) Then
            str_error = str_error & "Please select student <br />"
        End If

        'Dim amt_validation As String = getTotal()
        'If amt_validation <> "" Then
        '    str_error = str_error & " " & amt_validation & " <br />"
        'End If

        Dim dblTotal As Decimal = 0
        If txtTotal.Text.ToString <> "" Then
            dblTotal = CDbl(txtTotal.Text)
        End If

        If dblTotal <= 0 Then
            str_error = str_error & "Please select refund fee types <br />"
        End If
        If str_error <> "" Then
            lblError.Text = str_error
            lblError.CssClass = "alert-danger alert"
            Exit Sub
        Else
            lblError.CssClass = ""
        End If
        Dim dtFrom As DateTime
        dtFrom = CDate(lblDate.Text)
        Dim StuNos As String = ""
        Dim StuType As String = ""
        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim objConn As New SqlConnection(str_conn) '
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try
            Dim retval As String = "0"
            Dim STR_TYPE As Char = "S"
            Dim str_NEW_FRH_ID As String = "0"
            Dim LoopTotal As Double = 0
            For Each repitm As RepeaterItem In repInfo.Items
                Dim gvfee As New GridView
                gvfee = DirectCast(repitm.FindControl("gvFee"), GridView)
                Dim hfSTU_ID As HiddenField = CType(repitm.FindControl("hfSTU_ID"), HiddenField)
                Dim hfSTU_FEE_ID As HiddenField = CType(repitm.FindControl("hfSTU_FEE_ID"), HiddenField)
                Dim hfSTU_ACD_ID As HiddenField = CType(repitm.FindControl("hfSTU_ACD_ID"), HiddenField)
                Dim lblStuNo As Label = CType(repitm.FindControl("lbSNo"), Label)
                Dim hfSTU_CURRSTATUS As HiddenField = CType(repitm.FindControl("hfSTU_CURRSTATUS"), HiddenField)

                If Convert.ToDouble(txtTotal.Text) > 0 And retval = "0" Then

                    StuNos = StuNos & IIf(StuNos = "", TrimStudentNo(hfSTU_FEE_ID.Value), "," & TrimStudentNo(hfSTU_FEE_ID.Value))
                    StuType = IIf(hfSTU_CURRSTATUS.Value = "EN", "S", "E")

                    If str_NEW_FRH_ID = "0" Then
                        retval = FeeCommon.F_SaveFEE_REFUND_H(0, Session("sBsuid"), 0, StuType, _
                                       hfSTU_ID.Value, "0", lblDate.Text, lblDate.Text, False, str_NEW_FRH_ID, False, _
                                    txt_Remarks2.InnerText, "", "C", CASH_ACCOUNT, "", txtTotal.Text, "0", stTrans)
                    End If

                    If retval = "0" Then
                        For Each gvr As GridViewRow In gvfee.Rows
                            Dim cb1 As CheckBox = CType(gvr.FindControl("lblFSR_FEE_CHK_ID"), CheckBox)
                            If cb1.Checked Then
                                'Dim txtAmountToPay As Label = CType(gvr.FindControl("Amount"), Label)
                                Dim lblFSR_FEE_ID As Label = CType(gvr.FindControl("lblFSR_FEE_ID"), Label)

                                Dim txtAmountToRefund As TextBox = CType(gvr.FindControl("txtAmountToRefund"), TextBox)
                                If Not txtAmountToRefund Is Nothing Then
                                    If CDbl(txtAmountToRefund.Text > 0) Then
                                        retval = FeeCommon.F_SaveFEE_REFUND_D(0, str_NEW_FRH_ID, CDbl(txtAmountToRefund.Text), _
                                                      lblFSR_FEE_ID.Text, "", hfSTU_ID.Value, stTrans)
                                        If retval <> 0 Then
                                            Exit For
                                        End If
                                    End If
                                End If
                            End If
                        Next

                    End If
                End If
            Next

            If retval = "0" Then
                stTrans.Commit()
                Dim flagAudit As Integer = UtilityObj.operOnAudiTable("Fee Refund Online", str_NEW_FRH_ID, "Insert", Page.User.Identity.Name.ToString, Me.Page, Server.MachineName)
                Me.btnSave.Visible = False
                ShowMessage(getErrorMessage(retval) & " Request is forwarded to School Finance!!!", False)
                disableCheckbox()
                txt_Remarks2.Disabled = True
            Else
                stTrans.Rollback()
                lblError.Text = getErrorMessage(retval)
            End If

        Catch ex As Exception
            stTrans.Rollback()
            lblError.Text = getErrorMessage(1000)
            Errorlog(ex.Message)
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
                'Response.Redirect("~\login.aspx")
            End If
        End Try
    End Sub
    Protected Sub gvFeeCollection_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        Try
            If e.Row.RowType = DataControlRowType.Header Then

                dBalanceTotal = 0
                dPayAmountTotal = 0

            ElseIf e.Row.RowType = DataControlRowType.DataRow Then
                Dim txtAmountToPay As TextBox
                Dim lblAmount As New Label

                lblAmount = CType(e.Row.FindControl("lblAmount"), Label)
                txtAmountToPay = CType(e.Row.FindControl("txtAmountToPay"), TextBox)
                dBalanceTotal = dBalanceTotal + IIf(Not lblAmount Is Nothing, Convert.ToDouble(lblAmount.Text), 0)
                dPayAmountTotal = dPayAmountTotal + IIf(Not txtAmountToPay Is Nothing, Convert.ToDouble(txtAmountToPay.Text), 0)

            ElseIf e.Row.RowType = DataControlRowType.Footer Then
                'Dim lblAmountF As New Label
                'Dim lblAmounttoPayF As New Label

                'lblAmountF = CType(e.Row.FindControl("lblAmountF"), Label)
                'lblAmounttoPayF = CType(e.Row.FindControl("lblReFAmountF"), Label)

                CType(e.Row.FindControl("lblAmountF"), Label).Text = dBalanceTotal
                CType(e.Row.FindControl("lblReFAmountF"), Label).Text = dPayAmountTotal

                dBalanceTotal = 0
                dPayAmountTotal = 0

            End If
        Catch ex As Exception

        End Try

    End Sub
    Protected Sub txtAmountToPay_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim myCell, myRow As Object
            myCell = sender.parent
            If Not myCell Is Nothing Then
                myRow = myCell.parent
                If Not myRow Is Nothing Then
                    Dim txtAmountToRefund As TextBox = CType(myRow.FindControl("txtAmountToRefund"), TextBox)
                    Dim lblAmount As Label = CType(myRow.FindControl("lblAmount"), Label)

                    Try
                        Dim rfnd As Double = CDbl(txtAmountToRefund.Text)
                        Dim amt As Double = CDbl(lblAmount.Text)
                        If (rfnd > amt) Then
                            ShowMessage("Refund amount is greater than Balance amount!!!", True)
                            CType(myRow.FindControl("txtAmountToRefund"), TextBox).Text = amt
                        End If
                    Catch ex As Exception
                        ShowMessage("Enter Number in Refund Column", True)
                        CType(myRow.FindControl("txtAmountToRefund"), TextBox).Text = 0
                    End Try
                End If
            End If

            CalculateTotals()

        Catch ex As Exception

        End Try

    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click

        Response.Redirect("~\login.aspx")
    End Sub

    Sub Gridbind_Feedetails()
        Try

            Dim connPath As String = WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ConnectionString
            Dim noImagePath As String = "~/Images/Home/no_image.gif"
            Dim con As String = ConnectionManger.GetOASISConnectionString
            Dim param(2) As SqlClient.SqlParameter

            param(0) = New SqlClient.SqlParameter("@OLU_ID", Session("OLU_ID").ToString)
            param(1) = New SqlClient.SqlParameter("@connPath", connPath)
            param(2) = New SqlClient.SqlParameter("@noImagePath", noImagePath)

            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "[OPL].[GETCHILD_HOME_INFO]", param)

            repInfo.DataSource = ds.Tables(0)
            repInfo.DataBind()

        Catch ex As Exception
            repInfo.DataBind()

        End Try
    End Sub



    Sub ClearStudentData()
        Gridbind_Feedetails()
    End Sub

    Sub ClearDetail()

    End Sub

    Protected Sub rdSelect_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs)

        Try
            Dim myCell, myRow As Object
            myCell = sender.parent
            If Not myCell Is Nothing Then
                myRow = myCell.parent
                If Not myRow Is Nothing Then
                    'Dim txtAmountToRefund As TextBox = CType(myRow.FindControl("txtAmountToRefund"), TextBox)
                    'Dim lblAmount As Label = CType(myRow.FindControl("lblAmount"), Label)
                    Dim cb1 As CheckBox = CType(myRow.FindControl("lblFSR_FEE_CHK_ID"), CheckBox)
                    If cb1.Checked Then

                        CType(myRow.FindControl("txtAmountToRefund"), TextBox).Enabled = True
                        CType(myRow.FindControl("txtAmountToRefund"), TextBox).Text = CType(myRow.FindControl("lblAmount"), Label).Text
                    End If
                End If
            End If

            CalculateTotals()
        Catch ex As Exception

        End Try


    End Sub

    'Public Function getTotal() As String
    '    Dim err As String = ""
    '    Try

    '        Dim dblTotal As Double = 0
    '        Dim lblAmount As New TextBox
    '        For Each repitm As RepeaterItem In repInfo.Items
    '            Dim gvfee As New GridView
    '            gvfee = DirectCast(repitm.FindControl("gvFee"), GridView)

    '            For Each gvr As GridViewRow In gvfee.Rows

    '                Dim cb1 As CheckBox = CType(gvr.FindControl("lblFSR_FEE_CHK_ID"), CheckBox)
    '                If cb1.Checked Then
    '                    lblAmount = CType(gvr.FindControl("txtAmountToRefund"), TextBox)
    '                    CType(gvr.FindControl("txtAmountToRefund"), TextBox).Enabled = True

    '                    dblTotal += CDbl(lblAmount.Text)
    '                Else
    '                    CType(gvr.FindControl("txtAmountToRefund"), TextBox).Enabled = False
    '                    CType(gvr.FindControl("txtAmountToRefund"), TextBox).Text = 0
    '                End If
    '            Next

    '        Next
    '        txtTotal.Text = dblTotal
    '    Catch ex As Exception
    '        err = "Enter Number in Refund Column!!!"
    '        ShowMessage("Enter Number in Refund Column!!!", True)

    '    End Try

    '    Return err
    'End Function

    Protected Sub disableCheckbox()

        Try
            Dim dblTotal As Double = 0
            Dim lblAmount As New TextBox
            For Each repitm As RepeaterItem In repInfo.Items
                Dim gvfee As New GridView
                gvfee = DirectCast(repitm.FindControl("gvFee"), GridView)

                For Each gvr As GridViewRow In gvfee.Rows

                    CType(gvr.FindControl("lblFSR_FEE_CHK_ID"), CheckBox).Enabled = False
                    CType(gvr.FindControl("txtAmountToRefund"), TextBox).Enabled = False
                Next

            Next

        Catch ex As Exception
            'lblError.Text = ex.Message
        End Try

    End Sub

    Sub CalculateTotals()
        Try
            Dim dblTotal As Double = 0
            Dim dblTotalF As Double = 0
            Dim dblBalTotalF As Double = 0
            Dim AmountToRefund As New TextBox
            For Each repitm As RepeaterItem In repInfo.Items
                Dim gvfee As New GridView
                gvfee = DirectCast(repitm.FindControl("gvFee"), GridView)

                For Each gvr As GridViewRow In gvfee.Rows

                    Dim cb1 As CheckBox = CType(gvr.FindControl("lblFSR_FEE_CHK_ID"), CheckBox)
                    If cb1.Checked Then
                        Dim txtAmountToRefund As TextBox = CType(gvr.FindControl("txtAmountToRefund"), TextBox)
                        'CType(gvr.FindControl("txtAmountToRefund"), TextBox).Enabled = True
                        'CType(gvr.FindControl("txtAmountToRefund"), TextBox).Text = CType(gvr.FindControl("lblAmount"), Label).Text

                        If Not txtAmountToRefund Is Nothing Then
                            If CDbl(txtAmountToRefund.Text > 0) Then
                                AmountToRefund = CType(gvr.FindControl("txtAmountToRefund"), TextBox)
                                dblTotal += CDbl(AmountToRefund.Text)
                                dblTotalF += CDbl(AmountToRefund.Text)
                            End If
                        End If
                    Else
                        CType(gvr.FindControl("txtAmountToRefund"), TextBox).Enabled = False
                        CType(gvr.FindControl("txtAmountToRefund"), TextBox).Text = 0
                    End If
                    Dim balAmount As Label = CType(gvr.FindControl("lblAmount"), Label)
                    dblBalTotalF += CDbl(balAmount.Text)
                Next
                If gvfee.Rows.Count > 0 Then
                    CType(gvfee.FooterRow.FindControl("lblAmountF"), Label).Text = dblBalTotalF.ToString("#,##0.00")
                    CType(gvfee.FooterRow.FindControl("lblReFAmountF"), Label).Text = dblTotalF.ToString("#,##0.00")
                End If
                dblTotalF = 0
                dblBalTotalF = 0

            Next
            txtTotal.Text = dblTotal.ToString("#,##0.00")
        Catch ex As Exception
            'lblError.Text = ex.Message
        End Try

    End Sub
    Sub ShowMessage(ByVal Message As String, ByVal bError As Boolean)
        If Message <> "" Then
            If bError Then
                lblError.CssClass = "alert-danger alert"
            Else
                lblError.CssClass = "alert-danger alert"
            End If
        Else
            lblError.CssClass = ""
        End If
        lblError.Text = Message
    End Sub
End Class
