﻿<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/ParentMaster.master" CodeFile="BookSalesOnline.aspx.vb" Inherits="Fees_BookSalesOnline" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphParent" runat="Server">

    <script src="../Scripts/jquery-1.9.1.js" type="text/javascript"></script>

    <link rel="stylesheet" type="text/css" href="../Scripts/jQuery-ui-1.10.3.css" />
    <link href="../CSS/Popup.css" rel="stylesheet" />
    <!-- Add fancyBox -->

    <style>
        @media (min-width:980px) {
            .PopUpMessageStyle {
                /*background-image: linear-gradient(to left, #eff1fd 0%, #dbe0ff 100%) !important;*/
                left: 15px !important;
                border: 1px solid #d5dbff !important;
                border-radius: 4px !important;
                padding: 5px !important;
                text-align: -webkit-center !important;
                box-shadow: 4px 6px 13px #cdc;
                background-color: #f1f1f1;
            }
        }

        @media (max-width:980px) {
            .PopUpMessageStyle {
                /*background-image: linear-gradient(to left, #eff1fd 0%, #dbe0ff 100%) !important;*/
                left: 15px !important;
                border: 1px solid #d5dbff !important;
                padding: 5px !important;
                text-align: -webkit-center !important;
                top: 100px !important;
                box-shadow: 4px 6px 13px #cdc;
                background-color: #f1f1f1;
            }
        }


        /*.RadSlider_Metro .rslHorizontal .rslItem {
            background-image:none!important;            
        }*/

        .RadSlider .rslHorizontal .rslItem, .RadSlider .rslHorizontal .rslLargeTick, .RadSlider .rslHorizontal .rslSmallTick {
            background-position: center !important;
        }

        .RadSlider_Silk {
            height: 80px !important;
        }

        .imgStyle {
            float: left;
        }

        .border0 {
            border: 0px !important;
        }


        .book-name {
            font-weight: 600;
            font-size: 16px;
        }

        .book-id {
            font-weight: 400;
            font-size: 14px;
        }

        .def-text {
            font-weight: 400;
            font-size: 16px;
        }

        .footer-row {
            background-color: #e8e5e5 !important;
        }

        .header-row {
            background-color: rgb(0, 97, 195) !important;
            color: #ffffff !important;
        }

        .listRadio {
            display: block;
            padding: 0 18px 0 0;
        }
        /*Book sales css style by Aji Rajan dated 16th April 2019*/

        .sib-profile {
            height: 100px;
            width: 80px;
            border: 1px solid rgba(0,0,0,0.2);
            border-radius: 6px;
            box-shadow: 0px 2px 10px rgba(0,0,0,0.18);
        }

        .form-control label {
            display: inline;
            color: rgba(0,0,0,0.5);
            margin-right: 10px;
        }

        .pagination {
            display: table-row !important;
        }

            .pagination table tr td {
                /*padding: 6px;
    border: 1px solid rgba(0,0,0,0.2);*/
                padding: 6px 0;
            }

                .pagination table tr td span, .pagination table tr td a {
                    padding: 6px;
                    border: 1px solid rgba(0,0,0,0.2);
                }

                .pagination table tr td a {
                    background-color: rgba(0,0,0,0.1);
                }

                    .pagination table tr td a:hover {
                        background-color: rgb(0, 97, 195);
                        color: #ffffff;
                    }

        .hr-shadow {
            box-shadow: 6px 2px 5px rgba(0,0,0,0.04);
        }

        .book-quantity {
            vertical-align: super;
            font-weight: bold;
        }

        .qty-img {
            vertical-align: bottom;
        }

        input:hover, textarea:hover, input:focus, textarea:focus {
            border-color: none !important;
            -webkit-box-shadow: none !important;
        }

        .txt-bold {
            font-weight: bold;
        }

        .txt-italic {
            font-style: italic;
        }


        /*auto complete extender css class goes here*/
        div .darkPanlAlumini {
            padding: 2px 10px;
            border: 1px solid rgba(0,0,0,0.08);
        }

        .darkPanlAlumini {
            background: rgba(235, 228, 228,1) !important;
            display: block;
            width: 40% !important;
            color: #333;
            cursor: pointer;
            border-bottom: 1px solid rgba(0,0,0,0.1);
        }

        span.darkPanlAlumini:hover {
            background: #63bcf7 !important;
            color: #ffffff;
        }

        .completionListElement {
            width: 100% !important;
            margin: auto;
            list-style: none;
            display: block;
            /*left: 230px !important;
            position: absolute !important;*/
            /*width: 350px !important;*/
            left: 0;
        }

            .completionListElement .listitem {
                cursor: pointer;
            }

            .completionListElement .highlightedListItem {
                background-color: rgba(0,0,0,0.2);
            }

            .radio-label label {
                color:#333333 !important;
            }
    </style>

    <script type="text/javascript" src="../Scripts/Fancybox/jquery.fancybox.js?v=2.1.5"></script>
    <script type="text/javascript" src="../Scripts/Fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
    <link rel="stylesheet" href="../Scripts/Fancybox/jquery.fancybox.css" type="text/css"
        media="screen" />
    <link rel="stylesheet" type="text/css" href="../css/slider.css" />
    <script type="text/javascript" src="../Scripts/PopupJQuery.js"></script>

    <script type="text/javascript" language="javascript">

        function sliderEndSlide(sender, args) {
            var sliderMin;
            //var sliderMax;

            sliderMin = sender.get_element().getAttribute('sliderClientMinValue');
            // sliderMax = sender.get_element().getAttribute('sliderClientMaxValue');

            if (sender.get_selectionStart() < sliderMin) {
                sender.set_selectionStart(sliderMin);
            }

            //if (sender.get_selectionEnd() > sliderMax) {
            //    sender.set_slectionEnd(sliderMax);
            //}
        }


        function OnClientLoaded(sender, args) {
            var slider = sender;
            if ($(window).width() < 979) {
                slider.set_orientation(1);
                slider.set_width(50);
                slider.set_height(600);
            }
        }


        function ShowInfoW(w, h) {
            $.fancybox({
                href: '#testpopup',
                maxHeight: 600,
                fitToView: true,
                width: w,
                height: h,
                padding: 0,
                'titleShow': false,
                'transitionIn': 'elastic',
                'transitionOut': 'elastic',
                closeEffect: 'fade',
                helpers: {
                    overlay: { closeClick: false } // prevents closing when clicking OUTSIDE fancybox 
                },
            });

            return false;
        }



    </script>

    <script type="text/javascript" language="javascript">
        window.format = function (b, a) {
            if (!b || isNaN(+a)) return a; var a = b.charAt(0) == "-" ? -a : +a, j = a < 0 ? a = -a : 0, e = b.match(/[^\d\-\+#]/g), h = e && e[e.length - 1] || ".", e = e && e[1] && e[0] || ",", b = b.split(h), a = a.toFixed(b[1] && b[1].length), a = +a + "", d = b[1] && b[1].lastIndexOf("0"), c = a.split("."); if (!c[1] || c[1] && c[1].length <= d) a = (+a).toFixed(d + 1); d = b[0].split(e); b[0] = d.join(""); var f = b[0] && b[0].indexOf("0"); if (f > -1) for (; c[0].length < b[0].length - f;) c[0] = "0" + c[0]; else +c[0] == 0 && (c[0] = ""); a = a.split("."); a[0] = c[0]; if (c = d[1] && d[d.length -
        1].length) { for (var d = a[0], f = "", k = d.length % c, g = 0, i = d.length; g < i; g++) f += d.charAt(g), !((g - k + 1) % c) && g < i - c && (f += e); a[0] = f } a[1] = b[1] && a[1] ? h + a[1] : ""; return (j ? "-" : "") + a[0] + a[1]
        };

        var myIds = new Array();
        var myDescrs = new Array();



        function ClientItemSelected(sender, e) {
            var row = e._item.parentElement;
            //alert(row.getElementsByTagName('span')[0].textContent);
            //alert(row.getElementsByTagName('span')[1].textContent);

            var hf_Item_ID = sender.get_element().id.replace("txtItmSelect", "hf_Item_ID");
            //alert(2);
            $get(hf_Item_ID).value = row.getElementsByTagName('span')[0].textContent;
            //alert(3);
            $get(sender.get_element().id).value = row.getElementsByTagName('span')[1].textContent;
            //alert(4);
            //var index = sender._selectIndex;       
            //var hf_Item_ID = sender.get_element().id.replace("txtItmSelect", "hf_Item_ID");
            //$get(hf_Item_ID).value = myIds[index];
            //$get(sender.get_element().id).value = myDescrs[index];

        }

        function MultiSelect(sender, e) {
            var comletionList = sender.get_completionList();


            for (i = 0; i < comletionList.childNodes.length; i++) {
                var itemobj = new Object();
                var _data = comletionList.childNodes[i]._value;

                itemobj.descr = _data.substring(_data.lastIndexOf('|') + 1);
                comletionList.childNodes[i]._value = itemobj.name;

                _data = _data.substring(0, _data.lastIndexOf('|'));
                itemobj.itmid = _data.substring(_data.lastIndexOf('|') + 1); // parse name as item value



                if (itemobj.descr) {
                    myIds[i] = itemobj.itmid; // id used in updating hidden file
                    myDescrs[i] = itemobj.descr;

                }

                //comletionList.childNodes[i].innerHTML = "<div class='cloumnspan' style='width:80%;float:left'>" + itemobj.descr + "</div>" + "<div class='cloumnspan' style='width:20%;'>" + itemobj.itmid + "</div>";
                //comletionList.childNodes[i].innerHTML = "<div class='cloumnspan' style='width:100%;float:left'>" + itemobj.descr + "</div>";
                comletionList.childNodes[i].innerHTML = "<div><span class='darkPanlAlumini' style='display:none' align='left'> " + itemobj.itmid + " </span><span class='darkPanlAlumini' align='left'> " + itemobj.descr + " </span></div>"

            }
        }


        function formatme(me) {
            document.getElementById(me).value = format("#,##0.00", document.getElementById(me).value);
        }
        function Numeric_Only() {
            //alert(event.keyCode)
            if (event.keyCode < 46 || event.keyCode > 57 || (event.keyCode > 90 & event.keyCode < 97)) {
                if (event.keyCode == 13 || event.keyCode == 46)
                { return false; }
                event.keyCode = 0
            }
        }
    </script>
    <%-- <script>
        if ($(window).width() < 979) {
            if ($(location).attr("href").indexOf("FeeCollectionOnlineSibling_M.aspx") == -1) {
                window.location = "\\GEMSPARENTBETA\\fees\\FeeCollectionOnlineSibling_M.aspx";
            }
        }
        if ($(window).width() > 979) {
            if ($(location).attr("href").indexOf("FeeCollectionOnlineSibling.aspx") == -1) {
                window.location = "\\GEMSPARENTBETA\\fees\\FeeCollectionOnlineSibling.aspx";
            }
        }
    </script>--%>


    <div class="content margin-top30 margin-bottom60">
        <div class="container">
            <div class="row">

                <!-- Posts Block -->
                <div class="posts-block col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="my-account">

                        <div class="bottom-padding">
                            <div class="title-box">
                                <h3>Book Sales</h3>
                            </div>
                            <!-- Table  -->
                            <div class="table-responsive">

                                <%If lblError.Text <> "" Then%>
                                <asp:Label ID="lblError" runat="server" EnableViewState="False"></asp:Label>
                                <asp:HiddenField ID="hidAmountTotal" runat="server" />
                                <%End If%>

                                <table align="center" style="width: 100%;" class="table table-striped table-bordered table-responsive text-left my-orders-table">



                                    <tr style="display: none;">
                                        <th align="left" class="tdfields" width="20%">Date
                                        </th>
                                        <td align="left" class="tdfields">
                                            <asp:Label ID="lblDate" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr style="display: none;">
                                        <th align="left" class="tdfields">School
                                        </th>
                                        <td align="left" class="tdfields">
                                            <asp:Label ID="lblschool" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr style="display: none;">
                                        <th class="tdfields">Select a Payment Gateway
                <br />
                                            <asp:LinkButton ID="lMoreInfo" runat="server" Font-Bold="True" Font-Underline="True"
                                                Text="More Info" ForeColor="Blue">
                                            </asp:LinkButton>
                                        </th>
                                        <td align="left" style="height: 21px" valign="middle">
                                            <asp:RadioButtonList ID="rblPaymentGateway" runat="server" RepeatDirection="Horizontal"
                                                RepeatLayout="Flow" AutoPostBack="True" RepeatColumns="2">
                                            </asp:RadioButtonList>
                                            <asp:HiddenField ID="HFCardCharge" runat="server" />
                                        </td>
                                    </tr>
                                    <tr id="tr_AddFee" runat="server">
                                        <th align="left" colspan="2" class="trSub_Header">Book Details
                <%= IIf(Session("BSU_CURRENCY") Is Nothing, "", "(Amount In " & Session("BSU_CURRENCY") & ")")%>
                                            <%--<asp:label id="lblCurrency" runat="server" Text="" ></asp:label> --%>
                                            <%--<div>
                    <asp:LinkButton ID="lnkMoreFee" runat="server" OnClientClick="HideAddFee(); return false;"
                        ForeColor="White" Visible="False">Add More Fee Head(s)</asp:LinkButton>&nbsp;
                </div>--%>
                                        </th>
                                    </tr>
                                    <tr style="display: none;" visible="false">
                                        <td colspan="2"></td>
                                    </tr>
                                    <tr>
                                        <td align="left" colspan="2">
                                            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                <tr>
                                                    <td align="left" colspan="2">
                                                        <asp:Repeater ID="repInfo" runat="server">
                                                            <HeaderTemplate>
                                                                <table width="100%" style="padding: 0;">
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <tr>
                                                                    <td colspan="3">&nbsp;</td>
                                                                </tr>
                                                                <tr class="trSub_Header_Small">
                                                                    <th colspan="2">
                                                                        <asp:Label ID="lbSName" runat="server" Text='<%# Bind("SNAME") %>'></asp:Label>&nbsp;[<asp:Label ID="lbSNo" runat="server" Text='<%# Bind("STU_NO")%>'></asp:Label>]
                                                                    </th>
                                                                    <th align="right" class="text-right">
                                                                        <div style="clear: both;">
                                                                            <asp:Label ID="lbGrade" runat="server" Text='<%# Bind("GRM_DISPLAY")%>'></asp:Label>-<asp:Label ID="lbSection" runat="server" Text='<%# Bind("SCT_DESCR")%>'></asp:Label>
                                                                            <%--  <asp:LinkButton ID="lnkViewFeeSetup" Text="View Fee Schedule" CssClass="linkFee" runat="server"></asp:LinkButton>--%>
                                                                            <asp:HiddenField ID="hfSTU_ID" Value='<%# Bind("STU_ID") %>' runat="server" />
                                                                            <asp:HiddenField ID="hfSTU_GRD_ID" Value='<%# Bind("STU_GRD_ID") %>' runat="server" />
                                                                            <asp:HiddenField ID="hfSTU_FEE_ID" Value='<%# Bind("STU_FEE_ID")%>' runat="server" />
                                                                            <asp:HiddenField ID="hfSTU_ACD_ID" Value='<%# Bind("STU_ACD_ID")%>' runat="server" />
                                                                        </div>
                                                                    </th>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="3">&nbsp;</td>
                                                                </tr>
                                                                <tr id="trgvrerror" runat="server" visible="false">
                                                                    <td colspan="3" align="center">
                                                                        <asp:Label ID="lblGvrError" CssClass="linkText" Text="" runat="server"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr align="left" style="border: none 0px !important;">
                                                                    <td valign="top" colspan="3" align="left">
                                                                        <table width="100%" style="border-collapse: collapse !important; border-spacing: 0 !important; padding: 0 !important; border: none 0px !important;">
                                                                            <tr>
                                                                                <td valign="top" align="left" style="width: 150px;">
                                                                                    <asp:Image ID="imgEmpImage" runat="server" CssClass="sib-profile" ImageUrl='<%# Bind("PHOTOPATH") %>' ToolTip='<%# Bind("SNAME") %>' />
                                                                                </td>
                                                                                <td align="left" width="90%">
                                                                                    <h4>Select the Set</h4>
                                                                                    <%--<asp:DropDownList ID="drpBookSets" runat="server" CssClass="form-control" Width="50%" >
                                                                                                                <asp:ListItem Text="Set 1" Selected ="True"></asp:ListItem>
                                                                                                                <asp:ListItem Text="Set 2" Selected ="false"></asp:ListItem>
                                                                                                                <asp:ListItem Text="Set 3" Selected ="false"></asp:ListItem>
                                                                                                            </asp:DropDownList>--%>
                                                                                    <asp:CheckBoxList ID="rdbBookSets" Width="100%" runat="server" CssClass="form-control border0" RepeatDirection="Horizontal" RepeatColumns="3" OnSelectedIndexChanged="rdbBookSets_SelectedIndexChanged" AutoPostBack="true">
                                                                                    </asp:CheckBoxList>
                                                                                </td>
                                                                                <td align="right" width="10%" style="display: none;">
                                                                                    <asp:Button ID="btnSelectBook" runat="server" CssClass="btn btn-info" Text="Show Set Items" OnClick="btnSelectBook_Click" Visible="false" /></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="3">&nbsp;</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="left" width="100%" colspan="3">

                                                                                    <!-- Accordion starts -->
                                                                                    <%--  <div class="accordionMod panel-group">
                                                                                        <div class="accordion-item">
                                                                                            <h4 class="accordion-toggle">Select Books</h4>
                                                                                            <section class="accordion-inner panel-body">--%>

                                                                                    <table width="100%">
                                                                                        <%--<tr>
                                                                                                        <th align="left" width="20%">
                                                                                                            <asp:Label ID="lblBookDescr" runat="server" CssClass="title" Text="Select Your SET"></asp:Label></th>
                                                                                                        
                                                                                                    </tr>--%>

                                                                                        <tr>
                                                                                            <td width="100%" colspan="3">
                                                                                                <asp:Panel ID="GridScroll" runat="server" CssClass="grid_scroll">
                                                                                                    <div id="DivRoot" align="left">
                                                                                                        <div style="overflow: hidden;" class="header_row_grid" id="DivHeaderRow" runat="server">
                                                                                                        </div>
                                                                                                        <div style="overflow-y: scroll;" class="main_content_grid" id="DivMainContent" runat="server">

                                                                                                            <asp:GridView ID="grdSAL" runat="server" AutoGenerateColumns="False" PageSize="5" Width="100%" ShowFooter="true" AllowPaging="false"
                                                                                                                CaptionAlign="Top" class="table table-striped table-bordered table-responsive text-left my-orders-table " DataKeyNames="ID" OnPageIndexChanging="grdSAL_PageIndexChanging">
                                                                                                                <Columns>
                                                                                                                    <asp:TemplateField Visible="True" HeaderText="Sr.No.">
                                                                                                                        <ItemTemplate>
                                                                                                                            <asp:Label ID="lbl_ID" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                                                                                                                        </ItemTemplate>
                                                                                                                        <HeaderStyle CssClass="fixed_row_header"  />
                                                                                                                    </asp:TemplateField>
                                                                                                                    <asp:TemplateField Visible="False" HeaderText="S.No.">
                                                                                                                        <ItemTemplate>
                                                                                                                            <asp:Label ID="lbl_ITMID" runat="server" Text='<%# Bind("ITEM_ID")%>'></asp:Label>
                                                                                                                        </ItemTemplate>
                                                                                                                        <HeaderStyle CssClass="fixed_row_header" />
                                                                                                                    </asp:TemplateField>
                                                                                                                    <asp:TemplateField Visible="False" HeaderText="S.No.">
                                                                                                                        <ItemTemplate>
                                                                                                                            <asp:Label ID="lbl_PID" runat="server" Text='<%# Bind("STU_NO")%>'></asp:Label>
                                                                                                                        </ItemTemplate>
                                                                                                                        <HeaderStyle CssClass="fixed_row_header" />
                                                                                                                    </asp:TemplateField>
                                                                                                                    <asp:TemplateField Visible="False">
                                                                                                                        <ItemTemplate>
                                                                                                                            <asp:Label ID="lblSAD_ID" runat="server" Text='<%# Bind("BSH_ID")%>'></asp:Label>
                                                                                                                        </ItemTemplate>
                                                                                                                        <HeaderStyle CssClass="fixed_row_header" />
                                                                                                                    </asp:TemplateField>
                                                                                                                    <asp:TemplateField Visible="True" HeaderText="Book Name">
                                                                                                                        <ItemTemplate>
                                                                                                                            <asp:Label ID="lbl_ITEM_DESCR" CssClass="txt-bold1" runat="server" Text='<%# Bind("ITEM_DESCR") %>' Style="display: none;"></asp:Label>
                                                                                                                            <asp:Label ID="lbl_BOOK_NAME" runat="server" CssClass="txt-italic" Text='<%# Bind("BOOK_NAME") %>'></asp:Label>
                                                                                                                        </ItemTemplate>
                                                                                                                        <ItemStyle CssClass="txt-bold1" />
                                                                                                                        <HeaderStyle CssClass="fixed_row_header" />
                                                                                                                    </asp:TemplateField>
                                                                                                                    <asp:TemplateField Visible="False" HeaderText="ISBN">
                                                                                                                        <ItemTemplate>
                                                                                                                            <asp:Label ID="lbl_ISBN" runat="server" Text='<%# Bind("ISBN")%>'></asp:Label>
                                                                                                                        </ItemTemplate>
                                                                                                                        <HeaderStyle CssClass="fixed_row_header" />
                                                                                                                    </asp:TemplateField>

                                                                                                                    <asp:TemplateField Visible="True" HeaderText="Rate" ItemStyle-CssClass="text-right">
                                                                                                                        <ItemTemplate>
                                                                                                                            <asp:Label ID="lbl_PRICE" runat="server" Text='<%# Bind("PRICE")%>'></asp:Label>
                                                                                                                        </ItemTemplate>
                                                                                                                        <HeaderStyle CssClass="fixed_row_header text-center" />
                                                                                                                    </asp:TemplateField>

                                                                                                                    <asp:TemplateField Visible="True" HeaderText="Qty" ItemStyle-CssClass="text-center" ItemStyle-VerticalAlign="Middle">
                                                                                                                        <ItemTemplate>
                                                                                                                            <asp:ImageButton ID="btnMinus" runat="server" Text="-" OnClick="btnMinus_Click" CssClass="qty-img" ImageUrl="/images/minus_book.png" Width="36px" Style='<%# String.Format("{0}", Eval("MINUS_BTN_CSS"))%>' />
                                                                                                                            <asp:Label ID="lbl_QTY" runat="server" Text='<%# Bind("QTY")%>' CssClass="book-quantity"></asp:Label>
                                                                                                                            <asp:ImageButton ID="btnPlus" runat="server" Text="+" OnClick="btnPlus_Click" CssClass="qty-img" ImageUrl="/images/add_book.png" Width="36px" Style='<%# String.Format("{0}", Eval("PLUS_BTN_CSS"))%>' />
                                                                                                                        </ItemTemplate>
                                                                                                                        <HeaderStyle Width="140px" CssClass="fixed_row_header text-center" />
                                                                                                                        <ItemStyle CssClass="text-primary text-center" />
                                                                                                                    </asp:TemplateField>

                                                                                                                    <asp:TemplateField Visible="false" HeaderText="Tax Amount" ItemStyle-CssClass="text-right">
                                                                                                                        <ItemTemplate>
                                                                                                                            <asp:Label ID="lbl_TAX_AMOUNT" runat="server" Text='<%# Bind("TAX_AMOUNT")%>'></asp:Label>
                                                                                                                        </ItemTemplate>
                                                                                                                        <HeaderStyle CssClass="fixed_row_header text-center" />
                                                                                                                    </asp:TemplateField>

                                                                                                                    <asp:TemplateField Visible="true" HeaderText="Tax Amount" ItemStyle-CssClass="text-right">
                                                                                                                        <ItemTemplate>
                                                                                                                            <asp:Label ID="lbl_SHOW_TAX_AMOUNT" runat="server" Text='<%# Bind("TAX_AMOUNT")%>'></asp:Label>
                                                                                                                        </ItemTemplate>
                                                                                                                        <HeaderStyle CssClass="fixed_row_header text-center" />
                                                                                                                    </asp:TemplateField>

                                                                                                                    <asp:TemplateField Visible="true" HeaderText="Tax Type" ItemStyle-CssClass="text-left">
                                                                                                                        <ItemTemplate>
                                                                                                                            <asp:Label ID="lbl_TAX_TYPE" runat="server" Text='<%# Bind("TAX_DESCR")%>'></asp:Label>
                                                                                                                        </ItemTemplate>
                                                                                                                        <HeaderStyle CssClass="fixed_row_header text-center" />
                                                                                                                    </asp:TemplateField>

                                                                                                                    <asp:TemplateField Visible="True" HeaderText="Net Amount" ItemStyle-CssClass="text-right">
                                                                                                                        <ItemTemplate>
                                                                                                                            <asp:Label ID="lbl_NET_AMOUNT" runat="server" Text='<%# Bind("NET_AMOUNT")%>'></asp:Label>
                                                                                                                        </ItemTemplate>
                                                                                                                        <HeaderStyle CssClass="fixed_row_header text-center" />
                                                                                                                    </asp:TemplateField>

                                                                                                                </Columns>
                                                                                                                <PagerStyle CssClass="pagination" />
                                                                                                                <FooterStyle CssClass="pagination" />
                                                                                                            </asp:GridView>
                                                                                                        </div>
                                                                                                        <div id="DivFooterRow" style="overflow: hidden" runat="server">
                                                                                                        </div>
                                                                                                </asp:Panel>
                                                                                                <br />
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td align="left" colspan="2" class="tdfields" width="75%">

                                                                                                <div id="SelectItem" runat="server">
                                                                                                    <asp:TextBox ID="txtItmSelect" CssClass="form-control" runat="server" Visible="true" Style="width: 75% !important;">
                                                                                                    </asp:TextBox>
                                                                                                    <ajaxToolkit:TextBoxWatermarkExtender ID="TBWE3" runat="server"
                                                                                                    TargetControlID="txtItmSelect"
                                                                                                    WatermarkText="Please type book name and click add more item"
                                                                                                    WatermarkCssClass="watermarked" />
                                                                                                    <ajaxToolkit:AutoCompleteExtender ID="acBSU" runat="server" BehaviorID='<%# Bind("STU_ID")%>' ContextKey='<%# Bind("STU_ID")%>'
                                                                                                        OnClientPopulated="MultiSelect" CompletionListCssClass="completionListElement"
                                                                                                        CompletionListItemCssClass="listItem" FirstRowSelected="false" CompletionListHighlightedItemCssClass="highlightedListItem"
                                                                                                        OnClientItemSelected="ClientItemSelected" CompletionSetCount="5" EnableCaching="false"
                                                                                                        MinimumPrefixLength="1" UseContextKey="true" ServiceMethod="GetItmDescr" ServicePath="BookSalesOnline.aspx"
                                                                                                        TargetControlID="txtItmSelect">
                                                                                                    </ajaxToolkit:AutoCompleteExtender>
                                                                                                    <asp:HiddenField ID="hf_Item_ID" runat="server" />
                                                                                                    <asp:Button ID="btnAdd" runat="server" CssClass="btn btn-info" Text="Add More Item" OnClick="btnAdd_Click" />
                                                                                                </div>
                                                                                            </td>

                                                                                            <td align="right" class="tdfields" width="25%">
                                                                                                <table width="100%">
                                                                                                    <tbody>
                                                                                                        <tr>
                                                                                                            <td width="50%" align="left" class="tdfields">
                                                                                                                <h4 style="margin: 0px !important;">Total </h4>
                                                                                                            </td>
                                                                                                            <td width="50%" align="left">
                                                                                                                <h4>
                                                                                                                    <asp:Label ID="txtSubTotal" runat="server" CssClass="form-control text-right" Style="display: inline-block; width: 100%" Text="0.00"></asp:Label></h4>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </tbody>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td align="left" colspan="2" class="tdfields"></td>
                                                                                            <td align="right" class="tdfields">
                                                                                                <table width="100%">
                                                                                                    <tbody>
                                                                                                        <tr>
                                                                                                            <td width="50%" align="left">
                                                                                                                <h4 style="margin: 0px !important;">VAT Total </h4>
                                                                                                            </td>
                                                                                                            <td width="50%" align="left">
                                                                                                                <h4>
                                                                                                                    <asp:Label ID="txtSubVATTotal" runat="server" CssClass="form-control text-right" Style="display: inline-block; width: 100%" Text="0.00"></asp:Label></h4>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </tbody>
                                                                                                </table>
                                                                                            </td>

                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td align="left" colspan="2" class="tdfields"></td>
                                                                                            <td align="right" class="tdfields">
                                                                                                <table width="100%">
                                                                                                    <tbody>
                                                                                                        <tr>
                                                                                                            <td width="50%" align="left">
                                                                                                                <h4 style="margin: 0px !important;">Grand Total </h4>
                                                                                                            </td>
                                                                                                            <td width="50%" align="left">
                                                                                                                <h4>
                                                                                                                    <asp:Label ID="txtGrandTotal" runat="server" CssClass="form-control text-right" Style="display: inline-block; width: 100%" Text="0.00"></asp:Label></h4>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </tbody>
                                                                                                </table>
                                                                                            </td>

                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td colspan="3">
                                                                                                <hr class="hr-shadow" />
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                    <%-- </section>
                                                                                        </div>

                                                                                    </div>--%>
                                                                                    <!-- Accordion ends -->

                                                                                </td>
                                                                            </tr>
                                                                    </td>
                                                                </tr>
                                                                </table>
                                                                    </td>
                                                                </tr>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                </table>
                                                            </FooterTemplate>
                                                        </asp:Repeater>
                                                    </td>
                                                </tr>



                                                <tr class="tdblankAll">
                                                    <td align="left" colspan="2">
                                                       <h4 style="margin: 0px !important;">  Select delivery mode</h4>
                                                       <asp:RadioButtonList ID="rbl_deliverytype" runat="server" CssClass="form-control border0 font-weight-bold radio-label"  RepeatDirection="Horizontal" AutoPostBack="True">
                                                            <%--<asp:ListItem Text="Collect the Books from School Personally" Value="0"></asp:ListItem>
                                                            <asp:ListItem Text="Deliver Books To Child" Value="1"></asp:ListItem>--%>
                                                            <%--   <asp:ListItem Text="Deliver Books By Courier" Value="2"></asp:ListItem>--%>
                                                        </asp:RadioButtonList>
                                                    </td>

                                                </tr>
                                                <tr class="tdblankAll">
                                                    <td align="left" colspan="2">&nbsp;</td>

                                                </tr>

                                                <tr class="tdblankAll" id="id_colMessage" runat="server">
                                                    <td align="left" colspan="2">
                                                        <h4>
                                                            <asp:Label ID="lblCollectionMessage" CssClass="form-control" Style="text-align: left;" runat="server" Text="" Visible="true" Enabled="false" Width="67%">
                                                            </asp:Label></h4>
                                                        <asp:HiddenField ID="hf_CollectionMessageID" runat="server" Value="0" />
                                                    </td>

                                                </tr>
                                                <tr class="tdblankAll" id="id_courier" runat="server">

                                                    <td align="left" width="100%" valign="middle" class="tdfields" colspan="2">
                                                        <table width="100%">
                                                            <tbody>
                                                                <tr class="tdblankAll">
                                                                    <td width="20%" align="left">
                                                                        <h4 style="margin: 0px !important;">Parent Name </h4>
                                                                    </td>
                                                                    <td width="30%" align="left">
                                                                        <h4>
                                                                            <asp:TextBox ID="txt_contactperson" CssClass="form-control" Style="text-align: left;" runat="server" Text="" Visible="true">
                                                                            </asp:TextBox></h4>

                                                                    </td>
                                                                    <td width="20%" align="right">
                                                                        <h4 style="margin: 0px !important;">City Name </h4>
                                                                    </td>
                                                                    <td width="30%" align="right">
                                                                        <h4>
                                                                            <asp:TextBox ID="txt_cityname" CssClass="form-control" Style="text-align: left;" runat="server" Text="" Visible="true">
                                                                            </asp:TextBox></h4>

                                                                    </td>
                                                                </tr>
                                                                <tr class="tdblankAll">
                                                                    <td width="20%" align="left">
                                                                        <h4 style="margin: 0px !important;">Mobile No </h4>
                                                                    </td>
                                                                    <td width="30%" align="left">
                                                                        <h4>
                                                                            <asp:TextBox ID="txt_contactno1" CssClass="form-control" Style="text-align: left;" runat="server" Text="" Visible="true">
                                                                            </asp:TextBox></h4>

                                                                    </td>
                                                                    <td width="20%" align="right">
                                                                        <h4 style="margin: 0px !important;">Land Phone No </h4>
                                                                    </td>
                                                                    <td width="30%" align="right">
                                                                        <h4>
                                                                            <asp:TextBox ID="txt_contactno2" CssClass="form-control" Style="text-align: left;" runat="server" Text="" Visible="true">
                                                                            </asp:TextBox></h4>

                                                                    </td>
                                                                </tr>
                                                                <%-- <tr class="tdblankAll">
                                                                    <td align="left" colspan="4">&nbsp;</td>

                                                                </tr>--%>
                                                                <tr class="tdblankAll">
                                                                    <td width="20%" align="left">
                                                                        <h4 style="margin: 0px !important;">Delivery Address </h4>
                                                                    </td>
                                                                    <td width="80%" align="left" colspan="3">
                                                                        <h4>
                                                                            <asp:TextBox ID="txt_address" TextMode="MultiLine" Rows="3" CssClass="form-control" Style="text-align: left; width: 100% !important; height: 75px !important; resize: none;" runat="server" Text="" Visible="true">
                                                                            </asp:TextBox></h4>

                                                                    </td>
                                                                </tr>
                                                                <tr class="tdblankAll">
                                                                    <td width="20%" align="left"></td>
                                                                    <td width="30%" align="left"></td>
                                                                    <td width="20%" align="right">
                                                                        <h4 style="margin: 0px !important;">Courier Charge </h4>
                                                                    </td>
                                                                    <td width="30%" align="right">
                                                                        <h4>
                                                                            <asp:Label ID="txt_courier_charge" CssClass="form-control" Style="text-align: right;" runat="server" Text="" Visible="true" Enabled="false" Width="67%">
                                                                            </asp:Label></h4>
                                                                    </td>

                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>

                                                </tr>

                                                <tr class="tdblankAll">
                                                    <td width="60%" class="tdfields"></td>
                                                    <td align="right" width="40%" valign="middle" class="tdfields">
                                                        <table width="100%">
                                                            <tbody>
                                                                <tr>
                                                                    <td width="50%" align="left">
                                                                        <h4 style="margin: 0px !important;">Net Payable </h4>
                                                                    </td>
                                                                    <td width="50%" align="left">
                                                                        <asp:TextBox ID="txtTotal" CssClass="form-control" Style="text-align: right" runat="server" Text="0.00" TabIndex="152" Visible="false">
                                                                        </asp:TextBox>
                                                                        <asp:Label ID="lblTotal" CssClass="form-control text-right" runat="server" Text="0.00" TabIndex="152" Width="100%">    </asp:Label>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>

                                                <tr class="tdblankAll">
                                                    <td align="left" colspan="2" class="tdfields"></td>

                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr class="tdblankAll">
                                        <td align="center" colspan="2" class="tdblankAll">
                                            <asp:Button ID="btnSave" runat="server" CssClass="btn btn-info" TabIndex="155" Text="Confirm & Proceed" />
                                            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="btn btn-info"
                                                Text="Cancel" TabIndex="158" />
                                            <asp:Button ID="btnShow" runat="server" CausesValidation="False" Visible="false"
                                                CssClass="btn btn-info" Text="Show" TabIndex="158" />
                                    </tr>
                                    <tr class="tdblankAll" id="tr_continue" runat="server" visible="false">
                                        <td align="center" colspan="2" class="tdblankAll">
                                            <span class="ui-icon ui-icon-info" style="float: left; margin: 0 7px 7px 0;"></span>
                                            <asp:Label ID="lblPayMsg" runat="server"
                                                EnableViewState="False"></asp:Label><br />
                                            <br />
                                            <br />
                                            <span class="ui-icon ui-icon-notice" style="float: left; margin: 0 7px 7px 0;"></span>
                                            &nbsp
            <asp:Label ID="lblPayMsgIE" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr id="tr_comment" runat="server" class="tdblankAll" visible="false">
                                        <td align="left" style="height: 18px; font-size: 13px; color: #0026ff;" colspan="2"
                                            class="tdblankAll">Please enter the amount paying now and Click Proceed.</td>
                                    </tr>
                                </table>

                                <span class="anim">
                                    <label id="test" runat="server">
                                    </label>
                                </span>

                                <div id="testpopup" class="darkPanelM anim" runat="server" style="display: none; z-index: 3000 !important;">
                                    <div id="divboxpanelconfirm" runat="server" style="height: 50%; width: 45%; margin-left: 30%; margin-top: 10%;" class="darkPanelMTop">
                                        <div class="holderInner" style="height: 90%; width: 98%;">
                                            <center>
                            <table cellpadding="5" cellspacing="2" border="0" style="height: 90% !important; width: 100% !important; overflow-y: scroll;"
                                class="tableNoborder">
                                <tr class="trSub_Header">
                                    <td>
                                        <span style="float: left; margin: 0 7px 50px 10px;"></span>
                                        <h4>Confirm & Proceed</h4>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top">
                                        <span  style="float: left; margin: 0 7px 50px 10px;"></span>
                                        <asp:Label ID="lblMsg"  runat="server"></asp:Label><br />   <%--Style="font-family: Verdana, Geneva, Tahoma, sans-serif; font-size: 12px;"--%>
                                        <br />
                                        <br />
                                        
                                <asp:Label ID="lblMsg2" runat="server" class="alert-success alert" style="float: left; margin: 0 7px 50px 10px;"></asp:Label>  <%--BackColor="Beige" Font-Names="Verdana" ForeColor="Navy"  Font-Size="10pt"--%>
                                         <br />
                                        <br />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">
                                        <asp:Button ID="btnProceed" runat="server" CausesValidation="False" CssClass="btn btn-info"
                                            Text="Confirm & Proceed" />
                                        <asp:Button ID="btnSkip" runat="server" CausesValidation="False" CssClass="btn btn-info"
                                            Text="Cancel"  />

                                       
                                    </td>
                                </tr>
                            </table>
                        </center>
                                        </div>
                                    </div>
                                </div>


                            </div>
                            <!-- /Table  -->
                        </div>

                    </div>
                </div>
                <!-- /Posts Block -->

            </div>
        </div>
    </div>

    <script type="text/javascript">

        function MakeStaticHeader(gridId, HeaderRowId, MainContentId, FooterRowId, height, width, headerHeight, isFooter) {
            //alert(gridId);   
            var tbl = document.getElementById(gridId);
            if (tbl) {

                var DivHR = document.getElementById(HeaderRowId);
                var DivMC = document.getElementById(MainContentId);
                var DivFR = document.getElementById(FooterRowId);

                //*** Set divheaderRow Properties ****
                DivHR.style.height = headerHeight + 'px';
                DivHR.style.width = (parseInt(width) - 16) + 'px';
                DivHR.style.position = 'relative';
                DivHR.style.top = '0px';
                DivHR.style.zIndex = '15';
                DivHR.style.verticalAlign = 'top';
                //*** Set divMainContent Properties ****
                DivMC.style.width = width + 'px';
                DivMC.style.height = height + 'px';
                DivMC.style.position = 'relative';
                DivMC.style.top = -headerHeight + 'px';
                DivMC.style.zIndex = '1';
                //*** Set divFooterRow Properties ****
                DivFR.style.width = (parseInt(width) - 16) + 'px';
                DivFR.style.position = 'relative';
                DivFR.style.top = -headerHeight + 'px';
                DivFR.style.verticalAlign = 'top';
                DivFR.style.paddingtop = '2px';
                if (isFooter) {
                    var tblfr = tbl.cloneNode(true);
                    tblfr.removeChild(tblfr.getElementsByTagName('tbody')[0]);
                    var tblBody = document.createElement('tbody');
                    tblfr.style.width = '100%';
                    tblfr.cellSpacing = "0";
                    tblfr.border = "0px";
                    tblfr.rules = "none";
                    //*****In the case of Footer Row *******
                    tblBody.appendChild(tbl.rows[tbl.rows.length - 1]);
                    tblfr.appendChild(tblBody);
                    DivFR.appendChild(tblfr);
                }
                //****Copy Header in divHeaderRow****
                DivHR.appendChild(tbl.cloneNode(true));
            }
        }



    </script>
    <script src="Scripts/jquery-1.4.1.min.js" type="text/javascript"></script>
    <script src='<%= ResolveUrl("../js/ScrollableGridPlugin.js")%>'></script>
    <style>
        .grid_scroll {
            /*overflow: auto;*/
            height: 430px;
            /*overflow-y: scroll;*/
        }

        .grid_scroll0 {
            height: 230px;
        }

        .fixed_row_header {
            height: 49px;
        }

        .main_content_grid0 {
            overflow-y: scroll;
            /* width: 1000px; */
            width: 100% !important;
            height: 200px !important;
            position: relative;
            top: -60px;
            z-index: 1;
        }

        .main_content_grid {
            overflow-y: scroll;
            /* width: 1000px; */
            width: 100% !important;
            height: 400px !important;
            position: relative;
            top: -60px;
            z-index: 1;
        }

        .header_row_grid {
            overflow: hidden;
            height: 60px;
            width: 98.5% !important;
            top: 0px;
            z-index: 15;
            position: relative;
            vertical-align: top;
        }
    </style>

</asp:Content>







