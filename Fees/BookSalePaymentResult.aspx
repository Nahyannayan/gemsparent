﻿<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/ParentMaster.master" CodeFile="BookSalePaymentResult.aspx.vb" Inherits="Fees_BookSalePaymentResult" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphParent" runat="Server">

    <script src="../Scripts/jquery-1.9.1.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.10.2.min.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-ui-1.10.2.min.js" type="text/javascript"></script>
    <!-- Add fancyBox -->
    <script type="text/javascript" src="../Scripts/Fancybox/jquery.fancybox.js?v=2.1.5"></script>
    <script type="text/javascript" src="../Scripts/Fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
    <link rel="stylesheet" href="../Scripts/Fancybox/jquery.fancybox.css" type="text/css" media="screen" />
    <script type="text/javascript" src="../Scripts/PopupJQuery.js"></script>
    <script type="text/javascript" language="javascript">
        function CheckForPrint() {
            <%--if (document.getElementById('<%=h_Recno.ClientID %>').value != '') {
                var frmReceipt = "feereceipt.aspx";
                if ($("#<%=hfTaxable.ClientID%>").val() == "1")
                    frmReceipt = "FeeReceipt_TAX.aspx";
                else
                    frmReceipt = "feereceipt.aspx";
                showModelessDialog(frmReceipt + '?type=REC&id=' + document.getElementById('<%=h_Recno.ClientID %>').value, '', "dialogWidth: 800px; dialogHeight: 700px; help: no; resizable: no; scroll: yes; status: no; unadorned: no;");
                return false;
            }
            else
                alert('Sorry. There is some problem with the transaction');--%>
        }
    </script>


    <div class="content margin-top30 margin-bottom60">
        <div class="container">
            <div class="row">

                <!-- Posts Block -->

                <div class="posts-block col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <table align="center" cellpadding="0" cellspacing="0" class="BlueTable" width="100%">
                        <tr class="subheader_img">
                            <td style="height: 19px" align="left">
                                <asp:HiddenField ID="hfTaxable" runat="server" Value="0" />
                                <asp:Label ID="lblError" runat="server" EnableViewState="False" CssClass="alert alert-warning"></asp:Label>
                                <asp:Label ID="lblMessage" runat="server" EnableViewState="true" CssClass="text-danger"></asp:Label></td>
                        </tr>
                        <tr class="matters">
                            <th align="left" class="tdfields"><%--Transaction Reference :--%>
                                <asp:Label ID="Label_MerchTxnRef" runat="server" Visible="False" />
                                <asp:Label ID="lblDate" runat="server" CssClass="matters" Visible="False"></asp:Label>
                            </th>
                        </tr>
                        <tr>
                            <td align="left">





                                <div class="mainheading">
                                    <div>
                                        Payment Summary
                                    </div>
                                </div>
                                <div>
                                    <div class="divinfo" id="divinfo" runat="server" style="width: auto">
                                        <div align="right" class="text-info">
                                            Click on Receipt No to view Receipt
                                        </div>
                                    </div>
                                    <asp:Repeater ID="repInfo" runat="server">
                                        <HeaderTemplate>
                                            <table style="width: 100%;">
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr style="border: none 0px; border-color: inherit;">
                                                <td align="left" valign="top" style="display: none; width: 65px; border: none 0px !important; padding-top: 10px;">
                                                    <%--<asp:Image ID="imgEmpImage" runat="server" Height="80px" ImageUrl='<%# Bind("PHOTOPATH") %>'
                                                        ToolTip='<%# Bind("STU_NAME") %>' Width="50px" />--%>
                                                </td>
                                                <td valign="top" align="left" style="border: none 0px !important;" colspan="4">
                                                    <table class="table table-striped table-bordered table-responsive text-left my-orders-table" width="100%" border="0">
                                                        <tr>
                                                            <td class="tdfieldsHome" width="100px">Student Id
                                                            </td>
                                                            <td class="tdfieldsHomeValue" style="font-weight: bold !important;">
                                                                <asp:Literal ID="ltStuno" runat="server" Text='<%# Bind("STU_NO") %>'></asp:Literal>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="tdfieldsHome">Name
                                                            </td>
                                                            <td class="tdfieldsHomeValue">
                                                                <asp:Label ID="lblStuName" Font-Bold="true" runat="server" Text='<%# Bind("STU_NAME") %>'></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="tdfieldsHome">Grade
                                                            </td>
                                                            <td class="tdfieldsHomeValue" style="font-weight: bold !important;">
                                                                <asp:Literal ID="ltGrade" runat="server" Text='<%# Bind("GRM_DISPLAY") %>'></asp:Literal>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="tdfieldsHome">Section
                                                            </td>
                                                            <td class="tdfieldsHomeValue" style="font-weight: bold !important;">
                                                                <asp:Literal ID="ltSct" runat="server" Text='<%# Bind("SCT_DESCR") %>'></asp:Literal>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td valign="top" align="left" style="border: none 0px !important; padding-top: 10px;">
                                                    <table class="tableNoborder" style="padding: 0px; margin: 0px; height: 80px !important; width: 100%;">
                                                        <tr>
                                                            <td style="vertical-align: baseline !important; text-align: center !important; color: #FFA500; font-weight: bold; font-size: 11pt;"><%--color: #FFA500;   font-weight: bold; font-size: 11pt;--%>
                                                                <asp:Label ID="ltCurrency" ForeColor="#FF7F50" Font-Size="10pt" runat="server"><%= IIf(Session("BSU_CURRENCY") Is Nothing, "", Session("BSU_CURRENCY"))%></asp:Label>
                                                                <%--ForeColor="#FF7F50" Font-Size="10pt"--%>
                                                                <br />
                                                                <asp:Literal ID="ltAmount" runat="server" Text='<%# Bind("AMOUNT", "{0:0.00}")%>'></asp:Literal><br />
                                                                <br />
                                                                <asp:LinkButton ID="lblReceipt" Style="color: Blue; text-decoration: underline !important;"
                                                                    runat="server" Text='<%# Bind("RECNO") %>'></asp:LinkButton>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr style="border: none 0px !important;">
                                                <td colspan="5" style="border: none 0px !important;">
                                                    <hr />
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <tr style="border: none 0px; font-size: 11pt; font-weight: bold;">
                                                <td align="right" colspan="4" style="width: 65px; border: none 0px !important; padding-top: 10px;">TOTAL
                                                </td>
                                                
                                                <td style="border: none 0px !important; padding-top: 10px; text-align: center !important;">
                                                    <asp:Label ID="lbl_Amount_F" runat="server" Text="0.00" />
                                                </td>
                                            </tr>
                                            </table>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <br />
                    <table align="center" border="0" width="70%" style="display: none">
                        <tr class="title">
                            <td colspan="2">
                                <iframe id="frame1" scrolling="auto" runat="server"></iframe>
                                <p>
                                    <strong>&nbsp;Transaction Receipt Fields</strong>
                                </p>
                                <asp:Label ID="Label_Title" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <strong><i>VPC API Version: </i></strong>
                            </td>
                            <td>
                                <asp:Label ID="Label_Version" runat="server" /></td>
                        </tr>
                        <tr class='shade'>
                            <td align="right">
                                <strong><i>Command: </i></strong>
                            </td>
                            <td>
                                <asp:Label ID="Label_Command" runat="server" /></td>
                        </tr>
                        <tr>
                            <td align="right">
                                <strong><em>MerchTxnRef: </em></strong>
                            </td>
                            <td></td>
                        </tr>
                        <tr class="shade">
                            <td align="right">
                                <strong><em>Merchant ID: </em></strong>
                            </td>
                            <td>
                                <asp:Label ID="Label_MerchantID" runat="server" /></td>
                        </tr>
                        <tr>
                            <td align="right">
                                <strong><em>OrderInfo: </em></strong>
                            </td>
                            <td>
                                <asp:Label ID="Label_OrderInfo" runat="server" /></td>
                        </tr>
                        <tr class="shade">
                            <td align="right">
                                <strong><em>Transaction Amount: </em></strong>
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td colspan="2" align="center">
                                <div class='bl'>
                                    Fields above are the primary request values.<hr>
                                    Fields below are receipt data fields.
                                </div>
                            </td>
                        </tr>
                        <tr class="shade">
                            <td align="right">
                                <strong><em>Transaction Response Code: </em></strong>
                            </td>
                            <td>
                                <asp:Label ID="Label_TxnResponseCode" runat="server" /></td>
                        </tr>
                        <tr>
                            <td align="right">
                                <strong><em>QSI Response Code Description: </em></strong>
                            </td>
                            <td>
                                <asp:Label ID="Label_TxnResponseCodeDesc" runat="server" /></td>
                        </tr>
                        <tr class='shade'>
                            <td align="right">
                                <strong><i>Message: </i></strong>
                            </td>
                            <td>
                                <asp:Label ID="Label_Message" runat="server" /></td>
                        </tr>
                        <asp:Panel ID="Panel_Receipt" runat="server" Visible="false">
                            <!-- only display these next fields if not an error -->
                            <tr>
                                <td align="right">
                                    <strong><em>Shopping Transaction Number: </em></strong>
                                </td>
                                <td>
                                    <asp:Label ID="Label_TransactionNo" runat="server" /></td>
                            </tr>
                            <tr class="shade">
                                <td align="right">
                                    <strong><em>Batch Number for this transaction: </em></strong>
                                </td>
                                <td>
                                    <asp:Label ID="Label_BatchNo" runat="server" /></td>
                            </tr>
                            <tr>
                                <td align="right">
                                    <strong><em>Acquirer Response Code: </em></strong>
                                </td>
                                <td>
                                    <asp:Label ID="Label_AcqResponseCode" runat="server" /></td>
                            </tr>
                            <tr class="shade">
                                <td align="right">
                                    <strong><em>Receipt Number: </em></strong>
                                </td>
                                <td>
                                    <asp:Label ID="Label_ReceiptNo" runat="server" /></td>
                            </tr>
                            <tr>
                                <td align="right">
                                    <strong><em>Authorization ID: </em></strong>
                                </td>
                                <td>
                                    <asp:Label ID="Label_AuthorizeID" runat="server" /></td>
                            </tr>
                            <tr class="shade">
                                <td align="right">
                                    <strong><em>Card Type: </em></strong>
                                </td>
                                <td>
                                    <asp:Label ID="Label_CardType" runat="server" /></td>
                            </tr>
                        </asp:Panel>
                        <tr>
                            <td colspan="2" style="height: 32px">
                                <hr />
                                &nbsp;</td>
                        </tr>
                        <tr class="title">
                            <td colspan="2" height="25">
                                <p>
                                    <strong>&nbsp;Hash Validation</strong>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <strong><em>Hash Validated Correctly: </em></strong>
                            </td>
                            <td>
                                <asp:Label ID="Label_HashValidation" runat="server" /></td>
                        </tr>
                        <asp:Panel ID="Panel_StackTrace" runat="server">
                            <!-- only display these next fields if an stacktrace output exists-->
                            <tr>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                            <tr class="title">
                                <td colspan="2">
                                    <p>
                                        <strong>&nbsp;Exception Stack Trace</strong>
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <asp:Label ID="Label_StackTrace" runat="server" /></td>
                            </tr>
                        </asp:Panel>
                        <tr>
                            <td width="50%">&nbsp;</td>
                            <td width="50%">&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="2" align="center">
                                <asp:Label ID="Label_AgainLink" runat="server" /></td>
                        </tr>
                        <tr>
                            <td align="center" colspan="2">
                                <asp:Panel ID="Panel_Debug" runat="server" Visible="false">
                                    <!-- only display these next fields if debug enabled -->
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Label ID="Label_Debug" runat="server" /></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="Label_DigitalOrder" runat="server" /></td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
