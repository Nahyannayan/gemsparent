Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Partial Class feePaymentRedirectNew
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        PaymentRedirect()
    End Sub

    Sub PaymentRedirect()
        Dim CPS_ID As Int16, SRC As String = "SCHOOL"
        CPS_ID = Session("CPS_ID")
        Dim MyPaymentGateway As New PaymentGatewayClass(Session("CPS_ID"))
        Dim UrlRef As String = Request.UrlReferrer.ToString()
        If Not Request.QueryString("SRC") Is Nothing AndAlso Request.QueryString("SRC") <> "" Then
            SRC = Encr_decrData.Decrypt(Request.QueryString("SRC").Replace(" ", "+"))
        End If
        '-------------------code added by Jacob on 29/jul/2019 for MPGS integration
        If (MyPaymentGateway.CPM_GATEWAY_TYPE = "MPGS" Or MyPaymentGateway.CPM_GATEWAY_TYPE = "INDUS") Then
            Dim MPGSdata As New NameValueCollection()
            Session("CPM_GATEWAY_TYPE") = MyPaymentGateway.CPM_GATEWAY_TYPE
            MPGSdata("SRC") = Encr_decrData.Encrypt(SRC)
            MPGSdata("ID") = Encr_decrData.Encrypt(Session("vpc_MerchTxnRef").ToString().Replace("OTH", "")) ' "KzriJLvsKss="
            MPGSdata("APP") = Encr_decrData.Encrypt("GEMSPARENT")
            MPGSdata("BSU_ID") = Encr_decrData.Encrypt(Session("sBsuId"))
            If SRC = "ACTIVITY_FC" Then
                MPGSdata("APP_RESULT_PAGE") = Session("vpc_ReturnURL").ToString().ToLower().Replace("others/activitypaymentresult.aspx", "Fees/PaymentResultPage.aspx").Split("?")(0).Replace("http://", IIf(Request.Url.Host.Contains("localhost"), "http://", "https://"))
            ElseIf SRC = "ACTIVITY_OC" Then
                MPGSdata("APP_RESULT_PAGE") = Session("vpc_ReturnURL").ToString().ToLower().Replace("others/activitypaymentresultother.aspx", "Fees/PaymentResultPage.aspx").Split("?")(0).Replace("http://", IIf(Request.Url.Host.Contains("localhost"), "http://", "https://"))
            Else
                MPGSdata("APP_RESULT_PAGE") = Session("vpc_ReturnURL").ToString().ToLower().Replace("feepaymentresultsibling.aspx", "PaymentResultPage.aspx").Split("?")(0).Replace("http://", IIf(Request.Url.Host.Contains("localhost"), "http://", "https://"))
            End If
            HttpHelper.RedirectAndPOST(Me.Page, MyPaymentGateway.PAYMENT_GATEWAY_URL, MPGSdata)
        Else '-------------------------------------------------------------------------
            MyPaymentGateway.CPM_GATEWAY_TYPE = IIf(MyPaymentGateway.CPM_GATEWAY_TYPE = "MPGS", "MIGS", MyPaymentGateway.CPM_GATEWAY_TYPE)
            Session("CPM_GATEWAY_TYPE") = MyPaymentGateway.CPM_GATEWAY_TYPE
            Dim data As New NameValueCollection()
            data("vpc_Command") = "pay"
            data("vpc_Version") = "1"
            data("vpc_ReturnURL") = Session("vpc_ReturnURL")
            data("vpc_Locale") = "en"
            data("vpc_OrderInfo") = Session("vpc_OrderInfo")
            data("Title") = "ASP VPC 3-Party"
            Session("vpc_Amount") = MyPaymentGateway.GetAmountInPaymentGatewayFormat(Session("vpc_Amount"))
            HttpHelper.RedirectAndPOST(Me.Page, "CS_VPC_3Party_DO.aspx", data)
        End If
    End Sub

   
End Class
