﻿Imports Microsoft.ApplicationBlocks.Data
Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Text
Partial Class Fees_feePaymentHistory
    Inherits System.Web.UI.Page
    Private Property bBSUTaxable() As Boolean
        Get
            Return ViewState("bBSUTaxable")
        End Get
        Set(ByVal value As Boolean)
            ViewState("bBSUTaxable") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim BSU_ID As String = Session("sBsuid")
        If Session("username") Is Nothing Then
            Response.Redirect("~\ParentLogin\Login.aspx")
        End If
        lbChildNameTop.Text = Session("STU_NAME")
        If Page.IsPostBack = False Then
            Page.Title = ":: GEMS EDUCATION | Payment History ::"
            txtFrom.Text = Format(Date.Today.AddMonths(-1), "dd-MMM-yyyy")
            txtTo.Text = Format(Date.Today, "dd-MMM-yyyy")
            Gridbind_Feedetails()
            'bBSUTaxable = DirectCast(SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT ISNULL(BUS_bFEE_TAXABLE,0) FROM dbo.BUSINESSUNIT_SUB WHERE BUS_BSU_ID='" & Session("sBsuid") & "'"), Boolean)
            bBSUTaxable = FeeCommon.GET_FEE_TAXABLE(BSU_ID)
            hfTaxable.Value = IIf(bBSUTaxable, "1", "0")
        End If
    End Sub

    Public Function Receiptencrypt(ByVal recno As Object) As String
        Dim Encr_decrData As New Encryption64
        If Not recno Is DBNull.Value Then
            Return Encr_decrData.Encrypt(recno)
        Else : Return ""
        End If
    End Function

    Protected Sub btnsearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsearch.Click
        Gridbind_Feedetails()
    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Gridbind_Feedetails()
    End Sub

    Sub Gridbind_Feedetails()
        Dim strStudNo As String = ""
        Dim bNoError As Boolean = True
        If IsDate(Date.Today) Then
            Dim dt As New DataTable
            dt = FeeCollectionOnline.F_GetFeeCollectionHistory(Session("STU_BSU_ID"), txtFrom.Text, txtTo.Text, Session("STU_ID"))
            If gvFeeCollection.Rows.Count <= 0 Then
                If dt.Columns.Contains("FCB_TRAN_ID") Then
                    Dim boundField As New BoundField
                    boundField.DataField = "FCB_TRAN_ID"
                    boundField.HeaderText = "Transaction ID"
                    'boundField.SortExpression = "ID"
                    boundField.HeaderStyle.HorizontalAlign = HorizontalAlign.Left
                    boundField.ItemStyle.HorizontalAlign = HorizontalAlign.Left
                    gvFeeCollection.Columns.Add(boundField)
                End If
                If dt.Columns.Contains("FCB_STATUS") Then
                    Dim boundField As New BoundField
                    boundField.DataField = "FCB_STATUS"
                    boundField.HeaderText = "Transaction Status"
                    'boundField.SortExpression = "ID"
                    boundField.HeaderStyle.HorizontalAlign = HorizontalAlign.Left
                    boundField.ItemStyle.HorizontalAlign = HorizontalAlign.Left
                    gvFeeCollection.Columns.Add(boundField)
                End If
            End If

            gvFeeCollection.DataSource = dt
            gvFeeCollection.DataBind()
        End If
    End Sub
End Class
