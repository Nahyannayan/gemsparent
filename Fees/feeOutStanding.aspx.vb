﻿Imports Microsoft.ApplicationBlocks.Data
Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Text
Partial Class Fees_feeOutStanding
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("username") Is Nothing Then
            Response.Redirect("~\ParentLogin\Login.aspx")
        End If
        lbChildNameTop.Text = Session("STU_NAME")
        If Page.IsPostBack = False Then
            Page.Title = ":: GEMS EDUCATION | Outstanding Fees ::"
            Gridbind_Feedetails()
            If Session("STU_BSU_ID").ToString() = "223006" Then ' disabled pay button for MAK by Jacob on 19Sep2019 as per Charles.
                btnPay.Visible = False
            End If
        End If
    End Sub

    Sub Gridbind_Feedetails()
        Dim strStudNo As String = ""
        Dim bNoError As Boolean = True
        If IsDate(Date.Today) Then
            Dim dt As New DataTable
            dt = FeeCollectionOnline.F_GetFeeDetailsForCollection(Date.Today.ToString, Session("STU_ID"), "S", Session("STU_BSU_ID"), Session("STU_ACD_ID"))
            gvFeeCollection.DataSource = dt
            'CONC_AMOUNT, ADJUSTMENT
            Dim dvConc As New DataView(dt)
            dvConc.RowFilter = "CONC_AMOUNT>0"
            If dvConc.Count = 0 Then
                gvFeeCollection.Columns(3).Visible = False
            End If
            Dim dvAdj As New DataView(dt)
            dvAdj.RowFilter = "ADJUSTMENT>0"
            If dvAdj.Count = 0 Then
                gvFeeCollection.Columns(4).Visible = False
            End If
            Dim helper As GridViewHelper
            helper = New GridViewHelper(gvFeeCollection, True)
            helper.RegisterSummary("MONTHLY_AMOUNT", SummaryOperation.Sum)
            helper.RegisterSummary("CONC_AMOUNT", SummaryOperation.Sum)
            helper.RegisterSummary("ADJUSTMENT", SummaryOperation.Sum)
            helper.RegisterSummary("PAID_AMOUNT", SummaryOperation.Sum)
            helper.RegisterSummary("CLOSING", SummaryOperation.Sum)
            gvFeeCollection.DataBind()
        End If
    End Sub

End Class
