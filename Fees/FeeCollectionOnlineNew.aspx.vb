﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Fees_FeeCollectionOnlineNew
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Public Shared dtDiscount As DataTable
    Private Property bPaymentConfirmMsgShown() As Boolean
        Get
            Return ViewState("bPaymentConfirmMsgShown")
        End Get
        Set(ByVal value As Boolean)
            ViewState("bPaymentConfirmMsgShown") = value
            If value Then
                btnSave.Text = "Confirm & Proceed"
            Else
                btnSave.Text = "Proceed"
            End If
        End Set
    End Property

    Private Property DiscountBreakUpXML() As String
        Get
            Return ViewState("DiscountBreakUpXML")
        End Get
        Set(ByVal value As String)
            ViewState("DiscountBreakUpXML") = value
        End Set
    End Property



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim BSU_ID As String = Session("sBsuid")
        Dim STU_ID As String = Session("STU_ID")

        If Session("username") Is Nothing Then
            Response.Redirect("~\login.aspx")
        End If
        If Page.IsPostBack = False Then
            bPaymentConfirmMsgShown = False
            BindPaymentProviders(Session("sBsuid"))
            'Dim sqlStr As String
            'sqlStr = "select isnull(BSU_FEE_OTH_DISC,0) from BUSINESSUNIT_M where bsu_id='" & Session("sbsuid") & "'"
            'Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, _
            ' CommandType.Text, sqlStr)
            Dim dsData As DataSet = FeeCommon.GetOtherDiscount(BSU_ID)
            If Not dsData Is Nothing AndAlso dsData.Tables.Count > 0 AndAlso dsData.Tables(0).Rows.Count > 0 Then
                ViewState("OthDiscEnabled") = True
            Else
                ViewState("OthDiscEnabled") = False
            End If

            Page.Title = "::GEMS Education |Fee Payment::"
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            txtAmountAdd.Attributes.Add("Onkeypress", "return Numeric_Only()")
            ViewState("datamode") = "add"
            Dim CurUsr_id As String = Session("STU_ID")
            Dim CurBsUnit As String = Session("sBsuid")
            Dim USR_NAME As String = Session("STU_NAME")
            InitialiseCompnents()
            SetNarration()
            If USR_NAME = "" Or CurBsUnit = "" Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            Else
                'Gridbind_Feedetails()
                ViewState("STU_NO") = Session("STU_NO")
                ViewState("STU_NAME") = Session("STU_NAME")
                ViewState("STU_GRD_ID") = Session("STU_GRD_ID")
                ViewState("stu_section") = Session("stu_section")
                ViewState("BSU_NAME") = Session("BSU_NAME")
                'lblCurrency.Text = IIf(Session("BSU_CURRENCY") Is Nothing, "Fee Details", "Fee Details (Amount In " & Session("BSU_CURRENCY") & ")")

                txtStdNo.Text = ViewState("STU_NO")
                txtStudentname.Text = ViewState("STU_NAME")
                stugrd.Text = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT ISNULL(B.GRM_DISPLAY,A.STU_GRD_ID) FROM dbo.STUDENT_M AS A WITH(NOLOCK) INNER JOIN dbo.GRADE_BSU_M AS B WITH(NOLOCK) ON A.STU_GRM_ID=B.GRM_ID  WHERE STU_ID=" & Session("STU_ID") & "")
                lblsct.Text = ViewState("stu_section")
                lblschool.Text = ViewState("BSU_NAME")
                Gridbind_Feedetails()
                SetDiscountData()
                If Not CheckIfStudentAllowedForOnlinePayment(STU_ID) Then
                    btnSave.Enabled = False
                    lblError.Text = "Please contact the school – Fee payments will be accepted at the school Fee counter only."
                    lblError.CssClass = "alert alert-warning"
                Else
                    btnSave.Enabled = True
                End If
                lMoreInfo.Attributes.Add("onClick", "return ShowSubWindowWithClose('whatsThis.htm', '', '50%', '50%');")
                lnkShowFeeSetup.Attributes.Add("onClick", "return ShowSubWindowWithClose('PopupShowDataFeeSetup.aspx?id=FEESETUP', '', '50%', '50%');")


                'If Not Session("STU_CURRSTATUS") Is Nothing Then 'Enabled online payment for 2 students in RDS as per request
                '    If Session("STU_CURRSTATUS") <> "EN" And ViewState("STU_NO") <> "12501600051373" And ViewState("STU_NO") <> "12501600051355" Then
                '        btnSave.Enabled = False
                '    Else
                '        btnSave.Enabled = True
                '    End If
                'Else
                '    btnSave.Enabled = True
                'End If

            End If

            gvFeeCollection.DataBind()
            rblPaymentGateway.DataBind()
            If rblPaymentGateway.Items.Count > 0 And rblPaymentGateway.SelectedIndex = -1 Then
                rblPaymentGateway.SelectedIndex = 0
                rblPaymentGateway_SelectedIndexChanged(sender, e)
            End If
            If Session("BSU_CURRENCY") = "AED" Then

                lblalert.Text = getErrorMessage("647")
            Else
                lblalert.Visible = False
            End If
        End If

    End Sub
    Private Function CheckIfStudentAllowedForOnlinePayment(ByVal STU_ID As Integer) As Boolean
        Try
            'Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, _
            '  CommandType.Text, "SELECT * FROM VW_StudentAllowedForOnlinePayment WHERE STU_ID=" & STU_ID)
            Dim dsData As DataSet = FeeCommon.CheckStudentAllowedForOnlinePayment(STU_ID)
            If Not dsData Is Nothing AndAlso dsData.Tables.Count > 0 AndAlso dsData.Tables(0).Rows.Count > 0 Then
                CheckIfStudentAllowedForOnlinePayment = True
            Else
                CheckIfStudentAllowedForOnlinePayment = False
            End If
        Catch ex As Exception
            CheckIfStudentAllowedForOnlinePayment = False
        End Try
    End Function
    Public Sub BindPaymentProviders(ByVal BSU_ID As String)
        HFCardCharge.Value = ""
        Dim pParms(1) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(0).Value = BSU_ID

        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISFINConnectionString, _
          CommandType.StoredProcedure, "GetMerchantDetails_BSU_ID", pParms)
        If Not dsData Is Nothing AndAlso dsData.Tables.Count > 0 Then
            For Each row As DataRow In dsData.Tables(0).Rows
                HFCardCharge.Value = HFCardCharge.Value & IIf(HFCardCharge.Value = "", "", "|") & row("CPS_ID") & "=" & row("CRR_CLIENT_RATE")
                'Dim lst As New ListItem(String.Format("<img src='{0}' alt='{1}' align='absmiddle' onclick='javascript:return this.parentNode.previousSibling.checked=true;' class='feeImg' />", row("CPM_IMAGEPATH").ToString(), row("CPM_DESCR").ToString()), row("CPS_ID").ToString())
                Dim lst As New ListItem(String.Format("<img src='{0}' alt='{1}'  title='{2}' align='absmiddle' onclick='javascript:return this.parentNode.previousSibling.checked=true;' class='feeImg' />", row("CPM_IMAGEPATH").ToString(), row("CPM_DESCR").ToString(), row("CPM_REMARKS").ToString()), row("CPS_ID").ToString())
                rblPaymentGateway.Items.Add(lst)
            Next
        End If
    End Sub


    Sub SetDiscountData()
        Dim BSU_ID As String = Session("sBsuid")
        Dim ds As New DataSet
        'Dim str_discount_sql As String = "select isnull(BSU_bAPPLYDISCOUNTSCHEME,0) from businessunit_m where bsu_id='" & Session("sBsuid") & "'"
        'ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, str_discount_sql)
        ds = FeeCommon.GETAPPLYDISCOUNTSCHEME(BSU_ID)

        tr_Discount.Visible = False
        gvFeeCollection.Columns(3).Visible = False
        tr_DiscountTotal.Visible = False
        If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            If Convert.ToBoolean(ds.Tables(0).Rows(0)(0)) Then
                tr_Discount.Visible = True
                gvFeeCollection.Columns(3).Visible = True
                tr_DiscountTotal.Visible = True
                Dim dtDiscount As DataTable
                dtDiscount = FeeCommon.DIS_SelectDiscount(lblDate.Text, Session("STU_ID"), "S", Session("sBsuid"), "", False)
                gvDiscount.DataSource = dtDiscount
                gvDiscount.DataBind()
            End If
        End If
        gvFeeCollection.Columns(3).Visible = True 'discount showing forever
    End Sub

    Sub InitialiseCompnents()
        txtTotal.Attributes.Add("readonly", "readonly")
        txtCardCharge.Attributes.Add("readonly", "readonly")
        txtGridTotal.Attributes.Add("readonly", "readonly")
        txtStudentname.Attributes.Add("readonly", "readonly")
        txtStdNo.Attributes.Add("readonly", "readonly")
        lblDate.Text = Format(Date.Now, "dd/MMM/yyyy")
        ddlFeeType.DataBind()
    End Sub

    Sub clear_All()
        gvFeeCollection.DataBind()
        txtTotal.Text = ""
        txtCardCharge.Text = "0"
        txtGridTotal.Text = "0"
        HFCardCharge.Value = ""
        tr_comment.Visible = False
        lnkMoreFee.Visible = False
        txtAmountAdd.Text = ""
        DiscountBreakUpXML = ""
        SetNarration()
        ClearStudentData()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If bPaymentConfirmMsgShown = True Then
            btnSave.Enabled = False
            Session("FeeCollection").rows.clear()
            gvFeeCollection.DataSource = Session("FeeCollection")
            gvFeeCollection.DataBind()
            Response.Redirect("feePaymentRedirectNew.aspx")
            Exit Sub
        End If

        Set_GridTotal(True)
        Dim vpc_OrderInfo As String = String.Empty
        'Dim vpc_ReturnURL As String = String.Empty
        Dim boolPaymentInitiated As Boolean = False
        Dim str_error As String = ""
        If rblPaymentGateway.SelectedIndex = -1 Then

            If str_error <> "" Then
                str_error = str_error & "<br /><br />"
                str_error = str_error & "Please select a payment gateway"
            Else
                str_error = str_error & "Please select a payment gateway"
            End If
        End If
        If Not IsDate(lblDate.Text) Then

            If str_error <> "" Then
                str_error = str_error & "<br /><br />"
                str_error = str_error & "Invalid date"
            Else
                str_error = str_error & "Invalid date"
            End If
        End If
        If Not IsNumeric(Session("STU_ID")) Then

            If str_error <> "" Then
                str_error = str_error & "<br /><br />"
                str_error = str_error & "Please select student"
            Else
                str_error = str_error & "Please select student"
            End If
        End If

        If gvFeeCollection.Rows.Count = 0 Then

            If str_error <> "" Then
                str_error = str_error & "<br /><br />"
                str_error = str_error & "Please add fee details"
            Else
                str_error = str_error & "Please add fee details"
            End If
        End If
        Dim dblTotal As Decimal
        dblTotal = CDbl(txtTotal.Text)
        If dblTotal <= 0 Then

            If str_error <> "" Then
                str_error = str_error & "<br /><br />"
                str_error = str_error & "Please enter a valid amount"
            Else
                str_error = str_error & "Please enter a valid amount"
            End If
        End If
        If str_error <> "" Then
            lblError.Text = str_error
            lblError.CssClass = "alert alert-warning"
            Exit Sub
        End If
        Dim dtFrom As DateTime
        dtFrom = CDate(lblDate.Text)
        Dim str_new_FCL_ID As Long
        Dim str_NEW_FCL_RECNO As String = ""
        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim objConn As New SqlConnection(str_conn) '
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try
            Dim retval As String = "1000"
            Dim STR_TYPE As Char = "S"
            retval = FeeCollectionOnline.F_SaveFEECOLLECTION_H_ONLINE(0, "Online", lblDate.Text, _
            Session("STU_ACD_ID"), Session("STU_ID"), "S", txtTotal.Text, False, str_new_FCL_ID, _
            Session("sBsuid"), "Online Fee Collection", "CR", str_NEW_FCL_RECNO, _
            Request.UserHostAddress.ToString, rblPaymentGateway.SelectedItem.Value, stTrans, DiscountBreakUpXML.ToString)
            If retval = "0" Then
                For Each gvr As GridViewRow In gvFeeCollection.Rows
                    Dim txtAmountToPay As TextBox = CType(gvr.FindControl("txtAmountToPay"), TextBox)
                    Dim lblFSR_FEE_ID As Label = CType(gvr.FindControl("lblFSR_FEE_ID"), Label)
                    Dim lblDiscount As Label = CType(gvr.FindControl("lblDiscount"), Label)
                    Dim lblAmount As Label = CType(gvr.FindControl("lblAmount"), Label)
                    If Not txtAmountToPay Is Nothing Then
                        If CDbl(txtAmountToPay.Text > 0) Then
                            retval = FeeCollectionOnline.F_SaveFEECOLLSUB_ONLINE(0, str_new_FCL_ID, lblFSR_FEE_ID.Text, _
                                    Val(txtAmountToPay.Text) + Val(lblDiscount.Text), -1, lblAmount.Text, lblDiscount.Text, stTrans)
                            If retval <> 0 Then
                                Exit For
                            End If
                        End If
                    End If
                Next
                '--------------------Added by Jacob on 23/oct/2013--------------------
                If retval = "0" Then
                    If Val(txtCardCharge.Text) > 0 Then
                        retval = FeeCollectionOnline.F_SaveFEECOLLSUB_ONLINE(0, str_new_FCL_ID, 162, _
                                Val(txtCardCharge.Text), -1, Val(txtCardCharge.Text), 0, stTrans)
                    End If
                End If

            End If
            If retval = "0" Then
                If dblTotal > 0 And retval = "0" Then 'cash  here
                    retval = FeeCollectionOnline.F_SaveFEECOLLSUB_D_ONLINE(0, str_new_FCL_ID, COLLECTIONTYPE.CASH, _
                    dblTotal, "", lblDate.Text, 0, "", "", "", stTrans, Val(txtCardCharge.Text))
                End If
                If retval = "0" Then
                    retval = FEECOLLECTION_H_ONLINE_Validation(str_new_FCL_ID, stTrans)
                End If
                If retval = "0" Then
                    stTrans.Commit()
                    'btnSave.Enabled = False
                    Session("vpc_Amount") = CStr(dblTotal * 100).Split(".")(0)
                    Dim STU_FEE_ID As String = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT ISNULL(STU_FEE_ID,STU_ID) FROM dbo.STUDENT_M WITH(NOLOCK) WHERE STU_ID=" & Session("STU_ID"))
                    'vpc_OrderInfo = "Payment for " & txtStdNo.Text.Replace("  ", " ")
                    vpc_OrderInfo = "RefNo-" & str_new_FCL_ID.ToString & "," & "PaymentFor " & STU_FEE_ID

                    If vpc_OrderInfo.Length > 34 Then
                        vpc_OrderInfo = Left(vpc_OrderInfo, 34)
                    End If
                    ' vpc_ReturnURL = Request.Url.ToString.Replace("FeeCollectionOnlineNew.aspx", "feePaymentResultPage.aspx")
                    Session("vpc_ReturnURL") = Request.Url.ToString.Replace("FeeCollectionOnlineNew.aspx", "feePaymentResultPage.aspx")
                    Session("vpc_MerchTxnRef") = str_new_FCL_ID
                    Session("CPS_ID") = rblPaymentGateway.SelectedItem.Value
                    'Session("FeeCollection").rows.clear()
                    ' gvFeeCollection.DataSource = Session("FeeCollection")
                    Session("vpc_OrderInfo") = vpc_OrderInfo
                    ' gvFeeCollection.DataBind()
                    Dim flagAudit As Integer = UtilityObj.operOnAudiTable("Fee Online Payment", str_new_FCL_ID, "Insert", Page.User.Identity.Name.ToString, Me.Page, Server.MachineName)
                    'lblError.Text = "<br />" & "Please note reference no. <B>" & str_new_FCL_ID & "</B><br />" & "Now press Proceed to continue.you are about to pay a amount of<B>" & Session("BSU_CURRENCY") & "." & dblTotal & "</B> "
                    lblError.Text = "<br />Click on Confirm & Proceed to continue with this payment " & IIf(Session("BSU_CURRENCY") Is Nothing, "", Session("BSU_CURRENCY")) & " :" & txtTotal.Text & "<br />" & _
                    "Please note reference no. " & str_new_FCL_ID & " of this transaction for any future communication.<br /><br />"
                    lblPayMsg.Text = "Please do not close this window or Log off until you get the final receipt."
                    Panel1.Visible = False
                    tr_Discount.Visible = False
                    bPaymentConfirmMsgShown = True
                    '   clear_All()
                    ' Gridbind_Feedetails()
                    boolPaymentInitiated = True

                Else
                    stTrans.Rollback()
                    lblError.Text = getErrorMessage(retval)
                    lblError.CssClass = "alert alert-warning"
                End If
            Else
                stTrans.Rollback()
                lblError.Text = getErrorMessage(retval)
                lblError.CssClass = "alert alert-warning"
            End If
        Catch ex As Exception
            stTrans.Rollback()
            lblError.Text = getErrorMessage(1000)
            lblError.CssClass = "alert alert-warning"
            Errorlog(ex.Message)
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Sub

    Public Shared Function FEECOLLECTION_H_ONLINE_Validation(ByVal FCO_ID As Integer, ByVal p_stTrans As SqlTransaction) As String
        Dim pParms(2) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@FCO_ID", SqlDbType.Int)
        pParms(0).Value = FCO_ID

        pParms(1) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(1).Direction = ParameterDirection.ReturnValue
        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "FEES.FEECOLLECTION_H_ONLINE_Validation", pParms)
        FEECOLLECTION_H_ONLINE_Validation = pParms(1).Value
    End Function

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        bPaymentConfirmMsgShown = False
        Response.Redirect("~\login.aspx")
    End Sub

    Sub Gridbind_Feedetails()
        If txtStdNo.Text.Trim <> "" Then
            Dim bNoError As Boolean = True
            If IsDate(lblDate.Text) And bNoError Then
                Dim dt As New DataTable
                dt = FeeCommon.F_GetFeeDetailsFOrCollection_online(lblDate.Text, Session("STU_ID"), "S", Session("sBsuid"))
                Session("FeeCollection") = dt
                gvFeeCollection.DataSource = dt
                gvFeeCollection.DataBind()
                Set_GridTotal(False)
            Else
                gvFeeCollection.DataBind()
                Set_GridTotal(False)
            End If
        End If
    End Sub
    Private Function checkIfCobrand(ByVal GatewayID As Int16) As Boolean
        Dim pParms(1) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(0).Value = Session("sBsuid")

        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISFINConnectionString, _
          CommandType.StoredProcedure, "GetMerchantDetails_BSU_ID", pParms)
        If Not dsData Is Nothing AndAlso dsData.Tables.Count > 0 Then
            For Each row As DataRow In dsData.Tables(0).Rows
                If row("CPS_ID") = GatewayID And row("CPS_CPM_ID") = 3 Then
                    checkIfCobrand = True
                    Exit Function
                End If
            Next
            checkIfCobrand = False
            Exit Function
        End If
    End Function
    Sub Set_GridTotal(ByVal DonotCalulateDisc As Boolean)
        Dim dAmount As Decimal = 0
        Dim dDiscount As Decimal = 0
        '--------checking for advance enrolment or admission paid
        Dim EnrollFee, AdmissionFee, OtherCharge As Double
        Dim IsCobrandCard As Boolean = False
        EnrollFee = 0 : AdmissionFee = 0 : OtherCharge = 0
        If rblPaymentGateway.SelectedIndex <> -1 Then
            IsCobrandCard = checkIfCobrand(rblPaymentGateway.SelectedItem.Value)
            If IsCobrandCard Then
                For Each gvro As GridViewRow In gvFeeCollection.Rows
                    Dim FSR_FEE_ID As Label = CType(gvro.FindControl("lblFSR_FEE_ID"), Label)
                    Dim tAmountToPay As TextBox = CType(gvro.FindControl("txtAmountToPay"), TextBox)
                    If Not FSR_FEE_ID.Text Is Nothing AndAlso (FSR_FEE_ID.Text = "145") Then
                        AdmissionFee += Val(tAmountToPay.Text)
                    End If
                    If Not FSR_FEE_ID.Text Is Nothing AndAlso (FSR_FEE_ID.Text = "145" Or FSR_FEE_ID.Text = "146") Then
                        EnrollFee += Val(tAmountToPay.Text)
                    End If
                Next
            End If
        End If
        dtDiscount = Nothing
        '----------------------------------------------------------
        For Each gvr As GridViewRow In gvFeeCollection.Rows
            Dim lblDiscount As Label = CType(gvr.FindControl("lblDiscount"), Label)
            Dim txtAmountToPay As TextBox = CType(gvr.FindControl("txtAmountToPay"), TextBox)
            Dim lblFSR_FEE_ID As Label = CType(gvr.FindControl("lblFSR_FEE_ID"), Label)
            Dim lbCancel As LinkButton = CType(gvr.FindControl("lbCancel"), LinkButton)
            Dim h_OtherCharge As HiddenField = CType(gvr.FindControl("h_OtherCharge"), HiddenField)
            Dim h_Discount As HiddenField = CType(gvr.FindControl("h_Discount"), HiddenField)
            Dim spanInfo As HtmlGenericControl = CType(gvr.FindControl("spanInfo"), HtmlGenericControl)
            Dim gvDiscDt As Repeater = DirectCast(gvr.FindControl("gvDiscDt"), Repeater)

            txtAmountToPay.Text = Val(txtAmountToPay.Text)
            If Not txtAmountToPay Is Nothing Then
                If IsNumeric(txtAmountToPay.Text) = False Then
                    lblError.Text = "Invalid Amount..!"
                    lblError.CssClass = "alert alert-warning"
                    txtAmountToPay.Text = "0"
                    txtTotal.Text = "0"
                    Exit Sub
                Else
                    lblError.CssClass = ""
                End If
                If lblFSR_FEE_ID.Text.Trim = "5" And tr_Discount.Visible And Not lbCancel.Visible Then
                    If DonotCalulateDisc = False Then
                        Dim decDiscountamt As Decimal = 0
                        FeeCollectionOnline.DIS_FindDiscount(Session("STU_ID"), "S", lblDate.Text, Session("sBsuid"), "", txtAmountToPay.Text, decDiscountamt)
                        lblDiscount.Text = Format(decDiscountamt, "0.00")
                        'txtAmountToPay.Text = Convert.ToDouble(txtAmountToPay.Text) - Convert.ToDouble(lblDiscount.Text) User enter net
                    End If
                End If
                If rblPaymentGateway.SelectedIndex <> -1 Then
                    If lblFSR_FEE_ID.Text.Trim = "5" And Not lbCancel.Visible Then
                        If DonotCalulateDisc = False Then
                            Dim decDiscountamt As Decimal = 0
                            Dim p_Amount As Double = 0
                            p_Amount = Val(txtAmountToPay.Text) + EnrollFee + Val(h_Discount.Value)

                            If IsCobrandCard Then
                                GetDiscountedFeesPayable_COBRAND(Session("STU_ID"), "S", rblPaymentGateway.SelectedItem.Value, _
                                                         COLLECTIONTYPE.CREDIT_CARD, lblFSR_FEE_ID.Text, p_Amount, decDiscountamt, AdmissionFee)

                            Else
                                '  p_Amount = Val(IIf(ForeignCurr = True, txtAmountToPayFC.Text, txtAmountToPay.Text))
                                GetDiscountedFeesPayable_COBRAND(Session("STU_ID"), "S", 0, _
                                      COLLECTIONTYPE.CREDIT_CARD, lblFSR_FEE_ID.Text, p_Amount, decDiscountamt, AdmissionFee, OtherCharge)

                            End If
                            ' GetDiscountedFeesPayable_COBRAND_ONLINE(Session("STU_ID"), "S", rblPaymentGateway.SelectedItem.Value, lblFSR_FEE_ID.Text.Trim, txtAmountToPay.Text, decDiscountamt)
                            'txtAmountToPay.Text = Convert.ToDouble(txtAmountToPay.Text) - Convert.ToDouble(lblDiscount.Text) ' User enter net
                            '----set amount to zero if its a negative value, added by Jacob on 13/Mar/2017
                            If ((Convert.ToDouble(txtAmountToPay.Text) - decDiscountamt) + OtherCharge + Val(h_Discount.Value)) < 0 Then
                                txtAmountToPay.Text = 0
                                decDiscountamt = 0
                            Else
                                txtAmountToPay.Text = Format(Convert.ToDouble(txtAmountToPay.Text) - decDiscountamt, Session("BSU_DataFormatString")) + OtherCharge + Val(h_Discount.Value)
                            End If

                            lblDiscount.Text = Format(decDiscountamt, "0.00")
                            h_OtherCharge.Value = OtherCharge
                            h_Discount.Value = decDiscountamt
                            gvDiscDt.DataSource = dtDiscount
                            gvDiscDt.DataBind()
                            If decDiscountamt <> 0 Then
                                spanInfo.Visible = True
                            Else
                                spanInfo.Visible = False
                            End If
                        End If
                    End If
                Else
                    DiscountBreakUpXML = ""
                End If
                dDiscount = dDiscount + CDbl(lblDiscount.Text)
                dAmount = dAmount + CDbl(txtAmountToPay.Text)
            End If
        Next
        Dim CPS_ID As Integer = Val(Me.rblPaymentGateway.SelectedValue)
        txtGridTotal.Text = Format(dAmount, "0.00")
        txtCardCharge.Text = Format(Math.Ceiling(GetCardProcessingCharge(CPS_ID) * dAmount), "0.00")
        txtTotal.Text = Format(Val(txtGridTotal.Text) + Val(txtCardCharge.Text), "0.00")
        'txtTotal.Text = Format(dAmount, "0.00")
        txtDiscountTotal.Text = Format(dDiscount, "0.00")
    End Sub
    Public Function GetCardProcessingCharge(ByVal CPS_ID As Integer) As Double
        Dim CardCharge As Double = 0
        If HFCardCharge.Value <> "" And CPS_ID <> 0 Then
            Dim Temp As String()
            Temp = HFCardCharge.Value.Split("|")
            For i As Int16 = 0 To Temp.Length - 1
                If Temp(i).Split("=")(0) = CPS_ID Then
                    CardCharge = Temp(i).Split("=")(1) / 100
                    Exit For
                End If
            Next
        End If
        Return CardCharge
    End Function

    Sub ClearStudentData()
        Gridbind_Feedetails()
    End Sub

    Protected Sub btnAionddDetails_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddDetails.Click
        If txtStdNo.Text = "" Or txtStudentname.Text = "" Then
            lblError.Text = "Select student!!!"
            lblError.CssClass = "alert alert-warning"
            Exit Sub
        Else
            lblError.CssClass = ""
        End If

        For x As Integer = 0 To Session("FeeCollection").Rows.count - 1
            If ddlFeeType.SelectedValue.Equals(Session("FeeCollection").Rows(x)("FEE_ID").ToString()) Then
                lblError.Text = ddlFeeType.SelectedItem.Text & "..Fees Head Already Exists!"
                lblError.CssClass = "alert alert-warning"
                gvFeeCollection.DataSource = Session("FeeCollection")
                gvFeeCollection.DataBind()
                Exit Sub
            End If
        Next
        If IsNumeric(txtAmountAdd.Text) Then
            Dim dr As DataRow
            'FEE_ID, FEE_DESCR,  CLOSING, CurrentCharge,  ,Amount
            dr = Session("FeeCollection").NewRow
            dr("FEE_ID") = CInt(ddlFeeType.SelectedValue)
            dr("FEE_DESCR") = ddlFeeType.SelectedItem.Text.ToString()
            dr("CLOSING") = 0
            dr("Amount") = txtAmountAdd.Text
            Session("FeeCollection").rows.add(dr)
            gvFeeCollection.DataSource = Session("FeeCollection")
            gvFeeCollection.DataBind()
            ClearDetail()
        Else
            lblError.Text = "Invalid Amount!!!"
            lblError.CssClass = "alert alert-warning"
        End If
        Set_GridTotal(False)
    End Sub

    Sub ClearDetail()
        txtAmountAdd.Text = ""
    End Sub

    Sub setFeeCollection()
        Dim iEdit, iRowcount As Integer
        iRowcount = 0
        For Each gvr As GridViewRow In gvFeeCollection.Rows
            Dim lblId As Label = TryCast(gvr.FindControl("lblId"), Label)
            If Not lblId Is Nothing Then
                iRowcount = iRowcount + 1
                For iEdit = 0 To Session("FeeCollection").Rows.Count - 1
                    If lblId.Text = Session("FeeCollection").Rows(iEdit)("ID") Then
                        Exit For
                    End If
                Next
            End If
            Dim str_amount As String
            str_amount = gvr.Cells(7).Text
            str_amount = TryCast(gvr.FindControl("txtAmountToPay"), TextBox).Text
            Session("FeeCollection").Rows(iEdit)("Amount") = str_amount
        Next
    End Sub

    Protected Sub txtAmountToPay_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim myCell, myRow As Object
            myCell = sender.parent
            If Not myCell Is Nothing Then
                myRow = myCell.parent
                If Not myRow Is Nothing Then
                    Dim h_Discount As HiddenField = CType(myRow.FindControl("h_Discount"), HiddenField)
                    If Not h_Discount Is Nothing Then
                        h_Discount.Value = 0
                    End If
                End If
            End If
            bPaymentConfirmMsgShown = False
            Set_GridTotal(False)
        Catch ex As Exception

        End Try

    End Sub

    Sub SetNarration()
        If IsDate(lblDate.Text) Then
            txtRemarks.Text = FeeCollectionOnline.F_GetFeeNarration(lblDate.Text, Session("STU_ACD_ID"), Session("STU_BSU_ID"))
        End If
        If txtRemarks.Text.Trim = "" Then
            txtRemarks.Text = "Online School Fees Payment"
        End If
    End Sub

    Protected Sub lnkSetArea_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Gridbind_Feedetails()
    End Sub

    Protected Sub lbApply_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        gvDiscount.SelectedIndex = sender.Parent.Parent.RowIndex
        Dim dtDiscountGrid As DataTable
        dtDiscountGrid = FeeCommon.DIS_SelectDiscount(lblDate.Text, Session("STU_ID"), "S", Session("sBsuid"), "", False)
        gvDiscount.DataSource = dtDiscountGrid
        gvDiscount.DataBind()
        Dim lblFee_ID As Label = sender.Parent.Parent.Findcontrol("lblFee_ID")
        Dim lblFds_ID As Label = sender.Parent.Parent.Findcontrol("lblFds_ID")

        Dim dtDiscountData As DataTable = FeeCollectionOnline.DIS_GetDiscount(lblFds_ID.Text, Session("STU_ID"), "S", lblDate.Text)
        gvFeeCollection.DataSource = Session("FeeCollection")
        gvFeeCollection.DataBind()
        If Not dtDiscountData Is Nothing AndAlso dtDiscountData.Rows.Count > 0 Then
            For Each gvr As GridViewRow In gvFeeCollection.Rows
                Dim txtAmountToPay As TextBox = CType(gvr.FindControl("txtAmountToPay"), TextBox)
                Dim lblDiscount As Label = CType(gvr.FindControl("lblDiscount"), Label)
                Dim lblFSR_FEE_ID As Label = CType(gvr.FindControl("lblFSR_FEE_ID"), Label)
                Dim lblAmount As Label = CType(gvr.FindControl("lblAmount"), Label)
                Dim lbCancel As LinkButton = CType(gvr.FindControl("lbCancel"), LinkButton)
                If Not txtAmountToPay Is Nothing AndAlso lblFSR_FEE_ID.Text.Trim = lblFee_ID.Text.Trim Then
                    'txtAmountToPay.Text = Format(Math.Round(dtDiscountData.Rows(0)("Net"), 0) + Convert.ToDecimal(lblAmount.Text), "0.00")
                    txtAmountToPay.Text = Format(Math.Round(dtDiscountData.Rows(0)("Net"), 0), "0.00")
                    If txtAmountToPay.Text < 0 Then
                        txtAmountToPay.Text = "0.00"
                    End If
                    txtAmountToPay.Attributes.Add("readonly", "readonly")
                    lblDiscount.Text = Format(Math.Round(dtDiscountData.Rows(0)("FDS_DISCAMOUNT"), 0), "0.00")
                    lblAmount.Text = Format(Math.Round(dtDiscountData.Rows(0)("Total"), 0), "0.00")
                    lbCancel.Visible = True
                End If
            Next
        End If
        Set_GridTotal(True)
    End Sub

    Protected Sub lbCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim dtDiscount As DataTable
        gvDiscount.SelectedIndex = -1
        dtDiscount = FeeCommon.DIS_SelectDiscount(lblDate.Text, Session("STU_ID"), "S", Session("sBsuid"), "", False)
        gvDiscount.DataSource = dtDiscount
        gvDiscount.DataBind()
        For Each gvr As GridViewRow In gvFeeCollection.Rows
            Dim txtAmountToPay As TextBox = CType(gvr.FindControl("txtAmountToPay"), TextBox)
            Dim lbCancel As LinkButton = CType(gvr.FindControl("lbCancel"), LinkButton)
            If Not txtAmountToPay Is Nothing Then
                txtAmountToPay.Attributes.Remove("readonly")
                lbCancel.Visible = False
            End If
        Next
        gvFeeCollection.DataSource = Session("FeeCollection")
        gvFeeCollection.DataBind()
    End Sub

    Protected Sub rblPaymentGateway_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rblPaymentGateway.SelectedIndexChanged

        ''nahyan for including masterpass
        If (rblPaymentGateway.SelectedItem.Value = "715") Then
            rdlmasterpass.Visible = True
        Else
            rdlmasterpass.Visible = False
        End If
        Gridbind_Feedetails()
        Set_GridTotal(False)
    End Sub
    Public Function GetDiscountedFeesPayable_COBRAND(ByVal p_STU_ID As String, ByVal p_STU_TYPE As String, _
              ByVal p_CRR_ID As String, ByVal p_CLT_ID As String, ByVal p_FEE_ID As String, _
              ByVal p_AMOUNT As Decimal, ByRef p_Discount As Decimal, Optional ByVal AdmissionFee As Decimal = 0, Optional ByRef OtherCharge As Decimal = 0) As String

        Dim pParms(14) As SqlClient.SqlParameter
        Dim dtDiscnt As New DataSet

        pParms(0) = New SqlClient.SqlParameter("@STU_ID", SqlDbType.BigInt) '
        pParms(0).Value = p_STU_ID
        pParms(1) = New SqlClient.SqlParameter("@STU_TYPE", SqlDbType.VarChar, 2)
        pParms(1).Value = p_STU_TYPE
        pParms(2) = New SqlClient.SqlParameter("@CRR_ID", SqlDbType.Int)
        pParms(2).Value = p_CRR_ID
        pParms(3) = New SqlClient.SqlParameter("@CLT_ID", SqlDbType.Int)
        pParms(3).Value = p_CLT_ID
        pParms(4) = New SqlClient.SqlParameter("@FEE_ID", SqlDbType.BigInt)
        pParms(4).Value = p_FEE_ID
        pParms(5) = New SqlClient.SqlParameter("@AMOUNT", SqlDbType.Decimal, 21)
        pParms(5).Value = p_AMOUNT
        pParms(6) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(6).Direction = ParameterDirection.ReturnValue
        pParms(7) = New SqlClient.SqlParameter("@Discount", SqlDbType.Decimal, 21)
        pParms(7).Direction = ParameterDirection.Output
        pParms(8) = New SqlClient.SqlParameter("@NEW_ADMISSION_FEE", SqlDbType.Decimal, 21)
        pParms(8).Value = AdmissionFee
        pParms(9) = New SqlClient.SqlParameter("@SPLITUP_XML", SqlDbType.Xml, 100000)
        pParms(9).Direction = ParameterDirection.Output
        pParms(10) = New SqlClient.SqlParameter("@OTHER_CHARGE", SqlDbType.Decimal, 21)
        pParms(10).Direction = ParameterDirection.Output
        SqlHelper.ExecuteNonQuery(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.StoredProcedure, "GetDiscountedFeesPayable_COBRAND_NEW", pParms)
        If pParms(6).Value = 0 Then
            If Not pParms(7).Value Is Nothing And pParms(7).Value Is System.DBNull.Value Then
                p_Discount = 0
            Else
                p_Discount = pParms(7).Value
            End If

            If Not pParms(10).Value Is Nothing And pParms(10).Value Is System.DBNull.Value Then
                OtherCharge = 0
            Else
                OtherCharge = pParms(10).Value
            End If

        End If
        If Not pParms(9).Value Is DBNull.Value AndAlso pParms(9).Value <> "" Then
            Dim readXML As StringReader = New StringReader(HttpUtility.HtmlDecode(pParms(9).Value))
            dtDiscnt.ReadXml(readXML)
            dtDiscount = dtDiscnt.Tables(0).Copy
        End If
        DiscountBreakUpXML = IIf(pParms(9).Value Is DBNull.Value, "", pParms(9).Value)
        GetDiscountedFeesPayable_COBRAND = pParms(6).Value
    End Function

    'Protected Sub lbPnlJobClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbPnlJobClose.Click
    '    'pnlFeesetup.Visible = False
    '    'pnlMoreInfo.Visible = False
    'End Sub

    'Protected Sub lnkShowFeeSetup_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkShowFeeSetup.Click
    '    'pnlFeesetup.Visible = True
    'End Sub

    'Protected Sub lMoreInfo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lMoreInfo.Click
    '    'pnlMoreInfo.Visible = True
    'End Sub

    Protected Sub gvFeeCollection_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvFeeCollection.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim h_BlockPayNow As HiddenField, txtAmountToPay As TextBox
                h_BlockPayNow = CType(e.Row.FindControl("h_BlockPayNow"), HiddenField)
                txtAmountToPay = CType(e.Row.FindControl("txtAmountToPay"), TextBox)
                If h_BlockPayNow IsNot Nothing And txtAmountToPay IsNot Nothing Then
                    If h_BlockPayNow.Value = "1" Or h_BlockPayNow.Value.ToLower = "true" Then
                        txtAmountToPay.Enabled = False
                    Else
                        txtAmountToPay.Enabled = True
                    End If
                End If
            End If
        Catch ex As Exception

        End Try

    End Sub
End Class
