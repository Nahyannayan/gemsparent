Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Fees_FeeReceipt
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim RedirectToLogin As Boolean = False
        If Session("username") Is Nothing Then
            RedirectToLogin = True
        End If
        If Page.IsPostBack = False Then
            'Page.Title = FeeCollectionOnlineBB.GetTransportTitle()7bad10
            'gvFeeDetails.Attributes.Add("bordercolor", "#000095")    
            Try
                Select Case Request.QueryString("type")
                    Case "REC"
                        If RedirectToLogin Then
                            Response.Redirect("~\ParentLogin\Login.aspx")
                        End If
                        If Request.QueryString("id") <> "" Then
                            PrintReceipt(Encr_decrData.Decrypt(Request.QueryString("id").Replace(" ", "+")))
                        End If
                    Case "OC"
                        If RedirectToLogin Then
                            Response.Redirect("~\ParentLogin\Login.aspx")
                        End If
                        If Request.QueryString("id") <> "" Then
                            PrintReceipt_othercollection(Encr_decrData.Decrypt(Request.QueryString("id").Replace(" ", "+")))
                        End If
                    Case "API"
                        Dim BsuId As String = "0", Recno As String = 0, time As DateTime, ParentUsername As String = ""
                        If Not Request.QueryString("bid") Is Nothing Then
                            BsuId = Encr_decrData.Decrypt(Request.QueryString("bid").Replace(" ", "+"))
                        End If
                        If Not Request.QueryString("t") Is Nothing Then
                            Dim createdtime = Encr_decrData.Decrypt(Request.QueryString("t").Replace(" ", "+"))
                            Try
                                time = Convert.ToDateTime(Encr_decrData.Decrypt(Request.QueryString("t").Replace(" ", "+")))
                            Catch ex As Exception
                                time = DateTime.Now
                            End Try

                        End If
                        If Not Request.QueryString("pu") Is Nothing Then
                            ParentUsername = Encr_decrData.Decrypt(Request.QueryString("pu").Replace(" ", "+"))
                        End If
                        If Not Request.QueryString("id") Is Nothing Then
                            Recno = Encr_decrData.Decrypt(Request.QueryString("id").Replace(" ", "+"))
                            Dim timeDiff As TimeSpan = DateTime.Now - time

                            If Math.Abs(timeDiff.Minutes) < 2 Then
                                LoadReceipt(BsuId, Recno, ParentUsername)
                            Else
                                mainDiv.Visible = False
                            End If
                        End If
                    Case "ACT_OC"
                        Dim BsuId As String = "0", Recno As String = 0, time As DateTime, ParentUsername As String = ""
                        If Not Request.QueryString("bid") Is Nothing Then
                            BsuId = Encr_decrData.Decrypt(Request.QueryString("bid").Replace(" ", "+"))
                        End If
                        If Not Request.QueryString("t") Is Nothing Then
                            Dim createdtime = Encr_decrData.Decrypt(Request.QueryString("t").Replace(" ", "+"))
                            Try
                                time = Convert.ToDateTime(Encr_decrData.Decrypt(Request.QueryString("t").Replace(" ", "+")))
                            Catch ex As Exception
                                time = DateTime.Now
                            End Try
                        End If
                        If Not Request.QueryString("pu") Is Nothing Then
                            ParentUsername = Encr_decrData.Decrypt(Request.QueryString("pu").Replace(" ", "+"))
                        End If
                        If Not Request.QueryString("id") Is Nothing Then
                            Recno = Encr_decrData.Decrypt(Request.QueryString("id").Replace(" ", "+"))
                            Dim timeDiff As TimeSpan = DateTime.Now - time

                            If Math.Abs(timeDiff.Minutes) < 2 Then
                                LoadReceipt_othercollection(BsuId, Recno, ParentUsername)
                            Else
                                mainDiv.Visible = False
                            End If
                        End If
                End Select
            Catch ex As Exception
                Errorlog("GemsParent/fees/feereceipt.aspx, Error:" & ex.Message, "PHOENIX")
            End Try
        End If
    End Sub
    Protected Sub LoadReceipt(ByVal p_BsuId As String, ByVal p_Recno As String, ByVal p_ParentUsername As String)
        Dim QRY, strFilter As String
        'QRY = "SELECT FCL_RECNO FROM FEES.FEECOLLECTION_H WITH(NOLOCK) WHERE FCL_BSU_ID='" & p_BsuId & "' AND FCL_ID='" & p_CollectionId & "'"
        'Dim RecNo As String = SqlHelper.ExecuteScalar(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, QRY)
        strFilter = " FCL_BSU_ID ='" & p_BsuId & "' AND FCL_RECNO ='" & p_Recno & "' "
        QRY = "SELECT FCL_DATE,STU_NAME,FCO_STU_ID,STU_NO,BSU_HEADER1,BSU_HEADER2,BSU_HEADER3,BSU_NAME,GRD_DISPLAY,FCL_RECNO,FCL_LOGDATE," & _
        "FCL_NARRATION,FCL_BALANCE,BSU_CURRENCY,FCL_AMOUNT,TRANSACTION_REF_NO FROM [FEES].[VW_OSO_FEES_FEERECEIPT] WHERE " + strFilter

        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, QRY)
        If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
            Page.Title = "Online Fee Receipt - " & Format(ds.Tables(0).Rows(0)("FCL_DATE"), "dd-MMM-yyyy") & " - " & ds.Tables(0).Rows(0)("STU_NAME").ToString().ToUpper()
            lblStudentNo.Text = ds.Tables(0).Rows(0)("STU_NO")
            lblStudentName.Text = ds.Tables(0).Rows(0)("STU_NAME").ToString().ToUpper()
            imgLogo.ImageUrl = "GetLogo.aspx?BSU_ID=" & p_BsuId
            lblHeader1.Text = ds.Tables(0).Rows(0)("BSU_HEADER1").ToString().ToLower()
            lblHeader2.Text = ds.Tables(0).Rows(0)("BSU_HEADER2")
            lblHeader3.Text = ds.Tables(0).Rows(0)("BSU_HEADER3")
            lblSchool.Text = ds.Tables(0).Rows(0)("BSU_NAME")
            lblDate.Text = Format(ds.Tables(0).Rows(0)("FCL_DATE"), "dd/MMM/yyyy")
            lblGrade.Text = IIf(ds.Tables(0).Rows(0)("GRD_DISPLAY") Is System.DBNull.Value, "", ds.Tables(0).Rows(0)("GRD_DISPLAY"))
            lblRecno.Text = ds.Tables(0).Rows(0)("FCL_RECNO")

            'Dim STU_ID = ds.Tables(0).Rows(0)("FCO_STU_ID")
            'QRY = "SELECT OLU_NAME FROM OASIS.ONLINE.ONLINE_USERS_M AS A WITH(NOLOCK) INNER JOIN OASIS.ONLINE.ONLINE_USERS_S AS B WITH(NOLOCK) ON A.OLU_ID = B.OUS_OLU_ID WHERE OUS_STU_ID = " & STU_ID
            'Dim ParentUserName As String = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, QRY)
            lbluserloggedin.Text = p_ParentUsername & " (Issued Time: " + CDate(ds.Tables(0).Rows(0)("FCL_LOGDATE")).ToString("hh:mm tt") & ")"

            lblPrintTime.Text = "Printed Time: " + Now.ToString("dd/MMM/yyyy hh:mm tt") & ""
            Dim Narration As String = IIf(ds.Tables(0).Rows(0)("FCL_NARRATION").ToString = "", "", "Narration : ")
            lblNarration.Text = Narration & ds.Tables(0).Rows(0)("FCL_NARRATION").ToString
            Dim str_paymnts As String = " exec fees.GetReceiptPrint_Online @FCL_RECNO ='" & lblRecno.Text & "', @FCL_BSU_ID  = '" & p_BsuId & "'"
            If IsNumeric(ds.Tables(0).Rows(0)("FCL_BALANCE")) AndAlso ds.Tables(0).Rows(0)("FCL_BALANCE") <> 0 Then
                If ds.Tables(0).Rows(0)("FCL_BALANCE") > 0 Then
                    lblBalance.Text = "Due : " & ds.Tables(0).Rows(0)("BSU_CURRENCY") & " " & Format(ds.Tables(0).Rows(0)("FCL_BALANCE"), "0.00")
                Else
                    lblBalance.Text = "Advance : " & ds.Tables(0).Rows(0)("BSU_CURRENCY") & " " & Format(ds.Tables(0).Rows(0)("FCL_BALANCE") * -1, "0.00")
                End If
            End If

            Dim ds1 As New DataSet
            SqlHelper.FillDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, str_paymnts, ds1, Nothing)
            gvFeeDetails.DataSource = ds1.Tables(0)
            gvFeeDetails.DataBind()

            lblPaymentDetals.Text = "Received with thanks <b>" & ds.Tables(0).Rows(0)("BSU_CURRENCY") & " " & Format(ds.Tables(0).Rows(0)("FCL_AMOUNT"), "0.00") & _
                      "</b> (" & Mainclass.SpellNumber(ds.Tables(0).Rows(0)("FCL_AMOUNT")) & _
                      ") through " & ds1.Tables(0).Rows(0)("CPM_DESCR") & "'s Payment Gateway (Ref.No. " & ds.Tables(0).Rows(0)("TRANSACTION_REF_NO") & " ) over the Internet towards the School Fees. "

            gvFeeDetails.HeaderRow.Cells(1).Text = "Amount (" & ds.Tables(0).Rows(0)("BSU_CURRENCY") & ")"
            lblProviderMessage.Text = ds1.Tables(0).Rows(0)("CPS_PAYMENT_MESSAGE")
            gvFeeDetails.Rows(gvFeeDetails.Rows.Count - 1).Font.Bold = True
        End If
    End Sub
    Protected Sub PrintReceipt(ByVal p_Receiptno As String)
        Dim str_Sql, strFilter As String
        str_Sql = "SELECT STUFF((SELECT ',' + CAST(STU_ID AS varchar) FROM ONLINE.[FN_GET_LINKED_SIBLING] ('" & Session("username") & "','SCHOOL' ) FOR XML PATH('')), 1, 1, '') AS STU_IDS"
        Dim StuIds As String = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, str_Sql)
        If StuIds.Trim.Replace(",", "") <> "" Then
            strFilter = " FCL_BSU_ID='" & Session("sBsuid") & "' AND FCL_RECNO='" & p_Receiptno & "' AND FCO_STU_ID IN (" & StuIds & ") "
        Else
            strFilter = " FCL_BSU_ID='" & Session("sBsuid") & "' AND FCL_RECNO='" & p_Receiptno & "' "
        End If

        str_Sql = "SELECT * FROM [FEES].[VW_OSO_FEES_FEERECEIPT] WHERE " + strFilter
        Dim cmd As New SqlCommand
        cmd.CommandText = str_Sql
        cmd.CommandType = Data.CommandType.Text
        ' check whether Data Exits
        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim ds As New DataSet
        SqlHelper.FillDataset(str_conn, CommandType.Text, cmd.CommandText, ds, Nothing)
        If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
            Page.Title = "Online Fee Receipt - " & Format(ds.Tables(0).Rows(0)("FCL_DATE"), "dd-MMM-yyyy") & " - " & ds.Tables(0).Rows(0)("STU_NAME").ToString().ToUpper()
            lblStudentNo.Text = ds.Tables(0).Rows(0)("STU_NO")
            lblStudentName.Text = ds.Tables(0).Rows(0)("STU_NAME").ToString().ToUpper()
            imgLogo.ImageUrl = "GetLogo.aspx?BSU_ID=" & Session("sBsuid")
            lblHeader1.Text = ds.Tables(0).Rows(0)("BSU_HEADER1").ToString().ToLower()
            lblHeader2.Text = ds.Tables(0).Rows(0)("BSU_HEADER2")
            lblHeader3.Text = ds.Tables(0).Rows(0)("BSU_HEADER3")
            lblSchool.Text = ds.Tables(0).Rows(0)("BSU_NAME")
            ' lblBusno.Text = IIf(ds.Tables(0).Rows(0)("BNO_DESCR") Is System.DBNull.Value, "", ds.Tables(0).Rows(0)("BNO_DESCR"))
            lblDate.Text = Format(ds.Tables(0).Rows(0)("FCL_DATE"), "dd/MMM/yyyy")
            lblGrade.Text = IIf(ds.Tables(0).Rows(0)("GRD_DISPLAY") Is System.DBNull.Value, "", ds.Tables(0).Rows(0)("GRD_DISPLAY"))
            lblRecno.Text = ds.Tables(0).Rows(0)("FCL_RECNO")

            lbluserloggedin.Text = Session("username").ToString & " (Issued Time: " + CDate(ds.Tables(0).Rows(0)("FCL_LOGDATE")).ToString("hh:mm tt") & ")"
            lblPrintTime.Text = "Printed Time: " + Now.ToString("dd/MMM/yyyy hh:mm tt") & ""
            Dim Narration As String = IIf(ds.Tables(0).Rows(0)("FCL_NARRATION").ToString = "", "", "Narration : ")
            lblNarration.Text = Narration & ds.Tables(0).Rows(0)("FCL_NARRATION").ToString
            'lblAmount.Text = Mainclass.SpellNumber(ds.Tables(0).Rows(0)("FCL_AMOUNT"))
            Dim str_paymnts As String = " exec fees.GetReceiptPrint_Online @FCL_RECNO ='" & p_Receiptno & "', @FCL_BSU_ID  = '" & Session("sBsuid") & "'"
            If IsNumeric(ds.Tables(0).Rows(0)("FCL_BALANCE")) AndAlso ds.Tables(0).Rows(0)("FCL_BALANCE") <> 0 Then
                If ds.Tables(0).Rows(0)("FCL_BALANCE") > 0 Then
                    lblBalance.Text = "Due : " & ds.Tables(0).Rows(0)("BSU_CURRENCY") & " " & Format(ds.Tables(0).Rows(0)("FCL_BALANCE"), "0.00")
                Else
                    lblBalance.Text = "Advance : " & ds.Tables(0).Rows(0)("BSU_CURRENCY") & " " & Format(ds.Tables(0).Rows(0)("FCL_BALANCE") * -1, "0.00")
                End If
            End If

            Dim ds1 As New DataSet
            SqlHelper.FillDataset(str_conn, CommandType.Text, str_paymnts, ds1, Nothing)
            gvFeeDetails.DataSource = ds1.Tables(0)
            gvFeeDetails.DataBind()

            lblPaymentDetals.Text = "Received with thanks <b>" & ds.Tables(0).Rows(0)("BSU_CURRENCY") & " " & Format(ds.Tables(0).Rows(0)("FCL_AMOUNT"), "0.00") & _
                      "</b> (" & Mainclass.SpellNumber(ds.Tables(0).Rows(0)("FCL_AMOUNT")) & _
                      ") through " & ds1.Tables(0).Rows(0)("CPM_DESCR") & "'s Payment Gateway (Ref.No. " & ds.Tables(0).Rows(0)("TRANSACTION_REF_NO") & " ) over the Internet towards the School Fees. "

            gvFeeDetails.HeaderRow.Cells(1).Text = "Amount (" & ds.Tables(0).Rows(0)("BSU_CURRENCY") & ")"
            lblProviderMessage.Text = ds1.Tables(0).Rows(0)("CPS_PAYMENT_MESSAGE")
            gvFeeDetails.Rows(gvFeeDetails.Rows.Count - 1).Font.Bold = True
        End If
    End Sub

    Protected Sub gvFeeDetails_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvFeeDetails.RowDataBound
        For Each tc As TableCell In e.Row.Cells
            tc.Attributes("style") = "border-left: #7f83ee 0pt solid; border-right: #7f83ee 1pt dotted; border-top: #7f83ee 1pt dotted; border-bottom: #7f83ee 1pt dotted;"
        Next
        If e.Row.Cells(0).Text = "Total" Then
            e.Row.Font.Bold = True
        End If
        If e.Row.Cells(0).Text.ToLower.Contains("discount") Then
            lblDiscount.Visible = True
        End If
    End Sub

    ' OTHER COLLECTION PRINT CHANGES START
    Protected Sub PrintReceipt_othercollection(ByVal p_Receiptno As String)
        Dim str_Sql, strFilter As String
        str_Sql = "SELECT STUFF((SELECT ',' + CAST(STU_ID AS varchar) FROM ONLINE.[FN_GET_LINKED_SIBLING] ('" & Session("username") & "','SCHOOL' ) FOR XML PATH('')), 1, 1, '') AS STU_IDS"
        Dim StuIds As String = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, str_Sql)
        If StuIds.Trim.Replace(",", "") <> "" Then
            strFilter = " FOC_RECNO='" & p_Receiptno & "' AND FOC_BSU_ID='" & Session("sBsuid") & "' AND FOC_STU_ID IN (" & StuIds & ") "
        Else
            strFilter = " FOC_RECNO='" & p_Receiptno & "' AND FOC_BSU_ID='" & Session("sBsuid") & "' "
        End If
        'strFilter = "  FOC_RECNO='" & p_Receiptno & "' AND FOC_BSU_ID='" & Session("sBsuid") & "' "
        str_Sql = "select * FROM [FEES].[VW_OSO_FEES_FEEOTHRECEIPT] WHERE " + strFilter
        Dim cmd As New SqlCommand
        cmd.CommandText = str_Sql
        cmd.CommandType = Data.CommandType.Text
        ' check whether Data Exits
        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim ds As New DataSet
        SqlHelper.FillDataset(str_conn, CommandType.Text, cmd.CommandText, ds, Nothing)
        If Not ds Is Nothing Or ds.Tables(0).Rows.Count > 0 Then
            Page.Title = "GEMS OASIS Online Fee Receipt - " & Format(ds.Tables(0).Rows(0)("FOC_DATE"), "dd-MMM-yyyy") & " - " & ds.Tables(0).Rows(0)("STU_NAME").ToString().ToUpper()
            lblStudentNo.Text = ds.Tables(0).Rows(0)("STU_NO")
            lblStudentName.Text = ds.Tables(0).Rows(0)("STU_NAME").ToString().ToUpper()
            imgLogo.ImageUrl = "GetLogo.aspx?BSU_ID=" & Session("sBsuid")
            lblHeader1.Text = ds.Tables(0).Rows(0)("BSU_HEADER1").ToString().ToLower()
            lblHeader2.Text = ds.Tables(0).Rows(0)("BSU_HEADER2")
            lblHeader3.Text = ds.Tables(0).Rows(0)("BSU_HEADER3")
            lblSchool.Text = ds.Tables(0).Rows(0)("BSU_NAME")
            ' lblBusno.Text = IIf(ds.Tables(0).Rows(0)("BNO_DESCR") Is System.DBNull.Value, "", ds.Tables(0).Rows(0)("BNO_DESCR"))
            lblDate.Text = Format(ds.Tables(0).Rows(0)("FOC_DATE"), "dd/MMM/yyyy")
            lblGrade.Text = IIf(ds.Tables(0).Rows(0)("GRD_DISPLAY") Is System.DBNull.Value, "", ds.Tables(0).Rows(0)("GRD_DISPLAY"))
            lblRecno.Text = ds.Tables(0).Rows(0)("FOC_RECNO")

            lbluserloggedin.Text = Session("username").ToString & " (Issued Time: " + CDate(ds.Tables(0).Rows(0)("FOC_DATE")).ToString("hh:mm tt") & ")"
            lblPrintTime.Text = "Printed Time: " + Now.ToString("dd/MMM/yyyy hh:mm tt") & ""
            Dim Narration As String = IIf(ds.Tables(0).Rows(0)("FOC_NARRATION").ToString = "", "", "Narration : ")
            lblNarration.Text = Narration & ds.Tables(0).Rows(0)("FOC_NARRATION").ToString
            'lblAmount.Text = Mainclass.SpellNumber(ds.Tables(0).Rows(0)("FCL_AMOUNT"))
            Dim str_paymnts As String = " exec FEES.GetOTHReceiptPrint_Online @FOC_RECNO ='" & p_Receiptno & "', @FOC_BSU_ID  = '" & Session("sBsuid") & "'"
            'If IsNumeric(ds.Tables(0).Rows(0)("FCL_BALANCE")) AndAlso ds.Tables(0).Rows(0)("FCL_BALANCE") <> 0 Then
            '    If ds.Tables(0).Rows(0)("FCL_BALANCE") > 0 Then
            '        lblBalance.Text = "Due : " & ds.Tables(0).Rows(0)("BSU_CURRENCY") & " " & Format(ds.Tables(0).Rows(0)("FCL_BALANCE"), "0.00")
            '    Else
            '        lblBalance.Text = "Advance : " & ds.Tables(0).Rows(0)("BSU_CURRENCY") & " " & Format(ds.Tables(0).Rows(0)("FCL_BALANCE") * -1, "0.00")
            '    End If
            'End If
            lblBalance.Text = 0
            Dim ds1 As New DataSet
            SqlHelper.FillDataset(str_conn, CommandType.Text, str_paymnts, ds1, Nothing)
            gvFeeDetails.DataSource = ds1.Tables(0)
            gvFeeDetails.DataBind()

            lblPaymentDetals.Text = "Received with thanks <b>" & ds.Tables(0).Rows(0)("BSU_CURRENCY") & " " & Format(ds.Tables(0).Rows(0)("FOC_AMOUNT"), "0.00") & _
                      "</b> (" & Mainclass.SpellNumber(ds.Tables(0).Rows(0)("FOC_AMOUNT")) & _
                      ") through " & ds1.Tables(0).Rows(0)("CPM_DESCR") & "'s Payment Gateway (Ref.No. " & ds.Tables(0).Rows(0)("FOC_ID") & " ) over the Internet towards the School Fees. "

            gvFeeDetails.HeaderRow.Cells(1).Text = "Amount (" & ds.Tables(0).Rows(0)("BSU_CURRENCY") & ")"
            lblProviderMessage.Text = ds1.Tables(0).Rows(0)("CPS_PAYMENT_MESSAGE")
            gvFeeDetails.Rows(gvFeeDetails.Rows.Count - 1).Font.Bold = True
        End If
    End Sub
    Protected Sub LoadReceipt_othercollection(ByVal p_BsuId As String, ByVal p_Recno As String, ByVal p_ParentUsername As String)
        Dim str_Sql, strFilter As String
        str_Sql = "SELECT STUFF((SELECT ',' + CAST(STU_ID AS varchar) FROM ONLINE.[FN_GET_LINKED_SIBLING] ('" & p_ParentUsername & "','SCHOOL' ) FOR XML PATH('')), 1, 1, '') AS STU_IDS"
        Dim StuIds As String = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, str_Sql)
        If StuIds.Trim.Replace(",", "") <> "" Then
            strFilter = " FOC_RECNO='" & p_Recno & "' AND FOC_BSU_ID='" & p_BsuId & "' AND FOC_STU_ID IN (" & StuIds & ") "
        Else
            strFilter = " FOC_RECNO='" & p_Recno & "' AND FOC_BSU_ID='" & p_BsuId & "' "
        End If
        'strFilter = "  FOC_RECNO='" & p_Receiptno & "' AND FOC_BSU_ID='" & p_BsuId & "' "
        str_Sql = "select * FROM [FEES].[VW_OSO_FEES_FEEOTHRECEIPT] WHERE " + strFilter
        Dim cmd As New SqlCommand
        cmd.CommandText = str_Sql
        cmd.CommandType = Data.CommandType.Text
        ' check whether Data Exits
        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim ds As New DataSet
        SqlHelper.FillDataset(str_conn, CommandType.Text, cmd.CommandText, ds, Nothing)
        If Not ds Is Nothing Or ds.Tables(0).Rows.Count > 0 Then
            Page.Title = "Online Fee Receipt - " & Format(ds.Tables(0).Rows(0)("FOC_DATE"), "dd-MMM-yyyy") & " - " & ds.Tables(0).Rows(0)("STU_NAME").ToString().ToUpper()
            lblStudentNo.Text = ds.Tables(0).Rows(0)("STU_NO")
            lblStudentName.Text = ds.Tables(0).Rows(0)("STU_NAME").ToString().ToUpper()
            imgLogo.ImageUrl = "GetLogo.aspx?BSU_ID=" & p_BsuId
            lblHeader1.Text = ds.Tables(0).Rows(0)("BSU_HEADER1").ToString().ToLower()
            lblHeader2.Text = ds.Tables(0).Rows(0)("BSU_HEADER2")
            lblHeader3.Text = ds.Tables(0).Rows(0)("BSU_HEADER3")
            lblSchool.Text = ds.Tables(0).Rows(0)("BSU_NAME")
            ' lblBusno.Text = IIf(ds.Tables(0).Rows(0)("BNO_DESCR") Is System.DBNull.Value, "", ds.Tables(0).Rows(0)("BNO_DESCR"))
            lblDate.Text = Format(ds.Tables(0).Rows(0)("FOC_DATE"), "dd/MMM/yyyy")
            lblGrade.Text = IIf(ds.Tables(0).Rows(0)("GRD_DISPLAY") Is System.DBNull.Value, "", ds.Tables(0).Rows(0)("GRD_DISPLAY"))
            lblRecno.Text = ds.Tables(0).Rows(0)("FOC_RECNO")

            lbluserloggedin.Text = p_ParentUsername.ToString & " (Issued Time: " + CDate(ds.Tables(0).Rows(0)("FOC_DATE")).ToString("hh:mm tt") & ")"
            lblPrintTime.Text = "Printed Time: " + Now.ToString("dd/MMM/yyyy hh:mm tt") & ""
            Dim Narration As String = IIf(ds.Tables(0).Rows(0)("FOC_NARRATION").ToString = "", "", "Narration : ")
            lblNarration.Text = Narration & ds.Tables(0).Rows(0)("FOC_NARRATION").ToString
            'lblAmount.Text = Mainclass.SpellNumber(ds.Tables(0).Rows(0)("FCL_AMOUNT"))
            Dim str_paymnts As String = " exec FEES.GetOTHReceiptPrint_Online @FOC_RECNO ='" & p_Recno & "', @FOC_BSU_ID  = '" & p_BsuId & "'"
            'If IsNumeric(ds.Tables(0).Rows(0)("FCL_BALANCE")) AndAlso ds.Tables(0).Rows(0)("FCL_BALANCE") <> 0 Then
            '    If ds.Tables(0).Rows(0)("FCL_BALANCE") > 0 Then
            '        lblBalance.Text = "Due : " & ds.Tables(0).Rows(0)("BSU_CURRENCY") & " " & Format(ds.Tables(0).Rows(0)("FCL_BALANCE"), "0.00")
            '    Else
            '        lblBalance.Text = "Advance : " & ds.Tables(0).Rows(0)("BSU_CURRENCY") & " " & Format(ds.Tables(0).Rows(0)("FCL_BALANCE") * -1, "0.00")
            '    End If
            'End If
            lblBalance.Text = 0
            Dim ds1 As New DataSet
            SqlHelper.FillDataset(str_conn, CommandType.Text, str_paymnts, ds1, Nothing)
            gvFeeDetails.DataSource = ds1.Tables(0)
            gvFeeDetails.DataBind()

            lblPaymentDetals.Text = "Received with thanks <b>" & ds.Tables(0).Rows(0)("BSU_CURRENCY") & " " & Format(ds.Tables(0).Rows(0)("FOC_AMOUNT"), "0.00") & _
                      "</b> (" & Mainclass.SpellNumber(ds.Tables(0).Rows(0)("FOC_AMOUNT")) & _
                      ") through " & ds1.Tables(0).Rows(0)("CPM_DESCR") & "'s Payment Gateway (Ref.No. " & ds.Tables(0).Rows(0)("FOC_ID") & " ) over the Internet towards the School Fees. "

            gvFeeDetails.HeaderRow.Cells(1).Text = "Amount (" & ds.Tables(0).Rows(0)("BSU_CURRENCY") & ")"
            lblProviderMessage.Text = ds1.Tables(0).Rows(0)("CPS_PAYMENT_MESSAGE")
            gvFeeDetails.Rows(gvFeeDetails.Rows.Count - 1).Font.Bold = True
        End If
    End Sub
    'OTHER COLLECTION PRINT CHANGES END 
End Class