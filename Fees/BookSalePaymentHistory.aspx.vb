﻿Imports Microsoft.ApplicationBlocks.Data
Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Text
Partial Class Fees_BookSalePaymentHistory
    Inherits System.Web.UI.Page
    'Private Property bBSUTaxable() As Boolean
    '    Get
    '        Return ViewState("bBSUTaxable")
    '    End Get
    '    Set(ByVal value As Boolean)
    '        ViewState("bBSUTaxable") = value
    '    End Set
    'End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim BSU_ID As String = Session("sBsuid")
        If Session("username") Is Nothing Then
            Response.Redirect("~\ParentLogin\Login.aspx")
        End If
        If Page.IsPostBack = False Then
            Page.Title = ":: GEMS EDUCATION | Payment History ::"
            txtFrom.Text = Format(Date.Today.AddMonths(-1), "dd-MMM-yyyy")
            txtTo.Text = Format(Date.Today, "dd-MMM-yyyy")
            Gridbind_BookSaleDetails()
            'bBSUTaxable = DirectCast(SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT ISNULL(BUS_bFEE_TAXABLE,0) FROM dbo.BUSINESSUNIT_SUB WHERE BUS_BSU_ID='" & Session("sBsuid") & "'"), Boolean)
            'bBSUTaxable = FeeCommon.GET_FEE_TAXABLE(BSU_ID)
            'hfTaxable.Value = IIf(bBSUTaxable, "1", "0")
        End If
    End Sub

    Public Function Receiptencrypt(ByVal recno As Object) As String
        Dim Encr_decrData As New Encryption64
        If Not recno Is DBNull.Value Then
            Return Encr_decrData.Encrypt(recno)
        Else : Return ""
        End If
    End Function

    Protected Sub btnsearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsearch.Click
        Gridbind_BookSaleDetails()
    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Gridbind_BookSaleDetails()
    End Sub

    Sub Gridbind_BookSaleDetails()
        Dim strStudNo As String = ""
        Dim bNoError As Boolean = True
        If IsDate(Date.Today) Then
            Dim dt As New DataTable
            dt = GetBookSaleHistory(Session("STU_BSU_ID"), txtFrom.Text, txtTo.Text, Session("STU_ID"))
            gvFeeCollection.DataSource = dt
            gvFeeCollection.DataBind()
        End If
    End Sub
    Public Shared Function GetBookSaleHistory(ByVal BSU_ID As String, ByVal p_FROMDT As String, ByVal p_TODT As String, _
 ByVal STU_ID As String) As DataTable
        Dim pParms(5) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@OPTIONS", SqlDbType.Int)
        pParms(0).Value = 1
        pParms(1) = New SqlClient.SqlParameter("@STU_ID", SqlDbType.VarChar, 20)
        pParms(1).Value = STU_ID
        pParms(2) = New SqlClient.SqlParameter("@FRMDT", SqlDbType.VarChar, 20)
        pParms(2).Value = p_FROMDT
        pParms(3) = New SqlClient.SqlParameter("@TODT", SqlDbType.VarChar, 20)
        pParms(3).Value = p_TODT
        pParms(4) = New SqlClient.SqlParameter("@BSAH_BSU_ID", SqlDbType.VarChar, 20)
        pParms(4).Value = BSU_ID

        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_PUR_INVConnectionString, _
          CommandType.StoredProcedure, "[DBO].[GET_BOOKSALES_HISTORY]", pParms)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function
End Class
