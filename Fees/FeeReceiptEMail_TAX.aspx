<%@ page language="VB" autoeventwireup="false" codefile="FeeReceiptEMail_TAX.aspx.vb"
    inherits="Fees_FeeReceiptEMail_TAX" %>

<%@ outputcache duration="1" location="None" varybyparam="none" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Online Payment Receipt</title>
    <base target="_self" />
    <style type="text/css">
        img {
            display: block;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <center>
            <table cellspacing="0" width="800" style="padding-right: 0px; padding-left: 0px; padding-bottom: 0px; vertical-align: top; width: 800px; border-top-style: none; padding-top: 0px; border-right-style: none; border-left-style: none; text-align: center; border-bottom-style: none">
                <tr valign="top">
                    <td colspan="2" align="center" style="border-bottom: #000095 2pt solid">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td align="left" rowspan="5" style="width: 10%">
                                    <asp:image id="imgLogo" runat="server" />
                                </td>
                                <td style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 13px; font-weight: bold; color: #000095"
                                    align="center">
                                    <asp:label id="lblSchool" runat="server"></asp:label>
                                </td>
                            </tr>
                            <tr>
                                <td style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px; font-weight: bold; color: #000095"
                                    align="center">
                                    <asp:label id="lblHeader1" runat="server"></asp:label>
                                </td>
                            </tr>
                            <tr>
                                <td style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px; font-weight: bold; color: #000095"
                                    align="center">
                                    <asp:label id="lblHeader2" runat="server"></asp:label>
                                </td>
                            </tr>
                            <tr>
                                <td style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px; font-weight: bold; color: #000095"
                                    align="center">
                                    <asp:label id="lblHeader3" runat="server"></asp:label>
                                </td>
                            </tr>
                            <tr>
                                <td style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px; font-weight: bold; color: #000095"
                                    align="center">
                                    <asp:label id="lblHeader4" runat="server"></asp:label>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td align="center" colspan="1" style="height: 70px"></td>
                </tr>
                <tr>
                    <td style="height: 20px;">&nbsp;</td>
                </tr>
                <tr>
                    <td valign="top" align="center">
                        <table>
                            <tr>
                                <td colspan="6" align="center" style="font-weight: bold; font-size: 10pt; color: #000095; font-family: Verdana; height: 19px;">Online Payment Receipt<br />
                                    <br />
                                </td>
                            </tr>
                            <tr>
                                <td align="center" colspan="6">
                                    <!-- content starts here -->
                                    <table cellpadding="3" width="100%" border="0" style="border-right: #7f83ee 1pt solid; border-top: #7f83ee 1pt solid; border-left: #7f83ee 1pt solid; border-bottom: #7f83ee 1pt solid">
                                        <tr>
                                            <td align="left" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px; color: #000095; width: 110px;">Receipt No</td>
                                            <td align="center" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px; font-weight: bold; color: #000095; width: 1px">:</td>
                                            <td align="left" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px; font-weight: bold; color: #000095">
                                                <asp:label id="lblRecno" runat="server"></asp:label>
                                            </td>
                                            <td align="left" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px; color: #000095; width: 50px;">Date</td>
                                            <td align="left" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px; font-weight: bold; color: #000095; width: 1px">:</td>
                                            <td align="left" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px; color: #000095; width: 70px;">
                                                <asp:label id="lblDate" runat="server"></asp:label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px; color: #000095">Student ID</td>
                                            <td align="center" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px; font-weight: bold; color: #000095; width: 1px">:</td>
                                            <td align="left" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px; font-weight: bold; color: #000095">
                                                <asp:label id="lblStudentNo" runat="server"></asp:label>
                                            </td>
                                            <td align="left" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px; color: #000095">Grade</td>
                                            <td align="left" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px; font-weight: bold; color: #000095; width: 1px">:</td>
                                            <td align="left" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px; color: #000095">
                                                <asp:label id="lblGrade" runat="server"></asp:label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px; color: #000095">Name</td>
                                            <td align="center" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px; font-weight: bold; color: #000095; width: 1px">:</td>
                                            <td align="left" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px; font-weight: bold; color: #000095"
                                                colspan="4">
                                                <asp:label id="lblStudentName" runat="server"></asp:label>
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- content ends here -->
                                </td>
                            </tr>
                            <tr class="ReceiptCaption" runat="server">
                                <td colspan="6" align="center" style="height: 19px; font-weight: bold; font-size: 10pt; color: #000095; font-family: Verdana;">Fee Details</td>
                            </tr>
                            <tr>
                                <td align="center" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px; color: #000095; text-indent: 20px; height: 120px;"
                                    colspan="6" valign="top">
                                    <asp:gridview id="gvFeeDetails" runat="server" autogeneratecolumns="False" cellpadding="4"
                                        bordercolor="#7f83ee" borderwidth="1pt" width="100%">
                                        <Columns>
                                            <asp:BoundField DataField="FEE_DESCR" HeaderText="Fee">
                                                <HeaderStyle HorizontalAlign="Left" Font-Bold="true" Font-Names="Verdana" Font-Size="10px"
                                                    ForeColor="#000095" />
                                                <ItemStyle Width="50%" Font-Names="Verdana" Font-Size="10px" ForeColor="#000095" HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="ACTUAL_AMOUNT" DataFormatString="{0:###,###,###,##0.00}"
                                                HeaderText="Pre-GST">
                                                <HeaderStyle HorizontalAlign="Right" Font-Bold="true" Font-Names="Verdana" Font-Size="10px"
                                                    ForeColor="#000095" />
                                                <ItemStyle HorizontalAlign="Right" Width="20%" Font-Names="Verdana" Font-Size="10px" ForeColor="#000095" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="FCS_TAX_AMOUNT" DataFormatString="{0:###,###,###,##0.00}"
                                                HeaderText="GST">
                                                <HeaderStyle HorizontalAlign="Right" Font-Bold="true" Font-Names="Verdana" Font-Size="10px" ForeColor="#000095" />
                                                <ItemStyle HorizontalAlign="Right" Width="10%" Font-Names="Verdana" Font-Size="10px" ForeColor="#000095" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="FCS_AMOUNT" HeaderText="Amount" DataFormatString="{0:0.00}">
                                                <HeaderStyle HorizontalAlign="Right" Font-Bold="true" Font-Names="Verdana" Font-Size="10px"
                                                    ForeColor="#000095" />
                                                <ItemStyle Width="20%" Font-Names="Verdana" Font-Size="10px" ForeColor="#000095" HorizontalAlign="Right" />
                                            </asp:BoundField>
                                        </Columns>
                                    </asp:gridview>
                                </td>
                            </tr>
                            <tr id="Tr2" runat="server">
                                <td align="right" colspan="6" style="height: 19px; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px; font-weight: bold; color: #000095">
                                    <asp:label id="lblBalance" runat="server"></asp:label>
                                    &nbsp;</td>
                            </tr>
                            <tr runat="server">
                                <td align="left" style="height: 19px" class="matters_normal">
                                    <asp:label id="lblNarration" runat="server"></asp:label>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" valign="middle" style="text-indent: 10px; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px; color: #000095"
                                    colspan="6">
                                    <br />
                                    <br />
                                    <asp:label id="lblPaymentDetals" runat="server"></asp:label>
                                    <br />
                                    <br />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="center" style="vertical-align: middle; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 8px; color: #000095; height: 177px;"
                        valign="middle">
                        <asp:image id="ImgPrintLogo" runat="server" imageurl="~/Images/Fees/Receipt/receipt.gif" />
                        <br />
                        This receipt is electronically generated and does not require any signature.<br />
                        <asp:label id="lblProviderMessage" runat="server" text=" This receipt is subject to Network International LLC crediting the amount to our account."
                            visible="true"> </asp:label>
                        <asp:label id="lblDiscount" visible="false" text="The discounted fee structure is applicable on the existing fee structure of the School.<br />Any  revision in fee for the period if approved by the regulatory authority later, the difference shall be payable by the parent"
                            runat="server"> </asp:label>
                    </td>
                </tr>
                <tr>
                    <td align="center" style="vertical-align: middle; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 8px; color: #000095"
                        valign="middle">
                        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%; height: 100%">
                            <tr>
                                <td align="left" style="font-size: 8px; color: #000095; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 8px; color: #000095"
                                    valign="bottom">Source: PHOENIX
                                </td>
                                <td valign="bottom" align="right">
                                    <asp:image id="imgSwooosh" runat="server" imageurl="~/Images/Fees/Receipt/gemsfooter_small.png" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="right" valign="bottom" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px; color: #000095">
                        <table border="0" cellpadding="0" cellspacing="0" style="border-top: #003A63 1px solid; width: 100%; height: 100%">
                            <tr>
                                <td align="left" style="font-size: 8px; color: #000095; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 8px; color: #000095"
                                    valign="top">
                                    <asp:label id="lblPrintTime" runat="server"></asp:label>
                                </td>
                                <td valign="top" align="right" style="font-size: 8px; color: #000095; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 8px; color: #000095">
                                    <asp:label id="lblLogged" runat="server" text="User : "></asp:label>
                                    <asp:label id="lbluserloggedin" runat="server"></asp:label>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </center>
    </form>
</body>
</html>
