﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Imports Lesnikowski.Barcode


Imports System.Drawing
Imports System.Drawing.Imaging

Partial Class Fees_BookSaleReceipt
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("username") Is Nothing Then
            Response.Redirect("~\ParentLogin\Login.aspx")
        End If
        If Page.IsPostBack = False Then
            'Dim myfont As New Font("IDAutomationHC39M", 26, FontStyle.Regular)
            'lblbarcode.Font.Name = "IDAutomationHC39M"

            Select Case Request.QueryString("type")
                Case "BRN"
                    If Request.QueryString("id") <> "" Then
                        'Picture2.ImageUrl = "GetBarcode.aspx?receiptNo=" & Encr_decrData.Decrypt(Request.QueryString("id").Replace(" ", "+"))
                        PrintReceipt(Encr_decrData.Decrypt(Request.QueryString("id").Replace(" ", "+")))
                    End If
            End Select
        End If
    End Sub

    Protected Function PrintBarcode(ByVal p_Receiptno As String) As String
        Dim imgBarCode As System.Web.UI.WebControls.Image = New System.Web.UI.WebControls.Image()
        Using bitMap As New Bitmap(p_Receiptno.Length * 14, 80)

            Dim graphics As System.Drawing.Graphics = System.Drawing.Graphics.FromImage(bitMap)
            Dim oFont As Font = New Font("IDAutomationHC39M", 16)
            Dim point As PointF = New PointF(2.0F, 2.0F)
            Dim blackBrush As SolidBrush = New SolidBrush(Color.Black)
            Dim whiteBrush As SolidBrush = New SolidBrush(Color.White)
            graphics.FillRectangle(whiteBrush, 0, 0, bitMap.Width, bitMap.Height)
            graphics.DrawString("*" + p_Receiptno + "*", oFont, blackBrush, point)


            Using ms As New MemoryStream()

                bitMap.Save(ms, System.Drawing.Imaging.ImageFormat.Png)
                Dim byteImage As Byte() = ms.ToArray()

                Convert.ToBase64String(byteImage)
                imgBarCode.ImageUrl = "data:image/png;base64," + Convert.ToBase64String(byteImage)

            End Using
        End Using

        Return imgBarCode.ImageUrl
    End Function
    Public Shared Function ByteArrayToHexString(ByVal Bytes As Byte()) As String
        Dim Result As StringBuilder = New StringBuilder(Bytes.Length * 2)
        Dim HexAlphabet As String = "0123456789ABCDEF"

        For Each B As Byte In Bytes
            Result.Append(HexAlphabet(CInt((B >> 4))))
            Result.Append(HexAlphabet(CInt((B And &HF))))
        Next

        Return Result.ToString()
    End Function

    'Public Shared Function HexStringToByteArray(ByVal Hex As String) As Byte()
    '    Dim Bytes As Byte() = New Byte(Hex.Length / 2 - 1) {}
    '    Dim HexValue As Integer() = New Integer() {&H0, &H1, &H2, &H3, &H4, &H5, &H6, &H7, &H8, &H9, &H0, &H0, &H0, &H0, &H0, &H0, &H0, &HA, &HB, &HC, &HD, &HE, &HF}
    '    Dim x As Integer = 0, i As Integer = 0

    '    While i < Hex.Length
    '        Bytes(x) = CByte((HexValue(Char.ToUpper(Hex(i + 0)) - "0"c) << 4 Or HexValue(Char.ToUpper(Hex(i + 1)) - "0"c)))
    '        i += 2
    '        x += 1
    '    End While

    '    Return Bytes
    'End Function
    Protected Sub PrintReceipt(ByVal p_Receiptno As String)
        Dim str_Sql, strFilter As String
        strFilter = "  BSAH_NO='" & p_Receiptno & "' AND BSAH_BSU_ID='" & Session("sBsuid") & "' "
        str_Sql = "select BSAH_DATE,STU_NAME,STU_NO,BSU_HEADER1,BSU_HEADER2,BSU_HEADER3,BSU_NAME,GRD_DISPLAY,BSAH_NO,BSAH_NARRATION,BSAHO_DELIVERY_TYPE,BSU_CURRENCY,NET_TOTAL,BSAH_BSAH_ID FROM  [DBO].[VW_OSO_BOOKSALE_RECEIPT] WHERE " + strFilter
        Dim cmd As New SqlCommand
        cmd.CommandText = str_Sql
        cmd.CommandType = Data.CommandType.Text
        ' check whether Data Exits
        Dim str_conn As String = ConnectionManger.GetOASIS_PUR_INVConnectionString
        Dim ds As New DataSet
        SqlHelper.FillDataset(str_conn, CommandType.Text, cmd.CommandText, ds, Nothing)
        If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
            Page.Title = "GEMS OASIS Online Book Sale Receipt - " & Format(ds.Tables(0).Rows(0)("BSAH_DATE"), "dd-MMM-yyyy") & " - " & ds.Tables(0).Rows(0)("STU_NAME").ToString().ToUpper()
            lblStudentNo.Text = ds.Tables(0).Rows(0)("STU_NO").ToString()
            lblStudentName.Text = ds.Tables(0).Rows(0)("STU_NAME").ToString().ToUpper()
            imgLogo.ImageUrl = "GetLogo.aspx?BSU_ID=" & Session("sBsuid")
            lblHeader1.Text = ds.Tables(0).Rows(0)("BSU_HEADER1").ToString().ToLower()
            lblHeader2.Text = ds.Tables(0).Rows(0)("BSU_HEADER2").ToString()
            lblHeader3.Text = ds.Tables(0).Rows(0)("BSU_HEADER3").ToString()
            lblSchool.Text = ds.Tables(0).Rows(0)("BSU_NAME").ToString()
            ' lblBusno.Text = IIf(ds.Tables(0).Rows(0)("BNO_DESCR") Is System.DBNull.Value, "", ds.Tables(0).Rows(0)("BNO_DESCR"))
            lblDate.Text = Format(ds.Tables(0).Rows(0)("BSAH_DATE"), "dd/MMM/yyyy")
            lblGrade.Text = IIf(ds.Tables(0).Rows(0)("GRD_DISPLAY") Is System.DBNull.Value, "", ds.Tables(0).Rows(0)("GRD_DISPLAY").ToString())
            lblRecno.Text = ds.Tables(0).Rows(0)("BSAH_NO").ToString()

            'Imports Lesnikowski.Barcode
            'Dim barcode As BaseBarcode
            'barcode = BarcodeFactory.GetBarcode(Symbology.Code128)
            'barcode.Number = p_Receiptno
            'barcode.BackColor = Drawing.ColorTranslator.FromHtml("#FFFFFF")
            'barcode.ChecksumAdd = True
            'barcode.CustomText = p_Receiptno
            'barcode.NarrowBarWidth = 3
            'barcode.Height = 300
            'barcode.FontHeight = 0.3F
            'barcode.ForeColor = Drawing.Color.Black
            'Dim b As Byte()
            'ReDim b(barcode.Render(ImageType.Png).Length)
            'b = barcode.Render(ImageType.Png)
            'Dim base64string As String = System.Convert.ToBase64String(b, 0, b.Length)
            'Dim base64Hex As String = ByteArrayToHexString(b)
            'Picture2.Src = "data:image/png;base64," + base64string
            'Imports Lesnikowski.Barcode

            'Imports Lesnikowski.Barcode
            'Picture2.ImageUrl = "GetBarcode.aspx?receiptNo=" & Encr_decrData.Decrypt(Request.QueryString("id").Replace(" ", "+"))
            'Imports Lesnikowski.Barcode

            'IDAutomationHC39M
            'Picture2.Src = PrintBarcode(p_Receiptno)
            'IDAutomationHC39M

            'IDAutomationHC39M
            'lblbarcode.Text = "!" + p_Receiptno + "!"
            'IDAutomationHC39M

            RadBarcode2.Text = "" + p_Receiptno + ""


            'Dim BSAH_PRNT_ID = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT CAST(REPLACE('" & STUNO & "',SUBSTRING('" & STUNO & "',1,6),'')AS INTEGER)AS STUNO")

            'Dim objConn As New SqlConnection(str_conn) '
            'objConn.Open()
            'Dim stTrans As SqlTransaction = objConn.BeginTransaction
            'Try
            '    Dim retval As Integer = clsBookSalesOnline.UPDATE_BARCODE(1, BSAH_PRNT_ID, Session("sBsuid"), b, stTrans)
            '    If retval = 0 Then
            '        stTrans.Commit()
            '    Else
            '        stTrans.Rollback()
            '    End If
            'Catch ex As Exception
            '    stTrans.Rollback()
            '    Errorlog(ex.Message)
            'Finally
            '    If objConn.State = ConnectionState.Open Then
            '        objConn.Close()
            '    End If
            'End Try

            lbluserloggedin.Text = Session("username").ToString & " (Issued Time: " + CDate(ds.Tables(0).Rows(0)("BSAH_DATE")).ToString("hh:mm tt") & ")"
            lblPrintTime.Text = "Printed Time: " + Now.ToString("dd/MMM/yyyy hh:mm tt") & ""
            Dim Narration As String = IIf(ds.Tables(0).Rows(0)("BSAH_NARRATION").ToString() = "", "", "Narration : ")
            Narration = Narration & ds.Tables(0).Rows(0)("BSAH_NARRATION").ToString()

            If ds.Tables(0).Rows(0)("BSAHO_DELIVERY_TYPE").ToString() = "0" Then
                Dim pParms1(2) As SqlClient.SqlParameter
                pParms1(0) = New SqlClient.SqlParameter("@OPTIONS", SqlDbType.Int)
                pParms1(0).Value = 1
                pParms1(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.Int)
                pParms1(1).Value = Session("sBSUID")
                Dim dsM As New DataSet
                dsM = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_PUR_INVConnectionString, CommandType.StoredProcedure, "[dbo].[GET_BOOK_COLLECTION_MESSAGE]", pParms1)

                Narration = Narration & "" & "</br> Delivery Mode : "
                Narration = Narration & "Collect the Books from School Personally"
                If Not dsM Is Nothing AndAlso dsM.Tables(0).Rows.Count > 0 Then
                    Narration = Narration & "" & IIf(dsM.Tables(0).Rows(0)("BCM_MESSAGE").ToString = "", "", "</br> Collection Message : ")
                    Narration = Narration & dsM.Tables(0).Rows(0)("BCM_MESSAGE").ToString()
                End If
                lblNarration.Text = Narration
            ElseIf ds.Tables(0).Rows(0)("BSAHO_DELIVERY_TYPE").ToString() = "1" Then
                Narration = Narration & "" & "</br> Delivery Mode : "
                lblNarration.Text = Narration & "Deliver Books To Child"

                'lblNarration.Text = Narration
            ElseIf ds.Tables(0).Rows(0)("BSAHO_DELIVERY_TYPE").ToString() = "2" Then
                Narration = Narration & "" & "</br> Delivery Mode : "
                lblNarration.Text = Narration & "Deliver Books By Courier"

                'lblNarration.Text = Narration
            End If


            Dim id_show_price_details As Boolean = 0
            Dim pParms0(2) As SqlClient.SqlParameter
            pParms0(0) = New SqlClient.SqlParameter("@OPTIONS", SqlDbType.Int)
            pParms0(0).Value = 1
            pParms0(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.Int)
            pParms0(1).Value = Session("sBSUID")
            Dim ds0 As New DataSet
            ds0 = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_PUR_INVConnectionString, CommandType.StoredProcedure, "[dbo].[GET_SHOW_PRICE_DETAILS]", pParms0)
            id_show_price_details = Convert.ToBoolean(ds0.Tables(0).Rows(0)("IS_SHOW"))
            Dim str_paymnts As String = ""
            If id_show_price_details = False Then
                gvSALDetails.Columns(2).Visible = False
                gvSALDetails.Columns(3).Visible = False
                gvSALDetails.Columns(4).Visible = False
                gvSALDetails.Columns(5).Visible = False
                gvSALDetails.Columns(6).Visible = False
                str_paymnts = " exec [dbo].[GET_BOOKSALES_RECEIPT_ONLINE] @OPTIONS =3, @BSAH_NO  = '" & p_Receiptno & "', @BSU_ID='" & Session("sBSUID") & "'"
            Else
                str_paymnts = " exec [dbo].[GET_BOOKSALES_RECEIPT_ONLINE] @OPTIONS =1, @BSAH_NO  = '" & p_Receiptno & "', @BSU_ID='" & Session("sBSUID") & "'"
            End If
            'lblAmount.Text = Mainclass.SpellNumber(ds.Tables(0).Rows(0)("FCL_AMOUNT"))
            'Dim str_paymnts As String = " exec [dbo].[GET_BOOKSALES_RECEIPT_ONLINE] @OPTIONS =1, @BSAH_NO  = '" & p_Receiptno & "', @BSU_ID='" & Session("sBSUID") & "'"
            'If IsNumeric(ds.Tables(0).Rows(0)("FCL_BALANCE")) AndAlso ds.Tables(0).Rows(0)("FCL_BALANCE") <> 0 Then
            '    If ds.Tables(0).Rows(0)("FCL_BALANCE") > 0 Then
            '        lblBalance.Text = "Due : " & ds.Tables(0).Rows(0)("BSU_CURRENCY") & " " & Format(ds.Tables(0).Rows(0)("FCL_BALANCE"), "0.00")
            '    Else
            '        lblBalance.Text = "Advance : " & ds.Tables(0).Rows(0)("BSU_CURRENCY") & " " & Format(ds.Tables(0).Rows(0)("FCL_BALANCE") * -1, "0.00")
            '    End If
            'End If

            Dim ds1 As New DataSet
            SqlHelper.FillDataset(str_conn, CommandType.Text, str_paymnts, ds1, Nothing)
            gvSALDetails.DataSource = ds1.Tables(0)
            gvSALDetails.DataBind()

            Dim VAT0TOTAL As String = "0.00", VAT5TOTAL As String = "0.00",
                VATAMOUNT As String = "0.00", TOTALAMOUNT As String = "0.00"
            Dim pParms(3) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@OPTIONS", SqlDbType.Int)
            pParms(0).Value = 1
            pParms(1) = New SqlClient.SqlParameter("@BSAH_NO", SqlDbType.VarChar)
            pParms(1).Value = p_Receiptno
            pParms(2) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar)
            pParms(2).Value = Session("sBSUID")

            Dim ds70 As New DataSet
            ds70 = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_PUR_INVConnectionString, CommandType.StoredProcedure, "[dbo].[GET_BOOKSALE_SUBTOTALS]", pParms)
            If Not ds70 Is Nothing AndAlso ds70.Tables(0).Rows.Count > 0 Then
                VAT0TOTAL = ds70.Tables(0).Rows(0)("VAT0TOTAL")
                VAT5TOTAL = ds70.Tables(0).Rows(0)("VAT5TOTAL")
                VATAMOUNT = ds70.Tables(0).Rows(0)("VATAMOUNT")
                TOTALAMOUNT = ds70.Tables(0).Rows(0)("TOTALAMOUNT")
            End If

            subtotal.Text = "<b>Subtotal (Items with VAT 0%) : " & VAT0TOTAL & "</br></br> Subtotal (Items with VAT 5%) : " & VAT5TOTAL & "</br></br> VAT Amount  : " & VATAMOUNT & "</br></br> Amount Payable  : " & TOTALAMOUNT & "</b></br></br>"

            lblPaymentDetals.Text = "Received with thanks <b>" & ds.Tables(0).Rows(0)("BSU_CURRENCY").ToString() & " " & Format(ds.Tables(0).Rows(0)("NET_TOTAL"), "0.00") & _
                      "</b> (" & Mainclass.SpellNumber(ds.Tables(0).Rows(0)("NET_TOTAL")) & _
                      ") through Payment Gateway (Ref.No. " & ds.Tables(0).Rows(0)("BSAH_BSAH_ID").ToString() & " ) over the Internet towards the School Books. "
            '") through " & ds1.Tables(0).Rows(0)("CPM_DESCR") & "'s Payment Gateway (Ref.No. " & ds.Tables(0).Rows(0)("BSAH_BSAH_ID") & " ) over the Internet towards the School Fees. "
            gvSALDetails.HeaderRow.Cells(7).Text = "Amount (" & ds.Tables(0).Rows(0)("BSU_CURRENCY").ToString() & ")"
            'lblProviderMessage.Text = ds1.Tables(0).Rows(0)("CPS_PAYMENT_MESSAGE")
            gvSALDetails.Rows(gvSALDetails.Rows.Count - 1).Font.Bold = True
            gvSALDetails.Rows(gvSALDetails.Rows.Count - 1).Cells(0).Text = ""
            gvSALDetails.Rows(gvSALDetails.Rows.Count - 1).Cells(2).Text = ""
            gvSALDetails.Rows(gvSALDetails.Rows.Count - 1).Cells(3).Text = ""
            gvSALDetails.Rows(gvSALDetails.Rows.Count - 1).Cells(4).Text = ""
            gvSALDetails.Rows(gvSALDetails.Rows.Count - 1).Cells(5).Text = ""
            gvSALDetails.Rows(gvSALDetails.Rows.Count - 1).Cells(6).Text = ""
        End If
        ds.Dispose()

    End Sub

    Protected Sub gvSALDetails_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvSALDetails.RowDataBound
        For Each tc As TableCell In e.Row.Cells
            tc.Attributes("style") = "border-left: #7f83ee 0pt solid; border-right: #7f83ee 1pt dotted; border-top: #7f83ee 1pt dotted; border-bottom: #7f83ee 1pt dotted;"
        Next
        If e.Row.Cells(0).Text = "Total" Then
            e.Row.Font.Bold = True
        End If
        If e.Row.Cells(0).Text.ToLower.Contains("discount") Then
            lblDiscount.Visible = True
        End If
    End Sub
End Class
