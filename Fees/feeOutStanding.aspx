﻿<%@ Page Language="VB" MasterPageFile="~/ParentMaster.master"  AutoEventWireup="false" CodeFile="feeOutStanding.aspx.vb" Inherits="Fees_feeOutStanding" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphParent" runat="Server">
    <style>
        .boldFeehead {
            text-transform: uppercase;
        }
    </style>
    <div class="content margin-top30 margin-bottom60">
                    <div class="container">
                        <div class="row">
    
    <!-- Posts Block -->
    <div class="posts-block col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="my-account">
            
            <div class="bottom-padding">
                <div class="title-box">
                    <h3>Outstanding Fees <%= IIf(Session("BSU_CURRENCY") Is Nothing, "", "(Currency  " & Session("BSU_CURRENCY") & ")")%> 
                        <span class="profile-right">
                                <asp:Label ID="lbChildNameTop" runat="server"></asp:Label>
                            </span>
                    </h3>
                </div>
                <!-- Table  -->
                <div class="table-responsive">

    <asp:Label ID="lblerror" runat="server" CssClass="error"></asp:Label>
   <%-- <div class="mainheading">
        <div class="left">
            </div>
    </div>
  --%>


                <asp:GridView ID="gvFeeCollection" runat="server" AutoGenerateColumns="False" EmptyDataText="No Details"
                    SkinID="GridViewNormal" CellPadding="4" Width="100%" CssClass="table table-striped table-bordered table-responsive text-left my-orders-table">
                    <Columns>
                        <asp:BoundField DataField="FEE_DESCR"  HeaderText="Fee">
                            <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                             <ItemStyle  CssClass="boldFeehead"/>
                        </asp:BoundField>
                        <asp:BoundField DataField="OPENING" DataFormatString="{0:0.00}" HeaderText="Opening"
                            Visible="False">
                            <HeaderStyle HorizontalAlign="Right" />
                            <ItemStyle HorizontalAlign="Right" Width="89px" />
                            <FooterStyle HorizontalAlign="Right" />
                        </asp:BoundField>
                        <asp:BoundField DataField="MONTHLY_AMOUNT" DataFormatString="{0:0.00}" HeaderText="Amount">
                            <HeaderStyle HorizontalAlign="Right" />
                            <ItemStyle HorizontalAlign="Right" Width="89px" />
                            <FooterStyle HorizontalAlign="Right" />
                        </asp:BoundField>
                        <asp:BoundField DataField="CONC_AMOUNT" DataFormatString="{0:0.00}" HeaderText="Concession">
                            <HeaderStyle HorizontalAlign="Right" />
                            <ItemStyle HorizontalAlign="Right" Width="89px" />
                            <FooterStyle HorizontalAlign="Right" />
                        </asp:BoundField>
                        <asp:BoundField DataField="ADJUSTMENT" DataFormatString="{0:0.00}" HeaderText="Adjustment">
                            <HeaderStyle HorizontalAlign="Right" />
                            <ItemStyle HorizontalAlign="Right" Width="89px" />
                            <FooterStyle HorizontalAlign="Right" />
                        </asp:BoundField>
                        <asp:BoundField DataField="PAID_AMOUNT" DataFormatString="{0:0.00}" HeaderText="Paid">
                            <HeaderStyle HorizontalAlign="Right" />
                            <ItemStyle HorizontalAlign="Right" Width="89px" />
                            <FooterStyle HorizontalAlign="Right" />
                        </asp:BoundField>
                        <asp:BoundField DataField="CLOSING" DataFormatString="{0:0.00}" HeaderText="Due">
                            <HeaderStyle HorizontalAlign="Right" />
                            <ItemStyle HorizontalAlign="Right" Width="89px" />
                            <FooterStyle HorizontalAlign="Right" />
                        </asp:BoundField>
                    </Columns>
                </asp:GridView>
            
                    <div style="float:right;"><asp:Button id="btnPay" runat="server" Text="Pay Now" PostBackUrl="/Fees/FeeCollectionOnlineSibling.aspx" CssClass="btn btn-info"/></div>
                      </div>
                <!-- /Table  -->
            </div>

        </div>
    </div>

                        </div>
                    </div>
        </div>
  
</asp:Content>
