﻿<%@ Page Language="VB"  MasterPageFile="~/ParentMaster.master" AutoEventWireup="false" CodeFile="BookSalePaymentHistory.aspx.vb" Inherits="Fees_BookSalePaymentHistory" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphParent" runat="Server">

    <script src="../Scripts/jquery-1.9.1.js" type="text/javascript"></script>

     <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.js?1=2"></script>
    <link type="text/css" href="../Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
   <link href="../cssfiles/Popup.css" rel="stylesheet" />

    <script type="text/javascript" language="javascript">
        function CheckForPrint(id) {
            if (id != '') {
                var frmReceipt = "BookSaleReceipt.aspx";
                <%--if ($("#<%=hfTaxable.ClientID%>").val() == "1")
                    frmReceipt = "FeeReceipt_TAX.aspx";
                else
                    frmReceipt = "feereceipt.aspx";--%>
                //showModelessDialog('feereceipt.aspx?type=REC&id=' + id, '', "dialogWidth: 800px; dialogHeight: 700px; help: no; resizable: no; scroll: yes; status: no; unadorned: no;");
                //window.open(frmReceipt + '?type=REC&id=' + id, "_blank", "dialogWidth: 800px; dialogHeight: 700px; help: no; resizable: no; scroll: yes; status: no; unadorned: no;");
                //return false;
                return ShowWindowWithClose(frmReceipt + '?type=BRN&id=' + id, 'search', '45%', '85%')
                return false;
            }
            // else
            //alert('Sorry. There is some problem with the transaction');
        }
    </script>


        <div class="content margin-top30 margin-bottom60">
                    <div class="container">
                        <div class="row">


     <!-- Posts Block -->
    <div class="posts-block col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="my-account">
            
            <div class="bottom-padding">
                <div class="title-box">
                    <h3> Book Sale Payment History  <%= IIf(Session("BSU_CURRENCY") Is Nothing, "", "(Amount In " & Session("BSU_CURRENCY") & ")")%></h3>
                </div>
                <!-- Table  -->
                <div class="table-responsive">
    <asp:Label ID="lblerror" runat="server" CssClass="error"></asp:Label>
    <asp:HiddenField ID="hfTaxable" runat="server" Value="0" />
 <%--   <div class="mainheading">
        <div class="left">
            
        </div>
    </div>--%>
    <table class="table table-striped table-bordered table-responsive text-left my-orders-table" width="100%" border="1" >
        <tr class="matters">
            <td>
                From Date
            </td>
            <td>
                <asp:TextBox ID="txtFrom" runat="server" class="form-control">
                </asp:TextBox>
            </td>
            <td align="center">
                To Date
            </td>
            <td align="center">
                <asp:TextBox ID="txtTo" runat="server" class="form-control">
                </asp:TextBox>
            </td>
            <td>
                <asp:Button ID="btnsearch" runat="server" Text="Show History" ValidationGroup="s"
                    CssClass="btn btn-info" />
            </td>
        </tr>
       &nbsp;
        <tr class="matters">
            <td colspan="5">
                <asp:GridView ID="gvFeeCollection" runat="server" AutoGenerateColumns="False" SkinID="GridViewNormal" CssClass="table table-striped table-bordered table-responsive text-left my-orders-table"
                    Width="99%">
                    <columns>
                        <asp:TemplateField HeaderText="Rec.No.">
                            <ItemTemplate>
                                <asp:LinkButton ID="lbPrint" CssClass="MenuLink" runat="server" OnClientClick="<%# &quot;javascript:CheckForPrint('&quot;& Receiptencrypt(Container.DataItem(&quot;BSAH_NO&quot;))&&quot;');return false;&quot; %>"
                                    Text='<%# Bind("BSAH_NO") %>'></asp:LinkButton>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:BoundField DataField="BSAH_DATE" HeaderText="Receipt Date" DataFormatString="{0:dd/MMM/yyyy}"
                            HtmlEncode="False">
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="BSAH_SOURCE" HeaderText="Sale Type">
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField DataField="BSAH_INVOICE_NO" HeaderText="Ref. No.">
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField DataField="BSAH_NET_AMOUNT" DataFormatString="{0:0.00}" HeaderText="Amount">
                            <ItemStyle HorizontalAlign="Right" />
                            <HeaderStyle HorizontalAlign="Right" />
                        </asp:BoundField>
                      <%--  <asp:BoundField DataField="FCO_STATUS" HeaderText="Status">
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>--%>
                    </columns>
                </asp:GridView>
            </td>
        </tr>
    </table>
    <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtTo"
        Format="dd/MMM/yyyy">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtFrom"
        Format="dd/MMM/yyyy">
    </ajaxToolkit:CalendarExtender>

                    
                      </div>
                <!-- /Table  -->
            </div>

        </div>
    </div>

                        </div>
                    </div>
            </div>
  

    
    <script type="text/javascript" lang="javascript">
        function ShowWindowWithClose(gotourl, pageTitle, w, h) {
            $.fancybox({
                type: 'iframe',
                //maxWidth: 300,
                href: gotourl,
                //maxHeight: 600,
                fitToView: true,
                padding: 6,
                width: w,
                height: h,
                autoSize: false,
                openEffect: 'none',
                showLoading: true,
                closeClick: true,
                closeEffect: 'fade',
                'closeBtn': true,
                afterLoad: function () {
                    this.title = '';//ShowTitle(pageTitle);
                },
                helpers: {
                    overlay: { closeClick: false }, // prevents closing when clicking OUTSIDE fancybox 
                    title: { type: 'inside' }
                },
                onComplete: function () {
                    $("#fancybox-wrap").css({ 'top': '90px' });

                },
                onCleanup: function () {
                    var hfPostBack = $("#fancybox-frame").contents().find('#hfbPB').val();

                    if (hfPostBack == "Y")
                        window.location.reload(true);
                }
            });

            return false;
        }
        </script>
</asp:Content>
