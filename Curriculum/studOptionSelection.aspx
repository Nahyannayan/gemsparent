﻿<%@ Page Language="VB" MasterPageFile="~/ParentMaster.master" AutoEventWireup="false"
    CodeFile="studOptionSelection.aspx.vb" Inherits="ParentLogin_studOptionSelection"
    Title="Provisional Student Option Selection" MaintainScrollPositionOnPostback="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphParent" runat="Server">
<script language="javascript" type="text/javascript">
function showGuidlines()
{
window.open("../Curriculum/TwsOptionGuidelines.pdf");
return false
}
</script>
       <div class="content margin-top30 margin-bottom60">
        <div class="container">
            <div class="row">

                <!-- Posts Block -->

                <div class="posts-block col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="my-account">
    <div class="mainheading" style="display:none;">
        <div>
            Provisional Student Option Selection</div>
        
    </div>


                         <div class="title-box">
                            <h3> Provisional Student Option Selection
                            <span class="profile-right">
                               <asp:label ID="lbChildName" runat="server" CssClass="lblChildNameCss"></asp:label><br />
                                     <asp:LinkButton id="lnkGuidelines" forecolor="blue" OnClientClick="javascript:showGuidlines();"   runat="server">Guidelines</asp:LinkButton>
                            </span></h3>
                        </div>
                       
           
       
          
    <div>
        <div class="table-responsive" style="width: 100%; display: block !important;">
            <table id="tdOption" runat="server" border="0" cellpadding="5" cellspacing="0" class="table table-responsive text-left my-orders-table tableNoborder"
                width="100%">
                <tr class="trHeader border-bottom">
                    <td align="left">
                        <strong><asp:Label ID="lblAccText" runat="server" Text="Academic Year "></asp:Label></strong>
                    </td>
                    
                    <td align="left">
                        <asp:Label ID="lblAcdYear" runat="server"></asp:Label>
                    </td>

                    <td align="left">
                        <strong><asp:Label ID="lblGradeText" runat="server" Text="Grade"></asp:Label></strong>
                    </td>
                    
                    <td align="left">
                        <asp:Label ID="lblGrade" runat="server"></asp:Label>
                    </td>
                    
                </tr>
            </table>
        </div>
        <div style="width: 100%; display: block !important;">
            <asp:Literal ID="ltOptions" runat="server"></asp:Literal>
        </div>
        <div style="width: 100%; display: block !important;">
            <asp:Label ID="lblText" runat="server" Font-Size="XX-Small" Text="Use the drop down boxes to make PROVISIONAL option selections. You may only select one subject per option block. All BTEC subjects are automatically selected in two option blocks.">
            </asp:Label>
        </div>
        <div id="divTWS" runat="server">
              <div  class="alert alert-warning">
                            
                            <div>
                                &bull;ADDITIONAL STUDIES -  Additional class for Maths + English
                <br />
                                &bull;In Language option - <b>Students who have chosen French/ Urdu/ Art & design in Year 8, will continue with the same subjects for the language option.</b>
                <br />
                               
                                &bull;In Option 4 - <b>Arabic-A(MOE) is mandatory for all Arabic speakers.</b>
                            </div>
                        </div>
        </div>
        <div style="width: 100%; display: block;">
            <asp:Label ID="lblMin" runat="server" Font-Bold="True" Font-Overline="False"></asp:Label>
        </div>
        <table class="table table-striped table-bordered table-responsive text-left my-orders-table" width="100%" cellpadding="8" cellspacing="0">
            <tr>
                <td>
                    <asp:Label ID="lblOptionMsg" runat="server"></asp:Label>
                </td>
            </tr>
            <tr align="center" style="text-align: center !important;">
                <td align="center" style="text-align: center !important;">
                    <asp:GridView ID="gvOptions" runat="server" AutoGenerateColumns="False" Width="100%"
                       class="table table-striped table-bordered table-responsive text-left my-orders-table" BorderStyle="None" BorderWidth="0px">
                        <emptydatarowstyle wrap="True" horizontalalign="Center" />
                        <columns>
                            <asp:TemplateField HeaderText="Option">
                            <HeaderStyle HorizontalAlign="left" />
                            <ItemStyle HorizontalAlign="left"/>
                            <ItemTemplate>
                            <asp:Label ID="lblOption" Text='<%# BIND("OPT_DESCR")%>' runat="server" ></asp:Label>
                            </ItemTemplate>                    
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Subject">
                            <ItemTemplate>                    
                            <asp:DropDownList ID="ddlChoice1"  runat="server" 
                            OnSelectedIndexChanged="ddlChoice1_SelectedIndexChanged" AutoPostBack=true CssClass="form-control"></asp:DropDownList>
                              <br />                         
                            <asp:Label ID="lblMsg" Visible="false"  CssClass="alert-danger alert" SkinID="error" runat="server" ></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="left" />
                             <HeaderStyle HorizontalAlign="left"  />
                            </asp:TemplateField>                     
                            <asp:TemplateField HeaderText="OptId" Visible="false">
                            <ItemTemplate>
                            <asp:Label ID="lblOptId" Text='<%# BIND("SGO_OPT_ID")%>' runat="server" ></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="left" />
                            </asp:TemplateField>
                    </columns>
                    </asp:GridView>
                </td>
            </tr>
            <tr class="alert-danger alert" id="trSave" runat="server" visible="false">
                <td class="tdblankAll">
                    <asp:Label ID="lblError1" runat="server"></asp:Label>
                    <asp:Label ID="lblMessage" runat="server" Visible="False">Your provisional option selections have been successfully saved.
                      These choices are subject to official confirmation</asp:Label></td>
            </tr>
            <tr>
                <td style="text-align: center !important;">
                    <asp:Button ID="btnSave" runat="server" CssClass="btn btn-info" Text="Save" ValidationGroup="groupM1"
                        TabIndex="7"  />
                    <asp:Button ID="btnReset" runat="server" CssClass="btn btn-info" Text="Reset" ValidationGroup="groupM1"
                        TabIndex="7"  />
                </td>
            </tr>
        </table>
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="error" EnableViewState="False"
            ForeColor="" HeaderText="You must enter a value in the following fields:" SkinID="error"
            ValidationGroup="groupM1" style="text-align: left" />
        <asp:HiddenField id="hfACD_ID_NEXT" runat="server">
        </asp:HiddenField>
        <asp:HiddenField id="hfGRD_ID_NEXT" runat="server">
        </asp:HiddenField>
        <asp:HiddenField id="hfGender" runat="server">
        </asp:HiddenField>
        <asp:HiddenField id="hfSTM_ID" runat="server">
        </asp:HiddenField>
        <asp:HiddenField id="hfMinOptions" runat="server">
        </asp:HiddenField>
       </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
</asp:Content>
