﻿<%@ Page Language="VB" MasterPageFile="~/ParentMaster.master"  AutoEventWireup="false" CodeFile="GRADE_RUBRIC.aspx.vb" Inherits="ParentLogin_GRADE_RUBRIC" title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphParent" Runat="Server">
 
<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width=895
 style='width:671.2pt;margin-left:-.7pt;border-collapse:collapse;mso-yfti-tbllook:
 1184;mso-padding-alt:0in 0in 0in 0in'>
 <tr style='mso-yfti-irow:1;height:13.4pt'>
  <td width=827 nowrap colspan=7 valign=bottom style='width:620.2pt;border:
  solid windowtext 1.0pt;background:#B6DDE8;padding:0in 5.4pt 0in 5.4pt;
  height:13.4pt'>
  <p class=MsoNormal align=center style='text-align:center;line-height:115%'><b><span
  style='font-size:7.0pt;mso-bidi-font-size:22.0pt;line-height:115%;font-family:
  "Courier New";color:black'>GRADE RUBRIC<o:p></o:p></span></b></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:2;height:12.5pt'>
  <td width=135 nowrap valign=bottom style='width:101.05pt;border:solid windowtext 1.0pt;
  border-top:none;background:#F2DDDC;padding:0in 5.4pt 0in 5.4pt;height:12.5pt'>
  <p class=MsoNormal align=center style='text-align:center;line-height:115%'><b><span
  style='font-size:7.0pt;mso-bidi-font-size:16.0pt;line-height:115%;color:black'>10
  point scale<o:p></o:p></span></b></p>
  </td>
  <td width=179 nowrap valign=bottom style='width:134.0pt;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:#F2DDDC;padding:0in 5.4pt 0in 5.4pt;height:12.5pt'>
  <p class=MsoNormal align=center style='text-align:center;line-height:115%'><b><span
  style='font-size:7.0pt;mso-bidi-font-size:16.0pt;line-height:115%;color:black'>&gt;
  9.0<o:p></o:p></span></b></p>
  </td>
  <td width=164 nowrap valign=bottom style='width:123.35pt;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:#F2DDDC;padding:0in 5.4pt 0in 5.4pt;height:12.5pt'>
  <p class=MsoNormal align=center style='text-align:center;line-height:115%'><b><span
  style='font-size:7.0pt;mso-bidi-font-size:16.0pt;line-height:115%;color:black'>7.5
  - 9&gt;0<o:p></o:p></span></b></p>
  </td>
  <td width=130 nowrap valign=bottom style='width:97.85pt;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:#F2DDDC;padding:0in 5.4pt 0in 5.4pt;height:12.5pt'>
  <p class=MsoNormal align=center style='text-align:center;line-height:115%'><b><span
  style='font-size:7.0pt;mso-bidi-font-size:16.0pt;line-height:115%;color:black'>5.6
  - 7.4<o:p></o:p></span></b></p>
  </td>
  <td width=140 nowrap colspan=2 valign=bottom style='width:105.3pt;border-top:
  none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:#F2DDDC;padding:0in 5.4pt 0in 5.4pt;height:12.5pt'>
  <p class=MsoNormal align=center style='text-align:center;line-height:115%'><b><span
  style='font-size:7.0pt;mso-bidi-font-size:16.0pt;line-height:115%;color:black'>4.0
  - 5.5<o:p></o:p></span></b></p>
  </td>
  <td width=78 nowrap valign=bottom style='width:58.65pt;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:#F2DDDC;padding:0in 5.4pt 0in 5.4pt;height:12.5pt'>
  <p class=MsoNormal align=center style='text-align:center;line-height:115%'><b><span
  style='font-size:7.0pt;mso-bidi-font-size:16.0pt;line-height:115%;color:black'>&lt;4.0<o:p></o:p></span></b></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:3;height:12.5pt'>
  <td width=135 nowrap rowspan=2 style='width:101.05pt;border-top:none;
  border-left:solid windowtext 1.0pt;border-bottom:solid black 1.0pt;
  border-right:solid windowtext 1.0pt;background:#F2DDDC;padding:0in 5.4pt 0in 5.4pt;
  height:12.5pt'>
  <p class=MsoNormal align=center style='text-align:center;line-height:115%'><b><span
  style='font-size:7.0pt;mso-bidi-font-size:16.0pt;line-height:115%;color:black'>GRADE
  <o:p></o:p></span></b></p>
  </td>
  <td width=179 nowrap style='width:134.0pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:#F2DDDC;padding:0in 5.4pt 0in 5.4pt;height:12.5pt'>
  <p class=MsoNormal align=center style='text-align:center;line-height:115%'><b><span
  style='font-size:7.0pt;mso-bidi-font-size:16.0pt;line-height:115%;color:black'>A+<o:p></o:p></span></b></p>
  </td>
  <td width=164 nowrap style='width:123.35pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:#F2DDDC;padding:0in 5.4pt 0in 5.4pt;height:12.5pt'>
  <p class=MsoNormal align=center style='text-align:center;line-height:115%'><b><span
  style='font-size:7.0pt;mso-bidi-font-size:16.0pt;line-height:115%;color:black'>A<o:p></o:p></span></b></p>
  </td>
  <td width=130 nowrap style='width:97.85pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:#F2DDDC;padding:0in 5.4pt 0in 5.4pt;height:12.5pt'>
  <p class=MsoNormal align=center style='text-align:center;line-height:115%'><b><span
  style='font-size:7.0pt;mso-bidi-font-size:16.0pt;line-height:115%;color:black'>B<o:p></o:p></span></b></p>
  </td>
  <td width=140 nowrap colspan=2 style='width:105.3pt;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:#F2DDDC;padding:0in 5.4pt 0in 5.4pt;height:12.5pt'>
  <p class=MsoNormal align=center style='text-align:center;line-height:115%'><b><span
  style='font-size:7.0pt;mso-bidi-font-size:16.0pt;line-height:115%;color:black'>C<o:p></o:p></span></b></p>
  </td>
  <td width=78 nowrap style='width:58.65pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:#F2DDDC;padding:0in 5.4pt 0in 5.4pt;height:12.5pt'>
  <p class=MsoNormal align=center style='text-align:center;line-height:115%'><b><span
  style='font-size:7.0pt;mso-bidi-font-size:16.0pt;line-height:115%;color:black'>D<o:p></o:p></span></b></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:4;height:17.0pt'>
  <td width=179 nowrap style='width:134.0pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:#F2DDDC;padding:0in 5.4pt 0in 5.4pt;height:17.0pt'>
  <p class=MsoNormal align=center style='text-align:center;line-height:115%'><b><span
  style='font-size:7.0pt;mso-bidi-font-size:11.0pt;line-height:115%;color:black'>OUTSTANDING<o:p></o:p></span></b></p>
  </td>
  <td width=164 nowrap style='width:123.35pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:#F2DDDC;padding:0in 5.4pt 0in 5.4pt;height:17.0pt'>
  <p class=MsoNormal align=center style='text-align:center;line-height:115%'><b><span
  style='font-size:7.0pt;mso-bidi-font-size:11.0pt;line-height:115%;color:black'>EXCELLENT<o:p></o:p></span></b></p>
  </td>
  <td width=130 nowrap style='width:97.85pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:#F2DDDC;padding:0in 5.4pt 0in 5.4pt;height:17.0pt'>
  <p class=MsoNormal align=center style='text-align:center;line-height:115%'><b><span
  style='font-size:7.0pt;mso-bidi-font-size:11.0pt;line-height:115%;color:black'>GOOD<o:p></o:p></span></b></p>
  </td>
  <td width=140 nowrap colspan=2 style='width:105.3pt;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:#F2DDDC;padding:0in 5.4pt 0in 5.4pt;height:17.0pt'>
  <p class=MsoNormal align=center style='text-align:center;line-height:115%'><b><span
  style='font-size:7.0pt;mso-bidi-font-size:11.0pt;line-height:115%;color:black'>SATISFACTORY<o:p></o:p></span></b></p>
  </td>
  <td width=78 nowrap style='width:58.65pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:#F2DDDC;padding:0in 5.4pt 0in 5.4pt;height:17.0pt'>
  <p class=MsoNormal align=center style='text-align:center;line-height:115%'><b><span
  style='font-size:7.0pt;mso-bidi-font-size:11.0pt;line-height:115%;color:black'>SCOPE
  FOR IMPROVEMENT<o:p></o:p></span></b></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:5;height:28.25pt'>
  <td width=135 nowrap style='width:101.05pt;border:solid windowtext 1.0pt;
  border-top:none;background:#B6DDE8;padding:0in 5.4pt 0in 5.4pt;height:28.25pt'>
  <p class=MsoNormal align=center style='text-align:center;line-height:115%'><b><span
  style='font-size:7.0pt;mso-bidi-font-size:16.0pt;line-height:115%;color:black'>Skill
  Areas<o:p></o:p></span></b></p>
  </td>
  <td width=179 style='width:134.0pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:#B6DDE8;padding:0in 5.4pt 0in 5.4pt;height:28.25pt'>
  <p class=MsoNormal style='line-height:115%'><span style='font-size:7.0pt;
  mso-bidi-font-size:16.0pt;line-height:115%;color:black'>Knowledge/&nbsp;&nbsp;
  Understanding<o:p></o:p></span></p>
  </td>
  <td width=164 nowrap style='width:123.35pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:#B6DDE8;padding:0in 5.4pt 0in 5.4pt;height:28.25pt'>
  <p class=MsoNormal style='line-height:115%'><span style='font-size:7.0pt;
  mso-bidi-font-size:16.0pt;line-height:115%;color:black'>Application<o:p></o:p></span></p>
  </td>
  <td width=130 nowrap style='width:97.85pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:#B6DDE8;padding:0in 5.4pt 0in 5.4pt;height:28.25pt'>
  <p class=MsoNormal align=center style='text-align:center;line-height:115%'><span
  style='font-size:7.0pt;mso-bidi-font-size:16.0pt;line-height:115%;color:black'>HOTS<o:p></o:p></span></p>
  </td>
  <td width=219 nowrap colspan=3 style='width:163.95pt;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:#B6DDE8;padding:0in 5.4pt 0in 5.4pt;height:28.25pt'>
  <p class=MsoNormal align=center style='text-align:center;line-height:115%'><span
  style='font-size:7.0pt;mso-bidi-font-size:16.0pt;line-height:115%;color:black'>Communication<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:6;height:37.25pt'>
  <td width=135 style='width:101.05pt;border:solid windowtext 1.0pt;border-top:
  none;padding:0in 5.4pt 0in 5.4pt;height:37.25pt'>
  <p class=MsoNormal align=center style='text-align:center;line-height:115%'><span
  style='font-size:7.0pt;mso-bidi-font-size:36.0pt;line-height:115%;color:black'>&nbsp;&nbsp;
  A+<o:p></o:p></span></p>
  </td>
  <td width=179 style='width:134.0pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:37.25pt'>
  <p class=MsoNormal style='line-height:115%'><span style='font-size:7.0pt;
  mso-bidi-font-size:11.0pt;line-height:115%;color:black'>Thorough&nbsp;
  understanding of the concept<o:p></o:p></span></p>
  </td>
  <td width=164 style='width:123.35pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:37.25pt'>
  <p class=MsoNormal style='line-height:115%'><span style='font-size:7.0pt;
  mso-bidi-font-size:11.0pt;line-height:115%;color:black'>Fully applies the
  concepts in more complex/real world situations<o:p></o:p></span></p>
  </td>
  <td width=130 style='width:97.85pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:37.25pt'>
  <p class=MsoNormal style='line-height:115%'><span style='font-size:7.0pt;
  mso-bidi-font-size:11.0pt;line-height:115%;color:black'>Always uses effective
  reasoning and relevant thinking skills<o:p></o:p></span></p>
  </td>
  <td width=219 colspan=3 style='width:163.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid black 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:37.25pt'>
  <p class=MsoNormal style='line-height:115%'><span style='font-size:7.0pt;
  mso-bidi-font-size:11.0pt;line-height:115%;color:black'>Effectively uses/
  explains/represents /interprets the concepts learnt in various forms.
  (Diagrams/Graphs/tables /report<span class=GramE>&nbsp; etc</span>..)<o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:7;height:37.25pt'>
  <td width=135 style='width:101.05pt;border:solid windowtext 1.0pt;border-top:
  none;padding:0in 5.4pt 0in 5.4pt;height:37.25pt'>
  <p class=MsoNormal align=center style='text-align:center;line-height:115%'><span
  style='font-size:7.0pt;mso-bidi-font-size:36.0pt;line-height:115%;color:black'>A<o:p></o:p></span></p>
  </td>
  <td width=179 style='width:134.0pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:37.25pt'>
  <p class=MsoNormal style='line-height:115%'><span style='font-size:7.0pt;
  mso-bidi-font-size:11.0pt;line-height:115%;color:black'>Good understanding of
  the concept<o:p></o:p></span></p>
  </td>
  <td width=164 style='width:123.35pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:37.25pt'>
  <p class=MsoNormal style='line-height:115%'><span style='font-size:7.0pt;
  mso-bidi-font-size:11.0pt;line-height:115%;color:black'>Mostly applies the
  concepts in more complex/real world situations<o:p></o:p></span></p>
  </td>
  <td width=130 style='width:97.85pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:37.25pt'>
  <p class=MsoNormal style='line-height:115%'><span style='font-size:7.0pt;
  mso-bidi-font-size:11.0pt;line-height:115%;color:black'>Mostly uses effective
  reasoning and relevant thinking skills<o:p></o:p></span></p>
  </td>
  <td width=219 colspan=3 style='width:163.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid black 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:37.25pt'>
  <p class=MsoNormal style='line-height:115%'><span style='font-size:7.0pt;
  mso-bidi-font-size:11.0pt;line-height:115%;color:black'>Mostly uses/
  explains/represents /interprets the concepts learnt in various forms.
  (Diagrams/Graphs/tables etc<span class=GramE>..)</span><o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:8;height:39.5pt'>
  <td width=135 style='width:101.05pt;border:solid windowtext 1.0pt;border-top:
  none;padding:0in 5.4pt 0in 5.4pt;height:39.5pt'>
  <p class=MsoNormal align=center style='text-align:center;line-height:115%'><span
  style='font-size:7.0pt;mso-bidi-font-size:36.0pt;line-height:115%;color:black'>B<o:p></o:p></span></p>
  </td>
  <td width=179 style='width:134.0pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:39.5pt'>
  <p class=MsoNormal style='line-height:115%'><span style='font-size:7.0pt;
  mso-bidi-font-size:11.0pt;line-height:115%;color:black'>Considerable
  understanding of the concept<o:p></o:p></span></p>
  </td>
  <td width=164 style='width:123.35pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:39.5pt'>
  <p class=MsoNormal style='line-height:115%'><span style='font-size:7.0pt;
  mso-bidi-font-size:11.0pt;line-height:115%;color:black'>Applies the concepts
  in<span class=GramE>&nbsp; complex</span>/real world situations sometimes.<o:p></o:p></span></p>
  </td>
  <td width=130 style='width:97.85pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:39.5pt'>
  <p class=MsoNormal style='line-height:115%'><span style='font-size:7.0pt;
  mso-bidi-font-size:11.0pt;line-height:115%;color:black'>Occasionally uses
  effective reasoning and relevant procedures<o:p></o:p></span></p>
  </td>
  <td width=219 colspan=3 style='width:163.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid black 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:39.5pt'>
  <p class=MsoNormal style='line-height:115%'><span style='font-size:7.0pt;
  mso-bidi-font-size:11.0pt;line-height:115%;color:black'>Occasionally<span
  class=GramE>&nbsp; uses</span>/ explains/represents /interprets the concepts
  learnt in various forms. (Diagrams/Graphs/tables etc<span class=GramE>..)</span><o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:9;height:35.0pt'>
  <td width=135 style='width:101.05pt;border:solid windowtext 1.0pt;border-top:
  none;padding:0in 5.4pt 0in 5.4pt;height:35.0pt'>
  <p class=MsoNormal align=center style='text-align:center;line-height:115%'><span
  style='font-size:7.0pt;mso-bidi-font-size:36.0pt;line-height:115%;color:black'>C<o:p></o:p></span></p>
  </td>
  <td width=179 style='width:134.0pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:35.0pt'>
  <p class=MsoNormal style='line-height:115%'><span style='font-size:7.0pt;
  mso-bidi-font-size:11.0pt;line-height:115%;color:black'>Limited&nbsp; understanding
  of the concept<o:p></o:p></span></p>
  </td>
  <td width=164 style='width:123.35pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:35.0pt'>
  <p class=MsoNormal style='line-height:115%'><span style='font-size:7.0pt;
  mso-bidi-font-size:11.0pt;line-height:115%;color:black'>Needs help to&nbsp;
  apply&nbsp; the concepts in complex/real world situations<o:p></o:p></span></p>
  </td>
  <td width=130 style='width:97.85pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:35.0pt'>
  <p class=MsoNormal style='line-height:115%'><span style='font-size:7.0pt;
  mso-bidi-font-size:11.0pt;line-height:115%;color:black'>Needs help to&nbsp;
  use effective reasoning and relevant procedures<o:p></o:p></span></p>
  </td>
  <td width=219 colspan=3 style='width:163.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid black 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:35.0pt'>
  <p class=MsoNormal style='line-height:115%'><span style='font-size:7.0pt;
  mso-bidi-font-size:11.0pt;line-height:115%;color:black'>Struggles to<span
  class=GramE>&nbsp; use</span>/ explains/represents /interprets the concepts
  learnt in various forms. (Diagrams/Graphs/tables etc<span class=GramE>..)</span><o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:10;mso-yfti-lastrow:yes;height:39.5pt'>
  <td width=135 style='width:101.05pt;border:solid windowtext 1.0pt;border-top:
  none;padding:0in 5.4pt 0in 5.4pt;height:39.5pt'>
  <p class=MsoNormal align=center style='text-align:center;line-height:115%'><span
  style='font-size:7.0pt;mso-bidi-font-size:36.0pt;line-height:115%;color:black'>D<o:p></o:p></span></p>
  </td>
  <td width=179 style='width:134.0pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:39.5pt'>
  <p class=MsoNormal style='line-height:115%'><span style='font-size:7.0pt;
  mso-bidi-font-size:11.0pt;line-height:115%;color:black'>Poor or No&nbsp;
  understanding of the concept<o:p></o:p></span></p>
  </td>
  <td width=164 style='width:123.35pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:39.5pt'>
  <p class=MsoNormal style='line-height:115%'><span style='font-size:7.0pt;
  mso-bidi-font-size:11.0pt;line-height:115%;color:black'>Finds it too
  difficult to &nbsp;apply the concepts in&nbsp; complex/real world situations<o:p></o:p></span></p>
  </td>
  <td width=130 style='width:97.85pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:39.5pt'>
  <p class=MsoNormal style='line-height:115%'><span style='font-size:7.0pt;
  mso-bidi-font-size:11.0pt;line-height:115%;color:black'>Hardly uses effective
  reasoning and relevant procedures<o:p></o:p></span></p>
  </td>
  <td width=219 colspan=3 style='width:163.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid black 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:39.5pt'>
  <p class=MsoNormal style='line-height:115%'><span style='font-size:7.0pt;
  mso-bidi-font-size:11.0pt;line-height:115%;color:black'>Very limited ability to
  use/ explains/represents /interprets the concepts learnt in various forms.
  (Diagrams/Graphs/tables etc<span class=GramE>..)</span><o:p></o:p></span></p>
  </td>
 </tr>
 <![if !supportMisalignedColumns]>
 <tr height=0>
  <td width=135 style='border:none'></td>
  <td width=179 style='border:none'></td>
  <td width=164 style='border:none'></td>
  <td width=130 style='border:none'></td>
  <td width=16 style='border:none'></td>
  <td width=125 style='border:none'></td>
  <td width=78 style='border:none'></td>
 </tr>
 <![endif]>
</table>
</asp:Content>

