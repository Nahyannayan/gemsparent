Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class ParentLogin_GetLogo
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.Public)
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)
        Dim connectionstring As String = ConnectionManger.GetOASISConnectionString
        Dim ID As String = Request.QueryString("BSU_ID").ToString()
        Dim SqlText As String
        If ID = "131001" Then
            SqlText = "SELECT IMG_IMAGE FROM OASIS.dbo.BSU_IMAGES WHERE IMG_BSU_ID='" & ID & "' AND IMG_TYPE='CLM'"
        Else
            SqlText = "SELECT IMG_IMAGE FROM OASIS.dbo.BSU_IMAGES WHERE IMG_BSU_ID='" & ID & "' AND IMG_TYPE='LOGO'"
        End If

        Dim connection As New SqlConnection(connectionstring)
        Dim command As New SqlCommand(SqlText, connection)
        'open the database and get a datareader
        connection.Open()
        Dim dr As SqlDataReader = command.ExecuteReader()
        If dr.Read() Then
            Response.BinaryWrite(DirectCast(dr("IMG_IMAGE"), Byte()))
        End If
        connection.Close()
    End Sub
End Class
