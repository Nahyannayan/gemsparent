﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class ParentLogin_HTMLReprts_CBSESummativeReport
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Try
        If Page.IsPostBack = False Then
            'Session("SBSUID") = "121013"
            'Session("STU_ID") = "20152932"
            'Session("HFrsm_id") = "1005"
            'Session("HFrpf_id") = "111"
            'Session("RSM_ACD_ID") = "581"
            'Session("STU_GRD_ID") = "05"
            'Session("RPF_DESCR") = "SUMMATIVE ASSESSMENT 1"

            If Session("RPF_DESCR") = "SUMMATIVE REPORT 1" Then
                lblReportCard.Text = "TERM 1 REPORT"
            Else
                lblReportCard.Text = "TERM 2 REPORT"
            End If
            'Page.Title = FeeCollectionOnlineBB.GetTransportTitle() 
            gvSubject.Attributes.Add("bordercolor", "#ffffff")
            gvSch1.Attributes.Add("bordercolor", "#ffffff")
            gvSch2.Attributes.Add("bordercolor", "#ffffff")
            gvSch3.Attributes.Add("bordercolor", "#ffffff")
            gvSch4.Attributes.Add("bordercolor", "#ffffff")
            gvSch5.Attributes.Add("bordercolor", "#ffffff")

            gvScho1.Attributes.Add("bordercolor", "#ffffff")
            gvScho2.Attributes.Add("bordercolor", "#ffffff")
            gvScho3.Attributes.Add("bordercolor", "#ffffff")
            gvScho4.Attributes.Add("bordercolor", "#ffffff")
            gvScho5.Attributes.Add("bordercolor", "#ffffff")
            gvScho6.Attributes.Add("bordercolor", "#ffffff")

            BindRubric()

            BindHeader()
            CBSE_SUMMATIVE_ASSESSMENT_REPORT(Session("HFrsm_id"), Session("HFrpf_id"), Session("RSM_ACD_ID"), _
            Session("STU_ID"), Session("SBSUID"), Session("STU_GRD_ID"))
            If gvSubject.Rows.Count > 0 Then
                gvSubject.HeaderRow.Visible = False
            End If
            BindAttendance()

            If Session("SBSUID") = "123006" Then
                tdSch1.Visible = False
                tdSch2.Visible = False
            End If

            If Session("SBSUID") <> "121009" Then
                tdMusic.Visible = False
                tdMusic1.Visible = False
            End If

            If Session("SBSUID") = "121013" And (Session("STU_GRD_ID") = "05" Or Session("STU_GRD_ID") = "06" Or Session("STU_GRD_ID") = "07" Or Session("STU_GRD_ID") = "08") Then
                lblArt_H.Text = "ART/MUSIC/DANCE"
            Else
                lblArt_H.Text = "ART EDUCATION"
            End If

        End If
            'Catch ex As Exception
            '    UtilityObj.Errorlog(ex.Message)
            'End Try
    End Sub

    Sub BindRubric()
        Dim STR_CONN As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "EXEC [RPT].[GETGRADE_RUBRICS_ALL] " _
                                & "'" + Session("sBsu_id") + "'," _
                                & Session("RSM_ACD_ID") + "," _
                                & "'" + Session("STU_GRD_ID") + "'"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(STR_CONN, CommandType.Text, str_query)
        Try
            Dim dv As DataView = New DataView(ds.Tables(0))
            dv.RowFilter = "GR_FILL_GRID=2"
            gvScho1.DataSource = dv
            gvScho1.DataBind()

            lblSchHeader1.Text = dv.ToTable.Rows(0)("GRH_HEADER2")

            dv = New DataView(ds.Tables(0))
            dv.RowFilter = "GR_FILL_GRID=1"
            gvScho2.DataSource = dv
            gvScho2.DataBind()

            lblSchHeader2.Text = dv.ToTable.Rows(0)("GRH_HEADER2")

            dv = New DataView(ds.Tables(0))
            dv.RowFilter = "GR_FILL_GRID=3"
            gvScho3.DataSource = dv
            gvScho3.DataBind()

            lblschHeader3.Text = dv.ToTable.Rows(0)("GRH_HEADER2")

            dv = New DataView(ds.Tables(0))
            dv.RowFilter = "GR_FILL_GRID=4"
            gvScho4.DataSource = dv
            gvScho4.DataBind()

            lblschHeader4.Text = dv.ToTable.Rows(0)("GRH_HEADER2")

            dv = New DataView(ds.Tables(0))
            dv.RowFilter = "GR_FILL_GRID=5"
            gvScho5.DataSource = dv
            gvScho5.DataBind()

            lblschHeader5.Text = dv.ToTable.Rows(0)("GRH_HEADER2")


            dv = New DataView(ds.Tables(0))
            dv.RowFilter = "GR_FILL_GRID=6"
            gvScho6.DataSource = dv
            gvScho6.DataBind()

            lblschHeader6.Text = dv.ToTable.Rows(0)("GRH_HEADER2")

        Catch ex As Exception
        End Try

    End Sub

    Sub BindHeader()
        Dim STR_CONN As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim ds1 As New DataSet
        SqlHelper.FillDataset(STR_CONN, CommandType.Text, "EXEC RPT.BSUHEADER '" & Session("SBSUID") & "'", ds1, Nothing)

        imgLogo.ImageUrl = "~\ParentLogin\HTMLReprts\GetLogo.aspx?BSU_ID=" & Session("SBSUID")
        lblHeader1.Text = ds1.Tables(0).Rows(0)("BSU_HEADER1").ToString().ToLower()
        lblHeader2.Text = ds1.Tables(0).Rows(0)("BSU_HEADER2")
        lblHeader3.Text = ds1.Tables(0).Rows(0)("BSU_HEADER3")
        lblSchool.Text = ds1.Tables(0).Rows(0)("BSU_NAME")




    End Sub
    Private Function SetDataTable() As DataTable
        Dim dt As New DataTable
        Dim column As DataColumn
       
        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "SBG_DESCR"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "FA1"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "FA2"
        dt.Columns.Add(column)
        
        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "SA"
        dt.Columns.Add(column)


        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "TOTAL(FA+SA)"
        dt.Columns.Add(column)

        column = New DataColumn
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "TOTAL(FA)"
        dt.Columns.Add(column)

        Return dt
    End Function

    Private Sub CBSE_SUMMATIVE_ASSESSMENT_REPORT(ByVal RSM_ID As String, ByVal RPF_ID As String, ByVal ACD_ID As String, ByVal STU_ID As String, ByVal BSU_ID As String, ByVal GRD_ID As String)
        Dim str_query As String = "exec  RPT.CBSE_SUMMATIVE_ASSESSMENT_REPORT " _
                                & "'" + BSU_ID + "'," _
                                & ACD_ID + "," _
                                & RSM_ID + "," _
                                & RPF_ID + "," _
                                & STU_ID

        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        Dim dt As DataTable = SetDataTable()
        Dim strSubject As String = ""
        Dim dr As DataRow
        Dim bMajor As Boolean
        Dim i As Integer
        If ds.Tables(0).Rows.Count > 0 Then
            lblClass.Text = IIf(ds.Tables(0).Rows(0)("GRM_DISPLAY") Is System.DBNull.Value, "", ds.Tables(0).Rows(0)("GRM_DISPLAY"))
            lblAccYear.Text = IIf(ds.Tables(0).Rows(0)("ACY_DESCR") Is System.DBNull.Value, "", ds.Tables(0).Rows(0)("ACY_DESCR"))
            lblSection.Text = IIf(ds.Tables(0).Rows(0)("SCT_DESCR") Is System.DBNull.Value, "", ds.Tables(0).Rows(0)("SCT_DESCR"))
            lblID.Text = ds.Tables(0).Rows(0)("STU_NO")
            lblName.Text = ds.Tables(0).Rows(0)("STU_NAME").ToString().ToUpper()
            lblTeacher.Text = ds.Tables(0).Rows(0)("EMP_FNAME").ToString().ToUpper()


            lblGrade.Text = IIf(ds.Tables(0).Rows(0)("GRM_DISPLAY") Is System.DBNull.Value, "", ds.Tables(0).Rows(0)("GRM_DISPLAY"))
            lblGrade.Text = lblGrade.Text + IIf(ds.Tables(0).Rows(0)("SCT_DESCR") Is System.DBNull.Value, "", " - " & ds.Tables(0).Rows(0)("SCT_DESCR"))
            lblStudentNo.Text = ds.Tables(0).Rows(0)("STU_NO")
            lblStudentName.Text = ds.Tables(0).Rows(0)("STU_NAME").ToString().ToUpper()
            lblTeacher.Text = ds.Tables(0).Rows(0)("EMP_FNAME").ToString().ToUpper()
            H_SCT_ID.Value = IIf(ds.Tables(0).Rows(0)("STU_SCT_ID") Is System.DBNull.Value, "", ds.Tables(0).Rows(0)("STU_SCT_ID"))
            H_STARTDATE.Value = IIf(ds.Tables(0).Rows(0)("RPF_ATT_STARTDATE") Is System.DBNull.Value, "", ds.Tables(0).Rows(0)("RPF_ATT_STARTDATE"))
            H_ENDDATE.Value = IIf(ds.Tables(0).Rows(0)("RPF_ATT_ENDDATE") Is System.DBNull.Value, "", ds.Tables(0).Rows(0)("RPF_ATT_ENDDATE"))


            For i = 0 To ds.Tables(0).Rows.Count - 1

                With ds.Tables(0).Rows(i)
                    If .Item("SBG_bMAJOR") = True Then
                        If .Item("SBG_DESCR") <> strSubject Then
                            If strSubject <> "" Then
                                dt.Rows.Add(dr)
                            End If
                            strSubject = .Item("SBG_DESCR")
                            dr = dt.NewRow

                            dr.Item(0) = strSubject
                        End If
                        Select Case .Item("RSD_HEADER")
                            Case "FA1"
                                dr.Item(1) = .Item("RST_GRADING")
                            Case "FA2"
                                dr.Item(2) = .Item("RST_GRADING")
                            Case "SA"
                                dr.Item(3) = .Item("RST_GRADING")
                            Case "TOTAL(FA+SA)"
                                dr.Item(4) = .Item("RST_GRADING")
                            Case "TOTAL(FA)"
                                dr.Item(5) = .Item("RST_GRADING")
                        End Select
                    Else
                        Select Case .Item("RSD_HEADER")
                            Case "WORK EXPERIENCE"
                                lblWork.Text = .Item("RST_GRADING")
                            Case "ART EDUCATION"
                                lblArt.Text = .Item("RST_GRADING")
                            Case "MUSIC"
                                lblMusic.Text = .Item("RST_GRADING")
                            Case "PHYSICAL AND HEALTH EDUCATION"
                                lblPe.Text = .Item("RST_GRADING")
                        End Select
                    End If
                End With
            Next
            dt.Rows.Add(dr)
            gvSubject.DataSource = dt
            gvSubject.DataBind()

        End If

        BindScholasticAreas()
    End Sub

    Sub BindScholasticAreas()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "exec [RPT].[CBSE_SUMMATIVE_ASSESSMENT_REPORT_SUB_HTML] " _
                                & Session("HFrpf_id") + "," _
                                & Session("STU_ID")
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        Dim dv As New DataView(ds.Tables(0))
        dv.RowFilter = "RSD_PARENT='LIFE SKILLS'"
        gvSch1.DataSource = dv
        gvSch1.DataBind()

        dv.RowFilter = "RSD_PARENT='ATTITUDES & VALUES'"
        gvSch2.DataSource = dv
        gvSch2.DataBind()

        dv.RowFilter = "RSD_PARENT='SOCIAL QUALITIES'"
        gvSch3.DataSource = dv
        gvSch3.DataBind()

        dv.RowFilter = "RSD_PARENT='CO-CURRICULAR ACTIVITIES'"
        gvSch4.DataSource = dv
        gvSch4.DataBind()

        dv.RowFilter = "RSD_PARENT='HEALTH STATUS/PHYSICAL EDUCATION'"
        gvSch5.DataSource = dv
        gvSch5.DataBind()


        'str_query = "exec [RPT].[CBSE_SUMMATIVE_ASSESSMENT_REPORT_SUB3] " _
        '                       & Session("HFrpf_id") + "," _
        '                       & Session("STU_ID")
        'ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        'Dim i As Integer
        'For i = 0 To ds.Tables(0).Rows.Count - 1
        '    With ds.Tables(0).Rows(i)
        '        Select Case .Item("RSD_HEADER").ToString.ToUpper
        '            Case "HEIGHT"
        '                lblHeight.Text = "<U>" + .Item("RST_COMMENTS") + "</U>"
        '            Case "WEIGHT"
        '                lblWeight.Text = "<U>" + .Item("RST_COMMENTS") + "</U>"
        '            Case "BLOOD GROUP"
        '                lblBG.Text = "<U>" + .Item("RST_COMMENTS") + "</U>"
        '            Case "VISION L"
        '                lblVision_L.Text = "<U>" + .Item("RST_COMMENTS") + "</U>"
        '            Case "VISION R"
        '                lblVision_R.Text = "<U>" + .Item("RST_COMMENTS") + "</U>"
        '            Case "TEETH"
        '                lblTeeth.Text = "<U>" + .Item("RST_COMMENTS") + "</U>"
        '            Case "ORAL HYGIENE"
        '                lblOral.Text = "<U>" + .Item("RST_COMMENTS") + "</U>"
        '            Case "SPECIFIC AILMENT, IF ANY"
        '                lblAilment.Text = .Item("RST_COMMENTS")
        '        End Select
        '    End With
        'Next

    End Sub

    Sub BindAttendance()
        Try
            Dim str_query As String = "EXEC RPT.ATTENDANCE_ALL " _
                                   & Session("STU_ID") + "," _
                                   & Session("RSM_ACD_ID") + "," _
                                   & Session("STU_GRD_ID") + "," _
                                   & H_SCT_ID.Value + "," _
                                   & "'" + Format(Date.Parse(H_STARTDATE.Value), "dd/MMM/yyyy") + "'," _
                                   & "'" + Format(Date.Parse(H_ENDDATE.Value), "dd/MMM/yyyy") + "'," _
                                   & "'" + Session("sbsuid") + "'"


            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            lblAttnd.Text = ds.Tables(0).Rows(0)("tot_att")
            lblWD.Text = ds.Tables(0).Rows(0)("tot_marked")
        Catch ex As Exception
        End Try
    End Sub

    Protected Sub gvSubject_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.Header Then

            Dim HeaderGrid As GridView = DirectCast(sender, GridView)
            Dim HeaderGridRow As New GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert)
            Dim HeaderCell As New TableCell()

            '  If hfRow.Value = 1 Then
            HeaderCell.Text = "SUBJECT"
            HeaderCell.Font.Bold = True
            HeaderCell.RowSpan = 2
            HeaderGridRow.Cells.Add(HeaderCell)
            HeaderCell = New TableCell()
            HeaderCell.Height = 20
            HeaderCell.Text = "FA1"
            HeaderCell.Font.Bold = True
            HeaderGridRow.Cells.Add(HeaderCell)
            HeaderCell = New TableCell()
            HeaderCell.Height = 20
            HeaderCell.Text = "FA2"
            HeaderCell.Font.Bold = True
            HeaderGridRow.Cells.Add(HeaderCell)
            HeaderCell = New TableCell()
            HeaderCell.Height = 20
            HeaderCell.Text = "TOTAL(FA)"
            HeaderCell.Font.Bold = True
            HeaderGridRow.Cells.Add(HeaderCell)
            HeaderCell = New TableCell()
             HeaderCell.Height = 20
            HeaderCell.Text = "SA"
            HeaderCell.Font.Bold = True
            HeaderGridRow.Cells.Add(HeaderCell)
            HeaderCell = New TableCell()
            HeaderCell.Height = 20
            HeaderCell.Text = "TOTAL(FA+SA)"
            HeaderCell.Font.Bold = True
            HeaderGridRow.Cells.Add(HeaderCell)

            gvSubject.Controls(0).Controls.AddAt(0, HeaderGridRow)

            HeaderGridRow = New GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert)

            ' End If
            ' If hfRow.Value = 2 Then
            HeaderCell = New TableCell()
             HeaderCell.Height = 20
            HeaderCell.Text = "10%"
            HeaderCell.Font.Bold = True
            HeaderGridRow.Cells.Add(HeaderCell)
            HeaderCell = New TableCell()
             HeaderCell.Height = 20
            HeaderCell.Text = "10%"
            HeaderCell.Font.Bold = True
            HeaderGridRow.Cells.Add(HeaderCell)
            HeaderCell = New TableCell()
            HeaderCell.Height = 20
            HeaderCell.Text = "20%"
            HeaderCell.Font.Bold = True
            HeaderGridRow.Cells.Add(HeaderCell)
            If Session("RPF_DESCR") = "SUMMATIVE REPORT 1" Then
                HeaderCell = New TableCell()
                HeaderCell.Height = 20
                HeaderCell.Text = "20%"
                HeaderCell.Font.Bold = True
                HeaderGridRow.Cells.Add(HeaderCell)
                HeaderCell = New TableCell()
                HeaderCell.Height = 20
                HeaderCell.Text = "40%"
                HeaderCell.Font.Bold = True
                HeaderGridRow.Cells.Add(HeaderCell)
            Else
                HeaderCell = New TableCell()
                HeaderCell.Height = 20
                HeaderCell.Text = "40%"
                HeaderCell.Font.Bold = True
                HeaderGridRow.Cells.Add(HeaderCell)
                HeaderCell = New TableCell()
                HeaderCell.Height = 20
                HeaderCell.Text = "60%"
                HeaderCell.Font.Bold = True
                HeaderGridRow.Cells.Add(HeaderCell)
            End If
            gvSubject.Controls(0).Controls.AddAt(1, HeaderGridRow)

            ' End If
        End If

    End Sub

End Class
