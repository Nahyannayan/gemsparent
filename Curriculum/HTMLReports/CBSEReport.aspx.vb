Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class ParentLogin_CBSEReport
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("username") & "" = "" Then
                Response.Redirect("~/login.aspx")
            End If

            If Page.IsPostBack = False Then
                'Page.Title = FeeCollectionOnlineBB.GetTransportTitle() 
                gvSubject.Attributes.Add("bordercolor", "#000095")
                CBSE_FORMATIVE_ASSESSMENT_REPORT(Session("HFrsm_id"), Session("HFrpf_id"), Session("RSM_ACD_ID"), _
                Session("STU_ID"), Session("SBSUID"), Session("STU_GRD_ID"))
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Private Sub CBSE_FORMATIVE_ASSESSMENT_REPORT(ByVal RSM_ID As String, ByVal RPF_ID As String, ByVal ACD_ID As String, ByVal STU_ID As String, ByVal BSU_ID As String, ByVal GRD_ID As String)
        Dim pParms(10) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@ACD_ID", SqlDbType.BigInt)
        pParms(0).Value = ACD_ID
        pParms(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 100)
        pParms(1).Value = Session("SBSUID")
        pParms(2) = New SqlClient.SqlParameter("@RSM_ID", SqlDbType.VarChar, 100)
        pParms(2).Value = RSM_ID
        pParms(3) = New SqlClient.SqlParameter("@STU_ID", SqlDbType.VarChar, 20)
        pParms(3).Value = STU_ID
        pParms(4) = New SqlClient.SqlParameter("@RPF_ID", SqlDbType.BigInt)
        pParms(4).Value = RPF_ID

        pParms(5) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(5).Direction = ParameterDirection.ReturnValue
        Dim ds As New DataSet
        Dim STR_CONN As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        ds = SqlHelper.ExecuteDataset(STR_CONN, _
        CommandType.StoredProcedure, "[RPT].[CBSE_FORMATIVE_ASSESSMENT_REPORT]", pParms)

        If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
            lblGrade.Text = IIf(ds.Tables(0).Rows(0)("GRM_DISPLAY") Is System.DBNull.Value, "", ds.Tables(0).Rows(0)("GRM_DISPLAY"))
            lblGrade.Text = lblGrade.Text + IIf(ds.Tables(0).Rows(0)("SCT_DESCR") Is System.DBNull.Value, "", " - " & ds.Tables(0).Rows(0)("SCT_DESCR"))
            Dim str_heading As String = String.Empty
            Dim str_gradeHeading As String = String.Empty
            Dim str_teacher As String = String.Empty
            tblClassTEacherComments.Visible = False
            Dim tempSBG_DESCR As String = ""
            Dim tempTest2Mark As String = ""

            For Each dr As DataRow In ds.Tables(0).Rows
                If Session("STU_GRD_ID").ToString = "11" OrElse Session("STU_GRD_ID").ToString = "12" Then
                    If dr("RST_ATTENDANCE").ToString <> "" Then
                        dr("AVG_GRD") = dr("RST_ATTENDANCE")
                        dr("RST_GRADING") = dr("RST_ATTENDANCE")
                    Else
                        If Session("SBSUID") = "121014" Then
                            If tempSBG_DESCR <> "" Then
                                If tempSBG_DESCR = dr("SBG_DESCR").ToString Then
                                    dr("AVG_GRD") = dr("RST_MARK").ToString.Replace(".00", "")
                                    dr("RST_GRADING") = tempTest2Mark
                                    tempSBG_DESCR = ""
                                    tempTest2Mark = ""
                                End If
                            Else
                                tempSBG_DESCR = dr("SBG_DESCR").ToString
                                tempTest2Mark = dr("RST_MARK").ToString.Replace(".00", "")
                            End If

                            '    If dr("RSD_HEADER").ToString = "Test 2" Then
                            '        dr("AVG_GRD") = dr("RST_MARK").ToString.Replace(".00", "")
                            '    End If
                        Else
                            dr("AVG_GRD") = dr("RST_MARK").ToString.Replace(".00", "")
                            dr("RST_GRADING") = dr("RRM_MAXMARK").ToString.Replace(".00", "")
                        End If
                    End If
                End If
                str_heading = dr("RPF_DESCR")
                str_teacher = dr("EMP_FNAME")
                str_gradeHeading = dr("GradeHeader")
                If dr("SBG_PARENTS").ToString() <> "NA" And dr("SBG_PARENTS").ToString() <> "" Then
                    dr("SBG_DESCR") = dr("SBG_PARENTS") & " - " & dr("SBG_DESCR")
                End If
                If dr("RSD_HEADER").ToString.ToUpper = "REMARKS" Then
                    If dr("RST_COMMENTS").ToString = "" Then
                        tblClassTEacherComments.Visible = False
                    Else
                        tblClassTEacherComments.Visible = True
                        lblTeacherRemarks.Text = dr("RST_COMMENTS").ToString
                    End If
                End If
            Next
            lblReportHeader.Text = str_heading

            lblTeacher.Text = str_teacher

            Dim dv As New DataView(ds.Tables(0))
            dv.RowFilter = "RSD_HEADER=HeaderGradeShow AND SBG_DESCR<>''"
            dv.Sort = "SBG_ORDER_PARENT"
            gvSubject.DataSource = dv
            gvSubject.DataBind()

            gvSubject.HeaderRow.Cells(1).Text = "ASSESSMENT <br /> (Grade)" 'str_gradeHeading
            If Session("STU_GRD_ID").ToString = "11" OrElse Session("STU_GRD_ID").ToString = "12" Then
                'gvSubject.Columns(2).Visible = False
                If Session("SBSUID") = "121014" Then
                    gvSubject.HeaderRow.Cells(2).Text = " Test 2 "
                    gvSubject.HeaderRow.Cells(1).Text = " Test 1&nbsp; "
                Else
                    gvSubject.HeaderRow.Cells(2).Text = " Marks&nbsp;Obtained "
                    gvSubject.HeaderRow.Cells(1).Text = " Total Marks&nbsp; "
                End If
            Else
                gvSubject.HeaderRow.Cells(2).Text = " Grade Average&nbsp; <br />" & "(Grade)"
            End If
            lblStudentNo.Text = ds.Tables(0).Rows(0)("STU_NO")
            lblStudentName.Text = ds.Tables(0).Rows(0)("STU_NAME").ToString().ToUpper()

            Dim ds1 As New DataSet
            SqlHelper.FillDataset(STR_CONN, CommandType.Text, "EXEC RPT.BSUHEADER '" & Session("SBSUID") & "'", ds1, Nothing)

            imgLogo.ImageUrl = "~\Curriculum\HTMLReports\GetLogo.aspx?BSU_ID=" & Session("SBSUID")
            lblHeader1.Text = ds1.Tables(0).Rows(0)("BSU_HEADER1").ToString().ToLower()
            lblHeader2.Text = ds1.Tables(0).Rows(0)("BSU_HEADER2")
            lblHeader3.Text = ds1.Tables(0).Rows(0)("BSU_HEADER3")
            lblSchool.Text = ds1.Tables(0).Rows(0)("BSU_NAME")
        End If
    End Sub

    Protected Sub gvSubject_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvSubject.RowDataBound
        For Each tc As TableCell In e.Row.Cells
            tc.Attributes("style") = "border-left: #000095 0pt solid; border-right: #000095 1pt dotted; border-top: #000095 1pt dotted; border-bottom: #000095 1pt dotted;"
        Next
    End Sub

End Class

