﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="CBSESummativeReport_01_04.aspx.vb"
    Inherits="ParentLogin_HTMLReprts_CBSESummativeReport" %>

<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <base target="_self" />
    <style type="text/css">
        img
        {
            display: block;
        }
        .ReceiptCaption
        {
            font-family: Arial, Helvetica, sans-serif;
            font-weight: bold;
            font-size: 12pt;
            color: #000095;
            height: 19px;
            text-decoration: underline;
        }
        .ReceiptCaptionTeacher
        {
            font-family: Arial, Helvetica, sans-serif;
            font-weight: bold;
            font-size: 10pt;
            color: #000095;
            height: 19px;
        }
        .matters_print
        {
            font-family: Arial, Helvetica, sans-serif;
            font-size: 13px;
            font-weight: bold;
            color: #000095;
        }
        .matters_heading
        {
            font-family: Arial, Helvetica, sans-serif;
            font-size: 14px;
            font-weight: bold;
            color: #000095;
        }
        .matters_normal
        {
            font-family: Arial, Helvetica, sans-serif;
            font-size: 11px;
            color: #000095;
            font-weight: bold;
        }
        .matters_normal_1
        {
            font-family: Arial, Helvetica, sans-serif;
            font-size: 9px;
            color: #ffffff;
            font-weight: bold;
        }
        .matters
        {
            font-family: Arial, Helvetica, sans-serif;
            font-size: 10px;
            color: #000095;
        }
        .matters_grid
        {
            font-family: Arial, Helvetica, sans-serif;
            font-size: 9px;
            color: #000095;
            text-indent: 8px;
            background-color: #f1f1f8;
            border-color: #ffffff;
        }
        .matters_gridalternative
        {
            font-family: Arial, Helvetica, sans-serif;
            font-size: 9px;
            color: #000095;
            text-indent: 8px;
            background-color: #dfdfe2;
            border-color: #ffffff;
        }
        .matters_gridheader
        {
            font-family: Arial, Helvetica, sans-serif;
            font-size: 9px;
            color: #ffffff;
            text-indent: 8px;
            background-color: #8286e2;
            border-color: #ffffff;
        }
        .matters_small
        {
            font-family: Arial, Helvetica, sans-serif;
            font-size: 12px;
            color: #000095;
        }
        .matters_small_1
        {
            font-family: Arial, Helvetica, sans-serif;
            font-size: 9px;
            color: #000095;
        }
        .Printbg
        {
            vertical-align: middle;
            font-family: Arial, Helvetica, sans-serif;
            font-size: 12px;
            color: #000095;
        }
        .PrintSource
        {
            vertical-align: bottom;
            font-family: Arial, Helvetica, sans-serif;
            font-size: 12px;
            color: #000095;
        }
    </style>

    <script language="javascript" type="text/javascript">
function PrintReceiptExport() 
    { 
        document.getElementById('tr_Print').style.display='none';
       
        window.print();
        document.getElementById('tr_Print').style.display='inline';
        try
        {
            if (/MSIE (\d+\.\d+);/.test(navigator.userAgent))
            {
            var ieversion=new Number(RegExp.$1) // capture x.x portion and store as a number
            if (ieversion>=8)
            window.close();
            } 
        }
        catch(ex){}        
    } 
function PrintReceipt() 
    { 
        if ('<%= Request.QueryString("isexport") %>'=='0') 
            PrintReceiptExport();
    } 
 function OnEscape() 
    { 
        if ( (event.keyCode==27) || (event.keyCode==99) || (event.keyCode==120) )
          window.close(); 
    }  
    </script>

</head>
<body onload="PrintReceipt();" onkeypress="OnEscape()" onkeydown="OnEscape()">
    <form id="form1" runat="server">
    <table align="center" border="0" cellpadding="3" cellspacing="0" width="100%" style="border-right: #000095 1pt solid;
        border-top: #000095 1pt solid; border-left: #000095 1pt solid; border-bottom: #000095 1pt solid">
        <tr valign="top" id="tr_Print">
            <td align="right" colspan="3">
                <img src="../Images/Misc/print.gif" onclick="PrintReceiptExport();" style="cursor: hand" alt="" />
            </td>
        </tr>
        <tr valign="top">
            <td rowspan="2">
                <table cellpadding="5" cellspacing="0" style="border-right: #000095 1pt solid; border-top: #000095 1pt solid;
                    border-left: #000095 1pt solid; border-bottom: #000095 1pt solid;height:700px;width:500px" >
                    <tr valign="top">
                        <td class="matters_print" align="center" colspan="2">
                            <u>GRADING SYSTEM</u>
                        </td>
                    </tr>
                    <tr style="height: 30px">
                        <td colspan="2">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td class="matters_print" align="left">
                        </td>
                          <td class="matters_print" align="left">
                        </td>
                    </tr>
                    <tr style="height: 20px">
                        <td colspan="2">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblSchHeader1" CssClass="matters_small_1" Width="200px" runat="server"
                                Text=""></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="lblSchHeader2" CssClass="matters_small_1" Width="200px" runat="server" Visible="False"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:GridView ID="gvScho1" runat="server" AutoGenerateColumns="False" CellPadding="4"
                                BorderColor="#dfdfe2" BorderWidth="1pt" Width="20%">
                                <AlternatingRowStyle CssClass="matters_gridalternative" />
                                <RowStyle CssClass="matters_grid" />
                                <HeaderStyle CssClass="matters_gridheader" />
                                <Columns>
                                    <asp:BoundField DataField="GR_MARK" HeaderText="Marks Rage">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Left" Wrap="false" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="GR_GRADE" HeaderText="Grade">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="GR_GRADE_POINT" HeaderText="Grade Point">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="GR_DESCR" HeaderText="Description">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="left" />
                                    </asp:BoundField>
                                </Columns>
                            </asp:GridView>
                        </td>
                        <td>
                            <asp:GridView ID="gvScho2" runat="server" AutoGenerateColumns="False" CellPadding="4"
                                BorderColor="#dfdfe2" BorderWidth="1pt" Width="20%" Visible="False">
                                <AlternatingRowStyle CssClass="matters_gridalternative" />
                                <RowStyle CssClass="matters_grid" />
                                <HeaderStyle CssClass="matters_gridheader" />
                                <Columns>
                                    <asp:BoundField DataField="GR_MARK" HeaderText="Marks Rage">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Left" Wrap="False" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="GR_GRADE" HeaderText="Grade">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="GR_GRADE_POINT" HeaderText="Grade Point">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="GR_DESCR" HeaderText="Description">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr style="height: 20px">
                        <td colspan="2">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td class="matters_print" align="left" colspan="2">
                            CO-SCHOLASTIC : PART I B &amp; PART II</td>
                    </tr>
                    <tr style="height: 20px">
                        <td colspan="2">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" >
                            <asp:Label ID="lblschHeader3" CssClass="matters_small_1" Width="200px" runat="server"
                                Text=""></asp:Label>
                        </td>
                          <td valign="top">
                            <asp:Label ID="lblschHeader4" CssClass="matters_small_1" Width="200px" runat="server"
                                Text="" Visible="false"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <asp:GridView ID="gvScho3" runat="server" AutoGenerateColumns="False" CellPadding="4"
                                BorderColor="#dfdfe2" BorderWidth="1pt" >
                                <AlternatingRowStyle CssClass="matters_gridalternative" />
                                <RowStyle CssClass="matters_grid" />
                                <HeaderStyle CssClass="matters_gridheader" />
                                <Columns>
                                    <asp:BoundField DataField="GR_GRADE" HeaderText="Grade">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="GR_DESCR" HeaderText="Description">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="left" />
                                    </asp:BoundField>
                                </Columns>
                            </asp:GridView>
                        </td>
                         <td valign="top">
                            <asp:GridView ID="gvScho4" runat="server" AutoGenerateColumns="False" CellPadding="4"
                                BorderColor="#dfdfe2" BorderWidth="1pt" Visible="false" >
                                <AlternatingRowStyle CssClass="matters_gridalternative" />
                                <RowStyle CssClass="matters_grid" />
                                <HeaderStyle CssClass="matters_gridheader" />
                                <Columns>
                                    <asp:BoundField DataField="GR_GRADE" HeaderText="Grade">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="GR_DESCR" HeaderText="Description">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="left" />
                                    </asp:BoundField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="matters_normal" style="font-size:7px">
                                              * Kindly note the passing criteria for Arabic/Islamic Studies/UAE Social Studies will be as per MOE regulations.&nbsp;</td>
                    </tr>
                </table>
            </td>
            <td>
                <table border="0" cellpadding="0" cellspacing="0" width="570px">
                    <tr>
                        <td align="center" style="height: 50px; width: 600px">
                            <asp:Image ID="imgLogo" runat="server" ImageAlign="AbsMiddle" />
                        </td>
                    </tr>
                    <tr valign="top">
                        <td align="center" class="matters_heading" style="font-size: 20px">
                            <asp:Label ID="lblSchool" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" style="font-size: 10px;height: 12px" class="matters_print" >
                            <asp:Label ID="lblHeader1" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" style="font-size: 10px" class="matters_print">
                            <asp:Label ID="lblHeader2" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" style="font-size: 10px" class="matters_print">
                            <asp:Label ID="lblHeader3" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="height: 40px">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td class="matters_print" align="center" style="font-size: 18px">
                            <asp:Label ID="lblReportCard" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="matters_print" align="center">
                            Academic Year :
                            <asp:Label ID="lblAccYear" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="matters_print" align="center">
                            Grade :
                            <asp:Label ID="lblClass" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr style="height: 40px">
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <table cellpadding="3" border="0">
                                <tr>
                                    <td align="left" class="matters_normal">
                                        Name of the Student
                                    </td>
                                    <td align="center" class="matters_print" style="width: 1px;">
                                        :
                                    </td>
                                    <td align="left" class="matters_print">
                                        <asp:Label ID="lblName" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" class="matters_normal">
                                        Student ID
                                    </td>
                                    <td align="center" class="matters_print" style="width: 1px;">
                                        :
                                    </td>
                                    <td align="left" class="matters_print">
                                        <asp:Label ID="lblID" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" class="matters_normal">
                                        Section
                                    </td>
                                    <td align="center" class="matters_print" style="width: 1px;">
                                        :
                                    </td>
                                    <td align="left" class="matters_print" colspan="1">
                                        <asp:Label ID="lblSection" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" class="matters_normal">
                                        Class Teacher
                                    </td>
                                    <td align="center" class="matters_print" style="width: 1px">
                                        :
                                    </td>
                                    <td align="left" class="matters_print" colspan="1">
                                        <asp:Label ID="lblTeacher" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" class="matters_print" colspan="2">
                                        Attendance
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" class="matters_normal">
                                        Total Attendance of Student
                                    </td>
                                    <td align="center" class="matters_print" style="width: 1px">
                                        :
                                    </td>
                                    <td align="left" class="matters_print" colspan="1">
                                        <asp:Label ID="lblAttnd" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" class="matters_normal">
                                        Total Working Days
                                    </td>
                                    <td align="center" class="matters_print" style="width: 1px">
                                        :
                                    </td>
                                    <td align="left" class="matters_print" colspan="1">
                                        <asp:Label ID="lblWD" runat="server"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr valign="bottom">
            <td align="right">
                <img src="../Images/gemslogo.jpg" alt="" />
            </td>
        </tr>
    </table>
    <div style="page-break-after:always">
        &nbsp;</div>
    <table border="0" cellspacing="0" cellpadding="2" width="100%">
        <tr>
            <td align="left" class="matters_normal">
                Name of the Student
            </td>
            <td align="center" class="matters_print" style="width: 1px;">
                :
            </td>
            <td align="left" class="matters_print">
                <asp:Label ID="lblStudentName" runat="server"></asp:Label>
            </td>
            <td align="left" class="matters_normal">
                Student ID
            </td>
            <td align="center" class="matters_print" style="width: 1px;">
                :
            </td>
            <td align="left" class="matters_print">
                <asp:Label ID="lblStudentNo" runat="server"></asp:Label>
            </td>
            <td align="left" class="matters_normal">
                Grade
            </td>
            <td align="center" class="matters_print" style="width: 1px;">
                :
            </td>
            <td align="left" class="matters_print" colspan="1">
                <asp:Label ID="lblGrade" runat="server"></asp:Label>
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td valign="top" align="center" colspan="10" style="border-top: #000095 1pt solid">
                <table align="center" border="0" cellspacing="0" cellpadding="2" width="70%px">
                    <tr>
                        <td colspan="2" class="matters_normal">
                            PART I : ACADEMIC PERFORMANCE : SCHOLASTIC AREAS
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" style="height: 19px; width: 50px;" align="left" class="matters_normal">
                            <table>
                                <tr>
                                    <td align="center" style="height: 40px; width: 40px; background: transparent url('../Images/round.gif');">
                                        1(A)
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td align="center" valign="top">
                            <asp:GridView ID="gvSubject" runat="server" AutoGenerateColumns="False" CellPadding="4"
                                BorderColor="#ffffff" BorderWidth="1pt" Width="100%">
                                <AlternatingRowStyle CssClass="matters_gridalternative" />
                                <RowStyle CssClass="matters_grid" />
                                <HeaderStyle CssClass="matters_gridheader" />
                                <Columns>
                                    <asp:BoundField DataField="SBG_DESCR" HeaderText="SUBJECTS">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="FA1" HeaderText="FA1">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="FA2" HeaderText="FA2">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                                                    
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                            <td class="matters_normal" style="font-size:8px;" colspan="10">
                  <br />
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="matters_normal">
                            <table>
                                <tr>
                                    <td align="center" style="height: 40px; width: 40px; background: transparent url('../Images/round.gif');">
                                        1(B)
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td>
                            <table border="0" width="100%" cellpadding="5" cellspacing="0" style="border-right: #ffffff 1pt solid;
                                border-top: #ffffff 1pt solid; border-left: #ffffff 1pt solid; border-bottom: #ffffff 1pt solid" >
                                <tr>
                                    <td class="matters_normal_1" style="background-color: #8286e2; border-right: #ffffff 1pt solid;
                                        border-bottom: #ffffff 1pt solid">
                                        COMPUTER SCIENCE
                                    </td>
                                    <td class="matters_normal_1" style="background-color: #8286e2; border-right: #ffffff 1pt solid;
                                        border-bottom: #ffffff 1pt solid">
                                        ART EDUCATION
                                    </td>
                                     <td class="matters_normal_1" style="border-right: #ffffff 1pt solid;background-color: #8286e2; border-bottom: #ffffff 1pt solid" id="tdMusic" runat="server">
                                        MUSIC
                                    </td>
                                     <td class="matters_normal_1" style="border-right: #ffffff 1pt solid;background-color: #8286e2; border-bottom: #ffffff 1pt solid" id="tdSwimming" runat="server">
                                        SWIMMING
                                    </td>
                                    <td class="matters_normal_1" style="background-color: #8286e2; border-bottom: #ffffff 1pt solid">
                                        PHYSICAL AND HEALTH EDUCATION
                                    </td>
                                </tr>
                                <tr>
                                    <td class="matters" style="background-color: #f1f1f8; border-right: #ffffff 1pt solid;">
                                        <asp:Label ID="lblWork" runat="server" Text=""></asp:Label>
                                    </td>
                                    <td class="matters" style="background-color: #f1f1f8; border-right: #ffffff 1pt solid;">
                                        <asp:Label ID="lblArt" runat="server" Text=""></asp:Label>
                                    </td>
                                      <td class="matters" style="background-color: #f1f1f8;" id="tdMusic1" runat="server">
                                        <asp:Label ID="lblMusic" runat="server" Text=""></asp:Label>
                                    </td>
                                    <td class="matters" style="background-color: #f1f1f8;" id="tdSwimming1" runat="server">
                                        <asp:Label ID="lblSwimming" runat="server" Text=""></asp:Label>
                                    </td>
                                    <td class="matters" style="background-color: #f1f1f8;">
                                        <asp:Label ID="lblPe" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="10">
            </td>
        </tr>
        <tr>
            <td colspan="10" align="center" valign="top">
             
                <table border="0" cellpadding="0" cellspacing="0" width="500px">
                    <tr>
                        <td class="matters_normal" colspan="2">
                            PART II : CO-SCHOLASTIC ACTIVITIES(PERSONALITY DEVELOPMENT)
                        </td>
                    </tr>
        
        <tr>
            <td class="matters_normal" align="center">
                <table border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td align="center" style="height: 40px; width: 40px; background: transparent url('../Images/round.gif');">
                            2(A)
                        </td>
                    </tr>
                </table>
            </td>
            <td align="left" class="matters_normal">
                <u>PERSONALITY DEVELOPMENT(5 POINT SCALE: A*,A,B,C,D)</u>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <asp:GridView ID="gvSch1" runat="server" AutoGenerateColumns="False" CellPadding="4"
                    BorderColor="#ffffff" BorderWidth="1pt" Width="80%">
                    <AlternatingRowStyle CssClass="matters_gridalternative" />
                    <RowStyle CssClass="matters_grid" />
                    <HeaderStyle CssClass="matters_gridheader" />
                    <Columns>
                        <asp:BoundField DataField="RSD_HEADER" HeaderText="PERSONAL & SOCIAL">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Left" Width="300px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="RST_COMMENTS" HeaderText="GRADE">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td class="matters_normal" align="center">
                <table border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td align="center" style="height: 40px; width: 40px; background: transparent url('../Images/round.gif');">
                            2(B)
                        </td>
                    </tr>
                </table>
            </td>
            <td align="left" class="matters_normal">
                <u>ATTITUDES & VALUES (3 POINT SCALE: A*,A,B)</u>
            </td>
        </tr>
        <tr>
            <td align="center" colspan="2">
                <asp:GridView ID="gvSch2" runat="server" AutoGenerateColumns="False" CellPadding="4"
                    BorderColor="#ffffff" BorderWidth="1pt" Width="80%">
                    <AlternatingRowStyle CssClass="matters_gridalternative" />
                    <RowStyle CssClass="matters_grid" />
                    <HeaderStyle CssClass="matters_gridheader" />
                    <Columns>
                        <asp:BoundField DataField="RSD_HEADER" HeaderText="QUALITY">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Left" Width="300px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="RST_COMMENTS" HeaderText="GRADE">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td class="matters_normal" align="center">
                <table border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td align="center" style="height: 40px; width: 40px; background: transparent url('../Images/round.gif');">
                            2(C)
                        </td>
                    </tr>
                </table>
            </td>
            <td align="left" class="matters_normal">
                <u>SOCIAL QUALITIES (5 POINT SCALE: A*,A,B,C,D)</u>
            </td>
        </tr>
        <tr>
            <td align="center" colspan="2">
                <asp:GridView ID="gvSch3" runat="server" AutoGenerateColumns="False" CellPadding="4"
                    BorderColor="#ffffff" BorderWidth="1pt" Width="80%">
                    <AlternatingRowStyle CssClass="matters_gridalternative" />
                    <RowStyle CssClass="matters_grid" />
                    <HeaderStyle CssClass="matters_gridheader" />
                    <Columns>
                        <asp:BoundField DataField="RSD_HEADER" HeaderText="QUALITY">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Left" Width="300px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="RST_COMMENTS" HeaderText="GRADE">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
    </table>
    </td> </tr> </table>
    <asp:HiddenField ID="H_SCT_ID" runat="server" />
    <asp:HiddenField ID="H_STARTDATE" runat="server" />
    <asp:HiddenField ID="H_ENDDATE" runat="server" />
    </form>
</body>
</html>
