﻿Imports Microsoft.ApplicationBlocks.Data
Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Partial Class ParentLogin_studOptionSelection
    Inherits System.Web.UI.Page



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("username") Is Nothing Then
            Session("Active_tab") = "Home"
            Session("Site_Path") = ""
            Response.Redirect("~\login.aspx")
        ElseIf Session("bPasswdChanged") = "False" Then
            Response.Redirect("~\UpdateInfo\ForcePasswordChange.aspx", False)
        ElseIf Session("bUpdateContactDetails") = "False" Then
            Response.Redirect(Session("ForceUpdate_stud"), False)
        End If

        If Not Page.IsPostBack Then
            BindGender()
            lbChildName.Text = Session("STU_NAME")
            'If Session("STU_GRD_ID") = "12" Then
            '    hfACD_ID_NEXT.Value = Session("STU_ACD_ID")
            '    lblAcdYear.Text = Session("ACY_DESCR")
            '    hfGRD_ID_NEXT.Value = Session("STU_GRD_ID")
            '    lblGrade.Text = "Y" + Session("STU_GRD_ID")
            'Else
            '    GetNextACD_ID(Session("STU_ACD_ID"), Session("STU_BSU_ID"))
            '    GetNextGrade(Session("STU_GRD_ID"), hfACD_ID_NEXT.Value, Session("STU_BSU_ID"))
            'End If
            GetNextACD_GRADE()
            hfSTM_ID.Value = 1
            BindOptions()
            BindOptionList()
            SetMinOption()
            If Session("sbsuid") <> "125010" Then
                lnkGuidelines.Visible = False
                divTWS.Visible = False
            End If

            If Session("sbsuid") <> "125005" Then
                lblText.Text = "Use the drop down boxes below to make PROVISIONAL option selections with your child."
            ElseIf hfGRD_ID_NEXT.Value = "09" Then
                lblText.Text = "Use the drop down boxes to make PROVISIONAL option selections. You may only select one subject per option block. BTEC Engineering will be automatically selected in two option blocks."
            End If
        End If
    End Sub
    Protected Sub ddlChoice1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ddlChoice1 As DropDownList = DirectCast(sender, DropDownList)
        Dim lblOptId As Label = TryCast(sender.FindControl("lblOptId"), Label)
        Dim lblOption As Label = TryCast(sender.FindControl("lblOption"), Label)
        Dim lblMsg1 As Label = TryCast(sender.FindControl("lblMsg"), Label)
        Dim strSbg As String
        If Session("sbsuid") = "125005" Then
            If hfGRD_ID_NEXT.Value = "09" Then
                If ddlChoice1.SelectedItem.Text.ToUpper.Contains("BTEC ENGINEERING") = True Then
                    SelectBtecSubjects(ddlChoice1.SelectedValue, lblOptId.Text, lblOption.Text, lblMsg1)
                Else
                    UnSelectBtecSubjects(ddlChoice1.SelectedValue, lblOptId.Text, lblOption.Text, lblMsg1)
                End If
            ElseIf ddlChoice1.SelectedItem.Text.ToUpper.Contains("BTEC") = True Or ddlChoice1.SelectedItem.Text.ToUpper.Contains("DOUBLE SCIENCE") = True Then
                SelectBtecSubjects(ddlChoice1.SelectedValue, lblOptId.Text, lblOption.Text, lblMsg1)
            Else
                UnSelectBtecSubjects(ddlChoice1.SelectedValue, lblOptId.Text, lblOption.Text, lblMsg1)
            End If
        ElseIf Session("Sbsuid") = "125010" Then
            strSbg = ddlChoice1.SelectedItem.Text.ToUpper
            If hfGRD_ID_NEXT.Value = "10" Then
                Select Case lblOption.Text.ToUpper
                    Case "OPTION 1", "OPTION 2", "OPTION 3"
                        SelectTwsSubjects(lblOption.Text.ToUpper, strSbg)
                End Select
            End If
        End If
    End Sub

    Sub SelectTwsSubjects(ByVal selectedoption As String, ByVal selectedsubject As String)

        'if option 1 2 or 3 is physics chemistry or biology option 4 should be 
        'if option 1 is physics option 2 should be chemistry and viceversa
        'if option i in accounting or sociology option 2 should be BS and option 3 is Economics

        Dim i As Integer
        Dim ddlChoice1 As DropDownList
        Dim ddlSubject As DropDownList
        Dim lblOption As Label
        Dim strSbg As String
        Dim bScience As Boolean = False
        For i = 0 To gvOptions.Rows.Count - 1
            lblOption = gvOptions.Rows(i).FindControl("lblOption")
            ddlChoice1 = gvOptions.Rows(i).FindControl("ddlChoice1")
            strSbg = ddlChoice1.SelectedItem.Text
            If strSbg = "PHYSICS" Or strSbg = "CHEMISTRY" Or strSbg = "BIOLOGY" Then
                bScience = True
            End If

            If selectedoption = "OPTION 1" And lblOption.Text.ToUpper = "OPTION 2" And selectedsubject = "PHYSICS" And strSbg <> "CHEMISTRY" Then
                If Not ddlChoice1.Items.FindByText("CHEMISTRY") Is Nothing Then
                    ddlChoice1.ClearSelection()
                    ddlChoice1.Items.FindByText("CHEMISTRY").Selected = True
                End If
            End If

            If selectedoption = "OPTION 1" And lblOption.Text.ToUpper = "OPTION 2" And (selectedsubject = "ACCOUNTANCY" Or selectedsubject = "SOCIOLOGY") And strSbg <> "BUSINESS STUDIES" Then
                If Not ddlChoice1.Items.FindByText("BUSINESS STUDIES") Is Nothing Then
                    ddlChoice1.ClearSelection()
                    ddlChoice1.Items.FindByText("BUSINESS STUDIES").Selected = True
                End If
            End If

            If selectedoption = "OPTION 1" And lblOption.Text.ToUpper = "OPTION 2" And (selectedsubject = "GEOGRAPHY" Or selectedsubject = "COMBINED SCIENCE") And strSbg <> "ECONOMICS" Then
                If Not ddlChoice1.Items.FindByText("ECONOMICS") Is Nothing Then
                    ddlChoice1.ClearSelection()
                    ddlChoice1.Items.FindByText("ECONOMICS").Selected = True
                End If
            End If


            If selectedoption = "OPTION 2" And lblOption.Text.ToUpper = "OPTION 1" And selectedsubject = "CHEMISTRY" And strSbg <> "PHYSICS" Then
                If Not ddlChoice1.Items.FindByText("PHYSICS") Is Nothing Then
                    ddlChoice1.ClearSelection()
                    ddlChoice1.Items.FindByText("PHYSICS").Selected = True
                End If
            End If

            If lblOption.Text.ToUpper = "OPTION 4" Then
                ddlSubject = gvOptions.Rows(i).FindControl("ddlChoice1")
            End If


        Next

        'If bScience = True Then
        '    If Not ddlSubject.Items.FindByText("GLOBAL PERSPECTIVES") Is Nothing Then
        '        ddlSubject.ClearSelection()
        '        ddlSubject.Items.FindByText("GLOBAL PERSPECTIVES").Selected = True
        '    End If
        'Else
        '    If Not ddlSubject.Items.FindByText("GENERAL SCIENCE") Is Nothing Then
        '        ddlSubject.ClearSelection()
        '        ddlSubject.Items.FindByText("GENERAL SCIENCE").Selected = True
        '    End If
        'End If

    End Sub

   


    Sub SelectBtecSubjects(ByVal sbg_id As String, ByVal opt_id As Integer, ByVal opt As String, ByVal lblMsg1 As Label)
        Dim i As Integer
        Dim ddlChoice1 As DropDownList
        Dim lblOptId As Label
        Dim lblMsg2 As Label
        Dim lblOption As Label
        For i = 0 To gvOptions.Rows.Count - 1
            ddlChoice1 = gvOptions.Rows(i).FindControl("ddlChoice1")
            lblOptId = gvOptions.Rows(i).FindControl("lblOptId")
            lblOption = gvOptions.Rows(i).FindControl("lblOption")
            lblMsg2 = gvOptions.Rows(i).FindControl("lblMsg")
            If lblOptId.Text <> opt_id And (Not ddlChoice1.Items.FindByValue(sbg_id) Is Nothing) Then
                ddlChoice1.ClearSelection()
                ddlChoice1.Items.FindByValue(sbg_id).Selected = True
                ddlChoice1.Enabled = False
                lblMsg2.Text = ddlChoice1.SelectedItem.Text + " is automatically selected in " + opt + " and  " + lblOption.Text

                lblMsg1.Text = ddlChoice1.SelectedItem.Text + " is automatically selected in " + opt + " and  " + lblOption.Text

                lblMsg2.Visible = True
                lblMsg1.Visible = True
            End If
        Next
    End Sub

    Sub UnSelectBtecSubjects(ByVal sbg_id As String, ByVal opt_id As Integer, ByVal opt As String, ByVal lblMsg1 As Label)
        Dim i As Integer
        Dim ddlChoice1 As DropDownList
        Dim lblOptId As Label
        Dim lblMsg2 As Label
        Dim lblOption As Label
        For i = 0 To gvOptions.Rows.Count - 1
            ddlChoice1 = gvOptions.Rows(i).FindControl("ddlChoice1")
            lblOptId = gvOptions.Rows(i).FindControl("lblOptId")
            lblOption = gvOptions.Rows(i).FindControl("lblOption")
            lblMsg2 = gvOptions.Rows(i).FindControl("lblMsg")
            If lblMsg2.Text <> "" And lblMsg2.Text = lblMsg1.Text And lblOptId.Text <> opt_id Then
                ddlChoice1.ClearSelection()
                ddlChoice1.Enabled = True
                lblMsg2.Text = ""

                lblMsg1.Text = ""

                lblMsg2.Visible = False
                lblMsg1.Visible = False
            End If
        Next
    End Sub

    Sub GetNextACD_GRADE()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT OPT_NEXTYEAR,OPT_ACD_ID_NEXT,OPT_GRD_ID_NEXT FROM OPL.ONLINEOPTION_SETTING WHERE OPT_ACD_ID='" + Session("STU_ACD_ID") + "' AND OPT_GRD_ID='" + Session("STU_GRD_ID") + "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        lblAcdYear.Text = ds.Tables(0).Rows(0).Item(0)
        hfACD_ID_NEXT.Value = ds.Tables(0).Rows(0).Item(1)
        lblGrade.Text = ds.Tables(0).Rows(0).Item(2)
        hfGRD_ID_NEXT.Value = ds.Tables(0).Rows(0).Item(2)
    End Sub

    Public Sub GetNextACD_ID(ByVal vACD_ID As String, ByVal vBSU_ID As String)

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "select ACD_ID, ACY_DESCR from dbo.ACADEMICYEAR_D " & _
        " INNER JOIN ACADEMICYEAR_M ON ACY_ID = ACD_ACY_ID" & _
        " where ACD_ACY_ID =( Select (ACD_ACY_ID + 1) FROM ACADEMICYEAR_D WHERE " & _
        "ACD_BSU_ID = '" & vBSU_ID & "' and ACD_ID = " & vACD_ID & ") AND ACD_BSU_ID = '" & vBSU_ID & "'"

        Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        While (dr.Read())
            lblAcdYear.Text = dr("ACY_DESCR")
            hfACD_ID_NEXT.Value = dr("ACD_ID")
        End While
        dr.Close()
    End Sub

    Public Sub GetNextGrade(ByVal vGRD_ID As String, ByVal vACD_ID As String, ByVal vBSU_ID As String)

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim str_query As String = "SELECT GRD_ID FROM GRADE_M  WHERE GRD_DISPLAYORDER = " & _
        " (select (GRD_DISPLAYORDER + 1) from GRADE_M WHERE GRD_ID = '" & vGRD_ID & "')"

        Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        While (dr.Read())
            lblGrade.Text = dr("GRD_ID")
            hfGRD_ID_NEXT.Value = dr("GRD_ID")
        End While

    End Sub

    Sub BindOptionList1()
        Dim i As Integer
        Dim j As Integer
        Dim lblOption As Label


        Dim ddlChoice1 As DropDownList

        Dim sb As New StringBuilder
        Dim sb1 As New StringBuilder

        sb.AppendLine("<table width=""100%"" cellpadding=""8"" cellspacing=""0"" class=""ContentInfo""  align=""center"" >")
        sb.AppendLine("<tr>")
        sb1.AppendLine("<tr valign=top>")
        For i = 0 To gvOptions.Rows.Count - 1
            lblOption = gvOptions.Rows(i).FindControl("lblOption")
            ddlChoice1 = gvOptions.Rows(i).FindControl("ddlChoice1")
            sb.AppendLine("<th align=""center"" class=""tdfields"" >" + lblOption.Text + "</th>")

            sb1.AppendLine("<td align=left>")
            For j = 1 To ddlChoice1.Items.Count - 1
                sb1.AppendLine(ddlChoice1.Items(j).Text + "<br><br>")
            Next
            sb1.AppendLine("</td>")
        Next
        sb.AppendLine("</tr>")
        sb1.AppendLine("</tr>")

        sb.AppendLine(sb1.ToString)
        sb.AppendLine("</table>")
        ltOptions.Text = sb.ToString
    End Sub



    Sub BindOptionList()
        Dim i As Integer
        Dim j As Integer
        Dim lblOption As Label

        Dim color As String() = {"#8B21BD", "#3E35AE", "#437161", "#C1843D", "#1C8EA7", "#F4AD9D", "#D076C2"}
        Dim p As Integer = 0
        Dim subjects As New ArrayList
        Dim colorsubjects As New Hashtable

        Dim ddlChoice1 As DropDownList

        Dim sb As New StringBuilder
        Dim sb1 As New StringBuilder

        sb.AppendLine("<table align=""center"" border=""1"" width=""100%""  cellpadding=""5""  cellspacing=""0"" class=""table table-striped table-bordered table-responsive text-left my-orders-table"">")
        sb.AppendLine("<tr>")
        sb1.AppendLine("<tr valign=""top"">")
        For i = 0 To gvOptions.Rows.Count - 1
            lblOption = gvOptions.Rows(i).FindControl("lblOption")
            ddlChoice1 = gvOptions.Rows(i).FindControl("ddlChoice1")
            sb.AppendLine("<th>" + lblOption.Text + "</th>")
            sb1.AppendLine("<td align=""left"" style=""vertical-align:top;"">")
            For j = 1 To ddlChoice1.Items.Count - 1
                If Session("sbsuid") = "125005" Then
                    If ddlChoice1.Items(j).Text.ToLower.Contains("btec") Or ddlChoice1.Items(j).Text.ToLower = "double science" Then
                        If hfGRD_ID_NEXT.Value = "09" Then
                            If ddlChoice1.Items(j).Text.ToLower.Contains("btec engineering") Then
                                If Not subjects Is Nothing Then
                                    If subjects.Contains(ddlChoice1.Items(j).Text) = True Then
                                        colorsubjects.Add(ddlChoice1.Items(j).Text, color(p))
                                        p += 1
                                    Else
                                        subjects.Add(ddlChoice1.Items(j).Text)
                                    End If
                                Else
                                    subjects.Add(ddlChoice1.Items(j).Text)
                                End If
                                sb1.AppendLine("<font color=" + ddlChoice1.Items(j).Text + "_color" + ">" + ddlChoice1.Items(j).Text + "</font><br><br>")
                            End If
                        Else
                            If Not subjects Is Nothing Then
                                If subjects.Contains(ddlChoice1.Items(j).Text) = True Then
                                    colorsubjects.Add(ddlChoice1.Items(j).Text, color(p))
                                    p += 1
                                Else
                                    subjects.Add(ddlChoice1.Items(j).Text)
                                End If
                            Else
                                subjects.Add(ddlChoice1.Items(j).Text)
                            End If
                            sb1.AppendLine("<font color=" + ddlChoice1.Items(j).Text + "_color" + ">" + ddlChoice1.Items(j).Text + "</font><br><br>")
                        End If
                    Else
                        sb1.AppendLine("<font color=#000000>" + ddlChoice1.Items(j).Text + "</font><br><br>")
                    End If
                Else
                    sb1.AppendLine("<font color=#000000>" + ddlChoice1.Items(j).Text + "</font><br><br>")
                End If
            Next
            sb1.AppendLine("</td>")
        Next
        sb.AppendLine("</tr>")
        sb1.AppendLine("</tr>")

        sb.AppendLine(sb1.ToString)
        sb.AppendLine("</table>")

        Dim str As String = sb.ToString
        Dim clr As New DictionaryEntry

        For Each clr In colorsubjects
            str = str.Replace(clr.Key.ToString + "_color", clr.Value.ToString)
        Next

        For i = 0 To subjects.Count - 1
            str = str.Replace(subjects(i) + "_color", "#000000")
        Next
        ltOptions.Text = str
    End Sub


    Sub BindOptions()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT DISTINCT SGO_OPT_ID,OPT_DESCR FROM " _
                                & " SUBJECTGRADE_OPTIONS_S AS A INNER JOIN " _
                                & " OPTIONS_M AS B ON A.SGO_OPT_ID=B.OPT_ID " _
                                & " INNER JOIN SUBJECTS_GRADE_S AS C ON A.SGO_SBG_ID=" _
                                & " C.SBG_ID WHERE SBG_GRD_ID='" + hfGRD_ID_NEXT.Value _
                                & "' AND SBG_STM_ID=" + hfSTM_ID.Value _
                                & " AND SBG_ACD_ID=" + hfACD_ID_NEXT.Value _
                                & " AND ISNULL(OPT_bSUPRESSONLINE,0)=0 " _
                                & " ORDER BY OPT_DESCR "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)


        gvOptions.DataSource = ds
        gvOptions.DataBind()
    End Sub

    Sub BindGender()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT STU_GENDER FROM STUDENT_M WHERE STU_ID=" + Session("STU_ID")
        hfGender.Value = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
    End Sub



    Sub BindSubjects(ByVal OptId As String, ByVal ddlSubject As DropDownList)

        ddlSubject.Items.Clear()

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT DISTINCT SBG_ID,CASE WHEN SBG_BSU_ID='125010' THEN UPPER(SBG_DESCR) ELSE SBG_DESCR END SBG_DESCR FROM " _
                      & " SUBJECTGRADE_OPTIONS_S AS A INNER JOIN " _
                      & " SUBJECTS_GRADE_S AS C ON A.SGO_SBG_ID=" _
                      & " C.SBG_ID WHERE SBG_GRD_ID='" + hfGRD_ID_NEXT.Value _
                      & "' AND SBG_STM_ID=" + hfSTM_ID.Value _
                      & " AND SBG_ACD_ID=" + hfACD_ID_NEXT.Value _
                      & " AND SGO_OPT_ID=" + OptId

        If Session("SBSUID") = "115002" Then
            If hfGender.Value = "M" Then
                str_query += " AND ISNULL(SGO_ALLOCATEDTO,'ALL') IN('ALL','BOYS')"
            Else
                str_query += " AND ISNULL(SGO_ALLOCATEDTO,'ALL') IN('ALL','GIRLS')"
            End If
        End If


        str_query += " ORDER BY SBG_DESCR "

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlSubject.DataSource = ds
        ddlSubject.DataTextField = "SBG_DESCR"
        ddlSubject.DataValueField = "SBG_ID"
        ddlSubject.DataBind()

        Dim li As New ListItem
        li.Text = "--"
        li.Value = "0"
        ddlSubject.Items.Insert(0, li)

    End Sub

    Sub ResetOptions()
        Dim i As Integer
        Dim ddlChoice1 As DropDownList
        Dim lblOption As Label
        Dim lblMsg As Label
        For i = 0 To gvOptions.Rows.Count - 1
            ddlChoice1 = gvOptions.Rows(i).FindControl("ddlChoice1")
            lblOption = gvOptions.Rows(i).FindControl("lblOption")
            ddlChoice1.ClearSelection()
            ddlChoice1.Enabled = True
            If Session("sbsuid") = "125010" Then
                If lblOption.Text.ToUpper = "OPTION 2" Then
                    ddlChoice1.Enabled = False
                End If
            End If
            lblMsg = gvOptions.Rows(i).FindControl("lblMsg")
            lblMsg.Text = ""
            lblMsg.Visible = False
        Next
    End Sub


    Function ValidateOptionList() As Boolean
        Dim ar As New ArrayList
        Dim i As Integer
        Dim ddlChoice1 As DropDownList
        Dim bUnique As Boolean = True
        Dim strError As String = ""
        Dim bBlank As Boolean = False
        For i = 0 To gvOptions.Rows.Count - 1
            ddlChoice1 = gvOptions.Rows(i).FindControl("ddlChoice1")
            If ddlChoice1.SelectedValue <> 0 Then
                If Session("sbsuid") = "125010" Then
                    If ar.Contains(ddlChoice1.SelectedValue.ToString) Then
                        bUnique = False
                    End If
                Else
                    If ar.Contains(ddlChoice1.SelectedValue.ToString) And ddlChoice1.Enabled = True Then
                        bUnique = False
                    End If
                End If
              
                ar.Add(ddlChoice1.SelectedValue.ToString)
            Else
                bBlank = True
            End If

        Next


        If bUnique = False Then
            strError = "Subjects have to be unique."
        End If
        If hfMinOptions.Value = "0" Then
            If bBlank = True Then
                If strError <> "" Then
                    strError += vbCrLf
                End If
                strError = "Please select all options."
            End If
        ElseIf Val(hfMinOptions.Value) > ar.Count Then
            If strError <> "" Then
                strError += vbCrLf
            End If
            strError = "Please select a minimum of " + hfMinOptions.Value.ToString + " options."
        End If
        If strError <> "" Then
            lblOptionMsg.Width = "800"
            lblOptionMsg.Text = strError
            lblOptionMsg.CssClass = "diverror"
            Return False
        End If
        Return True
    End Function
    Sub SaveData()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim i As Integer
        Dim str_query As String
        Dim lblOptID As Label
        Dim ddlChoice1 As DropDownList
        For i = 0 To gvOptions.Rows.Count - 1
            With gvOptions.Rows(i)
                lblOptID = .FindControl("lblOptID")
                ddlChoice1 = .FindControl("ddlChoice1")
            End With
            If Session("sbsuid") = "125010" Then
                str_query = "exec saveSTUDENT_OPTIONREQ " _
                           & Session("STU_ID") + "," _
                           & hfACD_ID_NEXT.Value + "," _
                           & "'" + hfGRD_ID_NEXT.Value + "'," _
                           & lblOptID.Text + "," _
                           & ddlChoice1.SelectedValue.ToString + "," _
                           & "0," _
                           & "'" + Session("username") + "'"
                SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
            Else
                If ddlChoice1.Enabled = True Then
                    str_query = "exec saveSTUDENT_OPTIONREQ " _
                               & Session("STU_ID") + "," _
                               & hfACD_ID_NEXT.Value + "," _
                               & "'" + hfGRD_ID_NEXT.Value + "'," _
                               & lblOptID.Text + "," _
                               & ddlChoice1.SelectedValue.ToString + "," _
                               & "0," _
                               & "'" + Session("username") + "'"
                    SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
                End If
            End If

        Next

        If Session("sbsuid") = "125010" Then
            str_query = " EXEC INSERT_EMAIL_OPTION '" + Session("sbsuid") + "'," + Session("STU_ID")
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
        End If


    End Sub
    Sub GetRequestedSubjects(ByVal opt_id As String, ByVal ddlChoice1 As DropDownList)
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT isnull(STR_SBG_ID,0),isnull(STR_CHOICE2,0) FROM STUDENT_OPTIONREQ_S WHERE " _
                             & " STR_STU_ID=" + Session("STU_ID") + " AND STR_ACD_ID=" + hfACD_ID_NEXT.Value _
                             & " AND STR_OPT_ID=" + opt_id
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        If ds.Tables(0).Rows.Count > 0 Then
            If Not ddlChoice1.Items.FindByValue(ds.Tables(0).Rows(0).Item(0)) Is Nothing Then
                ddlChoice1.Items.FindByValue(ds.Tables(0).Rows(0).Item(0)).Selected = True
            End If


            btnSave.Visible = False
            btnReset.Visible = False
            ' lblMessage.Visible = True
            lblOptionMsg.Width = "800"
            lblOptionMsg.Text = "Your provisional option selections have been successfully saved.These choices are subject to official confirmation"
            lblOptionMsg.CssClass = "divvalid"
        End If
    End Sub

    Sub SetMinOption()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT isnull(GRO_OPTIONS,0) FROM GRADE_MINOPTIONSETTING WHERE GRO_ACD_ID=" + hfACD_ID_NEXT.Value _
                                  & " AND GRO_GRD_ID='" + hfGRD_ID_NEXT.Value + "'"
        hfMinOptions.Value = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)

        If hfMinOptions.Value <> "0" And hfMinOptions.Value <> gvOptions.Rows.Count.ToString And hfMinOptions.Value <> "" Then
            lblMin.Text = "**Note: Please select a minimum of " + hfMinOptions.Value.ToString + " options."
        End If
    End Sub
    Protected Sub gvOptions_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvOptions.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim lblOptId As Label
            Dim lblOption As Label = e.Row.FindControl("lblOption")
            lblOptId = e.Row.FindControl("lblOptId")
            Dim ddlChoice1 As DropDownList
            ddlChoice1 = e.Row.FindControl("ddlChoice1")
            BindSubjects(lblOptId.Text, ddlChoice1)
            GetRequestedSubjects(lblOptId.Text, ddlChoice1)

            If Session("sbsuid") = "125010" Then
                If lblOption.Text.ToUpper = "OPTION 2" Then
                    ddlChoice1.Enabled = False
                End If
            End If
        End If
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        '---  lblError1.Text = ""
        If ValidateOptionList() = False Then
            Exit Sub
        End If
        SaveData()
        ' lblMessage.Visible = True
        lblOptionMsg.Width = "800"
        lblOptionMsg.Text = "Your provisional option selections have been successfully saved.These choices are subject to official confirmation"
        lblOptionMsg.CssClass = "divvalid"
        btnSave.Visible = False
        btnReset.Visible = False

    End Sub


    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        ResetOptions()
    End Sub
End Class
