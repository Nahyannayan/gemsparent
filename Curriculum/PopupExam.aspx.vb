﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports UtilityObj
Partial Class Curriculum_PopupExam
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            gvSubject.Attributes.Add("bordercolor", "#E0DBD7")
            gridbind()
        End If
    End Sub
    Protected Sub txtfile_Click(ByVal sender As Object, ByVal e As System.EventArgs)


        Dim i As Integer = Convert.ToInt32(sender.CommandArgument)

        Dim strQuery As String = "select es_fileName,es_Content,es_doc from dbo.EXAMSETUP where es_id=" & i

        Dim cmd As SqlCommand = New SqlCommand(strQuery)

        Dim dt As DataTable = GetDt(cmd)

        If dt IsNot Nothing Then

            download(dt)

        End If

    End Sub


    Protected Sub imgF_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim i As Integer = Convert.ToInt32(sender.CommandArgument)

        Dim strQuery As String = "select es_fileName,es_Content,es_doc from dbo.EXAMSETUP where es_id=" & i

        Dim cmd As SqlCommand = New SqlCommand(strQuery)

        Dim dt As DataTable = GetDt(cmd)

        If dt IsNot Nothing Then

            download(dt)

        End If
    End Sub
    Public Function GetDt(ByVal cmd As SqlCommand) As DataTable

        Dim dt As New DataTable
        Dim strConnString As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim con As New SqlConnection(strConnString)
        Dim sda As New SqlDataAdapter

        cmd.CommandType = CommandType.Text
        cmd.Connection = con
        Try
            con.Open()
            sda.SelectCommand = cmd
            sda.Fill(dt)
            Return dt
        Catch ex As Exception
            Response.Write(ex.Message)
            Return Nothing
        Finally
            con.Close()
            sda.Dispose()
            con.Dispose()
        End Try
    End Function
    Protected Sub download(ByVal dt As DataTable)
        Try
            Dim bytes() As Byte = CType(dt.Rows(0)("es_doc"), Byte())

            'Response.Buffer = True

            'Response.Charset = ""
            Dim fname As String = dt.Rows(0)("es_filename").ToString()
            'Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Clear()
            Response.ClearHeaders()
            Response.ContentType = dt.Rows(0)("es_Content").ToString()
            Response.AddHeader("content-disposition", "inline;filename=" & fname)
            Response.BinaryWrite(bytes)

            Response.Flush()

            Response.End()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try

    End Sub
    Public Sub gridbind()
        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim str_Sql As String = ""

            Dim ds As New DataSet

            str_Sql = "select distinct es_sbg_id,SBG_DESCR  from EXAMSETUP inner join SUBJECTS_GRADE_S on es_sbg_id =SBG_ID " _
              & " where es_acd_id=" + Session("STU_ACD_ID") + "and es_grd_id='" + Session("STU_GRD_ID") + "'"

            'str_Sql = "select distinct es_sbg_id,SBG_DESCR  from EXAMSETUP inner join SUBJECTS_GRADE_S on es_sbg_id =SBG_ID " _
            '  & " where es_acd_id=931 and es_grd_id='" + Session("STU_GRD_ID") + "'"

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

            If ds.Tables(0).Rows.Count > 0 Then
                gvSubject.DataSource = ds.Tables(0)
                gvSubject.DataBind()
            Else
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                gvSubject.DataSource = ds.Tables(0)
                Try
                    gvSubject.DataBind()
                Catch ex As Exception
                End Try
                Dim columnCount As Integer = gvSubject.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns. I use a dropdown list in one of the column so this was necessary.
                gvSubject.Rows(0).Cells.Clear()
                gvSubject.Rows(0).Cells.Add(New TableCell)
                gvSubject.Rows(0).Cells(0).ColumnSpan = columnCount
                gvSubject.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvSubject.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
            End If


        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub
    Protected Sub gvSubject_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvSubject.RowDataBound
        Try
            Dim str As String = ""
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim gv = DirectCast(e.Row.FindControl("gvPaper"), GridView)
                Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
                Dim str_Sql As String = ""

                Dim ds As New DataSet
                Dim lblsId = DirectCast(e.Row.FindControl("lblsId"), Label)



                str_Sql = "select es_id,es_paper,es_date,es_dur,es_cost,es_filename  from EXAMSETUP where es_sbg_id =" + lblsId.Text



                ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)

                If ds.Tables(0).Rows.Count > 0 Then
                    gv.DataSource = ds.Tables(0)
                    gv.DataBind()
                Else
                    ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                    gv.DataSource = ds.Tables(0)

                    gv.DataBind()
                End If

                Dim txtfile As LinkButton = e.Row.FindControl("txtfile")
                Dim img As ImageButton = e.Row.FindControl("imgF")
                Dim s As String = System.IO.Path.GetExtension(txtfile.Text)
                If s = ".xls" Then
                    img.ImageUrl = "~/images/xls.jpg"
                ElseIf s = ".doc" Then
                    img.ImageUrl = "~/images/doc.jpg"
                ElseIf s = ".pdf" Then
                    img.ImageUrl = "~/images/pdf.jpg"
                Else
                    img.ImageUrl = ""
                End If


                Dim ScriptManager1 As ScriptManager = DirectCast(Master.FindControl("ScriptManager1"), ScriptManager)
                ScriptManager1.RegisterPostBackControl(txtfile)
                ScriptManager1.RegisterPostBackControl(img)
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
  
End Class
