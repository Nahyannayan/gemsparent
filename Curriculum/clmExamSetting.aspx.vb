﻿Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Web.Security
Imports System.Collections
Partial Class Curriculum_clmExamSetting
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("username") Is Nothing Then
            Session("Active_tab") = "Home"
            Session("Site_Path") = ""
            Response.Redirect("~\General\Home.aspx")
        ElseIf Session("bPasswdChanged") = "False" Then
            Response.Redirect("~\UpdateInfo\ForcePasswordChange.aspx", False)
        ElseIf Session("bUpdateContactDetails") = "False" Then
            Response.Redirect("~\UpdateInfo\ForceUpdatecontactdetails.aspx", False)
        End If
        If Not Page.IsPostBack Then

            Session("totalAmount") = "0"

            btnPayNow.Visible = False
            'btnPayatSchool.Visible = False
            lblError.CssClass = ""
            txtStdNo.Text = Session("STU_NO")
            lbChildName.Text = Session("STU_NAME")
            stugrd.Text = Session("STU_GRD_ID")
            ' BindSubject()
            ' BindPaper()
            ViewState("str_selectedPapers") = ""
            ViewState("int_InsertExamSetupID") = ""
            gridSubjectBind()
            gridbind()
            check_BSU_ONLINE()
        End If
    End Sub

    Public Sub gridSubjectBind()
        ViewState("str_selectedPapers") = ""
        hidTotal.Value = 0
        lblTot.Text = Session("BSU_CURRENCY") + " " + "0"
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim param(3) As SqlClient.SqlParameter
        param(0) = New SqlClient.SqlParameter("@ACD_ID", Session("STU_ACD_ID"))
        param(1) = New SqlClient.SqlParameter("@STU_ID", Session("STU_ID"))
        param(2) = New SqlClient.SqlParameter("@GRD_ID", Session("STU_GRD_ID"))
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GET_EXAM_LEVELS", param)
        If ds.Tables(0).Rows.Count > 0 Then
            grdExamSetting.DataSource = ds
            grdExamSetting.DataBind()

            rowtopay.Visible = True
        Else
            rowtopay.Visible = False
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
            grdExamSetting.DataSource = ds.Tables(0)
            Try
                grdExamSetting.DataBind()
            Catch ex As Exception
            End Try
            Dim columnCount As Integer = grdExamSetting.Rows(0).Cells.Count
            'Call the clear method to clear out any controls that you use in the columns. I use a dropdown list in one of the column so this was necessary.
            grdExamSetting.Rows(0).Cells.Clear()
            grdExamSetting.Rows(0).Cells.Add(New TableCell)
            grdExamSetting.Rows(0).Cells(0).ColumnSpan = columnCount
            grdExamSetting.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            grdExamSetting.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
        End If

        'GET_EXAM_LEVELS
    End Sub
    Protected Sub grdExamSetting_RowDataBound(sender As Object, e As GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim hidSubject As HiddenField = DirectCast(e.Row.FindControl("hidSubject"), HiddenField)
            Dim hidexamlevel As HiddenField = DirectCast(e.Row.FindControl("hidexamlevel"), HiddenField)
            Dim grdSubject As GridView = DirectCast(e.Row.FindControl("grdSubject"), GridView)
            'GET_SUBJ_FEES
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim param(3) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@SBG_ID", hidSubject.Value)
            param(1) = New SqlClient.SqlParameter("@STU_ID", Session("STU_ID"))
            param(2) = New SqlClient.SqlParameter("@EXAM_LEVEL", hidexamlevel.Value)
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GET_SUBJ_FEES", param)

            grdSubject.DataSource = ds
            grdSubject.DataBind()
        End If

    End Sub

    Protected Sub chkFee_CheckedChanged(sender As Object, e As EventArgs)
        Dim obj As Object = sender.parent.parent
        Dim chkFee As CheckBox = DirectCast(obj.FindControl("chkFee"), CheckBox)
        Dim hidSubID As HiddenField = DirectCast(obj.FindControl("hidSubID"), HiddenField)
        Dim hdnIsApprovalRequired As HiddenField = DirectCast(obj.FindControl("hdnIsApprovalRequired"), HiddenField)
        Dim GRDRow As GridViewRow = obj
        Dim amount As String = GRDRow.Cells(2).Text
        'Dim chkApproveReq As String = GRDRow.Cells(3).Text
        If chkFee.Checked Then
            'Added By Nikunj Aghera - (30-Jan-2020)
            If hdnIsApprovalRequired.Value = "Yes" Then
                CheckIsApproveReq(hidSubID.Value)
            End If

            Dim totalAmount As Double = CInt(hidTotal.Value) + amount
            If ViewState("str_selectedPapers") = "" Then
                ViewState("str_selectedPapers") = ViewState("str_selectedPapers") & hidSubID.Value
            Else
                ViewState("str_selectedPapers") = ViewState("str_selectedPapers") & "," & hidSubID.Value
            End If
            lblTot.Text = totalAmount
            lblTot.Text = Session("BSU_CURRENCY") + " " + lblTot.Text
            hidTotal.Value = totalAmount
            Session("totalAmount") = totalAmount
        Else
            Dim totalAmount As Double = CInt(hidTotal.Value) - amount
            ViewState("str_selectedPapers") = ViewState("str_selectedPapers").ToString.Replace(hidSubID.Value, "")
            ViewState("str_selectedPapers") = ViewState("str_selectedPapers").ToString.Replace(",,", ",")


            ViewState("int_InsertExamSetupID") = ViewState("int_InsertExamSetupID").ToString.Replace(hidSubID.Value, "")
            ViewState("int_InsertExamSetupID") = ViewState("int_InsertExamSetupID").ToString.Replace("||", "|")

            lblTot.Text = totalAmount
            lblTot.Text = Session("BSU_CURRENCY") + " " + lblTot.Text
            hidTotal.Value = totalAmount
            Session("totalAmount") = totalAmount
        End If

        If hidTotal.Value = "0" Then
            btnSubmit.Visible = False
            'btnPayNow.Visible = True
            'btnPayatSchool.Visible = True
        Else
            btnSubmit.Visible = True
        End If

        gridpaper()
    End Sub

    Protected Sub btnPayatSchool_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPayatSchool.Click
        Try
            Dim constring As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Using con As New SqlConnection(constring)
                Using cmd As New SqlCommand("dbo.saveExamPaper2", con)
                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.Parameters.AddWithValue("@ID", 0)
                    cmd.Parameters.AddWithValue("@Ep_ACD_ID", Session("STU_ACD_ID"))
                    cmd.Parameters.AddWithValue("@EP_STU_ID", Session("STU_ID"))
                    cmd.Parameters.AddWithValue("@Ep_GRD_ID", Session("STU_GRD_ID"))
                    ' cmd.Parameters.AddWithValue("@ep_amount", getamount())
                    cmd.Parameters.AddWithValue("@ep_amount", hidTotal.Value)
                    cmd.Parameters.Add("@EP_ID", SqlDbType.VarChar, 100)
                    cmd.Parameters.AddWithValue("@Ep_ES_IDS", ViewState("str_selectedPapers"))
                    cmd.Parameters("@EP_ID").Direction = ParameterDirection.Output
                    con.Open()
                    cmd.ExecuteNonQuery()
                    con.Close()
                    '  lblError.Text = cmd.Parameters("@EP_ID").Value.ToString()
                End Using
            End Using
            gridbind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            lblError.CssClass = "text-danger"
            lblError.Text = "Request could not be processed"
        End Try
    End Sub

    Protected Sub gvPaper_RowDataBound(sender As Object, e As GridViewRowEventArgs)
        'grdSubject_pay
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim hidSubject_pay As HiddenField = DirectCast(e.Row.FindControl("hidSubject_pay"), HiddenField)
            Dim hidexamlevel_pay As HiddenField = DirectCast(e.Row.FindControl("hidexamlevel_pay"), HiddenField)
            Dim grdSubject_pay As GridView = DirectCast(e.Row.FindControl("grdSubject_pay"), GridView)
            'GET_SUBJ_FEES
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim param(3) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@SBG_ID", hidSubject_pay.Value)
            param(1) = New SqlClient.SqlParameter("@STU_ID", Session("STU_ID"))
            param(2) = New SqlClient.SqlParameter("@EXAM_LEVEL", hidexamlevel_pay.Value)
            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GET_SUBJ_FEES_FOR_PAY", param)
            grdSubject_pay.DataSource = ds
            grdSubject_pay.DataBind()
        End If

    End Sub
    Public Sub gridbind()
        Try
            'btnSubmit.Visible = False
            'btnPayNow.Visible = True
            'btnPayatSchool.Visible = True

            If Session("totalAmount") = "0" Then
                btnSubmit.Visible = False
                'btnPayNow.Visible = True
                'btnPayatSchool.Visible = True
            Else
                btnSubmit.Visible = True
            End If

            gridpaper()
            gridSubjectBind()
            getTotal()


        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub
    Public Sub gridpaper()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim ds As New DataSet
        Dim QRY As String = "GET_PAPER_DETAILS"

        'str_Sql = "select ep_id,sbg_descr,ep_papers,ep_amount from EXAMpaperset " _
        '          & " inner join SUBJECTS_GRADE_S on ep_sbg_id=sbg_id " _
        '& " WHERE ep_stu_id=" + Session("STU_ID") + " and ep_ACD_ID=" + Session("STU_ACD_ID") + " and ep_grd_id='" + Session("STU_GRD_ID") + "' "
        Dim param(3) As SqlClient.SqlParameter
        param(0) = New SqlClient.SqlParameter("@STU_ID", Session("STU_ID"))
        param(1) = New SqlClient.SqlParameter("@ACD_ID", Session("STU_ACD_ID"))
        param(2) = New SqlClient.SqlParameter("@GRD_ID", Session("STU_GRD_ID"))

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, QRY, param)

        If ds.Tables(0).Rows.Count > 0 Then
            gvPaper.DataSource = ds.Tables(0)
            gvPaper.DataBind()
        Else
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
            gvPaper.DataSource = ds.Tables(0)
            Try
                gvPaper.DataBind()
            Catch ex As Exception
            End Try
            Dim columnCount As Integer = gvPaper.Rows(0).Cells.Count
            'Call the clear method to clear out any controls that you use in the columns. I use a dropdown list in one of the column so this was necessary.
            gvPaper.Rows(0).Cells.Clear()
            gvPaper.Rows(0).Cells.Add(New TableCell)
            gvPaper.Rows(0).Cells(0).ColumnSpan = columnCount
            gvPaper.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvPaper.Rows(0).Cells(0).Text = "Your Search query does not match any records. Kindly try with some other keywords."
        End If
    End Sub
    Public Sub getTotal()

        Dim tot As Double
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "GET_EXAMSETUP_FINAL_AMOUNT"
        Dim param(3) As SqlClient.SqlParameter
        param(0) = New SqlClient.SqlParameter("@STU_ID", Session("STU_ID"))
        param(1) = New SqlClient.SqlParameter("@ACD_ID", Session("STU_ACD_ID"))
        param(2) = New SqlClient.SqlParameter("@GRD_ID", Session("STU_GRD_ID"))
        'str_query = "select isnull(sum(ep_amount),0) as amount from EXAMpaperset" _
        '             & " WHERE ep_stu_id=" + Session("STU_ID") + " and ep_ACD_ID=" + Session("STU_ACD_ID") + " and ep_grd_id='" + Session("STU_GRD_ID") + "' "

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, str_query, param)
        If ds.Tables(0).Rows.Count > 0 Then
            rowtopayschool.Visible = True
            tot = ds.Tables(0).Rows(0).Item("Amount")
            lblTotal.Text = Session("BSU_CURRENCY") + " " + Convert.ToString(tot)
        End If




    End Sub

    Private Sub check_BSU_ONLINE()

        Dim str_conn2 As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim pParms1(1) As SqlClient.SqlParameter
        pParms1(0) = New SqlClient.SqlParameter("@CAM_BSU_ID", Session("STU_BSU_ID"))
        Dim online As Int16 = SqlHelper.ExecuteScalar(str_conn2, CommandType.StoredProcedure, "Check_BSU_OnlinePayment", pParms1)
        If online = 1 Then
            btnPayNow.Visible = True
        Else
            btnPayNow.Visible = False
        End If

    End Sub

    'Public Sub BindSubject(ByRef ddlSubj As dropdownList)
    '    ddlSubj.Items.Clear()

    '    Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
    '    Dim str_query As String = "SELECT DISTINCT SBG_ID,CASE SBG_PARENTS_SHORT WHEN 'NA' THEN SBG_DESCR ELSE " _
    '                             & " SBG_PARENTS+'-'+SBG_SHORTCODE END AS SBG_DESCR FROM SUBJECTS_GRADE_S inner join EXAMSETUP on sbg_id=es_sbg_id" _
    '                             & " and es_acd_id=" + Session("STU_ACD_ID")


    '    str_query += " ORDER BY SBG_DESCR"
    '    Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
    '    ddlSubj.DataSource = ds
    '    ddlSubj.DataTextField = "SBG_DESCR"
    '    ddlSubj.DataValueField = "SBG_ID"
    '    ddlSubj.DataBind()

    'End Sub
    'Public Sub BindPaper(ByVal ddlPaper As checkboxlist)
    '    ddlPaper.Items.Clear()

    '    Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
    '    Dim str_query As String = "SELECT es_id,es_paper from EXAMSETUP where  es_sbg_id=" + ddlSubj.SelectedValue

    '    Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
    '    ddlPaper.DataSource = ds
    '    ddlPaper.DataTextField = "es_paper"
    '    ddlPaper.DataValueField = "es_id"
    '    ddlPaper.DataBind()


    'End Sub

    'Protected Sub ddlSubj_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSubj.SelectedIndexChanged
    '    BindPaper()
    'End Sub
    'Protected Sub ddlPaper_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPaper.SelectedIndexChanged
    '    Dim value As String = Nothing
    '    Dim i As Integer
    '    Dim tot As Double
    '    Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
    '    Dim str_query As String = ""
    '    Dim str_selectedPapers As String = ""
    '    Try
    '        For Each checkBox As ListItem In ddlPaper.Items
    '            If checkBox.Selected = True Then
    '                str_selectedPapers = str_selectedPapers + checkBox.Value + ","
    '                str_query = "select es_cost from EXAMSETUP where es_id= " + checkBox.Value

    '                Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

    '                tot += ds.Tables(0).Rows(0).Item("es_cost")


    '            End If

    '        Next

    '        ViewState("str_selectedPapers") = str_selectedPapers

    '        lblTot.Text = "AED " + Convert.ToString(tot)
    '    Catch ex As Exception
    '        Dim msg As String = "Select Error:"
    '        msg += ex.Message
    '        Throw New Exception(msg)
    '    End Try

    'End Sub
    'Function getPapers() As String
    '    Dim subs As String = ""
    '    Dim i As Integer

    '    For i = 0 To ddlPaper.Items.Count - 1
    '        If ddlPaper.Items(i).Selected = True Then
    '            If subs <> "" Then
    '                subs += ","
    '            End If
    '            subs += ddlPaper.Items(i).Text
    '        End If
    '    Next
    '    Return subs

    'End Function
    'Function getamount() As Double
    '    Dim subs As String = ""
    '    Dim i As Integer
    '    Dim tot As Double
    '    Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
    '    Dim str_query As String = ""

    '    For i = 0 To ddlPaper.Items.Count - 1
    '        If ddlPaper.Items(i).Selected = True Then

    '            str_query = "select es_cost from EXAMSETUP where es_id= " + ddlPaper.Items(i).Value
    '            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

    '            tot += ds.Tables(0).Rows(0).Item("es_cost")
    '        End If
    '    Next
    '    Return tot

    'End Function

    Protected Sub lnkRemove_Command(sender As Object, e As CommandEventArgs)
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim strans As SqlTransaction = Nothing
        Dim qry As String = "REMOVE_EXAM_FEE_SETUP"
        Dim param(3) As SqlClient.SqlParameter
        param(0) = New SqlClient.SqlParameter("@EPD_ID", e.CommandArgument)
        Dim objConn As New SqlConnection(str_conn)
        objConn.Open()
        strans = objConn.BeginTransaction
        Try
            SqlHelper.ExecuteNonQuery(strans, CommandType.StoredProcedure, qry, param)
            strans.Commit()
            gridSubjectBind()
            gridbind()
        Catch ex As Exception
            strans.Rollback()
        End Try

    End Sub

    'Added by Nikunj Aghera - (30-Jan-2020)
    Public Sub CheckIsApproveReq(ByVal intID As Int16)
        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
            Dim ds As New DataSet

            Dim pParms(3) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@MODE", "CHECK_IS_APPROVE")
            pParms(1) = New SqlClient.SqlParameter("@ESA_STU_ID", Session("STU_ID"))
            pParms(2) = New SqlClient.SqlParameter("@ESA_ES_ID", intID)
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[dbo].[saveExamSetup_Approve]", pParms)

            If ds.Tables(0).Rows.Count = 0 Then
                ViewState("int_InsertExamSetupID") = ViewState("int_InsertExamSetupID") & "|" & intID
                btnSubmit.Visible = True
                'btnPayNow.Visible = False
                'btnPayatSchool.Visible = False
            ElseIf Not ds.Tables(0).Rows(0)(0).ToString() = "Pending" Or Not ds.Tables(0).Rows(0)(0).ToString() = "Reject" Then
                ViewState("int_InsertExamSetupID") = ViewState("int_InsertExamSetupID") & "|" & intID
                btnSubmit.Visible = True
                'btnPayNow.Visible = False
                'btnPayatSchool.Visible = False
            Else
                btnSubmit.Visible = False
                'btnPayNow.Visible = True
                'btnPayatSchool.Visible = True
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Try
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString

            Dim intExamStpID() As String
            intExamStpID = ViewState("int_InsertExamSetupID").ToString.Split("|")
            Dim i As Integer
            For i = 0 To intExamStpID.Length - 1
                Dim ESA_ES_ID As String
                ESA_ES_ID = intExamStpID(i).ToString()
                If Not ESA_ES_ID = "" Then
                    Dim cmd As New SqlCommand
                    Dim objConn As New SqlConnection(str_conn)
                    objConn.Open()
                    cmd = New SqlCommand("[dbo].[saveExamSetup_Approve]", objConn)
                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.Parameters.AddWithValue("@ESA_ES_ID", ESA_ES_ID)
                    cmd.Parameters.AddWithValue("@ESA_STATUS", "Pending")
                    cmd.Parameters.AddWithValue("@ESA_DATE", String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now))
                    cmd.Parameters.AddWithValue("@ESA_USER_ID", Session("username"))
                    cmd.Parameters.AddWithValue("@ESA_STU_ID", Session("STU_ID"))
                    cmd.Parameters.AddWithValue("@MODE", "ADD")
                    cmd.ExecuteNonQuery()
                    cmd.Dispose()
                    objConn.Close()
                End If
            Next



            ' Add flag
            Try
                Dim constring As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
                Using con As New SqlConnection(constring)
                    Using cmd As New SqlCommand("dbo.saveExamPaper2", con)
                        cmd.CommandType = CommandType.StoredProcedure
                        cmd.Parameters.AddWithValue("@ID", 0)
                        cmd.Parameters.AddWithValue("@Ep_ACD_ID", Session("STU_ACD_ID"))
                        cmd.Parameters.AddWithValue("@EP_STU_ID", Session("STU_ID"))
                        cmd.Parameters.AddWithValue("@Ep_GRD_ID", Session("STU_GRD_ID"))
                        cmd.Parameters.AddWithValue("@EP_bCONFIRM", 1)
                        cmd.Parameters.AddWithValue("@ep_amount", hidTotal.Value)
                        cmd.Parameters.Add("@EP_ID", SqlDbType.VarChar, 100)
                        cmd.Parameters.AddWithValue("@Ep_ES_IDS", ViewState("str_selectedPapers"))
                        cmd.Parameters("@EP_ID").Direction = ParameterDirection.Output
                        con.Open()
                        cmd.ExecuteNonQuery()
                        con.Close()
                    End Using
                End Using
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                lblError.CssClass = "text-danger"
                lblError.Text = "Request could not be processed"
            End Try

            Session("totalAmount") = "0"

            gridbind()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try

    End Sub
    Protected Sub grdSubject_pay_RowDataBound(sender As Object, e As GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim hdn_ESA_STATUS As HiddenField = DirectCast(e.Row.FindControl("hdn_CurrentStatus"), HiddenField)
            Dim lnkRemove As LinkButton = DirectCast(e.Row.FindControl("lnkRemove"), LinkButton)
            'Dim lnkRemoveDummy As Label = DirectCast(e.Row.FindControl("lnkRemoveDummy"), Label)
            If hdn_ESA_STATUS.Value.ToString = "Approve" Then
                lnkRemove.Visible = False
                'lnkRemoveDummy.Visible = True
            End If
        End If
    End Sub
End Class
