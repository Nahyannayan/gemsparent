﻿Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Web.Security
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports System.Collections

Partial Class Curriculum_CurrReport
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64




    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            ' lblcurrMsg.Text = "Please use Internet Explorer to view the reportcards"
            'lblcurrMsg.CssClass = "divinfo"
            'lblcurrMsg.Width = 760

            If Session("username") Is Nothing Then
                Session("Active_tab") = "Home"
                Session("Site_Path") = ""
                Response.Redirect("~\Login.aspx")
            ElseIf Session("bPasswdChanged") = "False" Then
                Response.Redirect("~\UpdateInfo\ForcePasswordChange.aspx", False)
            ElseIf Session("bUpdateContactDetails") = "False" Then
                Response.Redirect(Session("ForceUpdate_stud"), False)
            End If

          
            If Not Page.IsPostBack Then
                lbChildName.Text = Session("STU_NAME")
                PopulateAcademicYear()
                BindReports()
                Session("STP_GRD_ID") = Session("STU_GRD_ID")
                Session("ReportYear") = ddlAcademicYear.SelectedValue.ToString
                Session("ReportYear_DESC") = ddlAcademicYear.SelectedItem.Text
            End If
          
        Catch ex As Exception
        End Try
    End Sub
    Public Function GetReportCardStatus(ByVal vRPF_ID As String, ByVal vSTU_ID As String) As Boolean
        Dim SLQSTRING As String = "select count(*) from OASIS.ONLINE.PROGRESS_REPORT_VIEWERS where pra_rpf_id = " & vRPF_ID & _
        " AND PRA_STU_ID = " & vSTU_ID
        Dim viewed_count As Integer = SqlHelper.ExecuteScalar(ConnectionManger.GetOASIS_CURRICULUMConnectionString, CommandType.Text, SLQSTRING)
        If viewed_count = 0 Then
            Return True
        Else
            Return False
        End If

    End Function

   

    Protected Sub lnkReport_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lnkReport As LinkButton = DirectCast(sender, LinkButton)
        Dim lblRpf As Label = TryCast(sender.FindControl("lblRpf"), Label)
        Dim lblRsm As Label = TryCast(sender.FindControl("lblRsm"), Label)
        Dim lblDate As Label = TryCast(sender.FindControl("lblDate"), Label)
        Dim lblReport As Label = TryCast(sender.FindControl("lblReport"), Label)
        Dim lblAcdId As Label = TryCast(sender.FindControl("lblAcdId"), Label)
        Dim imgNew As Image = TryCast(sender.FindControl("imgNew"), Image)

        Session("HFrpf_id") = lblRpf.Text
        Session("HFrsm_id") = lblRsm.Text
        Session("HFrpf_date") = lblDate.Text
        Session("RPF_DESCR") = lblReport.Text
        Session("RSM_ACD_ID") = lblAcdId.Text
        Session("Active_menu") = "PERFORMANCE GRAPH"
        imgNew.Visible = False
        'If rdView.Checked = True Then
        Session("downloadreport") = "0"

        'ElseIf rdDownload.Checked = True Then
        '    Session("downloadreport") = "1"
        'End If


        Dim connection As String = WebConfigurationManager.ConnectionStrings("OASIS_CURRICULUMConnectionString").ConnectionString
        Dim con As SqlConnection = New SqlConnection(connection)
        con.Open()
        Dim sqltran As SqlTransaction

        sqltran = con.BeginTransaction("trans")
        Try


            Save_Registration(sqltran)

            sqltran.Commit()

            Session("Active_menu") = Nothing
            ifSibDetail.Src = "/curriculum/clmPerformancereport_view.aspx"
            'ifSibDetail.Src = "https://school.gemsoasis.com/GEMSPARENT/curriculum/progressReportHome.aspx?id=" + Encr_decrData.Encrypt(Session("PRS_ID"))
        Catch ex As Exception


            sqltran.Rollback()
        End Try





        If rdView.Checked = True Then
            'Session("downloadreport") = "0"
            mpe.Show()
        ElseIf rdDownload.Checked = True Then
            ' Session("downloadreport") = "1"
            ifReport.Attributes("src") = Server.MapPath("~/ParentLogin/progressReportHome.aspx?id=" + Encr_decrData.Encrypt(Session("PRS_ID")))
            ' ifReport.Attributes("src") = "https://school.gemsoasis.com/GEMSPARENT/curriculum/progressReportHome.aspx?id=" + Encr_decrData.Encrypt(Session("PRS_ID"))
        End If

    End Sub
    Private Sub Save_Registration(sqltran As SqlTransaction)
        Dim pParms(16) As SqlClient.SqlParameter

        pParms(0) = New SqlClient.SqlParameter("@PRS_HFrpf_id", Session("HFrpf_id"))
        pParms(1) = New SqlClient.SqlParameter("@PRS_HFrsm_id", Session("HFrsm_id"))
        pParms(2) = New SqlClient.SqlParameter("@PRS_STP_GRD_ID", Session("STP_GRD_ID"))
        pParms(3) = New SqlClient.SqlParameter("@PRS_STU_BSU_ID", Session("STU_BSU_ID"))
        pParms(4) = New SqlClient.SqlParameter("@PRS_RSM_ACD_ID", Session("RSM_ACD_ID"))
        pParms(5) = New SqlClient.SqlParameter("@PRS_STU_ID", Session("STU_ID"))
        pParms(6) = New SqlClient.SqlParameter("@PRS_ReportYear_DESC", Session("ReportYear_DESC"))
        pParms(7) = New SqlClient.SqlParameter("@PRS_bAOLReport", Session("bAOLReport"))
        pParms(8) = New SqlClient.SqlParameter("@PRS_Active_menu", Session("Active_menu"))
        pParms(9) = New SqlClient.SqlParameter("@PRS_RPF_DESCR", Session("RPF_DESCR"))
        pParms(10) = New SqlClient.SqlParameter("@PRS_ReportYear", Session("ReportYear"))
        pParms(11) = New SqlClient.SqlParameter("@PRS_HFrpf_date", Session("HFrpf_date"))
        pParms(12) = New SqlClient.SqlParameter("@PRS_downloadreport", Session("downloadreport"))
        pParms(13) = New SqlClient.SqlParameter("@PRS_USR_NAME", Session("username"))

        pParms(14) = New SqlClient.SqlParameter("@PRS_ID", SqlDbType.VarChar, 200)
        pParms(14).Direction = ParameterDirection.Output
        SqlHelper.ExecuteNonQuery(sqltran, CommandType.StoredProcedure, "rpt.SAVE_PROGRESS_REPORT_SES", pParms)
        Dim PRS_ID As String = Convert.ToString(pParms(14).Value)
        Session("PRS_ID") = PRS_ID

    End Sub
    Sub PopulateAcademicYear()
        ddlAcademicYear.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim str_query As String

        If Session("sbsuid") = "123004" And Session("clm") = 50 Then
            str_query = " SELECT ACY_DESCR,ACD_ID FROM ACADEMICYEAR_M AS A INNER JOIN ACADEMICYEAR_D AS B" _
                       & " ON B.ACD_ACY_ID=A.ACY_ID INNER JOIN STUDENT_PROMO_S ON STP_ACD_ID=ACD_ID " _
                       & " WHERE ACD_BSU_ID='" + Session("STU_BSU_ID") + "'" _
                       & " AND STP_STU_ID=" + Session("STU_ID") _
                       & " AND ACD_ACY_ID>=19 ORDER BY ACY_ID"
        Else
            str_query = " SELECT ACY_DESCR,ACD_ID FROM ACADEMICYEAR_M AS A INNER JOIN ACADEMICYEAR_D AS B" _
                                      & " ON B.ACD_ACY_ID=A.ACY_ID WHERE ACD_BSU_ID='" + Session("STU_BSU_ID") + "' AND ACD_CLM_ID=" + Session("ACD_CLM_ID") _
                                      & " AND ACD_ACY_ID>=19 ORDER BY ACY_ID"
        End If
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlAcademicYear.DataSource = ds
        ddlAcademicYear.DataTextField = "acy_descr"
        ddlAcademicYear.DataValueField = "acd_id"
        ddlAcademicYear.DataBind()

        str_query = " SELECT ACY_DESCR,ACD_ID FROM ACADEMICYEAR_M AS A INNER JOIN ACADEMICYEAR_D AS B" _
                                 & " ON B.ACD_ACY_ID=A.ACY_ID WHERE ACD_CURRENT=1 AND ACD_BSU_ID='" + Session("STU_BSU_ID") + "' AND ACD_CLM_ID=" + Session("ACD_CLM_ID")
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        Dim li As New ListItem
        li.Text = ds.Tables(0).Rows(0).Item(0)
        li.Value = ds.Tables(0).Rows(0).Item(1)
        ddlAcademicYear.Items(ddlAcademicYear.Items.IndexOf(li)).Selected = True

    End Sub

    Sub SetStudentSelectedGrade()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT ISNULL(STP_GRD_ID,'" + Session("STU_GRD_ID") + "') FROM OASIS..STUDENT_PROMO_S WHERE STP_STU_ID=" + Session("STU_ID") + "AND STP_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString
        Session("STP_GRD_ID") = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
    End Sub

    Sub BindReports()

        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT dbo.title_case(RPF_DESCR) RPF_DESCR,RPF_ID,isnull(RPF_DATE,getdate())RPF_DATE ,RPF_RSM_ID,RPF_DESCR RPF,RSM_ACD_ID,RPP_RELEASEDATE FROM RPT.REPORT_PRINTEDFOR_M AS A " _
                             & " INNER JOIN RPT.REPORT_SETUP_M AS B ON A.RPF_RSM_ID=B.RSM_ID" _
                             & " INNER JOIN RPT.REPORT_STUDENTS_PUBLISH AS C ON A.RPF_ID=C.RPP_RPF_ID" _
                             & " where C.RPP_STU_ID=" + Session("STU_ID").ToString + "  AND C.RPP_bRELEASEONLINE=1 AND " _
                             & " DATEDIFF(DAY,C.RPP_RELEASEDATE,getdate())>=0 " _
                             & " AND RSM_ACD_ID=" + ddlAcademicYear.SelectedValue.ToString

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        dlReports.DataSource = ds
        dlReports.DataBind()
    End Sub
    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        BindReports()
        SetStudentSelectedGrade()
        Session("ReportYear") = ddlAcademicYear.SelectedValue.ToString
        Session("ReportYear_DESC") = ddlAcademicYear.SelectedItem.Text
    End Sub

    Protected Sub dlReports_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles dlReports.ItemDataBound
        If hfType.Value <> "ace" Then
            Dim lblRpf As Label
            Dim imgNew As Image
            Dim lnkReport As LinkButton
            lblRpf = e.Item.FindControl("lblRpf")
            imgNew = e.Item.FindControl("imgNew")
            lnkReport = e.Item.FindControl("lnkReport")

            If Not lblRpf Is Nothing Then
                Dim status As Boolean = GetReportCardStatus(lblRpf.Text, Session("STU_ID"))
                imgNew.Visible = status
               
            End If
        End If
    End Sub

    
End Class
