﻿<%@ Page Language="VB" MasterPageFile="~/ParentMaster.master"  AutoEventWireup="false" CodeFile="Grade_Rubrics_CBSE.aspx.vb" Inherits="ParentLogin_GRADE_RUBRIC" title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphParent" Runat="Server">
    <p class="MsoNormal" style="margin: 0in 0in 0pt">
        <u><span style="font-size: 14pt"><span style="font-family: Arial">Grade 1-4<?xml
            namespace="" ns="urn:schemas-microsoft-com:office:office" prefix="o" ?><o:p></o:p></span></span></u></p>
    <p class="MsoNormal" style="margin: 0in 0in 0pt">
        <o:p><SPAN style="FONT-SIZE: 11pt; FONT-FAMILY: Arial">&nbsp;</SPAN></o:p>
    </p>
    <table border="0" cellpadding="0" cellspacing="0" class="MsoNormalTable" style="font-size: 11pt;
        font-family: Arial; border-collapse: collapse; mso-yfti-tbllook: 1184; mso-padding-alt: 0in 0in 0in 0in">
        <tr style="height: 12.4pt; mso-yfti-irow: 0; mso-yfti-firstrow: yes">
            <td colspan="3" style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: black 1pt solid;
                padding-left: 5.4pt; background: #92d050; padding-bottom: 0in; border-left: black 1pt solid;
                width: 463.05pt; padding-top: 0in; border-bottom: black 1pt solid; height: 12.4pt"
                valign="top" width="617">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <b><span style="font-size: 14pt; font-family: 'Cambria','serif'">Islamic Studies / Arabic
                        (Group A ) Subjects<o:p></o:p></span></b></p>
            </td>
        </tr>
        <tr style="height: 24.1pt; mso-yfti-irow: 1">
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: black 1pt solid; width: 83.2pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 24.1pt; background-color: transparent"
                valign="top" width="111">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-size: 14pt; font-family: 'Cambria','serif'">Grade<o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 208pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 24.1pt; background-color: transparent"
                valign="top" width="277">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-size: 14pt; font-family: 'Cambria','serif'">Description<o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 171.85pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 24.1pt; background-color: transparent"
                valign="top" width="229">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-size: 14pt; font-family: 'Cambria','serif'">Percentage Mark<o:p></o:p></span></p>
            </td>
        </tr>
        <tr style="height: 12.4pt; mso-yfti-irow: 2">
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: black 1pt solid; width: 83.2pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 12.4pt; background-color: transparent"
                valign="top" width="111">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-size: 14pt; font-family: 'Cambria','serif'">A*<o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 208pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 12.4pt; background-color: transparent"
                valign="top" width="277">
                <p class="MsoNormal" style="margin: 0in 0in 0pt">
                    <span style="font-size: 14pt; font-family: 'Cambria','serif'">Outstanding<o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 171.85pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 12.4pt; background-color: transparent"
                valign="top" width="229">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-size: 14pt; font-family: 'Cambria','serif'">90% - 100%<o:p></o:p></span></p>
            </td>
        </tr>
        <tr style="height: 11.7pt; mso-yfti-irow: 3">
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: black 1pt solid; width: 83.2pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 11.7pt; background-color: transparent"
                valign="top" width="111">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-size: 14pt; font-family: 'Cambria','serif'">A<o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 208pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 11.7pt; background-color: transparent"
                valign="top" width="277">
                <p class="MsoNormal" style="margin: 0in 0in 0pt">
                    <span style="font-size: 14pt; font-family: 'Cambria','serif'">Excellent<o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 171.85pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 11.7pt; background-color: transparent"
                valign="top" width="229">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-size: 14pt; font-family: 'Cambria','serif'">75% - 89%<o:p></o:p></span></p>
            </td>
        </tr>
        <tr style="height: 12.4pt; mso-yfti-irow: 4">
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: black 1pt solid; width: 83.2pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 12.4pt; background-color: transparent"
                valign="top" width="111">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-size: 14pt; font-family: 'Cambria','serif'">B<o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 208pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 12.4pt; background-color: transparent"
                valign="top" width="277">
                <p class="MsoNormal" style="margin: 0in 0in 0pt">
                    <span style="font-size: 14pt; font-family: 'Cambria','serif'">Good<o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 171.85pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 12.4pt; background-color: transparent"
                valign="top" width="229">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-size: 14pt; font-family: 'Cambria','serif'">56% - 74%<o:p></o:p></span></p>
            </td>
        </tr>
        <tr style="height: 12.4pt; mso-yfti-irow: 5">
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: black 1pt solid; width: 83.2pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 12.4pt; background-color: transparent"
                valign="top" width="111">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-size: 14pt; font-family: 'Cambria','serif'">C<o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 208pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 12.4pt; background-color: transparent"
                valign="top" width="277">
                <p class="MsoNormal" style="margin: 0in 0in 0pt">
                    <span style="font-size: 14pt; font-family: 'Cambria','serif'">Satisfactory<o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 171.85pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 12.4pt; background-color: transparent"
                valign="top" width="229">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-size: 14pt; font-family: 'Cambria','serif'">50 % - 55%<o:p></o:p></span></p>
            </td>
        </tr>
        <tr style="height: 11.7pt; mso-yfti-irow: 6; mso-yfti-lastrow: yes">
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: black 1pt solid; width: 83.2pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 11.7pt; background-color: transparent"
                valign="top" width="111">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-size: 14pt; font-family: 'Cambria','serif'">D<o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 208pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 11.7pt; background-color: transparent"
                valign="top" width="277">
                <p class="MsoNormal" style="margin: 0in 0in 0pt">
                    <span style="font-size: 14pt; font-family: 'Cambria','serif'">Scope for improvement<span
                        style="color: #c00000">
                        <o:p></o:p>
                    </span></span>
                </p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 171.85pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 11.7pt; background-color: transparent"
                valign="top" width="229">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-size: 14pt; font-family: 'Cambria','serif'">Below 50%<o:p></o:p></span></p>
            </td>
        </tr>
    </table>
    <p class="MsoNormal" style="margin: 0in 0in 0pt">
        <b><span style="font-size: 14pt; font-family: 'Cambria','serif'">
        </span></b>
    </p>
    <p class="MsoNormal" style="margin: 0in 0in 0pt">
        <b><span style="font-size: 14pt; font-family: 'Cambria','serif'">
            <o:p>&nbsp;</o:p>
        </span></b>
    </p>
    <table border="0" cellpadding="0" cellspacing="0" class="MsoNormalTable" style="border-collapse: collapse;
        mso-yfti-tbllook: 1184; mso-padding-alt: 0in 0in 0in 0in">
        <tr style="height: 12.4pt; mso-yfti-irow: 0; mso-yfti-firstrow: yes">
            <td colspan="3" style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: black 1pt solid;
                padding-left: 5.4pt; background: #92d050; padding-bottom: 0in; border-left: black 1pt solid;
                width: 463.05pt; padding-top: 0in; border-bottom: black 1pt solid; height: 12.4pt"
                valign="top" width="617">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <b><span style="font-size: 14pt; font-family: 'Cambria','serif'">UAE Social Studies
                        for Arabs (Group A )
                        <o:p></o:p>
                    </span></b>
                </p>
            </td>
        </tr>
        <tr style="height: 24.1pt; mso-yfti-irow: 1">
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: black 1pt solid; width: 83.2pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 24.1pt; background-color: transparent"
                valign="top" width="111">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-size: 14pt; font-family: 'Cambria','serif'">Grade<o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 208pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 24.1pt; background-color: transparent"
                valign="top" width="277">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-size: 14pt; font-family: 'Cambria','serif'">Description<o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 171.85pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 24.1pt; background-color: transparent"
                valign="top" width="229">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-size: 14pt; font-family: 'Cambria','serif'">Percentage Mark<o:p></o:p></span></p>
            </td>
        </tr>
        <tr style="height: 12.4pt; mso-yfti-irow: 2">
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: black 1pt solid; width: 83.2pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 12.4pt; background-color: transparent"
                valign="top" width="111">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-size: 14pt; font-family: 'Cambria','serif'">A*<o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 208pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 12.4pt; background-color: transparent"
                valign="top" width="277">
                <p class="MsoNormal" style="margin: 0in 0in 0pt">
                    <span style="font-size: 14pt; font-family: 'Cambria','serif'">Outstanding<o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 171.85pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 12.4pt; background-color: transparent"
                valign="top" width="229">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-size: 14pt; font-family: 'Cambria','serif'">90% - 100%<o:p></o:p></span></p>
            </td>
        </tr>
        <tr style="height: 11.7pt; mso-yfti-irow: 3">
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: black 1pt solid; width: 83.2pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 11.7pt; background-color: transparent"
                valign="top" width="111">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-size: 14pt; font-family: 'Cambria','serif'">A<o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 208pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 11.7pt; background-color: transparent"
                valign="top" width="277">
                <p class="MsoNormal" style="margin: 0in 0in 0pt">
                    <span style="font-size: 14pt; font-family: 'Cambria','serif'">Excellent<o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 171.85pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 11.7pt; background-color: transparent"
                valign="top" width="229">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-size: 14pt; font-family: 'Cambria','serif'">75% - 89%<o:p></o:p></span></p>
            </td>
        </tr>
        <tr style="height: 12.4pt; mso-yfti-irow: 4">
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: black 1pt solid; width: 83.2pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 12.4pt; background-color: transparent"
                valign="top" width="111">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-size: 14pt; font-family: 'Cambria','serif'">B<o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 208pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 12.4pt; background-color: transparent"
                valign="top" width="277">
                <p class="MsoNormal" style="margin: 0in 0in 0pt">
                    <span style="font-size: 14pt; font-family: 'Cambria','serif'">Good<o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 171.85pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 12.4pt; background-color: transparent"
                valign="top" width="229">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-size: 14pt; font-family: 'Cambria','serif'">56% - 74%<o:p></o:p></span></p>
            </td>
        </tr>
        <tr style="height: 12.4pt; mso-yfti-irow: 5">
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: black 1pt solid; width: 83.2pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 12.4pt; background-color: transparent"
                valign="top" width="111">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-size: 14pt; font-family: 'Cambria','serif'">C<o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 208pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 12.4pt; background-color: transparent"
                valign="top" width="277">
                <p class="MsoNormal" style="margin: 0in 0in 0pt">
                    <span style="font-size: 14pt; font-family: 'Cambria','serif'">Satisfactory<o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 171.85pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 12.4pt; background-color: transparent"
                valign="top" width="229">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-size: 14pt; font-family: 'Cambria','serif'">40 % - 55%<o:p></o:p></span></p>
            </td>
        </tr>
        <tr style="height: 11.7pt; mso-yfti-irow: 6; mso-yfti-lastrow: yes">
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: black 1pt solid; width: 83.2pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 11.7pt; background-color: transparent"
                valign="top" width="111">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-size: 14pt; font-family: 'Cambria','serif'">D<o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 208pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 11.7pt; background-color: transparent"
                valign="top" width="277">
                <p class="MsoNormal" style="margin: 0in 0in 0pt">
                    <span style="font-size: 14pt; font-family: 'Cambria','serif'">Scope for improvement<span
                        style="color: #c00000">
                        <o:p></o:p>
                    </span></span>
                </p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 171.85pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 11.7pt; background-color: transparent"
                valign="top" width="229">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-size: 14pt; font-family: 'Cambria','serif'">Below 40%<o:p></o:p></span></p>
            </td>
        </tr>
    </table>
    <p class="MsoNormal" style="margin: 0in 0in 0pt">
        <b><span style="font-size: 14pt; font-family: 'Cambria','serif'">
            <o:p>&nbsp;</o:p>
        </span></b>
    </p>
    <table border="0" cellpadding="0" cellspacing="0" class="MsoNormalTable" style="width: 468.9pt;
        border-collapse: collapse; mso-yfti-tbllook: 1184; mso-padding-alt: 0in 0in 0in 0in"
        width="625">
        <tr style="height: 24.1pt; mso-yfti-irow: 0; mso-yfti-firstrow: yes">
            <td colspan="4" style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: black 1pt solid;
                padding-left: 5.4pt; background: #92d050; padding-bottom: 0in; border-left: black 1pt solid;
                width: 468.9pt; padding-top: 0in; border-bottom: black 1pt solid; height: 24.1pt"
                valign="top" width="625">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <b><span style="font-size: 14pt; font-family: 'Cambria','serif'">Group B</span></b><span
                        style="font-size: 14pt; font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
        </tr>
        <tr style="height: 24.1pt; mso-yfti-irow: 1">
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: black 1pt solid; width: 55.05pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 24.1pt; background-color: transparent"
                valign="top" width="73">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-size: 14pt; font-family: 'Cambria','serif'">Grade<o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 163.65pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 24.1pt; background-color: transparent"
                valign="top" width="218">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-size: 14pt; font-family: 'Cambria','serif'">Description<o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 119.85pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 24.1pt; background-color: transparent"
                valign="top" width="160">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-size: 14pt; font-family: 'Cambria','serif'">Percentage Mark<o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 130.35pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 24.1pt; background-color: transparent"
                valign="top" width="174">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-size: 14pt; font-family: 'Cambria','serif'">Subjects<o:p></o:p></span></p>
            </td>
        </tr>
        <tr style="height: 12.4pt; mso-yfti-irow: 2">
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: black 1pt solid; width: 55.05pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 12.4pt; background-color: transparent"
                valign="top" width="73">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-size: 14pt; font-family: 'Cambria','serif'">A*<o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 163.65pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 12.4pt; background-color: transparent"
                valign="top" width="218">
                <p class="MsoNormal" style="margin: 0in 0in 0pt">
                    <span style="font-size: 14pt; font-family: 'Cambria','serif'">Outstanding<o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 119.85pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 12.4pt; background-color: transparent"
                valign="top" width="160">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-size: 14pt; font-family: 'Cambria','serif'">90% - 100%<o:p></o:p></span></p>
            </td>
            <td rowspan="5" style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 130.35pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 12.4pt; background-color: transparent"
                width="174">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-size: 14pt; font-family: 'Cambria','serif'">English, II Language,
                        Mathematics, Science,<o:p></o:p></span></p>
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-size: 14pt; font-family: 'Cambria','serif'">Environment Studies,
                        <o:p></o:p>
                    </span>
                </p>
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-size: 14pt; font-family: 'Cambria','serif'">Moral Instruction &nbsp;<o:p></o:p></span></p>
            </td>
        </tr>
        <tr style="height: 11.7pt; mso-yfti-irow: 3">
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: black 1pt solid; width: 55.05pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 11.7pt; background-color: transparent"
                valign="top" width="73">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-size: 14pt; font-family: 'Cambria','serif'">A<o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 163.65pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 11.7pt; background-color: transparent"
                valign="top" width="218">
                <p class="MsoNormal" style="margin: 0in 0in 0pt">
                    <span style="font-size: 14pt; font-family: 'Cambria','serif'">Excellent<o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 119.85pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 11.7pt; background-color: transparent"
                valign="top" width="160">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-size: 14pt; font-family: 'Cambria','serif'">75% - 89%<o:p></o:p></span></p>
            </td>
        </tr>
        <tr style="height: 12.4pt; mso-yfti-irow: 4">
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: black 1pt solid; width: 55.05pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 12.4pt; background-color: transparent"
                valign="top" width="73">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-size: 14pt; font-family: 'Cambria','serif'">B<o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 163.65pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 12.4pt; background-color: transparent"
                valign="top" width="218">
                <p class="MsoNormal" style="margin: 0in 0in 0pt">
                    <span style="font-size: 14pt; font-family: 'Cambria','serif'">Good<o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 119.85pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 12.4pt; background-color: transparent"
                valign="top" width="160">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-size: 14pt; font-family: 'Cambria','serif'">56% - 74%<o:p></o:p></span></p>
            </td>
        </tr>
        <tr style="height: 11.7pt; mso-yfti-irow: 5">
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: black 1pt solid; width: 55.05pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 11.7pt; background-color: transparent"
                valign="top" width="73">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-size: 14pt; font-family: 'Cambria','serif'">C<o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 163.65pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 11.7pt; background-color: transparent"
                valign="top" width="218">
                <p class="MsoNormal" style="margin: 0in 0in 0pt">
                    <span style="font-size: 14pt; font-family: 'Cambria','serif'">Satisfactory<o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 119.85pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 11.7pt; background-color: transparent"
                valign="top" width="160">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-size: 14pt; font-family: 'Cambria','serif'">35% - 55%<o:p></o:p></span></p>
            </td>
        </tr>
        <tr style="height: 24.8pt; mso-yfti-irow: 6; mso-yfti-lastrow: yes">
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: black 1pt solid; width: 55.05pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 24.8pt; background-color: transparent"
                valign="top" width="73">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-size: 14pt; font-family: 'Cambria','serif'">D<o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 163.65pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 24.8pt; background-color: transparent"
                valign="top" width="218">
                <p class="MsoNormal" style="margin: 0in 0in 0pt">
                    <span style="font-size: 14pt; font-family: 'Cambria','serif'">Scope of improvement<o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 119.85pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 24.8pt; background-color: transparent"
                valign="top" width="160">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-size: 14pt; font-family: 'Cambria','serif'">Below 35%<o:p></o:p></span></p>
            </td>
        </tr>
    </table>
    <p class="MsoNormal" style="margin: 0in 0in 0pt">
        <o:p><SPAN style="FONT-SIZE: 11pt; FONT-FAMILY: Arial">&nbsp;</SPAN></o:p>
    </p>
    <p class="MsoNormal" style="margin: 0in 0in 0pt">
        <b><u><span style="font-size: 14pt"><span style="font-family: Arial">5-10<o:p></o:p></span></span></u></b></p>
    <p class="MsoNormal" style="margin: 0in 0in 0pt">
        <o:p><SPAN style="FONT-SIZE: 11pt; FONT-FAMILY: Arial">&nbsp;</SPAN></o:p>
    </p>
    <p class="MsoNormal" style="margin: 0in 0in 0pt">
    </p>
    <p class="MsoNormal" style="margin: 0in 0in 0pt; text-align: justify">
        <b><u><span style="font-family: 'Bookman Old Style','serif'"><span style="font-size: 11pt">
            GRADING SYSTEM
  FOR 
 <![if !supportMisalignedColumns]>
 <![endif]>
GRADES V TO X (Islamic
            Studies, Arabic, UAE Social Studies – Subjects of GROUP A)<o:p></o:p></span></span></u></b></p>
    <table border="0" cellpadding="0" cellspacing="0" class="MsoNormalTable" style="font-size: 11pt;
        width: 501.55pt; border-collapse: collapse; mso-yfti-tbllook: 1184; mso-padding-alt: 0in 0in 0in 0in"
        width="669">
        <tr style="height: 15.55pt; mso-yfti-irow: 0; mso-yfti-firstrow: yes">
            <td colspan="5" style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: black 1pt solid;
                padding-left: 5.4pt; background: yellow; padding-bottom: 0in; border-left: black 1pt solid;
                width: 501.55pt; padding-top: 0in; border-bottom: black 1pt solid; height: 15.55pt"
                valign="top" width="669">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <b><span style="color: red; font-family: 'Cambria','serif'">GROUP A</span></b><b><span
                        style="color: red; font-family: 'Cambria','serif'"><o:p></o:p></span></b></p>
            </td>
        </tr>
        <tr style="height: 15.55pt; mso-yfti-irow: 1">
            <td colspan="5" style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; background: #92d050; padding-bottom: 0in; border-left: black 1pt solid;
                width: 501.55pt; padding-top: 0in; border-bottom: black 1pt solid; height: 15.55pt"
                valign="top" width="669">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <b><span style="color: red; font-family: 'Cambria','serif'">GRADING SYSTEM&nbsp; (5
                        – 10)&nbsp; : (6 POINT SCALE)</span></b><b><span style="color: red; font-family: 'Cambria','serif'"><o:p></o:p></span></b></p>
            </td>
        </tr>
        <tr style="height: 15.55pt; mso-yfti-irow: 2">
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: black 1pt solid; width: 88.5pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 15.55pt; background-color: transparent"
                valign="top" width="118">
                <p class="MsoNormal" style="margin: 0in 0in 0pt; text-align: justify">
                    <span style="font-family: 'Cambria','serif'">Marks</span><span style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 121.1pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 15.55pt; background-color: transparent"
                valign="top" width="161">
                <p class="MsoNormal" style="margin: 0in 0in 0pt; text-align: justify">
                    <span style="font-family: 'Cambria','serif'">Grade (Name)</span><span style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 90.85pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 15.55pt; background-color: transparent"
                valign="top" width="121">
                <p class="MsoNormal" style="margin: 0in 0in 0pt; text-align: justify">
                    <span style="font-family: 'Cambria','serif'">Min Mark</span><span style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 66.45pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 15.55pt; background-color: transparent"
                valign="top" width="89">
                <p class="MsoNormal" style="margin: 0in 0in 0pt; text-align: justify">
                    <span style="font-family: 'Cambria','serif'">Max Mark</span><span style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 134.65pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 15.55pt; background-color: transparent"
                width="180">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-family: 'Cambria','serif'">Subjects</span><span style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
        </tr>
        <tr style="height: 16.35pt; mso-yfti-irow: 3">
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: black 1pt solid; width: 88.5pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 16.35pt; background-color: transparent"
                valign="top" width="118">
                <p class="MsoNormal" style="margin: 0in 0in 0pt; text-align: justify">
                    <span style="font-family: 'Cambria','serif'">90 – 100</span><span style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 121.1pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 16.35pt; background-color: transparent"
                valign="top" width="161">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-family: 'Cambria','serif'">A +</span><span style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 90.85pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 16.35pt; background-color: transparent"
                valign="top" width="121">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-family: 'Cambria','serif'">9.0</span><span style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 66.45pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 16.35pt; background-color: transparent"
                valign="top" width="89">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-family: 'Cambria','serif'">10</span><span style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
            <td rowspan="6" style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 134.65pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 16.35pt; background-color: transparent"
                width="180">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <b><span style="color: red; font-family: 'Cambria','serif'">ISLAMIC STUDIES, ARABIC</span></b><b><span
                        style="color: red; font-family: 'Cambria','serif'"><o:p></o:p></span></b></p>
            </td>
        </tr>
        <tr style="height: 16.35pt; mso-yfti-irow: 4">
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: black 1pt solid; width: 88.5pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 16.35pt; background-color: transparent"
                valign="top" width="118">
                <p class="MsoNormal" style="margin: 0in 0in 0pt; text-align: justify">
                    <span style="font-family: 'Cambria','serif'">80 – 89</span><span style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 121.1pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 16.35pt; background-color: transparent"
                valign="top" width="161">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-family: 'Cambria','serif'">A</span><span style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 90.85pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 16.35pt; background-color: transparent"
                valign="top" width="121">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-family: 'Cambria','serif'">8.0</span><span style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 66.45pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 16.35pt; background-color: transparent"
                valign="top" width="89">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-family: 'Cambria','serif'">8.9</span><span style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
        </tr>
        <tr style="height: 15.55pt; mso-yfti-irow: 5">
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: black 1pt solid; width: 88.5pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 15.55pt; background-color: transparent"
                valign="top" width="118">
                <p class="MsoNormal" style="margin: 0in 0in 0pt; text-align: justify">
                    <span style="font-family: 'Cambria','serif'">70 – 79</span><span style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 121.1pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 15.55pt; background-color: transparent"
                valign="top" width="161">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-family: 'Cambria','serif'">B</span><span style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 90.85pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 15.55pt; background-color: transparent"
                valign="top" width="121">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-family: 'Cambria','serif'">7.0</span><span style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 66.45pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 15.55pt; background-color: transparent"
                valign="top" width="89">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-family: 'Cambria','serif'">7.9</span><span style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
        </tr>
        <tr style="height: 15.55pt; mso-yfti-irow: 6">
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: black 1pt solid; width: 88.5pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 15.55pt; background-color: transparent"
                valign="top" width="118">
                <p class="MsoNormal" style="margin: 0in 0in 0pt; text-align: justify">
                    <span style="font-family: 'Cambria','serif'">60 – 69</span><span style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 121.1pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 15.55pt; background-color: transparent"
                valign="top" width="161">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-family: 'Cambria','serif'">C</span><span style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 90.85pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 15.55pt; background-color: transparent"
                valign="top" width="121">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-family: 'Cambria','serif'">6.0</span><span style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 66.45pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 15.55pt; background-color: transparent"
                valign="top" width="89">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-family: 'Cambria','serif'">6.9</span><span style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
        </tr>
        <tr style="height: 16.35pt; mso-yfti-irow: 7">
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: black 1pt solid; width: 88.5pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 16.35pt; background-color: transparent"
                valign="top" width="118">
                <p class="MsoNormal" style="margin: 0in 0in 0pt; text-align: justify">
                    <span style="font-family: 'Cambria','serif'">50 – 59</span><span style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 121.1pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 16.35pt; background-color: transparent"
                valign="top" width="161">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-family: 'Cambria','serif'">D</span><span style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 90.85pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 16.35pt; background-color: transparent"
                valign="top" width="121">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-family: 'Cambria','serif'">5.0</span><span style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 66.45pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 16.35pt; background-color: transparent"
                valign="top" width="89">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-family: 'Cambria','serif'">5.9</span><span style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
        </tr>
        <tr style="height: 16.35pt; mso-yfti-irow: 8; mso-yfti-lastrow: yes">
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: black 1pt solid; width: 88.5pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 16.35pt; background-color: transparent"
                valign="top" width="118">
                <p class="MsoNormal" style="margin: 0in 0in 0pt; text-align: justify">
                    <span style="font-family: 'Cambria','serif'">00 – 49</span><span style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 121.1pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 16.35pt; background-color: transparent"
                valign="top" width="161">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-family: 'Cambria','serif'">E</span><span style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 90.85pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 16.35pt; background-color: transparent"
                valign="top" width="121">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-family: 'Cambria','serif'">0</span><span style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 66.45pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 16.35pt; background-color: transparent"
                valign="top" width="89">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-family: 'Cambria','serif'">4.9</span><span style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
        </tr>
    </table>
    <p class="MsoNormal" style="margin: 0in 0in 0pt; text-align: justify">
        <span style="font-size: 14pt; color: #c00000; font-family: 'Cambria','serif'">
            <o:p>&nbsp;</o:p>
        </span>
        <span style="font-size: 14pt; color: #c00000; font-family: 'Cambria','serif'">
            <o:p>&nbsp;</o:p>
        </span>
    </p>
    <table border="0" cellpadding="0" cellspacing="0" class="MsoNormalTable" style="width: 6.95in;
        border-collapse: collapse; mso-yfti-tbllook: 1184; mso-padding-alt: 0in 0in 0in 0in"
        width="667">
        <tr style="height: 15.55pt; mso-yfti-irow: 0; mso-yfti-firstrow: yes">
            <td colspan="5" style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: black 1pt solid;
                padding-left: 5.4pt; background: yellow; padding-bottom: 0in; border-left: black 1pt solid;
                width: 6.95in; padding-top: 0in; border-bottom: black 1pt solid; height: 15.55pt"
                valign="top" width="667">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <b><span style="font-size: 11pt; color: red; font-family: 'Cambria','serif'">UAE SOCIAL
                        STUDIES</span></b><b><span style="color: red; font-family: 'Cambria','serif'"><o:p></o:p></span></b></p>
            </td>
        </tr>
        <tr style="font-size: 11pt; height: 15.55pt; mso-yfti-irow: 1">
            <td colspan="5" style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; background: #92d050; padding-bottom: 0in; border-left: black 1pt solid;
                width: 6.95in; padding-top: 0in; border-bottom: black 1pt solid; height: 15.55pt"
                valign="top" width="667">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <b><span style="color: red; font-family: 'Cambria','serif'">GRADING SYSTEM&nbsp; (5
                        – 9)&nbsp; : (6 POINT SCALE)</span></b><b><span style="color: red; font-family: 'Cambria','serif'"><o:p></o:p></span></b></p>
            </td>
        </tr>
        <tr style="font-size: 11pt; height: 15.55pt; mso-yfti-irow: 2">
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: black 1pt solid; width: 81.25pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 15.55pt; background-color: transparent"
                valign="top" width="108">
                <p class="MsoNormal" style="margin: 0in 0in 0pt; text-align: justify">
                    <span style="font-family: 'Cambria','serif'">Marks</span><span style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 111.2pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 15.55pt; background-color: transparent"
                valign="top" width="148">
                <p class="MsoNormal" style="margin: 0in 0in 0pt; text-align: justify">
                    <span style="font-family: 'Cambria','serif'">Grade (Name)</span><span style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 92.7pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 15.55pt; background-color: transparent"
                valign="top" width="124">
                <p class="MsoNormal" style="margin: 0in 0in 0pt; text-align: justify">
                    <span style="font-family: 'Cambria','serif'">Min Mark</span><span style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 101.95pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 15.55pt; background-color: transparent"
                valign="top" width="136">
                <p class="MsoNormal" style="margin: 0in 0in 0pt; text-align: justify">
                    <span style="font-family: 'Cambria','serif'">Max Mark</span><span style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 113.3pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 15.55pt; background-color: transparent"
                width="151">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-family: 'Cambria','serif'">Subject</span><span style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
        </tr>
        <tr style="font-size: 11pt; height: 16.45pt; mso-yfti-irow: 3">
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: black 1pt solid; width: 81.25pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 16.45pt; background-color: transparent"
                valign="top" width="108">
                <p class="MsoNormal" style="margin: 0in 0in 0pt; text-align: justify">
                    <span style="font-family: 'Cambria','serif'">90 – 100</span><span style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 111.2pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 16.45pt; background-color: transparent"
                valign="top" width="148">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-family: 'Cambria','serif'">A +</span><span style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 92.7pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 16.45pt; background-color: transparent"
                valign="top" width="124">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-family: 'Cambria','serif'">9.0</span><span style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 101.95pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 16.45pt; background-color: transparent"
                valign="top" width="136">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-family: 'Cambria','serif'">10</span><span style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
            <td rowspan="7" style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 113.3pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 16.45pt; background-color: transparent"
                width="151">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <b><span style="color: red; font-family: 'Cambria','serif'">UAE SOCIAL STUDIES</span></b><span
                        style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
        </tr>
        <tr style="font-size: 11pt; height: 15.55pt; mso-yfti-irow: 4">
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: black 1pt solid; width: 81.25pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 15.55pt; background-color: transparent"
                valign="top" width="108">
                <p class="MsoNormal" style="margin: 0in 0in 0pt; text-align: justify">
                    <span style="font-family: 'Cambria','serif'">80 – 89</span><span style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 111.2pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 15.55pt; background-color: transparent"
                valign="top" width="148">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-family: 'Cambria','serif'">A</span><span style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 92.7pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 15.55pt; background-color: transparent"
                valign="top" width="124">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-family: 'Cambria','serif'">8.0</span><span style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 101.95pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 15.55pt; background-color: transparent"
                valign="top" width="136">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-family: 'Cambria','serif'">8.9</span><span style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
        </tr>
        <tr style="font-size: 11pt; height: 15.55pt; mso-yfti-irow: 5">
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: black 1pt solid; width: 81.25pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 15.55pt; background-color: transparent"
                valign="top" width="108">
                <p class="MsoNormal" style="margin: 0in 0in 0pt; text-align: justify">
                    <span style="font-family: 'Cambria','serif'">70 – 79</span><span style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 111.2pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 15.55pt; background-color: transparent"
                valign="top" width="148">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-family: 'Cambria','serif'">B</span><span style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 92.7pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 15.55pt; background-color: transparent"
                valign="top" width="124">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-family: 'Cambria','serif'">7.0</span><span style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 101.95pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 15.55pt; background-color: transparent"
                valign="top" width="136">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-family: 'Cambria','serif'">7.9</span><span style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
        </tr>
        <tr style="font-size: 11pt; height: 15.55pt; mso-yfti-irow: 6">
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: black 1pt solid; width: 81.25pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 15.55pt; background-color: transparent"
                valign="top" width="108">
                <p class="MsoNormal" style="margin: 0in 0in 0pt; text-align: justify">
                    <span style="font-family: 'Cambria','serif'">60 – 69</span><span style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 111.2pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 15.55pt; background-color: transparent"
                valign="top" width="148">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-family: 'Cambria','serif'">C</span><span style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 92.7pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 15.55pt; background-color: transparent"
                valign="top" width="124">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-family: 'Cambria','serif'">6.0</span><span style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 101.95pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 15.55pt; background-color: transparent"
                valign="top" width="136">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-family: 'Cambria','serif'">6.9</span><span style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
        </tr>
        <tr style="font-size: 11pt; height: 16.45pt; mso-yfti-irow: 7">
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: black 1pt solid; width: 81.25pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 16.45pt; background-color: transparent"
                valign="top" width="108">
                <p class="MsoNormal" style="margin: 0in 0in 0pt; text-align: justify">
                    <span style="font-family: 'Cambria','serif'">50 – 59</span><span style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 111.2pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 16.45pt; background-color: transparent"
                valign="top" width="148">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-family: 'Cambria','serif'">D</span><span style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 92.7pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 16.45pt; background-color: transparent"
                valign="top" width="124">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-family: 'Cambria','serif'">5.0</span><span style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 101.95pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 16.45pt; background-color: transparent"
                valign="top" width="136">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-family: 'Cambria','serif'">5.9</span><span style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
        </tr>
        <tr style="font-size: 11pt; height: 16.45pt; mso-yfti-irow: 8">
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: black 1pt solid; width: 81.25pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 16.45pt; background-color: transparent"
                valign="top" width="108">
                <p class="MsoNormal" style="margin: 0in 0in 0pt; text-align: justify">
                    <span style="font-family: 'Cambria','serif'">40 -49</span><span style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 111.2pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 16.45pt; background-color: transparent"
                valign="top" width="148">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-family: 'Cambria','serif'">E1</span><span style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 92.7pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 16.45pt; background-color: transparent"
                valign="top" width="124">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-family: 'Cambria','serif'">4.0</span><span style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 101.95pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 16.45pt; background-color: transparent"
                valign="top" width="136">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-family: 'Cambria','serif'">4.9</span><span style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
        </tr>
        <tr style="font-size: 11pt; height: 16.45pt; mso-yfti-irow: 9; mso-yfti-lastrow: yes">
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: black 1pt solid; width: 81.25pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 16.45pt; background-color: transparent"
                valign="top" width="108">
                <p class="MsoNormal" style="margin: 0in 0in 0pt; text-align: justify">
                    <span style="font-family: 'Cambria','serif'">Less than 40</span><span style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 111.2pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 16.45pt; background-color: transparent"
                valign="top" width="148">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-family: 'Cambria','serif'">E2</span><span style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 92.7pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 16.45pt; background-color: transparent"
                valign="top" width="124">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-family: 'Cambria','serif'">0</span><span style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 101.95pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 16.45pt; background-color: transparent"
                valign="top" width="136">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-size: 12pt; font-family: 'Cambria','serif'">
                        <o:p>&nbsp;</o:p>
                    </span>
                </p>
            </td>
        </tr>
    </table>
    <p class="MsoNormal" style="margin: 0in 0in 0pt; text-align: justify">
        <span style="font-size: 14pt; color: #c00000; font-family: 'Cambria','serif'">
            <o:p>&nbsp;</o:p>
        </span>
    </p>
    <table border="0" cellpadding="0" cellspacing="0" class="MsoNormalTable" style="width: 494.45pt;
        border-collapse: collapse; mso-yfti-tbllook: 1184; mso-padding-alt: 0in 0in 0in 0in"
        width="659">
        <tr style="height: 14pt; mso-yfti-irow: 0; mso-yfti-firstrow: yes">
            <td colspan="5" style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: black 1pt solid;
                padding-left: 5.4pt; background: yellow; padding-bottom: 0in; border-left: black 1pt solid;
                width: 494.45pt; padding-top: 0in; border-bottom: black 1pt solid; height: 14pt"
                valign="top" width="659">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <b><span style="font-size: 11pt; color: red; font-family: 'Cambria','serif'">GROUP B</span></b><b><span
                        style="color: red; font-family: 'Cambria','serif'"><o:p></o:p></span></b></p>
            </td>
        </tr>
        <tr style="font-size: 11pt; height: 14pt; mso-yfti-irow: 1">
            <td colspan="5" style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; background: #92d050; padding-bottom: 0in; border-left: black 1pt solid;
                width: 494.45pt; padding-top: 0in; border-bottom: black 1pt solid; height: 14pt"
                valign="top" width="659">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <b><span style="color: red; font-family: 'Cambria','serif'">GRADING SYSTEM&nbsp; (5
                        – 10)&nbsp; : (9 POINT SCALE)</span></b><b><span style="color: red; font-family: 'Cambria','serif'"><o:p></o:p></span></b></p>
            </td>
        </tr>
        <tr style="font-size: 11pt; height: 14pt; mso-yfti-irow: 2">
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: black 1pt solid; width: 80pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 14pt; background-color: transparent"
                valign="top" width="107">
                <p class="MsoNormal" style="margin: 0in 0in 0pt; text-align: justify">
                    <span style="font-family: 'Cambria','serif'">Marks</span><span style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 98.25pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 14pt; background-color: transparent"
                valign="top" width="131">
                <p class="MsoNormal" style="margin: 0in 0in 0pt; text-align: justify">
                    <span style="font-family: 'Cambria','serif'">Grade (Name)</span><span style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 1.25in;
                padding-top: 0in; border-bottom: black 1pt solid; height: 14pt; background-color: transparent"
                valign="top" width="120">
                <p class="MsoNormal" style="margin: 0in 0in 0pt; text-align: justify">
                    <span style="font-family: 'Cambria','serif'">Min Mark</span><span style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 63.2pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 14pt; background-color: transparent"
                valign="top" width="84">
                <p class="MsoNormal" style="margin: 0in 0in 0pt; text-align: justify">
                    <span style="font-family: 'Cambria','serif'">Max Mark</span><span style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 163pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 14pt; background-color: transparent"
                width="217">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-family: 'Cambria','serif'">Subjects</span><span style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
        </tr>
        <tr style="font-size: 11pt; height: 13.35pt; mso-yfti-irow: 3">
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: black 1pt solid; width: 80pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 13.35pt; background-color: transparent"
                valign="top" width="107">
                <p class="MsoNormal" style="margin: 0in 0in 0pt; text-align: justify">
                    <span style="font-family: 'Cambria','serif'">91 – 100</span><span style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 98.25pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 13.35pt; background-color: transparent"
                valign="top" width="131">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-family: 'Cambria','serif'">A1</span><span style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 1.25in;
                padding-top: 0in; border-bottom: black 1pt solid; height: 13.35pt; background-color: transparent"
                valign="top" width="120">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-family: 'Cambria','serif'">9.1</span><span style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 63.2pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 13.35pt; background-color: transparent"
                valign="top" width="84">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-family: 'Cambria','serif'">10</span><span style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
            <td rowspan="9" style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 163pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 13.35pt; background-color: transparent"
                width="217">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <b><span style="color: red; font-family: 'Cambria','serif'">ENGLISH, II LANGUAGE, MATHEMATICS,
                    </span></b><b><span style="color: red; font-family: 'Cambria','serif'">
                        <o:p></o:p>
                    </span></b>
                </p>
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <b><span style="color: red; font-family: 'Cambria','serif'">GENERAL SCIENCE,<o:p></o:p></span></b></p>
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <b><span style="color: red; font-family: 'Cambria','serif'">SOCIAL STUDIES ,<o:p></o:p></span></b></p>
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <b><span style="color: red; font-family: 'Cambria','serif'">MORAL INSTRUCTION &nbsp;</span></b><span
                        style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
        </tr>
        <tr style="font-size: 11pt; height: 13.35pt; mso-yfti-irow: 4">
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: black 1pt solid; width: 80pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 13.35pt; background-color: transparent"
                valign="top" width="107">
                <p class="MsoNormal" style="margin: 0in 0in 0pt; text-align: justify">
                    <span style="font-family: 'Cambria','serif'">81 – 90</span><span style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 98.25pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 13.35pt; background-color: transparent"
                valign="top" width="131">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-family: 'Cambria','serif'">A2</span><span style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 1.25in;
                padding-top: 0in; border-bottom: black 1pt solid; height: 13.35pt; background-color: transparent"
                valign="top" width="120">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-family: 'Cambria','serif'">8.1</span><span style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 63.2pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 13.35pt; background-color: transparent"
                valign="top" width="84">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-family: 'Cambria','serif'">9</span><span style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
        </tr>
        <tr style="font-size: 11pt; height: 13.35pt; mso-yfti-irow: 5">
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: black 1pt solid; width: 80pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 13.35pt; background-color: transparent"
                valign="top" width="107">
                <p class="MsoNormal" style="margin: 0in 0in 0pt; text-align: justify">
                    <span style="font-family: 'Cambria','serif'">71 – 80</span><span style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 98.25pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 13.35pt; background-color: transparent"
                valign="top" width="131">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-family: 'Cambria','serif'">B1</span><span style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 1.25in;
                padding-top: 0in; border-bottom: black 1pt solid; height: 13.35pt; background-color: transparent"
                valign="top" width="120">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-family: 'Cambria','serif'">7.1</span><span style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 63.2pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 13.35pt; background-color: transparent"
                valign="top" width="84">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-family: 'Cambria','serif'">8</span><span style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
        </tr>
        <tr style="font-size: 11pt; height: 14pt; mso-yfti-irow: 6">
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: black 1pt solid; width: 80pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 14pt; background-color: transparent"
                valign="top" width="107">
                <p class="MsoNormal" style="margin: 0in 0in 0pt; text-align: justify">
                    <span style="font-family: 'Cambria','serif'">61 – 70</span><span style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 98.25pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 14pt; background-color: transparent"
                valign="top" width="131">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-family: 'Cambria','serif'">B2</span><span style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 1.25in;
                padding-top: 0in; border-bottom: black 1pt solid; height: 14pt; background-color: transparent"
                valign="top" width="120">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-family: 'Cambria','serif'">6.1</span><span style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 63.2pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 14pt; background-color: transparent"
                valign="top" width="84">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-family: 'Cambria','serif'">7</span><span style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
        </tr>
        <tr style="font-size: 11pt; height: 14pt; mso-yfti-irow: 7">
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: black 1pt solid; width: 80pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 14pt; background-color: transparent"
                valign="top" width="107">
                <p class="MsoNormal" style="margin: 0in 0in 0pt; text-align: justify">
                    <span style="font-family: 'Cambria','serif'">51 – 60</span><span style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 98.25pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 14pt; background-color: transparent"
                valign="top" width="131">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-family: 'Cambria','serif'">C1</span><span style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 1.25in;
                padding-top: 0in; border-bottom: black 1pt solid; height: 14pt; background-color: transparent"
                valign="top" width="120">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-family: 'Cambria','serif'">5.1</span><span style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 63.2pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 14pt; background-color: transparent"
                valign="top" width="84">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-family: 'Cambria','serif'">6</span><span style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
        </tr>
        <tr style="font-size: 11pt; height: 13.35pt; mso-yfti-irow: 8">
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: black 1pt solid; width: 80pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 13.35pt; background-color: transparent"
                valign="top" width="107">
                <p class="MsoNormal" style="margin: 0in 0in 0pt; text-align: justify">
                    <span style="font-family: 'Cambria','serif'">41 – 50</span><span style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 98.25pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 13.35pt; background-color: transparent"
                valign="top" width="131">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-family: 'Cambria','serif'">C2</span><span style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 1.25in;
                padding-top: 0in; border-bottom: black 1pt solid; height: 13.35pt; background-color: transparent"
                valign="top" width="120">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-family: 'Cambria','serif'">4.1</span><span style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 63.2pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 13.35pt; background-color: transparent"
                valign="top" width="84">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-family: 'Cambria','serif'">5</span><span style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
        </tr>
        <tr style="font-size: 11pt; height: 14pt; mso-yfti-irow: 9">
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: black 1pt solid; width: 80pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 14pt; background-color: transparent"
                valign="top" width="107">
                <p class="MsoNormal" style="margin: 0in 0in 0pt; text-align: justify">
                    <span style="font-family: 'Cambria','serif'">33 – 40</span><span style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 98.25pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 14pt; background-color: transparent"
                valign="top" width="131">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-family: 'Cambria','serif'">D</span><span style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 1.25in;
                padding-top: 0in; border-bottom: black 1pt solid; height: 14pt; background-color: transparent"
                valign="top" width="120">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-family: 'Cambria','serif'">3.3</span><span style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 63.2pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 14pt; background-color: transparent"
                valign="top" width="84">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-family: 'Cambria','serif'">4</span><span style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
        </tr>
        <tr style="font-size: 11pt; height: 14pt; mso-yfti-irow: 10">
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: black 1pt solid; width: 80pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 14pt; background-color: transparent"
                valign="top" width="107">
                <p class="MsoNormal" style="margin: 0in 0in 0pt; text-align: justify">
                    <span style="font-family: 'Cambria','serif'">21 – 32</span><span style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 98.25pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 14pt; background-color: transparent"
                valign="top" width="131">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-family: 'Cambria','serif'">E1</span><span style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 1.25in;
                padding-top: 0in; border-bottom: black 1pt solid; height: 14pt; background-color: transparent"
                valign="top" width="120">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-family: 'Cambria','serif'">2.1</span><span style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 63.2pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 14pt; background-color: transparent"
                valign="top" width="84">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-family: 'Cambria','serif'">3.2</span><span style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
        </tr>
        <tr style="font-size: 11pt; height: 14pt; mso-yfti-irow: 11; mso-yfti-lastrow: yes">
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: black 1pt solid; width: 80pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 14pt; background-color: transparent"
                valign="top" width="107">
                <p class="MsoNormal" style="margin: 0in 0in 0pt; text-align: justify">
                    <span style="font-family: 'Cambria','serif'">00 – 20</span><span style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 98.25pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 14pt; background-color: transparent"
                valign="top" width="131">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-family: 'Cambria','serif'">E2</span><span style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 1.25in;
                padding-top: 0in; border-bottom: black 1pt solid; height: 14pt; background-color: transparent"
                valign="top" width="120">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-family: 'Cambria','serif'">0</span><span style="font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
            <td style="border-right: black 1pt solid; padding-right: 5.4pt; border-top: #d4d0c8;
                padding-left: 5.4pt; padding-bottom: 0in; border-left: #d4d0c8; width: 63.2pt;
                padding-top: 0in; border-bottom: black 1pt solid; height: 14pt; background-color: transparent"
                valign="top" width="84">
                <p align="center" class="MsoNormal" style="margin: 0in 0in 0pt; text-align: center">
                    <span style="font-family: 'Cambria','serif'">2</span><span style="font-size: 12pt;
                        font-family: 'Cambria','serif'"><o:p></o:p></span></p>
            </td>
        </tr>
    </table>
</asp:Content>

