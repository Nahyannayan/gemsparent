Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System
Imports System.Xml
Imports System.Data.SqlTypes
Imports System.IO
Partial Class Curriculum_clmSMARTTargetStudentView
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'hfSTU_ID.Value = "20226077"
        'hfACD_ID.Value = "881"
        'hfGRD_ID.Value = "07"
        hfSTU_ID.Value = Session("STU_ID") '"20226077"
        hfACD_ID.Value = Session("STU_ACD_ID") '"881"
        hfGRD_ID.Value = Session("STU_GRD_ID") '"07"
        BindSmartTargets()
    End Sub

#Region "Private Methods"
    Sub BindSmartTargets()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT TGM_ID,CASE WHEN ISNULL(TSM_bSUBMIT,0)=1 THEN TGM_DESCR ELSE TGM_DESCR+'<font color=red> - Not Submitted</font>' END TGM_DESCR  ,CASE WHEN ISNULL(TSM_bSUBMIT,0)=1 THEN '1' ELSE '0' END AS bSUBMIT FROM SMART.TARGET_SETUP_M " _
                              & " INNER JOIN SMART.TARGET_STUDENT_M ON TGM_ID=TSM_TGM_ID" _
                              & " AND TSM_STU_ID=" + hfSTU_ID.Value _
                              & " AND TGM_ACD_ID=" + hfACD_ID.Value + " AND ISNULL(TSM_bRELEASE,0)=1 AND '" + Format("yyyy-MM-dd", Now.Date) + "'" _
                              & " BETWEEN TSM_RELEASE_SDT AND TSM_RELEASE_EDT"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        dlReports.DataSource = ds
        dlReports.DataBind()

    End Sub
#End Region

    Protected Sub lnkReport_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblTgmId As Label = TryCast(sender.findcontrol("lblTgmId"), Label)
        Dim lblSubmit As Label = TryCast(sender.findcontrol("lblSubmit"), Label)
        If lblSubmit.Text = 1 Then
            Response.Redirect("~/Curriculum/ProgressReports/Aspx/rptSmartTargetPrint.aspx?tgm_id=" + Encr_decrData.Encrypt(lblTgmId.Text) _
                      & "&grd_id=" + Encr_decrData.Encrypt(hfGRD_ID.Value))
        End If
      
    End Sub
End Class
