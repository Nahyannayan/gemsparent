Imports Microsoft.ApplicationBlocks.Data
Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Text

Partial Class ParentLogin_OutStanding
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("username") Is Nothing Then
            Session("Active_tab") = "Home"
            Session("Site_Path") = ""
            Response.Redirect("~\login.aspx")
        End If
        If Page.IsPostBack = False Then
            lbChildName.Text = Session("STU_NAME")
            BindAcademicYear()
              ShowSubjectList(False)
            BindAOLDetails()
            PopulateSubject()
        End If
    End Sub
    Sub BindAcademicYear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim str_sql As String = "SELECT ACY_DESCR,ACD_ID FROM ACADEMICYEAR_D  INNER JOIN ACADEMICYEAR_M ON ACD_ACY_ID=ACY_ID" _
                              & " WHERE ACD_BSU_ID='" + Session("sbsuid") + "' AND ACD_CLM_ID=1 AND ACD_ACY_ID>=22"
        'hfACD_ID.Value = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_sql)
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_sql)

        ddlAcademicYear.DataSource = ds
        ddlAcademicYear.DataTextField = "acy_descr"
        ddlAcademicYear.DataValueField = "acd_id"
        ddlAcademicYear.DataBind()

        str_sql = " SELECT ACY_DESCR,ACD_ID FROM ACADEMICYEAR_M AS A INNER JOIN ACADEMICYEAR_D AS B" _
                                & " ON B.ACD_ACY_ID=A.ACY_ID WHERE ACD_CURRENT=1 AND ACD_BSU_ID='" + Session("STU_BSU_ID") + "' AND ACD_CLM_ID=" + Session("ACD_CLM_ID")
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_sql)
        Dim li As New ListItem
        li.Text = ds.Tables(0).Rows(0).Item(0)
        li.Value = ds.Tables(0).Rows(0).Item(1)
        ddlAcademicYear.Items(ddlAcademicYear.Items.IndexOf(li)).Selected = True

    End Sub
    Public Sub PopulateSubject()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString

        Dim str_sql As String = "SELECT DISTINCT SBG_DESCR, SBG_ID FROM ACT.STUDENT_ACTIVITY INNER JOIN SUBJECTS_GRADE_S " & _
        " ON ACT.STUDENT_ACTIVITY.STA_SBG_ID = SUBJECTS_GRADE_S.SBG_ID " & _
        " WHERE /*ISNULL(STA_bRELEASEONLINE, 0) = 1 AND  " & _
        " ISNULL(STA_RELEASEDATE,getdate()) <=  getdate()" & _
        " AND*/ STA_STU_ID = '" & Session("STU_ID") & "' " & _
        " AND STA_ACD_ID = " & ddlAcademicYear.SelectedValue.ToString
        If radReportSubjectWise.Checked Then
            str_sql += " AND STA_CAD_ID = '" & ddlAOLReports.SelectedValue & "'"
        End If
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_sql)
        trSubjectList.DataSource = ds
        trSubjectList.DataTextField = "SBG_DESCR"
        trSubjectList.DataValueField = "SBG_ID"
        trSubjectList.DataBind()
    End Sub

 
    Public Sub BindAOLDetails()

        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString

        Dim str_sql As String = " SELECT DISTINCT CAD_ID, CAD_DESC FROM ACT.STUDENT_ACTIVITY " & _
        " INNER JOIN ACT.ACTIVITY_D ON ACT.STUDENT_ACTIVITY.STA_CAD_ID = ACT.ACTIVITY_D.CAD_ID " & _
        " WHERE ISNULL(CAD_bAOL, 0) = 1 AND STA_STU_ID = '" & Session("STU_ID") & "' AND STA_ACD_ID = " & ddlAcademicYear.SelectedValue.ToString + " AND ISNULL(STA_bRELEASEONLINE,0)=1"
        '" WHERE ISNULL(CAD_bAOL, 0) = 1 AND STA_ACD_ID = 567 AND STA_STU_ID = 31267 "
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_sql)
        ddlAOLReports.DataSource = ds
        ddlAOLReports.DataTextField = "CAD_DESC"
        ddlAOLReports.DataValueField = "CAD_ID"
        ddlAOLReports.DataBind()
    End Sub

    Public Sub ShowSubjectList(ByVal bHide As Boolean)
        trAOLReport.Visible = Not bHide
        trSubject.Visible = True
    End Sub

    Protected Sub radReportOverAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radReportOverAll.CheckedChanged
        PopulateSubject()
        ShowSubjectList(True)
    End Sub

    Protected Sub radReportSubjectWise_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radReportSubjectWise.CheckedChanged
        ShowSubjectList(False)
        BindAOLDetails()
        PopulateSubject()
    End Sub

    Protected Sub ddlAOLReports_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAOLReports.SelectedIndexChanged
        PopulateSubject()
    End Sub

    Protected Sub btnViewReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewReport.Click
        If radReportOverAll.Checked Then
            CallOverAllReport()
        ElseIf radReportSubjectWise.Checked Then
            CallAOLReport()
        End If
        Session("bAOLReport") = True
        Session("bResultAnalysis") = 1
        mpe.Show()
    End Sub

    Private Sub CallOverAllReport()
        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAcademicYear.SelectedValue.ToString)
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@RPF_ID", GetRPFIDs())
        param.Add("subject", "")
        param.Add("@SBG_ID", SelectedSubjects())
        param.Add("@STU_ID", Session("STU_ID"))
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            .reportPath = Server.MapPath("ProgressReports/Rpt/rptFormativeAssesmentConsolidated.rpt")
        End With
        Session("rptClass") = rptClass
        Session("PrintAOL") = "YES"
    End Sub

    Private Function GetRPFIDs() As String
        Dim str_sql As String = String.Empty
        str_sql = "SELECT  ISNULL(STUFF((SELECT DISTINCT CAST(ISNULL(RRM_RPF_ID,'') as varchar) + '|' FROM " & _
        " ACT.STUDENT_ACTIVITY AS A " & _
        " INNER JOIN ACT.STUDENT_ACTIVITY_AOL AS B ON A.STA_ID=B.STS_STA_ID " & _
" INNER JOIN ACT.ACTIVITY_SCHEDULE AS I ON A.STA_CAS_ID=I.CAS_ID " & _
" INNER JOIN RPT.REPORT_SCHEDULE_D AS C ON A.STA_CAD_ID=C.RRD_CAD_ID " & _
" INNER JOIN RPT.REPORT_RULE_M AS D ON C.RRD_RRM_ID=D.RRM_ID AND A.STA_SBG_ID=D.RRM_SBG_ID " & _
" WHERE STA_ACD_ID = " & ddlAcademicYear.SelectedValue.ToString & _
        " AND D.RRM_GRD_ID = '" & Session("STU_GRD_ID") & "' AND STA_STU_ID = '" & Session("STU_ID") & "' for xml path('')),1,0,''),0) "

        Return SqlHelper.ExecuteScalar(ConnectionManger.GetOASIS_CURRICULUMConnectionString, CommandType.Text, str_sql)

    End Function

    Private Function SelectedSubjects() As String
        Dim strSBG_IDs As New StringBuilder
        Dim seperator As String = "|"

        Dim i As Integer
        For i = 0 To trSubjectList.Items.Count - 1
            If trSubjectList.Items(i).Selected = True Then
                strSBG_IDs.Append(trSubjectList.Items(i).Value)
                strSBG_IDs.Append(seperator)
            End If
        Next
        If strSBG_IDs.Length > 0 Then
            strSBG_IDs.Remove(strSBG_IDs.Length - (seperator.Length), seperator.Length)
        End If
        Return strSBG_IDs.ToString()
    End Function

    Private Sub CallAOLReport()
        Dim param As New Hashtable
        param.Add("@ACD_ID", ddlAcademicYear.SelectedValue.ToString)
        param.Add("@BSU_ID", Session("sBsuid"))
        param.Add("@CAM_ID", 0)
        param.Add("@CAD_ID", ddlAOLReports.SelectedValue)
        param.Add("@STU_ID", Session("STU_ID"))
        param.Add("@SBG_ID", SelectedSubjects())
        param.Add("RPT_CAPTION", ddlAOLReports.SelectedItem.Text)
        'Session("studentphotopath") = TransferPath(h_STU_IDs.Value.Replace("___", "|"))
        param.Add("UserName", "ONLINE")

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            .reportPath = Server.MapPath("ProgressReports/Rpt/rptAOL_REPORT_CBSE_03.rpt")
        End With
        Session("rptClass") = rptClass
        Session("PrintAOL") = "YES"

        'Response.Redirect("progressReportHome.aspx")
    End Sub

    Protected Sub ddlAcademicYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcademicYear.SelectedIndexChanged
        ShowSubjectList(False)
        BindAOLDetails()
        PopulateSubject()
    End Sub
End Class
