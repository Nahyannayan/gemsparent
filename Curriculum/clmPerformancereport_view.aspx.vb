﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data
Imports System.Web.Configuration
Partial Class Curriculum_clmPerformancereport_view
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim dsCurriculum As New DataSet
        dsCurriculum = bindCurriculumChart(Session("STU_ID"), Session("HFrpf_id"))
        If Not dsCurriculum Is Nothing AndAlso Not dsCurriculum.Tables(0) Is Nothing AndAlso dsCurriculum.Tables(0).Rows.Count > 0 Then
            RadCurriculumPie.DataSource = dsCurriculum.Tables(0)
            RadCurriculumPie.DataBind()
            RadCurriculumPie.ChartTitle.Text = "Student Performance Chart For -" + dsCurriculum.Tables(0).Rows(0)("RPF_DESCR").ToString
        End If
    End Sub
    Public Function bindCurriculumChart(ByVal stuId As String, ByVal rpf As String) As DataSet
        Try
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASIS_CURRICULUMConnectionString").ConnectionString
            Dim str_query As String = ""
            Dim PARAM(15) As SqlParameter


            PARAM(0) = New SqlParameter("@STU_ID", stuId)
            PARAM(1) = New SqlParameter("@RPF_ID", rpf)

            Dim dsDetails As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[RPT].[RESULT_ANALYSIS_HLAP_GRAPH_parent_portal]", PARAM)

            Return dsDetails


        Catch ex As Exception

        End Try
    End Function
End Class
