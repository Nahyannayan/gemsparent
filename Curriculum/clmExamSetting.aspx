<%@ Page Language="VB" MasterPageFile="~/ParentMaster.master" AutoEventWireup="false" CodeFile="clmExamSetting.aspx.vb" Inherits="Curriculum_clmExamSetting" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphParent" runat="Server">


    <script src="../Scripts/jquery-1.9.1.js" type="text/javascript"></script>

    <link rel="stylesheet" type="text/css" href="../Scripts/jQuery-ui-1.10.3.css" />
    <link href="../CSS/Popup.css" rel="stylesheet" />

    <div class="content margin-top30 margin-bottom60">
        <div class="container">
            <div class="row">
                <div class="posts-block col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="my-account">

                        <div class="title-box">
                            <h3>Examination Paper Selection
                             <span class="profile-right flip">
                                 <asp:Label ID="lbChildName" runat="server"></asp:Label>
                             </span></h3>
                        </div>

                        <div class="table-responsive">
                            <table align="center" class="table table-striped table-bordered table-responsive my-orders-table" style="width: 100%;">

                                <tr>
                                    <td align="right" class="tdfields" width="20.5%">Student ID
                                    </td>
                                    <td align="left" class="tdfields" width="39.2%">
                                        <asp:Label ID="txtStdNo" runat="server"></asp:Label>
                                    </td>
                                    <td align="right" class="tdfields" width="21.6%">Grade &amp; Section
                                    </td>
                                    <td align="left" class="tdfields" width="18.7%">
                                        <asp:Label ID="stugrd" runat="server"></asp:Label>
                                        <%---<strong></strong>--%>
                                        <asp:Label ID="lblsct" runat="server"></asp:Label>
                                        &nbsp; &nbsp;
              
                                        <%--<a id="lnkShowdetails" href="#ViewDetailsPopUp" style="font-size: 11px; font-weight: bold; text-decoration: underline;">View Details</a>--%>
                                        <div style="display: none">
                                            <div id="ViewDetailsPopUp">
                                                <asp:Panel ID="pnlsetup" runat="server" CssClass="darkPanlvisible">
                                                    <div class="panelFeeSetup">
                                                        <table width="100%" cellpadding="0" cellspacing="0" class="table table-striped  table-responsive my-orders-table">
                                                            <%--<tr>
                                                                <td align="right" height="10px">
                                                                    <asp:LinkButton ID="lbPnlJobClose" CssClass="closebtnFee" runat="server" OnClick="lbPnlJobClose_Click">Close</asp:LinkButton>
                                                                </td>
                                                            </tr>--%>
                                                            <tr>
                                                                <td>
                                                                    <iframe src="PopupExam.aspx" frameborder="0" style="margin: -1px;"
                                                                        scrolling="no" width="800" height="400" id="IfRefClose"></iframe>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </asp:Panel>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <%-- <tr>
                                    <td align="left" class="tdfields" width="20%">Subject
                                    </td>
                                    <td align="left" class="tdfields" width="20%">
                                        <asp:DropDownList ID="ddlSubj" runat="server" Width="109px" AutoPostBack="True"></asp:DropDownList>
                                    </td>
                                    <td align="left" class="tdfields" width="20%">Paper
                                    </td>
                                    <td align="left" class="tdfields" valign="middle" width="20%">
                                        <asp:CheckBoxList ID="ddlPaper" runat="server" BorderStyle="Solid" BorderWidth="1px"
                                            Height="104px" RepeatLayout="Flow" Style="border-right: #1b80b6 1px solid; border-top: #1b80b6 1px solid; vertical-align: middle; overflow: auto; border-left: #1b80b6 1px solid; border-bottom: #1b80b6 1px solid; text-align: left"
                                            Width="193px" AutoPostBack="true">
                                        </asp:CheckBoxList>
                                    </td>
                                </tr>--%>
                                <tr>
                                    <td colspan="4">
                                        <asp:HiddenField ID="hidTotal" runat="server" Value="0" />
                                        <asp:UpdatePanel ID="updpanl" runat="server">
                                            <ContentTemplate>
                                                <asp:GridView ID="grdExamSetting" runat="server" OnRowDataBound="grdExamSetting_RowDataBound" AutoGenerateColumns="false" CssClass="table table-striped table-bordered table-responsive text-left my-orders-table">
                                                    <Columns>
                                                        <asp:BoundField DataField="SBG_DESCR" HeaderText="Subject Name and Syllabus Number" ItemStyle-Width="20%" HeaderStyle-HorizontalAlign="Center" HeaderStyle-VerticalAlign="Middle" />
                                                        <asp:BoundField HeaderText="Examination Level" DataField="es_eXAMINTION_lEVEL" ItemStyle-Width="20%" HeaderStyle-HorizontalAlign="Center" HeaderStyle-VerticalAlign="Middle" />
                                                        <asp:BoundField HeaderText="Examination Board" DataField="exl_title" ItemStyle-Width="20%" HeaderStyle-HorizontalAlign="Center" HeaderStyle-VerticalAlign="Middle" />
                                                        <asp:TemplateField HeaderText="Subject Details" ItemStyle-Width="40%" HeaderStyle-HorizontalAlign="Center" HeaderStyle-VerticalAlign="Middle">
                                                            <ItemTemplate>
                                                                <asp:HiddenField ID="hidSubject" runat="server" Value='<%# Bind("SBG_ID") %>' />
                                                                <asp:HiddenField ID="hidexamlevel" runat="server" Value='<%# Bind("es_eXAMINTION_lEVEL")%>' />
                                                                <asp:GridView ID="grdSubject" runat="server" AutoGenerateColumns="false" CssClass="table table-striped table-bordered table-responsive text-left my-orders-table">
                                                                    <Columns>
                                                                        <asp:TemplateField ItemStyle-Width="10%">
                                                                            <HeaderTemplate>
                                                                                #
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:HiddenField ID="hidSubID" runat="server" Value='<%# Bind("ES_ID")%>' />
                                                                                <asp:HiddenField ID="hdnIsApprovalRequired" runat="server" Value='<%# Bind("IsApprovalRequired")%>' />
                                                                                <asp:CheckBox ID="chkFee" runat="server" OnCheckedChanged="chkFee_CheckedChanged" CssClass="delete-checkbox" AutoPostBack="true" />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:BoundField DataField="ES_PAPER" HeaderText="Subject Details" ItemStyle-Width="45%" />
                                                                        <asp:BoundField DataField="ES_COST" HeaderText="Fees Details" ItemStyle-Width="45%" />
                                                                        <asp:BoundField DataField="IsApprovalRequired" HeaderText="Approval Required" Visible="false" />
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>

                                    </td>
                                </tr>
                                <tr id="rowtopay" runat="server">

                                    <td align="right" colspan="3">
                                        <asp:Label ID="lblMessage" runat="server" Text="Total Amount To Pay" Font-Bold="true"></asp:Label>
                                    </td>
                                    <td align="right">
                                        <asp:Label ID="lblTot" runat="server" Text="0" Font-Bold="true"></asp:Label>
                                    </td>
                                </tr>
                                <%--<tr>
                                    <td align="right" class="tdfields" colspan="4">
                                        <asp:Button ID="btnPayatSchool" runat="server" Text="Pay at School" CssClass="btn btn-info" />

                                        &nbsp;
                                        <asp:Button ID="btnPayNow" runat="server" Text="Pay Now" CssClass="btn btn-info" />

                                        &nbsp;
                                        <asp:Button ID="btnSubmit" runat="server" Text="Request for Approval" Visible="false" CssClass="btn btn-info" />
                                    </td>
                                </tr>--%>
                                <tr>
                                    <td colspan="4">
                                        <asp:Label ID="lblError" runat="server" EnableViewState="False"
                                            Font-Names="Verdana"></asp:Label>
                                    </td>

                                </tr>
                            </table>
                        </div>
                        <table cellpadding="3" cellspacing="0" runat="server" style="width: 100%;" class="table table-striped table-bordered table-responsive my-orders-table">
                            <tr class="tdblankAll">
                                <td class="tdblankAll" align="left" colspan="2">
                                    <asp:GridView ID="gvPaper" runat="server" Width="100%" OnRowDataBound="gvPaper_RowDataBound"
                                        EmptyDataText="No Data Found" CssClass="table table-striped table-bordered table-responsive text-left my-orders-table" AutoGenerateColumns="False">

                                        <%--  <asp:TemplateField HeaderText="Id" Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblId" runat="server" Text='<%# Bind("EPD_ID")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Subject Name and Syllubus Number">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblsub" runat="server" Text='<%# bind("sbg_descr") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Papers">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblPaper" runat="server" Text='<%# Bind("es_paper")%>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Amount">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblamt" runat="server" Text='<%# Bind("EPD_COST")%>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Right" />
                                            </asp:TemplateField>--%>
                                        <Columns>
                                            <asp:BoundField DataField="SBG_DESCR" HeaderText="Subject Name and Syllabus Number" ItemStyle-Width="20%" />
                                            <asp:BoundField HeaderText="Examination Level" DataField="es_eXAMINTION_lEVEL" ItemStyle-Width="20%" />
                                            <asp:BoundField HeaderText="Examination Board" DataField="exl_title" ItemStyle-Width="20%" />

                                            <asp:TemplateField HeaderText="Subject Details" ItemStyle-Width="40%">
                                                <ItemTemplate>
                                                    <asp:HiddenField ID="hidSubject_pay" runat="server" Value='<%# Bind("SBG_ID") %>' />
                                                    <asp:HiddenField ID="hidexamlevel_pay" runat="server" Value='<%# Bind("es_eXAMINTION_lEVEL")%>' />
                                                    <asp:GridView ID="grdSubject_pay" runat="server" OnRowDataBound="grdSubject_pay_RowDataBound" AutoGenerateColumns="false" CssClass="table table-striped table-bordered table-responsive text-left my-orders-table">
                                                        <Columns>
                                                            <asp:BoundField DataField="ES_PAPER" HeaderText="Subject Details" ItemStyle-Width="28%" HeaderStyle-HorizontalAlign="Center" HeaderStyle-VerticalAlign="Middle" />
                                                            <asp:BoundField DataField="ES_COST" HeaderText="Fees Details" ItemStyle-Width="28%" HeaderStyle-HorizontalAlign="Center" HeaderStyle-VerticalAlign="Middle" />
                                                            <asp:BoundField DataField="ESA_STATUS" HeaderText="Status" ItemStyle-Width="28%" HeaderStyle-HorizontalAlign="Center" HeaderStyle-VerticalAlign="Middle" />

                                                            <asp:TemplateField ItemStyle-Width="16%" HeaderStyle-HorizontalAlign="Center" HeaderStyle-VerticalAlign="Middle" >
                                                                <HeaderTemplate>                                                                    
                                                                    Action
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:HiddenField ID="hdn_CurrentStatus" runat="server" Value='<%# Bind("CurrentStatus")%>' />
                                                                    <asp:HiddenField ID="hidepd_id" runat="server" Value='<%# Bind("EPD_ID")%>' />
                                                                    <%-- <asp:Label ID="lblPaid" Text="Paid" Visible='<%# Bind("ISPAID")%>' runat="server"></asp:Label>--%>
                                                                    <asp:Label ID="lblPaid" Text='<%# If(Eval("ISPAID").ToString() = "False", "Payment Due", "Paid") %>' runat="server"></asp:Label>

                                                                    <asp:LinkButton ID="lnkRemove" runat="server" Text="Remove" Visible='<%# Bind("ISREMOVE")%>' OnCommand="lnkRemove_Command" CommandArgument='<%# Bind("EPD_ID")%>'></asp:LinkButton>
                                                                    <%--<asp:Label Text="Remove" ID="lnkRemoveDummy" runat="server" Visible="false"></asp:Label>--%>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>

                                    </asp:GridView>
                                </td>
                            </tr>
                            <tr id="rowtopayschool" runat="server" visible="false">
                                <td align="right">
                                    <asp:Label ID="lblMsgPay" runat="server" Text="Total amount to be paid at school for approved subject." Font-Bold="true"></asp:Label>
                                </td>
                                <td align="right">
                                    <asp:Label ID="lblTotal" runat="server" Font-Size="14" Font-Bold="true"></asp:Label>
                                </td>
                            </tr>

                        </table>

                        <table style="width: 100%;">
                            <tr>
                                <td align="right" class="tdfields">
                                    <asp:Button ID="btnPayatSchool" runat="server" Text="Submit" CssClass="btn btn-info" Visible="false" />

                                    &nbsp;
                                        <asp:Button ID="btnPayNow" runat="server" Text="Pay Now" CssClass="btn btn-info" Visible="false" />

                                    &nbsp;
                                        <asp:Button ID="btnSubmit" runat="server" Text="Submit" Visible="false" CssClass="btn btn-info" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script lang="javascript" type="text/javascript">
        $(document).ready(function () {
            $('#lnkShowdetails').fancybox({
                'hideOnContentClick': false,
                'autoScale': false,
                'autoDimensions': false,
                openEffect: 'none',
                closeEffect: 'none',
                'modal': false

            });
        });
    </script>
</asp:Content>

