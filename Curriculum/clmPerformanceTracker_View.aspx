﻿<%@ Page Language="VB" MasterPageFile="~/ParentMaster.master" AutoEventWireup="false" CodeFile="clmPerformanceTracker_View.aspx.vb" Inherits="Curriculum_clmPerformanceTracker_View" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphParent" runat="Server">
    <div class="content margin-top30 margin-bottom60">
        <div class="container">
            <div class="row">
                <div class="posts-block col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="my-account">
                        <asp:Panel ID="plPrev1" runat="server" BackColor="White" Height="100%" ScrollBars="vertical">
                            <iframe id="ifPerf" height="800px" src="clmPerformanceTracker.aspx" scrolling="yes" marginwidth="0px" frameborder="0" width="100%"></iframe>
                        </asp:Panel>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

