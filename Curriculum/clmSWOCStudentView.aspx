<%@ Page Language="VB" MasterPageFile="~/ParentMaster.master" AutoEventWireup="false" CodeFile="clmSWOCStudentView.aspx.vb" Inherits="Curriculum_clmSWOCStudentView" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphParent" Runat="Server">

    <div class="content margin-top30 margin-bottom60">
        <div class="container">
            <div class="row">
                <div class="posts-block col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="my-account">

 <asp:DataList ID="dlReports" runat="server" RepeatColumns="1" CssClass="divcurrNoborder" >
     <ItemStyle Wrap="false"   />
     <ItemTemplate>
     
       <div  >
       <table width="100%"  class="BlueTable_noborder">
       <tr><td style="vertical-align:top">
          <asp:Image ID="Image1" runat="server"  ImageUrl="~/Images/Common/PageBody/arrow.png"    />
          </td><td style="vertical-align:bottom" class="tdfields">
         <asp:LinkButton ID="lnkReport" Font-Bold="true" Font-Size="14px" runat="server" Text='<%# Bind("SWM_DESCR") %>' Onclick="lnkReport_Click" ></asp:LinkButton>
         <asp:Label ID="lblSwmId" runat="server" Text='<%# Bind("SWM_ID") %>' Visible="false"></asp:Label>
          <asp:Label ID="lblSubmit" runat="server" Text='<%# Bind("bSUBMIT") %>' Visible="false"></asp:Label>
         </td></tr>
         </table>
         </div>
     </ItemTemplate>
     </asp:DataList>
 </div>
                    </div>
                </div>
            </div>
        </div>
         <asp:HiddenField ID="hfSTU_ID" runat="server" />
               <asp:HiddenField ID="hfACD_ID" runat="server" />
                <asp:HiddenField ID="hfGRD_ID" runat="server" />
</asp:Content>

