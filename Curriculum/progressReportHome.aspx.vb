Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Web.Security
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports System.Collections


Partial Class ProgressReport_Home
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            'Session("HFrpf_id") = "7732"
            'Session("HFrsm_id") = "3510"
            'Session("STP_GRD_ID") = "KG2"
            'Session("STU_BSU_ID") = "131001"
            'Session("RSM_ACD_ID") = "1273"
            'Session("STU_ID") = "20321179"
            'Session("ReportYear_DESC") = "2017-2018"
            'Session("bAOLReport") = "False"
            'Session("Active_menu") = ""
            'Session("RPF_DESCR") = ""
            'Session("ReportYear") = ""
            'Session("HFrpf_date") = ""
            'Session("downloadreport") = 1
            Dim PRS_ID As String

            PRS_ID = Encr_decrData.Decrypt(Request.QueryString("id"))

            GET_SESSIONS(PRS_ID)

            Dim strRPF_ID As String = Session("HFrpf_id")
            Dim strRSM_ID As String = Session("HFrsm_id")
            Dim strGRD_ID As String = Session("STP_GRD_ID")
            Dim strBSU_ID As String = Session("STU_BSU_ID")
            Dim strACD_ID As String = Session("RSM_ACD_ID")
            Dim strSTU_ID As String = Session("STU_ID")


            'UtilityObj.Errorlog(Session("STU_ID"), "Rajesh")

            Dim feepending As Boolean = False
            Dim feepending_tpt As Boolean = False
            If Not Session("bAOLReport") = True Then
                If strRSM_ID = "" AndAlso strRPF_ID = "" Then
                    Exit Sub
                End If
            End If

            If Session("Active_menu").ToString.ToUpper = "ACE REPORTS" Or Session("Active_menu").ToString.ToUpper = "SEP REPORTS" Or (strRSM_ID = 9 And strBSU_ID = "123006") Or (strRSM_ID = 10 And strBSU_ID = "123006") Then
                Select Case strBSU_ID
                    Case "123004"
                        GenerateDMHSAceReport(strRSM_ID, strRPF_ID, strACD_ID, strSTU_ID, strBSU_ID, strGRD_ID)
                    Case "123006"
                        GenerateTMSAceReport(strRSM_ID, strRPF_ID, strACD_ID, strSTU_ID, strBSU_ID, strGRD_ID)
                End Select
                Exit Sub
            Else
                If Not Session("bAOLReport") = True Then
                    UpdateReportViewStatus()
                End If
            End If

            If Session("bResultAnalysis") Is Nothing And Session("HFrpf_id") <> 1 And (Not Session("HFrpf_id") Is Nothing) Then
                feepending = HasPendingFee()
                lblmsg.Text = ""
                If feepending Then
                    h_print.Value = ""
                    lblmsg.Text = GetParentDisplayErrorMsg(False)
                    'lblmsg.Text = "If you are unable to access the report, please contact the school's accounts dept."

                End If
                feepending_tpt = HasPendingFee_TPT()
                If feepending_tpt Then
                    h_print.Value = ""
                    'lblmsg.Text = "If you are unable to access the report, please contact Bright Bus Transport's accounts dept." 
                    If lblmsg.Text <> "" Then
                        lblmsg.Text += vbCrLf
                        lblmsg.Text += GetParentDisplayErrorMsg(True)
                    Else
                        lblmsg.Text = GetParentDisplayErrorMsg(True)
                    End If

                End If
                hFeePending.Value = "0"
                If feepending Or feepending_tpt Then
                    If strRPF_ID = 1387 Then
                        hFeePending.Value = "1"
                    Else
                        Exit Sub
                    End If
                End If

            End If


            Page.Title = "::GEMS EDUCATION| Parent Home Page ::"
            lblmsg.Text = ""
            '' bindstuddetails()
            h_print.Value = "Print"
            'Dim strRPF_ID As String = TryCast(Master.FindControl("HFrpf_id"), HiddenField).Value.ToString
            'Dim strRSM_ID As String = TryCast(Master.FindControl("HFrsm_id"), HiddenField).Value.ToString


            If Not Session("bResultAnalysis") Is Nothing Then
                Session("bResultAnalysis") = Nothing
                GenerateAOLReports()
            ElseIf Session("Active_menu").ToString.ToUpper = "PERFORMANCE GRAPH" Then
                GeneratePerformanceGraph(strRSM_ID, strRPF_ID, strACD_ID, strSTU_ID, strBSU_ID, strGRD_ID)
            ElseIf Session("PerformanceReport") = 1 Then
                GeneratePerformanceGraph(strRSM_ID, strRPF_ID, strACD_ID, strSTU_ID, strBSU_ID, strGRD_ID)
            Else
                BindReportCardType()
                Select Case hfReportType.Value.ToUpper
                    Case "CBSE_FORMATIVE"
                        GenerateFormativeAssessmentReport(strRSM_ID, strRPF_ID, strACD_ID, strSTU_ID, strBSU_ID, strGRD_ID)
                    Case "CBSE_I_SUMMATIVE"
                        GenerateCBSE_I_SummativeAssessmentReport(strRSM_ID, strRPF_ID, strACD_ID, strSTU_ID, strBSU_ID, strGRD_ID)
                    Case "CBSE_I_TERMREPORT_01_03"
                        GenerateCBSE_I_TermReport01_03(strRSM_ID, strRPF_ID, strACD_ID, strSTU_ID, strBSU_ID, strGRD_ID)
                    Case "CBSE_TERMTEST_11-12"
                        GenerateTermTestReport()
                    Case "CBSE_I_KG_REPORT"
                        GenerateCBSE_I_KinderGarten(strRSM_ID, strRPF_ID, strACD_ID, strSTU_ID, strBSU_ID, strGRD_ID)
                    Case "CBSE_I_FINALREPORT"
                        GenerateCBSE_I_FINALREPORT(strRSM_ID, strRPF_ID, strACD_ID, strSTU_ID, strBSU_ID, strGRD_ID)
                    Case "CBSE_MIDTERMFEEDBACK"
                        GenerateCBSEMidTermReport()
                    Case "CBSE_REHEARSAL_10"
                        GenerateRehearsalReport_10(strRSM_ID, strRPF_ID, strACD_ID, strSTU_ID, strBSU_ID, strGRD_ID)
                    Case "CBSE_REHEARSAL_12"
                        GenerateRehearsalReport(strRSM_ID, strRPF_ID, strACD_ID, strSTU_ID, strBSU_ID, strGRD_ID)
                    Case "CIA_REPORTS"
                        GenerateCIAReports(strRSM_ID, strRPF_ID, strACD_ID, strSTU_ID, strBSU_ID, strGRD_ID)
                    Case "MTS_REPORTS"
                        GenerateMTSReports(strRSM_ID, strRPF_ID, strACD_ID, strSTU_ID, strBSU_ID, strGRD_ID)
                    Case "WSA_REPORTS"
                        GenerateWSAReports(strRSM_ID, strRPF_ID, strACD_ID, strSTU_ID, strBSU_ID, strGRD_ID)
                    Case "GIS_REPORTS"
                        GenerateGISReports(strRSM_ID, strRPF_ID, strACD_ID, strSTU_ID, strBSU_ID, strGRD_ID)
                    Case "NMS_REPORTS"
                        GenerateNMSReports(strRSM_ID, strRPF_ID, strACD_ID, strSTU_ID, strBSU_ID, strGRD_ID)
                    Case "WSR_REPORTS"
                        GenerateWSRReports(strRSM_ID, strRPF_ID, strACD_ID, strSTU_ID, strBSU_ID, strGRD_ID)
                    Case "FPS_PROGESSREPORTS_SIMS"
                        GenerateProgressreports_SIMS(strRPF_ID)
                    Case "OOL_IGCE_REPORTS"
                        GenerateOOLReports(strRSM_ID, strRPF_ID, strACD_ID, strSTU_ID, strBSU_ID, strGRD_ID)
                    Case "GIP_REPORTS"
                        GenerateGIPReports(strRSM_ID, strRPF_ID, strACD_ID, strSTU_ID, strBSU_ID, strGRD_ID)
                    Case "DMHS_BENCHMARK"
                        GenerateBENCHMARKReports(strRSM_ID, strRPF_ID, strACD_ID, strSTU_ID, strBSU_ID, strGRD_ID)
                    Case "CBSE_FINAL_FORMAT_01_04"
                        Generate_CBSE_FinalReport01_04(strRSM_ID, strRPF_ID, strACD_ID, strSTU_ID, strBSU_ID, strGRD_ID)
                    Case "CBSE_FINAL_FORMAT_05_08"
                        Generate_CBSE_FinalReport05_08(strRSM_ID, strRPF_ID, strACD_ID, strSTU_ID, strBSU_ID, strGRD_ID)
                    Case "CBSE_FINAL_FORMAT_09"
                        Generate_CBSE_FinalReport09(strRSM_ID, strRPF_ID, strACD_ID, strSTU_ID, strBSU_ID, strGRD_ID)
                    Case "CBSE_NEW_FORMAT_01_04"
                        If Session("RPF_DESCR").ToString.Contains("TERM") Then
                            If Session("RPF_DESCR").ToString.Contains("MID") Then
                                Generate_CBSE_AssessmentReport(strRSM_ID, strRPF_ID, strACD_ID, strSTU_ID, strBSU_ID, strGRD_ID)
                            Else
                                Generate_CBSE_TermReport01_04(strRSM_ID, strRPF_ID, strACD_ID, strSTU_ID, strBSU_ID, strGRD_ID)
                            End If

                        Else
                            Generate_CBSE_AssessmentReport(strRSM_ID, strRPF_ID, strACD_ID, strSTU_ID, strBSU_ID, strGRD_ID)
                        End If

                    Case "CBSE_NEW_FORMAT_05_10"
                        If Session("RPF_DESCR").ToString.Contains("TERM") Then
                            Generate_CBSE_TermReport_05_10(strRSM_ID, strRPF_ID, strACD_ID, strSTU_ID, strBSU_ID, strGRD_ID)
                        Else
                            Generate_CBSE_AssessmentReport(strRSM_ID, strRPF_ID, strACD_ID, strSTU_ID, strBSU_ID, strGRD_ID)
                        End If
                    Case Else

                        Select Case strBSU_ID
                            Case "125005"
                                GenerateCISProgressReport(strACD_ID, strBSU_ID, strRSM_ID, strRPF_ID, strSTU_ID)
                            Case "125015"
                                GenerateWISReports()
                            Case "123004"
                                If Session("HFrpf_id") = 1 Then
                                    GenerateDMHSAceReport(strRSM_ID, strRPF_ID, strACD_ID, strSTU_ID, strBSU_ID, strGRD_ID)
                                ElseIf Session("RPF_DESCR").ToString.Contains("MID TERM") Then
                                    GenerateMidTermReport_DMHS_Dubai()
                                ElseIf Session("RPF_DESCR").ToString.Contains("FINAL REPORT") Then
                                    GenerateFinalReport_DMHS_Dubai(strRSM_ID, strRPF_ID, strACD_ID, strSTU_ID, strBSU_ID, strGRD_ID)
                                Else
                                    GenerateTermlyReport_DMHS_Dubai(strRSM_ID, strRPF_ID, strACD_ID, strSTU_ID, strBSU_ID, strGRD_ID)
                                End If
                            Case "125017"
                                GenerateGWAReports(strRSM_ID, strRPF_ID, strACD_ID, strSTU_ID, strBSU_ID, strGRD_ID)
                            Case "125010"
                                'If hfbFinalReport.Value = "false" Then
                                If Session("RPF_DESCR").ToString.Contains("FINAL") Then
                                    GenerateFinalReport_TWS(strRSM_ID, strRPF_ID, strACD_ID, strSTU_ID, strBSU_ID, strGRD_ID)
                                Else
                                    GenerateTermlyReport_TWS(strRSM_ID, strRPF_ID, strACD_ID, strSTU_ID, strBSU_ID, strGRD_ID)
                                End If

                            Case GetAllCBSEBusinessUnit(strBSU_ID)
                                If Session("bAOLReport") = True Then
                                    GenerateAOLReports()
                                ElseIf strGRD_ID = "KG1" Or strGRD_ID = "KG2" Then
                                    If Session("RPF_DESCR").ToString.Contains("MIDTERM") Then
                                        GenerateCBSEMidTermReport()
                                    Else
                                        GenerateCBSEKinderGarden(strRSM_ID, strRPF_ID, strACD_ID, strSTU_ID, strBSU_ID, strGRD_ID)
                                    End If
                                ElseIf Session("RPF_DESCR").ToString.Contains("FORMATIVE") Then
                                    If Session("downloadreport") = "1" Then
                                        GenerateFormativeAssessmentReport(strRSM_ID, strRPF_ID, strACD_ID, strSTU_ID, strBSU_ID, strGRD_ID)
                                    Else
                                        Response.Redirect("~\Curriculum\HTMLReports\CBSEReport.aspx")
                                    End If
                                ElseIf Session("RPF_DESCR").ToString.Contains("FEEDBACK") And Session("sbsuid") = "123006" Then
                                    GenerateCBSEMidTermReport()
                                ElseIf Session("Clm") = 18 And Session("sbsuid") = "151001" And (strGRD_ID = "09" Or strGRD_ID = "10" Or strGRD_ID = "11") Then
                                    GenerateOOFIGCSEReports()
                                ElseIf Session("RPF_DESCR").ToString.Contains("SUMMATIVE") Then
                                    If Session("downloadreport") = "1" Then
                                        GenerateSummativeAssessmentReport()
                                    Else
                                        If strGRD_ID = "01" Or strGRD_ID = "02" Or strGRD_ID = "03" Or strGRD_ID = "04" Then
                                            Response.Redirect("~\Curriculum\HTMLReports\CBSESummativeReport.aspx")
                                        Else
                                            GenerateSummativeAssessmentReport()
                                        End If
                                    End If
                                ElseIf Session("RPF_DESCR").ToUpper.Contains("REHEARSAL") _
                                Or Session("RPF_DESCR").ToUpper.Contains("MODEL") _
                                Or Session("RPF_DESCR").ToUpper.Contains("REVISION") Then
                                    If strGRD_ID = "12" Then
                                        If strACD_ID = "1144" Then
                                            GenerateCBSEFormativeAssessmentReport_11_12(strRSM_ID, strRPF_ID, strACD_ID, strSTU_ID, strBSU_ID, strGRD_ID)
                                        Else
                                            GenerateRehearsalReport(strRSM_ID, strRPF_ID, strACD_ID, strSTU_ID, strBSU_ID, strGRD_ID)
                                        End If

                                    ElseIf strGRD_ID = "10" Then
                                        GenerateRehearsalReport_10(strRSM_ID, strRPF_ID, strACD_ID, strSTU_ID, strBSU_ID, strGRD_ID)
                                    End If
                                ElseIf Session("RPF_DESCR").ToUpper.Contains("TERM TEST") Or Session("RPF_DESCR").ToUpper.Contains("TERM1 TEST") Or Session("RPF_DESCR").ToUpper.Contains("TERM2 TEST") Then
                                    GenerateTermTestReport(strRSM_ID, strRPF_ID, strACD_ID, strSTU_ID, strBSU_ID, strGRD_ID)
                                ElseIf Session("RPF_DESCR").ToUpper.Contains("TERM") OrElse _
                                Session("RPF_DESCR").ToUpper.Contains("ASSESSMENT") OrElse _
                                Session("RPF_DESCR").ToUpper.Contains("FINAL") OrElse _
                                Session("RPF_DESCR").ToUpper.Contains("QUARTERLY") Or Session("RPF_DESCR").ToUpper.Contains("HALF") Or Session("RPF_DESCR").ToUpper.Contains("ANNUAL") Then
                                    Select Case strGRD_ID
                                        Case "01", "02", "03", "04"
                                            Select Case Session("sBSUID")
                                                Case "121013"
                                                    If strACD_ID = 560 Then
                                                        GenerateTermlyReport_OOEHS_Dubai(strRSM_ID, strRPF_ID, strACD_ID, strSTU_ID, strBSU_ID, strGRD_ID)
                                                    Else
                                                        If Session("RPF_DESCR").ToUpper.Contains("FINAL") Then
                                                            GenerateCBSEFinalReport01_04(strGRD_ID)
                                                        Else
                                                            GenerateCBSETermReport01_04()
                                                        End If
                                                    End If
                                                Case "121014"
                                                    If Session("RPF_DESCR").ToUpper.Contains("FINAL") Then
                                                        GenerateCBSEFinalReport01_04(strGRD_ID)
                                                    Else
                                                        GenerateCBSETermReport01_04()
                                                    End If
                                                Case Else
                                                    If Session("RPF_DESCR").ToUpper.Contains("FINAL") Or Session("RPF_DESCR").ToUpper.Contains("ANNUAL") Then
                                                        GenerateCBSEFinalReport01_04(strGRD_ID)
                                                    Else
                                                        GenerateCBSETermReport01_04()
                                                    End If
                                            End Select
                                        Case Else
                                            Select Case Session("sBSUID")
                                                Case "111001"
                                                    If strGRD_ID = "05" And strACD_ID = "1271" Then
                                                        If Session("RPF_DESCR").ToUpper.Contains("FINAL") Then
                                                            GenerateCBSEFinalReport01_04(strGRD_ID)
                                                        Else
                                                            GenerateCBSETermReport01_04()
                                                        End If
                                                    ElseIf Session("RPF_DESCR").ToUpper.Contains("FINAL") Or Session("RPF_DESCR").ToUpper.Contains("ANNUL") Then
                                                        GenerateCBSEFinalReport01_04(strGRD_ID)
                                                    Else
                                                        GenerateCBSEFormativeAssessmentReport_11_12(strRSM_ID, strRPF_ID, strACD_ID, strSTU_ID, strBSU_ID, strGRD_ID)
                                                    End If
                                                Case "121013"
                                                    If strACD_ID = 560 Then
                                                        GenerateTermlyReport_OOEHS_Dubai(strRSM_ID, strRPF_ID, strACD_ID, strSTU_ID, strBSU_ID, strGRD_ID)
                                                    ElseIf Session("RPF_DESCR").ToUpper.Contains("FINAL") Then
                                                        Select Case strGRD_ID
                                                            Case "11", "12"
                                                                GenerateTermlyReport_OOEHS_Dubai(strRSM_ID, strRPF_ID, strACD_ID, strSTU_ID, strBSU_ID, strGRD_ID)
                                                            Case Else
                                                                GenerateCBSEFinalReport01_04(strGRD_ID)
                                                        End Select
                                                    Else
                                                        GenerateTermlyReport_OOEHS_Dubai(strRSM_ID, strRPF_ID, strACD_ID, strSTU_ID, strBSU_ID, strGRD_ID)
                                                    End If
                                                Case Else
                                                    If Session("RPF_DESCR").ToUpper.Contains("FINAL") Then
                                                        GenerateCBSEFinalReport01_04(strGRD_ID)
                                                    Else
                                                        GenerateCBSEFormativeAssessmentReport_11_12(strRSM_ID, strRPF_ID, strACD_ID, strSTU_ID, strBSU_ID, strGRD_ID)
                                                    End If
                                            End Select
                                    End Select
                                End If
                            Case "121013"
                                feepending = HasPendingFee()

                                If feepending Then
                                    h_print.Value = ""
                                    lblmsg.Text = "If you are unable to access the report, please contact the class teacher"
                                    Exit Sub
                                Else
                                    GenerateTermlyReport_OOEHS_Dubai(strRSM_ID, strRPF_ID, strACD_ID, strSTU_ID, strBSU_ID, strGRD_ID)
                                End If

                            Case "125011"
                                'GenerateTermlyReport_TWS(strRSM_ID, strRPF_ID, strACD_ID, strSTU_ID, strBSU_ID, strGRD_ID)
                                GenerateWINReport(strRSM_ID, strRPF_ID, strACD_ID, strSTU_ID, strBSU_ID, strGRD_ID)
                            Case "126008"
                                GenerateAKNSProgressReport(strRSM_ID, strRPF_ID, strACD_ID, strSTU_ID, strBSU_ID, strGRD_ID)
                            Case "300001"
                                If Session("PrintAOL") Is Nothing Then
                                    GenerateFormativeAssessmentReport(strRSM_ID, strRPF_ID, strACD_ID, strSTU_ID, strBSU_ID, strGRD_ID)
                                Else
                                    GenerateAOLReports()
                                End If
                            Case "125012"
                                GenerateWSDReports(strRSM_ID, strRPF_ID, strACD_ID, strSTU_ID, strBSU_ID, strGRD_ID)
                            Case "115002"
                                GenerateCHSReports(strRSM_ID, strRPF_ID, strACD_ID, strSTU_ID, strBSU_ID, strGRD_ID)
                            Case "114003"
                                GenerateGAAReports(strRSM_ID, strRPF_ID, strACD_ID, strSTU_ID, strBSU_ID, strGRD_ID)
                            Case "114004"
                                GenerateWAAReports(strRSM_ID, strRPF_ID, strACD_ID, strSTU_ID, strBSU_ID, strGRD_ID)
                            Case "135010"
                                GenerateWSSReports(strRSM_ID, strRPF_ID, strACD_ID, strSTU_ID, strBSU_ID, strGRD_ID)
                        End Select
                End Select
            End If
        End If
    End Sub

    Sub GET_SESSIONS(ByVal prsid As String)

        Dim conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim pParms(1) As SqlClient.SqlParameter

        pParms(0) = New SqlParameter("@PRS_ID", prsid)

        Dim dsData As DataSet = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "rpt.GET_PROGRSS_REPORT_SES", pParms)

        If dsData.Tables(0).Rows.Count > 0 Then

            Session("HFrpf_id") = dsData.Tables(0).Rows(0)("PRS_HFrpf_id").ToString
            Session("HFrsm_id") = dsData.Tables(0).Rows(0)("PRS_HFrsm_id").ToString
            Session("STP_GRD_ID") = dsData.Tables(0).Rows(0)("PRS_STP_GRD_ID").ToString
            Session("STU_GRD_ID") = dsData.Tables(0).Rows(0)("PRS_STP_GRD_ID").ToString
            Session("STU_BSU_ID") = dsData.Tables(0).Rows(0)("PRS_STU_BSU_ID").ToString
            Session("RSM_ACD_ID") = dsData.Tables(0).Rows(0)("PRS_RSM_ACD_ID").ToString
            Session("STU_ACD_ID") = dsData.Tables(0).Rows(0)("PRS_RSM_ACD_ID").ToString
            Session("STU_ID") = dsData.Tables(0).Rows(0)("PRS_STU_ID").ToString
            Session("ReportYear_DESC") = dsData.Tables(0).Rows(0)("PRS_ReportYear_DESC").ToString
            Session("bAOLReport") = dsData.Tables(0).Rows(0)("PRS_bAOLReport").ToString
            Session("Active_menu") = dsData.Tables(0).Rows(0)("PRS_Active_menu").ToString
            Session("RPF_DESCR") = dsData.Tables(0).Rows(0)("PRS_RPF_DESCR").ToString
            Session("ReportYear") = dsData.Tables(0).Rows(0)("PRS_ReportYear").ToString
            Session("HFrpf_date") = dsData.Tables(0).Rows(0)("PRS_HFrpf_date").ToString
            Session("downloadreport") = dsData.Tables(0).Rows(0)("PRS_downloadreport").ToString
            Session("SBSUID") = dsData.Tables(0).Rows(0)("PRS_STU_BSU_ID").ToString
            Session("username") = dsData.Tables(0).Rows(0)("PRS_USR_NAME").ToString
        End If







    End Sub

    Private Sub Generate_CBSE_FinalReport01_04(ByVal strRSM_ID As String, ByVal strRPF_ID As String, ByVal strACD_ID As String, ByVal strSTU_ID As String, ByVal strBSU_ID As String, ByVal strGRD_ID As String)
        Dim param As New Hashtable
        param.Add("@ACD_ID", strACD_ID)
        param.Add("@BSU_ID", strBSU_ID)
        param.Add("@IMG_BSU_ID", strBSU_ID)
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@RSM_ID", strRSM_ID)
        param.Add("@RPF_ID", strRPF_ID)
        param.Add("@STU_ID", strSTU_ID)
        param.Add("accYear", Session("ReportYear_DESC"))
        param.Add("RPT_CAPTION", Session("RPF_DESCR"))

        param.Add("UserName", Session("username"))


        param.Add("GRD_ID", strGRD_ID)
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            '.Photos = GetPhotoClass()

            .reportPath = Server.MapPath("ProgressReports/Rpt/rptCBSE_FINAL_REPORT_01_04.rpt")


        End With
        Session("rptClass") = rptClass
        If Session("downloadreport") = "1" Then
            DownloadReports(rptClass)
        Else
            LoadReports(rptClass)
        End If
    End Sub
    Private Sub Generate_CBSE_FinalReport05_08(ByVal strRSM_ID As String, ByVal strRPF_ID As String, ByVal strACD_ID As String, ByVal strSTU_ID As String, ByVal strBSU_ID As String, ByVal strGRD_ID As String)
        Dim param As New Hashtable
        param.Add("@ACD_ID", strACD_ID)
        param.Add("@BSU_ID", strBSU_ID)
        param.Add("@IMG_BSU_ID", strBSU_ID)
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@RSM_ID", strRSM_ID)
        param.Add("@RPF_ID", strRPF_ID)
        param.Add("@STU_ID", strSTU_ID)
        param.Add("accYear", Session("ReportYear_DESC"))
        param.Add("RPT_CAPTION", Session("RPF_DESCR"))

        param.Add("UserName", Session("username"))


        param.Add("GRD_ID", strGRD_ID)
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            '.Photos = GetPhotoClass()

            .reportPath = Server.MapPath("ProgressReports/Rpt/rptCBSE_FINAL_REPORT_05_08.rpt")


        End With
        Session("rptClass") = rptClass
        If Session("downloadreport") = "1" Then
            DownloadReports(rptClass)
        Else
            LoadReports(rptClass)
        End If
    End Sub
    Private Sub Generate_CBSE_FinalReport09(ByVal strRSM_ID As String, ByVal strRPF_ID As String, ByVal strACD_ID As String, ByVal strSTU_ID As String, ByVal strBSU_ID As String, ByVal strGRD_ID As String)
        Dim param As New Hashtable
        param.Add("@ACD_ID", strACD_ID)
        param.Add("@BSU_ID", strBSU_ID)
        param.Add("@IMG_BSU_ID", strBSU_ID)
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@RSM_ID", strRSM_ID)
        param.Add("@RPF_ID", strRPF_ID)
        param.Add("@STU_ID", strSTU_ID)
        param.Add("accYear", Session("ReportYear_DESC"))
        param.Add("RPT_CAPTION", Session("RPF_DESCR"))

        param.Add("UserName", Session("username"))


        param.Add("GRD_ID", strGRD_ID)
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            '.Photos = GetPhotoClass()

            .reportPath = Server.MapPath("ProgressReports/Rpt/rptCBSE_FINAL_REPORT_09_10 .rpt")


        End With
        Session("rptClass") = rptClass
        If Session("downloadreport") = "1" Then
            DownloadReports(rptClass)
        Else
            LoadReports(rptClass)
        End If
    End Sub


    Private Sub Generate_CBSE_TermReport_05_10(ByVal strRSM_ID As String, ByVal strRPF_ID As String, ByVal strACD_ID As String, ByVal strSTU_ID As String, ByVal strBSU_ID As String, ByVal strGRD_ID As String)
        Dim param As New Hashtable
        param.Add("@ACD_ID", strACD_ID)
        param.Add("@BSU_ID", strBSU_ID)
        param.Add("@IMG_BSU_ID", strBSU_ID)
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@RSM_ID", strRSM_ID)
        param.Add("@RPF_ID", strRPF_ID)
        param.Add("@STU_ID", strSTU_ID)
        param.Add("accYear", Session("ReportYear_DESC"))


        param.Add("UserName", Session("username"))


        param.Add("GRD_ID", strGRD_ID)
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            '.Photos = GetPhotoClass()

            .reportPath = Server.MapPath("ProgressReports/Rpt/rptCBSE_TERM_REPORT_05_10.rpt")



        End With
        Session("rptClass") = rptClass
        If Session("downloadreport") = "1" Then
            DownloadReports(rptClass)
        Else
            LoadReports(rptClass)
        End If
    End Sub
    Private Sub Generate_CBSE_AssessmentReport(ByVal strRSM_ID As String, ByVal strRPF_ID As String, ByVal strACD_ID As String, ByVal strSTU_ID As String, ByVal strBSU_ID As String, ByVal strGRD_ID As String)
        Dim param As New Hashtable
        param.Add("@ACD_ID", strACD_ID)
        param.Add("@BSU_ID", strBSU_ID)
        param.Add("@RSM_ID", strRSM_ID)
        param.Add("@RPF_ID", strRPF_ID)
        param.Add("@STU_ID", strSTU_ID)
        param.Add("RPT_CAPTION", Session("RPF_DESCR"))

        param.Add("UserName", Session("username"))

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            '.Photos = GetPhotoClass()

            ' Select GetReportType()
            '    Case "FORMATIVE_ASSESSMENT_KGS"
            '        .reportPath = Server.MapPath("../Rpt/rptCBSE_FORMATIVE_ASSESSMENT_REPORT_03_KGS.rpt")
            '    Case Else
            .reportPath = Server.MapPath("ProgressReports/Rpt/rptCBSE_ASSESSMENT_REPORT_01_10.rpt")
            'End Select
        End With
        Session("rptClass") = rptClass
        If Session("downloadreport") = "1" Then
            DownloadReports(rptClass)
        Else
            LoadReports(rptClass)
        End If
    End Sub
    Private Sub Generate_CBSE_TermReport01_04(ByVal strRSM_ID As String, ByVal strRPF_ID As String, ByVal strACD_ID As String, ByVal strSTU_ID As String, ByVal strBSU_ID As String, ByVal strGRD_ID As String)
        Dim param As New Hashtable
        param.Add("@ACD_ID", strACD_ID)
        param.Add("@BSU_ID", strBSU_ID)
        param.Add("@IMG_BSU_ID", strBSU_ID)
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@RSM_ID", strRSM_ID)
        param.Add("@RPF_ID", strRPF_ID)
        param.Add("@STU_ID", strSTU_ID)
        param.Add("accYear", Session("ReportYear_DESC"))
        param.Add("RPT_CAPTION", Session("RPF_DESCR"))

        param.Add("UserName", Session("username"))
        'param.Add("SA_HEADER", "SA")
        'param.Add("FA1_PERC", "10%")
        'param.Add("FA2_PERC", "10%")
        'param.Add("SA_PERC", "20%")
        'param.Add("TOTAL_PERC", "60%")
        'param.Add("TOTAL_FA_PERC", "20%")

        'If ddlReportPrintedFor.SelectedItem.Text.ToUpper = "SUMMATIVE REPORT 1" Or ddlReportPrintedFor.SelectedItem.Text.ToUpper = "TERM 1 REPORT" Then
        '    param.Add("FA_HEADER1", "FA1")
        '    param.Add("FA_HEADER2", "FA2")
        'Else
        '    param.Add("FA_HEADER1", "FA3")
        '    param.Add("FA_HEADER2", "FA4")
        'End If

        param.Add("GRD_ID", strGRD_ID)
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            '.Photos = GetPhotoClass()

            .reportPath = Server.MapPath("ProgressReports/Rpt/rptCBSE_TERM_REPORT_01_04.rpt")


        End With
        Session("rptClass") = rptClass
        If Session("downloadreport") = "1" Then
            DownloadReports(rptClass)
        Else
            LoadReports(rptClass)
        End If
    End Sub
    Sub GeneratePerformanceGraph(ByVal strRSM_ID As String, ByVal strRPF_ID As String, ByVal strACD_ID As String, ByVal strSTU_ID As String, ByVal strBSU_ID As String, ByVal strGRD_ID As String)
        Dim param As New Hashtable
        param.Add("UserName", "")
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RPF_ID", strRPF_ID)
        param.Add("@ACD_ID", strACD_ID)
        param.Add("@STU_ID", strSTU_ID)

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            .reportPath = Server.MapPath("ProgressReports/Rpt/rpt_RA_HLAP_GRAPH_STUDWISE.rpt")
        End With
        Session("PerformanceReport") = 0
        Session("rptClass") = rptClass
        If Session("downloadreport") = "1" Then
            DownloadReports(rptClass)
        Else
            LoadReports(rptClass)
        End If
    End Sub

    Sub UpdateReportViewStatus()
        Dim status As Boolean = GetReportCardStatus(Session("HFrpf_id"), Session("STU_ID"))
        If status = True Then
            AuditReportView()
        End If
    End Sub

    Private Sub AuditReportView()

        Dim param(6) As SqlClient.SqlParameter
        param(0) = New SqlClient.SqlParameter("@PRA_OLU_ID", Session("OLU_ID"))
        param(1) = New SqlClient.SqlParameter("@PRA_OLU_NAME", Session("username"))
        param(2) = New SqlClient.SqlParameter("@PRA_RPF_ID", Session("HFrpf_id"))
        param(3) = New SqlClient.SqlParameter("@PRA_RPF_DESCR", Session("RPF_DESCR"))
        param(4) = New SqlClient.SqlParameter("@PRA_STU_ID", Session("STU_ID"))
        param(5) = New SqlClient.SqlParameter("@Return_value", SqlDbType.Int)
        param(5).Direction = ParameterDirection.ReturnValue
        Dim status As Integer

        SqlHelper.ExecuteNonQuery(ConnectionManger.GetOASISConnectionString, CommandType.StoredProcedure, "ONLINE.SAVEPROGRESSREPORT_AUDIT", param)
        status = param(5).Value
    End Sub

    Public Function GetReportCardStatus(ByVal vRPF_ID As String, ByVal vSTU_ID As String) As Boolean
        Dim SLQSTRING As String = "select count(*) from OASIS.ONLINE.PROGRESS_REPORT_VIEWERS where pra_rpf_id = " & vRPF_ID & _
        " AND PRA_STU_ID = " & vSTU_ID
        Dim viewed_count As Integer = SqlHelper.ExecuteScalar(ConnectionManger.GetOASIS_CURRICULUMConnectionString, CommandType.Text, SLQSTRING)
        If viewed_count = 0 Then
            Return True
        Else
            Return False
        End If

    End Function

    Private Sub GenerateTermTestReport()
        Dim param As New Hashtable
        param.Add("@ACD_ID", Session("RSM_ACD_ID"))
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RSM_ID", Session("HFrsm_id"))
        param.Add("@RPF_ID", Session("HFrpf_id"))
        param.Add("@STU_ID", Session("STU_ID"))
        param.Add("RPT_CAPTION", Session("RPF_DESCR"))
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            '.Photos = GetPhotoClass()
            If Session("sBSUID") = "151001" Then
                param.Add("RPT_ASSESSMENT_HEADER1", "Test 1")
                param.Add("RPT_ASSESSMENT_HEADER2", "Test 2")
                .reportPath = Server.MapPath("ProgressReports/Rpt/rptCBSE_OOW_UNIT_TEST_REPORT_11_12.rpt")
            ElseIf Session("ReportYear_DESC") = "2011-2012" And Session("SBSUID") = "131001" Then
                If Session("RPF_DESCR") = "FIRST TERM TEST" Then
                    param.Add("RPT_ASSESSMENT_HEADER1", "Test 1(Apr) out of 10")
                    param.Add("RPT_ASSESSMENT_HEADER2", "Test 2(May) out of 10")
                    param.Add("RPT_ASSESSMENT_HEADER3", "Test 3(June) out of 20")
                    .reportPath = Server.MapPath("ProgressReports/Rpt/rptCBSE_OOS_UNIT_TEST_REPORT_11_12.rpt")
                Else
                    param.Add("RPT_ASSESSMENT_HEADER1", "Test 1(Nov) out of 10")
                    param.Add("RPT_ASSESSMENT_HEADER2", "Test 2(Dec) out of 10")
                    param.Add("RPT_ASSESSMENT_HEADER3", "Test 3(Jan) out of 20")
                    param.Add("RPT_ASSESSMENT_HEADER4", "Test 3(Feb) out of 10")
                    .reportPath = Server.MapPath("../Rpt/rptCBSE_OOS_UNIT_TEST_REPORT_11_12_term2.rpt")
                End If
            ElseIf Session("sBSUID") = "121014" And Session("ReportYear_DESC") = "2010-2011" Then
                param.Add("RPT_ASSESSMENT_HEADER1", "Test 1")
                param.Add("RPT_ASSESSMENT_HEADER2", "Test 2")
                .reportPath = Server.MapPath("ProgressReports/Rpt/rptCBSE_OOW_UNIT_TEST_REPORT_11_12.rpt")
            ElseIf hfReportFormat.Value = "FORMAT2" Then
                param.Add("RPT_ASSESSMENT_HEADER", "Marks out of 25")
                .reportPath = Server.MapPath("ProgressReports/Rpt/rptCBSE_UNIT_TEST_REPORT_11_12_FORMAT2.rpt")
            Else
                param.Add("RPT_ASSESSMENT_HEADER", "Marks out of 25")
                .reportPath = Server.MapPath("ProgressReports/Rpt/rptCBSE_UNIT_TEST_REPORT_11_12.rpt")
            End If
        End With
        Session("rptClass") = rptClass
        If Session("downloadreport") = "1" Then
            DownloadReports(rptClass)
        Else
            LoadReports(rptClass)
        End If
    End Sub

    Private Sub GenerateCBSEMidTermReport()
        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@ACD_ID", Session("RSM_ACD_ID"))
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RSM_ID", Session("HFrsm_id"))
        param.Add("@RPF_ID", Session("HFrpf_id"))
        param.Add("@STU_ID", Session("STU_ID"))
        'param.Add("RPT_CAPTION", ddlReportPrintedFor.SelectedItem.Text)
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            Select Case Session("STU_GRD_ID")
                Case "KG1", "KG2"
                    .reportPath = Server.MapPath("ProgressReports/Rpt/rptCBSE_MIDTERMFEEDBACKREPORT_KG.rpt")
                Case Else
                    .reportPath = Server.MapPath("ProgressReports/Rpt/rptCBSE_MIDTERMFEEDBACKREPORT.rpt")
            End Select
        End With
        Session("rptClass") = rptClass
        If Session("downloadreport") = "1" Then
            DownloadReports(rptClass)
        Else
            LoadReports(rptClass)
        End If
    End Sub

    Sub GenerateGAAReports(ByVal RSM_ID As String, ByVal RPF_ID As String, ByVal ACD_ID As String, ByVal STU_ID As String, ByVal BSU_ID As String, ByVal GRD_ID As String)
        Dim param As New Hashtable
        param.Add("@ACD_ID", ACD_ID)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RSM_ID", RSM_ID)
        param.Add("@RPF_ID", RPF_ID)
        param.Add("@STU_ID", STU_ID)
        param.Add("RPF_DESCR", Session("RPF_DESCR"))
        param.Add("RPF_DATE", Now.ToShortDateString)

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            .Photos = GetPhotoClass(STU_ID)
            If ACD_ID = 574 Then
                .reportPath = Server.MapPath("ProgressReports/Rpt/GAA/rptGAA_ElementarySchool_2009-2010.rpt")
            ElseIf ACD_ID = 598 Then
                .reportPath = Server.MapPath("ProgressReports/Rpt/GAA/rptGAA_ElementarySchool_2010-2011.rpt")
            ElseIf ACD_ID = 622 Then
                Select Case GRD_ID
                    Case "PK"
                        If Session("RPF_DESCR").ToUpper = "TERM 3 REPORT" Then
                            .reportPath = Server.MapPath("ProgressReports/Rpt/GAA/rptGAA_ElementarySchool_KG2_2011-2012.rpt")
                        Else
                            .reportPath = Server.MapPath("ProgressReports/Rpt/GAA/rptGAA_ElementarySchool_KG1_2011-2012.rpt")
                        End If
                    Case "KG1"
                        .reportPath = Server.MapPath("ProgressReports/Rpt/GAA/rptGAA_ElementarySchool_KG2_2011-2012.rpt")
                    Case "KG2"
                        .reportPath = Server.MapPath("ProgressReports/Rpt/GAA/rptGAA_ElementarySchool_KG2(5)_2011-2012.rpt")
                    Case "06", "07", "08"
                        .reportPath = Server.MapPath("ProgressReports/Rpt/GAA/rptGAA_ElementarySchool_2011-2012_06_08.rpt")
                    Case Else
                        .reportPath = Server.MapPath("ProgressReports/Rpt/GAA/rptGAA_ElementarySchool_2011-2012.rpt")

                End Select
            ElseIf ACD_ID = 822 Then
                Select Case GRD_ID
                    Case "PK"
                        '.reportPath = Server.MapPath("ProgressReports/Rpt/GAA/rptGAA_ElementarySchool_KG2_2011-2012.rpt")
                        If Session("RPF_DESCR").ToUpper = "TERM 3 REPORT" Then
                            .reportPath = Server.MapPath("ProgressReports/Rpt/GAA/rptGAA_ElementarySchool_KG2_2011-2012.rpt")
                        Else
                            .reportPath = Server.MapPath("ProgressReports/Rpt/GAA/rptGAA_ElementarySchool_KG1_2011-2012.rpt")
                        End If
                    Case "KG1"
                        .reportPath = Server.MapPath("ProgressReports/Rpt/GAA/rptGAA_ElementarySchool_KG2_2011-2012.rpt")
                    Case "06", "07", "08"
                        .reportPath = Server.MapPath("ProgressReports/Rpt/GAA/rptGAA_ElementarySchool_2011-2012_06_08.rpt")
                    Case Else
                        .reportPath = Server.MapPath("ProgressReports/Rpt/GAA/rptGAA_ElementarySchool_2011-2012.rpt")
                End Select
            ElseIf ACD_ID = 893 Then
                '  Select Case ddlGrade.SelectedValue.ToString
                ' Case "PK", "KG1"
                .reportPath = Server.MapPath("ProgressReports/Rpt/GAA/rptGAA_ElementarySchool_2013-2014.rpt")
                '  Case Else
                ' .reportPath = Server.MapPath("ProgressReports/Rpt/GAA/rptGAA_ElementarySchool_2011-2012.rpt")
                'End Select

            ElseIf ACD_ID = 974 Then
                .reportPath = Server.MapPath("ProgressReports/Rpt/GAA/rptGAA_ElementarySchool.rpt")
            Else
                .reportPath = Server.MapPath("ProgressReports/Rpt/GAA/rptGAA_ElementarySchool_2015-16.rpt")
            End If
        End With
        Session("rptClass") = rptClass
        If Session("downloadreport") = "1" Then
            DownloadReports(rptClass)
        Else
            LoadReports(rptClass)
        End If
    End Sub

    Sub GenerateWAAReports(ByVal RSM_ID As String, ByVal RPF_ID As String, ByVal ACD_ID As String, ByVal STU_ID As String, ByVal BSU_ID As String, ByVal GRD_ID As String)
        Dim param As New Hashtable
        param.Add("@ACD_ID", ACD_ID)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RSM_ID", RSM_ID)
        param.Add("@RPF_ID", RPF_ID)
        param.Add("@STU_ID", STU_ID)
        param.Add("RPF_DESCR", Session("RPF_DESCR"))
        param.Add("RPF_DATE", Now.ToShortDateString)

        'Session("studentphotopath") = TransferPath(h_STU_IDs.Value.Replace("___", "|"))
        'param.Add("UserName", Session("sUsr_name"))
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            .Photos = GetPhotoClass(STU_ID)
            If ACD_ID = 958 Then
                Select Case GRD_ID
                    Case "PK", "KG1"
                        .reportPath = Server.MapPath("ProgressReports/Rpt/WAA/rptWAA_ElementarySchool_KG1-KG2(4).rpt")
                    Case Else
                        .reportPath = Server.MapPath("ProgressReports/Rpt/WAA/rptWAA_ElementarySchool.rpt")
                End Select
            Else
                .reportPath = Server.MapPath("ProgressReports/Rpt/WAA/rptWAA_ElementarySchool_2015-16.rpt")
            End If

        End With
        Session("rptClass") = rptClass
        If Session("downloadreport") = "1" Then
            DownloadReports(rptClass)
        Else
            LoadReports(rptClass)
        End If
    End Sub


    Sub BindReportCardType()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT ISNULL(RSM_REPORTTYPE,''),ISNULL(RSM_REPORTFORMAT,'') FROM RPT.REPORT_SETUP_M WHERE RSM_ID='" + Session("HFrsm_id") + "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        hfReportType.Value = ds.Tables(0).Rows(0).Item(0)
        hfReportFormat.Value = ds.Tables(0).Rows(0).Item(1)
    End Sub

    Private Sub GenerateCBSE_I_SummativeAssessmentReport(ByVal strRSM_ID As String, ByVal strRPF_ID As String, ByVal strACD_ID As String, ByVal strSTU_ID As String, ByVal strBSU_ID As String, ByVal strGRD_ID As String)
        Dim param As New Hashtable
        param.Add("@ACD_ID", strACD_ID)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        ' param.Add("@IMG_TYPE", "LOGO")
        param.Add("@RSM_ID", strRSM_ID)
        param.Add("@RPF_ID", strRPF_ID)
        param.Add("@STU_ID", strSTU_ID)
        param.Add("accYear", Session("ReportYear_DESC"))
        If Session("RPF_DESCR").ToString.ToUpper = "SUMMATIVE REPORT 1" Then
            param.Add("RPT_CAPTION", "TERM I REPORT")
            param.Add("FA1_PERC", "10%")
            param.Add("FA2_PERC", "10%")
            param.Add("TOTAL_FA_PERC", "20%")

            param.Add("SA_PERC", "30%")
            param.Add("TOTAL_PERC", "50%")

            param.Add("FA_HEADER1", "FA1")
            param.Add("FA_HEADER2", "FA2")
        Else
            param.Add("RPT_CAPTION", "TERM II REPORT")
            param.Add("FA1_PERC", "10%")
            param.Add("FA2_PERC", "10%")
            param.Add("TOTAL_FA_PERC", "20%")

            param.Add("SA_PERC", "30%")
            param.Add("TOTAL_PERC", "50%")

            param.Add("FA_HEADER1", "FA3")
            param.Add("FA_HEADER2", "FA4")
        End If


        Dim vSA_HEADER As String = "SA"
        param.Add("SA_HEADER", vSA_HEADER)
        param.Add("UserName", "")
        param.Add("GRD_ID", strGRD_ID)
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            .reportPath = Server.MapPath("ProgressReports/Rpt/CBSE-I/rptCBSE_I_SUMMATIVE_ASSESSMENT_REPORT_05_10.rpt")
        End With
        Session("rptClass") = rptClass
        If Session("downloadreport") = "1" Then
            DownloadReports(rptClass)
        Else
            LoadReports(rptClass)
        End If
    End Sub


    Private Sub GenerateCBSE_I_TermReport01_03(ByVal strRSM_ID As String, ByVal strRPF_ID As String, ByVal strACD_ID As String, ByVal strSTU_ID As String, ByVal strBSU_ID As String, ByVal strGRD_ID As String)
        Dim param As New Hashtable
        param.Add("@ACD_ID", strACD_ID)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        ' param.Add("@IMG_TYPE", "LOGO")
        param.Add("@RSM_ID", strRSM_ID)
        param.Add("@RPF_ID", strRPF_ID)
        param.Add("@STU_ID", strSTU_ID)
        param.Add("accYear", Session("ReportYear_DESC"))
        param.Add("RPT_CAPTION", Session("RPF_DESCR"))

        param.Add("UserName", "")
        param.Add("SA_HEADER", "SA")
        param.Add("FA1_PERC", "10%")
        param.Add("FA2_PERC", "10%")
        param.Add("SA_PERC", "20%")
        param.Add("TOTAL_PERC", "60%")
        param.Add("TOTAL_FA_PERC", "20%")

        If Session("RPF_DESCR").ToString.ToUpper = "SUMMATIVE REPORT 1" Or Session("RPF_DESCR").ToString.ToUpper = "TERM 1 REPORT" Then
            param.Add("FA_HEADER1", "FA1")
            param.Add("FA_HEADER2", "FA2")
        Else
            param.Add("FA_HEADER1", "FA3")
            param.Add("FA_HEADER2", "FA4")
        End If

        param.Add("GRD_ID", strGRD_ID)
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            '.Photos = GetPhotoClass()
            .reportPath = Server.MapPath("ProgressReports/Rpt/CBSE-I/rptCBSE_I_SUMMATIVE_ASSESSMENT_REPORT_01_04.rpt")
        End With
        Session("rptClass") = rptClass
        If Session("downloadreport") = "1" Then
            DownloadReports(rptClass)
        Else
            LoadReports(rptClass)
        End If
    End Sub
    Private Sub GenerateCBSE_I_FINALREPORT(ByVal strRSM_ID As String, ByVal strRPF_ID As String, ByVal strACD_ID As String, ByVal strSTU_ID As String, ByVal strBSU_ID As String, ByVal strGRD_ID As String)
        Dim param As New Hashtable
        param.Add("@ACD_ID", strACD_ID)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@RSM_ID", strRSM_ID)
        param.Add("@RPF_ID", strRPF_ID)
        param.Add("@STU_ID", strSTU_ID)
        param.Add("RPT_CAPTION", "")
        param.Add("GRD_ID", strGRD_ID)

        Dim rptClass As New rptClass
        With rptClass
            '   .Photos = GetPhotoClass()
            .crDatabase = "oasis_curriculum"
            .reportParameters = param

            Select Case hfReportFormat.Value
                Case "CBSE_I_FINALREPORT_01_03"
                    .Photos = GetPhotoClass(strSTU_ID)
                    .reportPath = Server.MapPath("ProgressReports/Rpt/CBSE-I/rptCBSE_I_FINALREPORT_01_03_2013.rpt")
                Case "CBSE_I_FINALREPORT_04_05"
                    .Photos = GetPhotoClass(strSTU_ID)
                    .reportPath = Server.MapPath("ProgressReports/Rpt/CBSE-I/rptCBSE_I_FINALREPORT_04_05_2013.rpt")
                Case Else
                    Select Case strGRD_ID
                        Case "01", "02", "03"
                            .reportPath = Server.MapPath("ProgressReports/Rpt/CBSE-I/rptCBSE_I_FINALREPORT_01_03.rpt")
                        Case "04", "05"
                            .reportPath = Server.MapPath("ProgressReports/Rpt/CBSE-I/rptCBSE_I_FINALREPORT_04_05.rpt")
                        Case Else
                            .Photos = GetPhotoClass(strSTU_ID)
                            .reportPath = Server.MapPath("ProgressReports/Rpt/CBSE-I/rptCBSE_I_FINALREPORT_06_10.rpt")
                    End Select
            End Select

        End With
        Session("rptClass") = rptClass
        If Session("downloadreport") = "1" Then
            DownloadReports(rptClass)
        Else
            LoadReports(rptClass)
        End If
    End Sub


    Private Sub GenerateCBSE_I_KinderGarten(ByVal strRSM_ID As String, ByVal strRPF_ID As String, ByVal strACD_ID As String, ByVal strSTU_ID As String, ByVal strBSU_ID As String, ByVal strGRD_ID As String)
        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("sbsuid"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@ACD_ID", strACD_ID)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RSM_ID", strRSM_ID)
        param.Add("@RPF_ID", strRPF_ID)
        param.Add("@STU_ID", strSTU_ID)
        param.Add("accYear", Session("ReportYear_DESC"))

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            .Photos = GetPhotoClass(strSTU_ID)
            If strACD_ID = "1231" Then
                .reportPath = Server.MapPath("ProgressReports/Rpt/CBSE-I/rptCBSE_I_KG_TERMREPORT_GHS.rpt")
            Else
                .reportPath = Server.MapPath("ProgressReports/Rpt/CBSE-I/rptCBSE_I_KG_TERMREPORT.rpt")
            End If


        End With
        Session("rptClass") = rptClass
        If Session("downloadreport") = "1" Then
            DownloadReports(rptClass)
        Else
            LoadReports(rptClass)
        End If
    End Sub


    'Private Sub GenerateRehearsalReport_10(ByVal RSM_ID As String, ByVal RPF_ID As String, ByVal ACD_ID As String, ByVal STU_ID As String, ByVal BSU_ID As String, ByVal GRD_ID As String)
    '    Dim param As New Hashtable
    '    param.Add("@ACD_ID", ACD_ID)
    '    param.Add("@BSU_ID", Session("SBSUID"))
    '    param.Add("@RSM_ID", RSM_ID)
    '    param.Add("@RPF_ID", RPF_ID)
    '    param.Add("@STU_ID", STU_ID)
    '    param.Add("RPT_CAPTION", Session("RPF_DESCR"))
    '    If ACD_ID = 624 Then
    '        param.Add("RPT_ASSESSMENT_HEADER", "EXAM")

    '    Else
    '        param.Add("RPT_ASSESSMENT_HEADER", "REHEARSAL EXAM")

    '    End If

    '    Dim rptClass As New rptClass
    '    With rptClass
    '        .crDatabase = "oasis_curriculum"
    '        .reportParameters = param
    '        .reportPath = Server.MapPath("ProgressReports/Rpt/rptCBSE_FORMATIVE_ASSESSMENT_REPORT_03.rpt")
    '    End With
    '    Session("rptClass") = rptClass
    '    If Session("downloadreport") = "1" Then
    '        DownloadReports(rptClass)
    '    Else
    '        LoadReports(rptClass)
    '    End If
    'End Sub
    Private Sub GenerateRehearsalReport_10(ByVal RSM_ID As String, ByVal RPF_ID As String, ByVal ACD_ID As String, ByVal STU_ID As String, ByVal BSU_ID As String, ByVal GRD_ID As String)
        Dim param As New Hashtable
        param.Add("@ACD_ID", ACD_ID)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RSM_ID", RSM_ID)
        param.Add("@RPF_ID", RPF_ID)
        param.Add("@STU_ID", STU_ID)
        param.Add("RPT_CAPTION", Session("RPF_DESCR"))

        If ACD_ID = "624" Then
            param.Add("RPT_ASSESSMENT_HEADER", "EXAM")
        Else
            param.Add("RPT_ASSESSMENT_HEADER", "REHEARSAL EXAM")
        End If

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            .reportPath = Server.MapPath("ProgressReports/Rpt/rptCBSE_FORMATIVE_ASSESSMENT_REPORT_03.rpt")
        End With
        Session("rptClass") = rptClass
        ' Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        If Session("downloadreport") = "1" Then
            DownloadReports(rptClass)
        Else
            LoadReports(rptClass)
        End If
    End Sub

    Private Sub GenerateRehearsalReport(ByVal RSM_ID As String, ByVal RPF_ID As String, ByVal ACD_ID As String, ByVal STU_ID As String, ByVal BSU_ID As String, ByVal GRD_ID As String)
        Dim param As New Hashtable
        param.Add("@ACD_ID", ACD_ID)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RSM_ID", RSM_ID)
        param.Add("@RPF_ID", RPF_ID)
        param.Add("@STU_ID", STU_ID)
        param.Add("RPT_CAPTION", Session("RPF_DESCR"))
        param.Add("ATT_ASON_DATE", GetAsOnDateForAttendance(ACD_ID, GRD_ID, RPF_ID))
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            Select Case hfReportFormat.Value
                Case "GRADE_11_12_MODEL_FORMAT1"
                    .reportPath = Server.MapPath("ProgressReports/Rpt/rptTERM_REPORT_CBSE_GRD11_12_MODEL.rpt")
                Case "GRADE_11_12_MODEL_FORMAT2"
                    .reportPath = Server.MapPath("ProgressReports/Rpt/rptTERM_REPORT_CBSE_GRD11_12_MODEL_FORMAT1.rpt")
                Case "GRADE_11_12_MODEL_FORMAT3"
                    .reportPath = Server.MapPath("ProgressReports/Rpt/rptTERM_REPORT_CBSE_GRD11_12_MODEL_FORMAT2.rpt")
            End Select
        End With
        If Session("downloadreport") = "1" Then
            DownloadReports(rptClass)
        Else
            LoadReports(rptClass)
        End If
    End Sub
    Private Sub GenerateWSAReports(ByVal RSM_ID As String, ByVal RPF_ID As String, ByVal ACD_ID As String, ByVal STU_ID As String, ByVal BSU_ID As String, ByVal GRD_ID As String)
        Dim param As New Hashtable
        param.Add("@ACD_ID", ACD_ID)
        param.Add("@BSU_ID", BSU_ID)
        param.Add("@RSM_ID", RSM_ID)
        param.Add("@RPF_ID", RPF_ID)
        param.Add("@STU_ID", STU_ID)
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            '.Photos = GetPhotoClass()
            Select Case hfReportFormat.Value
                Case "PROGRESS_REPORTS"
                    param.Add("GRD_ID", GRD_ID)
                    param.Add("CAPTION", Session("RPF_DESCR"))
                    .reportPath = Server.MapPath("ProgressReports/Rpt/WSA/rptWSA_PROGRESSREPORT_01_06.rpt")
                Case "PROGRESS_REPORTS_2015"
                    param.Add("GRD_ID", GRD_ID)
                    param.Add("CAPTION", Session("RPF_DESCR"))
                    .reportPath = Server.MapPath("ProgressReports/Rpt/WSA/rptWSA_PROGRESSREPORT_01_06_2015-16.rpt")
                Case "PROGRESS_REPORTS_2016"
                    param.Add("GRD_ID", GRD_ID)
                    param.Add("CAPTION", Session("RPF_DESCR"))
                    If Session("RPF_DESCR").Contains("END OF YEAR REPORT") Then
                        .reportPath = Server.MapPath("ProgressReports/Rpt/WSA/rptWSA_ENDOFYEARREPORT_2016-17.rpt")
                    Else
                        .reportPath = Server.MapPath("ProgressReports/Rpt/WSA/rptWSA_PROGRESSREPORT_01_06_2016-17.rpt")
                    End If
                Case "PROGRESS_REPORTS_2017"
                    param.Add("GRD_ID", GRD_ID)
                    param.Add("CAPTION", Session("RPF_DESCR"))

                    .reportPath = Server.MapPath("ProgressReports/Rpt/WSA/rptWSA_ENDOFYEARREPORT_2016-17.rpt")

                Case "PROGRESS_REPORTS_TERM2"
                Case "PROGRESS_REPORTS_TERM2"
                    param.Add("GRD_ID", GRD_ID)
                    param.Add("CAPTION", Session("RPF_DESCR"))
                    .reportPath = Server.MapPath("ProgressReports/Rpt/WSA/rptWSA_PROGRESSREPORT_01_06_Term2.rpt")
                Case "PROGRESS_REPORTS_2014_TERM2"
                    param.Add("@bSLT", "0")
                    If GRD_ID = "KG1" Or GRD_ID = "KG2" Then
                        .reportPath = Server.MapPath("ProgressReports/Rpt/WSA/rptWSA_INTERIMREPORT_APP_FS1_FS2_Term2.rpt")
                    Else
                        .reportPath = Server.MapPath("ProgressReports/Rpt/WSA/rptWSA_INTERIMREPORT_APP_01-06_Term2.rpt")
                    End If
                Case "PROGRESS_REPORTS_2014_TERM3"
                    param.Add("@bSLT", "0")
                    If GRD_ID = "KG1" Or GRD_ID = "KG2" Then
                        .reportPath = Server.MapPath("ProgressReports/Rpt/WSA/rptWSA_INTERIMREPORT_APP_FS1_FS2_Term3.rpt")
                    Else
                        .reportPath = Server.MapPath("ProgressReports/Rpt/WSA/rptWSA_INTERIMREPORT_APP_01-06_Term3.rpt")
                    End If
            End Select
        End With
        Session("rptClass") = rptClass
        If Session("downloadreport") = "1" Then
            DownloadReports(rptClass)
        Else
            LoadReports(rptClass)
        End If
    End Sub

    Sub GenerateGISReports(ByVal RSM_ID As String, ByVal RPF_ID As String, ByVal ACD_ID As String, ByVal STU_ID As String, ByVal BSU_ID As String, ByVal GRD_ID As String)
        Dim param As New Hashtable
        param.Add("@ACD_ID", ACD_ID)
        param.Add("@BSU_ID", BSU_ID)
        param.Add("@RSM_ID", RSM_ID)
        param.Add("@RPF_ID", RPF_ID)
        param.Add("@STU_ID", STU_ID)
        param.Add("RPF_DESCR", Session("RPF_DESCR"))
        param.Add("RPF_DATE", Now.ToShortDateString)

        'Session("studentphotopath") = TransferPath(h_STU_IDs.Value.Replace("___", "|"))
        'param.Add("UserName", Session("sUsr_name"))
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            .Photos = GetPhotoClass(STU_ID)
            Select Case hfReportFormat.Value
                Case "GIS_6_8_REPORT"
                    .reportPath = Server.MapPath("ProgressReports/Rpt/GIS/rptGIS_ProgressReport_6_8.rpt")
                Case Else
                    .reportPath = Server.MapPath("ProgressReports/Rpt/GIS/rptGIS_ElementarySchool.rpt")
            End Select
            '.reportPath = Server.MapPath("ProgressReports/Rpt/GIS/rptGIS_ElementarySchool.rpt")
        End With
        Session("rptClass") = rptClass
        If Session("downloadreport") = "1" Then
            DownloadReports(rptClass)
        Else
            LoadReports(rptClass)
        End If
    End Sub


    Private Sub GenerateCIAReports(ByVal RSM_ID As String, ByVal RPF_ID As String, ByVal ACD_ID As String, ByVal STU_ID As String, ByVal BSU_ID As String, ByVal GRD_ID As String)
        Dim param As New Hashtable
        param.Add("@ACD_ID", ACD_ID)
        param.Add("@BSU_ID", BSU_ID)
        param.Add("@RSM_ID", RSM_ID)
        param.Add("@RPF_ID", RPF_ID)
        param.Add("@STU_ID", STU_ID)
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            ' .Photos = GetPhotoClass()
            Select Case hfReportFormat.Value
                Case "PROGRESS_REPORT"
                    param.Add("accYear", Session("ReportYear_DESC"))
                    Select Case GRD_ID
                        Case "01", "02", "03", "04"
                            .reportPath = Server.MapPath("ProgressReports/Rpt/CIA/rptCIA_StudentProgressReport_01_04.rpt")
                        Case "KG1", "KG2"
                            .reportPath = Server.MapPath("ProgressReports/Rpt/CIA/rptCIA_StudentProgressReport_FS1_FS2.rpt")
                        Case Else
                            .reportPath = Server.MapPath("ProgressReports/Rpt/CIA/rptCIA_StudentProgressReport_05_08.rpt")
                    End Select
                Case "FINAL_REPORT"
                    param.Add("accYear", Session("ReportYear_DESC"))
                    .reportPath = Server.MapPath("ProgressReports/Rpt/CIA/rptCIA_FinalReport_01_08.rpt")
            End Select
        End With
        Session("rptClass") = rptClass
        If Session("downloadreport") = "1" Then
            DownloadReports(rptClass)
        Else
            LoadReports(rptClass)
        End If
    End Sub
    Private Sub GenerateMTSReports(ByVal RSM_ID As String, ByVal RPF_ID As String, ByVal ACD_ID As String, ByVal STU_ID As String, ByVal BSU_ID As String, ByVal GRD_ID As String)
        Dim param As New Hashtable
        param.Add("@ACD_ID", ACD_ID)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RSM_ID", RSM_ID)
        param.Add("@RPF_ID", RPF_ID)
        param.Add("@STU_ID", STU_ID)
        param.Add("accYear", Session("ReportYear_DESC"))
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            ' .Photos = GetPhotoClass()
            Select Case hfReportFormat.Value
                Case "MTS_PROGRESS REPORT_01_05"
                    .reportPath = Server.MapPath("ProgressReports/Rpt/MTS/rptMTS_StudentProgressReport_01_05.rpt")
                Case "MTS_PROGRESS REPORT_06_08"

                    .reportPath = Server.MapPath("ProgressReports/Rpt/MTS/rptMTS_StudentProgressReport_06_08.rpt")
            End Select
        End With
        Session("rptClass") = rptClass
        If Session("downloadreport") = "1" Then
            DownloadReports(rptClass)
        Else
            LoadReports(rptClass)
        End If
    End Sub


    Private Sub GenerateNMSReports(ByVal RSM_ID As String, ByVal RPF_ID As String, ByVal ACD_ID As String, ByVal STU_ID As String, ByVal BSU_ID As String, ByVal GRD_ID As String)
        Dim param As New Hashtable
        param.Add("@ACD_ID", ACD_ID)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RSM_ID", RSM_ID)
        param.Add("@RPF_ID", RPF_ID)
        param.Add("@STU_ID", STU_ID)
        param.Add("@GRD_ID", GRD_ID)
        param.Add("accYear", Session("ReportYear_DESC"))

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            Select Case hfReportFormat.Value
                Case "NMS_TERMREPORT"
                    .reportPath = Server.MapPath("ProgressReports/Rpt/NMS/rptNMS_PROGRESSREPORT_01_06.rpt")
                Case "NMS_TERMREPORT_2014"
                    .reportPath = Server.MapPath("ProgressReports/Rpt/NMS/rptNMS_PROGRESSREPORT_01_06_2014.rpt")
                Case "NMS_STRATEGIESREPORT"
                    .reportPath = Server.MapPath("ProgressReports/Rpt/NMS/rptNMS_STRATEGIESREPORT_01_06.rpt")
            End Select
        End With
        Session("rptClass") = rptClass
        If Session("downloadreport") = "1" Then
            DownloadReports(rptClass)
        Else
            LoadReports(rptClass)
        End If
    End Sub


    Private Sub GenerateAKNSProgressReport(ByVal RSM_ID As String, ByVal RPF_ID As String, ByVal ACD_ID As String, ByVal STU_ID As String, ByVal BSU_ID As String, ByVal GRD_ID As String)
        Dim param As New Hashtable
        param.Add("@ACD_ID", ACD_ID)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RSM_ID", RSM_ID)
        param.Add("@STU_ID", STU_ID)
        param.Add("RPT_CAPTION", Session("RPF_DESCR"))
        'Session("studentphotopath") = TransferPath(h_STU_IDs.Value.Replace("___", "|"))
        'param.Add("UserName", Session("sUsr_name"))
        'param.Add("ATTEND_TOTAL", GetTotalGradeAttendance())
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            Select Case hfReportFormat.Value

                Case "AKN_QUARTER_REPORTS_01-10"
                    param.Add("@RPF_ID", RPF_ID)
                    If Session("RPF_DESCR").ToString.Contains("QUARTER 2") Then
                        .reportPath = Server.MapPath("ProgressReports/Rpt/AKNS/rptTERM_REPORT_AKNS_GRD01_10_QUARTER2.rpt")
                    Else
                        .reportPath = Server.MapPath("ProgressReports/Rpt/AKNS/rptTERM_REPORT_AKNS_GRD01_10_QUARTER.rpt")
                    End If
                Case "AKN_FINALREPORTS_2017-18"
                    param.Add("@RPF_ID", RPF_ID)
                    .reportPath = Server.MapPath("ProgressReports/Rpt/AKNS/rptFINAL_REPORT_AKNS_2017-18.rpt")

                Case "AKN_TERMREPORTS_2014-15"
                    param.Add("@RPF_ID", RPF_ID)
                    .reportPath = Server.MapPath("ProgressReports/Rpt/AKNS/rptTERM_REPORT_AKNS_GRD01_12_2014-15.rpt")
                Case "AKN_FINALREPORTS_01-03"
                    param.Add("@RPF_ID", RPF_ID)
                    .reportPath = Server.MapPath("ProgressReports/Rpt/AKNS/rptFINAL_REPORT_AKNS_GRD01_03.rpt")
                Case "AKN_FINALREPORTS_04-10"
                    param.Add("@RPF_ID", RPF_ID)
                    .reportPath = Server.MapPath("ProgressReports/Rpt/AKNS/rptFINAL_REPORT_AKNS_GRD04_10.rpt")
                Case "AKN_FINALREPORTS_11-12"
                    param.Add("@RPF_ID", RPF_ID)
                    .reportPath = Server.MapPath("ProgressReports/Rpt/AKNS/rptFINAL_REPORT_AKNS_GRD11_12.rpt")

                Case "AKN_SEMREPORTS_01-03_SEM1"
                    param.Add("@RPF_ID", RPF_ID)
                    If Session("RPF_DESCR").ToString.Contains("SEMESTER 1") Then
                        .reportPath = Server.MapPath("ProgressReports/Rpt/AKNS/rptTERM_REPORT_AKNS_GRD01_03_SEM1.rpt")
                    Else
                        .reportPath = Server.MapPath("ProgressReports/Rpt/AKNS/rptTERM_REPORT_AKNS_GRD01_03_SEM2.rpt")
                    End If

                Case "AKN_SEMREPORTS_04-10_SEM1"
                    param.Add("@RPF_ID", RPF_ID)
                    If Session("RPF_DESCR").ToString.Contains("SEMESTER 1") Then
                        .reportPath = Server.MapPath("ProgressReports/Rpt/AKNS/rptTERM_REPORT_AKNS_GRD04_10_SEM1.rpt")
                    Else
                        .reportPath = Server.MapPath("ProgressReports/Rpt/AKNS/rptTERM_REPORT_AKNS_GRD04_10_SEM2.rpt")
                    End If

                Case "AKN_SEMREPORTS_11-12_SEM1"
                    param.Add("@RPF_ID", RPF_ID)
                    If Session("RPF_DESCR").ToString.Contains("SEMESTER 1") Then
                        .reportPath = Server.MapPath("ProgressReports/Rpt/AKNS/rptTERM_REPORT_AKNS_GRD11_12_SEM1.rpt")
                    Else
                        .reportPath = Server.MapPath("ProgressReports/Rpt/AKNS/rptTERM_REPORT_AKNS_GRD11_12_SEM2.rpt")
                    End If

                Case "AKN_TERMREPORTS_2014-15_TERM2"
                    param.Add("@RPF_ID", RPF_ID)
                    .reportPath = Server.MapPath("ProgressReports/Rpt/AKNS/rptTERM_REPORT_AKNS_GRD01_12_2014-15_TERM2.rpt")
                Case "AKN_TERMREPORTS_2014-15_TERM3"
                    param.Add("@RPF_ID", RPF_ID)
                    .reportPath = Server.MapPath("ProgressReports/Rpt/AKNS/rptTERM_REPORT_AKNS_GRD01_12_2014-15_TERM3.rpt")
                Case "AKN_REPORTS_2014-15_FINAL"
                    param.Add("@RPF_ID", RPF_ID)
                    .reportPath = Server.MapPath("ProgressReports/Rpt/AKNS/rptTERM_REPORT_AKNS_GRD01_12_2014-15_FINALREPORT.rpt")
                Case "AKN_REPORTS_2015-16_FINAL"
                    param.Add("@RPF_ID", RPF_ID)
                    .reportPath = Server.MapPath("ProgressReports/Rpt/AKNS/rptTERM_REPORT_AKNS_GRD01_12_2015-16_FINALREPORT.rpt")
                Case "AKN_MIDTERREPORTS"
                    param.Add("@RPF_ID", RPF_ID)
                    Select Case GRD_ID
                        Case "KG1", "KG2"
                            .Photos = GetPhotoClass(Session("STU_ID"))
                            param.Add("GRD_ID", GRD_ID)
                            .reportPath = Server.MapPath("ProgressReports/Rpt/AKNS/rptAKN_KINDERGARTENPROGRESSREPORT.rpt")
                        Case "01", "02"
                            .reportPath = Server.MapPath("ProgressReports/Rpt/AKNS/rptAKN_MidtermReport01_02.rpt")
                        Case "03", "04", "05", "06", "07", "08", "09"
                            .reportPath = Server.MapPath("ProgressReports/Rpt/AKNS/rptAKN_MidtermReport03_09.rpt")
                        Case "10"
                            .reportPath = Server.MapPath("ProgressReports/Rpt/AKNS/rptAKN_MidtermReport_10.rpt")
                        Case "11", "12"
                            .reportPath = Server.MapPath("ProgressReports/Rpt/AKNS/rptAKN_MidtermReport11_12.rpt")
                    End Select
                Case "AKN_TERMREPORTS_2014"
                    param.Add("@RPF_ID", RPF_ID)
                    Select Case GRD_ID
                        Case "09", "10", "11", "12"
                            .reportPath = Server.MapPath("ProgressReports/Rpt/AKNS/rptTERM_REPORT_AKNS_GRD9_12_2013-14.rpt")
                        Case Else
                            .reportPath = Server.MapPath("ProgressReports/Rpt/AKNS/rptTERM_REPORT_AKNS_GRD01_8_2013-14.rpt")
                    End Select
                Case "AKN_FINALREPORTS_2014"

                    param.Add("@RPF_ID", RPF_ID)
                    Select Case GRD_ID
                        Case "09", "10", "11", "12"
                            .reportPath = Server.MapPath("ProgressReports/Rpt/AKNS/rptFINALREPORT_AKNS_GRD9_12_2013-14.rpt")
                        Case Else
                            .reportPath = Server.MapPath("ProgressReports/Rpt/AKNS/rptFINALREPORT_AKNS_GRD01_8_2013-14.rpt")
                    End Select
                Case "AKN_CRITERIAREPORTS"
                    param.Add("@RPF_ID", RPF_ID)
                    .Photos = GetPhotoClass(Session("STU_ID"))
                    param.Add("GRD_ID", GRD_ID)
                    .reportPath = Server.MapPath("ProgressReports/Rpt/AKNS/rptAKN_KINDERGARTENPROGRESSREPORT.rpt")
                Case "AKN_CRITERIAREPORTS_2016"
                    param.Add("@RPF_ID", RPF_ID)
                    .Photos = GetPhotoClass(Session("STU_ID"))
                    param.Add("GRD_ID", GRD_ID)
                    .reportPath = Server.MapPath("ProgressReports/Rpt/AKNS/rptAKN_KINDERGARTENPROGRESSREPORT_2016-17.rpt")
                Case "AKN_TERMREPORTS"
                    param.Add("@RPF_ID", RPF_ID)
                    If Session("RPF_DESCR") = "TERM 2 REPORT" Then
                        .reportPath = Server.MapPath("ProgressReports/Rpt/AKNS/rptTERM_REPORT_AKNS(2012-13)_GRD01_12_TERM2.rpt")
                    Else
                        .reportPath = Server.MapPath("ProgressReports/Rpt/AKNS/rptTERM_REPORT_AKNS(2012-13)_GRD01_12.rpt")
                    End If
                Case "AKN_FINALREPORTS"
                    param.Add("@RPF_ID", RPF_ID)
                    If Session("RPF_DESCR") = "FINAL REPORT" Then
                        .reportPath = Server.MapPath("ProgressReports/Rpt/AKNS/rptFINAL_REPORT_AKNS_ALLTERMS.rpt")
                    Else
                        param.Add("@GRD_ID", GRD_ID)
                        .reportPath = Server.MapPath("ProgressReports/Rpt/AKNS/rptFINAL_REPORT_AKNS_MINISTRY.rpt")
                    End If

            End Select
        End With
        Session("rptClass") = rptClass
        If Session("downloadreport") = "1" Then
            DownloadReports(rptClass)
        Else
            LoadReports(rptClass)
        End If
    End Sub
    Private Sub GenerateGIPReports(ByVal RSM_ID As String, ByVal RPF_ID As String, ByVal ACD_ID As String, ByVal STU_ID As String, ByVal BSU_ID As String, ByVal GRD_ID As String)
        Dim param As New Hashtable


        param.Add("@ACD_ID", ACD_ID)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RPF_ID", RPF_ID)
        param.Add("@STU_ID", STU_ID)
        param.Add("@RSM_ID", RSM_ID)


        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            .Photos = GetPhotoClass(STU_ID)
            Select Case hfReportFormat.Value
                Case "GIP_TERMREPORT_1_6"
                    param.Add("GRD_ID", GRD_ID)
                    param.Add("accYear", Session("ReportYear_DESC"))
                    .reportPath = Server.MapPath("ProgressReports/Rpt/GIP/rptGIP_ProgressReport_1_6.rpt")
                Case "GIP_TERMREPORT_7_10"
                    .reportPath = Server.MapPath("ProgressReports/Rpt/GIP/rptGIP_ProgressReport_7_10.rpt")
            End Select
        End With

        Session("rptClass") = rptClass
        If Session("downloadreport") = "1" Then
            DownloadReports(rptClass)
        Else
            LoadReports(rptClass)
        End If
    End Sub

    Private Sub GenerateOOLReports(ByVal RSM_ID As String, ByVal RPF_ID As String, ByVal ACD_ID As String, ByVal STU_ID As String, ByVal BSU_ID As String, ByVal GRD_ID As String)
        Dim param As New Hashtable
        param.Add("@ACD_ID", ACD_ID)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RPF_ID", RPF_ID)
        param.Add("@STU_ID", STU_ID)
        param.Add("@RSM_ID", RSM_ID)
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"

            ' .Photos = GetPhotoClass()
            Select Case hfReportFormat.Value
                Case "OOL_IGCSE_PROGRESSREPORTS"
                    param.Add("GRD_ID", GRD_ID)
                    param.Add("RPF_DESCR", Session("RPF_DESCR"))
                    param.Add("@IMG_BSU_ID", Session("SBSUID"))
                    param.Add("@IMG_TYPE", "LOGO")
                    .reportPath = Server.MapPath("ProgressReports/Rpt/rptOOL_ProgressReport_2015-16.rpt")
                Case "OOL_IGCSE_FINALREPORT"
                    param.Add("GRD_ID", GRD_ID)
                    param.Add("RPF_DESCR", Session("RPF_DESCR"))
                    param.Add("@IMG_BSU_ID", Session("SBSUID"))
                    param.Add("@IMG_TYPE", "LOGO")
                    .reportPath = Server.MapPath("ProgressReports/Rpt/rptOOL_FinalReport_2015-16.rpt")

            End Select
            .reportParameters = param
            '.reportPath = Server.MapPath("../Rpt/rptWIN_PROGRESSREPORT_07_10_online.rpt")
        End With
        Session("rptClass") = rptClass
        If Session("downloadreport") = "1" Then
            DownloadReports(rptClass)
        Else
            LoadReports(rptClass)
        End If
    End Sub
    Private Sub GenerateCHSReports(ByVal RSM_ID As String, ByVal RPF_ID As String, ByVal ACD_ID As String, ByVal STU_ID As String, ByVal BSU_ID As String, ByVal GRD_ID As String)
        Dim param As New Hashtable
        param.Add("@ACD_ID", ACD_ID)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RPF_ID", RPF_ID)
        param.Add("@STU_ID", STU_ID)
        param.Add("@RSM_ID", RSM_ID)
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"

            ' .Photos = GetPhotoClass()
            Select Case hfReportFormat.Value
                Case "CHS_MIDTERM_KG_2015"
                    param.Add("GRD_ID", GRD_ID)
                    param.Add("RPF_DESCR", Session("RPF_DESCR"))
                    .reportPath = Server.MapPath("ProgressReports/Rpt/CHS/rptCHS_KINDERGARTENPROGRESSREPORT_2015-16.rpt")
                Case "CHS_MIDTERM_KG_2016"
                    param.Add("GRD_ID", GRD_ID)
                    param.Add("RPF_DESCR", Session("RPF_DESCR"))
                    .reportPath = Server.MapPath("ProgressReports/Rpt/CHS/rptCHS_KINDERGARTENPROGRESSREPORT_2016-17.rpt")
                Case "CHS_MIDTERM_2015"
                    param.Add("grd_id", GRD_ID)
                    Select Case GRD_ID
                        Case "11", "12", "13"
                            .reportPath = Server.MapPath("ProgressReports/Rpt/CHS/rptCHS_MidYearReport_11-13_2015-16.rpt")
                        Case Else
                            param.Add("@IMG_BSU_ID", Session("SBSUID"))
                            param.Add("@IMG_TYPE", "LOGO")
                            .reportPath = Server.MapPath("ProgressReports/Rpt/CHS/rptCHS_MidYearReport_2015-16.rpt")
                    End Select
                Case "CHS_MIDTERM_2016"
                    param.Add("grd_id", GRD_ID)

                    param.Add("@IMG_BSU_ID", Session("SBSUID"))
                    param.Add("@IMG_TYPE", "LOGO")
                    .reportPath = Server.MapPath("ProgressReports/Rpt/CHS/rptCHS_MidYearReport_2016-17.rpt")
                Case Else
                    Select Case hfReportType.Value

                        Case "CHS_REPORTS"
                            Select Case GRD_ID
                                Case "11", "12", "13"
                                    param.Add("grd_id", GRD_ID)
                                    If Session("RPF_DESCR").ToString.Contains("MID YEAR REPORT") Then
                                        If Session("ReportYear_DESC") = "2015-2016" Then
                                            .reportPath = Server.MapPath("ProgressReports/Rpt/rptCHS_MidYearReport_11-13_2015-16.rpt")
                                        Else
                                            .reportPath = Server.MapPath("ProgressReports/Rpt/rptCHS_MidYearReport_11-13.rpt")
                                        End If

                                    Else
                                        .reportPath = Server.MapPath("ProgressReports/Rpt/rptCHS_FinalReport_11-13.rpt")
                                    End If
                                Case "KG1", "KG2"
                                    param.Add("GRD_ID", GRD_ID)
                                    param.Add("RPF_DESCR", Session("RPF_DESCR"))
                                    If Session("ReportYear_DESC") = "2015-2016" Then
                                        .reportPath = Server.MapPath("ProgressReports/Rpt/rptCHS_KINDERGARTENPROGRESSREPORT_2015-16.rpt")
                                    Else
                                        .reportPath = Server.MapPath("ProgressReports/Rpt/rptCHS_KINDERGARTENPROGRESSREPORT.rpt")
                                    End If
                                Case Else
                                    param.Add("grd_id", GRD_ID)
                                    param.Add("@IMG_BSU_ID", Session("SBSUID"))
                                    param.Add("@IMG_TYPE", "LOGO")
                                    If Session("RPF_DESCR").ToString.Contains("MID YEAR REPORT") Then
                                        If Session("ReportYear_DESC") = "2015-2016" Then
                                            .reportPath = Server.MapPath("ProgressReports/Rpt/rptCHS_MidYearReport_2015-16.rpt")
                                        Else
                                            .reportPath = Server.MapPath("ProgressReports/Rpt/rptCHS_MidYearReport.rpt")
                                        End If

                                    Else
                                        If Session("ReportYear_DESC") = "2011-2012" Then
                                            .reportPath = Server.MapPath("ProgressReports/Rpt/rptCHS_FinalReport1-10_2011-12.rpt")
                                        Else
                                            .reportPath = Server.MapPath("ProgressReports/Rpt/rptCHS_FinalReport1-10.rpt")
                                        End If

                                    End If
                            End Select
                        Case Else
                            Select Case GRD_ID
                                Case "11", "12", "13"
                                    param.Add("grd_id", GRD_ID)
                                    If Session("RPF_DESCR").ToString.Contains("MID YEAR REPORT") Then
                                        .reportPath = Server.MapPath("ProgressReports/Rpt/rptCHS_MidYearReport_11-13_2013-14.rpt")
                                    Else
                                        .reportPath = Server.MapPath("ProgressReports/Rpt/rptCHS_FinalReport_11-13_2013-14.rpt")
                                    End If
                                Case "KG1", "KG2"
                                    param.Add("GRD_ID", GRD_ID)
                                    param.Add("RPF_DESCR", Session("RPF_DESCR"))
                                    .reportPath = Server.MapPath("ProgressReports/Rpt/rptCHS_KINDERGARTENPROGRESSREPORT_2013-14.rpt")

                                Case Else
                                    param.Add("grd_id", GRD_ID)
                                    param.Add("@IMG_BSU_ID", Session("SBSUID"))
                                    param.Add("@IMG_TYPE", "LOGO")
                                    If Session("RPF_DESCR").ToString.Contains("MID YEAR REPORT") Then
                                        .reportPath = Server.MapPath("ProgressReports/Rpt/rptCHS_MidYearReport.rpt")
                                    Else
                                        If Session("ReportYear_DESC") = "2011-2012" Then
                                            .reportPath = Server.MapPath("ProgressReports/Rpt/rptCHS_FinalReport1-10_2011-12.rpt")
                                        Else
                                            .reportPath = Server.MapPath("ProgressReports/Rpt/rptCHS_FinalReport1-10_2013-14.rpt")
                                        End If

                                    End If
                            End Select
                    End Select
            End Select

            .reportParameters = param
            '.reportPath = Server.MapPath("../Rpt/rptWIN_PROGRESSREPORT_07_10_online.rpt")
        End With
        Session("rptClass") = rptClass
        If Session("downloadreport") = "1" Then
            DownloadReports(rptClass)
        Else
            LoadReports(rptClass)
        End If
    End Sub




    Private Sub GenerateTermTestReport(ByVal RSM_ID As String, ByVal RPF_ID As String, ByVal ACD_ID As String, ByVal STU_ID As String, ByVal BSU_ID As String, ByVal GRD_ID As String)
        Dim param As New Hashtable
        param.Add("@ACD_ID", ACD_ID)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RSM_ID", RSM_ID)
        param.Add("@RPF_ID", RPF_ID)
        param.Add("@STU_ID", STU_ID)
        param.Add("RPT_CAPTION", Session("RPF_DESCR"))
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            If (Session("sBSUID") = "121014" And ACD_ID = 611) Or Session("sBSUID") = "151001" Then
                param.Add("RPT_ASSESSMENT_HEADER1", "Test 1")
                param.Add("RPT_ASSESSMENT_HEADER2", "Test 2")
                .reportPath = Server.MapPath("ProgressReports/Rpt/rptCBSE_OOW_UNIT_TEST_REPORT_11_12.rpt")
            ElseIf ACD_ID = "605" And Session("SBSUID") = "131001" Then
                If Session("RPF_DESCR").ToUpper = "FIRST TERM TEST" Then
                    param.Add("RPT_ASSESSMENT_HEADER1", "Test 1(Apr) out of 10")
                    param.Add("RPT_ASSESSMENT_HEADER2", "Test 2(May) out of 10")
                    param.Add("RPT_ASSESSMENT_HEADER3", "Test 3(June) out of 20")
                    .reportPath = Server.MapPath("ProgressReports/Rpt/rptCBSE_OOS_UNIT_TEST_REPORT_11_12.rpt")
                Else
                    param.Add("c:RPT_ASSESSMENT_HEADER1", "Test 1(Nov) out of 10")
                    param.Add("RPT_ASSESSMENT_HEADER2", "Test 2(Dec) out of 10")
                    param.Add("RPT_ASSESSMENT_HEADER3", "Test 3(Jan) out of 20")
                    param.Add("RPT_ASSESSMENT_HEADER4", "Test 3(Feb) out of 10")
                    .reportPath = Server.MapPath("ProgressReports/Rpt/rptCBSE_OOS_UNIT_TEST_REPORT_11_12_term2.rpt")
                End If
            Else
                param.Add("RPT_ASSESSMENT_HEADER", "Marks out of 25")
                .reportPath = Server.MapPath("ProgressReports/Rpt/rptCBSE_UNIT_TEST_REPORT_11_12.rpt")
            End If
        End With
        If Session("downloadreport") = "1" Then
            DownloadReports(rptClass)
        Else
            LoadReports(rptClass)
        End If
    End Sub

    Private Sub GenerateCBSETermReports11_12(ByVal RSM_ID As String, ByVal RPF_ID As String, ByVal ACD_ID As String, ByVal STU_ID As String, ByVal BSU_ID As String, ByVal GRD_ID As String)
        Dim param As New Hashtable
        param.Add("@ACD_ID", ACD_ID)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RSM_ID", RSM_ID)
        param.Add("@RPF_ID", RPF_ID)
        param.Add("@STU_ID", STU_ID)
        param.Add("RPT_CAPTION", Session("RPF_DESCR"))
        Select Case Session("RPF_DESCR")
            Case "FORMATIVE ASSESSMENT 1"
                param.Add("RPT_ASSESSMENT_HEADER", "ASSESSMENT 1")
            Case "FORMATIVE ASSESSMENT 2"
                param.Add("RPT_ASSESSMENT_HEADER", "ASSESSMENT 1")
        End Select

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            Select Case GetReportType(RSM_ID)
                Case "GRADE_11_12_FORMAT1"
                    param.Add("ATT_ASON_DATE", GetAsOnDateForAttendance(ACD_ID, GRD_ID, RPF_ID))
                    param.Add("UNIT_TEST_HEADING", "Unit Test")
                    .reportPath = Server.MapPath("ProgressReports/Rpt/rptTERM_REPORT_CBSE_GRD11_12_FORMAT1.rpt")
                Case "GRADE_11_12_FORMAT2"
                    param.Add("ATT_ASON_DATE", GetAsOnDateForAttendance(ACD_ID, GRD_ID, RPF_ID))
                    param.Add("UNIT_TEST_HEADING", "Unit Test")
                    .reportPath = Server.MapPath("ProgressReports/Rpt/rptTERM_REPORT_CBSE_GRD11_12_FORMAT2.rpt")
                Case "GRADE_11_12_FORMAT3"
                    param.Add("ATT_ASON_DATE", GetAsOnDateForAttendance(ACD_ID, GRD_ID, RPF_ID))
                    param.Add("UNIT_TEST_HEADING", "Unit Test")
                    .reportPath = Server.MapPath("ProgressReports/Rpt/rptTERM_REPORT_CBSE_GRD11_12_FORMAT3.rpt")
                Case "GRADE_11_12_FORMAT4"
                    param.Add("ATT_ASON_DATE", GetAsOnDateForAttendance(ACD_ID, GRD_ID, RPF_ID))
                    param.Add("UNIT_TEST_HEADING", "Unit Test")
                    .reportPath = Server.MapPath("ProgressReports/Rpt/rptTERM_REPORT_CBSE_GRD11_12_FORMAT4.rpt")
                Case "GRADE_11_12_FORMAT5"
                    param.Add("ATT_ASON_DATE", GetAsOnDateForAttendance(ACD_ID, GRD_ID, RPF_ID))
                    param.Add("UNIT_TEST_HEADING", "Unit Test")
                    .reportPath = Server.MapPath("ProgressReports/Rpt/rptTERM_REPORT_CBSE_GRD11_12_FORMAT5.rpt")
                Case "GRADE_11_12_FORMAT6"
                    param.Add("ATT_ASON_DATE", GetAsOnDateForAttendance(ACD_ID, GRD_ID, RPF_ID))
                    param.Add("UNIT_TEST_HEADING", "Unit Test")
                    .reportPath = Server.MapPath("ProgressReports/Rpt/rptTERM_REPORT_CBSE_GRD11_12_FORMAT6.rpt")
            End Select
        End With
        If Session("downloadreport") = "1" Then
            DownloadReports(rptClass)
        Else
            LoadReports(rptClass)
        End If
    End Sub
    Private Sub GenerateBENCHMARKReports(ByVal RSM_ID As String, ByVal RPF_ID As String, ByVal ACD_ID As String, ByVal STU_ID As String, ByVal BSU_ID As String, ByVal GRD_ID As String)
        Dim param As New Hashtable
        param.Add("@ACD_ID", ACD_ID)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RSM_ID", RSM_ID)
        param.Add("@RPF_ID", RPF_ID)
        param.Add("@STU_ID", STU_ID)

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            .reportPath = Server.MapPath("ProgressReports/Rpt/DMHS/rptDMHS_BENCHMARKREPORT.rpt")

        End With
        Session("rptClass") = rptClass
        If Session("downloadreport") = "1" Then
            DownloadReports(rptClass)
        Else
            LoadReports(rptClass)
        End If
    End Sub
    Private Sub GenerateCBSEKinderGarden(ByVal strRSM_ID As String, ByVal strRPF_ID As String, ByVal strACD_ID As String, ByVal strSTU_ID As String, ByVal strBSU_ID As String, ByVal strGRD_ID As String)
        Dim param As New Hashtable
        If hfReportFormat.Value.ToUpper <> "NMS_KG_REPORT" Then
            param.Add("@IMG_BSU_ID", Session("sbsuid"))
            param.Add("@IMG_TYPE", "LOGO")
            param.Add("accYear", Session("ReportYear_DESC"))
        Else
            param.Add("@GRD_ID", strGRD_ID)
        End If
        param.Add("@ACD_ID", strACD_ID)
        param.Add("@BSU_ID", strBSU_ID)
        param.Add("@RSM_ID", strRSM_ID)
        param.Add("@RPF_ID", strRPF_ID)
        param.Add("@STU_ID", strSTU_ID)


        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            .Photos = GetPhotoClass(strSTU_ID)
            Select Case hfReportFormat.Value.ToUpper
                Case "OIS_KG_REPORT"
                    ' If strGRD_ID = "KG1" Then
                    .reportPath = Server.MapPath("ProgressReports/Rpt/CBSE/rptCBSE_KinderGarten_OIS.rpt")
                    'Else
                    '    .reportPath = Server.MapPath("ProgressReports/Rpt/CBSE/rptCBSE_KinderGarten_OIS_KG2.rpt")
                    'End If
                Case "OOS_KG_REPORT"
                    .reportPath = Server.MapPath("ProgressReports/Rpt/CBSE/rptCBSE_KinderGarten_OOS.rpt")
                Case "OOS_KG_REPORT_NEW"
                    .reportPath = Server.MapPath("ProgressReports/Rpt/CBSE/rptCBSE_KinderGarten_OOS_2018-19.rpt")
                Case "KGS_KG_REPORT"
                    .reportPath = Server.MapPath("ProgressReports/Rpt/CBSE/rptCBSE_KinderGarten_KGS.rpt")
                Case "OOD_KG_REPORT"
                    Select Case Session("RPF_DESCR").ToUpper
                        Case "ON ENTRY APRIL", "END OF APRIL", "END OF MAY", "END OF JUNE", "ON ENTRY SEPTEMBER", "END OF SEPTEMBER", "END OF OCTOBER"
                            .reportPath = Server.MapPath("ProgressReports/Rpt/cbse/rptCBSE_KinderGarten_Term1_OOD.rpt")
                        Case "END OF NOVEMBER", "END OF DECEMBER", "END OF JANUARY", "END OF FEBRUARY"
                            .reportPath = Server.MapPath("ProgressReports/Rpt/rptCBSE_KinderGarten_Term2.rpt")
                        Case Else
                            .reportPath = Server.MapPath("ProgressReports/Rpt/rptCBSE_KinderGarten.rpt")
                    End Select
                Case "NMS_KG_REPORT"
                    .reportPath = Server.MapPath("ProgressReports/Rpt/NMS/rptNMS_PROGRESSREPORT_KG1_KG2.rpt")
                Case Else
                    If strACD_ID = "785" Or Session("clm") = 18 Then
                        Select Case Session("RPF_DESCR").ToUpper
                            Case "ON ENTRY SEPTEMBER", "END OF SEPTEMBER", "END OF OCTOBER", "END OF OCTOBER", "END OF NOVEMBER", "END OF DECEMBER", "END OF JANUARY"
                                If Session("STP_GRD_ID") = "KG1" Then
                                    .reportPath = Server.MapPath("ProgressReports/Rpt/rptIGCSE_KG1_Term1.rpt")
                                Else
                                    .reportPath = Server.MapPath("ProgressReports/Rpt/rptIGCSE_KG2_Term1.rpt")
                                End If
                            Case "END OF FEBRUARY", "END OF MARCH", "END OF APRIL", "END OF MAY", "END OF JUNE"
                                If strACD_ID = "836" Then
                                    .reportPath = Server.MapPath("ProgressReports/Rpt/rptIGCSE_KG1_KG2_Term2_2012-13.rpt")
                                Else
                                    .reportPath = Server.MapPath("ProgressReports/Rpt/rptIGCSE_KG1_KG2_Term2.rpt")
                                End If
                            Case Else
                                .reportPath = Server.MapPath("ProgressReports/Rpt/rptCBSE_KinderGarten.rpt")
                        End Select
                    Else
                        Select Case Session("RPF_DESCR").ToUpper
                            Case "ON ENTRY APRIL", "END OF APRIL", "END OF MAY", "END OF JUNE", "ON ENTRY SEPTEMBER", "END OF SEPTEMBER", "END OF OCTOBER"
                                .reportPath = Server.MapPath("ProgressReports/Rpt/rptCBSE_KinderGarten_Term1.rpt")
                            Case "END OF NOVEMBER", "END OF DECEMBER", "END OF JANUARY", "END OF FEBRUARY"
                                .reportPath = Server.MapPath("ProgressReports/Rpt/rptCBSE_KinderGarten_Term2.rpt")
                            Case Else
                                .reportPath = Server.MapPath("ProgressReports/Rpt/rptCBSE_KinderGarten.rpt")
                        End Select
                    End If
            End Select
            '.reportPath = Server.MapPath("ProgressReports/Rpt/rptCBSE_KinderGarten.rpt")
        End With
        Session("rptClass") = rptClass
        If Session("downloadreport") = "1" Then
            DownloadReports(rptClass)
        Else
            LoadReports(rptClass)
        End If
    End Sub

    Function GetPhotoClass(ByVal strSTU_ID As String) As OASISPhotos
        Dim vPhoto As New OASISPhotos
        vPhoto.BSU_ID = Session("sBSUID")
        vPhoto.PhotoType = OASISPhotoType.STUDENT_PHOTO

        ' Dim arrList As ArrayList

        'arrList = New ArrayList(strSTU_ID)

        'For Each vVal As Object In arrList
        '    If vVal.ToString <> "" Then
        vPhoto.IDs.Add(strSTU_ID)
        '    End If
        'Next
        Return vPhoto
    End Function

    Private Sub GenerateTermlyReport_DMHS_Dubai(ByVal RSM_ID As String, ByVal RPF_ID As String, ByVal ACD_ID As String, ByVal STU_ID As String, ByVal BSU_ID As String, ByVal GRD_ID As String)
        Dim param As New Hashtable
        param.Add("@ACD_ID", ACD_ID)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RSM_ID", RSM_ID)
        param.Add("@RPF_ID", RPF_ID)
        param.Add("@STU_ID", STU_ID)
        param.Add("RPT_CAPTION", Session("RPF_DESCR"))
        '    param.Add("ATT_ASON_DATE", GetAsOnDateForAttendance())

        'param.Add("TOT_ATTEND_VAL", "75")
        'Session("studentphotopath") = TransferPath(h_STU_IDs.Value.Replace("___", "|"))
        'param.Add("UserName", Session("sUsr_name"))
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            '.Photos = GetPhotoClass()
            Select Case hfReportFormat.Value.ToUpper
                Case "TERM_REPORTS_11_12_NEW"
                    Select Case GRD_ID
                        Case "12"
                            If Session("RPF_DESCR").ToString.Contains("TERM 3") Then
                                param("RPT_CAPTION") = "PRILIMINARY REPORT"
                                .reportPath = Server.MapPath("ProgressReports/Rpt/DMHS/rptPRILIMINARY_REPORT_DMHS_DUBAI_GRD10_12.rpt")
                            Else
                                .reportPath = Server.MapPath("ProgressReports/Rpt/DMHS/rptTERM_REPORT_DMHS_DUBAI_GRD11_2015-16.rpt")
                            End If
                        Case Else
                            .reportPath = Server.MapPath("ProgressReports/Rpt/DMHS/rptTERM_REPORT_DMHS_DUBAI_GRD11_2015-16.rpt")
                    End Select
                Case "TERM_REPORTS_05_NEW"
                    .reportPath = Server.MapPath("ProgressReports/Rpt/DMHS/rptTERM_REPORT_DMHS_DUBAI_GRD05_08_2016-17.rpt")
                Case "TERM_REPORTS_11_NEW"
                    .reportPath = Server.MapPath("ProgressReports/Rpt/DMHS/rptTERM_REPORT_DMHS_DUBAI_GRD11_2016-17.rpt")

                Case Else

                    Select Case GRD_ID
                        Case "01", "02", "03", "04"

                            Select Case hfReportFormat.Value.ToUpper
                                Case "TERMREPORT_01_02_2016"
                                    .reportPath = Server.MapPath("ProgressReports/Rpt/rptTERM_REPORT_DMHS_DUBAI_GRD01_04 _2015-16.rpt")
                                Case Else
                                    .reportPath = Server.MapPath("ProgressReports/Rpt/rptTERM_REPORT_DMHS_DUBAI_GRD01_04.rpt")
                            End Select
                        Case "05", "06", "07"
                            If hfReportFormat.Value.ToUpper = "TERM_REPORTS_05_07_NEW_TERM2_2017" Then
                                .reportPath = Server.MapPath("ProgressReports/Rpt/DMHS/rptTERM_REPORT_DMHS_DUBAI_GRD05_07_2016-17_TERM2.rpt")
                            ElseIf ((ACD_ID = "1053") Or (ACD_ID = "1097") Or (ACD_ID = "1268")) And (Session("RPF_DESCR") = "TERM 1 REPORT" Or Session("RPF_DESCR") = "TERM 3 REPORT") Then
                                .reportPath = Server.MapPath("ProgressReports/Rpt/DMHS/rptTERM_REPORT_DMHS_DUBAI_GRD05_08_2015-16.rpt")
                            ElseIf ((ACD_ID = "1053") Or (ACD_ID = "1097")) And (Session("RPF_DESCR") = "TERM 2 REPORT") Then
                                .reportPath = Server.MapPath("ProgressReports/Rpt/DMHS/rptTERM_REPORT_DMHS_DUBAI_GRD05_07_2015-16_TERM2.rpt")
                            ElseIf ((ACD_ID = "1268") And (Session("RPF_DESCR") = "TERM 2 REPORT")) Then
                                .reportPath = Server.MapPath("ProgressReports/Rpt/DMHS/rptTERM_REPORT_DMHS_DUBAI_GRD05_08_2015-16.rpt")
                            Else

                                .reportPath = Server.MapPath("ProgressReports/Rpt/rptTERM_REPORT_DMHS_DUBAI_GRD05_08.rpt")
                            End If
                        Case "08"
                            If ((ACD_ID = "1053") Or (ACD_ID = "1097") Or (ACD_ID = "1268")) Then
                                .reportPath = Server.MapPath("ProgressReports/Rpt/DMHS/rptTERM_REPORT_DMHS_DUBAI_GRD05_08_2015-16.rpt")
                            Else
                                .reportPath = Server.MapPath("ProgressReports/Rpt/rptTERM_REPORT_DMHS_DUBAI_GRD05_08.rpt")
                            End If

                        Case "09", "11"

                            If ((ACD_ID = "1053") Or (ACD_ID = "1097") Or (ACD_ID = "1268")) Then
                                If GRD_ID = "11" Then
                                    .reportPath = Server.MapPath("ProgressReports/Rpt/DMHS/rptTERM_REPORT_DMHS_DUBAI_GRD11_2015-16.rpt")
                                ElseIf GRD_ID = "09" Then
                                    .reportPath = Server.MapPath("ProgressReports/Rpt/DMHS/rptTERM_REPORT_DMHS_DUBAI_GRD09_12_2015-16.rpt")
                                End If

                            Else
                                .reportPath = Server.MapPath("ProgressReports/Rpt/rptTERM_REPORT_DMHS_DUBAI_GRD09_12.rpt")
                            End If

                        Case "10", "12"
                            If Session("RPF_DESCR").ToString.Contains("TERM 3") Then
                                param("RPT_CAPTION") = "PRILIMINARY REPORT"
                                .reportPath = Server.MapPath("ProgressReports/Rpt/rptPRILIMINARY_REPORT_DMHS_DUBAI_GRD10_12.rpt")
                            Else
                                If ((ACD_ID = "1053") Or (ACD_ID = "1097") Or (ACD_ID = "1268")) And GRD_ID = "10" Then
                                    .reportPath = Server.MapPath("ProgressReports/Rpt/DMHS/rptTERM_REPORT_DMHS_DUBAI_GRD09_12_2015-16.rpt")
                                ElseIf hfReportFormat.Value.ToUpper = "TERM_REPORTS_11_12_NEW" Then
                                    .reportPath = Server.MapPath("ProgressReports/Rpt/DMHS/rptTERM_REPORT_DMHS_DUBAI_GRD11_2015-16.rpt")
                                ElseIf hfReportFormat.Value.ToUpper = "TERM_REPORTS_05_07_NEW_TERM2_2017" Then
                                    .reportPath = Server.MapPath("ProgressReports/Rpt/DMHS/rptTERM_REPORT_DMHS_DUBAI_GRD05_07_2016-17_TERM2.rpt")
                                Else
                                    .reportPath = Server.MapPath("ProgressReports/Rpt/rptTERM_REPORT_DMHS_DUBAI_GRD09_12.rpt")
                                End If

                            End If
                    End Select
            End Select
        End With

        If Session("downloadreport") = "1" Then
            DownloadReports(rptClass)
        Else
            LoadReports(rptClass)
        End If
    End Sub


    Private Sub GenerateFinalReport_DMHS_Dubai(ByVal RSM_ID As String, ByVal RPF_ID As String, ByVal ACD_ID As String, ByVal STU_ID As String, ByVal BSU_ID As String, ByVal GRD_ID As String)
        Dim param As New Hashtable
        param.Add("@ACD_ID", ACD_ID)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RSM_ID", RSM_ID)
        param.Add("@RPF_ID", RPF_ID)
        param.Add("@STU_ID", STU_ID)
        param.Add("RPT_CAPTION", Session("RPF_DESCR"))

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            '.Photos = GetPhotoClass()

            If ACD_ID = 1053 Then

                Select Case GRD_ID
                    Case "01", "02", "03", "04"
                        .reportPath = Server.MapPath("ProgressReports/Rpt/rptFINAL_REPORT_DMHS_DUBAI_GRD01_04.rpt")
                    Case "05", "06", "07"
                        .reportPath = Server.MapPath("ProgressReports/Rpt/rptFINAL_REPORT_DMHS_DUBAI_GRD05_07_2015-16.rpt")
                    Case "08"
                        .reportPath = Server.MapPath("ProgressReports/Rpt/rptFINAL_REPORT_DMHS_DUBAI_GRD08_2015-16.rpt")
                    Case "09", "11"
                        .reportPath = Server.MapPath("ProgressReports/Rpt/rptFINAL_REPORT_DMHS_DUBAI_GRD09_11_2015-16.rpt")
                    Case "10"

                        .reportPath = Server.MapPath("ProgressReports/Rpt/rptFINAL_REPORT_DMHS_DUBAI_GRD10_2015-16.rpt")

                    Case "12"
                        .reportPath = Server.MapPath("ProgressReports/Rpt/rptFINAL_REPORT_DMHS_DUBAI_GRD10_12.rpt")
                End Select
            ElseIf ACD_ID = 1097 Then
                Select Case GRD_ID
                    Case "01", "02", "03", "04"
                        .reportPath = Server.MapPath("ProgressReports/Rpt/DMHS/rptFINAL_REPORT_DMHS_DUBAI_GRD01_04.rpt")
                    Case "05", "06", "07"
                        .reportPath = Server.MapPath("ProgressReports/Rpt/DMHS/rptFINAL_REPORT_DMHS_DUBAI_GRD05_07_2016_17.rpt")
                    Case "08", "09", "11"
                        .reportPath = Server.MapPath("ProgressReports/Rpt/DMHS/rptFINAL_REPORT_DMHS_DUBAI_GRD09_11_2015-16.rpt")
                    Case "10"
                        .reportPath = Server.MapPath("ProgressReports/Rpt/DMHS/rptFINAL_REPORT_DMHS_DUBAI_GRD10_2015-16.rpt")
                    Case "12"
                        .reportPath = Server.MapPath("ProgressReports/Rpt/DMHS/rptFINAL_REPORT_DMHS_DUBAI_GRD12_2015-16.rpt")


                End Select
            ElseIf ACD_ID = 1268 Then
                Select Case GRD_ID
                    Case "01", "02", "03", "04"
                        .reportPath = Server.MapPath("ProgressReports/Rpt/DMHS/rptFINAL_REPORT_DMHS_DUBAI_GRD01_04.rpt")
                    Case "05"
                        .reportPath = Server.MapPath("ProgressReports/Rpt/DMHS/rptFINAL_REPORT_DMHS_DUBAI_GRD05_2017-18.rpt")
                    Case "06", "07", "08", "09"
                        .reportPath = Server.MapPath("ProgressReports/Rpt/DMHS/rptFINAL_REPORT_DMHS_DUBAI_GRD09_11_2015-16.rpt")
                    Case "11"
                        .reportPath = Server.MapPath("ProgressReports/Rpt/DMHS/rptFINAL_REPORT_DMHS_DUBAI_GRD11_2017-18.rpt")
                    Case "10"
                        .reportPath = Server.MapPath("ProgressReports/Rpt/DMHS/rptFINAL_REPORT_DMHS_DUBAI_GRD10_2015-16.rpt")
                    Case "12"
                        .reportPath = Server.MapPath("ProgressReports/Rpt/DMHS/rptFINAL_REPORT_DMHS_DUBAI_GRD12_2015-16.rpt")


                End Select
            Else
                Select Case GRD_ID
                    Case "01", "02", "03", "04"
                        .reportPath = Server.MapPath("ProgressReports/Rpt/rptFINAL_REPORT_DMHS_DUBAI_GRD01_04.rpt")
                    Case "05", "06", "07", "08"
                        .reportPath = Server.MapPath("ProgressReports/Rpt/rptFINAL_REPORT_DMHS_DUBAI_GRD05_06.rpt")
                    Case "09", "11"
                        .reportPath = Server.MapPath("ProgressReports/Rpt/rptFINAL_REPORT_DMHS_DUBAI_GRD09_11.rpt")
                    Case "10"
                        If ACD_ID <= 962 Then
                            .reportPath = Server.MapPath("ProgressReports/Rpt/DMHS/rptFINAL_REPORT_DMHS_DUBAI_GRD10_12.rpt")
                        Else
                            .reportPath = Server.MapPath("ProgressReports/Rpt/DMHS/rptFINAL_REPORT_DMHS_DUBAI_GRD10_2015-16.rpt")
                        End If
                    Case "12"
                        If ((ACD_ID = 1097) Or (ACD_ID = 1268)) Then
                            .reportPath = Server.MapPath("ProgressReports/Rpt/DMHS/rptFINAL_REPORT_DMHS_DUBAI_GRD12_2015-16.rpt")
                        Else
                            .reportPath = Server.MapPath("ProgressReports/Rpt/DMHS/rptFINAL_REPORT_DMHS_DUBAI_GRD10_12.rpt")
                        End If

                End Select
            End If
        End With
        Session("rptClass") = rptClass
        If Session("downloadreport") = "1" Then
            DownloadReports(rptClass)
        Else
            LoadReports(rptClass)
        End If
    End Sub



    Private Sub GenerateDMHSAceReport(ByVal RSM_ID As String, ByVal RPF_ID As String, ByVal ACD_ID As String, ByVal STU_ID As String, ByVal BSU_ID As String, ByVal GRD_ID As String)
        Dim param As New Hashtable
        param.Add("@ACD_ID", Session("RSM_ACD_ID"))
        param.Add("@RSM_ID", RSM_ID)
        param.Add("@RPF_ID", RPF_ID)
        param.Add("@STU_IDS", Session("STU_ID"))
        param.Add("RPT_CAPTION", "TERM 1 REPORT")

        Dim rptClass As New rptClass

        With rptClass
            .crDatabase = "oasis_CCA"
            .reportParameters = param
            .reportPath = Server.MapPath("ProgressReports/Rpt/rptDMHSAceProgressReport.rpt")
        End With

        Session("rptClass") = rptClass
        If Session("downloadreport") = "1" Then
            DownloadReports(rptClass)
        Else
            LoadReports(rptClass)
        End If
    End Sub

    Private Sub GenerateTMSAceReport(ByVal RSM_ID As String, ByVal RPF_ID As String, ByVal ACD_ID As String, ByVal STU_ID As String, ByVal BSU_ID As String, ByVal GRD_ID As String)
        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("sbsuid"))
        param.Add("@IMG_TYPE", "logo")
        param.Add("@ACD_ID", Session("RSM_ACD_ID"))
        param.Add("@RSM_ID", RSM_ID)
        param.Add("@RPF_ID", RPF_ID)
        param.Add("@STU_IDS", STU_ID)
        param.Add("RPT_CAPTION", Session("RPF_DESCR"))

        Dim rptClass As New rptClass

        With rptClass
            .crDatabase = "oasis_CCA"
            .reportParameters = param
            .reportPath = Server.MapPath("ProgressReports/Rpt/rptTMSAceProgressReport.rpt")
        End With

        Session("rptClass") = rptClass
        If Session("downloadreport") = "1" Then
            DownloadReports(rptClass)
        Else
            LoadReports(rptClass)
        End If
    End Sub
    Private Sub GenerateCBSEFormativeAssessmentReport_11_12(ByVal RSM_ID As String, ByVal RPF_ID As String, ByVal ACD_ID As String, ByVal STU_ID As String, ByVal BSU_ID As String, ByVal GRD_ID As String)
        Dim param As New Hashtable
        param.Add("@ACD_ID", ACD_ID)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RSM_ID", RSM_ID)
        param.Add("@RPF_ID", RPF_ID)
        param.Add("@STU_ID", STU_ID)
        param.Add("RPT_CAPTION", Session("RPF_DESCR"))
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            '.Photos = GetPhotoClass()
            Select Case GetReportType(RSM_ID)
                Case "GRADE_11_12_FORMAT1"
                    param.Add("ATT_ASON_DATE", GetAsOnDateForAttendance(ACD_ID, GRD_ID, RPF_ID))
                    param.Add("UNIT_TEST_HEADING", "Unit Test")
                    .reportPath = Server.MapPath("ProgressReports/Rpt/rptTERM_REPORT_CBSE_GRD11_12_FORMAT1.rpt")
                Case "GRADE_11_12_FORMAT_NMS"
                    param.Add("ATT_ASON_DATE", GetAsOnDateForAttendance(ACD_ID, GRD_ID, RPF_ID))
                    param.Add("UNIT_TEST_HEADING", "Unit Test")
                    .reportPath = Server.MapPath("ProgressReports/Rpt/rptTERM_REPORT_CBSE_GRD11_12_FORMAT_NMS.rpt")
                Case "GRADE_11_12_FORMAT2"
                    param.Add("ATT_ASON_DATE", GetAsOnDateForAttendance(ACD_ID, GRD_ID, RPF_ID))
                    param.Add("UNIT_TEST_HEADING", "Unit Test")
                    .reportPath = Server.MapPath("ProgressReports/Rpt/rptTERM_REPORT_CBSE_GRD11_12_FORMAT2.rpt")

                Case "GRADE_11_12_FORMAT3"
                    param.Add("ATT_ASON_DATE", GetAsOnDateForAttendance(ACD_ID, GRD_ID, RPF_ID))
                    param.Add("UNIT_TEST_HEADING", "Unit Test")
                    .reportPath = Server.MapPath("ProgressReports/Rpt/rptTERM_REPORT_CBSE_GRD11_12_FORMAT3.rpt")
                Case "GRADE_11_12_FORMAT4"
                    param.Add("ATT_ASON_DATE", GetAsOnDateForAttendance(ACD_ID, GRD_ID, RPF_ID))
                    param.Add("UNIT_TEST_HEADING", "Unit Test")
                    .reportPath = Server.MapPath("ProgressReports/Rpt/rptTERM_REPORT_CBSE_GRD11_12_FORMAT4.rpt")
                Case "GRADE_11_12_FORMAT5"
                    param.Add("ATT_ASON_DATE", GetAsOnDateForAttendance(ACD_ID, GRD_ID, RPF_ID))
                    param.Add("UNIT_TEST_HEADING", "Unit Test")
                    .reportPath = Server.MapPath("ProgressReports/Rpt/rptTERM_REPORT_CBSE_GRD11_12_FORMAT5.rpt")
                Case "GRADE_11_12_FORMAT6"
                    param.Add("ATT_ASON_DATE", GetAsOnDateForAttendance(ACD_ID, GRD_ID, RPF_ID))
                    param.Add("UNIT_TEST_HEADING", "Unit Test")
                    .reportPath = Server.MapPath("ProgressReports/Rpt/rptTERM_REPORT_CBSE_GRD11_12_FORMAT6.rpt")
            End Select
        End With
        Session("rptClass") = rptClass
        If Session("downloadreport") = "1" Then
            DownloadReports(rptClass)
        Else
            LoadReports(rptClass)
        End If
    End Sub

    Private Sub GenerateCBSEFinalReport01_04(ByVal GRD_ID As String)

        Dim param As New Hashtable

        param.Add("@ACD_ID", Session("RSM_ACD_ID"))
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RSM_ID", Session("HFrsm_id"))
        param.Add("@RPF_ID", Session("HFrpf_id"))
        param.Add("@STU_ID", Session("STU_ID"))
        param.Add("RPT_CAPTION", Session("RPF_DESCR"))
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            Select Case GRD_ID
                Case "01", "02", "03", "04"
                    If ((Session("RSM_ACD_ID") = 1065) Or (Session("RSM_ACD_ID") = 1147)) Then
                        .reportPath = Server.MapPath("ProgressReports/Rpt/rptFINAL_REPORT_CBSE_01_04_2015-16.rpt")
                    Else
                        .reportPath = Server.MapPath("ProgressReports/Rpt/rptFINAL_REPORT_CBSE_01_04.rpt")
                    End If

                Case "11", "12"
                    If Session("RSM_ACD_ID") = 1063 Then
                        GenerateTermlyReport_OOEHS_Dubai(Session("HFrsm_id"), Session("HFrpf_id"), Session("RSM_ACD_ID"), Session("STU_ID"), Session("SBSUID"), GRD_ID)

                    Else
                        Select Case GetReportType(Session("HFrsm_id"))
                            Case "GRADE_11_12_FORMAT1"
                                .reportPath = Server.MapPath("ProgressReports/Rpt/rptFINAL_REPORT_CBSE_11_FORMAT1.rpt")
                            Case "GRADE_11_12_FORMAT3"
                                .reportPath = Server.MapPath("ProgressReports/Rpt/rptFINAL_REPORT_CBSE_11_FORMAT3.rpt")
                            Case "GRADE_11_12_FORMAT8"
                                .reportPath = Server.MapPath("ProgressReports/Rpt/rptFINAL_REPORT_CBSE_11_FORMAT8.rpt")
                            Case "GRADE_11_12_FORMAT9"
                                .reportPath = Server.MapPath("ProgressReports/Rpt/rptFINAL_REPORT_CBSE_11_FORMAT9.rpt")
                            Case "GRADE_11_12_FORMAT10"
                                .reportPath = Server.MapPath("ProgressReports/Rpt/rptFINAL_REPORT_CBSE_11_FORMAT10.rpt")
                            Case "GRADE_11_12_FORMAT11"
                                .reportPath = Server.MapPath("ProgressReports/Rpt/rptFINAL_REPORT_CBSE_11_FORMAT11.rpt")
                            Case "GRADE_11_FORMAT_NEW1"
                                .reportPath = Server.MapPath("ProgressReports/Rpt/rptFINAL_REPORT_CBSE_11_FORMAT_NEW1.rpt")
                            Case Else
                                .reportPath = Server.MapPath("ProgressReports/Rpt/rptFINAL_REPORT_CBSE_11_FORMAT2.rpt")
                        End Select
                    End If
                Case "05", "06", "07", "08"
                    If Session("ReportYear_DESC") = "2010-2011" Then
                        .reportPath = Server.MapPath("ProgressReports/Rpt/rptFINAL_REPORT_CBSE_05_09.rpt")
                    Else
                        .reportPath = Server.MapPath("ProgressReports/Rpt/rptCBSE_FINALREPORT_05_08_NEW.rpt")
                    End If
                Case "09"
                    Select Case hfReportFormat.Value
                        Case "CBSE_FINALREPORT_09"
                            param.Add("@IMG_BSU_ID", Session("SBSUID"))
                            param.Add("@IMG_TYPE", "LOGO")
                            param.Add("GRD_ID", Session("STP_GRD_ID"))
                            .reportPath = Server.MapPath("ProgressReports/Rpt/rptCBSEFinalReport_05_10_2012.rpt")
                            .Photos = GetPhotoClass(Session("STU_ID"))
                        Case "CBSE_FINALREPORT_09_2013"
                            param.Add("@IMG_BSU_ID", Session("SBSUID"))
                            param.Add("@IMG_TYPE", "LOGO")
                            param.Add("GRD_ID", Session("STP_GRD_ID"))
                            .reportPath = Server.MapPath("ProgressReports/Rpt/CBSE/rptCBSEFinalReport_05_10_2013.rpt")
                            .Photos = GetPhotoClass(Session("STU_ID"))
                        Case Else
                            If Session("ReportYear_DESC") = "2010-2011" Then
                                .reportPath = Server.MapPath("ProgressReports/Rpt/rptFINAL_REPORT_CBSE_05_09.rpt")
                            Else
                                If Session("sbsuid") = "121012" Or Session("sbsuid") = "121013" Or Session("sbsuid") = "121014" Or Session("sbsuid") = "123006" Then
                                    .reportPath = Server.MapPath("ProgressReports/Rpt/rptCBSE_FINALREPORT_05_08_NEW.rpt")
                                Else
                                    param.Add("@IMG_BSU_ID", Session("SBSUID"))
                                    param.Add("@IMG_TYPE", "LOGO")
                                    param.Add("GRD_ID", Session("STP_GRD_ID"))
                                    .reportPath = Server.MapPath("ProgressReports/Rpt/rptCBSEFinalReport_05_10_2012.rpt")
                                    .Photos = GetPhotoClass(Session("STU_ID"))
                                End If
                            End If
                    End Select
                Case Else
                    .reportPath = Server.MapPath("ProgressReports/Rpt/rptFINAL_REPORT_CBSE_05_09.rpt")
            End Select
        End With
        Session("rptClass") = rptClass
        If Session("downloadreport") = "1" Then
            DownloadReports(rptClass)
        Else
            LoadReports(rptClass)
        End If
    End Sub

    Private Sub GenerateCBSETermReport01_04()
        Dim param As New Hashtable
        param.Add("@ACD_ID", Session("RSM_ACD_ID"))
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@RSM_ID", Session("HFrsm_id"))
        param.Add("@RPF_ID", Session("HFrpf_id"))
        param.Add("@STU_ID", Session("STU_ID"))
        param.Add("accYear", Session("ReportYear_DESC"))
        param.Add("RPT_CAPTION", Session("RPF_DESCR"))

        param.Add("UserName", "")
        'param.Add("FA_HEADER1", "FA1")
        'param.Add("FA_HEADER2", "FA2")
        param.Add("SA_HEADER", "SA")
        param.Add("FA1_PERC", "10%")
        param.Add("FA2_PERC", "10%")
        param.Add("SA_PERC", "20%")
        param.Add("TOTAL_PERC", "60%")
        param.Add("TOTAL_FA_PERC", "20%")

        If Session("RPF_DESCR").ToUpper = "SUMMATIVE REPORT 1" Or Session("RPF_DESCR").ToUpper = "TERM 1 REPORT" Then
            param.Add("FA_HEADER1", "FA1")
            param.Add("FA_HEADER2", "FA2")
        Else
            param.Add("FA_HEADER1", "FA3")
            param.Add("FA_HEADER2", "FA4")
        End If
        param.Add("GRD_ID", Session("STP_GRD_ID"))
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            '.Photos = GetPhotoClass()
            If ((Session("RPF_DESCR").ToUpper = "TERM 2 REPORT") And (((Session("RSM_ACD_ID") = 1065) Or (Session("RSM_ACD_ID") = 1147)))) Then
                .reportPath = Server.MapPath("ProgressReports/Rpt/rptCBSE_SUMMATIVE_ASSESSMENT_REPORT_01_04_2015-16.rpt")
            Else
                .reportPath = Server.MapPath("ProgressReports/Rpt/rptCBSE_SUMMATIVE_ASSESSMENT_REPORT_01_04.rpt")
            End If

        End With
        Session("rptClass") = rptClass

        If Session("downloadreport") = "1" Then
            DownloadReports(rptClass)
        Else
            LoadReports(rptClass)
        End If

    End Sub

    Private Sub GenerateMidTermReport_DMHS_Dubai()
        Dim param As New Hashtable
        param.Add("@ACD_ID", Session("RSM_ACD_ID"))
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RSM_ID", Session("HFrsm_id"))
        param.Add("@RPF_ID", Session("HFrpf_id"))
        param.Add("@STU_ID", Session("STU_ID"))
        'param.Add("RPT_CAPTION", ddlReportPrintedFor.SelectedItem.Text)
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            Select Case hfReportFormat.Value.ToUpper
                Case "DMHS_BRIDGEREPORT"
                    .reportPath = Server.MapPath("ProgressReports/Rpt/DMHS/rptDMHS_IB_BRIDGEREPORT.rpt")
                Case "DMHS_BRIDGEREPORT_2"
                    .reportPath = Server.MapPath("ProgressReports/Rpt/DMHS/rptDMHS_IB_BRIDGEREPORT_2.rpt")
                Case "IB_MIDTERMREPORT"
                    .reportPath = Server.MapPath("ProgressReports/Rpt/DMHS/rptDMHS_IB_MIDTERMFEEDBACKREPORT.rpt")
                Case "IB_TERMREPORT"
                    .reportPath = Server.MapPath("ProgressReports/Rpt/DMHS/rptDMHS_IB_TERM1.rpt")
                Case "MTR_2018"
                    .reportPath = Server.MapPath("ProgressReports/Rpt/DMHS/rptDMHSMidTermReviewReport_2018-19.rpt")
                Case Else
                    If Session("STP_GRD_ID") = "KG1" OrElse Session("STP_GRD_ID") = "KG2" Then
                        .reportPath = Server.MapPath("ProgressReports/Rpt/rptMID_TERM_REPORT_DMHS_KG.rpt")
                    Else
                        .reportPath = Server.MapPath("ProgressReports/Rpt/rptDMHSMidTermReviewReport.rpt")
                    End If
            End Select

        End With
        Session("rptClass") = rptClass

        If Session("downloadreport") = "1" Then
            DownloadReports(rptClass)
        Else
            LoadReports(rptClass)
        End If
    End Sub




    Private Sub GenerateSummativeAssessmentReport()
        Dim param As New Hashtable
        param.Add("@ACD_ID", Session("RSM_ACD_ID"))
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        ' param.Add("@IMG_TYPE", "LOGO")
        param.Add("@RSM_ID", Session("HFrsm_id"))
        param.Add("@RPF_ID", Session("HFrpf_id"))
        param.Add("@STU_ID", Session("STU_ID"))
        param.Add("accYear", Session("ReportYear_DESC"))

        If Session("RPF_DESCR") = "SUMMATIVE REPORT 1" Then
            param.Add("RPT_CAPTION", "TERM I REPORT")
            param.Add("FA1_PERC", "10%")
            param.Add("FA2_PERC", "10%")
            param.Add("TOTAL_FA_PERC", "20%")
            If Session("ReportYear_DESC") = "2010-2011" Then
                param.Add("SA_PERC", "20%")
                param.Add("TOTAL_PERC", "40%")
            ElseIf Session("ReportYear_DESC") = "2011-2012" And Session("STP_GRD_ID") = "10" Then
                param.Add("SA_PERC", "20%")
                param.Add("TOTAL_PERC", "40%")
            Else
                param.Add("SA_PERC", "30%")
                param.Add("TOTAL_PERC", "50%")
            End If
            param.Add("FA_HEADER1", "FA1")
            param.Add("FA_HEADER2", "FA2")
        Else
            param.Add("RPT_CAPTION", "TERM II REPORT")
            param.Add("FA1_PERC", "10%")
            param.Add("FA2_PERC", "10%")
            If Session("ReportYear_DESC") = "2010-2011" Then
                param.Add("SA_PERC", "40%")
                param.Add("TOTAL_PERC", "60%")
            ElseIf Session("ReportYear_DESC") = "2011-2012" And Session("STP_GRD_ID") = "10" Then
                param.Add("SA_PERC", "40%")
                param.Add("TOTAL_PERC", "60%")
            Else
                param.Add("SA_PERC", "30%")
                param.Add("TOTAL_PERC", "50%")
            End If
            param.Add("TOTAL_FA_PERC", "20%")
            param.Add("FA_HEADER1", "FA3")
            param.Add("FA_HEADER2", "FA4")
        End If

        param.Add("UserName", "")
        'param.Add("SA_HEADER", "SA")
        Dim vSA_HEADER As String = "SA"

        If Session("STP_GRD_ID") = "10" Then
            If Session("RPF_DESCR").ToString.ToUpper <> "SUMMATIVE REPORT 1" Then
                param.Add("SA_HEADER", "Rehearsal Exam")
            Else
                param.Add("SA_HEADER", vSA_HEADER)
            End If
        Else
            param.Add("SA_HEADER", vSA_HEADER)
        End If
        param.Add("GRD_ID", Session("STP_GRD_ID"))
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            '.Photos = GetPhotoClass()
            '.reportPath = Server.MapPath("ProgressReports/Rpt/rptCBSE_SUMMATIVE_ASSESSMENT_REPORT_05_10.rpt")
            If Session("ReportYear_DESC") = "2010-2011" Then
                .reportPath = Server.MapPath("ProgressReports/Rpt/rptCBSE_SUMMATIVE_ASSESSMENT_REPORT_05_10.rpt")
            Else
                If Session("STP_GRD_ID") = "10" And Session("ReportYear_DESC") = "2011-2012" Then
                    .reportPath = Server.MapPath("ProgressReports/Rpt/rptCBSE_SUMMATIVE_ASSESSMENT_REPORT_10_new.rpt")
                Else
                    .reportPath = Server.MapPath("ProgressReports/Rpt/rptCBSE_SUMMATIVE_ASSESSMENT_REPORT_05_10_new.rpt")
                End If
            End If

        End With
        Session("rptClass") = rptClass
        If Session("downloadreport") = "1" Then
            DownloadReports(rptClass)
        Else
            LoadReports(rptClass)
        End If

    End Sub

    Private Sub GenerateWISReports()
        Dim param As New Hashtable
        param.Add("@ACD_ID", Session("RSM_ACD_ID"))
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RSM_ID", Session("HFrsm_id"))
        param.Add("@RPF_ID", Session("HFrpf_id"))
        param.Add("@STU_ID", Session("STU_ID"))

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            ' .Photos = GetPhotoClass()
            Select Case hfReportFormat.Value
                Case "FINAL_REPORT"
                    .Photos = GetPhotoClass(Session("STU_ID"))
                    Select Case Session("STP_GRD_ID")
                        Case "01", "02", "03", "04", "05", "06"
                            .reportPath = Server.MapPath("ProgressReports/Rpt/WIS/rptWIS_FinalRFeport_01_06.rpt")
                        Case "KG1", "KG2"
                            .reportPath = Server.MapPath("ProgressReports/Rpt/WIS/rptWIS_FinalRFeport_FS1_FS2.rpt")
                    End Select
                Case Else
                    Select Case Session("STP_GRD_ID")
                        Case "10", "11", "12", "13"
                            .reportPath = Server.MapPath("ProgressReports/Rpt/WIS/rptWISStudentProgressReport_10_13.rpt")
                        Case "07", "08", "09"
                            .reportPath = Server.MapPath("ProgressReports/Rpt/WIS/rptWISStudentProgressReport_07_09.rpt")
                        Case "01", "02", "03", "04", "05", "06"
                            param.Add("accYear", Session("ReportYear_DESC"))
                            .reportPath = Server.MapPath("ProgressReports/Rpt/WIS/rptWIS_StudentProgressReport_01_06.rpt")
                    End Select
            End Select





            '.reportPath = Server.MapPath("../Rpt/rptWIN_PROGRESSREPORT_07_10_online.rpt")
        End With
        Session("rptClass") = rptClass
        If Session("downloadreport") = "1" Then
            DownloadReports(rptClass)
        Else
            LoadReports(rptClass)
        End If
    End Sub
    Private Sub GenerateOOFIGCSEReports()
        Dim param As New Hashtable

        param.Add("@ACD_ID", Session("RSM_ACD_ID"))
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RSM_ID", Session("HFrsm_id"))
        param.Add("@RPF_ID", Session("HFrpf_id"))
        param.Add("@STU_ID", Session("STU_ID"))
        param.Add("RPT_CAPTION", Session("RPF_DESCR"))


        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param

            Select Case Session("STP_GRD_ID")
                Case "09"
                    If Session("RPF_DESCR") = "TERM 2 REPORT" Then
                        param.Add("ATT_ASON_DATE", "")
                        .reportPath = Server.MapPath("ProgressReports/Rpt/rptOOF_IGCSE_TERM_REPORT_S4.rpt")
                    ElseIf Session("RPF_DESCR") = "FINAL REPORT" Then
                        param.Add("RPT_ASSESSMENT_HEADER", Session("RPF_DESCR"))
                        param.Add("UserName", "")
                        .reportPath = Server.MapPath("ProgressReports/Rpt/rptFINAL_REPORT_CBSE_11_FORMAT8.rpt")
                    Else

                        If Session("RSM_ACD_ID") = "1353" Then
                            param.Add("ATT_ASON_DATE", GetAsOnDateForAttendance(Session("RSM_ACD_ID"), Session("STP_GRD_ID"), Session("HFrpf_id")))
                            .reportPath = Server.MapPath("ProgressReports/Rpt/rptOOF_IGCSE_TERM_REPORT_S3_S4_2017_18.rpt")
                        Else
                            param.Add("ATT_ASON_DATE", "")
                            .reportPath = Server.MapPath("ProgressReports/Rpt/rptOOF_IGCSE_TERM_REPORT_S3_S4.rpt")
                        End If

                    End If
                Case "10"
                    If Session("RPF_DESCR") = "TERM 2 REPORT" Then
                        param.Add("ATT_ASON_DATE", "")
                        .reportPath = Server.MapPath("ProgressReports/Rpt/rptOOF_IGCSE_TERM_REPORT_S4_S5_Term2.rpt")
                    ElseIf Session("RPF_DESCR") = "FINAL REPORT" Then
                        param.Add("RPT_ASSESSMENT_HEADER", Session("RPF_DESCR"))
                        param.Add("UserName", "")
                        .reportPath = Server.MapPath("ProgressReports/Rpt/rptFINAL_REPORT_CBSE_11_FORMAT8.rpt")
                    Else
                        param.Add("ATT_ASON_DATE", GetAsOnDateForAttendance(Session("RSM_ACD_ID"), Session("STP_GRD_ID"), Session("HFrpf_id")))
                        If Session("RSM_ACD_ID") = "1353" Then
                            .reportPath = Server.MapPath("ProgressReports/Rpt/rptOOF_IGCSE_TERM_REPORT_S3_S4_2017_18.rpt")
                        Else
                            .reportPath = Server.MapPath("ProgressReports/Rpt/rptOOF_IGCSE_TERM_REPORT_S3_S4.rpt")
                        End If
                    End If
                Case "11"

                    If Session("RPF_DESCR") = "FINAL REPORT" Then
                        param.Add("RPT_ASSESSMENT_HEADER", Session("RPF_DESCR"))
                        param.Add("UserName", "")
                        .reportPath = Server.MapPath("ProgressReports/Rpt/rptFINAL_REPORT_CBSE_11_FORMAT8.rpt")
                    Else
                        param.Add("ATT_ASON_DATE", GetAsOnDateForAttendance(Session("RSM_ACD_ID"), Session("STP_GRD_ID"), Session("HFrpf_id")))
                        If Session("RSM_ACD_ID") = "1353" Then
                            .reportPath = Server.MapPath("ProgressReports/Rpt/rptOOF_IGCSE_TERM_REPORT_S5_2016_17.rpt")
                        Else
                            .reportPath = Server.MapPath("ProgressReports/Rpt/rptOOF_IGCSE_TERM_REPORT_S5.rpt")
                        End If

                    End If

            End Select
        End With
        Session("rptClass") = rptClass

        If Session("downloadreport") = "1" Then
            DownloadReports(rptClass)
        Else
            LoadReports(rptClass)
        End If

    End Sub



    Sub GenerateGWAReports(ByVal strRSM_ID As String, ByVal strRPF_ID As String, ByVal strACD_ID As String, ByVal strSTU_ID As String, ByVal strBSU_ID As String, ByVal strGRD_ID As String)
        Dim param As New Hashtable
        param.Add("@ACD_ID", strACD_ID)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RSM_ID", strRSM_ID)
        param.Add("@RPF_ID", strRPF_ID)
        param.Add("@STU_ID", strSTU_ID)
        param.Add("RPF_DESCR", Session("RPF_DESCR"))
        param.Add("RPF_DATE", Now.ToShortDateString)

        'Session("studentphotopath") = TransferPath(h_STU_IDs.Value.Replace("___", "|"))
        'param.Add("UserName", Session("sUsr_name"))
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param

            .Photos = GetPhotoClass(strRSM_ID)
            Select Case GetReportType(strRSM_ID)
                Case "GWA_ELEMENTARYREPORT"
                    .reportPath = Server.MapPath("ProgressReports/Rpt/rptGWA_ElementarySchool.rpt")
                Case "GWA_TERMLYREPORT"
                    .reportPath = Server.MapPath("ProgressReports/Rpt/rptGWA_TERMLYREPORT.rpt")
                Case "GWA_MIDTERMREPORT"
                    .reportPath = Server.MapPath("ProgressReports/Rpt/rptGWA_MidSemester.rpt")
                Case "GWA_MYPREPORT"
                    .reportPath = Server.MapPath("ProgressReports/Rpt/rptGWA_MYPREPORT.rpt")
                Case "GWA_ALSREPORT"
                    .reportPath = Server.MapPath("ProgressReports/Rpt/rptGWA_ALSREPORT.rpt")

            End Select
        End With
        Session("rptClass") = rptClass
        If Session("downloadreport") = "1" Then
            DownloadReports(rptClass)
        Else
            LoadReports(rptClass)
        End If
    End Sub


    Sub DownloadReports(ByVal rptClass)
        Dim rs As New ReportDocument
        Try
            Dim iRpt As New DictionaryEntry
            Dim i As Integer
            Dim rptStr As String = ""

            Dim crParameterDiscreteValue As ParameterDiscreteValue
            Dim crParameterFieldDefinitions As ParameterFieldDefinitions
            Dim crParameterFieldLocation As ParameterFieldDefinition
            Dim crParameterValues As ParameterValues
            With rptClass
                'C:\Application\ParLogin\ParentLogin\ProgressReports\Rpt\rptTERM_REPORT_OOEHS_DUBAI_GRD05_08.rpt
                rs.Load(.reportPath)

                Dim myConnectionInfo As ConnectionInfo = New ConnectionInfo()
                myConnectionInfo.ServerName = .crInstanceName
                myConnectionInfo.DatabaseName = .crDatabase
                myConnectionInfo.UserID = .crUser
                myConnectionInfo.Password = .crPassword

                SetDBLogonForSubreports(myConnectionInfo, rs, .reportParameters)
                SetDBLogonForReport(myConnectionInfo, rs, .reportParameters)

                crParameterFieldDefinitions = rs.DataDefinition.ParameterFields
                If .reportParameters.Count <> 0 Then
                    For Each iRpt In .reportParameters
                        crParameterFieldLocation = crParameterFieldDefinitions.Item(iRpt.Key.ToString)
                        crParameterValues = crParameterFieldLocation.CurrentValues
                        crParameterDiscreteValue = New CrystalDecisions.Shared.ParameterDiscreteValue
                        crParameterDiscreteValue.Value = iRpt.Value
                        crParameterValues.Add(crParameterDiscreteValue)
                        crParameterFieldLocation.ApplyCurrentValues(crParameterValues)
                    Next
                End If

                If .selectionFormula <> "" Then
                    rs.RecordSelectionFormula = .selectionFormula
                End If
                Try
                    exportReport(rs, CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat)
                Catch ee As Exception
                End Try
                Try
                    For Each table As Table In rs.Database.Tables
                        table.Dispose()
                    Next
                    rs.Database.Dispose()
                Catch ex As Exception
                    UtilityObj.Errorlog("Online rs.database.dispose--", ex.Message)
                End Try
                rs.Close()
                rs.Dispose()
            End With
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "Report DownLoad")
            rs.Close()
            rs.Dispose()
        End Try
        'GC.Collect()
    End Sub

    Protected Sub exportReport(ByVal selectedReport As CrystalDecisions.CrystalReports.Engine.ReportDocument, ByVal eft As CrystalDecisions.Shared.ExportFormatType)
        selectedReport.ExportOptions.ExportFormatType = eft

        Dim contentType As String = ""
        ' Make sure asp.net has create and delete permissions in the directory
        Dim tempDir As String = Server.MapPath("ReportDownloads/")
        Dim tempFileName As String = Session("STU_BSU_ID") + "_" + Session("username") + "_" + Now.ToString.Replace("/", "_").Replace(":", "_").Replace(" ", "_") + "." 'Session.SessionID.ToString() & "."
        Select Case eft
            Case CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat
                tempFileName += "pdf"
                contentType = "application/pdf"
                Exit Select
            Case CrystalDecisions.[Shared].ExportFormatType.WordForWindows
                tempFileName += "doc"
                contentType = "application/msword"
                Exit Select
            Case CrystalDecisions.[Shared].ExportFormatType.Excel
                tempFileName += "xls"
                contentType = "application/vnd.ms-excel"
                Exit Select
            Case CrystalDecisions.[Shared].ExportFormatType.HTML32, CrystalDecisions.[Shared].ExportFormatType.HTML40
                tempFileName += "htm"
                contentType = "text/html"
                Dim hop As New CrystalDecisions.Shared.HTMLFormatOptions()
                hop.HTMLBaseFolderName = tempDir
                hop.HTMLFileName = tempFileName
                selectedReport.ExportOptions.FormatOptions = hop
                Exit Select
        End Select

        Dim dfo As New CrystalDecisions.Shared.DiskFileDestinationOptions()
        dfo.DiskFileName = tempDir + tempFileName
        selectedReport.ExportOptions.DestinationOptions = dfo
        selectedReport.ExportOptions.ExportDestinationType = CrystalDecisions.[Shared].ExportDestinationType.DiskFile

        selectedReport.Export()
        selectedReport.Close()

        Dim tempFileNameUsed As String
        If eft = CrystalDecisions.[Shared].ExportFormatType.HTML32 OrElse eft = CrystalDecisions.[Shared].ExportFormatType.HTML40 Then
            Dim fp As String() = selectedReport.FilePath.Split("\".ToCharArray())
            Dim leafDir As String = fp(fp.Length - 1)
            ' strip .rpt extension
            leafDir = leafDir.Substring(0, leafDir.Length - 4)
            tempFileNameUsed = String.Format("{0}{1}\{2}", tempDir, leafDir, tempFileName)
        Else
            tempFileNameUsed = tempDir + tempFileName
        End If

        'Response.ClearContent()
        'Response.ClearHeaders()
        'Response.ContentType = contentType

        'Response.WriteFile(tempFileNameUsed)
        'Response.Flush()
        'Response.Close()


        ' HttpContext.Current.Response.ContentType = "application/octect-stream"
        HttpContext.Current.Response.ContentType = "application/pdf"
        HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" & System.IO.Path.GetFileName(tempFileNameUsed))
        HttpContext.Current.Response.Clear()
        HttpContext.Current.Response.WriteFile(tempFileNameUsed)
        ' HttpContext.Current.Response.Flush()
        '  HttpContext.Current.Response.Close()
        HttpContext.Current.Response.End()

        System.IO.File.Delete(tempFileNameUsed)
    End Sub


    Protected Sub DisplayReport(ByVal selectedReport As CrystalDecisions.CrystalReports.Engine.ReportDocument, ByVal eft As CrystalDecisions.Shared.ExportFormatType)
        selectedReport.ExportOptions.ExportFormatType = eft

        Dim contentType As String = ""
        ' Make sure asp.net has create and delete permissions in the directory
        Dim tempDir As String = Server.MapPath("ReportDownloads/")

        ' UtilityObj.Errorlog(tempDir, "Rajesh123")
        'Dim tempDir As String = "https://school.gemsoasis.com/GEMSPARENT/curriculum/ReportDownloads/"

        Dim tempFileName As String = Session("STU_BSU_ID") + "_" + Session("username") + "_" + Now.ToString.Replace("/", "_").Replace(":", "_").Replace(" ", "_") + "." 'Session.SessionID.ToString() & "."
        Select Case eft
            Case CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat
                tempFileName += "pdf"
                contentType = "application/pdf"
                Exit Select
            Case CrystalDecisions.[Shared].ExportFormatType.WordForWindows
                tempFileName += "doc"
                contentType = "application/msword"
                Exit Select
            Case CrystalDecisions.[Shared].ExportFormatType.Excel
                tempFileName += "xls"
                contentType = "application/vnd.ms-excel"
                Exit Select
            Case CrystalDecisions.[Shared].ExportFormatType.HTML32, CrystalDecisions.[Shared].ExportFormatType.HTML40
                tempFileName += "htm"
                contentType = "text/html"
                Dim hop As New CrystalDecisions.Shared.HTMLFormatOptions()
                hop.HTMLBaseFolderName = tempDir
                hop.HTMLFileName = tempFileName
                selectedReport.ExportOptions.FormatOptions = hop
                Exit Select
        End Select

        Dim dfo As New CrystalDecisions.Shared.DiskFileDestinationOptions()
        dfo.DiskFileName = tempDir + tempFileName
        selectedReport.ExportOptions.DestinationOptions = dfo
        selectedReport.ExportOptions.ExportDestinationType = CrystalDecisions.[Shared].ExportDestinationType.DiskFile

        selectedReport.Export()
        selectedReport.Close()

        ' UtilityObj.Errorlog(tempFileName, "Rajesh123")

        Dim tempFileNameUsed As String
        If eft = CrystalDecisions.[Shared].ExportFormatType.HTML32 OrElse eft = CrystalDecisions.[Shared].ExportFormatType.HTML40 Then
            Dim fp As String() = selectedReport.FilePath.Split("\".ToCharArray())
            Dim leafDir As String = fp(fp.Length - 1)
            ' strip .rpt extension
            leafDir = leafDir.Substring(0, leafDir.Length - 4)
            tempFileNameUsed = String.Format("{0}{1}\{2}", tempDir, leafDir, tempFileName)
        Else
            tempFileNameUsed = tempDir + tempFileName
        End If
        ' UtilityObj.Errorlog(tempDir, "Report DownLoad")
        'Response.ClearContent()
        'Response.ClearHeaders()
        'Response.ContentType = contentType

        'Response.WriteFile(tempFileNameUsed)
        'Response.Flush()
        'Response.Close()


        '  HttpContext.Current.Response.ContentType = "application/octect-stream"

        'HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" & System.IO.Path.GetFileName(tempFileNameUsed))
        If Request.Browser.Type.StartsWith("IE") Or Request.Browser.Type.ToLower.Contains("internet") Or Request.Browser.Type.ToLower.Contains("mozilla") Then
            Response.ClearContent()
            Response.ClearHeaders()
            Response.ContentType = "application/pdf"
            Response.WriteFile(tempFileNameUsed)
            Response.Flush()
            Response.Close()
            System.IO.File.Delete(tempFileNameUsed)
        Else
            Dim ViewServer, PathOfFile As String
            ViewServer = Replace(WebConfigurationManager.AppSettings.Item("DocViewerURL"), "*", "&")
            'PathOfFile = "https://oasis.gemseducation.com/Curriculum/ReportDownloads/"
            'PathOfFile = "C:\Application\GEMSPARENT\Curriculum\ReportDownloads\"
            'PathOfFile = Replace(PathOfFile, "://", "%3A%2F%2F")
            ' PathOfFile = Replace(PathOfFile, "/", "%2F")
            ' PathOfFile += tempFileName

            ' PathOfFile = "/Curriculum/ReportDownloads/" & tempFileName
            ' PathOfFile = "https://oasis.gemseducation.com/Curriculum/ReportDownloads/" & tempFileName
            PathOfFile = "https://school.gemsoasis.com/GEMSPARENT/curriculum/ReportDownloads/" & tempFileName
            ' Dim s As String = "~/pdf.js/web/viewer.html?file=" & PathOfFile
            Dim s As String = "https://school.gemsoasis.com/GEMSPARENT/pdf.js/web/viewer.html?file=" & PathOfFile
            Response.Redirect(s, False)
            ' Dim ViewURL As String = ViewServer & PathOfFile
            ' DocumentIFrame.Attributes("src") = s

        End If

    End Sub

    Sub GenerateProgressreports_SIMS(ByVal RPF_ID As String)
        Dim tempDir As String = ConfigurationManager.ConnectionStrings("STU_REPORTCARDS").ConnectionString
        Dim tempFileName As String = "/" + Session("STU_BSU_ID") + "/" + RPF_ID + "/" + Session("STU_NO") + ".pdf"
        Dim tempFileNameUsed As String = tempDir + tempFileName
        If Session("downloadreport") = "1" Then
            HttpContext.Current.Response.ContentType = "application/pdf"
            HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" & System.IO.Path.GetFileName(tempFileNameUsed))
            HttpContext.Current.Response.Clear()
            HttpContext.Current.Response.WriteFile(tempFileNameUsed)
            HttpContext.Current.Response.End()
        Else
            Response.ClearContent()
            Response.ClearHeaders()
            Response.ContentType = "application/pdf"
            Response.WriteFile(tempFileNameUsed)
            Response.Flush()
            Response.Close()
        End If
    End Sub

    Private Sub GenerateFinalReport_TWS(ByVal RSM_ID As String, ByVal RPF_ID As String, ByVal ACD_ID As String, ByVal STU_ID As String, ByVal BSU_ID As String, ByVal GRD_ID As String)
        Dim param As New Hashtable
        param.Add("@ACD_ID", ACD_ID)
        param.Add("@BSU_ID", BSU_ID)
        param.Add("@RSM_ID", RSM_ID)
        param.Add("@RPF_ID", RPF_ID)
        param.Add("@STU_ID", STU_ID)
        param.Add("RPT_CAPTION", Session("RPF_DESCR"))

        'Session("studentphotopath") = TransferPath(h_STU_IDs.Value.Replace("___", "|"))
        param.Add("UserName", "Online")
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            Select Case hfReportFormat.Value
                Case "FINAL_REPORTS_15-16"
                    .reportPath = Server.MapPath("ProgressReports/Rpt/TWS/rptFINAL_REPORT_TWS_DUBAI_GRD01_06_2015-16.rpt")
                Case Else
                    Select Case GRD_ID
                        Case "01", "02", "03", "04", "05", "06"
                            If ACD_ID = "592" Then
                                .reportPath = Server.MapPath("ProgressReports/Rpt/TWS/rptFINAL_REPORT_TWS_DUBAI_GRD01_06.rpt")
                            Else
                                .reportPath = Server.MapPath("ProgressReports/Rpt/TWS/rptFINAL_REPORT_TWS_DUBAI_GRD01_06_2011.rpt")
                            End If
                        Case "07", "08"
                            param.Add("ATTEND_TOTAL", "")
                            If ((ACD_ID = 977) Or (ACD_ID = 1071) Or (ACD_ID = 1124)) Then
                                .reportPath = Server.MapPath("ProgressReports/Rpt/TWS/rptFINAL_REPORT_TWS_DUBAI_GRD_09_10_2014-15.rpt")

                            Else
                                .reportPath = Server.MapPath("ProgressReports/Rpt/TWS/rptFINAL_REPORT_TWS_DUBAI_GRD07_08.rpt")
                            End If
                        Case Else
                            param.Add("ATTEND_TOTAL", "")
                            If ((ACD_ID = 977) Or (ACD_ID = 1071) Or (ACD_ID = 1124)) Then
                                .reportPath = Server.MapPath("ProgressReports/Rpt/TWS/rptFINAL_REPORT_TWS_DUBAI_GRD_09_10_2014-15.rpt")

                            Else
                                .reportPath = Server.MapPath("ProgressReports/Rpt/TWS/rptFINAL_REPORT_TWS_DUBAI_GRD_09_10.rpt")

                            End If

                    End Select
            End Select
        End With
        If Session("downloadreport") = "1" Then
            DownloadReports(rptClass)
        Else
            LoadReports(rptClass)
        End If
    End Sub

    Private Sub GenerateTermlyReport_TWS(ByVal RSM_ID As String, ByVal RPF_ID As String, ByVal ACD_ID As String, ByVal STU_ID As String, ByVal BSU_ID As String, ByVal GRD_ID As String)
        Dim param As New Hashtable
        param.Add("@ACD_ID", ACD_ID)
        param.Add("@BSU_ID", BSU_ID)
        param.Add("@RSM_ID", RSM_ID)
        param.Add("@RPF_ID", RPF_ID)
        param.Add("@STU_ID", STU_ID)
        param.Add("RPT_CAPTION", Session("RPF_DESCR"))
        If GRD_ID <> "KG1" And GRD_ID <> "KG2" Then
            param.Add("ATTEND_TOTAL", GetTotalGradeAttendance(ACD_ID, GRD_ID, RPF_ID))
        End If
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            Select Case hfReportType.Value
                Case "TWS_INTERIM_REPORT_APP"
                    param.Add("@bSLT", "false")
                    Select Case GRD_ID
                        Case "KG1", "KG2"
                            .Photos = GetPhotoClass(STU_ID)
                            param.Add("GRD_ID", GRD_ID)
                            .reportPath = Server.MapPath("ProgressReports/Rpt/TWS/rptTWS_PROGRESSREPORT_APP_FS1-FS2.rpt")
                            Session("rptClass") = rptClass
                        Case "09", "10", "11", "12", "13"
                            .reportPath = Server.MapPath("ProgressReports/Rpt/TWS/rptTWS_INTERIMREPORT_9-10.rpt")
                        Case Else

                            .reportPath = Server.MapPath("ProgressReports/Rpt/TWS/rptTWS_INTERIMREPORT_APP_1-8.rpt")
                    End Select
                Case "TWS_TERMLY_REPORT_2016"
                    Select Case GRD_ID
                        Case "01", "02", "03", "04", "05", "06"
                            .reportPath = Server.MapPath("ProgressReports/Rpt/TWS/rptTERM_REPORT_TWS_DUBAI_GRD01_06_2016.rpt")
                    End Select
                Case "TWS_TERMLY_REPORT_2017"
                    Select Case GRD_ID
                        Case "01", "02", "03", "04", "05", "06"
                            .reportPath = Server.MapPath("ProgressReports/Rpt/TWS/rptTERM_REPORT_TWS_DUBAI_GRD01_06_2017.rpt")
                    End Select
                Case "TWS_TERMLY_REPORT_2014"
                    Select Case GRD_ID
                        Case "01", "02", "03", "04", "05", "06"
                            .reportPath = Server.MapPath("ProgressReports/Rpt/TWS/rptTERM_REPORT_TWS_DUBAI_GRD01_06_2014.rpt")
                        Case "07", "08"
                            If Session("RPF_DESCR") = "SECOND TERM REPORT" Then
                                .reportPath = Server.MapPath("ProgressReports/Rpt/TWS/rptTERM_REPORT_TWS_DUBAI_2014_GRD09_10.rpt")
                            Else
                                .reportPath = Server.MapPath("ProgressReports/Rpt/TWS/rptTERM_REPORT_TWS_DUBAI_2014_GRD07_08.rpt")
                            End If

                        Case "09", "10", "11"
                            .reportPath = Server.MapPath("ProgressReports/Rpt/TWS/rptTERM_REPORT_TWS_DUBAI_2014_GRD09_10.rpt")
                    End Select
                Case "TWS_TERMLY_REPORT_2015"
                    Select Case GRD_ID
                        Case "01", "02", "03", "04", "05", "06"
                            .reportPath = Server.MapPath("ProgressReports/Rpt/TWS/rptTERM_REPORT_TWS_DUBAI_GRD01_06_2014.rpt")
                        Case "07", "08"
                            .reportPath = Server.MapPath("ProgressReports/Rpt/TWS/rptTERM_REPORT_TWS_DUBAI_2014_GRD09_10.rpt")
                        Case "09", "10", "11"
                            .reportPath = Server.MapPath("ProgressReports/Rpt/TWS/rptTERM_REPORT_TWS_DUBAI_2014_GRD09_10.rpt")
                    End Select
                Case "TWS_TERMLY_REPORT_2012"
                    Select Case GRD_ID
                        Case "KG1", "KG2"
                            param.Add("GRD_ID", GRD_ID)
                            .reportPath = Server.MapPath("ProgressReports/Rpt/TWS/rptTWS_PROGRESSREPORT_FS1_FS2.rpt")
                        Case "01", "02", "03", "04", "05", "06"
                            .reportPath = Server.MapPath("ProgressReports/Rpt/TWS/rptTERM_REPORT_TWS_DUBAI_GRD01_06_2011.rpt")
                        Case "07", "08"
                            .reportPath = Server.MapPath("ProgressReports/Rpt/TWS/rptTERM_REPORT_TWS_DUBAI_2012_GRD07_08.rpt")
                        Case "09"
                            param("Max_Mark_UT") = "25"
                            param("Max_Mark_Exam") = "75"
                            .reportPath = Server.MapPath("ProgressReports/Rpt/TWS/rptTERM_REPORT_TWS_DUBAI_2012_GRD09_10.rpt")
                        Case "10"
                            param("Max_Mark_UT") = "20"
                            param("Max_Mark_Exam") = "80"
                            .reportPath = Server.MapPath("ProgressReports/Rpt/TWS/rptTERM_REPORT_TWS_DUBAI_2012_GRD09_10.rpt")
                        Case "11"
                            .reportPath = Server.MapPath("ProgressReports/Rpt/TWS/rptTERM_REPORT_TWS_DUBAI_2012_GRD11.rpt")
                        Case "12", "13"
                            .reportPath = Server.MapPath("ProgressReports/Rpt/TWS/rptTERM_REPORT_TWS_DUBAI_2012_GRD12_13.rpt")
                    End Select
                Case Else
                    Select Case GRD_ID
                        Case "KG1", "KG2"
                            param.Add("GRD_ID", GRD_ID)
                            .reportPath = Server.MapPath("ProgressReports/Rpt/TWS/rptTWS_PROGRESSREPORT_FS1_FS2.rpt")
                        Case "01", "02", "03", "04", "05", "06"
                            If ACD_ID = "592" Or ACD_ID = "570" Then
                                .reportPath = Server.MapPath("ProgressReports/Rpt/TWS/rptTERM_REPORT_TWS_DUBAI_GRD01_06.rpt")
                            Else
                                .reportPath = Server.MapPath("ProgressReports/Rpt/TWS/rptTERM_REPORT_TWS_DUBAI_GRD01_06_2011.rpt")
                            End If
                        Case "07", "08"
                            .reportPath = Server.MapPath("ProgressReports/Rpt/TWS/rptTERM_REPORT_TWS_DUBAI_GRD07.rpt")
                        Case "09"
                            param("Max_Mark_UT") = "25"
                            param("Max_Mark_Exam") = "75"
                            .reportPath = Server.MapPath("ProgressReports/Rpt/TWS/rptTERM_REPORT_TWS_DUBAI_GRD09.rpt")
                        Case "10"
                            param("Max_Mark_UT") = "20"
                            param("Max_Mark_Exam") = "80"
                            .reportPath = Server.MapPath("ProgressReports/Rpt/TWS/rptTERM_REPORT_TWS_DUBAI_GRD09.rpt")
                        Case "11"
                            .reportPath = Server.MapPath("ProgressReports/Rpt/TWS/rptTERM_REPORT_TWS_DUBAI_GRD11.rpt")
                        Case "12", "13"
                            If Session("RPF_DESCR") = "TERM 2 REPORT" Then
                                .reportPath = Server.MapPath("ProgressReports/Rpt/TWS/rptTERM2_REPORT_TWS_DUBAI_GRD12_13.rpt")
                            Else
                                .reportPath = Server.MapPath("ProgressReports/Rpt/TWS/rptTERM_REPORT_TWS_DUBAI_GRD12_13.rpt")
                            End If
                    End Select
            End Select
            .reportParameters = param
        End With


        If Session("downloadreport") = "1" Then
            DownloadReports(rptClass)
        Else
            LoadReports(rptClass)
        End If
    End Sub


    Private Sub GenerateWSDReports(ByVal RSM_ID As String, ByVal RPF_ID As String, ByVal ACD_ID As String, ByVal STU_ID As String, ByVal BSU_ID As String, ByVal GRD_ID As String)
        Dim param As New Hashtable
        param.Add("@ACD_ID", ACD_ID)
        param.Add("@BSU_ID", BSU_ID)
        param.Add("@RSM_ID", RSM_ID)
        param.Add("@RPF_ID", RPF_ID)
        param.Add("@STU_ID", STU_ID)

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"

            With rptClass
                Select Case hfReportFormat.Value.ToString.ToUpper
                    Case "WSD_INTERIM_REPORT_APP"
                        param.Add("@bSLT", "false")
                        Select Case GRD_ID
                            Case "KG1", "KG2"
                                .Photos = GetPhotoClass(Session("STU_ID"))
                                .reportPath = Server.MapPath("ProgressReports/Rpt/WSD/rptWSD_PROGRESSREPORT_APP_FS1-FS2.rpt")
                        End Select
                    Case "WSD_FINAL_REPORT_APP"
                        param.Add("@bSLT", "false")
                        .Photos = GetPhotoClass(Session("STU_ID"))
                        .reportPath = Server.MapPath("ProgressReports/Rpt/WSD/rptWSD_FINALREPORT_APP_FS2.rpt")
                    Case "PROGRESS_REPORTS_01_05"
                        param.Add("accYear", Session("ReportYear_DESC"))
                        .reportPath = Server.MapPath("ProgressReports/Rpt/WSD/rptWSD_PROGRESSREPORT_01_06_2012-13.rpt")
                    Case "PROGRESS_REPORTS_2012"
                        param.Add("GRD_ID", GRD_ID)
                        param.Add("CAPTION", Session("RPF_DESCR"))
                        .reportPath = Server.MapPath("ProgressReports/Rpt/WSD/rptWSD_TERMREPORT_01_06_2012.rpt")
                    Case "PROGRESS_REPORTS_2016"
                        param.Add("@GRD_ID", GRD_ID)
                        param.Add("CAPTION", Session("RPF_DESCR"))
                        .reportPath = Server.MapPath("ProgressReports/Rpt/WSD/rptWSD_TERMREPORT_01_10.rpt")
                    Case "PROGRESS_REPORTS"
                        Select Case GRD_ID
                            Case "KG1", "KG2"
                                param.Add("CAPTION", Session("RPF_DESCR"))
                                param.Add("GRD_ID", GRD_ID)
                                .reportPath = Server.MapPath("ProgressReports/Rpt/WSD/rptWSD_PROGRESSREPORT_FS1_FS2.rpt")
                            Case "07", "08", "09", "10"
                                param.Add("accYear", Session("ReportYear_DESC"))
                                .reportPath = Server.MapPath("ProgressReports/Rpt/WSD/rptWSD_PROGRESSREPORT_06_10.rpt")
                            Case "06"
                                If ACD_ID = "807" Then
                                    param.Add("CAPTION", Session("RPF_DESCR"))
                                    param.Add("GRD_ID", GRD_ID)
                                    .reportPath = Server.MapPath("ProgressReports/Rpt/WSD/rptWSD_TERMREPORT_01_06.rpt")
                                Else
                                    param.Add("accYear", Session("ReportYear_DESC"))
                                    .reportPath = Server.MapPath("ProgressReports/Rpt/WSD/rptWSD_PROGRESSREPORT_06_10.rpt")
                                End If
                            Case Else
                                '  If ACD_ID = "807" Then
                                param.Add("CAPTION", Session("RPF_DESCR"))
                                param.Add("GRD_ID", GRD_ID)
                                .reportPath = Server.MapPath("ProgressReports/Rpt/WSD/rptWSD_TERMREPORT_01_06.rpt")
                                '  Else
                                '    param.Add("accYear", Session("ReportYear_DESC"))
                                '   .reportPath = Server.MapPath("ProgressReports/Rpt/WSD/rptWSD_FinalReport.rpt")
                                '  End If
                        End Select
                    Case Else
                        param.Add("accYear", Session("ReportYear_DESC"))
                        .reportPath = Server.MapPath("ProgressReports/Rpt/WSD/rptWSD_FinalReport.rpt")
                End Select
            End With

            .reportParameters = param
        End With
        Session("rptClass") = rptClass
        If Session("downloadreport") = "1" Then
            DownloadReports(rptClass)
        Else
            LoadReports(rptClass)
        End If
    End Sub

    Private Sub GenerateWSSReports(ByVal RSM_ID As String, ByVal RPF_ID As String, ByVal ACD_ID As String, ByVal STU_ID As String, ByVal BSU_ID As String, ByVal GRD_ID As String)
        Dim param As New Hashtable
        param.Add("@ACD_ID", ACD_ID)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RPF_ID", RPF_ID)
        param.Add("@STU_ID", STU_ID)
        param.Add("@RSM_ID", RSM_ID)
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            Select Case hfReportFormat.Value
                Case "FS_REPORTS"
                    param.Add("GRD_ID", GRD_ID)
                    param.Add("RPF_DESCR", Session("RPF_DESCR"))
                    .reportPath = Server.MapPath("ProgressReports/Rpt/WSS/rptWSS_KINDERGARTENPROGRESSREPORT_TERM1.rpt")
                Case "FS_REPORTS_TERM2"
                    .Photos = GetPhotoClass(STU_ID)
                    param.Add("GRD_ID", GRD_ID)
                    param.Add("RPF_DESCR", Session("RPF_DESCR"))
                    .reportPath = Server.MapPath("ProgressReports/Rpt/WSS/rptWSS_KINDERGARTENPROGRESSREPORT.rpt")
                Case "FS_REPORTS_TERM2_NEW"
                    .Photos = GetPhotoClass(STU_ID)
                    param.Add("GRD_ID", GRD_ID)
                    param.Add("RPF_DESCR", Session("RPF_DESCR"))
                    .reportPath = Server.MapPath("ProgressReports/Rpt/WSS/rptWSS_KINDERGARTENPROGRESSREPORT_KG2_TERM2.rpt")
                Case "FS2_FINALREPORT"
                    .Photos = GetPhotoClass(STU_ID)
                    param.Add("GRD_ID", GRD_ID)
                    param.Add("RPF_DESCR", Session("RPF_DESCR"))
                    .reportPath = Server.MapPath("ProgressReports/Rpt/WSS/rptWSS_KINDERGARTENFINALREPORT.rpt")
                Case "FINAL_REPORT_1-9"
                    param.Add("@GRD_ID", GRD_ID)
                    If ((GRD_ID = "09") Or (GRD_ID = "10")) Then
                        .reportPath = Server.MapPath("ProgressReports/Rpt/WSS/rptFINAL_REPORT_WSS_MINISTRY_Grade09_13-14.rpt")
                    Else
                        .reportPath = Server.MapPath("ProgressReports/Rpt/WSS/rptFINAL_REPORT_WSS_MINISTRY_13-14.rpt")
                    End If

                Case "PROGRESS_REPORTS_APP"
                    param.Add("@bSLT", "false")
                    .reportPath = Server.MapPath("ProgressReports/Rpt/WSS/rptWSS_INTERIMREPORT_APP_1-8.rpt")
                Case "FS_REPORTS_TERM2"
                    .Photos = GetPhotoClass(STU_ID)
                    param.Add("GRD_ID", GRD_ID)
                    param.Add("RPF_DESCR", Session("RPF_DESCR"))
                    .reportPath = Server.MapPath("ProgressReports/Rpt/WSS/rptWSS_KINDERGARTENPROGRESSREPORT.rpt")
                Case "PROGRESS_REPORTS"
                    param.Add("accYear", Session("ReportYear_DESC"))
                    param.Add("GRD_ID", GRD_ID)
                    If Session("ReportYear_DESC") = "2013-2013" Then
                        .reportPath = Server.MapPath("ProgressReports/Rpt/WSS/rptWSS_PROGRESSREPORT_02_06_2012-13.rpt")
                    Else
                        .reportPath = Server.MapPath("ProgressReports/Rpt/WSS/rptWSS_PROGRESSREPORT_02_06.rpt")
                    End If
                Case "PROGRESS_REPORTS_APP_09_13"
                    param.Add("@bSLT", "false")
                    param.Add("RPT_CAPTION", Session("RPF_DESCR"))
                    param.Add("ATTEND_TOTAL", "0")
                    .reportPath = Server.MapPath("ProgressReports/Rpt/WSS/rptWSS_INTERIMREPORT_9-10.rpt")
                Case "PROGRESS_REPORTS_APP"
                    param.Add("@bSLT", "false")
                    .reportPath = Server.MapPath("ProgressReports/Rpt/WSS/rptWSS_INTERIMREPORT_APP_1-8.rpt")
                Case "PROGRESS_REPORTS_1-5"
                    .reportPath = Server.MapPath("ProgressReports/Rpt/WSS/rptWSS_PROGRESSREPORT_01_05.rpt")
                Case "PROGRESS_REPORTS_6-9"
                    param.Add("accYear", Session("ReportYear_DESC"))
                    param.Add("GRD_ID", GRD_ID)
                    .reportPath = Server.MapPath("ProgressReports/Rpt/WSS/rptWSS_PROGRESSREPORT_06_09.rpt")
            End Select
            .reportParameters = param
        End With
        Session("rptClass") = rptClass
        If Session("downloadreport") = "1" Then
            DownloadReports(rptClass)
        Else
            LoadReports(rptClass)
        End If
    End Sub


    Private Sub GenerateWSRReports(ByVal RSM_ID As String, ByVal RPF_ID As String, ByVal ACD_ID As String, ByVal STU_ID As String, ByVal BSU_ID As String, ByVal GRD_ID As String)
        Dim param As New Hashtable
        param.Add("@ACD_ID", ACD_ID)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RPF_ID", RPF_ID)
        param.Add("@STU_ID", STU_ID)
        param.Add("@RSM_ID", RSM_ID)
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            Select Case hfReportFormat.Value
                Case "FS1_FINALREPORT"
                    .Photos = GetPhotoClass(STU_ID)
                    param.Add("GRD_ID", GRD_ID)
                    param.Add("RPF_DESCR", Session("RPF_DESCR"))
                    .reportPath = Server.MapPath("ProgressReports/Rpt/WSR/rptWSR_FS1_FINALREPORT.rpt")
                Case "FS2_FINALREPORT"
                    .Photos = GetPhotoClass(STU_ID)
                    param.Add("GRD_ID", GRD_ID)
                    param.Add("RPF_DESCR", Session("RPF_DESCR"))
                    .reportPath = Server.MapPath("ProgressReports/Rpt/WSR/rptWSR_FS2FINALREPORT.rpt")
                Case "FS_REPORTS_2013-14"
                    param.Add("GRD_ID", GRD_ID)
                    param.Add("RPF_DESCR", Session("RPF_DESCR"))
                    .reportPath = Server.MapPath("ProgressReports/Rpt/WSR/rptWSR_KINDERGARTENPROGRESSREPORT_2013-14.rpt")
                Case "FS_REPORTS"
                    param.Add("GRD_ID", GRD_ID)
                    param.Add("RPF_DESCR", Session("RPF_DESCR"))
                    .reportPath = Server.MapPath("ProgressReports/Rpt/WSR/rptWSR_KINDERGARTENPROGRESSREPORT.rpt")
                Case "PROGRESS_REPORTS"
                    param.Add("accYear", Session("ReportYear_DESC"))
                    param.Add("GRD_ID", GRD_ID)
                    If Session("ReportYear_DESC") = "2014-2015" Then
                        If Session("RPF_DESCR") = "FINAL REPORT" Then
                            .reportPath = Server.MapPath("ProgressReports/Rpt/WSR/rptWSR_PROGRESSREPORT_01_07_2014-15.rpt")
                        Else
                            .reportPath = Server.MapPath("ProgressReports/Rpt/WSR/rptWSR_PROGRESSREPORT_01_07.rpt")
                        End If
                    ElseIf Session("ReportYear_DESC") = "2015-2016" Then
                        .reportPath = Server.MapPath("ProgressReports/Rpt/WSR/rptWSR_PROGRESSREPORT_01_07_2015-16.rpt")
                    ElseIf Session("ReportYear_DESC") = "2016-2017" Then
                        .reportPath = Server.MapPath("ProgressReports/Rpt/WSR/rptWSR_PROGRESSREPORT_01_07_2016-17.rpt")
                    Else
                        .reportPath = Server.MapPath("ProgressReports/Rpt/WSR/rptWSR_PROGRESSREPORT_01_07.rpt")
                    End If
                Case "PROGRESS_REPORTS_18"
                    param.Add("accYear", Session("ReportYear_DESC"))
                    param.Add("GRD_ID", GRD_ID)
                    .reportPath = Server.MapPath("ProgressReports/Rpt/WSR/rptWSR_PROGRESSREPORT_01_07_2016-17.rpt")
                Case "INTERIM_REPORTS"
                    param.Add("@bSLT", "false")
                    .reportPath = Server.MapPath("ProgressReports/Rpt/WSR/rptWSR_INTERIMREPORT_APP_1-8.rpt")
            End Select
            .reportParameters = param
        End With
        Session("rptClass") = rptClass
        If Session("downloadreport") = "1" Then
            DownloadReports(rptClass)
        Else
            LoadReports(rptClass)
        End If
    End Sub

    Private Function GetAllCBSEBusinessUnit(ByVal BSU_ID As String) As String
        Dim strQuery As String = " SELECT BSU_ID FROM BUSINESSUNIT_M WHERE BSU_CLM_ID = 1 AND BSU_ID = '" & BSU_ID & "'"
        Dim vBSU_ID As String = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, strQuery)
        Return vBSU_ID
    End Function

    Private Sub GenerateAOLReports()
        If Not Session("rptClass") Is Nothing Then
            Session("bAOLReport") = Nothing
            LoadReports(Session("rptClass"))
        End If
    End Sub

    Private Sub GenerateFormativeAssessmentReport(ByVal RSM_ID As String, ByVal RPF_ID As String, ByVal ACD_ID As String, ByVal STU_ID As String, ByVal BSU_ID As String, ByVal GRD_ID As String)
        Dim param As New Hashtable
        param.Add("@ACD_ID", ACD_ID)
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RSM_ID", RSM_ID)
        param.Add("@RPF_ID", RPF_ID)
        param.Add("@STU_ID", STU_ID)
        param.Add("RPT_CAPTION", Session("RPF_DESCR"))
        Select Case Session("RPF_DESCR")
            Case "FORMATIVE ASSESSMENT 1"
                param.Add("RPT_ASSESSMENT_HEADER", "ASSESSMENT")
            Case "FORMATIVE ASSESSMENT 2"
                param.Add("RPT_ASSESSMENT_HEADER", "ASSESSMENT")
        End Select

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            '.Photos = GetPhotoClass()
            .reportPath = Server.MapPath("ProgressReports/Rpt/rptCBSE_FORMATIVE_ASSESSMENT_REPORT_03.rpt")
        End With
        If Session("downloadreport") = "1" Then
            DownloadReports(rptClass)
        Else
            LoadReports(rptClass)
        End If
    End Sub

    Function GetParentDisplayErrorMsg(ByVal bTransport As Boolean) As String
        Dim str_conn As String
        If bTransport Then
            str_conn = ConnectionManger.GetOASISTRANSPORTConnectionString
        Else
            str_conn = ConnectionManger.GetOASIS_FEESConnectionString
        End If
        Dim str_query As String = "SELECT ISNULL(BSU_RPTCRD_PARENT_ERR_MSG,'') ERR_MSG FROM BUSINESSUNIT_M WHERE BSU_ID = '" & Session("STU_BSU_ID") & "'"
        Return SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
    End Function

    Function HasPendingFee() As Boolean
        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString

        Dim pendingDate As String
        If Session("HFrpf_date") Is Nothing Then
            Return False
        End If

        If Date.Parse(Session("HFrpf_date")) > Now Then
            pendingDate = Format(Date.Parse(Date.Now), "dd/MMM/yyyy")
        Else
            pendingDate = Format(Date.Parse(Session("HFrpf_date")), "dd/MMM/yyyy")
        End If

        Dim str_query As String = "exec [FEES].[F_GETFEEOUTSTANDING_REPORTCARD] @CutOffDt='" + pendingDate + "',@BSU_ID='" & Session("SBSUID") & "',@Dt='" + Format(Now.Date, "dd/MMM/yyyy") + "',@STU_ID=" + Session("STU_ID") + ",@RSM_ID= " + Session("HFrsm_id") + ", @RPF_ID = " + Session("HFrpf_id")

        Dim vPendingTutionFee As Boolean = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)


        'Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        'Dim fee As Double = 0
        'While reader.Read
        '    fee += reader.GetValue(0)
        'End While
        'reader.Close()

        'str_conn = ConnectionManger.GetOASISTRANSPORTConnectionString

        'reader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        'While reader.Read
        '    fee += reader.GetValue(0)
        'End While
        'reader.Close()

        If vPendingTutionFee Then
            Return True
        Else
            Return False
        End If

    End Function
    Function HasPendingFee_TPT() As Boolean
        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString

        Dim pendingDate As String
        If Session("HFrpf_date") Is Nothing Then
            Return False
        End If

        If Date.Parse(Session("HFrpf_date")) > Now Then
            pendingDate = Format(Date.Parse(Date.Now), "dd/MMM/yyyy")
        Else
            pendingDate = Format(Date.Parse(Session("HFrpf_date")), "dd/MMM/yyyy")
        End If

        Dim str_query As String = "exec [FEES].[F_GETFEEOUTSTANDING_REPORTCARD] @CutOffDt='" + pendingDate + "',@BSU_ID='" & Session("SBSUID") & "',@Dt='" + Format(Now.Date, "dd/MMM/yyyy") + "',@STU_ID=" + Session("STU_ID") + ",@RSM_ID= " + Session("HFrsm_id") + ", @RPF_ID = " + Session("HFrpf_id")

        'Dim vPendingTutionFee As Boolean = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)

        str_conn = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim vPendingTransportFee As Boolean = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        'Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        'Dim fee As Double = 0
        'While reader.Read
        '    fee += reader.GetValue(0)
        'End While
        'reader.Close()

        'str_conn = ConnectionManger.GetOASISTRANSPORTConnectionString

        'reader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        'While reader.Read
        '    fee += reader.GetValue(0)
        'End While
        'reader.Close()

        If vPendingTransportFee Then
            Return True
        Else
            Return False
        End If

    End Function

    Private Sub GenerateCISProgressReport(ByVal ACD_ID As String, ByVal BSU_ID As String, ByVal RSM_ID As String, ByVal RPF_ID As String, ByVal STU_ID As String)
        Dim param As New Hashtable
        param.Add("@ACD_ID", ACD_ID)
        param.Add("@BSU_ID", BSU_ID)
        param.Add("@RSM_ID", RSM_ID)
        param.Add("@RPF_ID", RPF_ID)
        param.Add("@STU_ID", STU_ID)
        'Session("studentphotopath") = TransferPath(h_STU_IDs.Value.Replace("___", "|"))
        'param.Add("UserName", Session("sUsr_name"))
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            '.Photos = GetPhotoClass()

            Select Case hfReportFormat.Value
                Case "CIS_TERMREPORT_10_13"
                    param.Add("GRD_ID", Session("STP_GRD_ID"))
                    .reportPath = Server.MapPath("ProgressReports/Rpt/rptCIS_TERM_REPORT_10-13.rpt")
                Case "CIS_MOCKREPORT_11_13"
                    .reportPath = Server.MapPath("ProgressReports/Rpt/rptCIS_INTERIMREPORTGRD9-10_2015-16.rpt")
                Case "CIS_INTERIM_REPORT_APP_KS1-3"
                    param.Add("@bSLT", "false")
                    .reportPath = Server.MapPath("ProgressReports/Rpt/rptCIS_INTERIMREPORT_APP_1-8.rpt")
                Case "CIS_INTERIM_REPORT_9_12_2012"
                    If Session("ReportYear_DESC") <> "2012-2013" Then
                        .reportPath = Server.MapPath("ProgressReports/Rpt/rptCIS_INTERIMREPORTGRD9-13(2013-14).rpt")
                    Else
                        .reportPath = Server.MapPath("ProgressReports/Rpt/rptCIS_INTERIMREPORTGRD9-13(2012-13).rpt")
                    End If
                Case "CIS_PROGRESS_REPORT_APP_FS1_FS2"
                    param.Add("@bSLT", "false")
                    .Photos = GetPhotoClass(Session("STU_ID"))
                    .reportPath = Server.MapPath("ProgressReports/Rpt/rptCIS_PROGRESSREPORT_APP_FS1-FS2.rpt")
                Case "CIS_INTERIM_REPORT_11_2012"
                    If Session("RPF_DESCR").ToString.ToUpper = "OCT-SENIOR SCHOOL 1ST INTERIM REPORT" Then
                        .reportPath = Server.MapPath("ProgressReports/Rpt/rptCIS_INTERIMREPORTGRD9-13(2012-13).rpt")
                    Else
                        .reportPath = Server.MapPath("ProgressReports/Rpt/rptCIS_INTERIMREPORTGRD11(2012-13).rpt")

                    End If
                Case "CIS_FINAL_REPORT_APP_KS1-3"
                    param.Add("@bSLT", "false")
                    .reportPath = Server.MapPath("ProgressReports/Rpt/rptCIS_FINALREPORT_APP_1-8.rpt")
                Case "CIS_BTECREPORTS"
                    .reportPath = Server.MapPath("ProgressReports/Rpt/rptCIS_INTERIMREPORTGRD9-13_BTEC.rpt")
                Case Else
                    Select Case GetReportType(RSM_ID)
                        Case "CIS_MONTHLY_REPORT" 'Monthly Progress Report
                            .reportPath = Server.MapPath("ProgressReports/Rpt/rptWIN_PROGRESS_REPORT_GRD07.rpt")
                        Case "CIS_FINAL_REPORT" ' Final Report
                            Select Case Session("STP_GRD_ID")
                                Case "KG1", "KG2"
                                    .reportPath = Server.MapPath("ProgressReports/Rpt/rptCIS_FINAL_REPORT_FS1_FS2.rpt")
                                Case "01", "02", "03", "04", "05", "06", "07", "08"
                                    If ACD_ID = 590 Or ACD_ID = 568 Then
                                        ' param.Add("feepending", hFeePending.Value)
                                        .reportPath = Server.MapPath("ProgressReports/Rpt/rptCIS_PROGRESS_REPORT.rpt")
                                    Else
                                        param.Add("@bSLT", "false")
                                        .reportPath = Server.MapPath("ProgressReports/Rpt/rptCIS_FINALREPORT_APP_1-8.rpt")
                                    End If
                                Case Else
                                    ' param.Add("feepending", hFeePending.Value)
                                    .reportPath = Server.MapPath("ProgressReports/Rpt/rptCIS_PROGRESS_REPORT.rpt")
                            End Select
                        Case "CIS_PROGRESS_REPORT_FS1_FS2"
                            If ACD_ID = 590 Or ACD_ID = 568 Then
                                .reportPath = Server.MapPath("ProgressReports/Rpt/rptCIS_PROGRESS_REPORT_FS1_FS2_2010.rpt")
                            Else
                                param.Add("GRD_ID", Session("STP_GRD_ID"))
                                .reportPath = Server.MapPath("ProgressReports/Rpt/rptCIS_PROGRESS_REPORT_FS1_FS2.rpt")
                            End If
                        Case "CIS_INTERIM_REPORT_1_8"
                            If Session("RPF_DESCR").ToUpper = "MARCH INTERIM REPORT" And Session("STP_GRD_ID") = "08" Then
                                .reportPath = Server.MapPath("ProgressReports/Rpt/rptCIS_INTERIMREPORTGRD-8_mock.rpt")
                            Else
                                .reportPath = Server.MapPath("ProgressReports/Rpt/rptCIS_INTERIMREPORTGRD1-8.rpt")
                            End If
                        Case "CIS_INTERIM_REPORT_9_12"
                            .reportPath = Server.MapPath("ProgressReports/Rpt/rptCIS_INTERIMREPORTGRD9-13.rpt")
                        Case "CIS_INTERIM_REPORT_13"
                            .reportPath = Server.MapPath("ProgressReports/Rpt/rptCIS_INTERIMREPORTGRD13.rpt")
                        Case "CIS_INTERIM_REPORT_9_10_new"
                            .reportPath = Server.MapPath("ProgressReports/Rpt/rptCIS_INTERIMREPORTGRD9-10_new.rpt")
                        Case "CIS_INTERIM_REPORT_13"
                            .reportPath = Server.MapPath("ProgressReports/Rpt/rptCIS_INTERIMREPORTGRD13.rpt")
                        Case "CIS_INTERIM_REPORT_11_13_new"
                            .reportPath = Server.MapPath("ProgressReports/Rpt/rptCIS_INTERIMREPORTGRD11-13_new.rpt")

                    End Select
            End Select
        End With
        Session("rptClass") = rptClass
        If Session("downloadreport") = "1" Then
            DownloadReports(rptClass)
        Else
            LoadReports(rptClass)
        End If
        'If ViewState("MainMnu_code") = "StudentProfile" Then
        '    Response.Redirect("~/Reports/ASPX Report/rptReportViewer_PRINCI.aspx")
        'Else
        '    Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        'End If

    End Sub

    Public Function GetReportType(ByVal pRSM_ID As String) As String
        Dim strGRD_ID As String = Session("STP_GRD_ID")
        Dim strACD_ID As String = Session("STU_ACD_ID")
        Dim strBSSU_ID As String = Session("STU_BSU_ID")
        Dim strRSM_ID As String = pRSM_ID
        Dim str_sql As String = " SELECT dbo.GetReportType('" & strBSSU_ID & "', " & strACD_ID & ", '" & strGRD_ID & "', " & strRSM_ID & " )"
        Return SqlHelper.ExecuteScalar(ConnectionManger.GetOASIS_CURRICULUMConnectionString, CommandType.Text, str_sql)
    End Function

    Private Sub GenerateWINReport(ByVal RSM_ID As String, ByVal RPF_ID As String, ByVal ACD_ID As String, ByVal STU_ID As String, ByVal BSU_ID As String, ByVal GRD_ID As String)
        Dim param As New Hashtable
        param.Add("@ACD_ID", ACD_ID)
        param.Add("@BSU_ID", BSU_ID)
        param.Add("@RSM_ID", RSM_ID)
        param.Add("@RPF_ID", RPF_ID)
        param.Add("@STU_ID", STU_ID)
        If ACD_ID = 593 Then
            param.Add("accYear", "2010-2011")
        Else
            param.Add("accYear", Session("ReportYear_DESC"))
        End If

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            Select Case hfReportFormat.Value
                Case "WIN_REPORTS_01_10"
                    .reportPath = Server.MapPath("ProgressReports/Rpt/rptWIN_PROGRESSREPORT_01_10_2013-14.rpt")
                Case "WIN_REPORTS_10_13"
                    .reportPath = Server.MapPath("ProgressReports/Rpt/rptWIN_PROGRESSREPORT_10_13_2014-15.rpt")
                Case Else

                    Select Case GRD_ID
                        Case "KG1"
                            .Photos = GetPhotoClass(STU_ID)
                            param.Add("@bSLT", "false")
                            .reportPath = Server.MapPath("ProgressReports/Rpt/rptWIN_PROGRESSREPORT_APP_FS1_FS2.rpt")
                        Case "KG2"
                            .Photos = GetPhotoClass(STU_ID)
                            param.Add("@bSLT", "false")
                            If Session("RPF_DESCR").ToUpper.Contains("FINAL REPORT") Then
                                .reportPath = Server.MapPath("ProgressReports/Rpt/rptWIN_PROGRESSREPORT_FS2_TERM3.rpt")
                            Else
                                .reportPath = Server.MapPath("ProgressReports/Rpt/rptWIN_PROGRESSREPORT_APP_FS1_FS2.rpt")
                            End If
                        Case "01", "02", "03", "04", "05", "06"
                            If RPF_ID = 4655 Then
                                .reportPath = Server.MapPath("ProgressReports/Rpt/rptWIN_PROGRESSREPORT_01_10_2013-14.rpt")
                            ElseIf ACD_ID = 593 Or ACD_ID = 616 Then
                                .reportPath = Server.MapPath("ProgressReports/Rpt/rptWIN_PROGRESSREPORT_01_06.rpt")
                            Else
                                .reportPath = Server.MapPath("ProgressReports/Rpt/rptWIN_PROGRESSREPORT_01_06_2012-13.rpt")
                            End If
                        Case "07", "08", "09"
                            If RPF_ID = 3672 Then
                                .reportPath = Server.MapPath("ProgressReports/Rpt/rptWIN_PROGRESSREPORT_01_10_2013-14.rpt")
                            Else
                                .reportPath = Server.MapPath("ProgressReports/Rpt/rptWIN_PROGRESSREPORT_07_10.rpt")
                            End If
                        Case "10"
                            If RPF_ID = 3672 Then
                                .reportPath = Server.MapPath("ProgressReports/Rpt/rptWIN_PROGRESSREPORT_10_2013-14.rpt")
                            ElseIf ACD_ID = 593 Or ACD_ID = 616 Then
                                .reportPath = Server.MapPath("ProgressReports/Rpt/rptWIN_PROGRESSREPORT_07_10.rpt")
                            Else
                                .reportPath = Server.MapPath("ProgressReports/Rpt/rptWIN_PROGRESSREPORT_10.rpt")
                            End If
                        Case "11", "12", "13"
                            If ACD_ID = 593 Then
                                .reportPath = Server.MapPath("ProgressReports/Rpt/rptWIN_PROGRESSREPORT_11_13_2011.rpt")
                            Else
                                .reportPath = Server.MapPath("ProgressReports/Rpt/rptWIN_PROGRESSREPORT_11_13.rpt")
                            End If

                    End Select
            End Select
            .reportParameters = param
        End With
        Session("rptClass") = rptClass
        'If ViewState("MainMnu_code") = "StudentProfile" Then
        '    Response.Redirect("~/Reports/ASPX Report/rptReportViewer_PRINCI.aspx")
        'Else
        '    Response.Redirect("~/Reports/ASPX Report/rptReportViewer.aspx")
        'End If
        LoadReports(rptClass)
    End Sub

    Private Sub GenerateTermlyReport_OOEHS_Dubai(ByVal RSM_ID As String, ByVal RPF_ID As String, ByVal ACD_ID As String, ByVal STU_ID As String, ByVal BSU_ID As String, ByVal GRD_ID As String)
        Dim param As New Hashtable
        param.Add("@ACD_ID", ACD_ID)
        param.Add("@BSU_ID", BSU_ID)
        param.Add("@RSM_ID", RSM_ID)
        param.Add("@RPF_ID", RPF_ID)
        param.Add("@STU_ID", STU_ID)
        param.Add("RPT_CAPTION", Session("RPF_DESCR"))
        'param.Add("ATTEND_TOTAL", GetTotalGradeAttendance(ACD_ID, GRD_ID, RPF_ID))
        'param.Add("TOT_ATTEND_VAL", "75")
        'Session("studentphotopath") = TransferPath(h_STU_IDs.Value.Replace("___", "|"))
        'param.Add("UserName", Session("sUsr_name"))
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            '.Photos = GetPhotoClass()
            If Not IsFinalReport(RSM_ID) Then
                param.Add("ATT_ASON_DATE", GetAsOnDateForAttendance(ACD_ID, GRD_ID, RPF_ID))
                Select Case GRD_ID
                    Case "01", "02"
                        param.Add("ASSIGNMENT1_HEADING", "Assessment 1")
                        param.Add("ASSIGNMENT2_HEADING", "Assessment 2")
                        .reportPath = Server.MapPath("ProgressReports/Rpt/rptTERM_REPORT_OOEHS_DUBAI_GRD01_02.rpt")
                    Case "03", "04"
                        param.Add("ASSIGNMENT1_HEADING", "Assessment 1")
                        param.Add("ASSIGNMENT2_HEADING", "Assessment 2")
                        .reportPath = Server.MapPath("ProgressReports/Rpt/rptTERM_REPORT_OOEHS_DUBAI_GRD03_04.rpt")
                    Case "05", "06", "07", "08"
                        param.Add("UNIT_TEST_HEADING", "Unit Test 1")
                        .reportPath = Server.MapPath("ProgressReports/Rpt/rptTERM_REPORT_OOEHS_DUBAI_GRD05_08.rpt")
                    Case "09", "10"
                        param.Add("UNIT_TEST_HEADING", "Unit Test 1")
                        .reportPath = Server.MapPath("ProgressReports/Rpt/rptTERM_REPORT_OOEHS_DUBAI_GRD09_10.rpt")
                    Case "11", "12"
                        param.Add("UNIT_TEST_HEADING", "Unit Test 1")
                        If (ACD_ID = 879 Or ACD_ID = 965) And (Session("RPF_DESCR") = "TERM 1 REPORT" Or Session("RPF_DESCR") = "TERM 2 REPORT") Then
                            .reportPath = Server.MapPath("ProgressReports/Rpt/CBSE/rptTERM_REPORT_OOEHS_DUBAI_GRD11_12_NEW.rpt")
                            'ElseIf ACD_ID And Session("RPF_DESCR") <> "MID TERM REPORT" Then
                            '    .reportPath = Server.MapPath("ProgressReports/Rpt/CBSE/rptTERM_REPORT_OOEHS_DUBAI_GRD11_12_NEW.rpt")
                            'Else
                            '    .reportPath = Server.MapPath("ProgressReports/Rpt/rptTERM_REPORT_OOEHS_DUBAI_GRD11_12.rpt")






                        Else
                            If ((Session("SBSUID") = "121013") And (ACD_ID = "1062")) Then
                                If Session("RPF_DESCR").contains("TERM 2") And (GRD_ID = "12") Then

                                    .reportPath = Server.MapPath("ProgressReports/Rpt/rptTERM_REPORT_OOEHS_DUBAI_GRD11_12_2015-16_TERM2.rpt")
                                Else


                                    If GRD_ID = "11" Then
                                        If Session("RPF_DESCR").contains("TERM 2") Then
                                            .reportPath = Server.MapPath("ProgressReports/Rpt/rptTERM_REPORT_OOEHS_DUBAI_GRD11_2015-16_TERM2.rpt")
                                        Else
                                            .reportPath = Server.MapPath("ProgressReports/Rpt/rptTERM_REPORT_OOEHS_DUBAI_GRD11_12_2015-16.rpt")
                                        End If

                                    Else
                                        .reportPath = Server.MapPath("ProgressReports/Rpt/rptTERM_REPORT_OOEHS_DUBAI_GRD11_12_2015-16_GRD12.rpt")
                                    End If
                                End If
                            Else
                                If ((ACD_ID = "1143") Or (ACD_ID = "1272") Or (ACD_ID = "1403")) Then
                                    If Session("RPF_DESCR") = "MID TERM REPORT" Then
                                        .reportPath = Server.MapPath("ProgressReports/Rpt/CBSE/rptTERM_REPORT_OOEHS_DUBAI_GRD11_12_NEW.rpt")
                                    ElseIf Session("RPF_DESCR") = "TERM 1 REPORT" Then
                                        .reportPath = Server.MapPath("ProgressReports/Rpt/CBSE/rptTERM_REPORT_OOEHS_DUBAI_GRD11_12_TERM1.rpt")
                                    ElseIf Session("RPF_DESCR") = "TERM 2 REPORT" Then
                                        If GRD_ID = "11" Then
                                            .reportPath = Server.MapPath("ProgressReports/Rpt/rptTERM_REPORT_OOEHS_DUBAI_GRD11_2015-16_TERM2.rpt")
                                        Else
                                            .reportPath = Server.MapPath("ProgressReports/Rpt/rptTERM_REPORT_OOEHS_DUBAI_GRD11_2015-16_TERM2.rpt")
                                        End If
                                    Else
                                        If GRD_ID = "11" Then
                                            If Session("RPF_DESCR").Contains("TERM 2") Then
                                                .reportPath = Server.MapPath("ProgressReports/Rpt/rptTERM_REPORT_OOEHS_DUBAI_GRD11_12_2015-16_GRD12.rpt")
                                            Else
                                                .reportPath = Server.MapPath("ProgressReports/Rpt/rptTERM_REPORT_OOEHS_DUBAI_GRD11_12_2015-16.rpt")
                                            End If
                                        Else
                                            .reportPath = Server.MapPath("ProgressReports/Rpt/rptTERM_REPORT_OOEHS_DUBAI_GRD11_12_2015-16_GRD12.rpt")
                                        End If
                                    End If

                                Else
                                    .reportPath = Server.MapPath("ProgressReports/Rpt/rptTERM_REPORT_OOEHS_DUBAI_GRD11_12.rpt")
                                End If
                            End If
                        End If

                End Select
            Else
                Select Case GRD_ID
                    Case "01", "02"
                        'param.Add("ASSIGNMENT1_HEADING", "Assessment 1")
                        'param.Add("ASSIGNMENT2_HEADING", "Assessment 2")
                        .reportPath = Server.MapPath("ProgressReports/Rpt/rptCUMULATIVE_REPORT_OOEHS_DUBAI_GRD01_02.rpt")
                    Case "03", "04"
                        'param.Add("ASSIGNMENT1_HEADING", "Assessment 1")
                        'param.Add("ASSIGNMENT2_HEADING", "Assessment 2")
                        .reportPath = Server.MapPath("ProgressReports/Rpt/rptCUMULATIVE_REPORT_OOEHS_DUBAI_GRD03_04.rpt")
                    Case "05", "06", "07", "08"
                        param.Add("TERM1_HEADING", "Term I 20%")
                        param.Add("TERM2_HEADING", "Term II 30%")
                        param.Add("TERM3_HEADING", "Term III 50%")
                        .reportPath = Server.MapPath("ProgressReports/Rpt/rptCUMULATIVE_REPORT_OOEHS_DUBAI_GRD05_08.rpt")
                    Case "09"
                        param.Add("TERM1_HEADING", "Term I 20%")
                        param.Add("TERM2_HEADING", "Term II 20%")
                        param.Add("TERM3_HEADING1", "FA 20%")
                        param.Add("TERM3_HEADING2", "SA 40%")
                        .reportPath = Server.MapPath("ProgressReports/Rpt/rptCUMULATIVE_REPORT_OOEHS_DUBAI_GRD09.rpt")
                    Case "10", "11", "12"
                        If ((GRD_ID = "11" And ACD_ID = "1143")) Then
                            param.Add("TERM1_HEADING", "Term I 20%")
                            param.Add("TERM2_HEADING", "Term II 20%")
                            param.Add("TERM3_HEADING", "Term III 50%")
                            param.Add("TEST_HEADING", "TEST 10%")
                            .reportPath = Server.MapPath("ProgressReports/Rpt/rptCUMULATIVE_REPORT_OOEHS_DUBAI_GRD11_2016-17.rpt")

                        Else
                            param.Add("TERM1_HEADING", "Term I 20%")
                            param.Add("TERM2_HEADING", "Term II 20%")
                            param.Add("TERM3_HEADING", "Term III 50%")
                            param.Add("TEST_HEADING", "TEST 10%")
                            .reportPath = Server.MapPath("ProgressReports/Rpt/rptCUMULATIVE_REPORT_OOEHS_DUBAI_GRD10_12.rpt")
                        End If
                End Select
            End If
            '.reportPath = Server.MapPath("ProgressReports/Rpt/rptCUMULATIVE_REPORT_OOEHS_DUBAI_GRD09.rpt")
        End With
        ' Session("rptClass") = rptClass
        '      Response.Redirect("../Reports/ASPX Report/rptReportViewerModel.aspx")
        If Session("downloadreport") = "1" Then
            DownloadReports(rptClass)
        Else
            LoadReports(rptClass)
        End If

        '    h_print.Value = "print"
    End Sub

    Function IsFinalReport(ByVal vRSM_ID As String) As Boolean
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT ISNULL(RSM_bFINALREPORT,'FALSE') FROM RPT.REPORT_SETUP_M " _
                                 & " WHERE RSM_ID='" + vRSM_ID + "'"
        Return SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query).ToString.ToLower
    End Function

    Sub LoadReports(ByVal rptClass)
        Dim rs As New ReportDocument
        Try
            Dim iRpt As New DictionaryEntry
            Dim i As Integer
            Dim newWindow As String

            Session("HFrpf_id") = Nothing
            Session("HFrsm_id") = Nothing
            Dim rptStr As String = ""


            '  UtilityObj.Errorlog("Loadreport", "Rajesh")

            Dim crParameterDiscreteValue As ParameterDiscreteValue
            Dim crParameterFieldDefinitions As ParameterFieldDefinitions
            Dim crParameterFieldLocation As ParameterFieldDefinition
            Dim crParameterValues As ParameterValues

            With rptClass





                'C:\Application\ParLogin\ParentLogin\ProgressReports\Rpt\rptTERM_REPORT_OOEHS_DUBAI_GRD05_08.rpt
                rs.Load(.reportPath)

                UtilityObj.Errorlog(.reportPath, "Rajesh")

                Dim myConnectionInfo As ConnectionInfo = New ConnectionInfo()
                myConnectionInfo.ServerName = .crInstanceName
                myConnectionInfo.DatabaseName = .crDatabase
                myConnectionInfo.UserID = .crUser
                myConnectionInfo.Password = .crPassword

                ' UtilityObj.Errorlog("Connection", "Rajesh")

                SetDBLogonForSubreports(myConnectionInfo, rs, .reportParameters)
                SetDBLogonForReport(myConnectionInfo, rs, .reportParameters)

                ' UtilityObj.Errorlog("Logging", "Rajesh")
                'If .subReportCount <> 0 Then
                '    For i = 0 To .subReportCount - 1
                '       rs.Subreports(i).VerifyDatabase()
                '    Next
                'End If


                crParameterFieldDefinitions = rs.DataDefinition.ParameterFields
                If .reportParameters.Count <> 0 Then
                    For Each iRpt In .reportParameters
                        crParameterFieldLocation = crParameterFieldDefinitions.Item(iRpt.Key.ToString)
                        crParameterValues = crParameterFieldLocation.CurrentValues
                        crParameterDiscreteValue = New CrystalDecisions.Shared.ParameterDiscreteValue
                        crParameterDiscreteValue.Value = iRpt.Value
                        crParameterValues.Add(crParameterDiscreteValue)
                        crParameterFieldLocation.ApplyCurrentValues(crParameterValues)
                    Next
                End If

                ' rs.VerifyDatabase()
                '  UtilityObj.Errorlog("Verifying", "Rajesh")

                If .selectionFormula <> "" Then
                    rs.RecordSelectionFormula = .selectionFormula
                End If
                ' Dim rs1 As New ReportDocument
                ' rs1 =rs


                'Response.ClearContent()
                'Response.ClearHeaders()
                'Response.ContentType = "application/pdf"
                'rs.ReportDocument.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, False, "Report")
                Try
                    DisplayReport(rs, CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat)
                Catch ee As Exception
                End Try
                'rs.ReportDocument.Close()
                'rs.Dispose()
                'Response.Flush()
                'Response.Close()
            End With
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "Report Viewer")
        Finally
            Try
                For Each table As Table In rs.Database.Tables
                    table.Dispose()
                Next
                rs.Database.Dispose()
            Catch ex As Exception
                UtilityObj.Errorlog("Online rs.database.dispose--", ex.Message)
            End Try

            rs.Close()
            rs.Dispose()
            'Response.Flush()
            ' Response.Close()
        End Try
        'GC.Collect()
    End Sub

    'Protected Sub exportReport(ByVal selectedReport As CrystalDecisions.CrystalReports.Engine.ReportClass, ByVal eft As CrystalDecisions.Shared.ExportFormatType)
    '    selectedReport.ExportOptions.ExportFormatType = eft

    '    Dim contentType As String = ""
    '    ' Make sure asp.net has create and delete permissions in the directory
    '    Dim tempDir As String = "c:\" 'System.Configuration.ConfigurationSettings.AppSettings("TempDir")
    '    Dim tempFileName As String = Session.SessionID.ToString() & "."
    '    Select Case eft
    '        Case CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat
    '            tempFileName += "pdf"
    '            contentType = "application/pdf"
    '            Exit Select
    '        Case CrystalDecisions.[Shared].ExportFormatType.WordForWindows
    '            tempFileName += "doc"
    '            contentType = "application/msword"
    '            Exit Select
    '        Case CrystalDecisions.[Shared].ExportFormatType.Excel
    '            tempFileName += "xls"
    '            contentType = "application/vnd.ms-excel"
    '            Exit Select
    '        Case CrystalDecisions.[Shared].ExportFormatType.HTML32, CrystalDecisions.[Shared].ExportFormatType.HTML40
    '            tempFileName += "htm"
    '            contentType = "text/html"
    '            Dim hop As New CrystalDecisions.Shared.HTMLFormatOptions()
    '            hop.HTMLBaseFolderName = tempDir
    '            hop.HTMLFileName = tempFileName
    '            selectedReport.ExportOptions.FormatOptions = hop
    '            Exit Select
    '    End Select

    '    Dim dfo As New CrystalDecisions.Shared.DiskFileDestinationOptions()
    '    dfo.DiskFileName = tempDir + tempFileName
    '    selectedReport.ExportOptions.DestinationOptions = dfo
    '    selectedReport.ExportOptions.ExportDestinationType = CrystalDecisions.[Shared].ExportDestinationType.DiskFile

    '    selectedReport.Export()
    '    selectedReport.Close()

    '    Dim tempFileNameUsed As String
    '    If eft = CrystalDecisions.[Shared].ExportFormatType.HTML32 OrElse eft = CrystalDecisions.[Shared].ExportFormatType.HTML40 Then
    '        Dim fp As String() = selectedReport.FilePath.Split("\".ToCharArray())
    '        Dim leafDir As String = fp(fp.Length - 1)
    '        ' strip .rpt extension
    '        leafDir = leafDir.Substring(0, leafDir.Length - 4)
    '        tempFileNameUsed = String.Format("{0}{1}\{2}", tempDir, leafDir, tempFileName)
    '    Else
    '        tempFileNameUsed = tempDir + tempFileName
    '    End If

    '    Response.ClearContent()
    '    Response.ClearHeaders()
    '    Response.ContentType = contentType

    '    Response.WriteFile(tempFileNameUsed)
    '    Response.Flush()
    '    Response.Close()

    '    System.IO.File.Delete(tempFileNameUsed)
    'End Sub

    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument, ByVal reportParameters As Hashtable)
        Try


            Dim myTables As Tables = myReportDocument.Database.Tables
            Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
            Dim crParameterDiscreteValue As ParameterDiscreteValue
            'Dim crParameterFieldDefinitions As ParameterFieldDefinitions
            'Dim crParameterFieldLocation As ParameterFieldDefinition
            'Dim crParameterValues As ParameterValues
            'Dim iRpt As New DictionaryEntry

            ' Dim myTableLogonInfo As TableLogOnInfo
            ' myTableLogonInfo = New TableLogOnInfo
            'myTableLogonInfo.ConnectionInfo = myConnectionInfo

            For Each myTable In myTables
                Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
                myTableLogonInfo = myTable.LogOnInfo
                myTableLogonInfo.ConnectionInfo = myConnectionInfo
                myTable.ApplyLogOnInfo(myTableLogonInfo)
                myTable.Location = myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)

            Next


            'crParameterFieldDefinitions = myReportDocument.DataDefinition.ParameterFields
            'If reportParameters.Count <> 0 Then
            '    For Each iRpt In reportParameters
            '        Try
            '            crParameterFieldLocation = crParameterFieldDefinitions.Item(iRpt.Key.ToString)
            '            crParameterValues = crParameterFieldLocation.CurrentValues
            '            crParameterDiscreteValue = New CrystalDecisions.Shared.ParameterDiscreteValue
            '            crParameterDiscreteValue.Value = iRpt.Value
            '            crParameterValues.Add(crParameterDiscreteValue)
            '            crParameterFieldLocation.ApplyCurrentValues(crParameterValues)
            '        Catch ex As Exception
            '        End Try
            '    Next
            'End If

            'myReportDocument.DataSourceConnections(0).SetConnection(myConnectionInfo.ServerName, myConnectionInfo.DatabaseName, myConnectionInfo.UserID, myConnectionInfo.Password) '"LIJO\SQLEXPRESS", "OASIS", "sa", "xf6mt") '
            'myReportDocument.SetDatabaseLogon(myConnectionInfo.ServerName, myConnectionInfo.DatabaseName, myConnectionInfo.UserID, myConnectionInfo.Password, True) '"sa", "xf6mt", "LIJO\SQLEXPRESS", "OASIS") '

            myReportDocument.VerifyDatabase()
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "Rajesh")
        End Try

    End Sub

    Private Sub SetDBLogonForSubreports(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument, ByVal reportParameters As Hashtable)
        Dim mySections As Sections = myReportDocument.ReportDefinition.Sections
        Dim mySection As Section
        For Each mySection In mySections
            Dim myReportObjects As ReportObjects = mySection.ReportObjects
            Dim myReportObject As ReportObject
            For Each myReportObject In myReportObjects
                If myReportObject.Kind = ReportObjectKind.SubreportObject Then
                    Dim mySubreportObject As SubreportObject = CType(myReportObject, SubreportObject)
                    Dim subReportDocument As ReportDocument = mySubreportObject.OpenSubreport(mySubreportObject.SubreportName)

                    Select Case subReportDocument.Name
                        Case "studphotos"
                            Dim dS As New dsImageRpt
                            Dim fs As New FileStream(Session("StudentPhotoPath"), System.IO.FileMode.Open, System.IO.FileAccess.Read)
                            Dim Image As Byte() = New Byte(fs.Length - 1) {}
                            fs.Read(Image, 0, Convert.ToInt32(fs.Length))
                            fs.Close()
                            Dim dr As dsImageRpt.StudPhotoRow = dS.StudPhoto.NewStudPhotoRow
                            dr.Photo = Image
                            'Add the new row to the dataset
                            dS.StudPhoto.Rows.Add(dr)
                            subReportDocument.SetDataSource(dS)
                        Case "rptSubPhoto.rpt"
                            Dim newWindow As String = String.Empty
                            Dim vrptClass As rptClass
                            newWindow = IIf(Request.QueryString("newWindow") <> String.Empty, Request.QueryString("newWindow"), String.Empty)
                            Dim rptStr As String = ""
                            If Not newWindow Is String.Empty Then
                                rptStr = "rptClass" + newWindow
                            Else
                                rptStr = "rptClass"
                            End If
                            vrptClass = Session.Item(rptStr)
                            If vrptClass.Photos IsNot Nothing Then
                                SetPhotoToReport(subReportDocument, vrptClass.Photos)
                            End If
                        Case "rptGWAElmtSub", "rptWSOInterimSub", "rptCISImageSub", "rptWINImageSub", "rptWSSImageSub", "rptWISImageSub"
                            Dim newWindow As String = String.Empty
                            Dim vrptClass As rptClass
                            newWindow = IIf(Request.QueryString("newWindow") <> String.Empty, Request.QueryString("newWindow"), String.Empty)
                            Dim rptStr As String = ""
                            If Not newWindow Is String.Empty Then
                                rptStr = "rptClass" + newWindow
                            Else
                                rptStr = "rptClass"
                            End If
                            vrptClass = Session.Item(rptStr)
                            If vrptClass.Photos IsNot Nothing Then
                                SetGWAPhotoToReport(subReportDocument, vrptClass.Photos, reportParameters)
                            End If
                        Case Else
                            SetDBLogonForReport(myConnectionInfo, subReportDocument, reportParameters)
                    End Select

                    ' subReportDocument.VerifyDatabase()
                End If
            Next
        Next

    End Sub
    Private Sub SetGWAPhotoToReport(ByVal subReportDocument As ReportDocument, ByVal vPhotos As OASISPhotos, ByVal param As Hashtable)
        Dim arrphotos As ArrayList = vPhotos.IDs
        Dim RPF_ID As String = param.Item("@RPF_ID")
        vPhotos = UpdateGWAPhotoPath(vPhotos, RPF_ID)
        Dim dS As New dsImageRpt
        Dim fs As FileStream = Nothing
        Dim Image As Byte() = Nothing
        Dim ienum As IDictionaryEnumerator = vPhotos.vHTPhoto_ID.GetEnumerator
        While (ienum.MoveNext)
            Try
                fs = New FileStream(ienum.Value, System.IO.FileMode.Open, System.IO.FileAccess.Read)
                Image = New Byte(fs.Length - 1) {}
                fs.Read(Image, 0, Convert.ToInt32(fs.Length))
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message + "--Report Photo", System.Reflection.MethodBase.GetCurrentMethod().Name)
            Finally
                If Not fs Is Nothing Then
                    fs.Close()
                End If
            End Try
            Dim dr As dsImageRpt.dsGWAImageRow = dS.dsGWAImage.NewdsGWAImageRow
            dr.IMAGE = Image
            dr.ID = ienum.Key
            'Add the new row to the dataset
            dS.dsGWAImage.Rows.Add(dr)
        End While
        subReportDocument.SetDataSource(dS)
        subReportDocument.VerifyDatabase()
    End Sub

    Function UpdateGWAPhotoPath(ByVal vPhotos As OASISPhotos, ByVal RPF_ID As String) As OASISPhotos
        Select Case vPhotos.PhotoType
            Case OASISPhotoType.STUDENT_PHOTO
                Dim Virtual_Path As String = ConfigurationManager.ConnectionStrings("STU_REPORT").ConnectionString  'Replace(WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ConnectionString, "http:", "")

                Dim dtFilePath As DataTable = GETStudGWA_photoPath(vPhotos.IDs, RPF_ID) 'get the image
                vPhotos.vHTPhoto_ID = New Hashtable
                For Each dr As DataRow In dtFilePath.Rows
                    If (dr("FILE_PATH") Is DBNull.Value) OrElse (dr("FILE_PATH") Is Nothing) OrElse (dr("FILE_PATH") = "") Then
                        vPhotos.vHTPhoto_ID(dr("STU_ID")) = Server.MapPath("~/Curriculum/noimg/NO_IMG_gwa_white.png")
                    Else
                        vPhotos.vHTPhoto_ID(dr("STU_ID")) = Virtual_Path + dr("FILE_PATH").ToString
                    End If
                Next
        End Select
        Return vPhotos
    End Function
    Private Function GETStudGWA_photoPath(ByVal arrSTU_IDs As ArrayList, ByVal RPF_ID As String) As DataTable
        Dim comma As String = String.Empty
        Dim strStudIDs As String = String.Empty
        For Each ID As Object In arrSTU_IDs
            strStudIDs += comma + ID.ToString
            comma = ", "
        Next
        'Dim sqlString As String = "SELECT '/125017/'+SFU_GRD_ID+'_'+RIGHT(ACY_DESCR,2)+'_'+CONVERT(VARCHAR(100),SFU_RPF_ID)+'\'+SFU_FILEPATH AS FILE_PATH,SFU_STU_ID AS STU_ID" _
        '                                & " FROM CURR.STUDENT_FILEUPLOAD INNER JOIN VW_ACADEMICYEAR_D ON SFU_ACD_ID=ACD_ID " _
        '                                & " INNER JOIN VW_ACADEMICYEAR_M ON ACD_ACY_ID=ACY_ID WHERE SFU_STU_ID IN " _
        '                                & "(" + strStudIDs + ") AND SFU_RPF_ID=" + RPF_ID


        Dim sqlString As String = "SELECT CASE WHEN ISNULL(SFU_FILEPATH,'')='' THEN '' ELSE '\" + Session("sbsuid") + "\'+SFU_GRD_ID+'_'+RIGHT(ACY_DESCR,2)+'_'+CONVERT(VARCHAR(100),SFU_RPF_ID)+'\'+SFU_FILEPATH END AS FILE_PATH " _
                                    & " ,STU_ID" _
                                    & " FROM STUDENT_M LEFT OUTER JOIN CURR.STUDENT_FILEUPLOAD ON SFU_STU_ID=STU_ID AND SFU_RPF_ID=" + RPF_ID _
                                    & " LEFT OUTER JOIN VW_ACADEMICYEAR_D ON SFU_ACD_ID=ACD_ID " _
                                    & " INNER JOIN VW_ACADEMICYEAR_M ON ACD_ACY_ID=ACY_ID WHERE STU_ID IN " _
                                    & "(" + strStudIDs + ")"


        Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_CURRICULUMConnectionString, CommandType.Text, sqlString)
        If ds IsNot Nothing Then
            Return ds.Tables(0)
        End If
        Return Nothing

    End Function

    Private Sub SetPhotoToReport(ByVal subReportDocument As ReportDocument, ByVal vPhotos As OASISPhotos)
        Dim arrphotos As ArrayList = vPhotos.IDs
        vPhotos = UpdatePhotoPath(vPhotos)
        Dim dS As New dsImageRpt
        Dim fs As FileStream = Nothing
        Dim Image As Byte() = Nothing
        Dim ienum As IDictionaryEnumerator = vPhotos.vHTPhoto_ID.GetEnumerator
        While (ienum.MoveNext)
            Try
                fs = New FileStream(ienum.Value, System.IO.FileMode.Open, System.IO.FileAccess.Read)
                Image = New Byte(fs.Length - 1) {}
                fs.Read(Image, 0, Convert.ToInt32(fs.Length))
            Catch
            Finally
                If Not fs Is Nothing Then
                    fs.Close()
                End If
            End Try
            Dim dr As dsImageRpt.DSPhotosRow = dS.DSPhotos.NewDSPhotosRow
            dr.IMAGE = Image
            dr.ID = ienum.Key
            'Add the new row to the dataset
            dS.DSPhotos.Rows.Add(dr)
        End While
        subReportDocument.SetDataSource(dS)
        subReportDocument.VerifyDatabase()
    End Sub

    Function UpdatePhotoPath(ByVal vPhotos As OASISPhotos) As OASISPhotos
        Select Case vPhotos.PhotoType
            Case OASISPhotoType.STUDENT_PHOTO
                Dim Virtual_Path As String = Web.Configuration.WebConfigurationManager.ConnectionStrings("EmpFilepath").ConnectionString()  'Replace(WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ConnectionString, "http:", "")
                Dim dtFilePath As DataTable = GETStud_photoPath(vPhotos.IDs) 'get the image
                vPhotos.vHTPhoto_ID = New Hashtable
                For Each dr As DataRow In dtFilePath.Rows
                    If (dr("FILE_PATH") Is DBNull.Value) OrElse (dr("FILE_PATH") Is Nothing) OrElse (dr("FILE_PATH") = "") Then
                        vPhotos.vHTPhoto_ID(dr("STU_ID")) = Virtual_Path + "/NOIMG/no_image.jpg"
                    Else
                        vPhotos.vHTPhoto_ID(dr("STU_ID")) = Virtual_Path + dr("FILE_PATH").ToString
                    End If
                Next
        End Select
        Return vPhotos
    End Function

    Private Function GETStud_photoPath(ByVal arrSTU_IDs As ArrayList) As DataTable
        Dim comma As String = String.Empty
        Dim strStudIDs As String = String.Empty
        For Each ID As Object In arrSTU_IDs
            strStudIDs += comma + ID.ToString
            comma = ", "
        Next
        Dim sqlString As String = "SELECT DISTINCT isnull(STU_PHOTOPATH,'') FILE_PATH,STU_ID FROM STUDENT_M where  STU_ID in(" & strStudIDs & ")"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, sqlString)
        If ds IsNot Nothing Then
            Return ds.Tables(0)
        End If
        Return Nothing

    End Function

    Public Function GetAsOnDateForAttendance(ByVal ACD_ID As String, ByVal GRD_ID As String, ByVal RPF_ID As String) As String
        Dim str_sql As String = " SELECT DISTINCT CONVERT(VARCHAR(11)," & _
        " ISNULL(MAX(OASIS.dbo.TERM_GRADEWISE.TRM_END_DT),GETDATE()), 103)  " & _
        " FROM RPT.REPORT_STUDENT_S INNER JOIN " & _
        " RPT.REPORT_RULE_M ON RPT.REPORT_STUDENT_S.RST_RRM_ID = RPT.REPORT_RULE_M.RRM_ID  " & _
        " INNER JOIN  OASIS.dbo.TERM_GRADEWISE " & _
        " ON RPT.REPORT_RULE_M.RRM_TRM_ID = OASIS.dbo.TERM_GRADEWISE.TRM_ID " & _
        " AND RRM_ACD_ID = RPT.REPORT_STUDENT_S.RST_ACD_ID  " & _
        " AND RRM_RPF_ID = RST_RPF_ID " & _
        " WHERE RPT.REPORT_STUDENT_S.RST_ACD_ID = " & ACD_ID & _
        " AND OASIS.dbo.TERM_GRADEWISE.TRM_GRD_ID = '" & GRD_ID & _
        "' AND RPT.REPORT_STUDENT_S.RST_RPF_ID = " & RPF_ID

        Return SqlHelper.ExecuteScalar(ConnectionManger.GetOASIS_CURRICULUMConnectionString, CommandType.Text, str_sql)

    End Function

    Public Function GetTotalGradeAttendance(ByVal ACD_ID As String, ByVal GRD_ID As String, ByVal RPF_ID As String) As String
        Dim str_sql As String = " SELECT DISTINCT ISNULL(OASIS.dbo.TERM_GRADEWISE.TRM_TOT_DAYS,0) " & _
        " FROM RPT.REPORT_STUDENT_S INNER JOIN " & _
        " RPT.REPORT_RULE_M ON RPT.REPORT_STUDENT_S.RST_RRM_ID = RPT.REPORT_RULE_M.RRM_ID  " & _
        " INNER JOIN  OASIS.dbo.TERM_GRADEWISE " & _
        " ON RPT.REPORT_RULE_M.RRM_TRM_ID = OASIS.dbo.TERM_GRADEWISE.TRM_ID " & _
        " AND RRM_ACD_ID = RPT.REPORT_STUDENT_S.RST_ACD_ID  " & _
        " AND RRM_RPF_ID = RST_RPF_ID " & _
        " WHERE RPT.REPORT_STUDENT_S.RST_ACD_ID = " & ACD_ID & _
        " AND OASIS.dbo.TERM_GRADEWISE.TRM_GRD_ID = '" & GRD_ID & _
        "' AND RPT.REPORT_STUDENT_S.RST_RPF_ID = " & RPF_ID

        Dim total_att = SqlHelper.ExecuteScalar(ConnectionManger.GetOASIS_CURRICULUMConnectionString, CommandType.Text, str_sql)
        If total_att Is Nothing Then
            Return 0
        Else
            Return total_att
        End If
    End Function

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

    End Sub


End Class
