﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="clmPerformancereport_view.aspx.vb" Inherits="Curriculum_clmPerformancereport_view" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
     <link rel="stylesheet" href="../css/bootstrap.css"/>
        <link rel="stylesheet" href="../css/bootstrap-theme.css"/>
</head>
<body>
    <form id="form1" runat="server">
        <ajaxToolkit:ToolkitScriptManager ID="scriptmanager12" runat="server"></ajaxToolkit:ToolkitScriptManager>
    <div>
        <section class="col-md-12 col-lg-12 col-sm-12 colContainerBox">
        <div class="col-md-12 col-lg-12 col-sm-12" >
            <div class="form-group">
                <div class="row" style="padding-top:10%;">
     <telerik:RadHtmlChart runat="server" ID="RadCurriculumPie" Height="350px">
                                            <PlotArea>
                                                <Series>
                                                    <telerik:ColumnSeries Name="Highest" DataFieldY="HIGHEST">

                                                        <LabelsAppearance Visible="false">
                                                        </LabelsAppearance>
                                                    </telerik:ColumnSeries>
                                                    <telerik:ColumnSeries Name="Lowest" DataFieldY="LOWEST">

                                                        <LabelsAppearance Visible="false">
                                                        </LabelsAppearance>
                                                    </telerik:ColumnSeries>
                                                    <telerik:ColumnSeries Name="Average" DataFieldY="GRADE_AVG">

                                                        <LabelsAppearance Visible="false">
                                                        </LabelsAppearance>
                                                    </telerik:ColumnSeries>
                                                      <telerik:ColumnSeries Name="Performance" DataFieldY="RST_MARK">

                                                        <LabelsAppearance Visible="false">
                                                        </LabelsAppearance>
                                                    </telerik:ColumnSeries>
                                                </Series>
                                                <XAxis DataLabelsField="SBG_SHORTCODE">
                                                   
                                                </XAxis>
                                            </PlotArea>
                                            <ChartTitle Text="Student Performance">

                                            </ChartTitle>
                                        </telerik:RadHtmlChart>
                    </div>
                </div>
            </div>
            </section>
    </div>
    </form>
</body>
</html>
