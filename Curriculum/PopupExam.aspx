﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="PopupExam.aspx.vb" Inherits="Curriculum_PopupExam" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <base target="_self" />
    <link media="screen" href="../CSS/SiteStyle.css" type="text/css" rel="stylesheet" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <!-- Google Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css' />
    <link href='https://fonts.googleapis.com/css?family=Raleway:100,200,300,700,800,900' rel='stylesheet' type='text/css' />

    <!-- Library CSS -->
    <link rel="stylesheet" href="/css/bootstrap.css" />

    <link rel="stylesheet" href="/css/bootstrap-theme.css" />

    <link rel="stylesheet" href="/css/fonts/font-awesome/css/font-awesome.css" />
    <link rel="stylesheet" href="/css/animations.css" media="screen" />
    <link rel="stylesheet" href="/css/animate.css" media="screen" />
    <link rel="stylesheet" href="/css/superfish.css" media="screen" />
    <link rel="stylesheet" href="/css/revolution-slider/css/settings.css" media="screen" />
    <link rel="stylesheet" href="/css/revolution-slider/css/extralayers.css" media="screen" />
    <link rel="stylesheet" href="/css/prettyPhoto.css" media="screen" />
    <link rel="stylesheet" href="/css/menus.css" />
    <!-- Theme CSS -->

    <!-- Skin -->
    <link rel="stylesheet" href="/css/colors/blue.css" class="colors" />
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="/css/theme-responsive.css" />
    <!-- Switcher CSS -->
    <link href="/css/switcher.css" rel="stylesheet" />
    <link href="/css/spectrum.css" rel="stylesheet" />
    <!-- Favicons -->
    <link rel="shortcut icon" href="/img/ico/favicon.ico" />
    <link rel="apple-touch-icon" href="/img/ico/apple-touch-icon.png" />
    <link rel="apple-touch-icon" sizes="72x72" href="/img/ico/apple-touch-icon-72.png" />
    <link rel="apple-touch-icon" sizes="114x114" href="/img/ico/apple-touch-icon-114.png" />
    <link rel="apple-touch-icon" sizes="144x144" href="/img/ico/apple-touch-icon-144.png" />

    <link rel="stylesheet" href="/css/lightslider.css" />
    <link rel="stylesheet" href="/css/marquee.css" />
    <link rel="stylesheet" href="/css/marquee-prefixed.css" />

    <link rel='stylesheet' href="/css/tooltips.css" />
    <style type="text/css">
        BODY {
            padding-right: 0px;
            padding-left: 0px;
            background: url(images/background1.jpg) #ffffff repeat;
            padding-bottom: 0px;
            margin: 0px;
            font: 12px "Verdana", sans-serif;
            padding-top: 0px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
            <div class="content margin-top30 margin-bottom60">
                <div class="container">
                    <div class="row">
                        <div class="posts-block col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <table cellpadding="3" cellspacing="0" class="BlueTable_simple  table table-striped  table-responsive my-orders-table">
                                <tr class="tdblankAll">
                                    <td class="tdblankAll" align="left">
                                       
                                            <asp:GridView ID="gvSubject" runat="server" SkinID="GridViewNormal" Width="100%"
                                                EmptyDataText="No Data Found" CssClass="table table-striped  table-responsive my-orders-table" AutoGenerateColumns="False">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Id" Visible="false">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblsId" runat="server" Text='<%# bind("ES_SBG_ID") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Subject">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblsub" runat="server" Text='<%# bind("sbg_descr") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:GridView ID="gvPaper" runat="server" SkinID="GridViewNormal" Width="100%" EmptyDataText="No Data Found"
                                                                AutoGenerateColumns="False" CssClass="table table-striped table-bordered table-responsive my-orders-table" Font-Size="8">
                                                                <Columns>
                                                                    <asp:TemplateField HeaderText="Id" Visible="false">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblId" runat="server" Text='<%# bind("ES_ID") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Papers">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblPaper" runat="server" Text='<%# bind("ES_paper") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Date">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblDate" runat="server" Text='<%# bind("ES_DATE","{0:dd/MMM/yyyy}") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Dur/Hr">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblDur" runat="server" Text='<%# bind("ES_DUR") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Cost/AED">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblCost" runat="server" Text='<%# bind("ES_COST") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="File Name">
                                                                        <ItemTemplate>
                                                                            <table>
                                                                                <tr>
                                                                                    <td align="center" style="height: 26px">
                                                                                        <asp:ImageButton ID="imgF" runat="server" Width="20px" OnClick="imgF_Click" CommandArgument='<%# Bind("es_id") %>' />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:LinkButton ID="txtfile" runat="server" Width="50px" Text='<%# Bind("es_filename") %>'
                                                                                            CommandName="View" CommandArgument='<%# Bind("es_id") %>' OnClick="txtfile_Click" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                            </asp:GridView>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                       
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
    </form>
</body>
</html>
