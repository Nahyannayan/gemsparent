﻿<%@ Page Language="VB" MasterPageFile="~/ParentMaster.master"  AutoEventWireup="false" CodeFile="Grade_Rubrics_DMHS.aspx.vb" Inherits="ParentLogin_Grade_Rubrics" title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphParent" Runat="Server">
    <table align="center" class="BlueTable" width="100%">
                                          <tr class="subheader_img">
            <td colspan="2">
                Grading System</td>
        </tr>
        <tr runat="server" id ="grdHeader1">
            <td valign="top">
                <asp:Label ID="lbl_header1" runat="server" ForeColor="#5A5A58" Font-Bold="True"></asp:Label>
                <br />
                <asp:Label ID="lbl_header2" runat="server" ForeColor="Red"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lbl_header3" runat="server" ForeColor="#5A5A58" Font-Bold="True"></asp:Label>
                <br />
                <asp:Label ID="lbl_header4" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td valign="top">
                <asp:GridView ID="gv_rubrics1" runat="server" AutoGenerateColumns="False" 
                    Width="100%" SkinID="GridViewView">
                    <Columns>
                        <asp:BoundField DataField="GR_MARK" HeaderText="Marks" >
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="GR_GRADE" HeaderText="Point" >
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                    </Columns>
                </asp:GridView>
            </td>
            <td valign="top">
                <asp:GridView ID="gv_rubrics2" runat="server" AutoGenerateColumns="False" 
                    Width="100%" SkinID="GridViewView">
                    <Columns>
                        <asp:BoundField DataField="GR_MARK" HeaderText="Marks" >
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="GR_GRADE" HeaderText="Grade" >
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
        <tr runat="server" id ="grdHeader_lowerGrade" >
            <td colspan="2" valign="top">
                <asp:Label ID="lblHeader_Lower1" runat="server" ForeColor="#5A5A58" Font-Bold="True"></asp:Label>
                <br />
                <asp:Label ID="lblHeader_Lower2" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2" valign="top"><asp:GridView ID="gv_rubrics_lower" runat="server" AutoGenerateColumns="False" 
                    Width="100%" SkinID="GridViewView">
                <Columns>
                    <asp:BoundField DataField="GR_MARK" HeaderText="Marks" >
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:BoundField DataField="GR_GRADE" HeaderText="Grade" >
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:BoundField DataField="GR_DESCR" HeaderText="Description" >
                        <ItemStyle HorizontalAlign="Justify" />
                    </asp:BoundField>
                </Columns>
            </asp:GridView>
            </td>
        </tr>
        <tr id ="trCurricular" runat="server" >
            <td colspan="2" valign="top">
                <asp:Label ID="lblCocurricularActivity" runat="server" Font-Bold="True" ForeColor="#5A5A58">Co-curricular 
                Activities and Personal Characteristics</asp:Label><br />
                <asp:GridView ID="gvCocurricularActivity" runat="server" AutoGenerateColumns="False" 
                    Width="100%" SkinID="GridViewView">
                    <Columns>
                        <asp:BoundField DataField="GR_GRADE" HeaderText="Grade" >
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="GR_DESCR" HeaderText="Description" >
                            <ItemStyle HorizontalAlign="Justify" />
                        </asp:BoundField>
                    </Columns>
                </asp:GridView>
            
            </td>
        </tr>
    </table>
</asp:Content>

