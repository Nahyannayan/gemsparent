﻿Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Web.Security
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports System.Collections
Partial Class Curriculum_clmPerformanceTracker
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        CallReport()
    End Sub


    Sub CallReport()
        Dim param As New Hashtable
        param.Add("@IMG_BSU_ID", Session("SBSUID"))
        param.Add("@IMG_TYPE", "LOGO")
        param.Add("@BSU_ID", Session("SBSUID"))
        param.Add("@RPF_DESCR", getReportcards)
        param.Add("@STM_ID", Session("STU_STM_ID"))
        param.Add("@STU_ID", Session("STU_ID"))
        param.Add("@bEVALUATION", True)
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            .reportPath = Server.MapPath("ProgressReports/Rpt/rptStudentPerformanceAcrossYear_CBSE_Evaluation.rpt")
        End With
        LoadReports(rptClass)
    End Sub


    Function getReportcards() As String
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String
        str_query = "EXEC RPT.GETREPORTCARDBYYEAR_CBSE_ONLINE " _
                       & "'" + Session("SBSUID") + "'," _
                       & "'" + Session("STU_GRD_ID") + "'," _
                       & Session("CLM").ToString + "," _
                        & Session("STU_ID")
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        Dim str As String = ""

        Dim i As Integer

        For i = 0 To ds.Tables(0).Rows.Count - 1
            If str <> "" Then
                str += "|"
            End If
            str += ds.Tables(0).Rows(i).Item(0)
        Next

        Return str
    End Function


    Sub LoadReports(ByVal rptClass)
        Dim rs As New ReportDocument
        Try
            Dim iRpt As New DictionaryEntry
            Dim i As Integer
            Dim newWindow As String

            Session("HFrpf_id") = Nothing
            Session("HFrsm_id") = Nothing
            Dim rptStr As String = ""

            Dim crParameterDiscreteValue As ParameterDiscreteValue
            Dim crParameterFieldDefinitions As ParameterFieldDefinitions
            Dim crParameterFieldLocation As ParameterFieldDefinition
            Dim crParameterValues As ParameterValues

            With rptClass

                rs.Load(.reportPath)
                Dim myConnectionInfo As ConnectionInfo = New ConnectionInfo()
                myConnectionInfo.ServerName = .crInstanceName
                myConnectionInfo.DatabaseName = .crDatabase
                myConnectionInfo.UserID = .crUser
                myConnectionInfo.Password = .crPassword


                SetDBLogonForSubreports(myConnectionInfo, rs, .reportParameters)
                SetDBLogonForReport(myConnectionInfo, rs, .reportParameters)


                crParameterFieldDefinitions = rs.DataDefinition.ParameterFields
                If .reportParameters.Count <> 0 Then
                    For Each iRpt In .reportParameters
                        crParameterFieldLocation = crParameterFieldDefinitions.Item(iRpt.Key.ToString)
                        crParameterValues = crParameterFieldLocation.CurrentValues
                        crParameterDiscreteValue = New CrystalDecisions.Shared.ParameterDiscreteValue
                        crParameterDiscreteValue.Value = iRpt.Value
                        crParameterValues.Add(crParameterDiscreteValue)
                        crParameterFieldLocation.ApplyCurrentValues(crParameterValues)
                    Next
                End If



                If .selectionFormula <> "" Then
                    rs.RecordSelectionFormula = .selectionFormula
                End If
                Try
                    DisplayReport(rs, CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat)
                Catch ee As Exception
                End Try

            End With
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "Report Viewer")
        Finally
            rs.Close()
            rs.Dispose()

        End Try
        'GC.Collect()
    End Sub


    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument, ByVal reportParameters As Hashtable)
        Dim myTables As Tables = myReportDocument.Database.Tables
        Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
        Dim crParameterDiscreteValue As ParameterDiscreteValue
        'Dim crParameterFieldDefinitions As ParameterFieldDefinitions
        'Dim crParameterFieldLocation As ParameterFieldDefinition
        'Dim crParameterValues As ParameterValues
        'Dim iRpt As New DictionaryEntry

        ' Dim myTableLogonInfo As TableLogOnInfo
        ' myTableLogonInfo = New TableLogOnInfo
        'myTableLogonInfo.ConnectionInfo = myConnectionInfo

        For Each myTable In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
            myTable.Location = myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)

        Next


        'crParameterFieldDefinitions = myReportDocument.DataDefinition.ParameterFields
        'If reportParameters.Count <> 0 Then
        '    For Each iRpt In reportParameters
        '        Try
        '            crParameterFieldLocation = crParameterFieldDefinitions.Item(iRpt.Key.ToString)
        '            crParameterValues = crParameterFieldLocation.CurrentValues
        '            crParameterDiscreteValue = New CrystalDecisions.Shared.ParameterDiscreteValue
        '            crParameterDiscreteValue.Value = iRpt.Value
        '            crParameterValues.Add(crParameterDiscreteValue)
        '            crParameterFieldLocation.ApplyCurrentValues(crParameterValues)
        '        Catch ex As Exception
        '        End Try
        '    Next
        'End If

        'myReportDocument.DataSourceConnections(0).SetConnection(myConnectionInfo.ServerName, myConnectionInfo.DatabaseName, myConnectionInfo.UserID, myConnectionInfo.Password) '"LIJO\SQLEXPRESS", "OASIS", "sa", "xf6mt") '
        'myReportDocument.SetDatabaseLogon(myConnectionInfo.ServerName, myConnectionInfo.DatabaseName, myConnectionInfo.UserID, myConnectionInfo.Password, True) '"sa", "xf6mt", "LIJO\SQLEXPRESS", "OASIS") '

        myReportDocument.VerifyDatabase()


    End Sub

    Private Sub SetDBLogonForSubreports(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument, ByVal reportParameters As Hashtable)
        Dim mySections As Sections = myReportDocument.ReportDefinition.Sections
        Dim mySection As Section
        For Each mySection In mySections
            Dim myReportObjects As ReportObjects = mySection.ReportObjects
            Dim myReportObject As ReportObject
            For Each myReportObject In myReportObjects
                If myReportObject.Kind = ReportObjectKind.SubreportObject Then
                    Dim mySubreportObject As SubreportObject = CType(myReportObject, SubreportObject)
                    Dim subReportDocument As ReportDocument = mySubreportObject.OpenSubreport(mySubreportObject.SubreportName)

                    Select Case subReportDocument.Name
                        Case "studphotos"
                            Dim dS As New dsImageRpt
                            Dim fs As New FileStream(Session("StudentPhotoPath"), System.IO.FileMode.Open, System.IO.FileAccess.Read)
                            Dim Image As Byte() = New Byte(fs.Length - 1) {}
                            fs.Read(Image, 0, Convert.ToInt32(fs.Length))
                            fs.Close()
                            Dim dr As dsImageRpt.StudPhotoRow = dS.StudPhoto.NewStudPhotoRow
                            dr.Photo = Image
                            'Add the new row to the dataset
                            dS.StudPhoto.Rows.Add(dr)
                            subReportDocument.SetDataSource(dS)
                        Case "rptSubPhoto.rpt"
                            Dim newWindow As String = String.Empty
                            Dim vrptClass As rptClass
                            newWindow = IIf(Request.QueryString("newWindow") <> String.Empty, Request.QueryString("newWindow"), String.Empty)
                            Dim rptStr As String = ""
                            If Not newWindow Is String.Empty Then
                                rptStr = "rptClass" + newWindow
                            Else
                                rptStr = "rptClass"
                            End If
                            vrptClass = Session.Item(rptStr)
                            If vrptClass.Photos IsNot Nothing Then
                                SetPhotoToReport(subReportDocument, vrptClass.Photos)
                            End If
                        Case Else
                            SetDBLogonForReport(myConnectionInfo, subReportDocument, reportParameters)
                    End Select

                    ' subReportDocument.VerifyDatabase()
                End If
            Next
        Next

    End Sub

    Private Sub SetPhotoToReport(ByVal subReportDocument As ReportDocument, ByVal vPhotos As OASISPhotos)
        Dim arrphotos As ArrayList = vPhotos.IDs
        vPhotos = UpdatePhotoPath(vPhotos)
        Dim dS As New dsImageRpt
        Dim fs As FileStream = Nothing
        Dim Image As Byte() = Nothing
        Dim ienum As IDictionaryEnumerator = vPhotos.vHTPhoto_ID.GetEnumerator
        While (ienum.MoveNext)
            Try
                fs = New FileStream(ienum.Value, System.IO.FileMode.Open, System.IO.FileAccess.Read)
                Image = New Byte(fs.Length - 1) {}
                fs.Read(Image, 0, Convert.ToInt32(fs.Length))
            Catch
            Finally
                If Not fs Is Nothing Then
                    fs.Close()
                End If
            End Try
            Dim dr As dsImageRpt.DSPhotosRow = dS.DSPhotos.NewDSPhotosRow
            dr.IMAGE = Image
            dr.ID = ienum.Key
            'Add the new row to the dataset
            dS.DSPhotos.Rows.Add(dr)
        End While
        subReportDocument.SetDataSource(dS)
        subReportDocument.VerifyDatabase()
    End Sub
    Protected Sub DisplayReport(ByVal selectedReport As CrystalDecisions.CrystalReports.Engine.ReportDocument, ByVal eft As CrystalDecisions.Shared.ExportFormatType)
        selectedReport.ExportOptions.ExportFormatType = eft

        Dim contentType As String = ""
        Dim tempDir As String = Server.MapPath("ReportDownloads/")
        Dim tempFileName As String = Session("susr_name") + Now.ToString.Replace("/", "_").Replace(":", "_").Replace(" ", "_") + "." 'Session.SessionID.ToString() & "."
        Select Case eft
            Case CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat
                tempFileName += "pdf"
                contentType = "application/pdf"
                Exit Select
            Case CrystalDecisions.[Shared].ExportFormatType.WordForWindows
                tempFileName += "doc"
                contentType = "application/msword"
                Exit Select
            Case CrystalDecisions.[Shared].ExportFormatType.Excel
                tempFileName += "xls"
                contentType = "application/vnd.ms-excel"
                Exit Select
            Case CrystalDecisions.[Shared].ExportFormatType.HTML32, CrystalDecisions.[Shared].ExportFormatType.HTML40
                tempFileName += "htm"
                contentType = "text/html"
                Dim hop As New CrystalDecisions.Shared.HTMLFormatOptions()
                hop.HTMLBaseFolderName = tempDir
                hop.HTMLFileName = tempFileName
                selectedReport.ExportOptions.FormatOptions = hop
                Exit Select
        End Select

        Dim dfo As New CrystalDecisions.Shared.DiskFileDestinationOptions()
        dfo.DiskFileName = tempDir + tempFileName
        selectedReport.ExportOptions.DestinationOptions = dfo
        selectedReport.ExportOptions.ExportDestinationType = CrystalDecisions.[Shared].ExportDestinationType.DiskFile

        selectedReport.Export()
        selectedReport.Close()


        Dim tempFileNameUsed As String
        If eft = CrystalDecisions.[Shared].ExportFormatType.HTML32 OrElse eft = CrystalDecisions.[Shared].ExportFormatType.HTML40 Then
            Dim fp As String() = selectedReport.FilePath.Split("\".ToCharArray())
            Dim leafDir As String = fp(fp.Length - 1)
            leafDir = leafDir.Substring(0, leafDir.Length - 4)
            tempFileNameUsed = String.Format("{0}{1}\{2}", tempDir, leafDir, tempFileName)
        Else
            tempFileNameUsed = tempDir + tempFileName
        End If
        Response.ClearContent()
        Response.ClearHeaders()
        Response.ContentType = "application/pdf"
        Response.WriteFile(tempFileNameUsed)
        Response.Flush()
        Response.Close()


    End Sub

    Function UpdatePhotoPath(ByVal vPhotos As OASISPhotos) As OASISPhotos
        Select Case vPhotos.PhotoType
            Case OASISPhotoType.STUDENT_PHOTO
                Dim Virtual_Path As String = Web.Configuration.WebConfigurationManager.ConnectionStrings("EmpFilepath").ConnectionString()  'Replace(WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ConnectionString, "http:", "")
                Dim dtFilePath As DataTable = GETStud_photoPath(vPhotos.IDs) 'get the image
                vPhotos.vHTPhoto_ID = New Hashtable
                For Each dr As DataRow In dtFilePath.Rows
                    If (dr("FILE_PATH") Is DBNull.Value) OrElse (dr("FILE_PATH") Is Nothing) OrElse (dr("FILE_PATH") = "") Then
                        vPhotos.vHTPhoto_ID(dr("STU_ID")) = Virtual_Path + "/NOIMG/no_image.jpg"
                    Else
                        vPhotos.vHTPhoto_ID(dr("STU_ID")) = Virtual_Path + dr("FILE_PATH").ToString
                    End If
                Next
        End Select
        Return vPhotos
    End Function

    Private Function GETStud_photoPath(ByVal arrSTU_IDs As ArrayList) As DataTable
        Dim comma As String = String.Empty
        Dim strStudIDs As String = String.Empty
        For Each ID As Object In arrSTU_IDs
            strStudIDs += comma + ID.ToString
            comma = ", "
        Next
        Dim sqlString As String = "SELECT DISTINCT isnull(STU_PHOTOPATH,'') FILE_PATH,STU_ID FROM STUDENT_M where  STU_ID in(" & strStudIDs & ")"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, sqlString)
        If ds IsNot Nothing Then
            Return ds.Tables(0)
        End If
        Return Nothing

    End Function
End Class
