﻿<%@ Page Language="VB" MasterPageFile="~/ParentMaster.master" AutoEventWireup="false" CodeFile="clmPerformanceReport.aspx.vb" Inherits="Curriculum_CurrReport" Title="GEMS EDUCATION" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphParent" runat="Server">
    <div class="content margin-top30 margin-bottom60">
        <div class="container">
            <div class="row">
                <div class="posts-block col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="my-account">
                        <div>
                            <ajaxToolkit:ModalPopupExtender ID="mpe" OnOkScript="onPrvOk();" CancelControlID="ImageButton1" DynamicServicePath="" runat="server"
                                Enabled="True" TargetControlID="btnTarget" PopupControlID="plPrev" BackgroundCssClass="modalBackground" />
                            <asp:Button ID="btnTarget" runat="server" Text="Button" Style="display: none" />
                             <div id="plPrev" runat="server" style="display: none; overflow: visible; background-color: White; border-color: #b5cae7; border-style: solid; border-width: 4px; width: 80%; vertical-align: top; margin-top: 0px;">
            <div class="msg_header" style=" margin-top: 0px; vertical-align: top">
                <div class="msg" style="text-align: right; margin-top: 0px; background-color: #ffffff; vertical-align: top;">

                    <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/close.png" />
                </div>
            </div>
            
            <asp:Panel ID="plPrev1" runat="server" BackColor="White" Height="530px" >
                <iframe id="ifSibDetail" runat="server" style="background-image: url(../Images/Misc/loading.gif); background-position: center center; background-attachment: fixed; background-repeat: no-repeat;" height="100%"  marginwidth="0" frameborder="0" width="100%"></iframe>
            </asp:Panel>
        </div>
                        </div>
                        <div class="mainheading" style="display:none;">
                            <div>Performance Report</div>
                            <div class="right">
                                <asp:Label ID="lbChildName1" runat="server">
                                </asp:Label>
                            </div>
                        </div>
                        <div class="title-box">
                            <h3>Performance Report Details
                            <span class="profile-right">
                                <asp:Label ID="lbChildName" runat="server"></asp:Label>
                            </span></h3>
                        </div>
                        <div>
                            <asp:Label ID="lblcurrMsg" runat="server"></asp:Label>
                        </div>
                        <div width="100%" align="left">
                            <table class="table table-striped table-condensed table-hover table-responsive text-left my-orders-table" width="100%" cellpadding="8" cellspacing="0">
                                <tr>
                                    <td style="vertical-align:middle">
                            Select Academic Year :
                                        </td>
                                    <td>
                            <asp:DropDownList ID="ddlAcademicYear" CssClass="form-control" runat="server" AutoPostBack="true"></asp:DropDownList>
                                        </td>
                                    <td style="vertical-align:middle">
                                        <div style="display:none;">
                            <asp:RadioButton runat="server" ID="rdView" Text="View" Checked="True" GroupName="g1" ></asp:RadioButton>
                            <asp:RadioButton runat="server" ID="rdDownload" Text="Download" GroupName="g1"></asp:RadioButton></div>
                                         </td>
                                </tr>
                            </table>
                        </div>
                        <asp:DataList ID="dlReports" runat="server" RepeatColumns="1" CssClass="divcurrNoborder">
                            <ItemStyle Wrap="false" />
                            <ItemTemplate>
                                <div class="divCurrBox">
                                    <asp:Image ID="imgNew" ImageUrl="~/Images/FlashingNEW.gif"
                                        Style="display: inline; float: left; width: 30px; height: 20px; vertical-align: top;" runat="server" />
                                    <asp:Image ID="Image1" runat="server"
                                        ImageUrl="~/Images/arrow.png" Style="display: inline; float: left; margin-top: 5px; margin-right: 4px" />
                                    <asp:LinkButton ID="lnkReport" runat="server" Text='<%# Eval("RPF_DESCR")%>' OnClick="lnkReport_Click"></asp:LinkButton>
                                    <asp:Label ID="lblRpf" runat="server" Text='<%# Bind("RPF_ID") %>' Visible="false"></asp:Label>
                                    <asp:Label ID="lblRsm" runat="server" Text='<%# Bind("RPF_RSM_ID") %>' Visible="false"></asp:Label>
                                    <asp:Label ID="lblDate" runat="server" Text='<%# Bind("RPF_DATE") %>' Visible="false"></asp:Label>
                                    <asp:Label ID="lblReport" runat="server" Text='<%# Bind("RPF") %>' Visible="false"></asp:Label>
                                    <asp:Label ID="lblAcdId" runat="server" Text='<%# Bind("RSM_ACD_ID") %>' Visible="false"></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:DataList>
                        <iframe runat="server" id="ifReport" style="display: none;"></iframe>
                        <asp:HiddenField ID="hfType" runat="server" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

