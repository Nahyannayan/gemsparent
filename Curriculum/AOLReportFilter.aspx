<%@ Page Language="VB" MasterPageFile="~/ParentMaster.master" AutoEventWireup="false" CodeFile="AOLReportFilter.aspx.vb" Inherits="ParentLogin_OutStanding" Title="::GEMS EDUCATION::" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphParent" runat="Server">
    <div class="content margin-top30 margin-bottom60">
        <div class="container">
            <div class="row">
                <div class="posts-block col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="my-account">
                        <div>
                            <ajaxToolkit:ModalPopupExtender ID="mpe" OnOkScript="onPrvOk();" CancelControlID="ImageButton1" DynamicServicePath="" runat="server"
                                Enabled="True" TargetControlID="btnTarget" PopupControlID="plPrev" BackgroundCssClass="modalBackground" />
                            <asp:Button ID="btnTarget" runat="server" Text="Button" Style="display: none" />
                            <div id="plPrev" runat="server" style="display: none; overflow: visible; background-color: White; border-color: #b5cae7; border-style: solid; border-width: 4px; width: 835px; vertical-align: top; margin-top: 0px;">
                                <div class="msg_header" style="width: 835px; margin-top: 0px; vertical-align: top">
                                    <div class="msg" style="text-align: right; margin-top: 0px; background-color: #ffffff; vertical-align: top;">

                                        <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/Common/PageBody/close.png" />
                                    </div>
                                </div>
                                <asp:Panel ID="plPrev1" runat="server" BackColor="White" Height="530px" ScrollBars="vertical" Width="835px">
                                    <iframe id="ifSibDetail" style="background-image: url(../Images/Misc/loading.gif); background-position: center center; background-attachment: fixed; background-repeat: no-repeat;" height="100%" src="progressReportHome.aspx" scrolling="yes" marginwidth="0" frameborder="0" width="818"></iframe>
                                </asp:Panel>
                            </div>
                        </div>
                        <div class="mainheading">
                            <div class="left">Assessment of Learning(AOL) Report</div>
                            <div class="right">
                                <asp:Label ID="lbChildName" runat="server">
                                </asp:Label>
                            </div>
                        </div>
                        <table class="table table-striped table-bordered table-responsive text-left my-orders-table" width="100%" cellpadding="8" cellspacing="0">
                            <tr>
                                <td align="left" style="display: none">Select the type of Report you want to view&nbsp;
            <asp:RadioButton ID="radReportSubjectWise" runat="server" Text="By Subject"
                AutoPostBack="True" GroupName="ReportType" Checked="True" />
                                    <asp:RadioButton ID="radReportOverAll" runat="server" Text="Over All"
                                        AutoPostBack="True" GroupName="ReportType" /></td>
                            </tr>
                            <tr id="tr1" runat="server">
                                <td align="left" width="33%" style="vertical-align:middle">Select AOL Report </td>
                                <td align="left">
                                    <asp:DropDownList ID="ddlAcademicYear" CssClass="form-control" runat="server" AutoPostBack="True">
                                    </asp:DropDownList>

                                </td>
                            </tr>
                            <tr id="trAOLReport" runat="server">
                                <td align="left" width="33%" style="vertical-align:middle">Select AOL Report </td>
                                <td align="left">
                                    <asp:DropDownList ID="ddlAOLReports" CssClass="form-control" runat="server" AutoPostBack="True">
                                    </asp:DropDownList>

                                </td>
                            </tr>
                            <tr id="trSubject" runat="server">
                                <td align="left" width="33%" style="vertical-align:middle">Select Subjects </td>
                                <td>
                                    <asp:CheckBoxList ID="trSubjectList" runat="server"
                                        Style="overflow: auto; text-align: left; border: solid 1px #ccc" >
                                    </asp:CheckBoxList>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" colspan="2">
                                    <asp:Button ID="btnViewReport" runat="server" CssClass="btn btn-info"
                                        Text="View Report" />
                                </td>
                            </tr>
                        </table>
                        <asp:HiddenField ID="hfACD_ID" runat="server" />
                    </div>
                </div>
            </div>
        </div>
    </div>

</asp:Content>


