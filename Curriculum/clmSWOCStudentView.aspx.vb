Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System
Imports System.Xml
Imports System.Data.SqlTypes
Imports System.IO
Partial Class Curriculum_clmSWOCStudentView
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            ' hfSTU_ID.Value = "20226077"
            'hfACD_ID.Value = "881"
            'hfGRD_ID.Value = "07"
            hfSTU_ID.Value = Session("STU_ID")
            hfACD_ID.Value = Session("ACD_ID")
            hfGRD_ID.Value = Session("GRD_ID")
            BindSWOCSchedules()
        End If
    End Sub

#Region "Private Methods"
    Sub BindSWOCSchedules()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT SWM_ID,CASE WHEN ISNULL(SWS_bSUBMIT,0)=1 THEN SWM_DESCR ELSE SWM_DESCR+'<font color=red> - Not Submitted</font>' END SWM_DESCR," _
                                & " CASE WHEN ISNULL(SWS_bSUBMIT,0)=1 THEN '1' ELSE '0' END bSUBMIT   FROM OASIS_CURRICULUM.SWOC.SWOC_STUDENT_RELEASE " _
                                & " INNER JOIN SWOC.SWOC_SCHEDULE_M ON SWS_SWM_ID=SWM_ID " _
                                & " WHERE SWS_STU_ID=" + hfSTU_ID.Value + " AND ISNULL(SWS_bRELEASE,0)=1 " _
                                & " AND GETDATE() BETWEEN SWS_STARTDATE AND SWS_ENDDATE"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        dlReports.DataSource = ds
        dlReports.DataBind()

    End Sub
#End Region

    Protected Sub lnkReport_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblSwmId As Label = TryCast(sender.findcontrol("lblSwmId"), Label)
        Dim lblSubmit As Label = TryCast(sender.findcontrol("lblSubmit"), Label)
        If lblSubmit.Text = "1" Then
            Session("reporttype") = "swocstudent"
            Response.Redirect("~/Curriculum/ProgressReports/Aspx/rptSmartTargetPrint.aspx?swm_id=" + Encr_decrData.Encrypt(lblSwmId.Text) _
                         & "&grd_id=" + Encr_decrData.Encrypt(hfGRD_ID.Value))
        End If


    End Sub
End Class
