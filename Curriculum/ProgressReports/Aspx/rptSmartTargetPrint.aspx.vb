Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Web.Security
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports System.Collections
Partial Class Curriculum_Reports_Aspx_rptSmartTargetPrint
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Sub CallReport()
        Dim param As New Hashtable
        param.Add("@ACD_ID", hfACD_ID.Value)
        param.Add("@TGM_ID", hfTGM_ID.Value)
        param.Add("@SCT_ID", "0")
        param.Add("@STU_ID", hfSTU_ID.Value)
        param.Add("@GRD_ID", hfGRD_ID.Value)
        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            .reportPath = Server.MapPath("../Rpt/rptSmartTargetPrint.rpt")
        End With
        LoadReports(rptClass)
    End Sub

    Sub CallSwocStudentReport()
        Dim param As New Hashtable
        param.Add("@ACD_ID", hfACD_ID.Value)
        param.Add("@SWM_ID", hfSWM_ID.Value)
        param.Add("@SCT_ID", "0")
        param.Add("@STU_ID", hfSTU_ID.Value)
        param.Add("@GRD_ID", hfGRD_ID.Value)

        param.Add("@IMG_BSU_ID", Session("sbsuid"))
        param.Add("@IMG_TYPE", "logo")

        Dim rptClass As New rptClass
        With rptClass
            .crDatabase = "oasis_curriculum"
            .reportParameters = param
            .reportPath = Server.MapPath("../Rpt/rptSWOCStudentDetails.rpt")
        End With
        LoadReports(rptClass)
    End Sub

    Sub LoadReports(ByVal rptClass)
        Try
            Dim iRpt As New DictionaryEntry
            Dim i As Integer
            Dim newWindow As String

          
            Dim crParameterDiscreteValue As ParameterDiscreteValue
            Dim crParameterFieldDefinitions As ParameterFieldDefinitions
            Dim crParameterFieldLocation As ParameterFieldDefinition
            Dim crParameterValues As ParameterValues

            With rptClass

                rs.ReportDocument.Load(.reportPath)

                Dim myConnectionInfo As ConnectionInfo = New ConnectionInfo()
                myConnectionInfo.ServerName = .crInstanceName
                myConnectionInfo.DatabaseName = .crDatabase
                myConnectionInfo.UserID = .crUser
                myConnectionInfo.Password = .crPassword

                SetDBLogonForSubreports(myConnectionInfo, rs.ReportDocument, .reportParameters)
                SetDBLogonForReport(myConnectionInfo, rs.ReportDocument, .reportParameters)

                crParameterFieldDefinitions = rs.ReportDocument.DataDefinition.ParameterFields
                If .reportParameters.Count <> 0 Then
                    For Each iRpt In .reportParameters
                        crParameterFieldLocation = crParameterFieldDefinitions.Item(iRpt.Key.ToString)
                        crParameterValues = crParameterFieldLocation.CurrentValues
                        crParameterDiscreteValue = New CrystalDecisions.Shared.ParameterDiscreteValue
                        crParameterDiscreteValue.Value = iRpt.Value
                        crParameterValues.Add(crParameterDiscreteValue)
                        crParameterFieldLocation.ApplyCurrentValues(crParameterValues)
                    Next
                End If



                If .selectionFormula <> "" Then
                    rs.ReportDocument.RecordSelectionFormula = .selectionFormula
                End If

                rs.ReportDocument.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, False, "Report")

                rs.ReportDocument.Close()
                rs.Dispose()
                Response.Flush()
                Response.Close()
            End With
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "Report Viewer")
            ' lblText.Text = ex.Message + rptClass.reportPath
        Finally
            rs.ReportDocument.Close()
            rs.Dispose()
            'Response.Flush()
            ' Response.Close()
        End Try
        'GC.Collect()
    End Sub
    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument, ByVal reportParameters As Hashtable)
        Dim myTables As Tables = myReportDocument.Database.Tables
        Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
        Dim crParameterDiscreteValue As ParameterDiscreteValue
        For Each myTable In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
            myTable.Location = myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)

        Next
        myReportDocument.VerifyDatabase()
    End Sub

    Private Sub SetDBLogonForSubreports(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument, ByVal reportParameters As Hashtable)
        Dim mySections As Sections = myReportDocument.ReportDefinition.Sections
        Dim mySection As Section
        For Each mySection In mySections
            Dim myReportObjects As ReportObjects = mySection.ReportObjects
            Dim myReportObject As ReportObject
            For Each myReportObject In myReportObjects
                If myReportObject.Kind = ReportObjectKind.SubreportObject Then
                    Dim mySubreportObject As SubreportObject = CType(myReportObject, SubreportObject)
                    Dim subReportDocument As ReportDocument = mySubreportObject.OpenSubreport(mySubreportObject.SubreportName)
                    SetDBLogonForReport(myConnectionInfo, subReportDocument, reportParameters)
                End If
            Next
        Next

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        hfGRD_ID.Value = Encr_decrData.Decrypt(Request.QueryString("grd_id").Replace(" ", "+"))
        hfACD_ID.Value = Session("STU_ACD_ID")
        hfSTU_ID.Value = Session("STU_ID")
        'hfSTU_ID.Value = "20226077"
        'hfACD_ID.Value = "881"
        'hfGRD_ID.Value = "07"
        If Session("reporttype") = "swocstudent" Then
            hfSWM_ID.Value = Encr_decrData.Decrypt(Request.QueryString("swm_id").Replace(" ", "+"))
            CallSwocStudentReport()
        Else
            hfTGM_ID.Value = Encr_decrData.Decrypt(Request.QueryString("tgm_id").Replace(" ", "+"))
            CallReport()
        End If
    End Sub
End Class
