<%@ Page Language="VB" AutoEventWireup="false" CodeFile="CBSEReport.aspx.vb" Inherits="ParentLogin_CBSEReport" %>
<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %> 

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
    <base target="_self" />
 <style>
img
    {
        display:block;
    } 
.ReceiptCaption
	{
	font-family: Arial, Helvetica, sans-serif;
	FONT-WEIGHT: bold; FONT-SIZE: 12pt; COLOR: #000095; 
	HEIGHT: 19px; text-decoration:underline;
	}
.ReceiptCaptionTeacher
	{
	font-family: Arial, Helvetica, sans-serif;
	FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: #000095;
	HEIGHT: 19px;
	}	
	
.matters_print 
    {  
    font-family: Arial, Helvetica, sans-serif; 
    font-size: 13px; font-weight: bold; color: #000095
    }
.matters_heading 
    {  
    font-family: Arial, Helvetica, sans-serif; 
    font-size: 14px; font-weight: bold; color: #000095
    }
.matters_normal 
    { 
    font-family: Arial, Helvetica, sans-serif; 
    font-size: 13px; color: #000095;font-weight: bold;
    }
.matters_grid 
    {  
    font-family: Arial, Helvetica, sans-serif; 
    font-size: 13px; color: #000095; TEXT-INDENT: 8px; 
    }
.matters_small 
    {  
    font-family: Arial, Helvetica, sans-serif; 
    font-size: 12px; color: #000095
    }	
.Printbg 
    {               
    vertical-align:middle;				 
    font-family: Arial, Helvetica, sans-serif; 
    font-size: 12px; color: #000095
    }
.PrintSource
    {               
    vertical-align:bottom;				 
    font-family: Arial, Helvetica, sans-serif; 
    font-size: 12px; color: #000095
    }
 </style> 
<script language="javascript" type="text/javascript">
function PrintReceiptExport() 
    { 
        document.getElementById('tr_Print').style.display='none';
        window.print();
        document.getElementById('tr_Print').style.display='inline';
        try
        {
            if (/MSIE (\d+\.\d+);/.test(navigator.userAgent))
            {
            var ieversion=new Number(RegExp.$1) // capture x.x portion and store as a number
            if (ieversion>=8)
            window.close();
            } 
        }
        catch(ex){}        
    } 
function PrintReceipt() 
    { 
        if ('<%= Request.QueryString("isexport") %>'=='0') 
            PrintReceiptExport();
    } 
 function OnEscape() 
    { 
        if ( (event.keyCode==27) || (event.keyCode==99) || (event.keyCode==120) )
          window.close(); 
    }  
</script>
</head>
<body onload="PrintReceipt();" onkeypress="OnEscape()" onkeydown="OnEscape()" >
<form id="form1" runat="server">
<div style="border-bottom: #000095 2pt solid; border-left: #000095 2pt solid; border-top: #000095 2pt solid; border-right: #000095 2pt solid; padding-bottom: 2px; padding-left: 2px; padding-right: 2px; padding-top: 2px;">
 <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="border-right: #000095 1pt solid; border-top: #000095 1pt solid; border-left: #000095 1pt solid; border-bottom: #000095 1pt solid">
     <tr valign="top" id="tr_Print">
         <td align="right">
             <img src="../Images/Misc/print.gif" onclick="PrintReceiptExport();" style="cursor:hand" /></td>
     </tr>
     <tr valign="top">
        <td colspan="2" align="center" style="border-bottom: #000095 1pt solid" >
            <table border="0" cellpadding="1" cellspacing="0" width="100%">
                <tr>
                    <td align="center" rowspan="4" width="10%">
                        <asp:Image ID="imgLogo" runat="server" ImageAlign="AbsMiddle" />
                    </td>
                    <td class="matters_heading">
                        <asp:Label ID="lblSchool" runat="server"></asp:Label>
                    </td>
                    <td class="matters_heading" rowspan="4" align="center">
                        <img src="../Images/Misc/GemseduLogo.gif" align="absMiddle" /></td>
                </tr>
                <tr>
                    <td class="matters_print" style="height: 12px">
                        <asp:Label ID="lblHeader1" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="matters_print">
                        <asp:Label ID="lblHeader2" runat="server"></asp:Label></td>
                </tr>
                <tr>
                    <td class="matters_print">
                        <asp:Label ID="lblHeader3" runat="server"></asp:Label></td>
                </tr>                       
            </table>
        </td>
         <td align="center" colspan="1" style="height: 90px">
         </td>
        </tr>
        <tr>
            <td height="20px">&nbsp;</td>
        </tr>
    <tr>  
    <td valign="top" align="center">
     <table align="center" border="0" cellspacing="0" cellpadding="2" Width="750px">
        <tr>          
            <td align="center" class="ReceiptCaption">
                <asp:Label ID="lblReportHeader" runat="server"></asp:Label></td>                
        </tr>
         <tr>
             <td align="center">
            <!-- content starts here -->       
            <table cellpadding="3" border="0" Width="100%" style="border-right: #000095 1pt solid; border-top: #000095 1pt solid; border-left: #000095 1pt solid; border-bottom: #000095 1pt solid">
             <tr>
             <td align="left" class="matters_normal" width="140">
                 Name of the Student</td>
             <td align="center" class="matters_print" style="width: 1px;">
                 :</td>
             <td align="left" class="matters_print" >
                 &nbsp;<asp:Label ID="lblStudentName" runat="server"></asp:Label></td>
             </tr>         
             <tr>
             <td align="left" class="matters_normal">
                 Student ID</td>
             <td align="center" class="matters_print" style="width: 1px;">
                 :</td>
             <td align="left" class="matters_print" >
                 <asp:Label ID="lblStudentNo" runat="server"></asp:Label></td>
             </tr>
             <tr>
             <td align="left" class="matters_normal">
                 Grade</td>
             <td align="center" class="matters_print" style="width: 1px;">
                 :</td>
             <td align="left" class="matters_print" colspan="1" >
                 <asp:Label ID="lblGrade" runat="server"></asp:Label></td>
                         </tr>
                <tr>
                    <td align="left" class="matters_normal">
                        Class Teacher</td>
                    <td align="center" class="matters_print" style="width: 1px">
                        :</td>
                    <td align="left" class="matters_print" colspan="1">
                        <asp:Label ID="lblTeacher" runat="server"></asp:Label></td>
                </tr>
                     </table>
                <!-- content ends here -->  
             </td>
         </tr>     
          <tr >
          <td style="height: 19px" align="left" >
             </td></tr>
              <tr >
                  <td align="center" class="matters_grid" valign="top">               
                      <asp:GridView ID="gvSubject" runat="server" AutoGenerateColumns="False" CellPadding="4" BorderColor="#000095" BorderWidth="1pt" Width="100%" >
                          <Columns>
                              <asp:BoundField DataField="SBG_DESCR" HeaderText="Subject" >
                                  <HeaderStyle HorizontalAlign="Center" />
                                  <ItemStyle HorizontalAlign="Left" />
                              </asp:BoundField>
                              <asp:BoundField DataField="RST_GRADING" HeaderText="Assesment" >
                                  <HeaderStyle HorizontalAlign="Center" />
                                  <ItemStyle HorizontalAlign="Center" Width="20%" />
                              </asp:BoundField>
                              <asp:BoundField DataField="AVG_GRD" HeaderText="Class Agerage" >
                                  <HeaderStyle HorizontalAlign="Center" />
                                  <ItemStyle HorizontalAlign="Center" Width="20%" />
                              </asp:BoundField>
                          </Columns>
                      </asp:GridView>
                      &nbsp;&nbsp;</td>
              </tr>
              <tr>
             <td align="right" class="matters_print">
                 &nbsp;</td>
         </tr>
         <tr>
             <td>
                <table runat="server" id ="tblClassTEacherComments" border="0" cellpadding="4" cellspacing="0" align="center" Width="100%">
                       <tr  class="ReceiptCaptionTeacher" >
             <td align="left" class="matters_print" style="border-right: #000095 1pt solid; border-top: #000095 1pt solid; border-bottom-width: 1pt; border-bottom-color: #000095; border-left: #000095 1pt solid; text-indent: 1px">
                 Class Teacher Remarks</td>
         </tr>
        <tr >
              <td align="left" valign="middle" style="text-indent: 1px; border-right: #000095 1pt solid; border-top: #000095 1pt solid; border-left: #000095 1pt solid; border-bottom: #000095 1pt solid;" class="matters_normal">
                  &nbsp;<asp:Label ID="lblTeacherRemarks" runat="server"></asp:Label></td>
        </tr>
                  </table>
             </td>
         </tr>
             
        <tr >
              <td align="left" valign="middle" style="text-indent: 1px;" 
                  class="matters_normal">             <br /><br /><br />
                  
             </td>
        </tr>
        <tr >
              <td align="center" valign="bottom" style="text-indent: 1px;" 
                  class="matters_small">             <br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
                  Note : This is a computer generated report and does not require any signature<br /><br />
             </td>
        </tr>
    </table>
    </td>
    </tr> 
    </table>
</div>
    </form>
</body>
</html>
