﻿Imports Microsoft.ApplicationBlocks.Data
Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Text
Partial Class ParentLogin_Grade_Rubrics
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("username") Is Nothing Then
            Response.Redirect("~\ParentLogin\Login.aspx")
        End If
        Page.Title = ":: GEMS EDUCATION |  ::"
        If Page.IsPostBack = False Then
            'Session("bPasswdChanged") = "False"
            If Session("STU_NAME") = "" Then
                If Not Request.UrlReferrer Is Nothing Then
                    Response.Redirect(Request.UrlReferrer.ToString())
                Else
                    Response.Redirect("~\noAccess.aspx")
                End If
            End If
            trCurricular.Visible = False

            Select Case Session("STU_GRD_ID")
                Case "01", "02", "03", "04"
                    GridBind_LowerGrade()

                    gv_rubrics1.Visible = False
                    gv_rubrics2.Visible = False

                    lbl_header1.Visible = False
                    lbl_header2.Visible = False
                    lbl_header3.Visible = False
                    lbl_header4.Visible = False
                    gv_rubrics_lower.Columns(2).Visible = True
                    lblHeader_Lower1.Visible = True
                    lblHeader_Lower2.Visible = True
                    gv_rubrics_lower.Visible = True
                    If Session("sBSUID") = "123004" Then
                        GridBind_CocurricularActivity()
                        trCurricular.Visible = True
                    Else
                        trCurricular.Visible = False
                    End If
                Case "05", "06", "07", "08"
                    Select Case Session("sBsuid")
                        Case "123006"
                            GridBind_LowerGrade()

                            gv_rubrics1.Visible = False
                            gv_rubrics2.Visible = False

                            lbl_header1.Visible = False
                            lbl_header2.Visible = False
                            lbl_header3.Visible = False
                            lbl_header4.Visible = False
                            gv_rubrics_lower.Columns(2).Visible = False

                            lblHeader_Lower1.Visible = True
                            lblHeader_Lower2.Visible = True
                            gv_rubrics_lower.Visible = True
                        Case Else
                            gridbind_Grid1()
                            gridbind_Grid2()
                            gv_rubrics1.Visible = True
                            gv_rubrics2.Visible = True
                            gv_rubrics_lower.Visible = False
                            lbl_header1.Visible = True
                            lbl_header2.Visible = True
                            lbl_header3.Visible = True
                            lbl_header4.Visible = True

                            lblHeader_Lower1.Visible = False
                            lblHeader_Lower2.Visible = False
                    End Select
                Case Else
                    gridbind_Grid1()
                    gridbind_Grid2()
                    gv_rubrics1.Visible = True
                    gv_rubrics2.Visible = True
                    gv_rubrics_lower.Visible = False
                    lbl_header1.Visible = True
                    lbl_header2.Visible = True
                    lbl_header3.Visible = True
                    lbl_header4.Visible = True

                    lblHeader_Lower1.Visible = False
                    lblHeader_Lower2.Visible = False
            End Select

        End If
    End Sub
    Function grade_setting(ByVal grd_id As String) As String
        grd_id = "'%" + grd_id + "%'"
        Return grd_id
    End Function

    Sub GridBind_LowerGrade()
        Dim str_con As String = ConnectionManger.GetOASISConnectionString
        Dim Param(5) As SqlClient.SqlParameter
        Param(0) = New SqlClient.SqlParameter("@BSU_ID", Session("sBsuid"))
        Param(1) = New SqlClient.SqlParameter("@ACD_ID", Session("STU_ACD_ID"))
        Param(2) = New SqlClient.SqlParameter("@GRD_ID", Session("STU_GRD_ID"))
        Param(3) = New SqlClient.SqlParameter("@FILL_GRID", "1")
        '  Param(3).Direction = ParameterDirection.Output
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_con, CommandType.StoredProcedure, "ONLINE.GETGRADE_RUBRICS_SA", Param)
        gv_rubrics_lower.DataSource = ds.Tables(0)
        gv_rubrics_lower.DataBind()
        If ds.Tables(0).Rows.Count > 0 Then
            lblHeader_Lower1.Text = ds.Tables(0).Rows(0)("GRH_HEADER1").ToString
            lblHeader_Lower2.Text = ds.Tables(0).Rows(0)("GRH_HEADER2").ToString
        End If
    End Sub

    Sub GridBind_CocurricularActivity()
        Dim str_con As String = ConnectionManger.GetOASISConnectionString
        Dim Param(5) As SqlClient.SqlParameter
        Param(0) = New SqlClient.SqlParameter("@BSU_ID", Session("sBsuid"))
        Param(1) = New SqlClient.SqlParameter("@ACD_ID", Session("STU_ACD_ID"))
        Param(2) = New SqlClient.SqlParameter("@GRD_ID", Session("STU_GRD_ID"))
        Param(3) = New SqlClient.SqlParameter("@FILL_GRID", "3")
        '  Param(3).Direction = ParameterDirection.Output
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_con, CommandType.StoredProcedure, "ONLINE.GETGRADE_RUBRICS_SA", Param)
        gvCocurricularActivity.DataSource = ds.Tables(0)
        gvCocurricularActivity.DataBind()
        'If ds.Tables(0).Rows.Count > 0 Then
        '    lblHeader_Lower1.Text = ds.Tables(0).Rows(0)("GRH_HEADER1").ToString
        '    lblHeader_Lower2.Text = ds.Tables(0).Rows(0)("GRH_HEADER2").ToString
        'End If
    End Sub

    Sub gridbind_Grid1()
        Dim str_con As String = ConnectionManger.GetOASISConnectionString
        Dim Param(5) As SqlClient.SqlParameter
        Param(0) = New SqlClient.SqlParameter("@BSU_ID", Session("sBsuid"))
        Param(1) = New SqlClient.SqlParameter("@ACD_ID", Session("STU_ACD_ID"))
        Param(2) = New SqlClient.SqlParameter("@GRD_ID", Session("STU_GRD_ID"))
        Param(3) = New SqlClient.SqlParameter("@FILL_GRID", "1")
        '  Param(3).Direction = ParameterDirection.Output
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_con, CommandType.StoredProcedure, "ONLINE.GETGRADE_RUBRICS_SA", Param)
        gv_rubrics1.DataSource = ds.Tables(0)
        gv_rubrics1.DataBind()
        If ds.Tables(0).Rows.Count > 0 Then
            lbl_header1.Text = ds.Tables(0).Rows(0)("GRH_HEADER1").ToString
            lbl_header2.Text = ds.Tables(0).Rows(0)("GRH_HEADER2").ToString
        End If
    End Sub

    Sub gridbind_Grid2()
        Dim str_con As String = ConnectionManger.GetOASISConnectionString
        Dim Param(5) As SqlClient.SqlParameter
        Param(0) = New SqlClient.SqlParameter("@BSU_ID", Session("sBsuid"))
        Param(1) = New SqlClient.SqlParameter("@ACD_ID", Session("STU_ACD_ID"))
        Param(2) = New SqlClient.SqlParameter("@GRD_ID", Session("STU_GRD_ID"))
        Param(3) = New SqlClient.SqlParameter("@FILL_GRID", "2")
        '  Param(3).Direction = ParameterDirection.Output
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_con, CommandType.StoredProcedure, "ONLINE.GETGRADE_RUBRICS_SA", Param)
        gv_rubrics2.DataSource = ds.Tables(0)
        gv_rubrics2.DataBind()
        If ds.Tables(0).Rows.Count > 0 Then
            lbl_header3.Text = ds.Tables(0).Rows(0)("GRH_HEADER1").ToString
            lbl_header4.Text = ds.Tables(0).Rows(0)("GRH_HEADER2").ToString
        End If
    End Sub

End Class
