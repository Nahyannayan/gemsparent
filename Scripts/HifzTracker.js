﻿var fileSizeInMB = 2;

var validationErrorMessage = {
    FileSelect: 'Please select the audio/video file.',
    FileSize: 'File size can&#39;t exceed ' + fileSizeInMB + 'MB.',
    FileType: "Allowed extensions are {0}",
    FileSubmit: 'Error! A problem has been occurred while submitting your data.',
    TechnicalError: 'Techinal Error! Some problem occured while submitting your data.'
};

var fileUploader = function () {

    loadUploadPopup = function (source, isError) {
        $("#myModal").modal('show');

        if (source != undefined) {
            $('#myModal').one('shown.bs.modal', function () {
                clearUploadPopup();
                initUploadPopup(source);
            });
        }
        if (isError == true) showFileError(validationErrorMessage.FileSubmit);
    },

        initUploadPopup = function (source) {
            $('#hdnTrackerId').val(source.data('trackerid'));
            $('#hdnTransactionId').val(source.data('transactionid'));
            $('#hdnVerseNo').val(source.data('verseno'));
            var target = source.closest('.verse-field').find('input.verse-check');
            $('#hdnIsCompleted').val(target.prop('checked'));
        },

        onClosePopup = function (filePath) {
            $("#myModal").modal('hide');
            $(".modal-backdrop").removeClass('in');
            clearUploadPopup();
        },

        clearUploadPopup = function () {
            $("#lblUploadedMsg").text('');
            $("#lnkUploadedFile").text('');
            $('#hdnTrackerId').val('');
            $('#hdnVerseNo').val('');
            $('#hdnIsCompleted').val('');
            $('#hdnTransactionId').val('');
        },

        initializeDownloadFileRequest = function (source) {
            var relationId = source.data('relationid');
            $.ajax({
                type: "POST",
                url: "HifzTracker.aspx/SetDownloadPath",
                data: '{ relationId: ' + relationId + ' }',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    if (data.d == true) {
                        var iframe = document.createElement("iframe");
                        iframe.src = "/Service/HifzFileDownloads.aspx";
                        iframe.style.display = "none";
                        document.body.appendChild(iframe);
                    }
                    else
                        showTechinalError()
                },
                failure: function (response) {
                    showTechinalError(response.d)
                }
            });
        },

        downloadHifzCertificateAsPdf = function (source) {
            var trackerId = source.data('id');
            $.ajax({
                type: "POST",
                url: "HifzTracker.aspx/SetCertificateDownloadPath",
                data: '{ trackerId: ' + trackerId + ' }',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    if (data.d == true) {
                        var iframe = document.createElement("iframe");
                        iframe.src = "/Service/HifzCertificate.aspx";
                        iframe.style.display = "none";
                        document.body.appendChild(iframe);
                    }
                    else
                        showTechinalError();
                },
                failure: function (response) {
                    showTechinalError(response.d)
                }
            });
        },

        validateAndSaveFileToCollection = function (uploader) {
            if (uploader == undefined) {
                uploader = document.getElementById('hifzFileUpload');
            }
            var file = uploader.files[0];
            if (file == undefined) {
                showFileError(validationErrorMessage.FileSelect);
                return false;
            }

            var fileSize = file.size;
            fileSize = (fileSize / (1024 * 1024)).toFixed(2);
            if (fileSize > fileSizeInMB) {
                showFileError(validationErrorMessage.FileSize);
                return false;
            }

            var lblMsg = $(uploader).parents('div.fileuploader').find("#lblUploadedMsg");
            var targetFileLink = $(uploader).parents('div.fileuploader').find(".file-input-name");
            var invalidMessage = validateFileType(file.name);
            if (invalidMessage == undefined || invalidMessage == '') {
                lblMsg.text("");
                targetFileLink.text("");
            }
            else {
                showFileError(invalidMessage);
                $(uploader).val("");
                return false;
            }

            targetFileLink.attr('href', 'javascript:void(0);');
            targetFileLink.text(file.name);
            return true;
        },

        updateHifzTransaction = function (source) {
            $.ajax({
                type: "POST",
                url: "HifzTracker.aspx/UpdateHifzTransaction",
                data: '{ trackerId: ' + source.data('trackerid') + ', verseNo: "' + source.data('verseno') + '", isCompleted: ' + source.prop('checked') + ' }',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    if (parseInt(data.d) > 0)
                        showHideUploadButton(source, data.d);
                    else
                        showTechinalError()
                },
                failure: function (response) {
                    showTechinalError(response.d)
                }
            });
        },

        deleteAttachment = function (source) {
            $.ajax({
                type: "POST",
                url: "HifzTracker.aspx/DeleteHifzTransactionAttachment",
                data: '{ relationId: ' + source.data('relationid') + ' }',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    if (data.d == true)
                        showUploadAndHideDownloadButton(source);
                    else
                        showTechinalError()
                },
                failure: function (response) {
                    showTechinalError(response.d)
                }
            });
        },

        checkAttachmentExistsInGrid = function (targetGrid, value) {
            var isExists = false;
            var tblAttachment = $(targetGrid);
            tblAttachment.find('tr > td.col-name').each(function () {
                if ($(this).data('filename') == value) isExists = true;
            });
            return isExists;
        },

        validateFileType = function (fileName) {
            var validExtensions = ['mp3', 'mp4', 'avi', 'aac', 'wav', 'mov', 'm4v'];
            var fileNameExt = fileName.substr(fileName.lastIndexOf('.') + 1).toLowerCase();
            if ($.inArray(fileNameExt, validExtensions) == -1) {
                return validationErrorMessage.FileType.replace("{0}", validExtensions.join(', '));
            }
        },

        showHideUploadButton = function (source, id) {
        source.prop('disabled', true);
            var target = source.closest('.verse-field').find('span.file-field');
            if (source.prop('checked'))
                showUploadAndHideDownloadButton(source, id);
            else
                target.addClass('hidden');
        },

        showUploadAndHideDownloadButton = function (source, id) {
            var target = source.closest('.verse-field').find('span.file-field');
            var sourceData = source.data();
            var iconTitle = "Upload Audio/Video for verse " + sourceData.verseno;
            var checkboxAttr = " data-verseno='" + sourceData.verseno + "' data-trackerid='" + sourceData.trackerid + "' data-transactionid='" + id + "'";
            var uploadIcon = "<i class='fa fa-upload upload-field' title='" + iconTitle + "' " + checkboxAttr + " ></i>";
            target.html(uploadIcon);
            target.removeClass('hidden');
        },



        showFileError = function (message) {
            $("#lblUploadedMsg").html(message)
            $("#lblUploadedMsg").addClass("field-validation-error");
        },

        showTechinalError = function (failureMessage) {
            console.log(failureMessage);
            alert(validationErrorMessage.TechnicalError);
        },


        getURLParameter = function (name) {
            name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
            var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
            var results = regex.exec(location.search);
            return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
        },

        //Function used to remove querystring
        removeQueryStringFromURL = function (key) {
            var urlValue = document.location.href;

            //Get query string value
            var searchUrl = location.search;

            if (key != "") {
                oldValue = getParameterByName(key);
                removeVal = key + "=" + oldValue;
                if (searchUrl.indexOf('?' + removeVal + '&') != "-1") {
                    urlValue = urlValue.replace('?' + removeVal + '&', '?');
                }
                else if (searchUrl.indexOf('&' + removeVal + '&') != "-1") {
                    urlValue = urlValue.replace('&' + removeVal + '&', '&');
                }
                else if (searchUrl.indexOf('?' + removeVal) != "-1") {
                    urlValue = urlValue.replace('?' + removeVal, '');
                }
                else if (searchUrl.indexOf('&' + removeVal) != "-1") {
                    urlValue = urlValue.replace('&' + removeVal, '');
                }
            }
            else {
                searchUrl = location.search;
                urlValue = urlValue.replace(searchUrl, '');
            }
            history.pushState({ state: 1, rand: Math.random() }, '', urlValue);
        };

    return {
        validateAndSaveFileToCollection: validateAndSaveFileToCollection,
        updateHifzTransaction: updateHifzTransaction,
        loadUploadPopup: loadUploadPopup,
        onClosePopup: onClosePopup,
        deleteAttachment: deleteAttachment,
        clearUploadPopup: clearUploadPopup,
        getURLParameter: getURLParameter,
        initializeDownloadFileRequest: initializeDownloadFileRequest,
        removeQueryStringFromURL: removeQueryStringFromURL,
        downloadHifzCertificateAsPdf: downloadHifzCertificateAsPdf
    }
}();



$(document).ready(function () {
    $(document).on('change', '.verse-check', function () {
        fileUploader.updateHifzTransaction($(this));
    });

    $(document).on('click', '.upload-field', function () {
        fileUploader.loadUploadPopup($(this));
    });

    $(document).on('click', '.delete-attachment', function () {
        fileUploader.deleteAttachment($(this));
    });

    $(document).on('click', '.download-field', function () {
        var filepath = $(this).data('filepath');
    });

    $('#myModal').one('hide.bs.modal', function () {
        fileUploader.clearUploadPopup();
    });

    var isError = fileUploader.getURLParameter('error');
    if (isError != undefined && isError == 'true') {
        fileUploader.loadUploadPopup(undefined, true);
        fileUploader.removeQueryStringFromURL('error');
    }
});