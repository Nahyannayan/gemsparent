﻿function ShowSubWindow(idmId, pageTitle, w, h) {
    $.fancybox({
        type: 'iframe',
        //maxWidth: 300,
        href: idmId,
        maxHeight: 600,
        fitToView: true,
        padding: 6,
        width: '50%',
        height: h,
        autoSize: false,
        openEffect: 'none',
        showLoading: true,
        closeClick: false,
        closeEffect: 'fade',
        'closeBtn': false,
        afterLoad: function () {
            this.title = fancyTitle(pageTitle);
        },
        helpers: {
            overlay: { closeClick: false }, // prevents closing when clicking OUTSIDE fancybox 
            title: { type: 'inside' }
        },
        onComplete: function () {
            $("#fancybox-wrap").css({ 'top': '90px' });
        },
        onCleanup: function () {
            var hfPostBack = $("#fancybox-frame").contents().find('#hfbPB').val();
            
            if (hfPostBack == "Y")
                window.location.reload(true);
        }
    });

    return false;
}
function ShowSubWindowWithClose(idmId, pageTitle, w, h) {
    $.fancybox({
        type: 'iframe',
        //maxWidth: 300,
        href: idmId,
        maxHeight: 600,
        fitToView: true,
        padding: 6,
        width: '50%',
        height: h,
        autoSize: false,
        openEffect: 'none',
        showLoading: true,
        closeClick: true,
        closeEffect: 'fade',
        'closeBtn': true,
        afterLoad: function () {
            this.title = fancyTitle(pageTitle);
        },
        helpers: {
            overlay: { closeClick: false }, // prevents closing when clicking OUTSIDE fancybox 
            title: { type: 'inside' }
        },
        onComplete: function () {
            $("#fancybox-wrap").css({ 'top': '90px' });
        },
        onCleanup: function () {
            var hfPostBack = $("#fancybox-frame").contents().find('#hfbPB').val();

            if (hfPostBack == "Y")
                window.location.reload(true);
        }
    });

    return false;
}
function fancyTitle(title) {
    if (title != '') {
        //var counterText = currentOpts.custom_counterText;
        var container = '<div id="fancybox-custom-title-container" style="text-align:left !important;" class="darkPanelFooter"><span id="fancybox-custom-title" CssClass="TitlePl">' + title + '</span></div>';
        //var $title = $('<span id="fancybox-custom-title" CssClass="TitlePl"></span>');
        //$title.text(title);
        //$container.append($title);
        return container;
    }
}
function FancyDIV() {
    $.fancybox({
        type: 'inline',
        content: '#tblEmp',
        maxHeight: 300,
        fitToView: true,
        padding: 6,
        width: '40%',
        height: '40%',
        autoSize: false,
        openEffect: 'none',
        showLoading: true,
        closeClick: true,
        closeEffect: 'fade'
    });
    return false;
}