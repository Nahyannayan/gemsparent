﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Collections.Generic
Imports System.Collections
Partial Class StudProf_StudMedication
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            Page.Title = OASISConstants.Gemstitle
            If Session("username") Is Nothing Then
                Session("Active_tab") = "Home"
                Session("Site_Path") = ""
                Response.Redirect("~\Login.aspx")
            ElseIf Session("bPasswdChanged") = "False" Then
                Response.Redirect("~\UpdateInfo\ForcePasswordChange.aspx", False)
                'ElseIf Session("bUpdateContactDetails") = "False" Then

                '    If String.IsNullOrEmpty(Convert.ToString(Session("bStuSteps"))) Then


                '        If (Convert.ToString(Session("isNewContact")) = "True") Then
                '            Response.Redirect("~\StudProf\StudentMainDetails.aspx", False)
                '        Else
                '            Response.Redirect(Session("ForceUpdate_stud"), False)
                '        End If
                '    End If
            End If
            If Page.IsPostBack = False Then
                Student_Med_Details(Session("STU_ID"))
            End If
            lblChildName.Text = Session("STU_NAME")
            lbChildNameTop.Text = Session("STU_NAME")
            'trShift.Visible = False
            'trMins.Visible = False
        Catch ex As Exception

        End Try


    End Sub
    Protected Sub btnSaveContinue_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveContinue.Click
        Try
            Dim status As Integer
            Dim transaction As SqlTransaction
            Using conn As SqlConnection = ConnectionManger.GetOASIS_MEDConnection
                transaction = conn.BeginTransaction("SampleTransaction")
                status = callTrans_Student_Med_Details(transaction)
                'Dim lblmsg As Label = CType(Page.Master.FindControl("lblSiteMessage"), Label)
                lblmsg.Text = ""
                If status = 1000 Then
                    lblmsg.CssClass = "alert-warning alert"
                    lblmsg.Text = "Please enter the details"
                    transaction.Rollback()
                ElseIf status <> 0 Then
                    lblmsg.CssClass = "alert-warning alert"
                    lblmsg.Text = "Please enter the details"
                    transaction.Rollback()
                Else
                    'divUploadmsg.CssClass = ""
                    'divUploadmsg.Text = ""
                    lblmsg.CssClass = "alert-warning alert"
                    lblmsg.Text = "Data saved successfully"
                    Session("bStuSteps") = 4
                    UtilityObj.InsertAuditdetailsWithoutGuid(transaction, "edit", "[OASIS_MED].[dbo].[STUDENT_MED_CONSENT]", "STU_ID", "STU_ID", "STU_ID=" + Session("STU_ID").ToString)
                    transaction.Commit()
                    Response.Redirect("~\StudProf\StudentGeneralConsent.aspx", False)
                End If

            End Using
        Catch ex As Exception

        End Try





    End Sub

    Protected Sub btnPrev_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Response.Redirect("~\StudProf\StudHealthDetails.aspx", False)
    End Sub

    Function callTrans_Student_Med_Details(ByVal trans As SqlTransaction) As Integer
        Try
            Dim Status As Integer = 0
            Dim IsMedPerm As Boolean
            Dim isEmegencyTreatment As Boolean
            Dim IsSchoolMed As Boolean
            Dim isArrangePrivate As Boolean
            Dim isImmunization As Boolean
            If rdbConsentMedicationyes.Checked Then
                IsMedPerm = True
            ElseIf rdbConsentMedicationNo.Checked Then

                IsMedPerm = False
            Else
                IsMedPerm = False
            End If


            If rdbEmergenyTreatmentYes.Checked Then
                isEmegencyTreatment = True
            ElseIf rdbEmergenyTreatmentNo.Checked Then

                isEmegencyTreatment = False
            Else
                isEmegencyTreatment = False
            End If


            If rdbIsSchoolMedYes.Checked Then
                IsSchoolMed = True
            ElseIf rdbIsSchoolMedNo.Checked Then

                IsSchoolMed = False
            Else
                IsSchoolMed = False
            End If

            If rdbArrangePrivateYes.Checked Then
                isArrangePrivate = True
            ElseIf rdbArrangePrivateNo.Checked Then

                isArrangePrivate = False
            Else
                isArrangePrivate = False
            End If

           

            'If Convert.ToString(rdoImmunizationConsent.SelectedItem.Value) = "1" Then
            '    isImmunization = True
            'Else
            '    isImmunization = False
            'End If

            Dim pParms(8) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@STU_ID", Session("STU_ID"))

            pParms(1) = New SqlClient.SqlParameter("@STU_MED_IS_MED_PERMISSION", IsMedPerm)

            pParms(2) = New SqlClient.SqlParameter("@STU_MED_IS_EMERGENCY", isEmegencyTreatment)
            pParms(3) = New SqlClient.SqlParameter("@STU_MED_IS_SCHOOL_MED", IsSchoolMed)
            pParms(4) = New SqlClient.SqlParameter("@STU_MED_IS_OWN_MED", isArrangePrivate)
            pParms(5) = New SqlClient.SqlParameter("@STU_MED_IS_IMMUN_CONSENT", 0)
            pParms(6) = New SqlClient.SqlParameter("@OLU_ID", Convert.ToInt32(Session("OLU_ID").ToString))

            pParms(7) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(7).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "Save_STUDENT_MED_CONSENT_GWA", pParms)
            Dim ReturnFlag As Integer = pParms(7).Value
            Return ReturnFlag
        Catch ex As Exception
            Return 1000
        End Try
    End Function

    Sub Student_Med_Details(ByVal STU_ID As String)


        Dim CONN As String = ConnectionManger.GetOASIS_MEDConnectionString
        Dim param(2) As SqlParameter
        param(0) = New SqlParameter("@STU_ID", STU_ID)


        Using readerStudent_D_Detail As SqlDataReader = SqlHelper.ExecuteReader(CONN, CommandType.StoredProcedure, "Get_STUDENT_MEDICATION_INFO", param)
            If readerStudent_D_Detail.HasRows = True Then
                While readerStudent_D_Detail.Read


                    lblChildName.Text = Session("STU_NAME")
                    lblGradeSection.Text = Convert.ToString(readerStudent_D_Detail("GRD_ID_JOIN")) & "-" & Convert.ToString(readerStudent_D_Detail("SCT_DESCR_JOIN"))

                    lblFeeID.Text = Convert.ToString(readerStudent_D_Detail("FEE_ID"))
                    If IsDate(readerStudent_D_Detail("SDOJ")) = True Then
                        lblDateOfJoin.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((readerStudent_D_Detail("SDOJ"))))
                    End If
                   

                    If Not Convert.ToString(readerStudent_D_Detail("STU_MED_IS_MED_PERMISSION")) Is Nothing Then
                        If Convert.ToBoolean(readerStudent_D_Detail("STU_MED_IS_MED_PERMISSION")) Then
                            rdbConsentMedicationyes.Checked = True

                        Else
                            rdbConsentMedicationNo.Checked = True

                        End If
                    End If

                    If Not Convert.ToString(readerStudent_D_Detail("STU_MED_IS_EMERGENCY")) Is Nothing Then
                        If Convert.ToBoolean(readerStudent_D_Detail("STU_MED_IS_EMERGENCY")) Then
                            rdbEmergenyTreatmentYes.Checked = True

                        Else
                            rdbEmergenyTreatmentNo.Checked = True

                        End If
                    End If

                    If Not Convert.ToString(readerStudent_D_Detail("STU_MED_IS_SCHOOL_MED")) Is Nothing Then
                        If Convert.ToBoolean(readerStudent_D_Detail("STU_MED_IS_SCHOOL_MED")) Then
                            rdbIsSchoolMedYes.Checked = True

                        Else
                            rdbIsSchoolMedNo.Checked = True

                        End If
                    End If
                    If Not Convert.ToString(readerStudent_D_Detail("STU_MED_IS_OWN_MED")) Is Nothing Then
                        If Convert.ToBoolean(readerStudent_D_Detail("STU_MED_IS_OWN_MED")) Then
                            rdbArrangePrivateYes.Checked = True

                        Else
                            rdbArrangePrivateNo.Checked = True

                        End If
                    End If
                   
                   
                    'If Not (Convert.ToString(readerStudent_D_Detail("STU_MED_IS_IMMUN_CONSENT"))) Is Nothing Then
                    '    If ((Convert.ToString(readerStudent_D_Detail("STU_MED_IS_IMMUN_CONSENT"))) = True) Then
                    '        rdoImmunizationConsent.ClearSelection()
                    '        rdoImmunizationConsent.Items.FindByValue("1").Selected = True
                    '    Else
                    '        rdoImmunizationConsent.Items.FindByValue("0").Selected = True
                    '    End If

                    'End If


                End While
            Else
            End If
        End Using
    End Sub
End Class
