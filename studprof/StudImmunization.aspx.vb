﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO
Imports System.Web.Configuration
Partial Class StudProf_StudImmunization
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            Page.Title = OASISConstants.Gemstitle
            If Session("username") Is Nothing Then
                Session("Active_tab") = "Home"
                Session("Site_Path") = ""
                Response.Redirect("~\login.aspx")
            ElseIf Session("bPasswdChanged") = "False" Then
                Response.Redirect("~\UpdateInfo\ForcePasswordChange.aspx", False)
                'ElseIf Session("bUpdateContactDetails") = "False" Then

                '    If String.IsNullOrEmpty(Convert.ToString(Session("bStuSteps"))) Then


                '        If (Convert.ToString(Session("isNewContact")) = "True") Then
                '            Response.Redirect("~\StudProf\StudentMainDetails.aspx", False)
                '        Else
                '            Response.Redirect(Session("ForceUpdate_stud"), False)
                '        End If
                '    End If
            End If

          
            'lblChildName.Text = Session("STU_NAME")
            'trShift.Visible = False
            'trMins.Visible = False
        Catch ex As Exception

        End Try


    End Sub

    Protected Sub btnSaveContinue_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Session("bStuSteps") = 5
            Response.Redirect("~\StudProf\StudentGeneralConsent.aspx", False)
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnPrev_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Response.Redirect("~\StudProf\StudMedication.aspx", False)
    End Sub

End Class
