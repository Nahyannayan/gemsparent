﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ParentMaster.master" AutoEventWireup="false" CodeFile="ProfMainInfo.aspx.vb" Inherits="ProfMainInfo" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphParent" runat="Server">

       <!-- Add fancyBox -->
  <script src="../Scripts/jquery-1.9.1.js" type="text/javascript"></script>

     <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.pack.js?1=2"></script>
    <script type="text/javascript" src="../Scripts/fancybox/jquery.fancybox.js?1=2"></script>
    <link type="text/css" href="../Scripts/fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
   <link href="../cssfiles/Popup.css" rel="stylesheet" />
    <style>
        .PopUpMessageStyle {
            background :border-box center #ffffff;      
            width:50%;
            border :1px solid;
            border-radius :4px;
            padding:5px;
        }
    </style>
    <script>
        function KeepStyle() {
            jQuery('.accordionMod').each(function (index) {
                var thisBox = jQuery(this).children(),
                    thisMainIndex = index + 1;
                jQuery(this).attr('id', 'accordion' + thisMainIndex);
                thisBox.each(function (i) {
                    var thisIndex = i + 1,
                        thisParentIndex = thisMainIndex,
                        thisMain = jQuery(this).parent().attr('id'),
                        thisTriggers = jQuery(this).find('.accordion-toggle'),
                        thisBoxes = jQuery(this).find('.accordion-inner');
                    jQuery(this).addClass('panel');
                    thisBoxes.wrap('<div id=\"collapseBox' + thisParentIndex + '_' + thisIndex + '\" class=\"panel-collapse collapse\" />');
                    thisTriggers.wrap('<div class=\"panel-heading\" />');
                    thisTriggers.attr('data-toggle', 'collapse').attr('data-parent', '#' + thisMain).attr('data-target', '#collapseBox' + thisParentIndex + '_' + thisIndex);
                });
                jQuery('.accordion-toggle').prepend('<span class=\"icon\" />');
                jQuery("div.accordion-item:first-child .accordion-toggle").addClass("current");
                jQuery("div.accordion-item:first-child .icon").addClass("iconActive");
                jQuery("div.accordion-item:first-child .panel-collapse").addClass("in");
                jQuery('.accordionMod .accordion-toggle').click(function () {
                    var boxid = $(this).attr("data-target")[$(this).attr("data-target").length - 1];
                    $('input[id$=hfaccordian]').val(boxid);
                    if (jQuery(this).parent().parent().find('.panel-collapse').is('.in')) {
                        jQuery(this).removeClass('current');
                        jQuery(this).find('.icon').removeClass('iconActive');
                    } else {
                        jQuery(this).addClass('current');
                        jQuery(this).find('.icon').addClass('iconActive');
                    }
                    jQuery(this).parent().parent().siblings().find('.accordion-toggle').removeClass('current');
                    jQuery(this).parent().parent().siblings().find('.accordion-toggle > .icon').removeClass('iconActive');
                });
            });
            //----------------------------function end--------------------------------------------------------------------------------
            //function start TABS

            jQuery("#horizontal-tabs").tytabs({
                tabinit: $('input[id$=hftytabs]').val(),
                fadespeed: "fast"
            });
            jQuery("#horizontal-tabs.two").tytabs({
                tabinit: $('input[id$=hftytabs]').val(),
                prefixtabs: "tab_two",
                prefixcontent: "content_two",
                fadespeed: "fast"
            });
            jQuery("#horizontal-tabs.three").tytabs({
                tabinit: $('input[id$=hftytabs]').val(),
                prefixtabs: "tab_three",
                prefixcontent: "content_three",
                fadespeed: "fast"
            });
            jQuery("#horizontal-tabs.four").tytabs({
                tabinit: $('input[id$=hftytabs]').val(),
                prefixtabs: "tab_four",
                prefixcontent: "content_four",
                fadespeed: "fast"
            });
            jQuery("#horizontal-tabs.five").tytabs({
                tabinit: $('input[id$=hftytabs]').val(),
                prefixtabs: "tab_five",
                prefixcontent: "content_five",
                fadespeed: "fast"
            });
            jQuery("#vertical-tabs").tytabs({
                tabinit: $('input[id$=hftytabs]').val(),
                prefixtabs: "tab_v",
                prefixcontent: "content_v",
                fadespeed: "fast"
            });
            jQuery("#vertical-tabs.two").tytabs({
                tabinit: $('input[id$=hftytabs]').val(),
                prefixtabs: "tab_v_two",
                prefixcontent: "content_v_two",
                fadespeed: "fast"
            });
            jQuery("#vertical-tabs.three").tytabs({
                tabinit: $('input[id$=hftytabs]').val(),
                prefixtabs: "tab_v_three",
                prefixcontent: "content_v_three",
                fadespeed: "fast"
            });
            jQuery("#vertical-tabs.four").tytabs({
                tabinit: $('input[id$=hftytabs]').val(),
                prefixtabs: "tab_v_four",
                prefixcontent: "content_v_four",
                fadespeed: "fast"
            });
            jQuery("#vertical-tabs.five").tytabs({
                tabinit: $('input[id$=hftytabs]').val(),
                prefixtabs: "tab_v_five",
                prefixcontent: "content_v_five",
                fadespeed: "fast"
            });
            jQuery(".hideit").click(function () {
                e(this).fadeOut(600)
            });
            jQuery("#toggle-view li h4").click(function () {
                var t = e(this).siblings("div.panel");
                if (t.is(":hidden")) {
                    t.slideDown("200");
                    e(this).siblings("span").html("-")
                } else {
                    t.slideUp("200");
                    e(this).siblings("span").html("+")
                }
            });

            jQuery(function (jQuery) {
                jQuery("#example").popover();
                jQuery("#example_left").popover({
                    placement: 'left'
                });
                jQuery("#example_top").popover({
                    placement: 'top'
                });
                jQuery("#example_bottom").popover({
                    placement: 'bottom'
                });
            });

            //function end TABS

      
            //Toggle mobile menu & search
         $('.toggle-nav').click(function() {
             console.log('toggle nav');
             $('.mobile-nav').slideToggle(200);
             //$('.mobile-search').slideUp(200);
         });
           
            //Mobile menu accordion toggle for sub pages
            $('.mobile-nav > ul > li.menu-item-has-children').append('<div class="accordion-toggle"><div class="fa fa-angle-down"></div></div>');
            $('.mobile-nav .accordion-toggle').click(function() {
                $(this).parent().find('> ul').slideToggle(200);
                $(this).toggleClass('toggle-background');
                $(this).find('.fa').toggleClass('toggle-rotate');
            });
    
        }

        function GetNavigateUrl1(stuid, bmid) {
            var argNames = [stuid, bmid];
           
                var frmDetails = "/studprof/stu_ActionViewDetails.aspx";
                           
                return ShowWindowWithClose(frmDetails + '?Info_id=' + stuid+','+ bmid, 'search', '45%', '85%')
                return false;
           
        }

        function CloseFrame() {
            jQuery.fancybox.close();
        }

    </script>
     <div class="content margin-top30 margin-bottom60">
                    <div class="container">
                        <div class="row">
    <!-- Posts Block -->
    <div class="posts-block col-lg-12 col-md-12 col-sm-12 col-xs-12">

        <div class="title-box">
                            <h3>Student Profile
                            <span class="profile-right">
                                <asp:Label ID="lbChildName" runat="server"></asp:Label>
                            </span></h3>
        </div>

        <div class="col-12">
            <div id="horizontal-tabs">
                <ul class="tabs">
                    <li id="tab1" class="current">Main Info</li>
                    <li id="tab2">Fee Details</li>
                    <li id="tab3">Curriculum</li>
                    <li id="tab4">Attendance</li>
                    <li id="tab5">Notifications</li>
                    <li id="tab6">Others</li>
                </ul>
                <div class="contents">
                    <div class="tabscontent widget category" id="content1">



                        <!-- Accordion -->
                        <div class="accordionMod panel-group">
                            <div class="accordion-item">
                                <h4 class="accordion-toggle">Personal Details</h4>
                                <section class="accordion-inner panel-body">

                                    <div class="table-responsive">


                                        <table align="center" style="width: 100%;" class="table table-striped table-bordered table-responsive text-left my-orders-table">
                                            <tbody>
                                                <tr>
                                                    <th>Date of Birth
                                                    </th>
                                                    <td>
                                                        <asp:Literal ID="txtDob" runat="server"></asp:Literal>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <th>Country of Birth
                                                    </th>
                                                    <td>
                                                        <asp:Literal ID="LTcunbirth" runat="server"></asp:Literal>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th>Place of Birth
                                                    </th>
                                                    <td>
                                                        <asp:Literal ID="LTpob" runat="server"></asp:Literal>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th>Gender</th>
                                                    <td>
                                                        <asp:Literal ID="LTgender" runat="server"></asp:Literal>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th>Nationality
                                                    </th>
                                                    <td>
                                                        <asp:Literal ID="LTnationality" runat="server"></asp:Literal>
                                                    </td>
                                                </tr>

                                                <tr style="display:none;">
                                                    <th>Religion</th>
                                                    <td>
                                                        <asp:Literal ID="LTreligion" runat="server"></asp:Literal>
                                                    </td>
                                                </tr>


                                            </tbody>
                                        </table>


                                    </div>

                                </section>
                            </div>

                            <div class="accordion-item">
                                <h4 class="accordion-toggle">Contact Information</h4>
                                <section class="accordion-inner panel-body">

                                    <!-- Table  -->
                                    <div class="table-responsive">


                                        <table width="100%" cellpadding="8" cellspacing="0" class="table table-striped table-bordered table-responsive text-left my-orders-table">

                                            <tbody>
                                                <tr>
                                                    <th align="center">Fields
                                                    </th>
                                                    <th align="center" >Father</th>
                                                    <th align="center" >Mother</th>
                                                    <th align="center" >Guardian</th>
                                                </tr>
                                                <tr>
                                                    <th class="tdfields">Name</th>
                                                    <td>
                                                        <asp:Literal ID="Ltl_Fname" runat="server"></asp:Literal></td>
                                                    <td>
                                                        <asp:Literal ID="Ltl_Mname" runat="server"></asp:Literal></td>
                                                    <td>
                                                        <asp:Literal ID="Ltl_Gname" runat="server"></asp:Literal></td>
                                                </tr>
                                                <tr>
                                                    <th class="tdfields">Nationality</th>
                                                    <td>
                                                        <asp:Literal ID="Ltl_Fnationality" runat="server"></asp:Literal></td>
                                                    <td>
                                                        <asp:Literal ID="Ltl_Mnationality" runat="server"></asp:Literal></td>
                                                    <td>
                                                        <asp:Literal ID="Ltl_Gnationality" runat="server"></asp:Literal></td>
                                                </tr>
                                                <tr>
                                                    <th class="tdfields">PO Box</th>
                                                    <td>
                                                        <asp:Literal ID="Ltl_Fpob" runat="server"></asp:Literal></td>
                                                    <td>
                                                        <asp:Literal ID="Ltl_Mpob" runat="server"></asp:Literal></td>
                                                    <td>
                                                        <asp:Literal ID="Ltl_Gpob" runat="server"></asp:Literal></td>
                                                </tr>
                                                <tr>
                                                    <th class="tdfields">Emirate</th>
                                                    <td>
                                                        <asp:Literal ID="Ltl_FEmirate" runat="server"></asp:Literal></td>
                                                    <td>
                                                        <asp:Literal ID="Ltl_MEmirate" runat="server"></asp:Literal></td>
                                                    <td>
                                                        <asp:Literal ID="Ltl_GEmirate" runat="server"></asp:Literal></td>
                                                </tr>
                                                <tr>
                                                    <th class="tdfields">Home Phone</th>
                                                    <td>
                                                        <asp:Literal ID="Ltl_FPhoneRes" runat="server"></asp:Literal></td>
                                                    <td>
                                                        <asp:Literal ID="Ltl_MPhoneRes" runat="server"></asp:Literal></td>
                                                    <td>
                                                        <asp:Literal ID="Ltl_GPhoneRes" runat="server"></asp:Literal></td>
                                                </tr>
                                                <tr>
                                                    <th class="tdfields">Office Phone</th>
                                                    <td>
                                                        <asp:Literal ID="Ltl_FOfficePhone" runat="server"></asp:Literal></td>
                                                    <td>
                                                        <asp:Literal ID="Ltl_MOfficePhone" runat="server"></asp:Literal></td>
                                                    <td>
                                                        <asp:Literal ID="Ltl_GOfficePhone" runat="server"></asp:Literal></td>
                                                </tr>
                                                <tr>
                                                    <th class="tdfields">Mobile</th>
                                                    <td>
                                                        <asp:Literal ID="Ltl_FMobile" runat="server"></asp:Literal></td>
                                                    <td>
                                                        <asp:Literal ID="Ltl_MMobile" runat="server"></asp:Literal></td>
                                                    <td>
                                                        <asp:Literal ID="Ltl_GMobile" runat="server"></asp:Literal></td>
                                                </tr>
                                                <tr>
                                                    <th class="tdfields">Email</th>
                                                    <td>
                                                        <asp:Literal ID="Ltl_FEmail" runat="server"></asp:Literal></td>
                                                    <td>
                                                        <asp:Literal ID="Ltl_MEmail" runat="server"></asp:Literal></td>
                                                    <td>
                                                        <asp:Literal ID="Ltl_GEmail" runat="server"></asp:Literal></td>
                                                </tr>
                                                <tr>
                                                    <th class="tdfields">Fax</th>
                                                    <td>
                                                        <asp:Literal ID="Ltl_FFax" runat="server"></asp:Literal></td>
                                                    <td>
                                                        <asp:Literal ID="Ltl_MFax" runat="server"></asp:Literal></td>
                                                    <td>
                                                        <asp:Literal ID="Ltl_GFax" runat="server"></asp:Literal></td>
                                                </tr>
                                                <tr>
                                                    <th class="tdfields">Occupation</th>
                                                    <td>
                                                        <asp:Literal ID="Ltl_FOccupation" runat="server"></asp:Literal></td>
                                                    <td>
                                                        <asp:Literal ID="Ltl_MOccupation" runat="server"></asp:Literal></td>
                                                    <td>
                                                        <asp:Literal ID="Ltl_GOccupation" runat="server"></asp:Literal></td>
                                                </tr>
                                                <tr>

                                                    <th class="tdfields">Company</th>
                                                    <td>
                                                        <asp:Literal ID="Ltl_FCompany" runat="server"></asp:Literal></td>
                                                    <td>
                                                        <asp:Literal ID="Ltl_MCompany" runat="server"></asp:Literal></td>
                                                    <td>
                                                        <asp:Literal ID="Ltl_GCompany" runat="server"></asp:Literal></td>
                                                </tr>
                                            </tbody>
                                        </table>


                                    </div>
                                    <!-- /Table  -->

                                </section>
                            </div>

                            <div class="accordion-item">
                                <h4 class="accordion-toggle">Joining Details</h4>
                                <section class="accordion-inner panel-body">


                                    <div class="table-responsive">


                                        <table align="center" width="100%" class="table table-striped table-bordered table-responsive text-left my-orders-table">
                                            <tbody>
                                                <tr>
                                                    <th>Student ID
                                                    </th>
                                                    <td>
                                                        <asp:Literal ID="txtFee_ID" runat="server"></asp:Literal>
                                                    </td>
                                                </tr>
                                                <tr style="display:none;">
                                                    <th>MOE Reg#</th>
                                                    <td>
                                                        <asp:Literal ID="txtMOE_No" runat="server"></asp:Literal>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th>Joining Date
                                                    </th>
                                                    <td>
                                                        <asp:Literal ID="txtDOJ" runat="server"></asp:Literal>
                                                    </td>
                                                </tr>
                                                <tr style="display:none;">
                                                    <th>Transfer Type
                                                    </th>
                                                    <td>
                                                        <asp:Literal ID="TLtftype" runat="server"></asp:Literal>
                                                    </td>
                                                </tr>
                                                <tr style="display:none;">
                                                    <th>Date of Join(Ministry)
                                                    </th>
                                                    <td>
                                                        <asp:Literal ID="txtMINDOJ" runat="server"></asp:Literal>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <th>Joining Academic Year</th>
                                                    <td>
                                                        <asp:Literal ID="txtACD_ID_Join" runat="server"></asp:Literal>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th>Joining Grade
                                                    </th>
                                                    <td>
                                                        <asp:Literal ID="txtGRD_ID_Join" runat="server"></asp:Literal>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th>Joining Section</th>
                                                    <td>
                                                        <asp:Literal ID="txtSCT_ID_JOIN" runat="server"></asp:Literal>
                                                    </td>
                                                </tr>
                                                <tr id="trShift" runat="server">
                                                    <th>Joining Shift</th>

                                                    <td>
                                                        <asp:Literal ID="txtJoin_Shift" runat="server"></asp:Literal></td>
                                                    <th>Joining Stream</th>

                                                    <td>
                                                        <asp:Literal ID="txtJoin_Stream" runat="server"></asp:Literal></td>
                                                </tr>
                                                <tr id="trMins" runat="server">
                                                    <th>Ministry List</th>

                                                    <td>
                                                        <asp:Literal ID="LTminlist" runat="server"></asp:Literal></td>
                                                    <th>Ministry List Type</th>

                                                    <td>
                                                        <asp:Literal ID="LTminlistType" runat="server"></asp:Literal></td>
                                                </tr>
                                                   <tr>
                                                    <th>House
                                                    </th>
                                                    <td>
                                                        <asp:Literal ID="txtHouse" runat="server"></asp:Literal>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th>Emergency Contact</th>

                                                    <td>
                                                        <asp:Literal ID="LTemgcon" runat="server"></asp:Literal></td>
                                                </tr>
                                                <tr>
                                                    <th>Fee Sponsor</th>

                                                    <td>
                                                        <asp:Literal ID="txtFee_Spon" runat="server"></asp:Literal></td>
                                                </tr>
                                            </tbody>
                                        </table>


                                    </div>


                                </section>
                            </div>
                            <div class="accordion-item">
                                <h4 class="accordion-toggle">Document Details</h4>
                                <section class="accordion-inner panel-body">


                                    <!-- Table  -->
                                    <div class="table-responsive">


                                        <table align="center" style="width: 100%;" class="table table-striped table-bordered table-responsive text-left my-orders-table">
                                            <tbody>
                                                <tr>
                                                    <th>Passport Number
                                                    </th>
                                                    <td>
                                                        <asp:Literal ID="txtPNo" runat="server"></asp:Literal>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th>Passport Issue Place</th>
                                                    <td>
                                                        <asp:Literal ID="txtPIssPlace" runat="server"></asp:Literal>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th>Passport Issue Date
                                                    </th>
                                                    <td>
                                                        <asp:Literal ID="txtPIssDate" runat="server"></asp:Literal>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th>Passport Expiry Date</th>
                                                    <td>
                                                        <asp:Literal ID="txtPExpDate" runat="server"></asp:Literal>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th>National ID
                                                    </th>
                                                    <td>
                                                        <asp:Literal ID="txtEmiratesId" runat="server"></asp:Literal>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th>Visa Number
                                                    </th>
                                                    <td>
                                                        <asp:Literal ID="txtVNo" runat="server"></asp:Literal>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th>Visa Issue Place</th>
                                                    <td>
                                                        <asp:Literal ID="txtVIssPlace" runat="server"></asp:Literal>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th>Visa Issue Date</th>
                                                    <td>
                                                        <asp:Literal ID="txtVIssDate" runat="server"></asp:Literal>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th>Visa Expiry Date</th>
                                                    <td>
                                                        <asp:Literal ID="txtVExpDate" runat="server"></asp:Literal>
                                                    </td>
                                                </tr>
                                                <tr style="display:none;">
                                                    <th>Issuing Authority
                                                    </th>
                                                    <td>
                                                        <asp:Literal ID="txtVIssAuth" runat="server"></asp:Literal>
                                                    </td>
                                                </tr>
                                                








                                            </tbody>
                                        </table>


                                    </div>
                                    <!-- /Table  -->

                                </section>
                            </div>
                            <div class="accordion-item">
                                <h4 class="accordion-toggle">Language Details</h4>
                                <section class="accordion-inner panel-body">

                                    <!-- Table  -->
                                    <div class="table-responsive">


                                        <table align="center" style="width: 100%;" class="table table-striped table-bordered table-responsive text-left my-orders-table">
                                            <tbody>
                                                <tr>
                                                    <th>First Language
                                                    </th>
                                                    <td>
                                                        <asp:Literal ID="ltFirstLang" runat="server"></asp:Literal>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <th>Other Languages
                                                    </th>
                                                    <td>
                                                        <asp:Literal ID="ltOthLang" runat="server"></asp:Literal>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th>Proficiency in English
                                                    </th>
                                                    <td>Reading&nbsp;&nbsp;&nbsp;: &nbsp;&nbsp;&nbsp;<span><asp:Literal ID="ltProEng_R" runat="server"></asp:Literal></span>&nbsp;&nbsp;&nbsp;
         
            Writing&nbsp;&nbsp;&nbsp;: &nbsp;&nbsp;&nbsp;<span><asp:Literal ID="ltProEng_W" runat="server"></asp:Literal></span>&nbsp;&nbsp;&nbsp;
            
           Speaking&nbsp;&nbsp;&nbsp;: &nbsp;&nbsp;&nbsp;<span><asp:Literal ID="ltProEng_S" runat="server"></asp:Literal></span>&nbsp;&nbsp;&nbsp;
                                                    </td>
                                                </tr>



                                            </tbody>
                                        </table>


                                    </div>
                                    <!-- /Table  -->

                                </section>
                            </div>

                            
                        </div>
                        <!-- /Accordin -->

                        <!--Accordion Ends -->


                    </div>
                    <div class="tabscontent widget category" id="content2">

                        <!-- Accordion -->
                        <div class="accordionMod panel-group">
                            <div class="accordion-item">
                                <h4 class="accordion-toggle">Fee Details for the current Term/Month <span style="float:right;"><%= IIf(Session("BSU_CURRENCY") Is Nothing, "", "(Currency  " & Session("BSU_CURRENCY") & ")")%></span> </h4>
                                <section class="accordion-inner panel-body">
                                    <div class="table-responsive">
                                        <table rules="all" id="gvFeeDetails" width="100%">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <asp:GridView ID="gvFeeDetails1" runat="server"
                                                            AutoGenerateColumns="False"
                                                            EmptyDataText="No records available."
                                                            Width="100%" CssClass="table table-striped table-bordered table-responsive text-left my-orders-table" BorderStyle="None" BorderWidth="0px"
                                                            HeaderStyle-HorizontalAlign="Center">
                                                            <EmptyDataRowStyle Wrap="True" HorizontalAlign="Center" />
                                                            <Columns>
                                                                <asp:TemplateField SortExpression="FEE_TYPE" HeaderText="Fee Description">
                                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                    <ItemStyle HorizontalAlign="left" ></ItemStyle>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblFEE_TYPE" runat="server" Text='<%# Bind("FEE_DESCR") %>' style="text-transform: uppercase;"></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField SortExpression="OPENING" HeaderText="Opening Balance">
                                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                    <ItemStyle HorizontalAlign="RIGHT" ></ItemStyle>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblOpening" runat="server" Text='<%# Bind("OPENING") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField SortExpression="Monthly_Amount" HeaderText="Invoiced Amount">
                                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                    <ItemStyle HorizontalAlign="RIGHT" ></ItemStyle>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblMC" runat="server" Text='<%# Bind("Monthly_Amount") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>



                                                                <asp:TemplateField SortExpression="Conc_Amount" HeaderText="Concession">
                                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                    <ItemStyle HorizontalAlign="RIGHT" ></ItemStyle>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblCC" runat="server" Text='<%# Bind("Conc_Amount") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>



                                                                <asp:TemplateField SortExpression="Paid_Amount" HeaderText="Paid Amount">

                                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                    <ItemStyle HorizontalAlign="RIGHT" ></ItemStyle>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblPA" runat="server" Text='<%# Bind("Paid_Amount") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>



                                                                <asp:TemplateField SortExpression="Amount" HeaderText="Balance Amount">

                                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                    <ItemStyle HorizontalAlign="RIGHT" ></ItemStyle>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblBal" runat="server" Text='<%# Bind("Amount") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                            <PagerStyle CssClass="gridpager" />
                                                        </asp:GridView>

                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </section>
                            </div>
                            <div class="accordion-item">
                                <h4 class="accordion-toggle">Payment History<span style="float:right;"><%= IIf(Session("BSU_CURRENCY") Is Nothing, "", "(Currency  " & Session("BSU_CURRENCY") & ")")%></span></h4>
                                <section class="accordion-inner panel-body">


                                    <div class="table-responsive">

                                        <table rules="all" id="gvPaymentHist" width="100%">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <asp:GridView ID="gvPaymentHist1" runat="server"
                                                            AutoGenerateColumns="False" EmptyDataText="No records available."
                                                            Width="100%" CssClass="table table-striped table-bordered table-responsive text-left my-orders-table" BorderStyle="None" BorderWidth="0px"
                                                            HeaderStyle-HorizontalAlign="Center">
                                                            <EmptyDataRowStyle Wrap="True" HorizontalAlign="Center" />
                                                            <Columns>

                                                                <asp:BoundField DataField="Sl_No" HeaderText="Sr.No." ReadOnly="True" />
                                                                <asp:TemplateField SortExpression="FCL_DATE" HeaderText="Receipt Date">

                                                                    <ItemStyle HorizontalAlign="center"></ItemStyle>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblFCL_DATE" runat="server" Text='<%# Bind("FCL_DATE") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField SortExpression="FCL_RECNO" HeaderText="Receipt Number">
                                                                    <ItemStyle HorizontalAlign="center"></ItemStyle>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblFCL_RECNO" runat="server" Text='<%# Bind("FCL_RECNO") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField SortExpression="FCL_NARRATION" HeaderText="Narration">
                                                                    <ItemStyle HorizontalAlign="left"></ItemStyle>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblFCL_NARRATION" runat="server" Text='<%# Bind("FCL_NARRATION") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField SortExpression="FCL_AMOUNT" HeaderText="Amount">
                                                                    <ItemStyle HorizontalAlign="RIGHT"></ItemStyle>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblFCL_AMOUNT" runat="server" Text='<%# Bind("FCL_AMOUNT") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                            </Columns>
                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                            <PagerStyle CssClass="gridpager" />
                                                        </asp:GridView>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>



                                    </div>


                                </section>
                            </div>
                        </div>

                        <!--Accordion Ends -->

                    </div>
                    <div class="tabscontent widget category" id="content3">

                        <!-- Accordion -->
                        <div class="accordionMod panel-group">
                            <div class="accordion-item">
                                <h4 class="accordion-toggle">Subject List</h4>
                                <section class="accordion-inner panel-body">
                                    <div class="table-responsive">

                                        <table cellspacing="0" rules="all" id="cphParent_gvSubjects" width="100%">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <asp:GridView ID="gvSubjects" runat="server" AllowPaging="true" AutoGenerateColumns="False"
                                                            EmptyDataText="No records available." PageSize="25"
                                                            Width="100%" CssClass="table table-striped table-bordered table-responsive text-left my-orders-table" BorderStyle="None" BorderWidth="0px"
                                                            HeaderStyle-HorizontalAlign="Center">
                                                            <EmptyDataRowStyle Wrap="True" HorizontalAlign="Center" />
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="Subject">
                                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                    <ItemStyle Width="300px" />
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblSubject" runat="server" Text='<%# Bind("SBG_DESCR") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Group" Visible="False">
                                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                    <ItemStyle Width="300px" />
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblGroup" runat="server" Text='<%# Bind("SGR_DESCR") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Teacher">
                                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                    <ItemStyle Width="300px" />
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblTutor" runat="server" Text='<%# Bind("TEACHERS") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                            <PagerStyle CssClass="gridpager" />

                                                        </asp:GridView>
                                                    </td>
                                                </tr>

                                            </tbody>
                                        </table>
                                    </div>
                                </section>
                            </div>

                        </div>


                        <!--Accordion Ends -->

                    </div>

                    <div class="tabscontent widget category" id="content4">

                        <!-- Accordion -->
                        <div class="accordionMod panel-group">
                            <div class="accordion-item">
                                <h4 class="accordion-toggle">Attendance Details</h4>
                                <section class="accordion-inner panel-body">


                                    <!-- Table  -->
                                    <div class="table-responsive">

                                        <table id="table1" class="table table-striped table-bordered table-responsive text-left my-orders-table">
                                            <tbody>

                                                <tr>
                                                    <td class="tdfields">
                                                        <asp:Literal ID="ltTitleAcd" runat="server"></asp:Literal></td>

                                                    <td colspan="3">
                                                        <asp:Literal ID="ltAcdWorkingDays" runat="server"></asp:Literal></td>
                                                </tr>
                                                <tr>
                                                    <td class="tdfields">
                                                        <asp:Literal ID="ltTotTilldate" runat="server"></asp:Literal></td>

                                                    <td colspan="3">
                                                        <asp:Literal ID="ltTotWorkTilldate" runat="server"></asp:Literal></td>
                                                </tr>
                                                <tr>
                                                    <td class="tdfields">
                                                        <asp:Literal ID="ltMrkTilldate" runat="server"></asp:Literal></td>

                                                    <td colspan="3">
                                                        <asp:Literal ID="ltAttMarkTilldate" runat="server"></asp:Literal></td>
                                                </tr>
                                                <tr>
                                                    <td class="tdfields">Days Present</td>

                                                    <td colspan="3">
                                                        <asp:Literal ID="ltDayPresent" runat="server"></asp:Literal></td>
                                                </tr>
                                                <tr>
                                                    <td class="tdfields">Days Absent</td>

                                                    <td colspan="3">
                                                        <asp:Literal ID="ltDayAbsent" runat="server"></asp:Literal></td>
                                                </tr>
                                                <tr>
                                                    <td class="tdfields">Days on Leave</td>

                                                    <td colspan="3">
                                                        <asp:Literal ID="ltDayLeave" runat="server"></asp:Literal></td>
                                                </tr>
                                                <tr>
                                                    <td class="tdfields">Percentage of Attendance</td>

                                                    <td colspan="3">
                                                        <asp:Literal ID="ltPerAtt" runat="server"></asp:Literal></td>
                                                </tr>
                                                <tr style="display:none;">
                                                    <td colspan="5">
                                                        <asp:LinkButton ID="lnkmore" runat="server" OnClientClick="ShowWindowWithClose()">Attendance Summary Graph</asp:LinkButton>
                                                    </td>
                                                </tr>
                                        </table>

                                    </div>
                                    <!-- /Table  -->

                                </section>
                            </div>

                        </div>
                        <!--Accordion Ends -->

                         <!-- Accordion -->
                        <div class="accordionMod panel-group">
                            <div class="accordion-item">
                                <h4 class="accordion-toggle">Attendance Graph By Month</h4>
                                <section class="accordion-inner panel-body">

                                    <div class="container-fluide">
                                            <div class="row"><div class="col-12 col-lg-12">
                                                <div class="card">
                                        <telerik:RadHtmlChart runat="server" ID="radAttendanceChart" Height="250" Skin="Metro">
                                                    <PlotArea>
                                                        <Series>
                                                            <telerik:ColumnSeries Name="TMONTH" DataFieldY="TOT_ATT">
                                                                <TooltipsAppearance  />
                                                                <LabelsAppearance Visible="false" />
                                                            </telerik:ColumnSeries>
                                                        </Series>
                                                        <XAxis DataLabelsField="TMONTH">
                                                        </XAxis>
                                                        <YAxis>
                                                            <LabelsAppearance  />
                                                        </YAxis>
                                                    </PlotArea>
                                                    <Legend>
                                                        <Appearance Visible="false" />
                                                    </Legend>
                                                    <ChartTitle Text="Attendance By Month">
                                                    </ChartTitle>
                                                </telerik:RadHtmlChart>
                                                </div>
                                            </div></div>
                                        </div>

                                </section>
                            </div>
                        </div>
                        <!--Accordion Ends -->

                         <!-- Accordion -->
                        <div class="accordionMod panel-group">
                            <div class="accordion-item">
                                <h4 class="accordion-toggle">Attendance Graph By Weekly Absent Pattern</h4>
                                <section class="accordion-inner panel-body">

                                    <div class="container-fluide">

                                            <div class="row">
                                                <div class="col-lg-12 col-md-12 col-sm-12">
                                                <div class="card">
                                    <telerik:RadHtmlChart runat="server" ID="radAttendanceABSChart" Height="250" Skin="Metro">
                                                    <PlotArea>
                                                        <Series>
                                                            <telerik:ColumnSeries Name="TWEEK" DataFieldY="TOT_ABSCOUNT" >
                                                                <TooltipsAppearance  />
                                                                <LabelsAppearance Visible="false" />
                                                            </telerik:ColumnSeries>
                                                        </Series>
                                                        <XAxis DataLabelsField="TWEEK">
                                                        </XAxis>
                                                        <YAxis   MinValue="0" MaxValue="10">
                                                            <LabelsAppearance Step="1" />
                                                        </YAxis>
                                                    </PlotArea>
                                                    <Legend>
                                                        <Appearance Visible="false" />
                                                    </Legend>
                                                    <ChartTitle Text="Weekly Absent Pattern">
                                                    </ChartTitle>
                                                </telerik:RadHtmlChart>
                                                    </div>
                                                </div>
                                                </div>
                                        </div>

                                </section>
                            </div>
                        </div>
                        <!--Accordion Ends -->

                         <!-- Accordion -->
                        <div class="accordionMod panel-group">
                            <div class="accordion-item">
                                <h4 class="accordion-toggle">Attendance Graph By Weekly Late Pattern</h4>
                                <section class="accordion-inner panel-body">

                                    <div class="container-fluide">

                                            <div class="row">
                                            <div class="col-lg-12 col-md-12 col-sm-12">
                                                <div class="card">
                                        <telerik:RadHtmlChart runat="server" ID="radAttendanceLateChart" Height="250" Skin="Metro">
                                                    <PlotArea>
                                                        <Series>
                                                            <telerik:ColumnSeries Name="TWEEK" DataFieldY="TOT_LCOUNT">
                                                                <TooltipsAppearance  />
                                                                <LabelsAppearance Visible="false" />
                                                            </telerik:ColumnSeries>
                                                        </Series>
                                                        <XAxis DataLabelsField="TWEEK">
                                                        </XAxis>
                                                        <YAxis  MinValue="0" MaxValue="10">
                                                           
                                                            <LabelsAppearance Step="1"   />
                                                        </YAxis>
                                                    </PlotArea>
                                                    <Legend>
                                                        <Appearance Visible="false" />
                                                    </Legend>
                                                    <ChartTitle Text="Weekly Late Pattern">
                                                    </ChartTitle>
                                                </telerik:RadHtmlChart>
                                                </div>
                                                    
                                                </div>
                                                </div>
                                        </div>

                                </section>
                            </div>
                        </div>
                        <!--Accordion Ends -->

                    </div>

                    <div class="tabscontent widget category" id="content5">

                        <!-- Accordion -->
                        <!-- calling another aspx page into iframe -->
                            <iframe src="Notifications.aspx" width="100%" height="600px" class="no-border"></iframe>
                        <!-- /calling another aspx page into iframe -->
                        <div class="accordionMod panel-group" style="display:none;">

                            <div class="accordion-item">
                                <h4 class="accordion-toggle">Email/SMS History</h4>
                                <section class="accordion-inner panel-body">


                                    <!-- Table  -->
                                    <div class="table-responsive">


                                        <h4>SMS Sent</h4>


                                        <table class="table table-striped table-bordered table-responsive text-left my-orders-table" cellspacing="0" rules="all" id="cphParent_ComManageSMS1_GridMSMS" style="border-width: 0px; border-style: None; width: 100%; border-collapse: collapse;">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <asp:GridView ID="GridMSMS" AutoGenerateColumns="False" runat="server"
                                                            AllowPaging="True" EmptyDataText="No SMS sent"
                                                            OnPageIndexChanging="GridMSMS_PageIndexChanging" PageSize="10"
                                                            Width="100%" CssClass="table table-striped table-bordered table-responsive text-left my-orders-table" BorderStyle="None" BorderWidth="0px"
                                                            HeaderStyle-HorizontalAlign="Center">

                                                            <Columns>
                                                                <asp:TemplateField HeaderText="ID" Visible="False">

                                                                    <ItemTemplate>
                                                                        <center>
                                <%#Eval("LOG_CMS_ID")%></center>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Sms Text">
                                                                    <ItemTemplate>
                                                                        <asp:Label
                                                                            ID="T12lblview" runat="server" Text='<%#Eval("tempview")%>' CssClass="girdpoplbl"></asp:Label><asp:LinkButton
                                                                                ID="lblDetail" runat="server" Visible='<%# BIND("bShow") %>' CssClass="viewdetails">...Detail</asp:LinkButton>

                                                                        <div id="plR_loc" runat="server" class="gridMsg" visible='<%# BIND("bShow") %>'>
                                                                            <asp:Literal ID="ltRemarks" runat="server" Text='<%# BIND("CMS_SMS_TEXT") %>'>
                                                                            </asp:Literal>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Mobile Number">

                                                                    <ItemTemplate>
                                                                        <center>
                                <%#Eval("LOG_MOBILE_NUMBER")%></center>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Sent Date">

                                                                    <ItemTemplate>
                                                                        <center>
                                <%#Eval("LOG_ENTRY_DATE")%></center>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Status" Visible="False">

                                                                    <ItemTemplate>
                                                                        <center>
                                <%#Eval("status")%></center>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                            </Columns>

                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                            <PagerStyle CssClass="gridpager" />
                                                        </asp:GridView>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>

                                    </div>
                                </section>
                            </div>

                            <div class="accordion-item">
                                <h4 class="accordion-toggle">Newsletter and Plain Text Emails</h4>
                                <section class="accordion-inner panel-body">


                                    <!-- Table  -->
                                    <div class="table-responsive">

                                        <table class="table table-striped table-bordered table-responsive text-left my-orders-table" cellspacing="0" rules="all" id="cphParent_ComManageSMS1_GridEmails" style="border-width: 0px; border-style: None; width: 100%; border-collapse: collapse;">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <asp:GridView ID="GridEmails" AutoGenerateColumns="False" runat="server"
                                                            AllowPaging="True" EmptyDataText="No Emails Sent"
                                                            PageSize="10" Width="100%" CssClass="table table-striped table-bordered table-responsive text-left my-orders-table" BorderStyle="None" BorderWidth="0px"
                                                            HeaderStyle-HorizontalAlign="Center">

                                                            <Columns>
                                                                <asp:TemplateField HeaderText="ID" Visible="False">

                                                                    <ItemTemplate>
                                                                        <center>
                                <%#Eval("LOG_EML_ID")%></center>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Title">

                                                                    <ItemTemplate>

                                                                        <asp:Label ID="T12lblview" runat="server" Text='<%#Eval("tempview")%>'></asp:Label>
                                                                        <asp:LinkButton ID="lblDetailEmail" runat="server" Visible='<%# BIND("bShow") %>' CssClass="viewdetails">...Detail</asp:LinkButton>
                                                                        <div id="plR_email" runat="server" class="gridMsg" visible='<%# BIND("bShow") %>'>
                                                                            <asp:Literal ID="ltRemarksEmail" runat="server" Text='<%# BIND("EML_TITLE") %>'>
                                                                            </asp:Literal>
                                                                        </div>


                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Type">

                                                                    <ItemTemplate>
                                                                        <center>
                                <%#Eval("EmailType")%></center>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Email ID">

                                                                    <ItemTemplate>
                                                                        <center>
                                <%#Eval("LOG_EMAIL_ID")%></center>

                                                                    </ItemTemplate>
                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Sent Date">

                                                                    <ItemTemplate>
                                                                        <center>
                                <%#Eval("LOG_ENTRY_DATE")%></center>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Status" Visible="False">

                                                                    <ItemTemplate>
                                                                        <center>
                                <%#Eval("status")%></center>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                            </Columns>

                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                            <PagerStyle CssClass="gridpager" />
                                                        </asp:GridView>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>



                                    </div>
                                    <!-- /Table  -->

                                </section>
                            </div>


                        </div>


                        <!--Accordion Ends -->

                    </div>

                    <div class="tabscontent widget category" id="content6">

                        <!-- Accordion -->
                        <div class="accordionMod panel-group">

                            
                             <div class="accordion-item">
                                <h4 class="accordion-toggle">Achievements</h4>
                                <section class="accordion-inner panel-body">


                                    <!-- Table  -->
                                    <div class="table-responsive">


                                        <table class="table table-striped table-bordered table-responsive text-left my-orders-table" cellspacing="0" rules="all" width="100%">
                                            <tbody>

                                                <tr>
                                                    <td>
                                                        <asp:GridView ID="gvStudGrade_ACHV" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                                            EmptyDataText="No Records Available"
                                                            PageSize="15" CssClass="table table-striped table-bordered table-responsive text-left my-orders-table" BorderStyle="None" BorderWidth="0px"
                                                            HeaderStyle-HorizontalAlign="Center" Width="100%">
                                                            <EmptyDataRowStyle Wrap="True" HorizontalAlign="Center" />
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="INCIDENTID" Visible="False">
                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblMRSID" runat="server" Text='<%# Bind("ID")%>'></asp:Label>
                                                                        <asp:Label ID="lblStudent" runat="server" Text='<%# Eval("STUDENT_ID")%>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Date">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblAchvDate" runat="server" Text='<%# Format(Eval("INCIDENT_DATE"), "dd/MMM/yyyy")%>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                               
                                                                <asp:TemplateField HeaderText="Category">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblShift" runat="server" Text='<%# Bind("INCIDENT_TYPE")%>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Remarks">
                                                                    <ItemTemplate>
                                                                      <asp:Label 
                                                                            ID="T12lblview1" runat="server" Text='<%#Eval("tempview")%>' CssClass="girdpoplbl"  ></asp:Label><asp:LinkButton
                                                                                ID="lblDetail1" runat="server" Visible='<%# BIND("bShow") %>' CssClass="viewdetails">...Detail</asp:LinkButton>

                                                                        <div id="plR_loc1" Visible='<%# BIND("bShow") %>' class="PopUpMessageStyle"  runat="server"  ><%--visible='<%#If(Eval("bShow"), False, True)%>'--%>
                                                                            <asp:Literal ID="ltRemarks1" runat="server" Text='<%# Bind("DESCRIPTION")%>'>
                                                                            </asp:Literal>
                                                                        </div>
                                                                         <ajaxToolkit:PopupControlExtender ID="pcet_Lib_I1" runat="server"
                                                                            OffsetX="-150" PopupControlID="plR_loc1"
                                                                            Position="Bottom" TargetControlID="lblDetail1">
                                                                        </ajaxToolkit:PopupControlExtender>
                                                                       <%-- &nbsp;<asp:LinkButton ID="LinkButton1" runat="server" CommandArgument='<%# GetNavigateUrl1(Eval("STU_ID").ToString() & "," & Eval("BM_ID").ToString()) %>'
                                                                            OnClientClick='<%# GetNavigateUrl1(Eval("STU_ID").ToString() & "," & Eval("BM_ID").ToString()) %>'
                                                                            Width="35px">View</asp:LinkButton>&nbsp;--%>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>

                                                        </asp:GridView>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>



                                    </div>
                                    <!-- /Table  -->

                                </section>
                            </div>
                            <div class="accordion-item" id="pbd2" runat="server">
                                <h4 class="accordion-toggle">Behavioural Merits</h4>
                                <section class="accordion-inner panel-body">


                                    <!-- Table  -->
                                    <div class="table-responsive">

                                        <table class="table table-striped table-bordered table-responsive text-left my-orders-table" cellspacing="0" rules="all" id="cphParent_gvStudGrade" width="100%">
                                            <tbody>

                                                <tr>
                                                    <td>
                                                        <asp:GridView ID="gvStudGrade_POS" runat="server" AllowPaging="True"
                                                            PageSize="15" EmptyDataText="No Records Available"
                                                            AutoGenerateColumns="false"
                                                            CssClass="table table-striped table-bordered table-responsive text-left my-orders-table" BorderStyle="None" BorderWidth="0px" Width="100%"
                                                            HeaderStyle-HorizontalAlign="Center">
                                                            <EmptyDataRowStyle Wrap="True" HorizontalAlign="Center" />
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="INCIDENTID" Visible="False">
                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblStudId" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                                                                        <asp:Label ID="lblStudent" runat="server" Text='<%# Eval("STUDENT_ID")%>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Date">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblEntry" runat="server" Text='<%# Format(Eval("ENTRY_DATE"), "dd/MMM/yyyy")%>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                              <%--  <asp:TemplateField HeaderText="Staff Name">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblIncidt" runat="server" Text='<%# Bind("INCIDENT_TYPE")%>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>--%>
                                                                <asp:TemplateField HeaderText="Category">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblShift" runat="server" Text='<%# Bind("INCIDENT_TYPE")%>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Remarks">
                                                                    <ItemTemplate>
                                                                       <asp:Label  ID="T12lblview2" runat="server" Text='<%#Eval("tempview")%>' CssClass="girdpoplbl" ></asp:Label>
                                                                        <asp:LinkButton
                                                                                ID="lblDetail2" runat="server" Visible='<%# BIND("bShow") %>' CssClass="viewdetails">...Detail</asp:LinkButton>

                                                                        <div id="plR_loc2" Visible='<%# BIND("bShow") %>' class="PopUpMessageStyle" runat="server"  ><%--Visible='<%#If(Eval("bShow"), False, True)%>'--%>
                                                                            <asp:Literal ID="ltRemarks2" runat="server" Text='<%# Bind("DESCRIPTION")%>'>
                                                                            </asp:Literal>
                                                                        </div>
                                                                        <ajaxToolkit:PopupControlExtender ID="pcet_Lib_I2" runat="server"
                                                                            OffsetX="-150" PopupControlID="plR_loc2"
                                                                            Position="Bottom" TargetControlID="lblDetail2">
                                                                        </ajaxToolkit:PopupControlExtender>
                                                                       <%-- &nbsp;<asp:LinkButton ID="LinkButton1" runat="server" CommandArgument='<%# GetNavigateUrl1(Eval("STU_ID").ToString() & "," & Eval("BM_ID").ToString()) %>'
                                                                            OnClientClick='<%# GetNavigateUrl1(Eval("STU_ID").ToString() & "," & Eval("BM_ID").ToString()) %>'
                                                                            Width="35px">View</asp:LinkButton>&nbsp;--%>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>


                                                        </asp:GridView>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>


                                    </div>
                                    <!-- /Table  -->

                                </section>
                            </div>
                            <div class="accordion-item" id="nbd2" runat="server">
                                <h4 class="accordion-toggle">Behavioural Demerits</h4>
                                <section class="accordion-inner panel-body">


                                    <!-- Table  -->
                                    <div class="table-responsive">

                                        <table class="table table-striped table-bordered table-responsive text-left my-orders-table" cellspacing="0" rules="all" id="cphParent_gvStudGrade11" width="100%">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <asp:GridView ID="gvStudGrade" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                                            EmptyDataText="No Records Available"
                                                            PageSize="15" Width="100%"
                                                            CssClass="table table-striped table-bordered table-responsive text-left my-orders-table" BorderStyle="None" BorderWidth="0px"
                                                            HeaderStyle-HorizontalAlign="Center">
                                                            <EmptyDataRowStyle Wrap="True" HorizontalAlign="Center" />
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="INCIDENTID" Visible="False">
                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblStudId" runat="server" Text='<%# Bind("ID")%>'></asp:Label>
                                                                        <asp:Label ID="lblStudent" runat="server" Text='<%# Eval("STUDENT_ID")%>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Date">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblStudentNo" runat="server" Text='<%# Format(Eval("ENTRY_DATE"), "dd/MMM/yyyy")%>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            <%--    <asp:TemplateField HeaderText="Staff Name">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblStudName" runat="server" Text='<%# Bind("EMPNAME") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>--%>
                                                                <asp:TemplateField HeaderText="Category">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblShift" runat="server" Text='<%# Bind("INCIDENT_TYPE")%>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Remarks">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="T12lblview3" runat="server" Text='<%#Eval("tempview")%>' CssClass="girdpoplbl" ></asp:Label>
                                                                        <asp:LinkButton
                                                                                ID="lblDetail3" runat="server" Visible='<%# BIND("bShow") %>' CssClass="viewdetails">...Detail</asp:LinkButton>

                                                                        <div id="plR_loc3" Visible='<%# BIND("bShow") %>' runat="server" class="PopUpMessageStyle" > <%--Visible='<%#If(Eval("bShow"), False, True)%>'--%>
                                                                            <asp:Literal ID="ltRemarks3" runat="server" Text='<%# Bind("DESCRIPTION")%>'>
                                                                            </asp:Literal>
                                                                        </div>
                                                                        <ajaxToolkit:PopupControlExtender ID="pcet_Lib_I3" runat="server"
                                                                            OffsetX="-150" PopupControlID="plR_loc3"
                                                                            Position="Bottom" TargetControlID="lblDetail3">
                                                                        </ajaxToolkit:PopupControlExtender>
                                                                      <%--  &nbsp;<asp:LinkButton ID="LinkButton1" runat="server" CommandArgument='<%# GetNavigateUrl1(Eval("STU_ID").ToString() & "," & Eval("BM_ID").ToString()) %>'
                                                                            OnClientClick='<%# GetNavigateUrl1(Eval("STU_ID").ToString() & "," & Eval("BM_ID").ToString()) %>'
                                                                            Width="35px">View</asp:LinkButton>&nbsp;--%>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>

                                                        </asp:GridView>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </section>
                            </div>
                            <div class="accordion-item">
                                <h4 class="accordion-toggle">Library</h4>
                                <section class="accordion-inner panel-body">

                                    <h4>Library Membership(s)</h4>
                                    <!-- Table  -->
                                    <div class="table-responsive">

                                        <table class="table table-striped table-bordered table-responsive text-left my-orders-table" cellspacing="0" rules="all" id="cphParent_gvStudGrade_POS" width="100%">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <asp:GridView ID="GridMemberships" runat="server"
                                                            Width="100%" EmptyDataText="Membership not assigned." AutoGenerateColumns="false"
                                                            CssClass="table table-striped table-bordered table-responsive text-left my-orders-table" BorderStyle="None" BorderWidth="0px"
                                                            HeaderStyle-HorizontalAlign="Center">

                                                            <Columns>

                                                                <asp:TemplateField HeaderText="Library Divisions">

                                                                    <ItemTemplate>
                                                                        <center>
                                                <%#Eval("LIBRARY_DIVISION_DES")%>
                                            </center>
                                                                    </ItemTemplate>

                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Library Membership">
                                                                    <ItemTemplate>
                                                                        <center>
                                                <%#Eval("MEMBERSHIP_DES")%>
                                            </center>
                                                                    </ItemTemplate>

                                                                </asp:TemplateField>



                                                            </Columns>
                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                            <PagerStyle CssClass="gridpager" />
                                                        </asp:GridView>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>


                                    </div>
                                    <!-- /Table  -->
                                    <h4>Library Usage</h4>
                                    <div class="table-responsive">

                                        <table class="table table-striped table-bordered table-responsive text-left my-orders-table" cellspacing="0" rules="all" id="cphParent_gvStudGrade_POS1" width="100%">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <asp:GridView ID="GrdUserTransaction" runat="server" AllowPaging="True"
                                                            EmptyDataText="No record available." AutoGenerateColumns="false"
                                                            Width="100%"
                                                            CssClass="table table-striped table-bordered table-responsive text-left my-orders-table" BorderStyle="None" BorderWidth="0px"
                                                            HeaderStyle-HorizontalAlign="Center">

                                                            <Columns>
                                                                <asp:TemplateField HeaderText="Accession No">
                                                                    <ItemTemplate>
                                                                        <center>
                                <%#Eval("STOCK_ID")%>
                            </center>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Item Name">

                                                                    <ItemTemplate>
                                                                        <asp:Label ID="T12lblview" runat="server" Text=' <%#Eval("tempview")%>' CssClass="girdpoplbl"> </asp:Label><asp:LinkButton ID="lblDetailLib_Iss" runat="server" CssClass="viewdetails">...Detail</asp:LinkButton>
                                                                        <div id="plR_Lib_Iss" runat="server" class="gridMsgDetail">
                                                                            <table width="100%" border="0" style="height: 100%" class="tableNoborder">
                                                                                <tr>
                                                                                    <td rowspan="4" align="left" width="100px">
                                                                                        <asp:Image ID="Image4" runat="server" ImageUrl='<%#Eval("PRODUCT_IMAGE_URL")%>' Width="100px" Height="90px" /></td>
                                                                                    <td><span style="font-weight: bold;">Title :</span>
                                                                                        <asp:Literal ID="ltlibTitle" runat="server"
                                                                                            Text='<%# BIND("ITEM_TITLE") %>'>
                                                                                        </asp:Literal></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td><span style="font-weight: bold;">Author :</span><asp:Literal ID="ltLibAuth" runat="server"
                                                                                        Text='<%# BIND("AUTHOR") %>'>
                                                                                    </asp:Literal></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td><span style="font-weight: bold;">Publisher :</span><asp:Literal ID="ltLibPub" runat="server"
                                                                                        Text='<%# BIND("PUBLISHER") %>'>
                                                                                    </asp:Literal></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td><span style="font-weight: bold;">Library :</span><asp:Literal ID="Literal1" runat="server" Text='<%# BIND("LIBRARY_DIVISION_DES") %>'>
                                                                                    </asp:Literal></td>
                                                                                </tr>
                                                                            </table>

                                                                        </div>





                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Issue Date">
                                                                    <ItemTemplate>
                                                                        <center><%#Eval("ITEM_TAKEN_DATE")%></center>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Tentative Date">
                                                                    <ItemTemplate>
                                                                        <center>  <%#Eval("ITEM_RETURN_DATE")%></center>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Return Date">
                                                                    <ItemTemplate>
                                                                        <center> <%#Eval("RTN_DATE")%></center>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Fine">
                                                                    <ItemTemplate>
                                                                        <center>
                                <%#Eval("FINE_AMOUNT") %>
                                <%#Eval("CURRENCY_ID") %>
                            </center>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                            <PagerStyle CssClass="gridpager" />
                                                        </asp:GridView>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>

                                    <h4>Item(s) Due</h4>
                                    <div class="table-responsive">

                                        <table class="table table-striped table-bordered table-responsive text-left my-orders-table" cellspacing="0" rules="all" id="cphParent_gvStudGrade_POS11" width="100%">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <asp:GridView ID="GridDue" runat="server" AllowPaging="True"
                                                            AutoGenerateColumns="false" EmptyDataText="No Items on Due."
                                                            Width="100%"
                                                            CssClass="table table-striped table-bordered table-responsive text-left my-orders-table" BorderStyle="None" BorderWidth="0px"
                                                            HeaderStyle-HorizontalAlign="Center">

                                                            <Columns>
                                                                <asp:TemplateField HeaderText="Accession No">

                                                                    <ItemTemplate>
                                                                        <center>
                                <%#Eval("STOCK_ID")%>
                            </center>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Item Name">
                                                                    <ItemTemplate>

                                                                        <asp:Label ID="T12lblview" runat="server" Text=' <%#Eval("tempview")%>' CssClass="girdpoplbl">
                                                                        </asp:Label><asp:LinkButton ID="lblDetailLib" runat="server" CssClass="viewdetails">...Detail</asp:LinkButton>
                                                                        <div id="plR_Lib" runat="server" class="gridMsgDetail">
                                                                            <table width="100%" border="0" style="height: 100%" class="tableNoborder">
                                                                                <tr>
                                                                                    <td rowspan="4" align="left" width="100px">
                                                                                        <asp:Image ID="Image4" runat="server" ImageUrl='<%#Eval("PRODUCT_IMAGE_URL")%>' Width="100px" Height="90px" /></td>
                                                                                    <td><span style="font-weight: bold;">Title :</span>
                                                                                        <asp:Literal ID="ltlibTitle" runat="server"
                                                                                            Text='<%# BIND("ITEM_TITLE") %>'>
                                                                                        </asp:Literal></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td><span style="font-weight: bold;">Author :</span><asp:Literal ID="ltLibAuth" runat="server"
                                                                                        Text='<%# BIND("AUTHOR") %>'>
                                                                                    </asp:Literal></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td><span style="font-weight: bold;">Publisher :</span><asp:Literal ID="ltLibPub" runat="server"
                                                                                        Text='<%# BIND("PUBLISHER") %>'>
                                                                                    </asp:Literal></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td><span style="font-weight: bold;">Library :</span><asp:Literal ID="Literal1" runat="server" Text='<%# BIND("LIBRARY_DIVISION_DES") %>'>
                                                                                    </asp:Literal></td>
                                                                                </tr>
                                                                            </table>

                                                                        </div>




                                                                    </ItemTemplate>
                                                                </asp:TemplateField>



                                                                <asp:TemplateField HeaderText="Issue Date">

                                                                    <ItemTemplate>
                                                                        <center><%#Eval("ITEM_TAKEN_DATE")%></center>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Tentative Date">

                                                                    <ItemTemplate>
                                                                        <center>  <%#Eval("ITEM_RETURN_DATE")%> </center>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>


                                                            </Columns>
                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                            <PagerStyle CssClass="gridpager" />
                                                        </asp:GridView>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>

                                    <h4>Reservations</h4>
                                    <div class="table-responsive">

                                        <table class="table table-striped table-bordered table-responsive text-left my-orders-table" cellspacing="0" rules="all" id="cphParent_gvStudGrade_POS111" width="100%">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <asp:GridView ID="GridReservations" runat="server" AllowPaging="True"
                                                            EmptyDataText="No Items Reserved." AutoGenerateColumns="false"
                                                            Width="100%"
                                                            CssClass="table table-striped table-bordered table-responsive text-left my-orders-table" BorderStyle="None" BorderWidth="0px">
                                                            <Columns>


                                                                <asp:TemplateField HeaderText="Item Name">
                                                                    <ItemTemplate>

                                                                        <asp:Label ID="T12lblview" runat="server" Text=' <%#Eval("tempview")%>' CssClass="girdpoplbl"></asp:Label><asp:LinkButton ID="lblDetailLib_Res" runat="server" CssClass="viewdetails">...Detail</asp:LinkButton>
                                                                        <div id="plR_Lib_Res" runat="server" class="gridMsgDetail">
                                                                            <table width="100%" border="0" style="height: 100%" class="tableNoborder">
                                                                                <tr>
                                                                                    <td rowspan="4" align="left" width="100px">
                                                                                        <asp:Image ID="Image4" runat="server" ImageUrl='<%#Eval("PRODUCT_IMAGE_URL")%>' Width="100px" Height="90px" /></td>
                                                                                    <td><span style="font-weight: bold;">Title :</span>
                                                                                        <asp:Literal ID="ltlibTitle" runat="server"
                                                                                            Text='<%# BIND("ITEM_TITLE") %>'>
                                                                                        </asp:Literal></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td><span style="font-weight: bold;">Author :</span><asp:Literal ID="ltLibAuth" runat="server"
                                                                                        Text='<%# BIND("AUTHOR") %>'>
                                                                                    </asp:Literal></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td><span style="font-weight: bold;">Publisher :</span><asp:Literal ID="ltLibPub" runat="server"
                                                                                        Text='<%# BIND("PUBLISHER") %>'>
                                                                                    </asp:Literal></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td><span style="font-weight: bold;">Library :</span><asp:Literal ID="Literal1" runat="server" Text='<%# BIND("LIBRARY_DIVISION_DES") %>'>
                                                                                    </asp:Literal></td>
                                                                                </tr>
                                                                            </table>

                                                                        </div>


                                                                    </ItemTemplate>
                                                                </asp:TemplateField>



                                                                <asp:TemplateField HeaderText="Reserved Date">
                                                                    <ItemTemplate>
                                                                        <center><%#Eval("ENTRY_DATE")%></center>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Period Reserved">
                                                                    <ItemTemplate>
                                                                        <center><%#Eval("RESERVE_START_DATE")%> - <%#Eval("RESERVE_END_DATE")%> </center>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Status">


                                                                    <ItemTemplate>
                                                                        <center>  <%#Eval("CANCEL_RESERVATION")%> </center>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>


                                                            </Columns>
                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                            <PagerStyle CssClass="gridpager" />
                                                        </asp:GridView>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </section>
                            </div>

                           

                        </div>


                        <!--Accordion Ends -->

                    </div>


                </div>
            </div>

        </div>

    </div>
    <!-- /Posts Block -->


                        </div>
                </div>
        </div>

     <asp:HiddenField ID="hfaccordian" Value="1" runat="server" />
   <asp:HiddenField ID="hftytabs" Value="1" runat="server" />

     <script type="text/javascript" language="javascript">

       function ShowWindowWithClose(gotourl, pageTitle, w, h) {
           $.fancybox({
               type: 'iframe',
               //maxWidth: 300,
               href: gotourl,
               //maxHeight: 600,
               fitToView: true,
               padding: 6,
               width: w,
               height: h,
               autoSize: false,
               openEffect: 'none',
               showLoading: true,
               closeClick: true,
               closeEffect: 'fade',
               'closeBtn': true,
               afterLoad: function () {
                   this.title = '';//ShowTitle(pageTitle);
               },
               helpers: {
                   overlay: { closeClick: false }, // prevents closing when clicking OUTSIDE fancybox 
                   title: { type: 'inside' }
               },
               onComplete: function () {
                   $("#fancybox-wrap").css({ 'top': '90px' });

               },
               onCleanup: function () {
                   var hfPostBack = $("#fancybox-frame").contents().find('#hfbPB').val();

                   if (hfPostBack == "Y")
                       window.location.reload(true);
               }
           });

           return false;
       }


         //function ShowWindowWithClose() {

         //    $.fancybox({
         //        type: 'iframe',
         //        //maxWidth: 300,
         //        href: '/studprof/ProfAttendance.aspx',
         //        //maxHeight: 600,
         //        fitToView: true,
         //        padding: 6,
         //        width: '80%',
         //        height: '75%',
         //        autoSize: false,
         //        openEffect: 'none',
         //        showLoading: true,
         //        closeClick: true,
         //        closeEffect: 'fade',
         //        'closeBtn': true,
         //        afterLoad: function () {
         //            this.title = '';//ShowTitle(pageTitle);
         //        },
         //        helpers: {
         //            overlay: { closeClick: false }, // prevents closing when clicking OUTSIDE fancybox 
         //            title: { type: 'inside' }
         //        },
         //        onComplete: function () {
         //            $("#fancybox-wrap").css({ 'top': '90px' });
         //        },
         //        onCleanup: function () {
         //            var hfPostBack = $("#fancybox-frame").contents().find('#hfbPB').val();

         //           // if (hfPostBack == "Y")
         //               // window.location.reload(true);
         //        },
         //        afterClose: function () {
         //            //window.location.reload();
         //        }
         //    });
         //    return false;
         //}



    </script>

   
</asp:Content>

