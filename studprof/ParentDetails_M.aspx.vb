﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Collections.Generic
Imports System.Collections
Partial Class StudProf_ParentDetails
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            Page.Title = OASISConstants.Gemstitle
            If Session("username") Is Nothing Then
                Session("Active_tab") = "Home"
                Session("Site_Path") = ""
                Response.Redirect("~\Login.aspx")
            ElseIf Session("bPasswdChanged") = "False" Then
                Response.Redirect("~\UpdateInfo\ForcePasswordChange.aspx", False)
                'ElseIf Session("bUpdateContactDetails") = "False" Then
                '    'commented by nahyan on 4thmay 2014...
                '    'Response.Redirect(Session("ForceUpdate_stud"), False)
                '    'commentend s here by nahyan on 4thmay 2014...
                '    Response.Redirect("~\StudProf\StudentMainDetails.aspx", False)
            End If

            If Page.IsPostBack = False Then
                BindEmirate_info(ddlFCOMEmirate)
                BindEmirate_info(ddlMCOMEmirate)
                BindEmirate_info(ddlGComEmirate)
                Call GetCountry_info()
                GetCompany_Name()
                Call Student_D_Details(Session("STU_ID"), Session("STU_BSU_ID"))
            End If
            'lbChildName.Text = Session("STU_NAME")
            'trShift.Visible = False
            'trMins.Visible = False
        Catch ex As Exception

        End Try


    End Sub
    Protected Sub btnSaveContinue_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveContinue.Click
        Dim status As Integer
        Dim transaction As SqlTransaction




        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            status = callTrans_Student_D_Details(transaction)
            'Dim lblmsg As Label = CType(Page.Master.FindControl("lblSiteMessage"), Label)
            lblmsg.Text = ""
            If status = 1000 Then
                lblmsg.CssClass = "alert-warning alert"
                lblmsg.Text = "Please enter the details"
                transaction.Rollback()
            ElseIf status <> 0 Then
                lblmsg.CssClass = "alert-warning alert"
                lblmsg.Text = "Please enter the details"
                transaction.Rollback()
            Else
                'divUploadmsg.CssClass = ""
                'divUploadmsg.Text = ""
                lblmsg.CssClass = "alert-warning alert"
                lblmsg.Text = "Data saved successfully"
                Session("bStuSteps") = 2
                UtilityObj.InsertAuditdetails(transaction, "edit", "STUDENT_D", "STS_STU_ID", "STS_STU_ID", "STS_STU_ID=" + Session("STU_ID").ToString)
                transaction.Commit()
                Response.Redirect("~\StudProf\StudHealthDetails.aspx", False)
            End If

        End Using


    End Sub
    Protected Sub btnPrev_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Response.Redirect("~\StudProf\StudentMainDetails.aspx", False)
    End Sub

    Protected Sub ddlFeeCompany_SelectedIndex(ByVal sender As Object, ByVal e As System.EventArgs)
        If ddlFeeCompany.SelectedItem.Value = "0" Then
            rfvFeeCompany.Enabled = True
            txtFeeCompanyName.Visible = True
        Else
            rfvFeeCompany.Enabled = False
            txtFeeCompanyName.Visible = False
        End If
    End Sub

    Protected Sub ddFeeSponsor_SelectedIndex(ByVal sender As Object, ByVal e As System.EventArgs)
        If ddFeeSponsor.SelectedItem.Value = "4" Then
            ddlFeeCompany.ClearSelection()
            ddlFeeCompany.Items.FindByValue(ddlFCompany_Name.SelectedItem.Value).Selected = True
            If ddlFCompany_Name.SelectedItem.Value = "0" Then
                txtFeeCompanyName.Text = txtFComp_Name.Text
            End If
            trFeeCompany.Visible = True
        ElseIf ddFeeSponsor.SelectedItem.Value = "5" Then
            ddlFeeCompany.ClearSelection()
            ddlFeeCompany.Items.FindByValue(ddlMCompany_Name.SelectedItem.Value).Selected = True
            If ddlMCompany_Name.SelectedItem.Value = "0" Then
                txtFeeCompanyName.Text = txtMCompany.Text
            End If
            trFeeCompany.Visible = True
        ElseIf ddFeeSponsor.SelectedItem.Value = "6" Then
            ddlFeeCompany.ClearSelection()
            ddlFeeCompany.Items.FindByValue(ddlGCompany.SelectedItem.Value).Selected = True
            If ddlGCompany.SelectedItem.Value = "0" Then
                txtFeeCompanyName.Text = txtGCompany.Text
            End If
            trFeeCompany.Visible = True
        Else
            trFeeCompany.Visible = False
        End If
    End Sub

    Sub Student_D_Details(ByVal STU_ID As String, ByVal CurBusUnit As String)

        Dim temp_FOFFPhone, temp_FRESPhone, temp_FFax, temp_FMobile, temp_FPRMPHONE, temp_MFAX, temp_MOFFPHONE, temp_MRESPHONE, temp_MMOBILE, temp_MPRMPHONE, temp_GOFFPHONE, temp_GResPhone, temp_GFax, temp_GMOBILE, temp_GPRMPHONE As String, temp_EMGCONTACT As String
        Dim arInfo As String() = New String(2) {}
        Dim splitter As Char = "-"
        Dim sPrimaryContact As String = ""
        Dim CONN As String = ConnectionManger.GetOASISConnectionString
        Dim param(2) As SqlParameter
        param(0) = New SqlParameter("@STU_ID", STU_ID)


        Using readerStudent_D_Detail As SqlDataReader = SqlHelper.ExecuteReader(CONN, CommandType.StoredProcedure, "OPL.GETPROF_STUDENT_DETAILS_FOR_GWA", param)
            If readerStudent_D_Detail.HasRows = True Then
                While readerStudent_D_Detail.Read


                    lblChildName.Text = Session("STU_NAME")
                    lblGradeSection.Text = Convert.ToString(readerStudent_D_Detail("GRD_ID_JOIN")) & "-" & Convert.ToString(readerStudent_D_Detail("SCT_DESCR_JOIN"))

                    lblFeeID.Text = Convert.ToString(readerStudent_D_Detail("FEE_ID"))
                    If IsDate(readerStudent_D_Detail("SDOJ")) = True Then
                        lblDateOfJoin.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((readerStudent_D_Detail("SDOJ"))))
                    End If
                    ''fatehrs info
                    ltFathersName.Text = Convert.ToString(readerStudent_D_Detail("STS_FFIRSTNAME")) & " " & Convert.ToString(readerStudent_D_Detail("STS_FMIDNAME")) & " " & Convert.ToString(readerStudent_D_Detail("STS_FLASTNAME"))

                    txtFResAddressStreet.Text = Convert.ToString(readerStudent_D_Detail("STS_FCOMSTREET"))
                    txtFResAddArea.Text = Convert.ToString(readerStudent_D_Detail("STS_FCOMAREA"))
                    txtFResAddBuilding.Text = Convert.ToString(readerStudent_D_Detail("STS_FCOMBLDG"))
                    txtFResApartment.Text = Convert.ToString(readerStudent_D_Detail("STS_FCOMAPARTNO"))
                    txtFPOBOX.Text = Convert.ToString(readerStudent_D_Detail("STS_FCOMPOBOX"))
                    txtFOccupation.Text = Convert.ToString(readerStudent_D_Detail("STS_FOCC"))

                    If Not ddlPrefContact.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STU_PREFCONTACT"))) Is Nothing Then
                        ddlPrefContact.ClearSelection()
                        ddlPrefContact.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STU_PREFCONTACT"))).Selected = True

                    End If

                    ViewState("temp_PrimaryContact") = Convert.ToString(readerStudent_D_Detail("STU_PRIMARYCONTACT"))
                    sPrimaryContact = ViewState("temp_PrimaryContact")

                    If UCase(ViewState("temp_PrimaryContact")) = "F" Then
                        ltPrimaryCont.Text = "Father"
                        ltFathersName.Enabled = False
                        rfFatherName.Enabled = False
                        rfvRes.Enabled = True
                        rfArea.Enabled = True
                        rfBuilding.Enabled = True
                        rfApartment.Enabled = True
                        rfPobox.Enabled = True
                        rfMobF.Enabled = True
                        rfEmirate.Enabled = True
                        rfFemail.Enabled = True
                        rfOcc.Enabled = True
                        rfCompanyF.Enabled = True
                        spF1.Visible = True
                        spF2.Visible = True
                        spF3.Visible = True
                        spF4.Visible = True
                        spF5.Visible = True
                        spF6.Visible = True
                        spF7.Visible = True
                        spF8.Visible = True
                        spF9.Visible = True
                        spF10.Visible = True

                        rfNameMother.Enabled = True
                        rfMStreet.Enabled = True
                        rfAreaM.Enabled = True
                        rfBuildM.Enabled = True
                        rfApartM.Enabled = True
                        rfEmirateM.Enabled = True
                        rfPOM.Enabled = True
                        rfvMMobNo.Enabled = True
                        rfEmailM.Enabled = True
                        rfOccM.Enabled = True

                        spM1.Visible = True
                        spM2.Visible = True
                        spM3.Visible = True
                        spM4.Visible = True
                        spM5.Visible = True
                        spM6.Visible = True
                        spM7.Visible = True
                        spM8.Visible = True
                        spM9.Visible = True
                        spM10.Visible = True
                    ElseIf UCase(ViewState("temp_PrimaryContact")) = "M" Then
                        ltPrimaryCont.Text = "Mother"
                        ltFathersName.Enabled = True
                        rfFatherName.Enabled = True
                        txtMname.Enabled = False

                        rfvRes.Enabled = True
                        rfArea.Enabled = True
                        rfBuilding.Enabled = True
                        rfApartment.Enabled = True
                        rfPobox.Enabled = True
                        rfMobF.Enabled = True
                        rfEmirate.Enabled = True
                        rfFemail.Enabled = True
                        rfOcc.Enabled = True
                        rfCompanyF.Enabled = True
                        spF1.Visible = True
                        spF2.Visible = True
                        spF3.Visible = True
                        spF4.Visible = True
                        spF5.Visible = True
                        spF6.Visible = True
                        spF7.Visible = True
                        spF8.Visible = True
                        spF9.Visible = True
                        spF10.Visible = True

                        rfNameMother.Enabled = False
                        rfMStreet.Enabled = True
                        rfAreaM.Enabled = True
                        rfBuildM.Enabled = True
                        rfApartM.Enabled = True
                        rfEmirateM.Enabled = True
                        rfPOM.Enabled = True
                        rfvMMobNo.Enabled = True
                        rfEmailM.Enabled = True
                        rfOccM.Enabled = True

                        spM1.Visible = True
                        spM2.Visible = True
                        spM3.Visible = True
                        spM4.Visible = True
                        spM5.Visible = True
                        spM6.Visible = True
                        spM7.Visible = True
                        spM8.Visible = True
                        spM9.Visible = True
                        spM10.Visible = True
                    ElseIf UCase(ViewState("temp_PrimaryContact")) = "G" Then

                        ltPrimaryCont.Text = "Guardian"
                        rfvRes.Enabled = True
                        rfArea.Enabled = True
                        rfBuilding.Enabled = True
                        rfApartment.Enabled = True
                        rfPobox.Enabled = True
                        rfMobF.Enabled = True
                        rfEmirate.Enabled = True
                        rfFemail.Enabled = True
                        rfOcc.Enabled = True
                        rfCompanyF.Enabled = True
                        spF1.Visible = True
                        spF2.Visible = True
                        spF3.Visible = True
                        spF4.Visible = True
                        spF5.Visible = True
                        spF6.Visible = True
                        spF7.Visible = True
                        spF8.Visible = True
                        spF9.Visible = True
                        spF10.Visible = True

                        rfNameMother.Enabled = True
                        rfMStreet.Enabled = True
                        rfAreaM.Enabled = True
                        rfBuildM.Enabled = True
                        rfApartM.Enabled = True
                        rfEmirateM.Enabled = True
                        rfPOM.Enabled = True
                        rfvMMobNo.Enabled = True
                        rfEmailM.Enabled = True
                        rfOccM.Enabled = True

                        spM1.Visible = True
                        spM2.Visible = True
                        spM3.Visible = True
                        spM4.Visible = True
                        spM5.Visible = True
                        spM6.Visible = True
                        spM7.Visible = True
                        spM8.Visible = True
                        spM9.Visible = True
                        spM10.Visible = True


                        rfGNAme.Enabled = True
                        rfGStreet.Enabled = True
                        rfResAreaG.Enabled = True
                        rfBuilG.Enabled = True
                        rfGApart.Enabled = True
                        rfEmirateG.Enabled = True
                        rfPOG.Enabled = True
                        rfvGMobNo.Enabled = True
                        rfGEmail.Enabled = True
                        rfGOccup.Enabled = True
                        rfGComp.Enabled = True

                        spG1.Visible = True
                        spG2.Visible = True
                        spG3.Visible = True
                        spG4.Visible = True
                        spG5.Visible = True
                        spG6.Visible = True
                        spG7.Visible = True
                        spG8.Visible = True
                        spG9.Visible = True
                        spG10.Visible = True
                    End If

                    txtFemail.Text = Convert.ToString(readerStudent_D_Detail("STS_FEMAIL"))


                    ''mothers info
                    txtMname.Text = Convert.ToString(readerStudent_D_Detail("STS_MFIRSTNAME")) & " " & Convert.ToString(readerStudent_D_Detail("STS_MMIDNAME")) & " " & Convert.ToString(readerStudent_D_Detail("STS_MLASTNAME"))
                    txtMResAddressStreet.Text = Convert.ToString(readerStudent_D_Detail("STS_MCOMSTREET"))
                    txtMResAddArea.Text = Convert.ToString(readerStudent_D_Detail("STS_MCOMAREA"))
                    txtMResAddBuilding.Text = Convert.ToString(readerStudent_D_Detail("STS_MCOMBLDG"))
                    txtMResApartment.Text = Convert.ToString(readerStudent_D_Detail("STS_MCOMAPARTNO"))
                    txtMPOBOX.Text = Convert.ToString(readerStudent_D_Detail("STS_MCOMPOBOX"))


                    txtMemail.Text = Convert.ToString(readerStudent_D_Detail("STS_MEMAIL"))
                    txtMCompany.Text = Convert.ToString(readerStudent_D_Detail("STS_MCOMPANY_ADDR"))
                    txtMOccupation.Text = Convert.ToString(readerStudent_D_Detail("STS_MOCC"))
                    ''guardian info
                    txtGuardian.Text = Convert.ToString(readerStudent_D_Detail("STS_GFIRSTNAME"))
                    txtGResAddressStreet.Text = Convert.ToString(readerStudent_D_Detail("STS_GCOMSTREET"))
                    txtGResAddArea.Text = Convert.ToString(readerStudent_D_Detail("STS_GCOMAREA"))
                    txtGResAddBuilding.Text = Convert.ToString(readerStudent_D_Detail("STS_GCOMBLDG"))
                    txtGResApartment.Text = Convert.ToString(readerStudent_D_Detail("STS_GCOMAPARTNO"))
                    txtGPOBOX.Text = Convert.ToString(readerStudent_D_Detail("STS_GCOMPOBOX"))

                    txtGCompany.Text = Convert.ToString(readerStudent_D_Detail("STS_GCOMPANY_ADDR"))
                    txtGemail.Text = Convert.ToString(readerStudent_D_Detail("STS_GEMAIL"))
                    txtGOccupation.Text = Convert.ToString(readerStudent_D_Detail("STS_GOCC"))

                    temp_MMOBILE = Convert.ToString(readerStudent_D_Detail("STS_FMOBILE"))
                    arInfo = temp_MMOBILE.Split(splitter)
                    If arInfo.Length > 2 Then
                        txtFMobile_Country.Text = arInfo(0)
                        txtFMobile_Area.Text = arInfo(1)
                        txtFMobile_No.Text = arInfo(2)
                    ElseIf arInfo.Length = 2 Then
                        txtFMobile_Area.Text = arInfo(0)
                        txtFMobile_No.Text = arInfo(1)

                    Else
                        txtFMobile_No.Text = temp_MMOBILE
                    End If

                    temp_MMOBILE = Convert.ToString(readerStudent_D_Detail("STS_MMOBILE"))
                    arInfo = temp_MMOBILE.Split(splitter)
                    If arInfo.Length > 2 Then
                        txtMMobile_Country.Text = arInfo(0)
                        txtMMobile_Area.Text = arInfo(1)
                        txtMMobile_No.Text = arInfo(2)
                    ElseIf arInfo.Length = 2 Then
                        txtMMobile_Area.Text = arInfo(0)
                        txtMMobile_No.Text = arInfo(1)

                    Else
                        txtMMobile_No.Text = temp_MMOBILE
                    End If

                    temp_GMOBILE = Convert.ToString(readerStudent_D_Detail("STS_GMOBILE"))
                    arInfo = temp_GMOBILE.Split(splitter)
                    If arInfo.Length > 2 Then
                        txtGMobile_Country.Text = arInfo(0)
                        txtGMobile_Area.Text = arInfo(1)
                        txtGMobile_No.Text = arInfo(2)
                    ElseIf arInfo.Length = 2 Then
                        txtGMobile_Area.Text = arInfo(0)
                        txtGMobile_No.Text = arInfo(1)
                    Else
                        txtGMobile_No.Text = temp_GMOBILE
                    End If


                    If Not ddlFCOMEmirate.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_FEMIR"))) Is Nothing Then
                        ddlFCOMEmirate.ClearSelection()
                        ddlFCOMEmirate.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_FEMIR"))).Selected = True
                    End If
                    If Not ddlMCOMEmirate.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_MEMIR"))) Is Nothing Then
                        ddlMCOMEmirate.ClearSelection()
                        ddlMCOMEmirate.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_MEMIR"))).Selected = True
                    End If

                    If Not ddlGComEmirate.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_GEMIR"))) Is Nothing Then
                        ddlGComEmirate.ClearSelection()
                        ddlGComEmirate.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_GEMIR"))).Selected = True
                    End If

                    If Not ddlFCompany_Name.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_F_COMP_ID"))) Is Nothing Then
                        ddlFCompany_Name.ClearSelection()
                        ddlFCompany_Name.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_F_COMP_ID"))).Selected = True
                    End If

                    If Not ddlMCompany_Name.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_M_COMP_ID"))) Is Nothing Then
                        ddlMCompany_Name.ClearSelection()
                        ddlMCompany_Name.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_M_COMP_ID"))).Selected = True
                    End If

                    If Not ddlGCompany.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_G_COMP_ID"))) Is Nothing Then
                        ddlGCompany.ClearSelection()
                        ddlGCompany.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_G_COMP_ID"))).Selected = True
                    End If

                    If Not ddFeeSponsor.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_FEESPONSOR"))) Is Nothing Then
                        ddFeeSponsor.ClearSelection()
                        ddFeeSponsor.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_FEESPONSOR"))).Selected = True
                    End If

                    If Convert.ToString(readerStudent_D_Detail("STS_FEESPONSOR")) = "4" Then
                        ddlFeeCompany.ClearSelection()
                        ddlFeeCompany.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_F_COMP_ID"))).Selected = True
                        txtFeeCompanyName.Text = Convert.ToString(readerStudent_D_Detail("STS_FCOMPANY"))
                    ElseIf Convert.ToString(readerStudent_D_Detail("STS_FEESPONSOR")) = "5" Then
                        ddlFeeCompany.ClearSelection()
                        ddlFeeCompany.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_M_COMP_ID"))).Selected = True
                        txtFeeCompanyName.Text = Convert.ToString(readerStudent_D_Detail("STS_MCOMPANY"))
                    ElseIf Convert.ToString(readerStudent_D_Detail("STS_FEESPONSOR")) = "6" Then
                        ddlFeeCompany.ClearSelection()
                        ddlFeeCompany.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_G_COMP_ID"))).Selected = True
                        txtFeeCompanyName.Text = Convert.ToString(readerStudent_D_Detail("STS_GCOMPANY"))

                    End If

                    txtFComp_Name.Text = Convert.ToString(readerStudent_D_Detail("STS_FCOMPANY"))
                    txtMCompany.Text = Convert.ToString(readerStudent_D_Detail("STS_MCOMPANY"))
                    txtGCompany.Text = Convert.ToString(readerStudent_D_Detail("STS_GCOMPANY"))

                    '---------------------------
                    If Not ddlFCOMCountry.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_FCOMCOUNTRY"))) Is Nothing Then
                        ddlFCOMCountry.ClearSelection()
                        ddlFCOMCountry.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_FCOMCOUNTRY"))).Selected = True
                        ddlFCOMCountry_SelectedIndexChanged(ddlFCOMCountry, Nothing)
                    End If


                    If Not ddlFCity.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_FCOMSTATE_ID"))) Is Nothing Then
                        ddlFCity.ClearSelection()
                        ddlFCity.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_FCOMSTATE_ID"))).Selected = True
                        ddlFCity_SelectedIndexChanged(ddlFCity, Nothing)

                    End If



                    If Not ddlFArea.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_FCOMAREA_ID"))) Is Nothing Then
                        ddlFArea.ClearSelection()
                        ddlFArea.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_FCOMAREA_ID"))).Selected = True
                    End If
                    If Not ddlMCOMCountry.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_MCOMCOUNTRY"))) Is Nothing Then
                        ddlMCOMCountry.ClearSelection()
                        ddlMCOMCountry.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_MCOMCOUNTRY"))).Selected = True
                        ddlMCOMCountry_SelectedIndexChanged(ddlMCOMCountry, Nothing)
                    End If

                    If Not ddlMCity.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_MCOMSTATE_ID"))) Is Nothing Then
                        ddlMCity.ClearSelection()
                        ddlMCity.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_MCOMSTATE_ID"))).Selected = True
                        ddlMCity_SelectedIndexChanged(ddlMCity, Nothing)
                    End If

                    If Not ddlMArea.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_MCOMAREA_ID"))) Is Nothing Then
                        ddlMArea.ClearSelection()
                        ddlMArea.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_MCOMAREA_ID"))).Selected = True
                    End If
                    If Not ddlGCOMCountry.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_GCOMCOUNTRY"))) Is Nothing Then
                        ddlGCOMCountry.ClearSelection()
                        ddlGCOMCountry.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_GCOMCOUNTRY"))).Selected = True
                        ddlGCOMCountry_SelectedIndexChanged(ddlGCOMCountry, Nothing)
                    Else
                        ddlGCOMCountry.ClearSelection()
                        ddlGCOMCountry.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_FCOMCOUNTRY"))).Selected = True
                        ddlGCOMCountry_SelectedIndexChanged(ddlGCOMCountry, Nothing)
                    End If


                    If Not ddlGCity.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_GCOMSTATE_ID"))) Is Nothing Then
                        ddlGCity.ClearSelection()
                        ddlGCity.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_GCOMSTATE_ID"))).Selected = True
                        ddlGCity_SelectedIndexChanged(ddlGCity, Nothing)
                    End If

                End While
            Else
            End If
        End Using
    End Sub

    Sub GetCompany_Name()
        Try
            Using GetCompany_Name_reader As SqlDataReader = studClass.GetCompany_Name()
                ddlFCompany_Name.Items.Clear()
                ddlMCompany_Name.Items.Clear()
                ddlGCompany.Items.Clear()
                ddlFeeCompany.Items.Clear()

                ddlFCompany_Name.Items.Add(New ListItem("Other", "0"))
                ddlMCompany_Name.Items.Add(New ListItem("Other", "0"))
                ddlGCompany.Items.Add(New ListItem("Other", "0"))
                ddlFeeCompany.Items.Add(New ListItem("Other", "0"))

                If GetCompany_Name_reader.HasRows = True Then
                    While GetCompany_Name_reader.Read
                        ddlFCompany_Name.Items.Add(New ListItem(GetCompany_Name_reader("comp_Name"), GetCompany_Name_reader("comp_ID")))
                        ddlMCompany_Name.Items.Add(New ListItem(GetCompany_Name_reader("comp_Name"), GetCompany_Name_reader("comp_ID")))
                        ddlGCompany.Items.Add(New ListItem(GetCompany_Name_reader("comp_Name"), GetCompany_Name_reader("comp_ID")))
                        ddlFeeCompany.Items.Add(New ListItem(GetCompany_Name_reader("comp_Name"), GetCompany_Name_reader("comp_ID")))
                    End While
                End If

            End Using

            'ddlFeeCompany_SelectedIndex(ddlFeeCompany, Nothing)
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "GetCompany_Name()")
        End Try
    End Sub

    Function callTrans_Student_D_Details(ByVal trans As SqlTransaction) As Integer
        Try
            Dim Status As Integer = 0

            ''Father info
            Dim Fname As String = ltFathersName.Text
            Dim prefContact As String = ddlPrefContact.SelectedItem.Value
            Dim FResAddStrt As String = txtFResAddressStreet.Text
            Dim FResAddArea As String = txtFResAddArea.Text
            Dim FResAddBuilding As String = txtFResAddBuilding.Text
            Dim FResApartment As String = txtFResApartment.Text
            Dim FEmirate As String = ddlFCOMEmirate.SelectedItem.Value
            Dim FPOBOX As String = txtFPOBOX.Text
            Dim STS_FMOBILE As String = txtFMobile_Country.Text & "-" & txtFMobile_Area.Text & "-" & txtFMobile_No.Text
            Dim Femail As String = txtFemail.Text
            Dim FOccupation As String = txtFOccupation.Text
            Dim STS_FCOMPANY As String
            Dim STS_F_COMP_ID As String

            If ddlFCompany_Name.SelectedItem.Text = "Other" Then
                STS_FCOMPANY = txtFComp_Name.Text
                STS_F_COMP_ID = "0"
            Else
                STS_FCOMPANY = ""
                STS_F_COMP_ID = ddlFCompany_Name.SelectedItem.Value
            End If

            'mother info
            Dim STS_MNAME As String = txtMname.Text
            Dim MResAddStrt As String = txtMResAddressStreet.Text
            Dim MResAddArea As String = txtMResAddArea.Text
            Dim MResAddBuilding As String = txtMResAddBuilding.Text
            Dim MResApartment As String = txtMResApartment.Text
            Dim MEmirate As String = ddlMCOMEmirate.SelectedItem.Value
            Dim MPOBOX As String = txtMPOBOX.Text
            Dim STS_MMOBILE As String = txtMMobile_Country.Text & "-" & txtMMobile_Area.Text & "-" & txtMMobile_No.Text
            Dim Memail As String = txtMemail.Text
            Dim MOccupation As String = txtMOccupation.Text
            Dim STS_MCOMPANY As String
            Dim STS_M_COMP_ID As String

            If ddlMCompany_Name.SelectedItem.Text = "Other" Then
                STS_MCOMPANY = txtMCompany.Text
                STS_M_COMP_ID = "0"
            Else
                STS_MCOMPANY = ""
                STS_M_COMP_ID = ddlMCompany_Name.SelectedItem.Value
            End If

            'guardian info
            Dim GName As String = txtGuardian.Text
            Dim GResAddStrt As String = txtGResAddressStreet.Text
            Dim GResAddArea As String = txtGResAddArea.Text
            Dim GResAddBuilding As String = txtGResAddBuilding.Text
            Dim GResApartment As String = txtGResApartment.Text
            Dim GEmirate As String = ddlGComEmirate.SelectedItem.Value
            Dim GPOBOX As String = txtGPOBOX.Text
            Dim STS_GMOBILE As String = txtGMobile_Country.Text & "-" & txtGMobile_Area.Text & "-" & txtGMobile_No.Text
            Dim Gemail As String = txtGemail.Text
            Dim GOccupation As String = txtGOccupation.Text
            Dim STS_GCOMPANY As String
            Dim STS_G_COMP_ID As String

            If ddlMCompany_Name.SelectedItem.Text = "Other" Then
                STS_GCOMPANY = txtGCompany.Text
                STS_G_COMP_ID = "0"
            Else
                STS_GCOMPANY = ""
                STS_G_COMP_ID = ddlGCompany.SelectedItem.Value
            End If

            Dim STS_FCOMCOUNTRY = ddlFCOMCountry.SelectedItem.Value
            Dim STS_MCOMCOUNTRY = ddlMCOMCountry.SelectedItem.Value
            Dim STS_GCOMCOUNTRY = ddlGCOMCountry.SelectedItem.Value


            Dim STS_FCOMAREA_ID As String
            Dim STS_MCOMAREA_ID As String
            Dim STS_GCOMAREA_ID As String


            Dim STS_FCOMSTATE_ID = ddlFCity.SelectedItem.Value
            Dim STS_MCOMSTATE_ID = ddlMCity.SelectedItem.Value
            Dim STS_GCOMSTATE_ID = ddlGCity.SelectedItem.Value

            If ddlFArea.SelectedItem.Text = "Other" Then
                FResAddArea = txtFResAddArea.Text
                STS_FCOMAREA_ID = "0"
            Else
                FResAddArea = ""
                STS_FCOMAREA_ID = ddlFArea.SelectedItem.Value
            End If

            If ddlGArea.SelectedItem.Text = "Other" Then
                GResAddArea = txtGResAddArea.Text
                STS_GCOMAREA_ID = "0"
            Else
                GResAddArea = ""
                STS_GCOMAREA_ID = ddlGArea.SelectedItem.Value
            End If

            If ddlMArea.SelectedItem.Text = "Other" Then
                MResAddArea = txtMResAddArea.Text
                STS_MCOMAREA_ID = "0"
            Else
                MResAddArea = ""
                STS_MCOMAREA_ID = ddlMArea.SelectedItem.Value
            End If




            Dim pParms(51) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@STU_ID", Session("STU_ID"))
            pParms(1) = New SqlClient.SqlParameter("@STU_BSU_ID", Session("STU_BSU_ID"))
            pParms(2) = New SqlClient.SqlParameter("@prefContact", prefContact)
            'father
            pParms(3) = New SqlClient.SqlParameter("@FResAddStrt", FResAddStrt)
            pParms(4) = New SqlClient.SqlParameter("@FResAddArea", FResAddArea)
            pParms(5) = New SqlClient.SqlParameter("@FResAddBuilding", FResAddBuilding)
            pParms(6) = New SqlClient.SqlParameter("@FResApartment", FResApartment)
            pParms(7) = New SqlClient.SqlParameter("@FEmirate", FEmirate)
            pParms(8) = New SqlClient.SqlParameter("@FPOBOX", FPOBOX)
            pParms(9) = New SqlClient.SqlParameter("@STS_FMOBILE", STS_FMOBILE)
            pParms(10) = New SqlClient.SqlParameter("@Femail", Femail)
            pParms(11) = New SqlClient.SqlParameter("@FOccupation", FOccupation)
            pParms(12) = New SqlClient.SqlParameter("@STS_FCOMPANY", STS_FCOMPANY)
            pParms(13) = New SqlClient.SqlParameter("@STS_F_COMP_ID", STS_F_COMP_ID)
            'mother
            pParms(14) = New SqlClient.SqlParameter("@MResAddStrt", MResAddStrt)
            pParms(15) = New SqlClient.SqlParameter("@MResAddArea", MResAddArea)
            pParms(16) = New SqlClient.SqlParameter("@MResAddBuilding", MResAddBuilding)
            pParms(17) = New SqlClient.SqlParameter("@MResApartment", MResApartment)
            pParms(18) = New SqlClient.SqlParameter("@MEmirate", MEmirate)
            pParms(19) = New SqlClient.SqlParameter("@MPOBOX", MPOBOX)
            pParms(20) = New SqlClient.SqlParameter("@STS_MMOBILE", STS_MMOBILE)
            pParms(21) = New SqlClient.SqlParameter("@Memail", Memail)
            pParms(22) = New SqlClient.SqlParameter("@MOccupation", MOccupation)
            pParms(23) = New SqlClient.SqlParameter("@STS_MCOMPANY", STS_MCOMPANY)
            pParms(24) = New SqlClient.SqlParameter("@STS_M_COMP_ID", STS_M_COMP_ID)
            'GUARDIAN
            pParms(25) = New SqlClient.SqlParameter("@GResAddStrt", GResAddStrt)
            pParms(26) = New SqlClient.SqlParameter("@GResAddArea", GResAddArea)
            pParms(27) = New SqlClient.SqlParameter("@GResAddBuilding", GResAddBuilding)
            pParms(28) = New SqlClient.SqlParameter("@GResApartment", GResApartment)
            pParms(29) = New SqlClient.SqlParameter("@GEmirate", GEmirate)
            pParms(30) = New SqlClient.SqlParameter("@GPOBOX", GPOBOX)
            pParms(31) = New SqlClient.SqlParameter("@STS_GMOBILE", STS_GMOBILE)
            pParms(32) = New SqlClient.SqlParameter("@Gemail", Gemail)
            pParms(33) = New SqlClient.SqlParameter("@GOccupation", GOccupation)
            pParms(34) = New SqlClient.SqlParameter("@STS_GCOMPANY", STS_GCOMPANY)
            pParms(35) = New SqlClient.SqlParameter("@STS_G_COMP_ID", STS_G_COMP_ID)
            pParms(36) = New SqlClient.SqlParameter("@STS_G_Name", GName)
            pParms(37) = New SqlClient.SqlParameter("@STS_M_Name", STS_MNAME)
            pParms(38) = New SqlClient.SqlParameter("@OLU_ID", Convert.ToInt32(Session("OLU_ID").ToString))
            pParms(40) = New SqlClient.SqlParameter("@TEMP_FEESPONSOR", ddFeeSponsor.SelectedItem.Value)
            pParms(41) = New SqlClient.SqlParameter("@STS_FCOMCOUNTRY", STS_FCOMCOUNTRY)
            pParms(42) = New SqlClient.SqlParameter("@STS_MCOMCOUNTRY", STS_MCOMCOUNTRY)
            pParms(43) = New SqlClient.SqlParameter("@STS_GCOMCOUNTRY", STS_GCOMCOUNTRY)
            pParms(44) = New SqlClient.SqlParameter("@STS_FCOMAREA_ID", STS_FCOMAREA_ID)
            pParms(45) = New SqlClient.SqlParameter("@STS_MCOMAREA_ID", STS_MCOMAREA_ID)
            pParms(46) = New SqlClient.SqlParameter("@STS_GCOMAREA_ID", STS_GCOMAREA_ID)
            pParms(47) = New SqlClient.SqlParameter("@STS_FCOMSTATE_ID", STS_FCOMSTATE_ID)
            pParms(48) = New SqlClient.SqlParameter("@STS_GCOMSTATE_ID", STS_GCOMSTATE_ID)
            pParms(49) = New SqlClient.SqlParameter("@STS_MCOMSTATE_ID", STS_MCOMSTATE_ID)
            pParms(50) = New SqlClient.SqlParameter("@STS_F_NAME", Fname)

            pParms(39) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(39).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "Update_Parent_Info_GWA", pParms)
            Dim ReturnFlag As Integer = pParms(39).Value
            Return ReturnFlag
        Catch ex As Exception
            Return 1000
        End Try
    End Function

    Private Sub BindEmirate_info(ByVal ddlEmirate As DropDownList)
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String = "SELECT EMR_CODE,EMR_DESCR  FROM EMIRATE_M order by EMR_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        ddlEmirate.DataSource = ds
        ddlEmirate.DataTextField = "EMR_DESCR"
        ddlEmirate.DataValueField = "EMR_CODE"
        ddlEmirate.DataBind()
        ddlEmirate.SelectedValue = "-"
    End Sub
    '----------------------COPIED FROM OLD PORTAL-----------------------------
    Protected Sub ddlFCOMCountry_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlFCOMCountry.SelectedIndexChanged
        bind_FCITY()
        ddlFCity_SelectedIndexChanged(ddlFCity, Nothing)

    End Sub
    Protected Sub ddlMCOMCountry_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlMCOMCountry.SelectedIndexChanged
        bind_MCITY()
        ddlMCity_SelectedIndexChanged(ddlMCity, Nothing)
    End Sub
    Protected Sub ddlGCOMCountry_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlGCOMCountry.SelectedIndexChanged
        bind_GCITY()
        ddlGCity_SelectedIndexChanged(ddlGCity, Nothing)
    End Sub
    Sub GetCountry_info()
        Try
            ddlFCOMCountry.Items.Clear()
            ddlMCOMCountry.Items.Clear()
            ddlGCOMCountry.Items.Clear()


            ddlFCOMCountry.Items.Add(New ListItem("", ""))
            ddlMCOMCountry.Items.Add(New ListItem("", ""))
            ddlGCOMCountry.Items.Add(New ListItem("", ""))


            Using AllCountry_reader As SqlDataReader = AccessRoleUser.GetCountry()
                Dim di_Country As ListItem

                If AllCountry_reader.HasRows = True Then
                    While AllCountry_reader.Read
                        di_Country = New ListItem(AllCountry_reader("CTY_DESCR"), AllCountry_reader("CTY_ID"))
                        ddlFCOMCountry.Items.Add(New ListItem(AllCountry_reader("CTY_DESCR"), AllCountry_reader("CTY_ID")))
                        ddlMCOMCountry.Items.Add(New ListItem(AllCountry_reader("CTY_DESCR"), AllCountry_reader("CTY_ID")))
                        ddlGCOMCountry.Items.Add(New ListItem(AllCountry_reader("CTY_DESCR"), AllCountry_reader("CTY_ID")))
                    End While

                End If
            End Using

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "GetCountry_info")
        End Try
    End Sub
    Private Sub bind_FCITY()
        Dim sql_conn As String = ConnectionManger.GetOASISConnectionString
        Dim PARAM(1) As SqlParameter
        PARAM(0) = New SqlParameter("@CTY_ID", ddlFCOMCountry.SelectedValue)
        PARAM(1) = New SqlParameter("@INFO_TYPE", "STATE")

        Try
            Using State_reader As SqlDataReader = SqlHelper.ExecuteReader(sql_conn, CommandType.StoredProcedure, "ONLINE_ENQ.GETSTATE_AREA", PARAM)
                ddlFCity.Items.Clear()
                ddlFCity.Items.Add(New ListItem("", "-1"))
                If State_reader.HasRows = True Then
                    While State_reader.Read
                        ddlFCity.Items.Add(New ListItem(State_reader("EMR_DESCR"), State_reader("EMR_ID")))
                    End While
                End If

                ddlFCity.Items.Add(New ListItem("Other", "0"))
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "bind_FState")
        End Try
    End Sub
    Private Sub bind_MCITY()
        Dim sql_conn As String = ConnectionManger.GetOASISConnectionString
        Dim PARAM(1) As SqlParameter
        PARAM(0) = New SqlParameter("@CTY_ID", ddlMCOMCountry.SelectedValue)
        PARAM(1) = New SqlParameter("@INFO_TYPE", "STATE")

        Try
            Using State_reader As SqlDataReader = SqlHelper.ExecuteReader(sql_conn, CommandType.StoredProcedure, "ONLINE_ENQ.GETSTATE_AREA", PARAM)
                ddlMCity.Items.Clear()
                ddlMCity.Items.Add(New ListItem("", "-1"))
                If State_reader.HasRows = True Then
                    While State_reader.Read
                        ddlMCity.Items.Add(New ListItem(State_reader("EMR_DESCR"), State_reader("EMR_ID")))
                    End While
                End If

                ddlMCity.Items.Add(New ListItem("Other", "0"))
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "bind_MState")
        End Try
    End Sub
    Private Sub bind_GCITY()
        Dim sql_conn As String = ConnectionManger.GetOASISConnectionString
        Dim PARAM(1) As SqlParameter
        PARAM(0) = New SqlParameter("@CTY_ID", ddlGCOMCountry.SelectedValue)
        PARAM(1) = New SqlParameter("@INFO_TYPE", "STATE")

        Try
            Using State_reader As SqlDataReader = SqlHelper.ExecuteReader(sql_conn, CommandType.StoredProcedure, "ONLINE_ENQ.GETSTATE_AREA", PARAM)
                ddlGCity.Items.Clear()
                ddlGCity.Items.Add(New ListItem("", "-1"))
                If State_reader.HasRows = True Then
                    While State_reader.Read
                        ddlGCity.Items.Add(New ListItem(State_reader("EMR_DESCR"), State_reader("EMR_ID")))
                    End While
                End If

                ddlGCity.Items.Add(New ListItem("Other", "0"))
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "bind_GState")
        End Try
    End Sub
    Private Sub bind_FArea()
        Dim sql_conn As String = ConnectionManger.GetOASISConnectionString
        Dim PARAM(1) As SqlParameter
        PARAM(0) = New SqlParameter("@STATE_ID", ddlFCity.SelectedValue)
        PARAM(1) = New SqlParameter("@INFO_TYPE", "AREA")

        Try
            Using AREA_reader As SqlDataReader = SqlHelper.ExecuteReader(sql_conn, CommandType.StoredProcedure, "ONLINE_ENQ.GETSTATE_AREA", PARAM)
                ddlFArea.Items.Clear()
                ddlFArea.Items.Add(New ListItem("", "-1"))
                If AREA_reader.HasRows = True Then
                    While AREA_reader.Read
                        ddlFArea.Items.Add(New ListItem(AREA_reader("EMA_DESCR"), AREA_reader("EMA_ID")))
                    End While
                End If

                ddlFArea.Items.Add(New ListItem("Other", "0"))
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "bind_FArea")
        End Try
    End Sub
    Private Sub bind_MArea()
        Dim sql_conn As String = ConnectionManger.GetOASISConnectionString
        Dim PARAM(1) As SqlParameter
        PARAM(0) = New SqlParameter("@STATE_ID", ddlMCity.SelectedValue)
        PARAM(1) = New SqlParameter("@INFO_TYPE", "AREA")

        Try
            Using AREA_reader As SqlDataReader = SqlHelper.ExecuteReader(sql_conn, CommandType.StoredProcedure, "ONLINE_ENQ.GETSTATE_AREA", PARAM)
                ddlMArea.Items.Clear()
                ddlMArea.Items.Add(New ListItem("", "-1"))
                If AREA_reader.HasRows = True Then
                    While AREA_reader.Read
                        ddlMArea.Items.Add(New ListItem(AREA_reader("EMA_DESCR"), AREA_reader("EMA_ID")))
                    End While
                End If

                ddlMArea.Items.Add(New ListItem("Other", "0"))
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "bind_FArea")
        End Try
    End Sub
    Private Sub bind_GArea()
        Dim sql_conn As String = ConnectionManger.GetOASISConnectionString
        Dim PARAM(1) As SqlParameter
        PARAM(0) = New SqlParameter("@STATE_ID", ddlGCity.SelectedValue)
        PARAM(1) = New SqlParameter("@INFO_TYPE", "AREA")

        Try
            Using AREA_reader As SqlDataReader = SqlHelper.ExecuteReader(sql_conn, CommandType.StoredProcedure, "ONLINE_ENQ.GETSTATE_AREA", PARAM)
                ddlGArea.Items.Clear()
                ddlGArea.Items.Add(New ListItem("", "-1"))
                If AREA_reader.HasRows = True Then
                    While AREA_reader.Read
                        ddlGArea.Items.Add(New ListItem(AREA_reader("EMA_DESCR"), AREA_reader("EMA_ID")))
                    End While
                End If

                ddlGArea.Items.Add(New ListItem("Other", "0"))
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "bind_FArea")
        End Try
    End Sub
    Protected Sub ddlFCity_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlFCity.SelectedIndexChanged
        bind_FArea()
    End Sub
    Protected Sub ddlMCity_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlMCity.SelectedIndexChanged
        bind_MArea()
    End Sub
    Protected Sub ddlGCity_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlGCity.SelectedIndexChanged
        bind_GArea()
    End Sub
End Class
