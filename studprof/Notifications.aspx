﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Notifications.aspx.vb" Inherits="studprof_Notifications" ViewStateMode="Inherit" EnableViewState="true" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<html>
<head>


        <!-- Library CSS -->
        <link rel="stylesheet" href="../css/bootstrap.css"/>
        <link rel="stylesheet" href="../css/bootstrap-theme.css"/>
        <link rel="stylesheet" href="../css/fonts/font-awesome/css/font-awesome.css"/>
        <link rel="stylesheet" href="../css/animations.css" media="screen"/>
    <link rel="stylesheet" href="../css/animate.css" media="screen"/>
        
       
        <!-- Theme CSS -->
        <link rel="stylesheet" href="../css/style.css"/>
        <!-- Skin -->
        <link rel="stylesheet" href="../css/colors/blue.css" class="colors"/>
        <!-- Responsive CSS -->
        <link rel="stylesheet" href="../css/theme-responsive.css"/>
        <!-- Switcher CSS -->
        <link href="../css/switcher.css" rel="stylesheet"/>
        <link href="../css/spectrum.css" rel="stylesheet"/>
    <style>
        .PopUpMessageStyle {
            background :border-box center #ffffff;      
            width:50%;
            border :1px solid;
            border-radius :4px;
            padding:5px;
        }

    </style>
    <script>
        function KeepStyle() {
            jQuery('.accordionMod').each(function (index) {
                var thisBox = jQuery(this).children(),
                    thisMainIndex = index + 1;
                jQuery(this).attr('id', 'accordion' + thisMainIndex);
                thisBox.each(function (i) {
                    var thisIndex = i + 1,
                        thisParentIndex = thisMainIndex,
                        thisMain = jQuery(this).parent().attr('id'),
                        thisTriggers = jQuery(this).find('.accordion-toggle'),
                        thisBoxes = jQuery(this).find('.accordion-inner');
                    jQuery(this).addClass('panel');
                    thisBoxes.wrap('<div id=\"collapseBox' + thisParentIndex + '_' + thisIndex + '\" class=\"panel-collapse collapse\" />');
                    thisTriggers.wrap('<div class=\"panel-heading\" />');
                    thisTriggers.attr('data-toggle', 'collapse').attr('data-parent', '#' + thisMain).attr('data-target', '#collapseBox' + thisParentIndex + '_' + thisIndex);
                });

                jQuery('.accordion-toggle').prepend('<span class=\"icon\" />');
                var hidden = $('input[id$=hfTab]').val();
                //alert(hidden);
                jQuery("div.accordion-item:nth-child(" + hidden + ") .accordion-toggle").addClass("current");
                jQuery("div.accordion-item:nth-child(" + hidden + ") .icon").addClass("iconActive");
                jQuery("div.accordion-item:nth-child(" + hidden + ") .panel-collapse").addClass("in");
            });
            jQuery('.accordionMod .accordion-toggle').click(function () {
                var boxid = $(this).attr("data-target")[$(this).attr("data-target").length - 1];
                $('input[id$=hfTab]').val(boxid);
                if (jQuery(this).parent().parent().find('.panel-collapse').is('.in')) {
                    jQuery(this).removeClass('current');
                    jQuery(this).find('.icon').removeClass('iconActive');
                } else {
                    jQuery(this).addClass('current');
                    jQuery(this).find('.icon').addClass('iconActive');
                }
                jQuery(this).parent().parent().siblings().find('.accordion-toggle').removeClass('current');
                jQuery(this).parent().parent().siblings().find('.accordion-toggle > .icon').removeClass('iconActive');
            });
        }
    </script>
    <%--<script type="text/javascript">
        $(document).ready(function () {
            var selectedTab = $("#<%=hfTab.ClientID%>");
            var tabId = selectedTab.val() != "" ? selectedTab.val() : "tab1";
            $('#dvTab a[href="#' + tabId + '"]').tab('show');
            $("#dvTab a").click(function () {
                selectedTab.val($(this).attr("href").substring(1));
            });
        });
    </script>--%>
 </head>      
<body>
    <form id="fromBody" runat="server">

        <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="True">
        </asp:ScriptManager>
        <asp:UpdatePanel ID="uppanelsms" runat="server">
            <ContentTemplate>
                <!-- Accordion -->
                <div class="content">

                    <div class="accordionMod panel-group" id="accordian">
                       
                        <div class="accordion-item" >
                            <div class="panel-heading">
                                <h4 class="accordion-toggle">Email/SMS History</h4>
                            </div>
                            <section class="accordion-inner panel-body">
                                <!-- Table  -->
                                <div class="table-responsive">
                                    <h4>SMS Sent</h4>
                                    <asp:GridView ID="GridMSMS" AutoGenerateColumns="False" runat="server"
                                        AllowPaging="True" EmptyDataText="No SMS sent"
                                        OnPageIndexChanging="GridMSMS_PageIndexChanging" PageSize="10"
                                        Width="100%" CssClass="table table-striped table-bordered table-responsive text-left my-orders-table" BorderStyle="None" BorderWidth="0px"
                                        HeaderStyle-HorizontalAlign="Center">

                                        <Columns>
                                            <asp:TemplateField HeaderText="ID" Visible="False">

                                                <ItemTemplate>
                                                    <center>
                                <%#Eval("LOG_CMS_ID")%></center>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Sms Text">
                                                <ItemTemplate>
                                                    <%-- <asp:Label
                                                                            ID="T12lblview" runat="server" Text='<%#Eval("tempview")%>' CssClass="girdpoplbl"></asp:Label><asp:LinkButton
                                                                            ID="lblsmsDetail" runat="server" Visible='<%# BIND("bShow") %>' CssClass="viewdetails">...Detail</asp:LinkButton>
                                                                        <div id="plR_loc" runat="server" class="gridMsg" visible='<%# BIND("bShow") %>'>
                                                                            <asp:Literal ID="ltRemarks" runat="server" Text='<%# BIND("CMS_SMS_TEXT") %>'>
                                                                            </asp:Literal>
                                                                        </div>--%>
                                                    <asp:Image ID="imgNew" ImageUrl="~/Images/FlashingNEW.gif" Visible='<%#Eval("NEW")%>'
                                        Style="display: inline; float: left; width: 30px; height: 20px; vertical-align: top;" runat="server" />
                                                    <asp:Label
                                                        ID="T12lblview" runat="server" Text='<%#Eval("tempview")%>' CssClass="girdpoplbl"></asp:Label>
                                                    <asp:LinkButton
                                                        ID="lblsmsDetail" runat="server" CssClass="viewdetails">...Detail</asp:LinkButton>
                                                    <asp:Panel ID="panelsms" runat="server">

                                                        <div id="plR_sms" runat="server" class="PopUpMessageStyle">
                                                            <asp:Literal ID="ltRemarks" runat="server" Text='<%# BIND("CMS_SMS_TEXT") %>'>
                                                            </asp:Literal>
                                                        </div>

                                                    </asp:Panel>
                                                    <ajaxToolkit:PopupControlExtender ID="pcet_Lib_Iss" runat="server"
                                                        OffsetX="-150" PopupControlID="plR_sms"
                                                        Position="Bottom" TargetControlID="lblsmsDetail">
                                                    </ajaxToolkit:PopupControlExtender>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Mobile Number">

                                                <ItemTemplate>
                                                    <center>
                                <%#Eval("LOG_MOBILE_NUMBER")%></center>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Sent Date">

                                                <ItemTemplate>
                                                    <center>
                                <%#Eval("LOG_ENTRY_DATE")%></center>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Status" Visible="False">

                                                <ItemTemplate>
                                                    <center>
                                <%#Eval("status")%></center>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                        </Columns>

                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <PagerStyle CssClass="gridpager" />
                                    </asp:GridView>

                                </div>
                            </section>
                        </div>
                        <div class="accordion-item" >
                            <div class="panel-heading">
                                <h4 class="accordion-toggle">Newsletter and Plain Text Emails</h4>
                            </div>
                            <section class="accordion-inner panel-body">


                                <!-- Table  -->
                                <div class="table-responsive">

                                    <asp:GridView ID="GridEmails" AutoGenerateColumns="False" runat="server"
                                        AllowPaging="True" EmptyDataText="No Emails Sent"
                                        PageSize="10" Width="100%" CssClass="table table-striped table-bordered table-responsive text-left my-orders-table" BorderStyle="None" BorderWidth="0px"
                                        HeaderStyle-HorizontalAlign="Center">

                                        <Columns>
                                            <asp:TemplateField HeaderText="ID" Visible="False">

                                                <ItemTemplate>
                                                    <center>
                                <%#Eval("LOG_EML_ID")%></center>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Title">

                                                <ItemTemplate>

                                                    <%--<asp:Label ID="T12lblview" runat="server" Text='<%#Eval("tempview")%>'></asp:Label>
                                                                        <asp:LinkButton ID="lbleMAILDetailEmail" runat="server" Visible='<%# BIND("bShow") %>' CssClass="viewdetails">...Detail</asp:LinkButton>
                                                                        <div id="plR_email" runat="server" class="gridMsg" visible='<%# BIND("bShow") %>'>
                                                                            <asp:Literal ID="ltRemarksEmail" runat="server" Text='<%# BIND("EML_TITLE") %>'>
                                                                            </asp:Literal>
                                                                        </div>--%>
                                                    <asp:Image ID="imgNew" ImageUrl="~/Images/FlashingNEW.gif" Visible='<%#Eval("NEW")%>'
                                        Style="display: inline; float: left; width: 30px; height: 20px; vertical-align: top;" runat="server" />
                                                    <asp:Label ID="T12lblview" runat="server" Text='<%#Eval("tempview")%>'></asp:Label>
                                                    <asp:LinkButton ID="lbleMAILDetailEmail" runat="server" CssClass="viewdetails" Style="display:none;">...Detail</asp:LinkButton>
                                                    <div id="plR_email" runat="server" class="PopUpMessageStyle">
                                                        <asp:Literal ID="ltRemarksEmail" runat="server" Text='<%# BIND("EML_TITLE") %>'>
                                                        </asp:Literal>
                                                    </div>
                                                    <ajaxToolkit:PopupControlExtender ID="pcet_Lib" runat="server"
                                                        OffsetX="-150" PopupControlID="plR_email"
                                                        Position="Bottom" TargetControlID="lbleMAILDetailEmail">
                                                    </ajaxToolkit:PopupControlExtender>

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Type">

                                                <ItemTemplate>
                                                    <center>
                                <%#Eval("EmailType")%></center>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Email ID">

                                                <ItemTemplate>
                                                    <center>
                                <%#Eval("LOG_EMAIL_ID")%></center>

                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Sent Date">

                                                <ItemTemplate>
                                                    <center>
                                <%#Eval("LOG_ENTRY_DATE")%></center>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Status" Visible="False">

                                                <ItemTemplate>
                                                    <center>
                                <%#Eval("status")%></center>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                        </Columns>

                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <PagerStyle CssClass="gridpager" />
                                    </asp:GridView>
                                </div>
                                <!-- /Table  -->
                            </section>
                        </div>
                         <div class="accordion-item" >
                            <div class="panel-heading">
                                <h4 class="accordion-toggle" >Notifications</h4>
                            </div>
                            <section class="accordion-inner panel-body">
                                <!-- Table  -->
                                <div class="table-responsive">
                                    <asp:GridView ID="GridNotifications" AutoGenerateColumns="False" runat="server"
                                        AllowPaging="True" EmptyDataText="No Notifications To Display"
                                        PageSize="10" Width="100%" CssClass="table table-striped table-bordered table-responsive text-left my-orders-table" BorderStyle="None" BorderWidth="0px"
                                        HeaderStyle-HorizontalAlign="Center">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Notification">
                                                <ItemTemplate>
                                                    <asp:HiddenField ID="hidViewId" runat="server" Value='<%#Eval("NLD_ID")%>'/>
                                                    <asp:Image ID="imgNew" ImageUrl="~/Images/FlashingNEW.gif" Visible='<%#Eval("NEW")%>'
                                        Style="display: inline; float: left; width: 30px; height: 20px; vertical-align: top;" runat="server" />
                                                    <asp:Label
                                                        ID="Notilblview" runat="server" Text='<%#Eval("tempview")%>' CssClass="girdpoplbl"></asp:Label>
                                                    <asp:LinkButton
                                                        ID="lblNotiDetail" runat="server" CssClass="viewdetails" OnClick="lblNotiDetail_Click">...Detail</asp:LinkButton>
                                                    <div id="plR_loc" runat="server" class="PopUpMessageStyle">
                                                        <asp:Literal ID="ltRemarks" runat="server" Text='<%# Bind("NOTIFICATION_CONTENT")%>'>
                                                        </asp:Literal>
                                                    </div>
                                                    <ajaxToolkit:PopupControlExtender ID="pcet_Lib_I" runat="server"
                                                        OffsetX="-150" PopupControlID="plR_loc"
                                                        Position="Bottom" TargetControlID="lblNotiDetail">
                                                    </ajaxToolkit:PopupControlExtender>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <PagerStyle CssClass="gridpager" />
                                    </asp:GridView>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
                <!--Accordion Ends -->
                <asp:HiddenField ID="hfTab" Value="1" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>

    </form>

    <!-- The Scripts -->
    <script src="../js/jquery.min.js"></script>
    <script src="../js/jquery-migrate-1.0.0.js"></script>
    <script src="../js/jquery-ui.js"></script>
    <script src="../js/bootstrap.js"></script>


    <script src="../js/jquery.wait.js"></script>
    <script src="../js/fappear.js"></script>
    <script src="../js/modernizr-2.6.2.min.js"></script>
    <script src="../js/jquery.bxslider.min.js"></script>

    <script src="../js/tytabs.js"></script>
    <script src="../js/jquery.gmap.min.js"></script>
    <script src="../js/jquery.sticky.js"></script>
    <script src="../js/jquery.countTo.js"></script>
    <script src="../js/jflickrfeed.js"></script>
    <script src="../js/jquery.knob.js"></script>
    <script src="../js/imagesloaded.pkgd.min.js"></script>
    <script src="../js/waypoints.min.js"></script>
    <script src="../js/wow.js"></script>
    <script src="../js/jquery.fitvids.js"></script>
    <script src="../js/spectrum.js"></script>
    <script src="../js/switcher.js"></script>
    <script src="../js/custom.js?<%=DateTime.Now.Ticks.ToString()%>"></script>

</body>
 </html>
