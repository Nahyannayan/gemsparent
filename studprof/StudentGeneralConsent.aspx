﻿<%@ Page Language="VB" MasterPageFile="~/ParentMaster.master" AutoEventWireup="false"
    CodeFile="StudentGeneralConsent.aspx.vb" Inherits="StudProf_StudentGeneralConsent"
    Title="Untitled Page" %>

<%@ Register Src="~/StudProf/UserControl/studContactDetailsMenu.ascx" TagName="contactDetails" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphParent" runat="Server">
    <link href="../CSS/Popup.css" rel="stylesheet" />
    <div class="content margin-top30 margin-bottom60">
        <div class="container">
            <div class="row">

                <!-- Posts Block -->

                <div class="posts-block col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="my-account">

                        <div class="title-box">
                                <h3>Update Student Details 
                                    <span class="profile-right">
                                <asp:Label ID="lbChildNameTop" runat="server"></asp:Label>
                            </span></h3>
                        </div>

                    <div style="display: none;">
                        <div class="left">
                            <font color="black">      Name :</font>
                            <asp:Label ID="lblChildName" CssClass="lblChildNameCss" runat="server"></asp:Label>
                        </div>
                        <div class="right">
                            <font color="black">  Fee ID :</font>
                            <asp:Label ID="lblFeeID" runat="server" CssClass="lblChildNameCss">
                            </asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </div>
                        <br />
                        <br />
                        <div class="left">
                            <font color="black">    Grade & Section :</font>
                            <asp:Label ID="lblGradeSection" CssClass="lblChildNameCss" runat="server"></asp:Label>
                        </div>

                        <div class="right">
                            <font color="black">     DOJ :</font>
                            <asp:Label ID="lblDateOfJoin" runat="server" CssClass="lblChildNameCss">
                            </asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </div>
                    </div>
                    <asp:Label ID="lblmsg" runat="server"></asp:Label>
                    <uc1:contactDetails ID="contactDetails1" runat="server" />

                    <div>
                        <div align="left">
                            <table style="width: 100%;" align="left" border="0" cellspacing="0" 
                                class="table table-striped table-bordered table-responsive text-left my-orders-table">
                                <tr >
                                    <td colspan="6" class="sub-heading">&nbsp;Photographs and Videos
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">

                                        <%=Session("BSU_NAME") %> has permission to include my child in internal school publications materials
                            like the year book. 
                              <asp:RadioButton ID="rdpermissionyes" runat="server" GroupName="rdpermissionGroup" Text="Yes" />
                                        <asp:RadioButton ID="rdpermissionNo" runat="server" GroupName="rdpermissionGroup" Text="No" />


                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <%=Session("BSU_NAME") %> has permission to include my child in marketing and external school
                            publications material (website, posters etc). 
                            
                             <asp:RadioButton ID="rdExternalYes" runat="server" GroupName="rdExternalGroup" Text="Yes" />
                                        <asp:RadioButton ID="rdExternalNo" runat="server" GroupName="rdExternalGroup" Text="No" />



                                    </td>
                                </tr>
                                <tr >
                                    <td colspan="6" class="sub-heading">&nbsp;School Directory
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <div class="tdPadDiv">
                                            <p>
                                                <%=Session("BSU_NAME") %> has permission to include our family phone and name information for the
                            annual in-school directory.
                                            </p>

                                            <asp:RadioButton ID="rdSchoolDirectoryYes" runat="server" GroupName="rdSchoolDirectoryGroup" Text="Yes" />
                                            <asp:RadioButton ID="rdSchoolDirectoryNo" runat="server" GroupName="rdSchoolDirectoryGroup" Text="No" />


                                        </div>
                                    </td>
                                </tr>
                                <tr >
                                    <td colspan="6" class="sub-heading">&nbsp;Field Trips
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <div class="tdPadDiv">
                                            I give permission for my child to participate in field trips:
                       (Thoughout your child's years at <%=Session("BSU_NAME") %>, field trips are planned in and out of the vicinity
                            of Dubai. All field trips are announced to parents in advance, and if at any time
                            we receive notice that you do not wish your child to go on a particular field trip,
                            we will certainly honour that request. Your concent here gives permission to attend
                            unless you inform the teacher otherwise.) 
                            
                             <asp:RadioButton ID="rdbFieldTripsYes" runat="server" GroupName="rdbFieldTripsYesGroup" Text="Yes" />
                                            <asp:RadioButton ID="rdbFieldTripsYesNo" runat="server" GroupName="rdbFieldTripsYesGroup" Text="No" />


                                        </div>
                                    </td>
                                </tr>

                                <tr id="trConsent" runat="server">
                                    <td colspan="6" class="sub-heading">&nbsp;Participation in Online Classes 
                                    </td>
                                </tr>
                                <tr id="trConsent1" runat="server">
 <td colspan="3">
                                        <div class="tdPadDiv">
                                           For the purposes of facilitating 24 hour access to parents/students/staff members to online lesson materials and discussions, quality and safeguarding assurance of online classes, assessment monitoring, and the professional development of staff members, I hereby:
                            <br /><br />
                                            <table>
                                                <tr>
                                                    <td style="vertical-align:top;"><asp:RadioButton ID="rbVideoAudio" runat="server" GroupName="rdbFieldTripsYesGroup1" Text=""  /></td>
                                                    <td>&nbsp;</td>
                                                    <td> Consent to the recording of <b> <%=Session("STU_NAME")%></b> ’s participation (image, video and voice) in his/her online classes through the remote learning tool designated by the school (e.g. Zoom, Microsoft Teams, etc.) and to the processing of my child’s personal data for such purposes.</td>
                                                </tr>
                                                <tr><td><br /></td><td></td><td></td></tr>
                                                <tr>
                                                    <td style="vertical-align:top;"><asp:RadioButton ID="rbNoVideoAudio" runat="server" GroupName="rdbFieldTripsYesGroup1" Text="" /></td><td>&nbsp;</td>
                                                    <td>Do not consent to the recording of  <b><%=Session("STU_NAME")%></b> ’s image, video and/or voice participation in his/her online classes. (If you do not wish to have your child recorded, then you should also ensure your child’s video and audio are turned off during the recorded part of the online class.)</td>
                                                </tr>
                                            </table>
                             
                                            <br />
                                            You may change your consent preferences by returning to this portal and updating your choice above at any time.
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" align="center">
                                        <asp:Button ID="btnPrev" runat="server" Text="PREVIOUS" CssClass="btn btn-info"  OnClick="btnPrev_Click" />&nbsp;
                        <asp:Button ID="btnSaveContinue" runat="server" Text="SUBMIT" CssClass="btn btn-info" OnClick="btnSaveContinue_Click"  />
                                    </td>
                                </tr>
                            </table>
                        </div>

<div id="pldis" runat="server" style="display: none; overflow: visible; background-color: White; border-color: #b5cae7; border-style: solid; border-width: 4px;">
                                <div>
                                    <span style="clear: right; display: inline; float: right; visibility: visible; margin-top: 0px; vertical-align: top;">
                                        <asp:ImageButton ID="btnCloseedit" runat="server" ImageUrl="~/Images/close.png" /></span>
                                </div>
                                <div class="mainheading">
                                    <div>
                                        <asp:Label ID="lblPopHead" runat="server" Text="Consent"></asp:Label></div>

                                </div>
                                <asp:Panel ID="plStudAtt" runat="server"  ScrollBars="Auto">
                                    <table class="table  table-condensed table-responsive text-left my-orders-table" align="center" cellpadding="8" cellspacing="3" >
                                       <tr>
                                           <td align="left" class="tdfields">
                                               Thank you for your response. <br />
                                               We have registered your consent preference for  <%=Session("STU_NAME")%>. 

                                           </td>
                                            <td align="left" colspan="3">
                                                </td>
                                       </tr>
                                           <tr>
                                            <td align="left" class="tdfields">Please also review your consent preference for:   </td>
                                            <td align="left" colspan="3">
                                                 <asp:DropDownList ID="ddlChildNames" runat="server" CssClass="form-control"></asp:DropDownList> 
                                               
                                            </td>

                                        </tr>
                                        
                                       
                                        <%--<tr>
                                            <td style="width: 241px; height: 19px" align="left" colspan="4"></td>

                                        </tr>--%>
                                        <tr>
                                            <td align="center" colspan="4">
                                                <asp:Button ID="btnCloseAbs" runat="server" Text="Cancel" CssClass="btn btn-info" />&nbsp;
                                                <asp:Button ID="btnAdd" runat="server" Text="Proceed" CssClass="btn btn-info" ValidationGroup="popValid" />
                        
                                            </td>
                                        </tr>
                                        <%--<tr>
                                            <td align="center" colspan="4"></td>
                                        </tr>--%>
                                        <tr>
                                            <td align="left" colspan="4">
                                                <asp:Label ID="lblPopError" runat="server"></asp:Label>
                                                <asp:ValidationSummary ID="vsVisaDetails" runat="server" ValidationGroup="popValid"
                                                    HeaderText="<div class='validationheader'>Please correct the following:</div>"
                                                    CssClass="text-danger" ForeColor="" />

                                            </td>
                                        </tr>
                                    </table>

                                </asp:Panel>
       

        <asp:Button ID="btnShowPopup" runat="server" Style="display: none" />
                            <ajaxToolkit:ModalPopupExtender
                                ID="mdlPopup" runat="server" TargetControlID="btnShowPopup" PopupControlID="pldis"
                                BackgroundCssClass="modalBackground" DropShadow="false" RepositionMode="RepositionOnWindowResizeAndScroll">
                            </ajaxToolkit:ModalPopupExtender>
                            </div>

                         <div id="testpopup" class="darkPanelM anim" runat="server" style="display: none; z-index: 3000 !important;">
                <div id="divboxpanelconfirm" runat="server" style="height: 50%; width: 45%; margin-left: 30%; margin-top: 10%;" class="darkPanelMTop">
                    <div class="holderInner" style="height: 90%; width: 98%;">
                        <center>
                            <table cellpadding="5" cellspacing="2" border="0" style="height: 90% !important; width: 100% !important; overflow-y: scroll;"
                                class="tableNoborder">
                                
                                <tr>
                                    <td valign="top">
                                        <span  style="float: left; margin: 0 7px 50px 0;"></span><br />
                                        <h4>Data Saved Successfully</h4><br />
                                         Click to proceed  
                                        <br />
                                        <br />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">
                                        <asp:Button ID="btnProceed" runat="server" CausesValidation="False" CssClass="btn btn-info"
                                            Text="Proceed" />
                                    
                                    </td>
                                </tr>
                            </table>
                        </center>
                    </div>
                </div>
            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
     </div>
</asp:Content>
