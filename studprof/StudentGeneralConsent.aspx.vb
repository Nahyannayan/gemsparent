﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO
Imports System.Web.Configuration
Partial Class StudProf_StudentGeneralConsent
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            Page.Title = OASISConstants.Gemstitle
            If Session("username") Is Nothing Then
                Session("Active_tab") = "Home"
                Session("Site_Path") = ""
                Response.Redirect("~\Login.aspx")
            ElseIf Session("bPasswdChanged") = "False" Then
                Response.Redirect("~\UpdateInfo\ForcePasswordChange.aspx", False)
                'ElseIf Session("bUpdateContactDetails") = "False" Then

                '    If String.IsNullOrEmpty(Convert.ToString(Session("bStuSteps"))) Then


                '        If (Convert.ToString(Session("isNewContact")) = "True") Then
                '            Response.Redirect("~\StudProf\StudentMainDetails.aspx", False)
                '        Else
                '            Response.Redirect(Session("ForceUpdate_stud"), False)
                '        End If
                '    End If
            End If
            If Page.IsPostBack = False Then
                Student_Gen_Details(Session("STU_ID"))
                If show_consent() = 1 Then
                    trConsent.Visible = True
                    trConsent1.Visible = True

                    'rbVideoAudio.Text = "Consent to the recording of " + Session("STU_NAME") + "’s participation (image, video and voice) in his/her online classes through the remote learning tool designated by the school (e.g. Zoom, Microsoft Teams, etc.) and to the processing of my child’s personal data for such purposes. "
                    ' rbNoVideoAudio.Text = "Do not consent to the recording of " + Session("STU_NAME") + "’s image, video and/or voice participation in his/her online classes. (If you do not wish to have your child recorded, then you should also ensure your child’s video and audio are turned off during the recorded part of the online class.)"

                    Session("show_consent") = 1
                    getconsent()
                Else
                    trConsent.Visible = False
                    trConsent1.Visible = False
                    Session("show_consent") = 0
                End If


            End If
            'lblChildName.Text = Session("STU_NAME")
            'trShift.Visible = False
            'trMins.Visible = False
            lbChildNameTop.Text = Session("STU_NAME")
            Session("names_exist") = 0
        Catch ex As Exception

        End Try


    End Sub
    Function show_consent() As Integer
        Dim con As String = ConnectionManger.GetOASISConnectionString
        Dim param(1) As SqlClient.SqlParameter
        Dim i As Integer = 0
        param(0) = New SqlClient.SqlParameter("@STU_ID", Session("STU_ID"))
        param(1) = New SqlClient.SqlParameter("@OLU_NAME", Session("username"))

        Dim ds As DataSet = SqlHelper.ExecuteDataset(con, "opl.show_consent", param)
        If ds.Tables(0).Rows.Count > 0 Then

            i = ds.Tables(0).Rows(0)("IsVisible")
        End If
        Return i
    End Function
    Sub bindNames()
        Try
            'Dim strPath As String = Convert.ToString(readerStudent_Detail("STU_PHOTOPATH"))
            Dim connPath As String = WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ConnectionString
            Dim noImagePath As String = "~/Images/Home/no_image.gif"
            Dim con As String = ConnectionManger.GetOASISConnectionString
            Dim param(5) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@OLU_ID", Session("OLU_ID").ToString)
            param(1) = New SqlClient.SqlParameter("@connPath", connPath)
            param(2) = New SqlClient.SqlParameter("@noImagePath", noImagePath)
            param(3) = New SqlClient.SqlParameter("@STU_BSU_ID", Session("STU_BSU_ID")) '@
            param(4) = New SqlClient.SqlParameter("@SELECTED_STU_ID", 0)
            param(5) = New SqlClient.SqlParameter("@type", 0)
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "[OPL].[GETCHILD_HOME_INFO_CONSENT]", param)
            If ds.Tables(0).Rows.Count > 0 Then
                ddlChildNames.DataSource = ds.Tables(0)

                ddlChildNames.DataValueField = "STU_ID"
                ddlChildNames.DataTextField = "SNAME"
                ddlChildNames.DataBind()
                Session("names_exist") = 1
            End If
        Catch ex As Exception


        End Try
    End Sub
    Sub getconsent()
        Dim con As String = ConnectionManger.GetOASISConnectionString
        Dim param(1) As SqlClient.SqlParameter
        Dim i As Integer = 0
        param(0) = New SqlClient.SqlParameter("@OCM_ACD_ID", Session("STU_ACD_ID"))
        param(1) = New SqlClient.SqlParameter("@OCM_STU_ID", Session("STU_ID"))

        Dim ds As DataSet = SqlHelper.ExecuteDataset(con, "ONLINE.GET_CONSENT", param)
        If ds.Tables(0).Rows.Count > 0 Then

            i = ds.Tables(0).Rows(0)("OCM_STATUS")

            If i = 1 Then
                rbVideoAudio.Checked = True
                'ElseIf i = 2 Then
                '    rbAudio.Checked = True
            ElseIf i = 2 Then
                rbNoVideoAudio.Checked = True
            End If

        End If

    End Sub
    Protected Sub btnPrev_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Response.Redirect("~\StudProf\StudHealthDetails.aspx", False)
    End Sub

    Protected Sub btnSaveContinue_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveContinue.Click
        Try
           


            Dim status As Integer
            Dim transaction As SqlTransaction
            Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                transaction = conn.BeginTransaction("SampleTransaction")
                status = callTrans_Student_General_Consent_Details(transaction)

                If Session("show_consent") = 1 Then
                    status = callTrans_Consent_Details(transaction)
                End If
                'Dim lblmsg As Label = CType(Page.Master.FindControl("lblSiteMessage"), Label)
                lblmsg.Text = ""
                If status = 1000 Then
                    lblmsg.CssClass = "alert-warning alert"
                    lblmsg.Text = "Please enter the details"
                    transaction.Rollback()
                ElseIf status <> 0 Then
                    lblmsg.CssClass = "alert-warning alert"
                    lblmsg.Text = "Please enter the details"
                    transaction.Rollback()
                Else
                    'divUploadmsg.CssClass = ""
                    'divUploadmsg.Text = ""
                    lblmsg.CssClass = "alert-warning alert"
                    lblmsg.Text = "Data saved successfully"
                    UtilityObj.InsertAuditdetails(transaction, "edit", "STUDENT_M", "STU_ID", "STU_ID", "STU_ID=" + Session("STU_ID").ToString)
                    UtilityObj.InsertAuditdetailsWithoutGuid(transaction, "edit", "online.ONLINE_USERS_M", "OLU_ID", "OLU_ID", "OLU_ID=" + Session("OLU_ID").ToString)
                    transaction.Commit()
                    Session.Remove("bStuSteps")
                    Session("bUpdateContactDetails") = True
                    If Session("show_consent") = 1 Then
                        bindNames()
                        If Session("names_exist") = 1 Then
                            mdlPopup.Show()
                        End If
                    Else
                        Me.btnSaveContinue.Visible = False
                        Me.testpopup.Style.Item("display") = "block"
                    End If
                    

                    

                End If

            End Using
        Catch ex As Exception

        End Try
    End Sub

    Sub Student_Gen_Details(ByVal STU_ID As String)


        Dim CONN As String = ConnectionManger.GetOASISConnectionString
        Dim param(2) As SqlParameter
        param(0) = New SqlParameter("@STU_ID", STU_ID)


        Using readerStudent_D_Detail As SqlDataReader = SqlHelper.ExecuteReader(CONN, CommandType.StoredProcedure, "Get_Student_General_Consent", param)
            If readerStudent_D_Detail.HasRows = True Then
                While readerStudent_D_Detail.Read


                    lblChildName.Text = Session("STU_NAME")
                    lblGradeSection.Text = Convert.ToString(readerStudent_D_Detail("GRD_ID_JOIN")) & "-" & Convert.ToString(readerStudent_D_Detail("SCT_DESCR_JOIN"))

                    lblFeeID.Text = Convert.ToString(readerStudent_D_Detail("FEE_ID"))
                    If IsDate(readerStudent_D_Detail("SDOJ")) = True Then
                        lblDateOfJoin.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((readerStudent_D_Detail("SDOJ"))))
                    End If
                    If Not (Convert.ToString(readerStudent_D_Detail("STU_is_internal_pub"))) Is Nothing Then
                        If ((Convert.ToString(readerStudent_D_Detail("STU_is_internal_pub"))) = "Yes") Then
                            rdpermissionyes.Checked = True
                        Else
                            rdpermissionNo.Checked = True
                        End If

                    End If

                    If Not (Convert.ToString(readerStudent_D_Detail("STU_is_family_phone"))) Is Nothing Then
                        If ((Convert.ToString(readerStudent_D_Detail("STU_is_family_phone"))) = "Yes") Then
                            rdSchoolDirectoryYes.Checked = True
                        Else
                            rdSchoolDirectoryNo.Checked = True
                        End If

                    End If

                    If Not (Convert.ToString(readerStudent_D_Detail("STU_is_marketing_pub"))) Is Nothing Then
                        If ((Convert.ToString(readerStudent_D_Detail("STU_is_marketing_pub"))) = "Yes") Then
                            rdExternalYes.Checked = True
                        Else
                            rdExternalNo.Checked = True
                        End If

                    End If


                    If Not (Convert.ToString(readerStudent_D_Detail("STU_is_field_Trips"))) Is Nothing Then
                        If ((Convert.ToString(readerStudent_D_Detail("STU_is_field_Trips"))) = "Yes") Then
                            rdbFieldTripsYes.Checked = True
                        Else
                            rdbFieldTripsYesNo.Checked = True
                        End If

                    End If


                End While
            Else
            End If
        End Using
    End Sub

    Function callTrans_Student_General_Consent_Details(ByVal trans As SqlTransaction) As Integer
        Try
            Dim Status As Integer = 0
            Dim STU_is_internal_pub As Boolean
            Dim STU_is_marketing_pub As Boolean
            Dim STU_is_family_phone As Boolean
            Dim STU_is_field_Trips As Boolean
            Dim STU_is_directory As Boolean

             If rdpermissionyes.Checked Then
                STU_is_internal_pub = True
            Else
                STU_is_internal_pub = False
            End If

            If rdExternalYes.Checked Then
                STU_is_marketing_pub = True
            Else
                STU_is_marketing_pub = False
            End If

            If rdbFieldTripsYes.Checked Then
                STU_is_field_Trips = True
            Else
                STU_is_field_Trips = False
            End If
            If rdSchoolDirectoryYes.Checked Then
                STU_is_family_phone = True
            Else
                STU_is_family_phone = False
            End If
           

            Dim pParms(8) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@STU_ID", Session("STU_ID"))

            pParms(1) = New SqlClient.SqlParameter("@STU_is_internal_pub", STU_is_internal_pub)

            pParms(2) = New SqlClient.SqlParameter("@STU_is_marketing_pub", STU_is_marketing_pub)
            pParms(3) = New SqlClient.SqlParameter("@STU_is_family_phone", STU_is_family_phone)
            pParms(4) = New SqlClient.SqlParameter("@STU_is_field_Trips", STU_is_field_Trips)
            pParms(5) = New SqlClient.SqlParameter("@STU_is_directory", STU_is_directory)
            pParms(6) = New SqlClient.SqlParameter("@OLU_ID", Convert.ToInt32(Session("OLU_ID").ToString))

            pParms(7) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(7).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "update_Stu_Photo_And_Video", pParms)
            Dim ReturnFlag As Integer = pParms(7).Value
            Return ReturnFlag
        Catch ex As Exception
            Return 1000
        End Try
    End Function

    Function callTrans_Consent_Details(ByVal trans As SqlTransaction) As Integer
        Try
            Dim Status As Integer = 0
            If rbVideoAudio.Checked Then
                Status = 1
                'ElseIf rbAudio.Checked Then
                '    Status = 2
            ElseIf rbNoVideoAudio.Checked Then
                Status = 2
            End If


            Dim pParms(8) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@OCM_ACD_ID", Session("STU_acd_ID"))
            pParms(1) = New SqlClient.SqlParameter("@OCM_STU_ID", Session("STU_ID"))
            pParms(2) = New SqlClient.SqlParameter("@OCM_STATUS", Status)
            pParms(3) = New SqlClient.SqlParameter("@OCL_USER ", Session("username"))
            pParms(4) = New SqlClient.SqlParameter("@OCL_SOURCE", "PORTAL")
    

            pParms(5) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(5).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "ONLINE.SAVE_CONSENT", pParms)
            Dim ReturnFlag As Integer = pParms(5).Value
            Return ReturnFlag
        Catch ex As Exception
            Return 1000
        End Try
    End Function



    Protected Sub btnProceed_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnProceed.Click
        Response.Redirect("~\Home.aspx", False)
    End Sub

    Protected Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        Session("STU_ID") = ddlChildNames.SelectedValue
        Session("STU_NAME") = ddlChildNames.SelectedItem.Text
        Response.Redirect("~\studprof\StudentGeneralConsent.aspx", False)

    End Sub
End Class
