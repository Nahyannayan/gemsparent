<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ProfAttendance.aspx.vb"
 Inherits="Stud_Prof_ProfAttendance" title="GEMS EDUCATION" %>

<%@ Import Namespace="InfoSoftGlobal" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head runat="server">
<title ></title>   
    <script type="text/javascript" src="/Scripts/jquery-1.8.2.min.js"></script>
    <script type="text/javascript" src="/Scripts/jquery-ui-1.10.2.js"></script>  
    <script type="text/javascript" src="/Scripts/Fancybox/jquery.fancybox.pack.js?1=2"></script>
    <script type="text/javascript" src="/Scripts/Fancybox/jquery.fancybox.js?1=2"></script>
    <link type="text/css" href="/Scripts/Fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
    <link href="/css/Popup.css" rel="stylesheet" />
    <link href="/css/SiteStyle.css?1=2" rel="stylesheet" type="text/css" />
    <link href="/css/MenuStyle.css?1=2" rel="stylesheet" type="text/css" />
    <link href="/css/StyleBox.css?1=2" rel="stylesheet" type="text/css" />
    <link href="/css/style.css" rel="stylesheet" type="text/css" />
    <link href="/css/bootstrap.css" rel="stylesheet" type="text/css" />
</head>
<body>
  <form id="form1" runat="server">
      <asp:ScriptManager ID="scriptmanager1" runat="server">
      </asp:ScriptManager>
       <div class="popup-wrap">
          <asp:UpdatePanel ID="UpdatePanel1" runat="server">
              <ContentTemplate>
    <div class="mainheading" style="display:none;">
      <div class="left">Attendance</div>
       <div class="right"><asp:label ID="lbChildName" runat="server" CssClass="lblChildNameCss" ></asp:label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
      </div>
    
   <div id="divatt" class="">

              <h3 class="mainheading">Attendance</h3>

              <table class="table table-striped table-bordered table-responsive text-left my-orders-table">
    <tr class="tdblankTop">
                           <td    align="center" colspan="6"
                            class="tdblankTop" 
                             > <asp:Literal ID="FCLiteral" runat="server"></asp:Literal></td>
                             </tr>
               
    
        <tr class="tdblankTop">
            <td align="left" colspan="3" class="tdblankTop">
            <asp:Literal ID="FCAbsent" runat="server"></asp:Literal>&nbsp;
            </td>
            
            <td align="left" colspan="3" class="tdblankTop">
            <asp:Literal ID="FCLate" runat="server"></asp:Literal>&nbsp;
            </td>
        
        </tr>
    </table>
    
                    
       <asp:HiddenField ID="HF_stuid" runat="server" />
        <asp:Label ID="lblerror" runat="server" CssClass="error"></asp:Label>
      </div>
     </ContentTemplate>
              </asp:UpdatePanel>
           </div>
    
  </form>
    
    </body></html>

