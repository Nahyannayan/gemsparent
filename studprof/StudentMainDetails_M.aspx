﻿<%@ Page Language="VB" MasterPageFile="~/ParentMaster.master" AutoEventWireup="false"
    CodeFile="StudentMainDetails_M.aspx.vb" Inherits="StudProf_StudentMainDetails"
    Title="Untitled Page" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="~/StudProf/UserControl/studContactDetailsMenu.ascx" TagName="contactDetails"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphParent" runat="Server">
         <script>
             if ($(window).width() < 979) {
                 if ($(location).attr("href").indexOf("StudentMainDetails_M.aspx") == -1) {
                     window.location = "\\StudProf\\StudentMainDetails_M.aspx";
                 }
             }
             if ($(window).width() > 979) {
                 if ($(location).attr("href").indexOf("StudentMainDetails.aspx") == -1) {
                     alert((location).attr("href"));
                     window.location = "\\StudProf\\StudentMainDetails.aspx";
                 }
             }
    </script>
    <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtDob"
        Format="dd/MMM/yyyy" PopupButtonID="ImageButton1">
    </ajaxToolkit:CalendarExtender>
    <%-- <div align="center" style="width:200px;text-align:center;" id="divMainProg" runat="server" >--%>
    <!-- Posts Block -->
    <div class="content margin-top30 margin-bottom60">
        <div class="container">
            <div class="row">

                <!-- Posts Block -->
                <div class="posts-block col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="my-account">
                        <div  class="alert alert-warning">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <div>
                                &nbsp;&nbsp;&bull;&nbsp;&nbsp;For changing the primary contact, please send a request
                to the school registrar
                <br />
                                &nbsp;&nbsp;&bull;&nbsp;&nbsp;An asterisk (<font color="#ff0000">*</font>) indicates
                mandatory fields.
                <br />
                                &nbsp;&nbsp;&bull;&nbsp;&nbsp;All tabs needs to be completed to access the other details.
                <br />
                                &nbsp;&nbsp;&bull;&nbsp;&nbsp;Sibling information can be updated only after completing all the details through Update Information tab.
                            </div>
                        </div><br />
                        <asp:Label ID="lblmsg" runat="server" ></asp:Label><br />
                        <uc1:contactDetails ID="contactDetails1" runat="server" />
                        <%-- </div>--%>
                        <div>

                            <div align="left">
                                <br />
                                <table style="width: 100%;" cellspacing="0" cellpadding="3" class="table table-striped table-bordered table-responsive text-left my-orders-table">
                                    <tr >
                                        <td  class="sub-heading">&nbsp;Primary Details
                                        </td>
                                    </tr>
                                    <tr>
                                        <td  align="left">
                                            <asp:ValidationSummary ID="ValidationSummary2"
                                                EnableClientScript="true" runat="server" ValidationGroup="vgMain" HeaderText="<div class='error text-danger'>Following fields cannot be left empty:</div>"
                                                CssClass="text-danger" ForeColor="" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td >
                                            <div class="sub-heading" style="display:none;">
                                                <div>
                                                    Upload Photo
                                                </div>
                                                <div style="display:none;">
                                                    <asp:Label ID="lbChildName" runat="server" CssClass="lblChildNameCss" Visible="false"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                </div>
                                            </div>

                                            <table style="width: 100%; color: #084777; font-size: 12px;" align="left" border="0" cellspacing="0" class="tableNoborder">
                                                <tr>
                                                    <td align="left">
                                                        <div id="box" style="width: 140px">
                                                            <div id="lb">
                                                                <div id="rb">
                                                                    <div id="bb">
                                                                        <div id="blc">
                                                                            <div id="brc">
                                                                                <div id="tb">
                                                                                    <div id="tlc">
                                                                                        <div id="trc">
                                                                                            <div id="content2">
                                                                                                <asp:Image ID="imgParentImage" runat="server" Height="132px" ImageUrl="~/Images/Common/PageBody/no_image.gif"
                                                                                                    Width="125px" />
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div id="trUploadBtn" runat="server">
                                                            <asp:FileUpload ID="fileUpload" runat="server" Style="width: 260px; height: 22px; margin-top: 2px;" />
                                                            <asp:Button ID="btnSave" runat="server" Text="Save Photo"  CssClass="btn btn-info" />
                                                            <asp:Label ID="divUploadmsg" runat="server"></asp:Label>
                                                            <br />
                                                            <div class="alert alert-info" id="spnUploadMsg" runat="server">&nbsp;Upload a recent photograph
                                            of your child in school uniform.
                                            <br />
                                                                &nbsp;Upload file format:JPG, file size max:20KB, resize to:3.5cmx4.5cm</div>
                                                        </div>
                                                    </td>
                                                    <td align="left" valign="top">
                                                        <table style="width: 100%;" align="left" border="0" cellspacing="8" cellpadding="8" 
                                                            class="table table-striped table-bordered table-responsive text-left my-orders-table">
                                                            <tr>
                                                                <td align="left" style="vertical-align:middle">Name</td>
                                                                <td>
                                                                    <asp:Label ID="lblChildName" CssClass="lblChildNameCss" runat="server" ForeColor="black"></asp:Label></td>
                                                            </tr>

                                                            <tr>
                                                                <td align="left" style="vertical-align:middle">Student ID</td>
                                                                <td>
                                                                    <asp:Label ID="lblFeeID" runat="server" CssClass="lblChildNameCss" ForeColor="black">
                                                                    </asp:Label></td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left" style="vertical-align:middle">Grade & Section</td>
                                                                <td>
                                                                    <asp:Label ID="lblGradeSection" CssClass="lblChildNameCss" runat="server" ForeColor="black"></asp:Label></td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left" style="vertical-align:middle">Joining Date</td>
                                                                <td>
                                                                    <asp:Label ID="lblDateOfJoin" runat="server" CssClass="lblChildNameCss" ForeColor="black">
                                                                    </asp:Label></td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>

                                            <asp:HiddenField ID="hfParent" runat="server" />
                                            <asp:HiddenField ID="hfUploadPath" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align:middle">Full Name in Passport
                                        <br />
                                            <asp:TextBox ID="txtStudentNamePP" runat="server"  CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                        </td>
                                        </tr>
                                    <tr>
                                        <td style="vertical-align:middle">Name as in National Id
                                        
                                            <br />
                                            <asp:TextBox ID="txtStudentNameGiven" runat="server"  CssClass="form-control" ReadOnly="true"></asp:TextBox>

                                            <%--<div class="remark">(what name would your child be called in class, for example: Chin Min Cho will be called Diego)</div>--%>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td style="vertical-align:middle">Gender<span style="color: Red;">*</span>
                                        <br />
                                            <asp:RadioButtonList ID="RBLgender" runat="server" RepeatDirection="Horizontal" CssClass="radioAns" Enabled="false">
                                                <asp:ListItem Value="M">Male</asp:ListItem>
                                                <asp:ListItem Value="F">Female</asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                         </tr>
                                    <tr>
                                        <td style="vertical-align:middle">Religion<span style="color: Red;"></span>
                                            <br />
                                            <asp:DropDownList ID="ddlReligion" runat="server" CssClass="form-control" Enabled="false">
                                            </asp:DropDownList>
                                         <%--   <asp:RequiredFieldValidator ID="rfvRel" runat="server" ControlToValidate="ddlReligion"
                                                InitialValue="0" ErrorMessage="Religion" ValidationGroup="vgMain" Display="None"></asp:RequiredFieldValidator>--%></td>
                                    </tr>

                                    <tr>
                                        <td style="vertical-align:middle">Date of Birth<span style="color: Red;"> </span>
                                       <br />
                                            <asp:TextBox ID="txtDob" runat="server"  CssClass="form-control" MaxLength="11" ReadOnly="true"></asp:TextBox>
                                            <asp:ImageButton ID="ImageButton1" CssClass="pos-absolute" runat="server" ImageUrl="~/Images/Calendar.png"  Enabled="false"/>
                                          <%--  <asp:RequiredFieldValidator ID="rfvDob" runat="server" ControlToValidate="txtDob"
                                                ErrorMessage="Date of Birth" ValidationGroup="vgMain" Display="None"></asp:RequiredFieldValidator>
                                         <asp:RegularExpressionValidator ID="revFEmir_Exp_date"
                                            runat="server" ControlToValidate="txtDob" Display="Dynamic" EnableViewState="False"
                                            ErrorMessage="Invalid Date  format (dd/mmm/yyyy)" ForeColor="red"
                                            ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                            ValidationGroup="vgMain"></asp:RegularExpressionValidator>--%>
                                        </td>
                                         </tr>
                                    <tr>
                                        <td style="vertical-align:middle">Country Of Birth<span style="color: Red;"></span>
                                        <br />
                                            <asp:DropDownList ID="ddlCountry_Birth" runat="server" CssClass="form-control" ReadOnly="true" Enabled="false">
                                            </asp:DropDownList>
                                          <%--  <asp:RequiredFieldValidator ID="rfvCob" runat="server" ControlToValidate="ddlCountry_Birth"
                                                ErrorMessage="Country of birth " ValidationGroup="vgMain" Display="None"></asp:RequiredFieldValidator>--%>
                                        </td>
                                    </tr>

                                    <tr >
                                        <td style="vertical-align:middle">Nationality 1<span style="color: Red;"></span><br />
                                           
                                        <br />
                                            <asp:DropDownList ID="ddlNationality_Birth" runat="server" CssClass="form-control" Enabled="false">
                                            </asp:DropDownList>
                                            <%--<asp:RequiredFieldValidator ID="rfvNationa" runat="server" ControlToValidate="ddlNationality_Birth"
                                                ErrorMessage="Nationality" ValidationGroup="vgMain" Display="None"></asp:RequiredFieldValidator>--%>
                                       </td>
                                         </tr>
                                    <tr>
                                         <td style="vertical-align:middle">Nationality 2<br />
                                            (If holding dual Nationality)<br />
                                       
                                            <asp:DropDownList ID="ddlNationality_Birth1" runat="server" CssClass="form-control" Enabled="false">
                                            </asp:DropDownList></td>
                                    </tr>

                                    <tr  >
                                        <td  class="sub-heading">&nbsp;Passport/Visa/National ID Details
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align:middle">Passport Number<span style="color: Red;">*</span>
                                       <br />
                                            <asp:TextBox ID="txtPPNo" runat="server" CssClass="form-control"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvPp" runat="server" ControlToValidate="txtPPNo"
                                                ErrorMessage="Passport No" ValidationGroup="vgMain" Display="None"></asp:RequiredFieldValidator>
                                        </td>
                                         </tr>
                                    <tr>
                                        <td style="vertical-align:middle" >Passport Issue Place<span style="color: Red;">*</span>
                                       <br />
                                            <asp:TextBox ID="txtPPIssuPlace" runat="server" CssClass="form-control"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvPPIssue" runat="server" ControlToValidate="txtPPIssuPlace"
                                                ErrorMessage="Passport Issue Place" ValidationGroup="vgMain" Display="None"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align:middle" >Passport Issue Date<span style="color: Red;">*</span>
                                       <br />
                                            <asp:TextBox ID="txtPIssDate" runat="server"  CssClass="form-control" MaxLength="11"></asp:TextBox>
                                            <asp:ImageButton ID="ImageButton2" CssClass="pos-absolute" runat="server" ImageUrl="~/Images/Calendar.png" />
                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtPIssDate"
                                                Format="dd/MMM/yyyy" PopupButtonID="ImageButton2">
                                            </ajaxToolkit:CalendarExtender>
                                            <asp:RequiredFieldValidator ID="rfvppIssuedt" runat="server" ControlToValidate="txtPIssDate"
                                                ErrorMessage="Passport Issue Date" ValidationGroup="vgMain" Display="None"></asp:RequiredFieldValidator>
                                         <asp:RegularExpressionValidator ID="RegularExpressionValidator1"
                                            runat="server" ControlToValidate="txtPIssDate" Display="Dynamic" EnableViewState="False"
                                            ErrorMessage="Invalid Date  format (dd/mmm/yyyy)" ForeColor="red"
                                            ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                            ValidationGroup="vgMain"></asp:RegularExpressionValidator>
                                        </td>
                                         </tr>
                                    <tr>
                                        <td style="vertical-align:middle" >Passport Expiry Date<span style="color: Red;">*</span>
                                       <br />
                                            <asp:TextBox ID="txtPExpDate" runat="server"  CssClass="form-control" MaxLength="11"></asp:TextBox>
                                            <asp:ImageButton ID="ImageButton3" CssClass="pos-absolute" runat="server" ImageUrl="~/Images/Calendar.png" />
                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server" TargetControlID="txtPExpDate"
                                                Format="dd/MMM/yyyy" PopupButtonID="ImageButton3">
                                            </ajaxToolkit:CalendarExtender>
                                            <asp:RequiredFieldValidator ID="rfvexpDt" runat="server" ControlToValidate="txtPExpDate"
                                                ErrorMessage="Passport Expiry Date" ValidationGroup="vgMain" Display="None"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator3"
                                            runat="server" ControlToValidate="txtPExpDate" Display="Dynamic" EnableViewState="False"
                                            ErrorMessage="Invalid Date  format (dd/mmm/yyyy)" ForeColor="red"
                                            ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                            ValidationGroup="vgMain"></asp:RegularExpressionValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align:middle" >Visa Number
                                        <br />
                                            <asp:TextBox ID="txtVsaNo" runat="server" CssClass="form-control"></asp:TextBox>
                                        </td>
                                         </tr>
                                    <tr>
                                        <td style="vertical-align:middle" >Visa Issue Place
                                        <br />
                                            <asp:TextBox ID="txtVIssPlace" runat="server" CssClass="form-control"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align:middle" >Visa Issue Date
                                        <br />
                                            <asp:TextBox ID="txtVIssDate" runat="server"  CssClass="form-control" MaxLength="11"></asp:TextBox>
                                            <asp:ImageButton ID="ImageButton4" CssClass="pos-absolute" runat="server" ImageUrl="~/Images/Calendar.png" />
                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender4" runat="server" TargetControlID="txtVIssDate"
                                                Format="dd/MMM/yyyy" PopupButtonID="ImageButton4">
                                            </ajaxToolkit:CalendarExtender>
                                             <asp:RegularExpressionValidator ID="RegularExpressionValidator4"
                                            runat="server" ControlToValidate="txtVIssDate" Display="Dynamic" EnableViewState="False"
                                            ErrorMessage="Invalid Date  format (dd/mmm/yyyy)" ForeColor="red"
                                            ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                            ValidationGroup="vgMain"></asp:RegularExpressionValidator>
                                        </td>
                                         </tr>
                                    <tr>
                                        <td style="vertical-align:middle" >Visa Expiry Date
                                        <br />
                                            <asp:TextBox ID="txtVExpDate" runat="server"  CssClass="form-control" MaxLength="11"></asp:TextBox>
                                            <asp:ImageButton ID="ImageButton5" CssClass="pos-absolute" runat="server" ImageUrl="~/Images/Calendar.png" />
                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender5" runat="server" TargetControlID="txtVExpDate"
                                                Format="dd/MMM/yyyy" PopupButtonID="ImageButton5">
                                            </ajaxToolkit:CalendarExtender>
                                             <asp:RegularExpressionValidator ID="RegularExpressionValidator5"
                                            runat="server" ControlToValidate="txtVExpDate" Display="Dynamic" EnableViewState="False"
                                            ErrorMessage="Invalid Date  format (dd/mmm/yyyy)" ForeColor="red"
                                            ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                            ValidationGroup="vgMain"></asp:RegularExpressionValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align:middle" >Issuing Authority
                                       <br />
                                            <asp:TextBox ID="txtVIssAuth" runat="server" CssClass="form-control"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align:middle" >National ID Number
                                        <br />
                                            <asp:TextBox ID="txtEmiratesID" runat="server" CssClass="form-control"> </asp:TextBox>
                                        </td>
                                         </tr>
                                    <tr>
                                        <td style="vertical-align:middle" >National ID Expiry Date
                                       <br />
                                            <asp:TextBox ID="txtEmiratesIDExp" runat="server"  CssClass="form-control" MaxLength="11"></asp:TextBox>
                                            <asp:ImageButton ID="ImageButton6" CssClass="pos-absolute" runat="server" ImageUrl="~/Images/Calendar.png" />
                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender6" runat="server" TargetControlID="txtEmiratesIDExp"
                                                Format="dd/MMM/yyyy" PopupButtonID="ImageButton6">
                                            </ajaxToolkit:CalendarExtender>
                                             <asp:RegularExpressionValidator ID="RegularExpressionValidator6"
                                            runat="server" ControlToValidate="txtEmiratesIDExp" Display="Dynamic" EnableViewState="False"
                                            ErrorMessage="Invalid Date  format (dd/mmm/yyyy)" ForeColor="red"
                                            ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                            ValidationGroup="vgMain"></asp:RegularExpressionValidator>
                                        </td>
                                    </tr>
                                    <tr id="trPremises" runat="server" visible="false">
                                        <td style="vertical-align:middle" >Premises Id
                                       <br />
                                            <asp:TextBox ID="txtPremisesId" runat="server" CssClass="form-control"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr >
                                        <td  class="sub-heading">&nbsp;Language Details
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align:middle">First Language(Main)<span style="color: Red;">*</span>
                                        <br />
                                            <asp:DropDownList ID="ddlFLang" runat="server" CssClass="form-control">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="rfFirstLanguage" runat="server" ControlToValidate="ddlFLang"
                                                ErrorMessage="First Language" ValidationGroup="vgMain" Display="None" InitialValue="0"></asp:RequiredFieldValidator>

                                        </td>
                                         </tr>
                                    <tr>
                                        <td style="vertical-align:middle">Other Languages(Specify)
                                        <br />
                                            <asp:Panel ID="plOLang" runat="server" Height="100px" ScrollBars="Vertical" Width="220px">
                                                <asp:CheckBoxList ID="chkOLang" runat="server" CssClass="checkboxAns">
                                                </asp:CheckBoxList>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                    <tr >
                                        <td  class="sub-heading">&nbsp;Other Information
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align:middle"  >Emergency Contact Number<span style="color: Red;">*</span>
                                        <br />

                                            <asp:TextBox ID="txtEmgMobile_Country" runat="server" Width="40px" MaxLength="3" CssClass="form-control"></asp:TextBox>
                                            -
                                    <asp:TextBox ID="txtEmgMobile_Area" runat="server" Width="40px" MaxLength="4" CssClass="form-control"></asp:TextBox>
                                            -
                                    <asp:TextBox ID="txtEmgMobile_No" runat="server" Width="90px" MaxLength="10" CssClass="form-control"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvEmgNum" runat="server" ControlToValidate="txtEmgMobile_No"
                                                ErrorMessage="Emergency Contact Number" ValidationGroup="vgMain" Display="None"></asp:RequiredFieldValidator>

                                            <div class="remark">(Country-Area-Number)</div>
                                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server"
                                                TargetControlID="txtEmgMobile_Country"
                                                FilterType="Numbers">
                                            </ajaxToolkit:FilteredTextBoxExtender>
                                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server"
                                                TargetControlID="txtEmgMobile_Area"
                                                FilterType="Numbers">
                                            </ajaxToolkit:FilteredTextBoxExtender>
                                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server"
                                                TargetControlID="txtEmgMobile_No"
                                                FilterType="Numbers">
                                            </ajaxToolkit:FilteredTextBoxExtender>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align:middle">Student Personal Email
                                       <br />
                                            <asp:TextBox ID="txtStuPersonalEmail" runat="server" CssClass="form-control"></asp:TextBox>&nbsp;
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2"
                                            runat="server"
                                            ControlToValidate="txtStuPersonalEmail" Display="Dynamic"
                                            EnableViewState="False"
                                            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                            ValidationGroup="info" ErrorMessage="Invalid Email"
                                            ForeColor="red"></asp:RegularExpressionValidator>
                                        </td>
                                         </tr>
                                    <tr>
                                        <td style="vertical-align:middle">Student Mobile Number
                                        <br />

                                            <asp:TextBox ID="txtStuMobile_Country" runat="server" Width="40px" MaxLength="3" CssClass="form-control"></asp:TextBox>
                                            -
                                    <asp:TextBox ID="txtStuMobile_Area" runat="server" Width="40px" MaxLength="4" CssClass="form-control"></asp:TextBox>
                                            -
                                    <asp:TextBox ID="txtStuMobile_No" runat="server" Width="90px" MaxLength="10" CssClass="form-control"></asp:TextBox>
                                            <div class="remark">(Country-Area-Number)</div>
                                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server"
                                                TargetControlID="txtStuMobile_Country"
                                                FilterType="Numbers">
                                            </ajaxToolkit:FilteredTextBoxExtender>
                                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server"
                                                TargetControlID="txtStuMobile_Area"
                                                FilterType="Numbers">
                                            </ajaxToolkit:FilteredTextBoxExtender>
                                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server"
                                                TargetControlID="txtStuMobile_No"
                                                FilterType="Numbers">
                                            </ajaxToolkit:FilteredTextBoxExtender>


                                        </td>
                                    </tr>
                                    <tr>
                                        <td  align="center">
                                            <asp:Button ID="btnSaveContinue" runat="server" Text="SAVE & CONTINUE" CssClass="btn btn-info"
                                                OnClick="btnSaveContinue_Click" ValidationGroup="vgMain" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td  align="left">
                                            <asp:ValidationSummary ID="ValidationSummary1"
                                                DisplayMode="BulletList" EnableClientScript="true" runat="server" ValidationGroup="vgMain" HeaderText="<div class='error text-danger'>Following fields cannot be left empty.</div>"
                                                CssClass="text-danger" ForeColor="" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
