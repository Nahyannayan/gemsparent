﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Collections.Generic
Imports System.Collections
Imports InfoSoftGlobal
Partial Class Stud_Prof_ProfAttendance
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

 If Session("username") & "" = "" Then
            Response.Redirect("~/login.aspx")
        End If

            'ViewState("viewid") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
            ' bindMain_details(Session("STU_ID"))
            bindAttChart(Session("STU_ID"))
            bindABSChart(Session("STU_ID"))
            bindLATEChart(Session("STU_ID"))
            lbChildName.Text = Session("ACTIVE_STU_NAME")

        Catch ex As Exception

        End Try
    End Sub
   


    Sub bindAttChart(ByVal stu_id As String)
        Try
            Dim todayDT As String = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim param(10) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@stu_id", stu_id)
            Dim strXML As String
            Dim TMonth As String = String.Empty
            Dim TOT_ATT As String = String.Empty


            Dim arr() As String
            arr = New String() {"AFD8F8", "F6BD0F", "8BBA00", "FF8E46", "008E8E", "D64646", "8E468E", "588526", "B3AA00", "008ED6", "9D080D", "A186BE"}
            Dim i As Integer = 0



            strXML = ""
            strXML = strXML & "<graph caption='Yearly Attendance Pattern' xAxisName='Month' yAxisName='Count' decimalPrecision='0' formatNumberScale='0'>"

            Using readerStudent_Detail As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "GetStuddashBoard_Att_Patten", param)

                If readerStudent_Detail.HasRows = True Then
                    While readerStudent_Detail.Read


                        TMonth = readerStudent_Detail("TMONTH").ToString
                        TOT_ATT = readerStudent_Detail("TOT_ATT").ToString

                        strXML = strXML & "<set name=" + "'" & TMonth & "' value=" + "'" & TOT_ATT & "' color=" + "'" & arr(i) & "' />"
                        i = i + 1
                    End While
                    strXML = strXML & "</graph>"
                    FCLiteral.Text = FusionCharts.RenderChartHTML("../FusionCharts/FCF_Column3D.swf", "", strXML, "myNext", "600", "280", False)
                Else
                    'lblerror.Text = "No Records Found "

                    strXML = ""
                    strXML = strXML & "<graph caption='Yearly Attendance Pattern' xAxisName='Month' yAxisName='Count' decimalPrecision='0' formatNumberScale='0'>"
                    strXML = strXML & "<set name='Jan' value='' color='AFD8F8'/>"
                    strXML = strXML & "<set name='Feb' value='' color='F6BD0F'/>"
                    strXML = strXML & "<set name='Mar' value='' color='8BBA00'/>"
                    strXML = strXML & "<set name='Apr' value='' color='FF8E46'/>"
                    strXML = strXML & "<set name='May' value='' color='008E8E'/>"
                    strXML = strXML & "<set name='Jun' value='' color='D64646'/>"
                    strXML = strXML & "<set name='Jul' value='' color='8E468E'/>"
                    strXML = strXML & "<set name='Aug' value='' color='588526'/>"
                    strXML = strXML & "<set name='Sep' value='' color='B3AA00'/>"
                    strXML = strXML & "<set name='Oct' value='' color='008ED6'/>"
                    strXML = strXML & "<set name='Nov' value='' color='9D080D'/>"
                    strXML = strXML & "<set name='Dec' value='' color='A186BE'/>"
                    strXML = strXML & "</graph>"
                    FCLiteral.Text = FusionCharts.RenderChartHTML("../FusionCharts/FCF_Column3D.swf", "", strXML, "myNext", "600", "280", False)
                End If
            End Using



            'strXML = ""
            'strXML = strXML & "<graph caption='Yearly Attedance Pattern' xAxisName='Month' yAxisName='Count' decimalPrecision='0' formatNumberScale='0'>"
            'strXML = strXML & "<set name='Jan' value='462' color='AFD8F8'/>"
            'strXML = strXML & "<set name='Feb' value='857' color='F6BD0F'/>"
            'strXML = strXML & "<set name='Mar' value='671' color='8BBA00'/>"
            'strXML = strXML & "<set name='Apr' value='494' color='FF8E46'/>"
            'strXML = strXML & "<set name='May' value='761' color='008E8E'/>"
            'strXML = strXML & "<set name='Jun' value='960' color='D64646'/>"
            'strXML = strXML & "<set name='Jul' value='629' color='8E468E'/>"
            'strXML = strXML & "<set name='Aug' value='622' color='588526'/>"
            'strXML = strXML & "<set name='Sep' value='376' color='B3AA00'/>"
            'strXML = strXML & "<set name='Oct' value='494' color='008ED6'/>"
            'strXML = strXML & "<set name='Nov' value='761' color='9D080D'/>"
            'strXML = strXML & "<set name='Dec' value='960' color='A186BE'/>"
            'strXML = strXML & "</graph>"

            'Create the chart - Column 3D Chart with data from strXML variable using dataXML method


        Catch ex As Exception
            ' lblerror.Text = "ERROR WHILE RETREVING DATA"
        End Try
    End Sub


    Sub bindABSChart(ByVal stu_id As String)
        Try
            Dim todayDT As String = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim param(10) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@stu_id", stu_id)
            Dim strXML As String
            Dim Tweek As String = String.Empty
            Dim TOT_ABSCOUNT As String = String.Empty
            Dim i As Integer

            Dim arr() As String
            arr = New String() {"AFD8F8", "F6BD0F", "8BBA00", "FF8E46", "008E8E", "D64646", "8E468E", "588526", "B3AA00", "008ED6", "9D080D", "A186BE"}


            strXML = ""
            strXML = strXML & "<graph caption='Weekly Absent Pattern'  xAxisName='Week' yAxisName='Count' pieSliceDepth='25' decimalPrecision='0'>"

            Using readerStudent_Detail As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "GetStuddashBoard_ABS_Patten", param)

                If readerStudent_Detail.HasRows = True Then
                    While readerStudent_Detail.Read


                        Tweek = readerStudent_Detail("TWEEK").ToString
                        'If Tweek = "SUN" Then
                        '    Tweek = "Sunday"
                        'ElseIf Tweek = "MON" Then
                        '    Tweek = "Monday"
                        'ElseIf Tweek = "TUE" Then
                        '    Tweek = "Tuesday"
                        'ElseIf Tweek = "WED" Then
                        '    Tweek = "Wednesday"
                        'ElseIf Tweek = "THU" Then
                        '    Tweek = "Thursday"
                        'ElseIf Tweek = "FRI" Then
                        '    Tweek = "Friday"
                        'ElseIf Tweek = "SAT" Then
                        '    Tweek = "Saturday"
                        'End If
                        TOT_ABSCOUNT = readerStudent_Detail("TOT_ABSCOUNT").ToString
                        If TOT_ABSCOUNT = "0" Then
                            TOT_ABSCOUNT = ""
                        End If
                        strXML = strXML & "<set name=" + "'" & Tweek & "' value=" + "'" & TOT_ABSCOUNT & "' color=" + "'" & arr(i) & "' />"
                        i = i + 1
                    End While
                    strXML = strXML & "</graph>"
                    FCAbsent.Text = FusionCharts.RenderChartHTML("../FusionCharts/FCF_Column3D.swf", "", strXML, "myNext", "330", "250", False)
                Else
                    'lblerror.Text = "No Records Found "

                    strXML = ""
                    strXML = strXML & "<graph caption='Weekly Absent Pattern'  pieSliceDepth='25' decimalPrecision='0' showNames='1'>"
                    strXML = strXML & "<set name='Sun' value=''  color='AFD8F8'/>"
                    strXML = strXML & "<set name='Mon' value='' color='AFD8F8'/>"
                    strXML = strXML & "<set name='Tue' value='' color='AFD8F8' />"
                    strXML = strXML & "<set name='Wed' value='' color='AFD8F8'/>"
                    strXML = strXML & "<set name='Thu' value='' color='AFD8F8'/>"
                    strXML = strXML & "<set name='Fri' value='' color='AFD8F8'/>"
                    strXML = strXML & "<set name='Sat' value='' color='AFD8F8'/>"

                    strXML = strXML & "</graph>"
                    FCAbsent.Text = FusionCharts.RenderChartHTML("../FusionCharts/FCF_Column3D.swf", "", strXML, "myNext", "330", "250", False)
                End If
            End Using

        Catch ex As Exception
            'lblerror.Text = "ERROR WHILE RETREVING DATA"
        End Try
    End Sub

    Sub bindLATEChart(ByVal stu_id As String)
        Try
            Dim todayDT As String = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim param(10) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@stu_id", stu_id)
            Dim strXML As String
            Dim Tweek As String = String.Empty
            Dim TOT_late As String = String.Empty
            Dim i As Integer
            Dim arr() As String
            arr = New String() {"AFD8F8", "F6BD0F", "8BBA00", "FF8E46", "008E8E", "D64646", "8E468E", "588526", "B3AA00", "008ED6", "9D080D", "A186BE"}


            strXML = ""
            strXML = strXML & "<graph caption='Weekly Late Pattern'  xAxisName='Week' yAxisName='Count' pieSliceDepth='25' decimalPrecision='0'>"
            Using readerStudent_Detail As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "GetStuddashBoard_LATE_Patten", param)

                If readerStudent_Detail.HasRows = True Then
                    While readerStudent_Detail.Read


                        Tweek = readerStudent_Detail("TWEEK").ToString
                        'If Tweek = "SUN" Then
                        '    Tweek = "Sunday"
                        'ElseIf Tweek = "MON" Then
                        '    Tweek = "Monday"
                        'ElseIf Tweek = "TUE" Then
                        '    Tweek = "Tuesday"
                        'ElseIf Tweek = "WED" Then
                        '    Tweek = "Wednesday"
                        'ElseIf Tweek = "THU" Then
                        '    Tweek = "Thursday"
                        'ElseIf Tweek = "FRI" Then
                        '    Tweek = "Friday"
                        'ElseIf Tweek = "SAT" Then
                        '    Tweek = "Saturday"
                        'End If
                        TOT_late = readerStudent_Detail("TOT_LCOUNT").ToString
                        If TOT_late = "0" Then
                            TOT_late = ""
                        End If
                        strXML = strXML & "<set name=" + "'" & Tweek & "' value=" + "'" & TOT_late & "' color=" + "'" & arr(i) & "' />"

                        'strXML = strXML & "<set name=" + "'" & Tweek & "' value=" + "'" & TOT_late & "' />"
                        i = i + 1
                    End While
                    strXML = strXML & "</graph>"
                    FCLate.Text = FusionCharts.RenderChartHTML("../FusionCharts/FCF_Column3D.swf", "", strXML, "Late", "330", "250", False)
                Else
                    'lblerror.Text = "No Records Found "

                    strXML = ""
                    strXML = strXML & "<graph caption='Weekly Late Pattern'  pieSliceDepth='25' decimalPrecision='0' showNames='1'>"
                    strXML = strXML & "<set name='Sun' value=''  color='AFD8F8'/>"
                    strXML = strXML & "<set name='Mon' value='' color='AFD8F8'/>"
                    strXML = strXML & "<set name='Tue' value='' color='AFD8F8' />"
                    strXML = strXML & "<set name='Wed' value='' color='AFD8F8'/>"
                    strXML = strXML & "<set name='Thu' value='' color='AFD8F8'/>"
                    strXML = strXML & "<set name='Fri' value='' color='AFD8F8'/>"
                    strXML = strXML & "<set name='Sat' value='' color='AFD8F8'/>"

                    strXML = strXML & "</graph>"
                    FCLate.Text = FusionCharts.RenderChartHTML("../FusionCharts/FCF_Column3D.swf", "", strXML, "late", "330", "250", False)
                End If
            End Using

        Catch ex As Exception
            ' lblerror.Text = "ERROR WHILE RETREVING DATA"
        End Try
    End Sub
End Class
