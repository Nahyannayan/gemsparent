﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports InfoSoftGlobal
Partial Class studprof_Notifications
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
 	If Session("username") & "" = "" Then
            Response.Redirect("~/login.aspx")
        End If

        BindGrid(Session("STU_ID"))
        BindGridEmail(Session("STU_ID"))
        BindNotifications(Session("STU_ID"))
        If (Page.IsPostBack) Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "MyFun1", "KeepStyle()", True)
        End If

    End Sub
    'Protected Sub Page_Render(sender As Object, e As EventArgs) Handles Me.PreRender


    'End Sub

    Protected Sub BindNotifications(ByVal stu_id As String)
        Dim ds As DataSet
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim PARAM(5) As SqlParameter
        PARAM(0) = New SqlParameter("@STU_ID", stu_id)
        PARAM(1) = New SqlParameter("@ACD_ID", Session("STU_ACD_ID"))
        PARAM(2) = New SqlParameter("@BSU_ID", Session("STU_BSU_ID"))
        PARAM(3) = New SqlParameter("@GRADE", Session("STU_GRD_ID"))
        PARAM(4) = New SqlParameter("@SCT_ID", Session("STU_SCT_ID"))
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "OPL.GETNOTIFICATION_DETAILS", PARAM)
        If ds.Tables(0).Rows.Count = 0 Then
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
            ds.Tables(0).Rows(0)("NOTIFICATION_CONTENT") = ""
            ds.Tables(0).Rows(0)("NEW") = False

            GridNotifications.DataSource = ds
            GridNotifications.DataBind()

            Dim columnCount As Integer = GridNotifications.Rows(0).Cells.Count

            GridNotifications.Rows(0).Cells.Clear()
            GridNotifications.Rows(0).Cells.Add(New TableCell)
            GridNotifications.Rows(0).Cells(0).ColumnSpan = columnCount
            GridNotifications.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            GridNotifications.Rows(0).Cells(0).Text = "No Notification To Display."
        Else
            GridNotifications.DataSource = ds
            GridNotifications.DataBind()
        End If
    End Sub

    Public Sub BindGrid(ByVal stu_id As String)
        Try

            Dim ds As DataSet
            Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim PARAM(2) As SqlParameter
            PARAM(0) = New SqlParameter("@STU_ID", stu_id)
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "OPL.GETSMS_DETAILS", PARAM)

            If ds.Tables(0).Rows.Count = 0 Then
                ViewState("SMS_row") = "FALSE"

                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                ds.Tables(0).Rows(0)("LOG_CMS_ID") = 0
                ds.Tables(0).Rows(0)("CMS_SMS_TEXT") = ""
                ds.Tables(0).Rows(0)("LOG_MOBILE_NUMBER") = ""
                ds.Tables(0).Rows(0)("LOG_ENTRY_DATE") = ""
                ds.Tables(0).Rows(0)("bShow") = False
                ds.Tables(0).Rows(0)("tempview") = ""
                ds.Tables(0).Rows(0)("status") = ""
                ds.Tables(0).Rows(0)("NEW") = False
                GridMSMS.DataSource = ds
                GridMSMS.DataBind()

                Dim columnCount As Integer = GridMSMS.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                GridMSMS.Rows(0).Cells.Clear()
                GridMSMS.Rows(0).Cells.Add(New TableCell)
                GridMSMS.Rows(0).Cells(0).ColumnSpan = columnCount
                GridMSMS.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                GridMSMS.Rows(0).Cells(0).Text = "No SMS sent."
            Else
                ViewState("SMS_row") = "TRUE"
                GridMSMS.DataSource = ds
                GridMSMS.DataBind()
            End If
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub GridMSMS_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        GridMSMS.PageIndex = e.NewPageIndex
        BindGrid(Session("STU_ID"))
    End Sub


    Protected Sub GridEmails_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridEmails.PageIndexChanging
        GridEmails.PageIndex = e.NewPageIndex
        BindGridEmail(Session("STU_ID"))
    End Sub
    Public Sub BindGridEmail(ByVal stu_id As String)
        Try


            Dim ds As DataSet
            Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim PARAM(2) As SqlParameter
            PARAM(0) = New SqlParameter("@STU_ID", stu_id)
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "OPL.GETEMAIL_DETAILS", PARAM)
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())

                ds.Tables(0).Rows(0)("LOG_EML_ID") = 0
                ds.Tables(0).Rows(0)("EML_TITLE") = ""
                ds.Tables(0).Rows(0)("LOG_EMAIL_ID") = ""
                ds.Tables(0).Rows(0)("LOG_ENTRY_DATE") = ""
                ds.Tables(0).Rows(0)("bShow") = False
                ds.Tables(0).Rows(0)("tempview") = ""
                ds.Tables(0).Rows(0)("status") = ""
                ds.Tables(0).Rows(0)("EmailType") = ""
                ds.Tables(0).Rows(0)("NEW") = False

                GridEmails.DataSource = ds
                GridEmails.DataBind()

                Dim columnCount As Integer = GridEmails.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                GridEmails.Rows(0).Cells.Clear()
                GridEmails.Rows(0).Cells.Add(New TableCell)
                GridEmails.Rows(0).Cells(0).ColumnSpan = columnCount
                GridEmails.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                GridEmails.Rows(0).Cells(0).Text = "No Emails sent."
            Else
                GridEmails.DataSource = ds
                GridEmails.DataBind()
            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub lblNotiDetail_Click(sender As Object, e As EventArgs)

        Dim myrow As Object
        myrow = sender.parent
        Dim hidViewId As HiddenField = CType(myrow.FindControl("hidViewId"), HiddenField)
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim strans As SqlTransaction = Nothing
        Dim PARAM(4) As SqlParameter
        Try
            Dim objConn As New SqlConnection(str_conn)
            objConn.Open()
            strans = objConn.BeginTransaction
            PARAM(0) = New SqlParameter("@STU_ID", Session("STU_ID"))
            PARAM(1) = New SqlParameter("@VIEWID", hidViewId.Value)
            PARAM(2) = New SqlParameter("@GRADE", Session("STU_GRD_ID"))
            PARAM(3) = New SqlParameter("@SECTION", Session("STU_SCT_ID"))
            SqlHelper.ExecuteNonQuery(strans, CommandType.StoredProcedure, "[OPL].[INSERT_NOTIFICATION_DETAILS_TOPARENT]", PARAM)
            strans.Commit()

        Catch ex As Exception
            strans.Rollback()
        End Try
        'Response.Redirect(Request.RawUrl, False)
    End Sub

End Class
