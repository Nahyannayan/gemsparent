<%@ Page Language="VB" MasterPageFile="~/ParentMaster.master" AutoEventWireup="false"
    CodeFile="StudHealthDetails.aspx.vb" Inherits="StudProf_StudHealthDetails" Title="Untitled Page" %>

<%@ Register Src="~/StudProf/UserControl/studContactDetailsMenu.ascx" TagName="contactDetails"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphParent" runat="Server">
   <%-- <script src="../Scripts/jquery-1.9.1.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.8.2.min.js" type="text/javascript"></script>--%>
    <script type="text/javascript">
        function functionValidate() {

            var result = $('#<%= chkAccept.ClientID%>').is(':checked');

        if (result) {
            return true;

        }
        else {

            alert('Please tick to confirm that the informations are true.');
            $('#<%= chkAccept.ClientID%>').css('color', 'red');
            $('#<%= chkAccept.ClientID%>').css('style', 'solid');
            $('#<%= chkAccept.ClientID%>').css('width', 'thin');
            $('#<%= chkAccept.ClientID%>').focus();

            return false;

        }

    }
    </script>
    
    <div class="content margin-top30 margin-bottom60">
        <div class="container">
            <div class="row">

                <!-- Posts Block -->

                <div class="posts-block col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="my-account">

                        <div class="title-box">
                                <h3>Update Student Details 
                                    <span class="profile-right">
                                <asp:Label ID="lbChildNameTop" runat="server"></asp:Label>
                            </span></h3>
                            </div>

                        <div style="display: none;">
                            <div class="left">
                                <font color="black">Name :</font>
                                <asp:Label ID="lblChildName" CssClass="lblChildNameCss" runat="server"></asp:Label>
                            </div>
                            <div class="right">
                                <font color="black">Fee ID :</font>
                                <asp:Label ID="lblFeeID" runat="server" CssClass="lblChildNameCss">
                                </asp:Label>
                            </div>
                            <br />
                            <br />
                            <div class="left">
                                <font color="black">Grade & Section :</font>
                                <asp:Label ID="lblGradeSection" CssClass="lblChildNameCss" runat="server"></asp:Label>
                            </div>

                            <div class="right">
                                <font color="black">DOJ :</font>
                                <asp:Label ID="lblDateOfJoin" runat="server" CssClass="lblChildNameCss">
                                </asp:Label>
                            </div>
                        </div>
                        <asp:Label ID="lblmsg" runat="server"></asp:Label>
                        <uc1:contactDetails ID="contactDetails1" runat="server" />
                        <div>
                            <div align="left">
                                <table style="width: 100%;" align="left" border="0" cellspacing="0" 
                                    class="table table-striped table-bordered table-responsive text-left my-orders-table">
                                    <tr class="trSub_Header">
                                        <td colspan="6" class="sub-heading">Health Details
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="6">
                                            <table width="100%" cellpadding="8" cellspacing="0">
                                                <tr>
                                                    <td colspan="6">
                                                        <asp:CheckBox ID="chkAccept" runat="server" /><span>I hereby certify that the information mentioned below are true and correct to the best of my knowledge.</span>

                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="" colspan="3">
                                                        <div class="tdPadDiv">
                                                            Health Card No / Medical Insurance No
                                                        </div>
                                                        <asp:TextBox ID="txtHealthCard" runat="server" CssClass="form-control"></asp:TextBox>
                                                    </td>
                                                    <td class="" colspan="3">
                                                        <div class="tdPadDiv">
                                                            Blood Group
                                                        </div>
                                                        <asp:DropDownList ID="ddlBgroup" runat="server" CssClass="form-control">
                                                            <asp:ListItem>--</asp:ListItem>
                                                            <asp:ListItem Value="AB+">AB+</asp:ListItem>
                                                            <asp:ListItem Value="AB-">AB-</asp:ListItem>
                                                            <asp:ListItem Value="B+">B+</asp:ListItem>
                                                            <asp:ListItem Value="A+">A+</asp:ListItem>
                                                            <asp:ListItem Value="B-">B-</asp:ListItem>
                                                            <asp:ListItem Value="A-">A-</asp:ListItem>
                                                            <asp:ListItem Value="O+">O+</asp:ListItem>
                                                            <asp:ListItem Value="O-">O-</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr >
                                        <td colspan="6" class="sub-heading">Health Restriction
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" colspan="6">
                                            <div class="tdPadDiv">
                                                Does your child have any allergies?
                            <asp:RadioButton ID="rdbAllergyyes" runat="server" GroupName="rdAllergyGroup" OnCheckedChanged="rdbAllergyYes_CheckedChanged" AutoPostBack="true" Text="Yes" />
                                                <asp:RadioButton ID="rdbAllergyNo" runat="server" GroupName="rdAllergyGroup" OnCheckedChanged="rdbAllergyNo_CheckedChanged" AutoPostBack="true" Text="No" />

                                            </div>
                                            <div class="tdPadDiv">
                                                <div class="remark_info" style="padding-top: 4px; padding-bottom: 4px;">
                                                    If you answered YES, please add details below 
                                                </div>
                                                <asp:TextBox ID="txtAllergies" runat="server" TabIndex="22" TextMode="MultiLine"
                                                    Rows="4" CssClass="multipleText_full" MaxLength="255">
                                                </asp:TextBox>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" colspan="6">
                                            <div class="tdPadDiv">
                                                Does your child have any Prescribed Special Medication?
                             <asp:RadioButton ID="rdbPrescMedYes" runat="server" GroupName="rdbPrescMedGroup" OnCheckedChanged="rdbPrescMedYes_CheckedChanged" AutoPostBack="true" Text="Yes" />
                                                <asp:RadioButton ID="rdbPrescMedNo" runat="server" GroupName="rdbPrescMedGroup" OnCheckedChanged="rdbPrescMedNo_CheckedChanged" AutoPostBack="true" Text="No" />


                                            </div>
                                            <div class="tdPadDiv">
                                                <div class="remark_info" style="padding-top: 4px; padding-bottom: 4px;">
                                                   If you answered YES, please add details below 
                                                </div>
                                                <asp:TextBox ID="txtPrescMedication" runat="server" TabIndex="22" TextMode="MultiLine"
                                                    Rows="4" CssClass="multipleText_full" MaxLength="255">
                                                </asp:TextBox>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" colspan="6">
                                            <div class="tdPadDiv">
                                                Is there any Physical Education Restrictions for your child?                         
                            
                              <asp:RadioButton ID="rdbPhysicalEduYes" runat="server" GroupName="rdbPhysicalEduGroup" OnCheckedChanged="rdbPhysicalEduYes_CheckedChanged" AutoPostBack="true" Text="Yes" />
                                                <asp:RadioButton ID="rdbPhysicalEduNo" runat="server" GroupName="rdbPhysicalEduGroup" OnCheckedChanged="rdbPhysicalEduNo_CheckedChanged" AutoPostBack="true" Text="No" />

                                            </div>
                                            <div class="tdPadDiv">
                                                <div class="remark_info" style="padding-top: 4px; padding-bottom: 4px;">
                                                    If you answered YES, please add details below 
                                                </div>
                                                <asp:TextBox ID="txtPhysicalEdu" runat="server" TabIndex="22" TextMode="MultiLine"
                                                    Rows="4" CssClass="multipleText_full" MaxLength="255">
                                                </asp:TextBox>
                                            </div>
                                        </td>
                                    </tr>
                                    
                                    <tr>
                                        <td align="left" colspan="6">
                                            <div class="tdPadDiv">
                                                Does your child have any visual disability?                      
                            
                             <asp:RadioButton ID="rdbVisualDisabilityYes" runat="server" GroupName="rdbVisualDisabilityGroup" OnCheckedChanged="rdbVisualDisabilityYes_CheckedChanged" AutoPostBack="true" Text="Yes" />
                                                <asp:RadioButton ID="rdbVisualDisabilityNo" runat="server" GroupName="rdbVisualDisabilityGroup" OnCheckedChanged="rdbVisualDisabilityNo_CheckedChanged" AutoPostBack="true" Text="No" />

                                            </div>
                                            <div class="tdPadDiv">
                                                <div class="remark_info" style="padding-top: 4px; padding-bottom: 4px;">
                                                   If you answered YES, please add details below 
                                                </div>
                                                <asp:TextBox ID="txtVisualDisability" runat="server" TabIndex="22" TextMode="MultiLine"
                                                    Rows="4" CssClass="multipleText_full" MaxLength="255">
                                                </asp:TextBox>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" colspan="6">
                                            <div class="tdPadDiv">
                                                Does your child have any special educational needs?                       
                             <asp:RadioButton ID="rdbEduNeedsYes" runat="server" GroupName="rdbEduNeedsGroup" OnCheckedChanged="rdbEduNeedsYes_CheckedChanged" AutoPostBack="true" Text="Yes" />
                                                <asp:RadioButton ID="rdbEduNeedsNo" runat="server" GroupName="rdbEduNeedsGroup" OnCheckedChanged="rdbEduNeedsNo_CheckedChanged" AutoPostBack="true" Text="No" />

                                            </div>
                                            <div class="tdPadDiv">
                                                <div class="remark_info" style="padding-top: 4px; padding-bottom: 4px;">
                                                    If you answered YES, please add details below 
                                                </div>
                                                <asp:TextBox ID="txtEduNeeds" runat="server" TabIndex="22" TextMode="MultiLine" Rows="4"
                                                    CssClass="multipleText_full" MaxLength="255">
                                                </asp:TextBox>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" colspan="6">
                                            <div class="tdPadDiv">
                                                Any other information related to health issue of your child the school should be
                            aware of?                       
                             
                              <asp:RadioButton ID="rdbSchoolawareYes" runat="server" GroupName="rdbSchoolawareEduGroup" OnCheckedChanged="rdbSchoolawareYes_CheckedChanged" AutoPostBack="true" Text="Yes" Visible="false" />
                                                <asp:RadioButton ID="rdbSchoolawareNo" runat="server" GroupName="rdbSchoolawareGroup" OnCheckedChanged="rdbSchoolawareNo_CheckedChanged" AutoPostBack="true" Text="No" Visible="false" />

                                            </div>
                                            <div class="tdPadDiv">
                                                <div class="remark_info" style="padding-top: 4px; padding-bottom: 4px; display:none;">
                                                    If you answered YES, please add details below 
                                                </div>
                                                <asp:TextBox ID="txtSchoolaware" runat="server" TabIndex="22" TextMode="MultiLine"
                                                    Rows="4" CssClass="multipleText_full" MaxLength="255">
                                                </asp:TextBox>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr >
                                        <td colspan="6" class="sub-heading">Has your child had any of the following?
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="6">
                                            <table style="width: 100% !important;" cellpadding="8" cellspacing="0" >
                                                <tr>
                                                    <td>
                                                        <asp:GridView ID="gvInfectious" runat="server" AutoGenerateColumns="False" CssClass="table table-striped table-bordered table-responsive text-left my-orders-table"
                                                            Height="100%" Width="100%" EnableModelValidation="True">
                                                            <RowStyle CssClass="griditem" />
                                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="NO" Visible="false">
                                                                    <ItemTemplate>
                                                                        <asp:Label runat="server" ID="lblSlNo"><%# Container.DataItemIndex + 1 %></asp:Label>
                                                                        <asp:Label ID="lblDisID" runat="server" Text='<%# Bind("DIS_ID") %>' Visible="false"></asp:Label>
                                                                    </ItemTemplate>
                                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="10px"></ItemStyle>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="INFECTIOUS DISEASE">

                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblINFDIS" runat="server" Text='<%# Bind("DIS_DESC") %>' ></asp:Label>
                                                                    </ItemTemplate>

                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="YES">
                                                                    <ItemTemplate>
                                                                        <asp:RadioButton ID="rdbInfYes" runat="server" Checked='<%# Bind("IsYChecked") %>'
                                                                            GroupName="rdbYesInfG" />
                                                                    </ItemTemplate>
                                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="10px"></ItemStyle>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="No">
                                                                    <ItemTemplate>
                                                                        <asp:RadioButton ID="rdbInfNo" runat="server" Checked='<%# Bind("IsNChecked") %>'
                                                                            GroupName="rdbYesInfG" />
                                                                    </ItemTemplate>
                                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="10px"></ItemStyle>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>

                                                        <asp:GridView ID="Gvnoninfectious" runat="server" AutoGenerateColumns="False" CssClass="table table-striped table-bordered table-responsive text-left my-orders-table"
                                                            Height="100%" Width="100%" EnableModelValidation="True" BorderWidth="1px">
                                                            <RowStyle CssClass="griditem"/>
                                                            <AlternatingRowStyle CssClass="griditem_alternative" />
                                                            <Columns>
                                                                <asp:TemplateField visible="false">
                                                                    <ItemTemplate>

                                                                        <asp:Label ID="lblNonDiscID" runat="server" Text='<%# Bind("DIS_ID") %>' Visible="false"></asp:Label>
                                                                    </ItemTemplate>

                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="NON-INFECTIOUS DISEASE">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblNONINFDIS" runat="server" Text='<%# Bind("DIS_DESC") %>' ></asp:Label>
                                                                        <asp:Label ID="lblNonDisID" runat="server" Text='<%# Bind("DIS_ID") %>' Visible="false"></asp:Label>
                                                                    </ItemTemplate>

                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="YES">
                                                                    <ItemTemplate>
                                                                        <asp:RadioButton ID="rdbNonInfYes" runat="server" Checked='<%# Bind("IsYChecked") %>'
                                                                            GroupName="rdbNoInfG" />
                                                                    </ItemTemplate>
                                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="10px"></ItemStyle>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="No">
                                                                    <ItemTemplate>
                                                                        <asp:RadioButton ID="rdbNonInfNo" runat="server" Checked='<%# Bind("IsNChecked") %>'
                                                                            GroupName="rdbNoInfG" />
                                                                    </ItemTemplate>
                                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="10px"></ItemStyle>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="6">If you answered YES to any questions above, please add details below.
                        <br />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="6">
                                            <asp:TextBox ID="txtDiseaseNotes" runat="server" TextMode="MultiLine" CssClass="multipleText_full form-control"
                                                Rows="4" Columns="34"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="6">Family History :<asp:CheckBox ID="chkDiabetes" runat="server" Text="Diabetes"  />
                                            <asp:CheckBox ID="chkHyperTension" runat="server" Text="Hypertension" />
                                            <asp:CheckBox ID="chkStroke" runat="server" Text="Stroke"  />
                                             <asp:CheckBox ID="chkTubor" Text="Tuberculosis" runat="server"  />
                                            
                        <asp:CheckBox ID="chkOther" Text="Other" runat="server"  />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="6">if other specify
                        <br />
                                            <asp:TextBox ID="txtFamilyHistoryOther" runat="server" TextMode="MultiLine" Rows="4"
                                                CssClass="multipleText_full form-control" MaxLength="255"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="6" align="center">
                                            <asp:Button ID="btnPrev" runat="server" Text="PREVIOUS" CssClass="btn btn-info"  OnClick="btnPrev_Click" />
                        <asp:Button ID="btnSaveContinue" runat="server" Text="SAVE & CONTINUE" CssClass="btn btn-info" 
                             OnClientClick="javascript:return functionValidate();" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
