<%@ Page Language="VB" MasterPageFile="~/ParentMaster.master" AutoEventWireup="false"
    CodeFile="ParentDetails.aspx.vb" Inherits="StudProf_ParentDetails" Title="Untitled Page" %>

<%@ Register Src="~/StudProf/UserControl/studContactDetailsMenu.ascx" TagName="contactDetails"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphParent" runat="Server">
    <style type="text/css">
        .buttonx {
            border-style: solid;
            border-color: White;
            border-width: 1pt;
            background-color: #1B80B6;
            background-image: url('../images/bg-menu-main.png');
            font-weight: bold;
            color: #FFFFFF;
            font-family: tahoma,sans-serif;
            font-size: 11px;
            height: 24px;
            text-decoration: none;
        }

            .buttonx:hover {
                background: grey;
                background-image: url('../images/bg-menu-mainred.png');
                border-style: solid;
                border-color: white;
                border-width: 1pt;
                font-weight: bold;
                color: #FFFFFF;
                font-family: tahoma,sans-serif;
                font-size: 11px;
                height: 24px;
                text-decoration: none;
                cursor: hand;
            }

        .buttonxSelected {
            border-style: solid;
            border-color: White;
            border-width: 1pt;
            background-color: Gray;
            background-image: url('../images/bg-menu-main.png');
            font-weight: bold;
            color: #FFFFFF;
            font-family: tahoma,sans-serif;
            font-size: 11px;
            height: 24px;
            text-decoration: none;
        }
    </style>

    <script type="text/javascript">
        function copyFathertoMother(chkThis) {
            var chk_state = chkThis.checked;
            if (chk_state == true) {

                document.getElementById('<%=txtMResAddressStreet.ClientID %>').value = document.getElementById('<%=txtFResAddressStreet.ClientID %>').value;
                document.getElementById('<%=txtMResAddArea.ClientID %>').value = document.getElementById('<%=txtFResAddArea.ClientID %>').value;
                document.getElementById('<%=txtMResAddBuilding.ClientID %>').value = document.getElementById('<%=txtFResAddBuilding.ClientID %>').value;
                document.getElementById('<%=txtMResApartment.ClientID %>').value = document.getElementById('<%=txtFResApartment.ClientID %>').value;

                document.getElementById('<%=txtMPOBOX.ClientID %>').value = document.getElementById('<%=txtFPOBOX.ClientID %>').value;
                document.getElementById('<%=ddlMCOMEmirate.ClientID %>').selectedIndex = document.getElementById('<%=ddlFCOMEmirate.ClientID %>').selectedIndex;
               
                document.getElementById('<%=ddlMCOMCountry.ClientID %>').selectedIndex = document.getElementById('<%=ddlFCOMCountry.ClientID %>').selectedIndex;
                document.getElementById('<%=ddlMCOMCountry.ClientID%>').onchange();
                document.getElementById('<%=ddlMCity.ClientID%>').selectedIndex = document.getElementById('<%=ddlFCity.ClientID%>').selectedIndex;
                document.getElementById('<%=ddlMCity.ClientID %>').onchange();
                document.getElementById('<%=ddlMArea.ClientID %>').selectedIndex = document.getElementById('<%=ddlFArea.ClientID %>').selectedIndex;

                document.getElementById('<%=txtMMobile_Country.ClientID %>').value = document.getElementById('<%=txtFMobile_Country.ClientID %>').value;
                document.getElementById('<%=txtMMobile_Area.ClientID %>').value = document.getElementById('<%=txtFMobile_Area.ClientID %>').value;
                document.getElementById('<%=txtMMobile_No.ClientID %>').value = document.getElementById('<%=txtFMobile_No.ClientID %>').value;
                document.getElementById('<%=txtMemail.ClientID %>').value = document.getElementById('<%=txtFemail.ClientID %>').value;
           <%--     document.getElementById('<%=txtMOccupation.ClientID %>').value = document.getElementById('<%=txtFOccupation.ClientID %>').value;
                document.getElementById('<%=ddlMCompany_Name.ClientID %>').selectedIndex = document.getElementById('<%=ddlFCompany_Name.ClientID %>').selectedIndex;--%>
            }
            else {
                document.getElementById('<%=txtMResAddressStreet.ClientID %>').value = ""
                document.getElementById('<%=txtMResAddressStreet.ClientID %>').value = ""
                document.getElementById('<%=txtMResAddBuilding.ClientID %>').value = ""
                document.getElementById('<%=txtMResApartment.ClientID %>').value = ""

                document.getElementById('<%=txtMPOBOX.ClientID %>').value = ""
                document.getElementById('<%=ddlMCOMEmirate.ClientID %>').selectedIndex = 0

                document.getElementById('<%=ddlMCOMCountry.ClientID %>').selectedIndex=0
                document.getElementById('<%=ddlMArea.ClientID %>').selectedIndex=0
                document.getElementById('<%=ddlMCity.ClientID%>').selectedIndex = 0

                document.getElementById('<%=txtMMobile_Country.ClientID %>').value = ""
                document.getElementById('<%=txtMMobile_Area.ClientID %>').value = ""
                document.getElementById('<%=txtMMobile_No.ClientID %>').value = ""
                document.getElementById('<%=txtMemail.ClientID %>').value = ""
                 document.getElementById('<%=ddlMArea.ClientID %>').selectedIndex=0
<%--            document.getElementById('<%=txtMOccupation.ClientID %>').value = ""
                document.getElementById('<%=ddlMCompany_Name.ClientID %>').selectedIndex = 0--%>

            }
            return true;
        }

    </script>

    <script>
        if ($(window).width() < 979) {
            if ($(location).attr("href").indexOf("ParentDetails_M.aspx") == -1) {
                window.location = "\\StudProf\\ParentDetails_M.aspx";
            }
        }
        if ($(window).width() > 979) {
            if ($(location).attr("href").indexOf("ParentDetails.aspx") == -1) {
                window.location = "\\StudProf\\ParentDetails.aspx";
            }
        }
    </script>

    <div class="content margin-top30 margin-bottom60">
        <div class="container">
            <div class="row">

                <!-- Posts Block -->

                <div class="posts-block col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="my-account">

                        <div class="title-box">
                                <h3>Update Student Details 
                                    <span class="profile-right">
                                <asp:Label ID="lbChildNameTop" runat="server"></asp:Label>
                            </span></h3>
                        </div>

                        <div style="display: none;">
                            <div class="left">
                                <font color="black">Name :</font>
                                <asp:Label ID="lblChildName" CssClass="lblChildNameCss" runat="server"></asp:Label>
                            </div>
                            <div class="right">
                                <font color="black">Fee ID :</font>
                                <asp:Label ID="lblFeeID" runat="server" CssClass="lblChildNameCss">
                                </asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </div>
                            <br />
                            <br />
                            <div class="left">
                                <font color="black">Grade & Section :</font>
                                <asp:Label ID="lblGradeSection" CssClass="lblChildNameCss" runat="server"></asp:Label>
                            </div>
                            <div class="right">
                                <font color="black">DOJ :</font>
                                <asp:Label ID="lblDateOfJoin" runat="server" CssClass="lblChildNameCss">
                                </asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </div>
                        </div>
                        
                        <uc1:contactDetails ID="contactDetails1" runat="server" />
                        <div align="left">
                            <table style="width: 100%;" align="left" border="0"
                                cellspacing="0" class="table table-striped table-bordered table-responsive text-left my-orders-table">
                                <tr>
                                    <td colspan="4" align="left">
                                        <asp:Label ID="lblmsg" runat="server"></asp:Label>
                                        <asp:ValidationSummary ID="ValidationSummary1" HeaderText="<div class='validationheader'>Following fields cannot be left empty.</div>"
                                            DisplayMode="BulletList" CssClass="text-danger" EnableClientScript="true" runat="server" ValidationGroup="vgContinue" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="6" class="sub-heading">&nbsp;Fee Sponsor
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" style="vertical-align: middle">Fee Sponsor<font color="red">*</font></td>

                                    <td class="matters" align="left" colspan="3" style="vertical-align: middle">
                                        <asp:DropDownList ID="ddFeeSponsor" runat="server" onchange="if(this.selectedIndex == 0)return false;" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddFeeSponsor_SelectedIndex">
                                            <asp:ListItem Value="0">-Select-</asp:ListItem>
                                            <asp:ListItem Value="1">Father</asp:ListItem>
                                            <asp:ListItem Value="2">Mother</asp:ListItem>
                                            <asp:ListItem Value="3">Guardian</asp:ListItem>
                                            <asp:ListItem Value="4">Fathers Company</asp:ListItem>
                                            <asp:ListItem Value="5">Mothers Company</asp:ListItem>
                                            <asp:ListItem Value="6">Guardians Company</asp:ListItem>
                                        </asp:DropDownList>

                                        <asp:RequiredFieldValidator ID="rfvFeeSposor" runat="server" InitialValue="0" ErrorMessage="Fee Sponsor" ValidationGroup="vgContinue"
                                            Display="None" ControlToValidate="ddFeeSponsor"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr id="trFeeCompany" runat="server">
                                    <td align="left" style="vertical-align: middle">Company</td>
                                    <td class="matters" align="left" colspan="3" style="vertical-align: middle">
                                        <asp:DropDownList ID="ddlFeeCompany" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlFeeCompany_SelectedIndex" CssClass="form-control"></asp:DropDownList>
                                        <br />
                                        <br />
                                        <div class="remark">
                                            (If you choose other,please specify the Company Name)
                                        </div>
                                        <asp:TextBox ID="txtFeeCompanyName" runat="server" Visible="false" CssClass="form-control"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvFeeCompany" runat="server" ErrorMessage="Fee sponsor Company Name" ValidationGroup="vgContinue"
                                            Display="None" ControlToValidate="txtFeeCompanyName" Enabled="false" Visible="false"  ></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="6" class="sub-heading">&nbsp;Parent Details
                                    </td>
                                </tr>

                                <tr>
                                    <td style="vertical-align: middle">Primary Contact
                                    </td>
                                    <td style="vertical-align: middle">
                                        <asp:Literal ID="ltPrimaryCont" runat="server"></asp:Literal>
                                    </td>
                                    <td style="vertical-align: middle">Preferred Contact
                                    </td>
                                    <td style="vertical-align: middle">
                                        <asp:DropDownList ID="ddlPrefContact" runat="server" CssClass="form-control">
                                            <asp:ListItem>Home Phone</asp:ListItem>
                                            <asp:ListItem>Office Phone</asp:ListItem>
                                            <asp:ListItem>Mobile</asp:ListItem>
                                            <asp:ListItem>Email</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="6" class="sub-heading">&nbsp;Parent Details (Father)
                                    </td>
                                </tr>
                                <tr>
                                    <td style="vertical-align: middle">Father's Name
                                    </td>
                                    <td colspan="3" style="vertical-align: middle">
                                        <asp:TextBox ID="ltFathersName" runat="server" CssClass="form-control" style="width:100%;"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfFatherName" runat="server" ControlToValidate="ltFathersName"
                                            ErrorMessage="Father's Name" ValidationGroup="vgContinue" Enabled="false" Display="None"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                 <tr>
                                    <td style="vertical-align: middle">Residential Address Country<span style="color: Red;" id="Span1" runat="server" visible="false">*</span>
                                    </td>
                                    <td style="vertical-align: middle">
                                        <asp:DropDownList ID="ddlFCOMCountry" runat="server" CssClass="form-control" AutoPostBack="true">
                                    </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlFCOMCountry"
                                            ErrorMessage="*" ValidationGroup="vgContinue" Enabled="false"
                                            Display="None"></asp:RequiredFieldValidator>
                                    </td>
                                    <td style="vertical-align: middle">Residential Address City<span style="color: Red;" id="Span2" runat="server" visible="false">*</span>
                                    </td>
                                    <td style="vertical-align: middle">
                                        <asp:DropDownList ID="ddlFCity" runat="server" CssClass="form-control" AutoPostBack="true"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlFCity"
                                            ErrorMessage="*" ValidationGroup="vgContinue" Enabled="false"
                                            Display="None"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="vertical-align: middle">Residential Address Area<span style="color: Red;" id="spF2" runat="server" visible="false">*</span>
                                    </td>
                                    <td style="vertical-align: middle">
                                        <asp:DropDownList ID="ddlFArea" runat="server" CssClass="form-control">
                                            </asp:DropDownList>
                                            
                                           <asp:RequiredFieldValidator ID="rfArea" runat="server" ControlToValidate="ddlFArea" Enabled="false"
                                         ErrorMessage="Father's residential address area required" ForeColor="red" ValidationGroup="vgContinue">*</asp:RequiredFieldValidator>
                                         
                                            <div class="remark_info" style="padding-top: 4px; padding-bottom: 4px;">
                                                If you choose other,please specify the Area Name</div>
                                        <asp:TextBox ID="txtFResAddArea" runat="server" CssClass="form-control"></asp:TextBox>
                                       
                                    </td>
                                    <td style="vertical-align: middle">Residential Address Street<span style="color: Red;" id="spF1" runat="server" visible="false">*</span>
                                    </td>
                                    <td style="vertical-align: middle">
                                        <asp:TextBox ID="txtFResAddressStreet" runat="server" CssClass="form-control"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvRes" runat="server" ControlToValidate="txtFResAddressStreet"
                                            ErrorMessage="Residential Address Street(F)" ValidationGroup="vgContinue" Enabled="false"
                                            Display="None"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="vertical-align: middle">Residential Address Building<span style="color: Red;" id="spF3" runat="server" visible="false">*</span>
                                    </td>
                                    <td style="vertical-align: middle">
                                        <asp:TextBox ID="txtFResAddBuilding" runat="server" CssClass="form-control"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfBuilding" runat="server" ControlToValidate="txtFResAddBuilding"
                                            ErrorMessage="Residential Address Building(F)" ValidationGroup="vgContinue" Enabled="false"
                                            Display="None"></asp:RequiredFieldValidator>
                                    </td>
                                    <td style="vertical-align: middle">Residential Address Apartment No<span style="color: Red;" id="spF4" runat="server"
                                        visible="false">*</span>
                                    </td>
                                    <td style="vertical-align: middle">
                                        <asp:TextBox ID="txtFResApartment" runat="server" CssClass="form-control"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfApartment" runat="server" ControlToValidate="txtFResApartment"
                                            ErrorMessage="Residential Address Apartment No(F)" ValidationGroup="vgContinue"
                                            Display="None" Enabled="false"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="vertical-align: middle">City/Emirate<span style="color: Red;" id="spF5" runat="server" visible="false">*</span>
                                    </td>
                                    <td style="vertical-align: middle">
                                        <asp:DropDownList ID="ddlFCOMEmirate" runat="server" CssClass="form-control">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="rfEmirate" runat="server" ControlToValidate="ddlFCOMEmirate"
                                            ErrorMessage="City/Emirate(F)" InitialValue="0" Enabled="false" ValidationGroup="vgContinue"
                                            Display="None"></asp:RequiredFieldValidator>
                                    </td>
                                    <td style="vertical-align: middle">P.O Box<span style="color: Red;" id="spF6" runat="server" visible="false">*</span>
                                    </td>
                                    <td style="vertical-align: middle">
                                        <asp:TextBox ID="txtFPOBOX" runat="server" CssClass="form-control"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfPobox" runat="server" ControlToValidate="txtFPOBOX"
                                            ErrorMessage="P.O Box (F)" ValidationGroup="vgContinue" Enabled="false" Display="None"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="vertical-align: middle">Father's Mobile<span style="color: Red;" id="spF7" runat="server" visible="false">*</span>
                                    </td>
                                    <td style="vertical-align: middle">
                                        <asp:TextBox ID="txtFMobile_Country" runat="server" Width="60px" MaxLength="3" CssClass="form-control"></asp:TextBox>
                                        -
                        <asp:TextBox ID="txtFMobile_Area" runat="server" Width="60px" MaxLength="4" CssClass="form-control"></asp:TextBox>
                                        -
                        <asp:TextBox ID="txtFMobile_No" runat="server" Width="90px" MaxLength="10" CssClass="form-control"></asp:TextBox>
                                        <div class="remark">
                                            (Country-Area-Number)
                                        </div>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server"
                                            TargetControlID="txtFMobile_Country" FilterType="Numbers">
                                        </ajaxToolkit:FilteredTextBoxExtender>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server"
                                            TargetControlID="txtFMobile_Area" FilterType="Numbers">
                                        </ajaxToolkit:FilteredTextBoxExtender>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server"
                                            TargetControlID="txtFMobile_No" FilterType="Numbers">
                                        </ajaxToolkit:FilteredTextBoxExtender>
                                        <asp:RequiredFieldValidator ID="rfMobF" runat="server" ControlToValidate="txtFMobile_No"
                                            Display="none" ErrorMessage="Father's mobile no required" Enabled="false" ValidationGroup="groupM1">*</asp:RequiredFieldValidator>
                                    </td>
                                    <td style="vertical-align: middle">Father's Email Address<span style="color: Red;" id="spF8" runat="server" visible="false">*</span>
                                    </td>
                                    <td style="vertical-align: middle">
                                        <asp:TextBox ID="txtFemail" runat="server" CssClass="form-control"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfFemail" runat="server" ControlToValidate="txtFemail"
                                            ErrorMessage="Father's Email Address " ValidationGroup="vgContinue" Enabled="false"
                                            Display="None"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="reg_fEmail"
                                            runat="server"
                                            ControlToValidate="txtFemail" Display="Dynamic"
                                            EnableViewState="False"
                                            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                            ValidationGroup="info" ErrorMessage="Invalid Email"
                                            ForeColor="red"></asp:RegularExpressionValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="vertical-align: middle">Father's Occupation<span style="color: Red;" id="spF9" runat="server" visible="false">*</span>
                                    </td>
                                    <td colspan="3" style="vertical-align: middle">
                                        <asp:TextBox ID="txtFOccupation" runat="server" CssClass="form-control"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfOcc" runat="server" ControlToValidate="txtFOccupation"
                                            ErrorMessage="Father's Occupation" ValidationGroup="vgContinue" Enabled="false"
                                            Display="None"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="vertical-align: middle">Father's Company<span style="color: Red;" id="spF10" runat="server" visible="false">*</span>
                                    </td>
                                    <td colspan="3" style="vertical-align: middle">
                                        <asp:DropDownList ID="ddlFCompany_Name" runat="server" CssClass="form-control">
                                        </asp:DropDownList>
                                        <br />
                                        <asp:RequiredFieldValidator ID="rfCompanyF" runat="server" ControlToValidate="ddlFCompany_Name"
                                            ErrorMessage="Father's Company" ValidationGroup="vgContinue" Enabled="false"
                                            Display="None"></asp:RequiredFieldValidator><br />
                                        <div class="remark">
                                            (If you choose other,please specify the Company Name)
                                        </div>
                                        <asp:TextBox ID="txtFComp_Name" runat="server" CssClass="form-control"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="6" class="sub-heading">&nbsp;Parent Details (Mother)<span style="float: right;">
                                        <asp:CheckBox ID="chkCopyF_Details" runat="server" CssClass="checkboxHeader" Text="Copy Father Details"
                                            onclick="javascript:return copyFathertoMother(this);" /></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="vertical-align: middle">Mother's Name <span style="color: Red;" id="spM1" runat="server" visible="false">*</span>
                                    </td>
                                    <td colspan="3">
                                        <asp:TextBox ID="txtMname" runat="server" CssClass="form-control" style="width:100%;"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfNameMother" runat="server" ControlToValidate="txtMname"
                                            ErrorMessage="Mother's Name" ValidationGroup="vgContinue" Enabled="false" Display="None"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                 <tr>
                                    <td style="vertical-align: middle">Residential Address Country<span style="color: Red;" id="Span3" runat="server" visible="false">*</span>
                                    </td>
                                    <td style="vertical-align: middle">
                                        <asp:DropDownList ID="ddlMCOMCountry" runat="server" CssClass="form-control" AutoPostBack="true">
                                    </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddlMCOMCountry"
                                            ErrorMessage="*" ValidationGroup="vgContinue" Enabled="false"
                                            Display="None"></asp:RequiredFieldValidator>
                                    </td>
                                    <td style="vertical-align: middle">Residential Address City<span style="color: Red;" id="Span4" runat="server" visible="false">*</span>
                                    </td>
                                    <td style="vertical-align: middle">
                                        <asp:DropDownList ID="ddlMCity" runat="server" CssClass="form-control" AutoPostBack="true"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="ddlMCity"
                                            ErrorMessage="*" ValidationGroup="vgContinue" Enabled="false"
                                            Display="None"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="vertical-align: middle">Residential Address Area <span style="color: Red;" id="spM3" runat="server" visible="false">*</span>
                                    </td>
                                    <td style="vertical-align: middle">
                                        <asp:DropDownList ID="ddlMArea" runat="server" CssClass="form-control">
                                            </asp:DropDownList>
                                            
                                           <asp:RequiredFieldValidator ID="rfAreaM" runat="server" ControlToValidate="ddlMArea"
                                         ErrorMessage="Mother's residential address area required" ForeColor="red" ValidationGroup="vgContinue" Enabled="false">*</asp:RequiredFieldValidator>
                                         
                                            <div class="remark_info" style="padding-top: 4px; padding-bottom: 4px;">
                                                If you choose other,please specify the Area Name</div>
                                        <asp:TextBox ID="txtMResAddArea" runat="server" CssClass="form-control"></asp:TextBox>
                                       
                                    </td>
                                    <td style="vertical-align: middle">Residential Address Street <span style="color: Red;" id="spM2" runat="server" visible="false">*</span>
                                    </td>
                                    <td style="vertical-align: middle">
                                        <asp:TextBox ID="txtMResAddressStreet" runat="server" CssClass="form-control"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfMStreet" runat="server" ControlToValidate="txtMResAddressStreet"
                                            ErrorMessage="Residential Address Street(Mother)" ValidationGroup="vgContinue"
                                            Enabled="false" Display="None"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="vertical-align: middle">Residential Address Building<span style="color: Red;" id="spM4" runat="server" visible="false">*</span>
                                    </td>
                                    <td style="vertical-align: middle">
                                        <asp:TextBox ID="txtMResAddBuilding" runat="server" CssClass="form-control"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfBuildM" runat="server" ControlToValidate="txtMResAddBuilding"
                                            ErrorMessage="Residential Address Building(Mother)" ValidationGroup="vgContinue"
                                            Enabled="false" Display="None"></asp:RequiredFieldValidator>
                                    </td>
                                    <td style="vertical-align: middle">Residential Address Apartment No <span style="color: Red;" id="spM5" runat="server"
                                        visible="false">*</span>
                                    </td>
                                    <td style="vertical-align: middle">
                                        <asp:TextBox ID="txtMResApartment" runat="server" CssClass="form-control"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfApartM" runat="server" ControlToValidate="txtMResApartment"
                                            ErrorMessage="Residential Address Apartment No(Mother)" ValidationGroup="vgContinue"
                                            Enabled="false" Display="None"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="vertical-align: middle">City/Emirate <span style="color: Red;" id="spM6" runat="server" visible="false">*</span>
                                    </td>
                                    <td style="vertical-align: middle">
                                        <asp:DropDownList ID="ddlMCOMEmirate" runat="server" CssClass="form-control">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="rfEmirateM" InitialValue="0" runat="server" ControlToValidate="ddlMCOMEmirate"
                                            ErrorMessage="Emirate(Mother)" ValidationGroup="vgContinue" Enabled="false" Display="None"></asp:RequiredFieldValidator>
                                    </td>
                                    <td style="vertical-align: middle">P.O Box <span style="color: Red;" id="spM7" runat="server" visible="false">*</span>
                                    </td>
                                    <td style="vertical-align: middle">
                                        <asp:TextBox ID="txtMPOBOX" runat="server" CssClass="form-control"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfPOM" runat="server" ControlToValidate="txtMPOBOX"
                                            ErrorMessage="P.O Box" ValidationGroup="vgContinue" Enabled="false" Display="None"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="vertical-align: middle">Mother's Mobile<span style="color: Red;" id="spM10" runat="server" visible="false">*</span>
                                    </td>
                                    <td style="vertical-align: middle">
                                        <asp:TextBox ID="txtMMobile_Country" runat="server" Width="60px" MaxLength="3" CssClass="form-control"></asp:TextBox>
                                        -
                        <asp:TextBox ID="txtMMobile_Area" runat="server" Width="60px" MaxLength="4" CssClass="form-control"></asp:TextBox>
                                        -
                        <asp:TextBox ID="txtMMobile_No" runat="server" Width="90px" MaxLength="10" CssClass="form-control"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvMMobNo" runat="server" ControlToValidate="txtMMobile_No"
                                            Display="Dynamic" ErrorMessage="Mother's mobile no required" ValidationGroup="groupM1">*</asp:RequiredFieldValidator>
                                        <div class="remark">
                                            (Country-Area-Number)
                                        </div>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender22" runat="server"
                                            TargetControlID="txtMMobile_Country" FilterType="Numbers">
                                        </ajaxToolkit:FilteredTextBoxExtender>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender23" runat="server"
                                            TargetControlID="txtMMobile_Area" FilterType="Numbers">
                                        </ajaxToolkit:FilteredTextBoxExtender>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender24" runat="server"
                                            TargetControlID="txtMMobile_No" FilterType="Numbers">
                                        </ajaxToolkit:FilteredTextBoxExtender>
                                    </td>
                                    <td style="vertical-align: middle">Mother's Email Address <span style="color: Red;" id="spM8" runat="server" visible="false">*</span>
                                    </td>
                                    <td style="vertical-align: middle">
                                        <asp:TextBox ID="txtMemail" runat="server" CssClass="form-control"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfEmailM" runat="server" ControlToValidate="txtMemail"
                                            ErrorMessage="Mother's Email" ValidationGroup="vgContinue" Enabled="false" Display="None"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1"
                                            runat="server"
                                            ControlToValidate="txtMemail" Display="Dynamic"
                                            EnableViewState="False"
                                            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                            ValidationGroup="info" ErrorMessage="Invalid Email"
                                            ForeColor="red"></asp:RegularExpressionValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="vertical-align: middle">Mother's Occupation <span style="color: Red;" id="spM9" runat="server" visible="false">*</span>
                                    </td>
                                    <td colspan="3" style="vertical-align: middle">
                                        <asp:TextBox ID="txtMOccupation" runat="server" CssClass="form-control"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfOccM" runat="server" ControlToValidate="txtMOccupation"
                                            ErrorMessage="Mother's Occupation" ValidationGroup="vgContinue" Enabled="false"
                                            Display="None"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="vertical-align: middle">Mother's Company
                                    </td>
                                    <td colspan="3" style="vertical-align: middle">
                                        <asp:DropDownList ID="ddlMCompany_Name" runat="server" CssClass="form-control">
                                        </asp:DropDownList>
                                        <br />
                                        <br />
                                        <div class="remark" style="display: block;">
                                            (If you choose other,please specify the Company Name)
                                        </div>
                                        <asp:TextBox ID="txtMCompany" runat="server" CssClass="form-control"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="6" class="sub-heading">&nbsp;Parent Details (Guardian)
                                    </td>
                                </tr>
                                <tr>
                                    <td style="vertical-align: middle">Guardian's Name <span style="color: Red;" id="spG1" runat="server" visible="false">*</span>
                                    </td>
                                    <td colspan="3" style="vertical-align: middle">
                                        <asp:TextBox ID="txtGuardian" runat="server" CssClass="form-control"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfGNAme" runat="server" ControlToValidate="txtGuardian"
                                            ErrorMessage="Guardians Name" ValidationGroup="vgContinue" Enabled="false" Display="None"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="vertical-align: middle">Residential Address Country<span style="color: Red;" id="Span5" runat="server" visible="false">*</span>
                                    </td>
                                    <td style="vertical-align: middle">
                                        <asp:DropDownList ID="ddlGCOMCountry" runat="server" CssClass="form-control" AutoPostBack="true">
                                    </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="ddlGCOMCountry"
                                            ErrorMessage="*" ValidationGroup="vgContinue" Enabled="false"
                                            Display="None"></asp:RequiredFieldValidator>
                                    </td>
                                    <td style="vertical-align: middle">Residential Address City<span style="color: Red;" id="Span6" runat="server" visible="false">*</span>
                                    </td>
                                    <td style="vertical-align: middle">
                                        <asp:DropDownList ID="ddlGCity" runat="server" CssClass="form-control" AutoPostBack="true"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="ddlGCity"
                                            ErrorMessage="*" ValidationGroup="vgContinue" Enabled="false"
                                            Display="None"></asp:RequiredFieldValidator>
                                    </td>
                                            </tr>
                                <tr>
                                    <td style="vertical-align: middle">Residential Address Area<span style="color: Red;" id="spG3" runat="server" visible="false">*</span>
                                    </td>
                                    <td style="vertical-align: middle">
                                        <asp:DropDownList ID="ddlGArea" runat="server" CssClass="form-control">
                                            </asp:DropDownList>
                                            
                                           <asp:RequiredFieldValidator ID="rfResAreaG" runat="server" ControlToValidate="ddlGArea"
                                         ErrorMessage="Guardian's residential address area required" ForeColor="red" ValidationGroup="vgContinue" Enabled="false">*</asp:RequiredFieldValidator>
                                         
                                            <div class="remark_info" style="padding-top: 4px; padding-bottom: 4px;">
                                                If you choose other,please specify the Area Name</div>
                                        <asp:TextBox ID="txtGResAddArea" runat="server" CssClass="form-control"></asp:TextBox>
                                      
                                    </td>
                                    <td style="vertical-align: middle">Residential Address Street<span style="color: Red;" id="spG2" runat="server" visible="false">*</span>
                                    </td>
                                    <td style="vertical-align: middle">
                                        <asp:TextBox ID="txtGResAddressStreet" runat="server" CssClass="form-control"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfGStreet" runat="server" ControlToValidate="txtGResAddressStreet"
                                            ErrorMessage="Residential Address Street " ValidationGroup="vgContinue" Enabled="false"
                                            Display="None"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="vertical-align: middle">Residential Address Building<span style="color: Red;" id="spG4" runat="server" visible="false">*</span>
                                    </td>
                                    <td style="vertical-align: middle">
                                        <asp:TextBox ID="txtGResAddBuilding" runat="server" CssClass="form-control"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfBuilG" runat="server" ControlToValidate="txtGResAddBuilding"
                                            ErrorMessage="Residential Address Building " ValidationGroup="vgContinue" Enabled="false"
                                            Display="None"></asp:RequiredFieldValidator>
                                    </td>
                                    <td style="vertical-align: middle">Residential Address Apartment No<span style="color: Red;" id="spG5" runat="server"
                                        visible="false">*</span>
                                    </td>
                                    <td style="vertical-align: middle">
                                        <asp:TextBox ID="txtGResApartment" runat="server" CssClass="form-control"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfGApart" runat="server" ControlToValidate="txtGResApartment"
                                            ErrorMessage="Residential Address Apartment No " ValidationGroup="vgContinue"
                                            Enabled="false" Display="None"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="vertical-align: middle">City/Emirate<span style="color: Red;" id="spG6" runat="server" visible="false">*</span>
                                    </td>
                                    <td style="vertical-align: middle">
                                        <asp:DropDownList ID="ddlGComEmirate" runat="server" CssClass="form-control">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="rfEmirateG" runat="server" ControlToValidate="ddlGComEmirate"
                                            ErrorMessage="Emirate " ValidationGroup="vgContinue" InitialValue="0" Enabled="false"
                                            Display="None"></asp:RequiredFieldValidator>
                                    </td>
                                    <td style="vertical-align: middle">P.O Box<span style="color: Red;" id="spG7" runat="server" visible="false">*</span>
                                    </td>
                                    <td style="vertical-align: middle">
                                        <asp:TextBox ID="txtGPOBOX" runat="server" CssClass="form-control"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfPOG" runat="server" ControlToValidate="txtGPOBOX"
                                            ErrorMessage="P.O Box (G)" ValidationGroup="vgContinue" Enabled="false" Display="None"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="vertical-align: middle">Guardian's Mobile<span style="color: Red;" id="spG10" runat="server" visible="false">*</span>
                                    </td>
                                    <td style="vertical-align: middle">
                                        <asp:TextBox ID="txtGMobile_Country" runat="server" Width="60px" MaxLength="3" CssClass="form-control"></asp:TextBox>
                                        -
                        <asp:TextBox ID="txtGMobile_Area" runat="server" Width="60px" MaxLength="4" CssClass="form-control"></asp:TextBox>
                                        -
                        <asp:TextBox ID="txtGMobile_No" runat="server" Width="90px" MaxLength="10" CssClass="form-control">
                        </asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvGMobNo" runat="server" ControlToValidate="txtGMobile_No"
                                            ErrorMessage="Guardian's mobile no required" ValidationGroup="groupM1" Enabled="false"
                                            Display="None">*</asp:RequiredFieldValidator>
                                        <div class="remark">
                                            (Country-Area-Number)
                                        </div>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender37" runat="server"
                                            TargetControlID="txtGMobile_Country" FilterType="Numbers">
                                        </ajaxToolkit:FilteredTextBoxExtender>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender38" runat="server"
                                            TargetControlID="txtGMobile_Area" FilterType="Numbers">
                                        </ajaxToolkit:FilteredTextBoxExtender>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender39" runat="server"
                                            TargetControlID="txtGMobile_No" FilterType="Numbers">
                                        </ajaxToolkit:FilteredTextBoxExtender>
                                    </td>
                                    <td style="vertical-align: middle">Guardian's Email Address<span style="color: Red;" id="spG8" runat="server" visible="false">*</span>
                                    </td>
                                    <td style="vertical-align: middle">
                                        <asp:TextBox ID="txtGemail" runat="server" CssClass="form-control"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfGEmail" runat="server" ControlToValidate="txtGemail"
                                            ErrorMessage="Guardian's Email Address" ValidationGroup="vgContinue" Enabled="false"
                                            Display="None"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2"
                                            runat="server"
                                            ControlToValidate="txtGemail" Display="Dynamic"
                                            EnableViewState="False"
                                            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                            ValidationGroup="info" ErrorMessage="Invalid Email"
                                            ForeColor="red"></asp:RegularExpressionValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="vertical-align: middle">Guardian's Occupation<span style="color: Red;" id="spG9" runat="server" visible="false">*</span>
                                    </td>
                                    <td colspan="3" style="vertical-align: middle">
                                        <asp:TextBox ID="txtGOccupation" runat="server" CssClass="form-control"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfGOccup" runat="server" ControlToValidate="txtGOccupation"
                                            ErrorMessage="Guardian's Occupation" ValidationGroup="vgContinue" Enabled="false"
                                            Display="None"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="vertical-align: middle">Guardian's Company 
                                    </td>
                                    <td colspan="3" style="vertical-align: middle">
                                        <asp:DropDownList ID="ddlGCompany" runat="server" CssClass="form-control">
                                        </asp:DropDownList>
                                        <br />
                                        <asp:RequiredFieldValidator ID="rfGComp" runat="server" ControlToValidate="ddlGCompany"
                                            ErrorMessage="Guardian's Company" ValidationGroup="vgContinue" InitialValue="0"
                                            Enabled="false" Display="None"></asp:RequiredFieldValidator>
                                        <br />
                                        <div class="remark">
                                            (If you choose other,please specify the Company Name)
                                        </div>
                                        <asp:TextBox ID="txtGCompany" runat="server" CssClass="form-control"></asp:TextBox>
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="4" align="center">
                                        <asp:Button ID="btnPrev" runat="server" Text="PREVIOUS" CssClass="btn btn-info" OnClick="btnPrev_Click" />&nbsp;
                        <asp:Button ID="btnSaveContinue" runat="server" Text="SAVE & CONTINUE" CssClass="btn btn-info"
                            OnClick="btnSaveContinue_Click" ValidationGroup="vgContinue" />
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="4" align="left">
                                        <asp:ValidationSummary ID="ValidationSummary2" HeaderText="<div class='validationheader'>Following fields cannot be left empty.</div>"
                                            DisplayMode="BulletList"  CssClass="text-danger" EnableClientScript="true" runat="server" ValidationGroup="vgContinue" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
