﻿<%@ Page Language="VB" MasterPageFile="~/ParentMaster.master" AutoEventWireup="false"
    CodeFile="StudMedication.aspx.vb" Inherits="StudProf_StudMedication" Title="Untitled Page" %>
<%@ Register src="~/StudProf/UserControl/studContactDetailsMenu.ascx" tagname="contactDetails" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphParent" runat="Server">
  
    <div class="content margin-top30 margin-bottom60">
        <div class="container">
            <div class="row">

                <!-- Posts Block -->

                <div class="posts-block col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="my-account">

                        <div class="title-box">
                                <h3>Update Student Details 
                                    <span class="profile-right">
                                <asp:Label ID="lbChildNameTop" runat="server"></asp:Label>
                            </span></h3>
                        </div>

    <div style="display:none;">
        <div class="left">
        <font color="black">      Name :</font>
          <asp:Label ID="lblChildName" CssClass="lblChildNameCss" runat="server"></asp:Label></div>
        <div class="right">
          <font color="black">  Fee ID :</font> <asp:Label ID="lblFeeID" runat="server" CssClass="lblChildNameCss">
            </asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
        <br />
        <br />
        <div class="left">
         <font color="black">    Grade & Section :</font>
            <asp:Label ID="lblGradeSection" CssClass="lblChildNameCss" runat="server"></asp:Label></div>
       
        <div class="right">
       <font color="black">     DOJ :</font> <asp:Label ID="lblDateOfJoin" runat="server" CssClass="lblChildNameCss">
            </asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
    </div>
 <asp:Label ID="lblmsg" runat="server"></asp:Label>
 <uc1:contactDetails ID="contactDetails1" runat="server" />
        <div align="left">
           <table  style="width: 100%;" align="left" border="0" cellspacing="0" 
               class="table table-striped table-bordered table-responsive text-left my-orders-table">
              
                <tr >
                    <td colspan="6" class="sub-heading">
                        &nbsp;Consent for medication
                    </td>
                </tr>
                <tr>
                    <td colspan="6" >
                     <div style="margin-top:4px;margin-bottom:4px;"> If your child is unable to take certain medication, please contact the school nurse
                            to discuss the use of an alternate medication. </div>
                        <div style="margin-top:4px;margin-bottom:4px;">
                                The school has the permission to give my child over - the - counter medicines should
                                it be considered necessary by the school nurse. These medicines will only be administered
                                following careful deliberation.
                                
                                  <asp:RadioButton ID="rdbConsentMedicationyes" runat="server" GroupName="rdbConsentMedicationGroup"  Text="Yes" />
                            <asp:RadioButton ID="rdbConsentMedicationNo" runat="server" GroupName="rdbConsentMedicationGroup"  Text="No"/>
                          
                           </div>
                    </td>
                </tr>
                <tr >
                    <td colspan="6" class="sub-heading">
                        &nbsp;Emergency Treatment
                    </td>
                </tr>
                <tr>
                    <td colspan="6" >
                        <div style="margin-top:4px;margin-bottom:4px;"> 
                        <p>
                           The school nurse will attempt to contact you should an emergency arise. In the events
                            parents cannot be contacted, I authorize and empower the School nurse or School Administrator to make any and all decisions concerning
                            the medical and/surgical care of my child, which may include taking the child to a doctor or hospital for emergency treatment.</p>
                              <asp:RadioButton ID="rdbEmergenyTreatmentYes" runat="server" GroupName="rdbEmergenyTreatmentGroup"  Text="Yes" />
                            <asp:RadioButton ID="rdbEmergenyTreatmentNo" runat="server" GroupName="rdbEmergenyTreatmentGroup"  Text="No"/>
                          
                            
                      </div>
                    </td>
                </tr>
                <tr >
                    <td colspan="6" class="sub-heading">
                        School Medicals
                    </td>
                </tr>
                <tr>
                    <td colspan="6" >
                        <div style="margin-top:4px;margin-bottom:4px;"> 
                            The UAE Department of Health and Medical Services requires that all students in
                            both private and Public school entering Pre-K, Kindergarten and Grades 1,5,9 and
                            12 as well as all new students must have a clear Medical Health examination filed
                            in the <%=Session("BSU_NAME") %> Medical Centre. This examination may undertaken by the doctor of your
                            choice. School medicals will be conducted throughout the year by our licensed school
                            doctor.</div>
                       <div style="margin-top:4px;margin-bottom:4px;"> 
                         I consent to my child having a school medical, conducted by the school doctor, if
                            in the above specified grades or upon school entry.
                            
                               <asp:RadioButton ID="rdbIsSchoolMedYes" runat="server" GroupName="rdbIsSchoolMedGroup"  Text="Yes" />
                            <asp:RadioButton ID="rdbIsSchoolMedNo" runat="server" GroupName="rdbIsSchoolMedGroup"  Text="No"/>
                          
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="6" >
                       <div style="margin-top:4px;margin-bottom:4px;"> 
                             I will arrange my own private medical appointment by a registered physician and will
                            submit the required report to <%=Session("BSU_NAME") %> Medical Centre.
                              <asp:RadioButton ID="rdbArrangePrivateYes" runat="server" GroupName="rdbArrangePrivateGroup"  Text="Yes" />
                            <asp:RadioButton ID="rdbArrangePrivateNo" runat="server" GroupName="rdbArrangePrivateGroup"  Text="No"/>

                           
                        </div>
                    </td>
                </tr>
                <tr  style="display:none">
                    <td colspan="6" class="sub-heading">
                        CONSENT FOR IMMUNIZATION
                    </td>
                </tr>
                <tr style="display:none">
                    <td colspan="6" style="width: 776px">
                    <p style="font-weight:normal;font-size:small;">
                        Do you wish your child to be vaccinated in the school? Please check the box for
                        your answer.
                        <br />
                        <asp:RadioButtonList ID="rdoImmunizationConsent" runat="server" RepeatDirection="Vertical">
                            <asp:ListItem Value="1" Text="I give the consent for the immunization of my child." Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0" Text="I don't agree for immunization of my child in the school."></asp:ListItem>
                        </asp:RadioButtonList></p>
                    </td>
                </tr>
                <tr style="display:none">
                    <td colspan="6" >
                        Click on the link to update <a href="#">immunization table.</a>
                    </td>
                </tr>
                
                    <tr>
                <td>
                
                </td>
                </tr>
               <tr>
                    <td colspan="6" align="center">
                    <asp:Button ID="btnPrev" runat="server" Text="PREVIOUS" CssClass="btn btn-info"  OnClick="btnPrev_Click" /> &nbsp;
                        <asp:Button ID="btnSaveContinue" runat="server" Text="SAVE & CONTINUE" CssClass="btn btn-info" 
                            OnClick="btnSaveContinue_Click" />
                    </td>
                </tr>
            </table>
        
        </div>
   </div>
        </div>
                </div>
            </div>
     </div>
</asp:Content>
