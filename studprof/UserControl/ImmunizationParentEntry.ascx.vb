﻿Imports System.Data
Imports Microsoft.ApplicationBlocks.Data

Imports System.Data.SqlClient
Imports System.Web.Configuration

Imports System.IO

Partial Class Medical_Usercontrols_ImmunizationParentEntry
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            checknovax()
            ''mdlPopup.Show()
            CheckValid()
            BindStuVaccinationGrind()
            Check_VAX()

        End If

        If Session("immundone") = True Then
            Panel2.Visible = False
            Panel1.Visible = True
            BindStuVaccinationGrind()
        End If


    End Sub

     
    Public Sub CheckValid()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringMedical").ConnectionString()
        Dim query = "select * from VACCINE_CONSENT_TRAN WHERE VCO_STU_ID='" & Session("STU_ID") & "'" '  AND VCO_PNT_APPROVED=1"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, query)

        If ds.Tables(0).Rows.Count > 0 Then

            Panel2.Visible = False
            Panel1.Visible = True
            BindStuVaccinationGrind()
            Session("immundone") = True

        End If



    End Sub

    Public Sub BindStuVaccinationGrind()

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringMedical").ConnectionString()
        Dim pParms(2) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@STU_ID", Session("STU_ID"))
        pParms(1) = New SqlClient.SqlParameter("@OPTION", 1)

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "STUDENT_IMMUNE_PARENT_ENTRY_TRAN", pParms)

        GridInfo.DataSource = ds
        GridInfo.DataBind()

        ds.Tables(0).Columns(0).ToString()

        Dim cell = 0
        Dim vaccinid = ""
        Dim dossid = ""
        GridInfo.HeaderRow.Cells(0).Visible = False
        GridInfo.HeaderRow.Cells(2).Visible = False

        For Each row As GridViewRow In GridInfo.Rows

            vaccinid = row.Cells(0).Text.ToString()
            row.Cells(0).Visible = False
            row.Cells(2).Visible = False

            For cell = 3 To row.Cells.Count - 1


                dossid = row.Cells(cell).Text.ToString()
                GridInfo.HeaderRow.Cells(cell).Text = "<b> <center>" & HeaderText(dossid, 1) & "</center></b>"


                row.Cells(cell).Text = ""
                row.Cells(cell).Width = 100
                If CheckDosage(vaccinid, dossid) Then
                    Dim jscript = "javascript:calljv('" & vaccinid & "','" & dossid & "');return false;"
                    row.Cells(cell).Attributes.Add("OnClick", jscript)
                    row.Cells(cell).BackColor = System.Drawing.Color.FromName("#FEFFB3")
                    row.Cells(cell).Width = 100

                    Dim jscriptHelp = "javascript:callhelp('" & vaccinid & "','" & dossid & "');return false;"
                    row.Cells(1).Attributes.Add("OnClick", jscriptHelp)


                Else
                    row.Cells(cell).Text = "--"
                    row.Cells(cell).Width = 100
                End If


                Dim jscript2 = "javascript:changecolor('" & row.Cells(cell).ClientID & "');return false;"
                row.Cells(cell).Attributes.Add("onmouseover", jscript2)

                '' Display Date - Exists Records
                Dim dquery = "SELECT TOP 1 VST_DATE FROM VACCINE_STUDENTS_M A INNER JOIN dbo.VACCINE_DOSAGES B ON A.VST_VDO_ID=B.VDO_ID WHERE VDO_VCN_ID='" & vaccinid & "' AND VDO_DOS_ID='" & dossid & "' AND VST_STU_ID='" & Session("STU_ID") & "' "
                Dim ddate As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, dquery)
                If ddate.Tables(0).Rows.Count > 0 Then
                    row.Cells(cell).Text = Convert.ToDateTime(ddate.Tables(0).Rows(0).Item("VST_DATE")).ToString("dd/MMM/yyyy")
                    row.Cells(cell).Width = 100
                End If

            Next
            'Dim imagebutton As New Image
            'imagebutton.ImageUrl = "~\Images\Common\PageBody\qm.png"
            'imagebutton.ToolTip = row.Cells(2).Text.ToString()
            'row.Cells(1).Controls.Add(imagebutton)
            row.Cells(1).Width = 600

            Dim a As String = "<table border='0'><tr   style='border:none' ><td   style='border:none'>" + row.Cells(1).Text.ToString() + "</td><td style='border:none'><img src='..\..\Images\Common\PageBody\qm.png'></td></tr></table>"
            Dim lb As New Label
            lb.Text = a 'row.Cells(1).Text.ToString() & "<img src='..\..\Images\Common\PageBody\qm.png'>"
            row.Cells(1).Controls.Add(lb)






        Next



        'Create new Row in Gridview 
        Dim hrow As New GridViewRow(-1, -1, DataControlRowType.DataRow, DataControlRowState.Insert)


        ds.Tables(0).Columns.Remove("Vaccination")
        ds.Tables(0).Columns.Remove("VCN_DESC")

        Dim j = 0
        For j = 0 To ds.Tables(0).Columns.Count - 1

            'create Cell and it will be added to row 
            Dim hcell As New TableCell()
            If j <> 0 Then
                hcell.Text = "<b> <center>" & HeaderText(ds.Tables(0).Rows(0).Item(j), 2) & "</center></b>"
            Else
                hcell.Text = ""
            End If

            hrow.Cells.Add(hcell)
            hrow.Cells(j).Width = 100
        Next

        'add the row to Gridview and AddAT(Index of where it will be appear
        GridInfo.Controls(0).Controls.AddAt(1, hrow)


        j = 0

        For j = 0 To GridInfo.HeaderRow.Cells.Count - 1
            Combine(j, GridInfo.HeaderRow.Cells.Count - 1, GridInfo.HeaderRow.Cells(j).Text.ToString(), GridInfo.HeaderRow)
        Next



    End Sub


    Public Function HeaderText(ByVal dosid As Integer, ByVal soption As Integer) As String

        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringMedical").ConnectionString()

        Dim rval = ""
        Dim query = ""


        If soption = 1 Then
            query = "SELECT DOS_CAT FROM DOSE_MASTER WHERE DOS_ID='" & dosid & "' "
        ElseIf soption = 2 Then
            query = "SELECT DOS_GRD_HEADER FROM DOSE_MASTER WHERE DOS_ID='" & dosid & "' "
        End If

        rval = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, query)


        Return rval

    End Function


    Public Sub Combine(ByVal scellid As Integer, ByVal ecellid As Integer, ByVal starttext As String, ByVal row As GridViewRow)

        If starttext <> "-" Then

            Dim i = 0
            Dim span = 1
            For i = scellid + 1 To ecellid

                If row.Cells(i).Text = starttext Then
                    span += 1
                    row.Cells(i).Visible = False
                End If

            Next

            row.Cells(scellid).ColumnSpan = span

        End If

    End Sub




    Public Function CheckDosage(ByVal vcn_id As String, ByVal dos_id As String) As Boolean

        Dim returnval = True
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringMedical").ConnectionString()
        Dim query = "  SELECT TOP 1 VDO_ID,VDO_DOS_ID,VDO_COMBINED_VDO_ID FROM dbo.VACCINE_DOSAGES A INNER JOIN dbo.SCHEDULE_BSU B ON A.VDO_SCH_ID=B.SCB_SCH_ID WHERE A.VDO_VCN_ID='" & vcn_id & "' AND A.VDO_DOS_ID='" & dos_id & "' AND B.SCB_BSU_ID='" & Session("sBsuid") & "'"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, query)

        If ds.Tables(0).Rows.Count > 0 Then

            returnval = True

        Else
            returnval = False

        End If

        Return returnval

    End Function


    Function Cvalidate() As Boolean
        Dim Val = True
        lblmessage.Text = ""
        If Radiooption.SelectedValue = "" Then
            Val = False
            lblmessage.Text = "Please select an Option."
            Radiooption.Focus()
        End If
        If Checkc2.Checked = False And Radiooption.SelectedValue = "1" Then
            Val = False
            lblmessage.Text &= "<br>Please agree to terms and conditions"
            Checkc2.Focus()
        End If


        Return Val
    End Function


    Protected Sub btnsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsave.Click


        If Cvalidate() Then

            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringMedical").ConnectionString()
            Dim pParms(4) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@VCO_STU_ID", Session("STU_ID"))
            pParms(1) = New SqlClient.SqlParameter("@VCO_PNT_APPROVED", Radiooption.SelectedValue)
            pParms(2) = New SqlClient.SqlParameter("@OPTION", 1)
            lblmessage.Text = SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "VACCINE_CONSENT_TRAN_TRAN", pParms)

            CheckValid()

        End If


    End Sub

    Protected Sub Imageclose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Imageclose.Click
        mdlPopup.Hide()
    End Sub

    Protected Sub Radiooption_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Radiooption.SelectedIndexChanged
        lblmessage.Text = ""
        If Radiooption.SelectedValue = "0" Then
            div1.Visible = False
        Else
            div1.Visible = True

        End If
    End Sub

    Protected Sub chbNoVax_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chbNoVax.CheckedChanged
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringMedical").ConnectionString()
        Dim pParms(4) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@VCO_STU_ID", Session("STU_ID"))
        pParms(1) = New SqlClient.SqlParameter("@VCO_PNT_APPROVED", chbNoVax.Checked)
        pParms(2) = New SqlClient.SqlParameter("@OPTION", 2)
        lblMessage0.Text = SqlHelper.ExecuteScalar(str_conn, CommandType.StoredProcedure, "VACCINE_CONSENT_TRAN_TRAN", pParms)
        MO1.Show()
    End Sub


    Public Sub checknovax()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringMedical").ConnectionString()
        Dim query = "select isNULL(VCO_VAX_CARD_MISSING,'') as VCO_VAX_CARD_MISSING from VACCINE_CONSENT_TRAN where VCO_STU_ID='" & Session("STU_ID") & "'"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, query)
        If ds.Tables(0).Rows.Count > 0 Then
            If ds.Tables(0).Rows(0).Item("VCO_VAX_CARD_MISSING") = "1" Then
                chbNoVax.Enabled = False
            Else
                chbNoVax.Enabled = True
            End If
        End If

    End Sub


    Protected Sub btnmok_Click(sender As Object, e As System.EventArgs) Handles btnmok.Click
        checknovax()
        sendmail()
        MO1.Hide()
    End Sub

    Public Sub sendmail()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString

        Dim str_query = "select * from BSU_COMMUNICATION_M where BSC_BSU_ID='" & Session("sBsuid") & "' AND BSC_TYPE='COM'"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        If ds.Tables(0).Rows.Count > 0 Then
            Dim efrom = ds.Tables(0).Rows(0).Item("BSC_FROMEMAIL").ToString()
            Dim host = ds.Tables(0).Rows(0).Item("BSC_HOST").ToString()
            Dim port = ds.Tables(0).Rows(0).Item("BSC_PORT").ToString()
            Dim username = ds.Tables(0).Rows(0).Item("BSC_USERNAME").ToString()
            Dim Password = ds.Tables(0).Rows(0).Item("BSC_PASSWORD").ToString()

            str_query = "select email from VV_STUDENT_DETAILS where stu_id='" & Session("STU_ID") & "'"
            Dim toemailid = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)

            str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringMedical").ConnectionString
            str_query = "SELECT * FROM MED_PULL_DATA WHERE ID=1"
            Dim ds1 As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

            Dim subject = ds1.Tables(0).Rows(0).Item("MATTER1").ToString()

            Dim mailbody = ds1.Tables(0).Rows(0).Item("MATTER2").ToString()

            EmailService.Email.SendPlainTextEmails(efrom, toemailid, subject, mailbody, username, Password, host, port, "", False)

        End If


    End Sub

    Protected Sub Check_VAX()
        Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionStringMedical").ConnectionString()
        Dim ds As DataSet
        Dim pParms(4) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@VCO_STU_ID", Session("STU_ID"))
        pParms(2) = New SqlClient.SqlParameter("@OPTION", 3)
        Using reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "VACCINE_CONSENT_TRAN_TRAN", pParms)
            While reader.Read
                Dim lstrVAX = Convert.ToString(reader("VCO_VAX_CARD_MISSING"))
                If lstrVAX = "1" Then
                    chbNoVax.Checked = True
                End If
            End While
        End Using


    End Sub

  
End Class
