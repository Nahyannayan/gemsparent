﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ImmunizationParentEntry.ascx.vb"
    Inherits="Medical_Usercontrols_ImmunizationParentEntry" %>
    <%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
    <link rel="stylesheet" href="/css/style.css"/>
<script type="text/javascript">

    function openwindow() {

        window.showModalDialog("../Medical/Pages/ImmunizationSchedule.aspx", "", "dialogWidth:1200px; dialogHeight:500px; center:yes");

    }

    function openwindow1(a) {

        var docval = '';
        if (a == 1) {
            docval = 'CONSENT'
        }
        if (a == 2) {
            docval = 'IMMUNDOC'
        }
var dt=(new Date()).valueOf();
        window.showModalDialog("../Medical/Pages/ImmunizationDocumentUpload.aspx?doctype=" + docval+"&tm="+ dt +"", "", "dialogWidth:702px; dialogHeight:300px; center:yes");


    }

    function calljv(a, b) {


        window.showModalDialog("../Medical/Pages/ImmunizationParentEntryLevel.aspx?vcn_id=" + a + "&dos_id=" + b + "", "", "dialogWidth:702px; dialogHeight:430px; center:yes");
        window.location.reload();
    }

    function callhelp(a, b) {


        window.showModalDialog("../ImmunizationHelp.aspx?vcn_id=" + a + "&dos_id=" + b + "", "", "dialogWidth:702px; dialogHeight:330px; center:yes");
       
    }



    function changecolor(a) {

        //document.getElementById(a).style.backgroundColor = "#1084AA";
        // if (document.getElementById("<%=HiddenPvalue.ClientID %>").value != '') {

        // document.getElementById(document.getElementById("<%=HiddenPvalue.ClientID %>").value).style.backgroundColor = "#ffffff"
        //}
        //document.getElementById("<%=HiddenPvalue.ClientID %>").value = a;

        var elem;
        if (document.getElementById && (elem = document.getElementById(a))) {
            if (elem.style) elem.style.cursor = 'pointer';
        }
    }

</script>
<div class="mainheading">
    <div class="left">
        Immunization Details</div>
</div>
<asp:Panel ID="Panel2" runat="server">
    Immunization is a proven tool for controlling and eliminating life-threatening infectious diseases and one of the most cost-effective health investment. From time to time schools will facilitate vaccination days
    under the guidance of Dubai Health Authority (DHA) and according to their vaccination schedule.
    <br />
    <br />
    For our records and your own information please either opt in or out of this programme
    by ticking the appropriate box below.<br />
    <br />
    Important note: As part of the yearly admissions procedure we kindly request you
    to fill in the vaccination history of your child / children for our reference and
    attach a recent copy of his/her vaccination card. This information is important
    for us regardless of your consent / non-consent.
    <br />
    <br />
  In general, do you want to immunize your child at school? Specific vaccination details and dates will be communicated to you by your school closer to the time.
    <br />
    <asp:RadioButtonList ID="Radiooption" RepeatDirection="Vertical" runat="server" 
        AutoPostBack="True">
        <asp:ListItem Text="Yes, I agree to vaccinations given to my child" Value="1"></asp:ListItem>
        <asp:ListItem Text="No, I do not consent to my child being vaccinated at school"
            Value="0"></asp:ListItem>
    </asp:RadioButtonList>
    <br />
    <br />
    <br />
    <br />
    <br />
    <div id="div1" runat="server" visible="false">
        <br />
        <b>Terms &amp; Conditions</b>
        <br />
        <br />
        The date and time of a particular vaccination programme will be advised approximately
        one week before.
        <br />
        Vaccinations will only be carried out with your consent and the original vaccination
        card received by the school at least 1 day before the day of immunization. Vaccination
        card copies cannot be accepted on those days.
        <br />
        All vaccines are capable of causing serious problems, such as allergic reactions.
        The risks are however extremely rare and for the most part the side effects are
        minor, such as (but not limited to):
        <br />
        - redness, swelling or tenderness at the injection site
        <br />
        - the child may be a little irritable with general feeling of being unwell
        <br />
        - the child may develop a fever. This should be treated with an aspirin-free pain
        reliever like paracetamol or ibuprofen
        <br />
        - seizures
        <br />
        - the child may develop a rash 7-10 days post measles vaccine
        <br />
        <br />
        I hereby release the school of any liability in case of an adverse reaction due
        to a vaccine administered at my consent.
        <br />
        By ticking below box you agree to all of the above. This will replace your original signature.
        <br />
        <br />
        <asp:LinkButton ID="lnksave" Visible="false" CausesValidation="false" OnClientClick="openwindow1(1);return false;"
            runat="server">Upload Document</asp:LinkButton>
        <br />
        <br />
        <asp:CheckBox ID="Checkc2" Text="I agree to the Terms & Conditions." runat="server" />
        <br />
        <br />
    </div>
 
    <asp:Button ID="btnsave" runat="server" Text="Proceed" />
  
  
    <br />
    <asp:Label ID="lblmessage" runat="server" ForeColor="Red"></asp:Label>
</asp:Panel>
<asp:Panel ID="Panel1" Visible="false" runat="server">
<%--    <asp:LinkButton ID="lnkschedule" OnClientClick="openwindow();return false;" runat="server"><b>Click here to view DHA immunization schedule</b></asp:LinkButton>--%>
<a href="..\Images\Common\PageBody\DHA_Immunization_Schedule.pdf" target="_blank"><b>Click here to view DHA immunization schedule</b></a>
    <br />
    
    <br />
    Important note: As part of the yearly admissions procedure we kindly request you
    to fill in the vaccination history of your child / children for our reference and
    attach a recent copy of his/her vaccination card. This information is important
    for us regardless of your consent / non-consent.
    <br />
    <br />
    Please fill in your child’s vaccination history below. For further information on the different vaccines below, click the (<img src='..\..\Images\Common\PageBody\qm.png' alt='Details' width='11px' height='11px'/>). Before you enter the date, kindly note that some vaccines may sound the same but include different combinations – please chose carefully! 
    <br />
    <br />
    If your child does not have a vaccination card or the card has been lost, please tick this box  :   <asp:CheckBox ID="chbNoVax" Text="" runat="server" AutoPostBack="true" />
    <br /><font color="Red">(Tick on this box, will send a notification email to the authorised staff and acknowledgment to the parent.)</font>
      <br />  <br />
    <asp:LinkButton ID="lnkdocument" CausesValidation="false" OnClientClick="openwindow1(2);return false;"
        runat="server">
   <font color="red">Upload your child’s vaccination card copy</font>
   <font color="red"> <b>here</b></font></asp:LinkButton>
    <br />
    <br />
    Important: Vaccination cards in any other language than English
    need to be translated into English by a certified translator.
    Click on the <b>yellow</b> block to update your child’s immunization record. Kindly ignore blocks marked  as <b>--</b>
    <br />
    <asp:GridView ID="GridInfo" runat="server">
    </asp:GridView>
     
    <asp:HiddenField ID="HiddenPvalue" runat="server" />
</asp:Panel>
<div id="pldis" runat="server" style="display: none; overflow: visible; background-color: White;
    border-color: #b5cae7; border-style: solid; border-width: 4px; width: 700px;">
    <div align="right">
        <asp:ImageButton ID="Imageclose" ImageUrl="~/Images/Common/PageBody/error.png" runat="server" /></div>
    <iframe id="FEx1" src="../Medical/Pages/ImmunizationDocumentUpload.aspx" width="702px" height="230px"
        scrolling="auto" marginwidth="0px" frameborder="0"></iframe>
</div>
<asp:Button ID="btnShowPopup" runat="server" Style="display: none" />
<ajaxToolkit:ModalPopupExtender ID="mdlPopup" runat="server" TargetControlID="btnShowPopup"
    PopupControlID="pldis" BackgroundCssClass="modalBackground" DropShadow="false"
    RepositionMode="RepositionOnWindowResizeAndScroll">
</ajaxToolkit:ModalPopupExtender>

<asp:Label ID="Label1" runat="server"></asp:Label><br />
 <asp:Panel ID="PanelStatusupdate"
    runat="server" BackColor="white" CssClass="modalPopup" Style="display: none">
    <table border="1" style="border-color:#1b80b6;" cellpadding="5" cellspacing="0" width="240">
      
        <tr>
            <td align="center">
                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                    <ContentTemplate>
                        <table>
                            <tr>
                                <td >
            <asp:Label ID="lblMessage0" runat="server" ForeColor="Red"></asp:Label></td>
                            </tr>
                            <tr>
                                <td align="center">
                                   
                                   <asp:Button ID="btnmok" runat="server"  CssClass="btn btn-info" Text="Ok"
                                        ValidationGroup="s" Width="80px" CausesValidation="False" />
                                   </td>
                            </tr>
                        </table>
                        &nbsp;
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</asp:Panel>
<ajaxToolkit:ModalPopupExtender ID="MO1" runat="server" BackgroundCssClass="modalBackground"
    DropShadow="true" PopupControlID="PanelStatusupdate"
    RepositionMode="RepositionOnWindowResizeAndScroll"  TargetControlID="Label1">
</ajaxToolkit:ModalPopupExtender>
 