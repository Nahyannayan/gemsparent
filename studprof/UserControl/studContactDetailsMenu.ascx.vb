﻿Imports System.Data
Imports Microsoft.ApplicationBlocks.Data

Imports System.Data.SqlClient
Imports System.Web.Configuration

Imports System.IO
Partial Class StudProf_UserControl_studContactDetailsMenu
    Inherits System.Web.UI.UserControl
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            'If (Convert.ToString(Session("bUpdateContactDetails")).ToUpper = "TRUE") Then
            '    tblBasic.Visible = False
            Dim cMCount As String = "0"
            Dim cPCount As String = "0"
            Dim cHCount As String = "0"
            Dim cMedCount As String = "0"
            Dim cGenCount As String = "0"
            'Dim cImmuneCount As String = "0"

            cMCount = Is_EntryExists(Session("OLU_ID"), "%1|%")
            cPCount = Is_EntryExists(Session("OLU_ID"), "%2|%")
            cHCount = Is_EntryExists(Session("OLU_ID"), "%3|%")
            cMedCount = Is_EntryExists(Session("OLU_ID"), "%4|%")
            cGenCount = Is_EntryExists(Session("OLU_ID"), "%6|%")
            'cImmuneCount = Is_EntryExists(Session("OLU_ID"), "%5|%")
            cMCount = 1
            cPCount = 1
            cHCount = 1
            cMedCount = 1
            cGenCount = 1



            If cMCount <> "" Then
                If Convert.ToInt16(cMCount) > 0 Then
                   
                    lbtnStep1.Attributes.Add("class", "cssStepBtnComp")
                Else
                    lbtnStep1.Attributes.Add("class", "cssStepBtnInActive")
                End If
            Else
                lbtnStep1.Attributes.Add("class", "cssStepBtnInActive")
            End If
            If cPCount <> "" Then
                If Convert.ToInt16(cPCount) > 0 Then
                  
                    lbtnStep2.Attributes.Add("class", "cssStepBtnComp")
                Else
                    lbtnStep2.Attributes.Add("class", "cssStepBtnInActive")
                End If
            Else
                lbtnStep2.Attributes.Add("class", "cssStepBtnInActive")
            End If
            If cHCount <> "" Then
                If Convert.ToInt16(cHCount) > 0 Then


                    lbtnStep3.Attributes.Add("class", "cssStepBtnComp")
                Else
                    lbtnStep3.Attributes.Add("class", "cssStepBtnInActive")
                End If
            Else
                lbtnStep3.Attributes.Add("class", "cssStepBtnInActive")
            End If
            'If cMedCount <> "" Then
            '    If Convert.ToInt16(cMedCount) > 0 Then

            '        lbtnStep4.Attributes.Add("class", "cssStepBtnComp")
            '    Else
            '        lbtnStep4.Attributes.Add("class", "cssStepBtnInActive")
            '    End If
            'Else
            '    lbtnStep4.Attributes.Add("class", "cssStepBtnInActive")
            'End If
           
            If cGenCount <> "" Then
                If Convert.ToInt16(cGenCount) > 0 Then
                    
                    lbtnStep6.Attributes.Add("class", "cssStepBtnComp")
                Else
                    lbtnStep6.Attributes.Add("class", "cssStepBtnInActive")
                End If
            Else
                lbtnStep6.Attributes.Add("class", "cssStepBtnInActive")
            End If

            'If cImmuneCount <> "" Then
            '    If Convert.ToInt16(cImmuneCount) > 0 Then

            '        lbtnStep5.Attributes.Add("class", "cssStepBtnComp")
            '    Else
            '        lbtnStep5.Attributes.Add("class", "cssStepBtnInActive")
            '    End If
            'Else
            '    lbtnStep5.Attributes.Add("class", "cssStepBtnInActive")
            'End If
           
            Dim currentpage As String = Path.GetFileName(Page.Request.Path).ToString

            If currentpage = "StudentMainDetails.aspx" Then
                lbtnStep1.Attributes.Add("class", "")
                lbtnStep1.Attributes.Add("class", "cssStepBtnActive")
            ElseIf currentpage = "ParentDetails.aspx" Then
                lbtnStep2.Attributes.Add("class", "")
                lbtnStep2.Attributes.Add("class", "cssStepBtnActive")
            ElseIf currentpage = "StudHealthDetails.aspx" Then
                lbtnStep3.Attributes.Add("class", "")
                lbtnStep3.Attributes.Add("class", "cssStepBtnActive")
                'ElseIf currentpage = "StudMedication.aspx" Then
                '    lbtnStep4.Attributes.Add("class", "")
                '    lbtnStep4.Attributes.Add("class", "cssStepBtnActive")
                'ElseIf currentpage = "StudImmunization.aspx" Then
                '    lbtnStep5.Attributes.Add("class", "")
                '    lbtnStep5.Attributes.Add("class", "cssStepBtnActive")
            ElseIf currentpage = "StudentGeneralConsent.aspx" Then
                lbtnStep6.Attributes.Add("class", "")
                lbtnStep6.Attributes.Add("class", "cssStepBtnActive")

            End If

           

            
        End If
    End Sub

    Private Function Is_EntryExists(ByVal STU_ID As String, ByVal Type As String) As String


        Dim CONN As String = ConnectionManger.GetOASISConnectionString
        Dim cCount As String = String.Empty
        Dim param(2) As SqlParameter
        param(0) = New SqlParameter("@OLU_ID", Convert.ToInt32(Session("OLU_ID").ToString))
        param(1) = New SqlParameter("@Type", Type)
        Try
            Using readerStudent_D_Detail As SqlDataReader = SqlHelper.ExecuteReader(CONN, CommandType.StoredProcedure, "Get_Student_Contact_info_Exist", param)
                If readerStudent_D_Detail.HasRows = True Then
                    While readerStudent_D_Detail.Read
                        cCount = Convert.ToString(readerStudent_D_Detail("cCount"))
                    End While
                End If
            End Using
        Catch ex As Exception

        End Try


        Return cCount
    End Function
End Class
