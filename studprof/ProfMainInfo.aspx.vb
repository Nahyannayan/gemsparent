﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports InfoSoftGlobal
Partial Class ProfMainInfo
    Inherits System.Web.UI.Page
    Dim BshowPastoral As Integer = 0
    Sub studentdetails(ByVal stu_id As String)
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim param(10) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@stu_id", stu_id)
            Using readerStudent_Detail As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "student_Details", param)

                If readerStudent_Detail.HasRows = True Then
                    While readerStudent_Detail.Read
                        'handle the null value returned from the reader incase  convert.tostring

                        LTpob.Text = Convert.ToString(readerStudent_Detail("STU_POB"))
                        LTcunbirth.Text = Convert.ToString(readerStudent_Detail("COB"))
                        txtJoin_Shift.Text = Convert.ToString(readerStudent_Detail("SHF_DESCR_JOIN"))
                        txtJoin_Stream.Text = Convert.ToString(readerStudent_Detail("STM_DESCR_JOIN"))
                        txtACD_ID_Join.Text = Convert.ToString(readerStudent_Detail("ACD_ID_JOIN_Y"))
                        txtGRD_ID_Join.Text = Convert.ToString(readerStudent_Detail("GRM_JOIN_DESCR"))
                        txtSCT_ID_JOIN.Text = Convert.ToString(readerStudent_Detail("SCT_DESCR_JOIN"))
                        txtFee_ID.Text = Convert.ToString(readerStudent_Detail("FEE_ID"))
                        txtMOE_No.Text = Convert.ToString(readerStudent_Detail("BLUEID"))
                        txtFee_Spon.Text = Convert.ToString(readerStudent_Detail("SFEESPONSOR"))
                        txtPNo.Text = Convert.ToString(readerStudent_Detail("SPASPRTNO"))
                        txtPIssPlace.Text = Convert.ToString(readerStudent_Detail("SPASPRTISSPLACE"))
                        txtVNo.Text = Convert.ToString(readerStudent_Detail("STU_VISANO"))
                        txtVIssPlace.Text = Convert.ToString(readerStudent_Detail("STU_VISAISSPLACE"))
                        txtVIssAuth.Text = Convert.ToString(readerStudent_Detail("STU_VISAISSAUTH"))
                        TLtftype.Text = Convert.ToString(readerStudent_Detail("TFR_DESCR"))
                        LTreligion.Text = Convert.ToString(readerStudent_Detail("RLG_ID"))
                        LTnationality.Text = Convert.ToString(readerStudent_Detail("SNATIONALITY"))
                        LTminlist.Text = Convert.ToString(readerStudent_Detail("MINLIST"))
                        LTminlistType.Text = Convert.ToString(readerStudent_Detail("SMINLISTTYPE"))
                        txtFee_Spon.Text = Convert.ToString(readerStudent_Detail("SFEESPONSOR"))
                        LTemgcon.Text = Convert.ToString(readerStudent_Detail("SEMGCONTACT"))
                        txtEmiratesId.Text = Convert.ToString(readerStudent_Detail("STU_EMIRATES_ID"))

                        ViewState("temp_COB") = Convert.ToString(readerStudent_Detail("COB"))
                        ViewState("temp_HOUSE") = Convert.ToString(readerStudent_Detail("HOUSE"))
                        ViewState("temp_PrefContact") = Convert.ToString(readerStudent_Detail("STU_PREFCONTACT"))
                        ViewState("temp_Blood") = Convert.ToString(readerStudent_Detail("STU_BLOODGROUP"))
                        ViewState("ACD_ID") = Convert.ToString(readerStudent_Detail("ACD_ID"))
                        'Setting date
                        If IsDate(readerStudent_Detail("SDOJ")) = True Then
                            txtDOJ.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((readerStudent_Detail("SDOJ"))))
                        End If

                        If IsDate(readerStudent_Detail("MINDOJ")) = True Then
                            txtMINDOJ.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((readerStudent_Detail("MINDOJ"))))
                        End If

                        ''added by nahyan on 20aug- prem advise- house color
                        txtHouse.Text = Convert.ToString(readerStudent_Detail("HOUSE_DESCRIPTION"))

                        If IsDate(readerStudent_Detail("DOB")) = True Then
                            txtDob.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((readerStudent_Detail("DOB"))))
                        End If

                        If IsDate(readerStudent_Detail("STU_PASPRTISSDATE")) = True Then
                            txtPIssDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((readerStudent_Detail("STU_PASPRTISSDATE"))))
                        End If
                        If IsDate(readerStudent_Detail("STU_PASPRTEXPDATE")) = True Then
                            txtPExpDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((readerStudent_Detail("STU_PASPRTEXPDATE"))))
                        End If

                        If IsDate(readerStudent_Detail("STU_VISAISSDATE")) = True Then
                            txtVIssDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((readerStudent_Detail("STU_VISAISSDATE")))).Replace("01/Jan/1900", "")
                        End If
                        If IsDate(readerStudent_Detail("STU_VISAEXPDATE")) = True Then
                            txtVExpDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((readerStudent_Detail("STU_VISAEXPDATE")))).Replace("01/Jan/1900", "")
                        End If

                        Dim temp_Gender As String
                        temp_Gender = Convert.ToString(readerStudent_Detail("GENDER"))
                        If UCase(temp_Gender) = "F" Then
                            LTgender.Text = "Female"
                        ElseIf UCase(temp_Gender) = "M" Then
                            LTgender.Text = "Male"
                        End If

                        ltFirstLang.Text = Convert.ToString(readerStudent_Detail("STU_FIRSTLANG"))
                        ltOthLang.Text = Convert.ToString(readerStudent_Detail("STU_OTHLANG"))
                        ltProEng_R.Text = Convert.ToString(readerStudent_Detail("STU_ENG_READING"))
                        ltProEng_W.Text = Convert.ToString(readerStudent_Detail("STU_ENG_WRITING"))
                        ltProEng_S.Text = Convert.ToString(readerStudent_Detail("STU_ENG_SPEAKING"))

                    End While
                Else
                End If
            End Using
        Catch ex As Exception

        End Try
    End Sub
    Sub binddetails(ByVal stu_id As String)
        ' AccessStudentClass.GetStudent_D(stu_id)
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim param(10) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@stu_id", stu_id)
            Dim ds As DataSet
            'Using readerStudent_Detail As SqlDataReader = AccessStudentClass.GetStudent_M_DDetails(ViewState("viewid"), Session("sBsuid"))
            Using readerStudent_Detail As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "stu_contactDetails", param)

                If readerStudent_Detail.HasRows = True Then
                    While readerStudent_Detail.Read

                        Ltl_Fname.Text = Convert.ToString(readerStudent_Detail("STS_FNAME"))
                        Ltl_Fnationality.Text = Convert.ToString(readerStudent_Detail("FNATIONALITY"))
                        Ltl_Fpob.Text = Convert.ToString(readerStudent_Detail("STS_FCOMPOBOX"))
                        Ltl_FEmirate.Text = Convert.ToString(readerStudent_Detail("STS_FEMIR"))
                        Ltl_FPhoneRes.Text = Convert.ToString(readerStudent_Detail("STS_FRESPHONE"))
                        Ltl_FOfficePhone.Text = Convert.ToString(readerStudent_Detail("STS_FOFFPHONE"))
                        Ltl_FMobile.Text = Convert.ToString(readerStudent_Detail("STS_FMOBILE"))
                        Ltl_FEmail.Text = Convert.ToString(readerStudent_Detail("STS_FEMAIL"))
                        Ltl_FFax.Text = Convert.ToString(readerStudent_Detail("STS_FFAX"))
                        Ltl_FOccupation.Text = Convert.ToString(readerStudent_Detail("STS_FOCC"))
                        Ltl_FCompany.Text = Convert.ToString(readerStudent_Detail("STS_FCOMPANY"))

                        Ltl_Mname.Text = Convert.ToString(readerStudent_Detail("STS_MNAME"))
                        Ltl_Mnationality.Text = Convert.ToString(readerStudent_Detail("MNATIONALITY"))
                        Ltl_Mpob.Text = Convert.ToString(readerStudent_Detail("STS_MCOMPOBOX"))
                        Ltl_MEmirate.Text = Convert.ToString(readerStudent_Detail("STS_MEMIR"))
                        Ltl_MPhoneRes.Text = Convert.ToString(readerStudent_Detail("STS_MRESPHONE"))
                        Ltl_MOfficePhone.Text = Convert.ToString(readerStudent_Detail("STS_MOFFPHONE"))
                        Ltl_MMobile.Text = Convert.ToString(readerStudent_Detail("STS_MMOBILE"))
                        Ltl_MEmail.Text = Convert.ToString(readerStudent_Detail("STS_MEMAIL"))
                        Ltl_MFax.Text = Convert.ToString(readerStudent_Detail("STS_MFAX"))
                        Ltl_MOccupation.Text = Convert.ToString(readerStudent_Detail("STS_MOCC"))
                        Ltl_MCompany.Text = Convert.ToString(readerStudent_Detail("STS_MCOMPANY"))

                        Ltl_Gname.Text = Convert.ToString(readerStudent_Detail("STS_GNAME"))
                        Ltl_Gnationality.Text = Convert.ToString(readerStudent_Detail("GNATIONALITY"))
                        Ltl_Gpob.Text = Convert.ToString(readerStudent_Detail("STS_GCOMPOBOX"))
                        Ltl_GEmirate.Text = Convert.ToString(readerStudent_Detail("STS_GEMIR"))
                        Ltl_GPhoneRes.Text = Convert.ToString(readerStudent_Detail("STS_GRESPHONE"))
                        Ltl_GOfficePhone.Text = Convert.ToString(readerStudent_Detail("STS_GOFFPHONE"))
                        Ltl_GMobile.Text = Convert.ToString(readerStudent_Detail("STS_GMOBILE"))
                        Ltl_GEmail.Text = Convert.ToString(readerStudent_Detail("STS_GEMAIL"))
                        Ltl_GFax.Text = Convert.ToString(readerStudent_Detail("STS_GFAX"))
                        Ltl_GOccupation.Text = Convert.ToString(readerStudent_Detail("STS_GOCC"))
                        Ltl_GCompany.Text = Convert.ToString(readerStudent_Detail("STS_GCOMPANY"))

                        Dim col As Integer
                        If Convert.ToString(readerStudent_Detail("STU_PRIMARYCONTACT")) = "F" Then
                            col = 2
                        ElseIf Convert.ToString(readerStudent_Detail("STU_PRIMARYCONTACT")) = "M" Then
                            col = 3

                        ElseIf Convert.ToString(readerStudent_Detail("STU_PRIMARYCONTACT")) = "G" Then
                            col = 4
                        Else
                            col = 2
                        End If

                    End While
                Else
                    'lblerror.Text = "No Records Found "
                End If
            End Using
            'If ds.Tables(0).Rows.Count > 0 Then
            'Else
            '    lblerror.Text = "No Records Found "
            'End If
        Catch ex As Exception
            '  lblerror.Text = "ERROR WHILE RETREVING DATA"
        End Try

    End Sub
    Private Sub GridBindFeeDetails(ByVal stu_id As String)
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_FEESConnectionString").ConnectionString
        Dim objConn As New SqlConnection(str_conn) '
        Dim ds As New DataSet
        Try
            Using objConn
                Dim pParms(5) As SqlClient.SqlParameter

                pParms(0) = New SqlClient.SqlParameter("@DOCDT", SqlDbType.DateTime)
                pParms(0).Value = Format(Date.Parse(Now.Date), "dd/MMM/yyyy")
                pParms(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
                pParms(1).Value = Session("STU_BSU_ID")
                pParms(2) = New SqlClient.SqlParameter("@STU_ID", SqlDbType.BigInt)
                pParms(2).Value = stu_id
                pParms(3) = New SqlClient.SqlParameter("@STU_TYPE", SqlDbType.VarChar, 2)
                pParms(3).Value = "S"
                pParms(4) = New SqlClient.SqlParameter("@ACD_ID", SqlDbType.Int)
                pParms(4).Value = Session("STU_ACD_ID")



                ds = SqlHelper.ExecuteDataset(objConn, CommandType.StoredProcedure, "FEES.F_GetFeeDetailsFOrCollection", pParms)
                gvFeeDetails1.DataSource = ds
                gvFeeDetails1.DataBind()



            End Using
        Catch ex As Exception

        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try

    End Sub
    Sub bindAttChart(ByVal stu_id As String)
        Try

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim param(2) As SqlClient.SqlParameter
            Dim ds As New DataSet
            param(0) = New SqlClient.SqlParameter("@stu_id", stu_id)

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GetStuddashBoard_Att_Patten", param)
            radAttendanceChart.DataSource = ds.Tables(0)
            radAttendanceChart.DataBind()
        Catch ex As Exception

        End Try
    End Sub
    Sub bindABSChart(ByVal stu_id As String)
        Try

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim param(2) As SqlClient.SqlParameter
            Dim ds As New DataSet
            param(0) = New SqlClient.SqlParameter("@stu_id", stu_id)

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GetStuddashBoard_ABS_Patten", param)
            radAttendanceABSChart.DataSource = ds.Tables(0)
            radAttendanceABSChart.DataBind()
        Catch ex As Exception

        End Try
    End Sub
    Sub bindLATEChart(ByVal stu_id As String)
        Try

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim param(2) As SqlClient.SqlParameter
            Dim ds As New DataSet
            param(0) = New SqlClient.SqlParameter("@stu_id", stu_id)

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GetStuddashBoard_LATE_Patten", param)
            radAttendanceLateChart.DataSource = ds.Tables(0)
            radAttendanceLateChart.DataBind()
        Catch ex As Exception

        End Try
    End Sub


    Private Sub GridBindPayHist(ByVal stu_id As String)
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_FEESConnectionString").ConnectionString
        Dim objConn As New SqlConnection(str_conn) '
        Dim ds As New DataSet
        Try
            Using objConn
                Dim pParms(3) As SqlClient.SqlParameter

                pParms(0) = New SqlClient.SqlParameter("@STU_ID", SqlDbType.VarChar, 20)
                pParms(0).Value = stu_id

                ds = SqlHelper.ExecuteDataset(objConn, CommandType.StoredProcedure, "FEES.Dashboard_PaymentHistory", pParms)
                gvPaymentHist1.DataSource = ds
                gvPaymentHist1.DataBind()



            End Using
        Catch ex As Exception

        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try

    End Sub
    Sub BindSubjectList(ByVal stu_id As String)
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString


        Dim str_query As String = " SELECT DISTINCT SBG_DESCR,SGR_DESCR, TEACHERS=ISNULL((SELECT " _
                               & "  STUFF((SELECT ', '+ ISNULL(EMP_FNAME,'')+' '+ISNULL(EMP_MNAME,' ')+' '+ " _
                               & " ISNULL(EMP_LNAME,'') FROM EMPLOYEE_M AS K INNER JOIN  GROUPS_TEACHER_S " _
                               & " AS L ON K.EMP_ID=L.SGS_EMP_ID   WHERE SGS_SGR_ID = C.SGR_ID" _
                               & " ORDER BY EMP_FNAME,EMP_MNAME,EMP_LNAME for xml path('')),1,1,'')),'')" _
                               & " FROM  STUDENT_GROUPS_S AS A INNER JOIN" _
                               & " SUBJECTS_GRADE_S AS B ON A.SSD_SBG_ID=B.SBG_ID" _
                               & " LEFT OUTER JOIN GROUPS_M AS C ON A.SSD_SGR_ID=C.SGR_ID" _
                               & " WHERE SSD_STU_ID=" + stu_id + " AND SSD_ACD_ID='" + Session("STU_ACD_ID") + "' ORDER BY SBG_DESCR"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        If ds.Tables(0).Rows.Count = 0 Then
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
            ds.Tables(0).Rows(0)("SBG_DESCR") = ""
            ds.Tables(0).Rows(0)("SGR_DESCR") = ""
            ds.Tables(0).Rows(0)("TEACHERS") = ""
            gvSubjects.DataSource = ds
            gvSubjects.DataBind()
            Dim columnCount As Integer = gvSubjects.Rows(0).Cells.Count
            'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
            gvSubjects.Rows(0).Cells.Clear()
            gvSubjects.Rows(0).Cells.Add(New TableCell)
            gvSubjects.Rows(0).Cells(0).ColumnSpan = columnCount
            gvSubjects.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvSubjects.Rows(0).Cells(0).Text = "No record available."
        Else
            gvSubjects.DataSource = ds
            gvSubjects.DataBind()
        End If

    End Sub
    Sub bindMain_details(ByVal stu_id As String)
        Try
            Dim todayDT As String = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim param(10) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@stu_id", stu_id)
            Dim tot_mrk As Double
            Dim DayPrs As Double


            Using readerStudent_Detail As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "GetStud_dashBoard_Att", param)

                If readerStudent_Detail.HasRows = True Then
                    While readerStudent_Detail.Read

                        ltAcdWorkingDays.Text = Convert.ToString(readerStudent_Detail("tot_acddays"))
                        ltTotWorkTilldate.Text = readerStudent_Detail("tot_wrkdays").ToString
                        ltAttMarkTilldate.Text = readerStudent_Detail("tot_marked").ToString
                        ltDayAbsent.Text = readerStudent_Detail("tot_abs").ToString
                        ltDayPresent.Text = readerStudent_Detail("tot_att").ToString
                        ltDayLeave.Text = readerStudent_Detail("tot_leave").ToString
                        ltTitleAcd.Text = "Total working days for the academic year " + readerStudent_Detail("acd_year").ToString
                        ltTotTilldate.Text = "Total working days till " + todayDT
                        ltMrkTilldate.Text = "Total Attendance marked till " + todayDT
                        tot_mrk = Convert.ToDecimal(readerStudent_Detail("tot_marked"))
                        DayPrs = Convert.ToDecimal(readerStudent_Detail("tot_att"))
                        If tot_mrk = 0 Then
                            tot_mrk = 1
                        End If
                        ltPerAtt.Text = Math.Round(((DayPrs / tot_mrk) * 100), 2)
                    End While
                Else
                    '  lblerror.Text = "No Records Found "
                End If
            End Using
            'If ds.Tables(0).Rows.Count > 0 Then
            'Else
            '    lblerror.Text = "No Records Found "
            'End If
        Catch ex As Exception
            'lblerror.Text = "ERROR WHILE RETREVING DATA"
        End Try

    End Sub



    Public Sub BindGrid(ByVal stu_id As String)
        Try


            Dim ds As DataSet
            Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim PARAM(2) As SqlParameter
            PARAM(0) = New SqlParameter("@STU_ID", stu_id)
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "OPL.GETSMS_DETAILS", PARAM)

            If ds.Tables(0).Rows.Count = 0 Then
                ViewState("SMS_row") = "FALSE"

                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
                ds.Tables(0).Rows(0)("LOG_CMS_ID") = 0
                ds.Tables(0).Rows(0)("CMS_SMS_TEXT") = ""
                ds.Tables(0).Rows(0)("LOG_MOBILE_NUMBER") = ""
                ds.Tables(0).Rows(0)("LOG_ENTRY_DATE") = ""
                ds.Tables(0).Rows(0)("bShow") = False
                ds.Tables(0).Rows(0)("tempview") = ""
                ds.Tables(0).Rows(0)("status") = ""
                GridMSMS.DataSource = ds
                GridMSMS.DataBind()

                Dim columnCount As Integer = GridMSMS.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                GridMSMS.Rows(0).Cells.Clear()
                GridMSMS.Rows(0).Cells.Add(New TableCell)
                GridMSMS.Rows(0).Cells(0).ColumnSpan = columnCount
                GridMSMS.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                GridMSMS.Rows(0).Cells(0).Text = "No SMS sent."

            Else
                ViewState("SMS_row") = "TRUE"


                GridMSMS.DataSource = ds
                GridMSMS.DataBind()
            End If
        Catch ex As Exception

        End Try
    End Sub

    Public Sub BindGridEmail(ByVal stu_id As String)
        Try


            Dim ds As DataSet
            Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim PARAM(2) As SqlParameter
            PARAM(0) = New SqlParameter("@STU_ID", stu_id)
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "OPL.GETEMAIL_DETAILS", PARAM)
            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())

                ds.Tables(0).Rows(0)("LOG_EML_ID") = 0
                ds.Tables(0).Rows(0)("EML_TITLE") = ""
                ds.Tables(0).Rows(0)("LOG_EMAIL_ID") = ""
                ds.Tables(0).Rows(0)("LOG_ENTRY_DATE") = ""
                ds.Tables(0).Rows(0)("bShow") = False
                ds.Tables(0).Rows(0)("tempview") = ""
                ds.Tables(0).Rows(0)("status") = ""
                ds.Tables(0).Rows(0)("EmailType") = ""

                GridEmails.DataSource = ds
                GridEmails.DataBind()

                Dim columnCount As Integer = GridEmails.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                GridEmails.Rows(0).Cells.Clear()
                GridEmails.Rows(0).Cells.Add(New TableCell)
                GridEmails.Rows(0).Cells(0).ColumnSpan = columnCount
                GridEmails.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                GridEmails.Rows(0).Cells(0).Text = "No Emails sent."
            Else


                GridEmails.DataSource = ds
                GridEmails.DataBind()
            End If
        Catch ex As Exception

        End Try
    End Sub
    Public Sub BindMemerships(ByVal stu_id As String)
        Try

            Dim ds As DataSet

            Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString
            Dim query = " SELECT RECORD_ID,LIBRARY_DIVISION_DES,MEMBERSHIP_DES FROM  LIBRARY_MEMBERSHIP_USERS A" & _
                              " INNER JOIN LIBRARY_MEMBERSHIPS B on A.MEMBERSHIP_ID = B.MEMBERSHIP_ID " & _
                              " INNER JOIN LIBRARY_DIVISIONS C ON C.LIBRARY_DIVISION_ID=A.LIBRARY_DIVISION_ID  WHERE A.USER_TYPE='STUDENT' AND USER_ID='" & stu_id & "'"

            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, query)

            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())

                ds.Tables(0).Rows(0)("RECORD_ID") = 0
                ds.Tables(0).Rows(0)("LIBRARY_DIVISION_DES") = ""
                ds.Tables(0).Rows(0)("MEMBERSHIP_DES") = ""


                GridMemberships.DataSource = ds
                GridMemberships.DataBind()

                Dim columnCount As Integer = GridMemberships.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                GridMemberships.Rows(0).Cells.Clear()
                GridMemberships.Rows(0).Cells.Add(New TableCell)
                GridMemberships.Rows(0).Cells(0).ColumnSpan = columnCount
                GridMemberships.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                GridMemberships.Rows(0).Cells(0).Text = "No records available."
            Else
                GridMemberships.DataSource = ds
                GridMemberships.DataBind()

            End If

        Catch ex As Exception

        End Try
    End Sub


    Public Sub BindTransactions(ByVal stu_id As String)
        Try
            Dim ds As DataSet
            Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString
            Dim PARAM(2) As SqlParameter
            PARAM(0) = New SqlParameter("@STU_ID", stu_id)
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "OPL.GETSTU_ISSUE_DETAILS", PARAM)

            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())

                ds.Tables(0).Rows(0)("STOCK_ID") = 0
                ds.Tables(0).Rows(0)("PRODUCT_IMAGE_URL") = ""
                ds.Tables(0).Rows(0)("ITEM_TITLE") = ""
                ds.Tables(0).Rows(0)("LIBRARY_DIVISION_DES") = ""
                ds.Tables(0).Rows(0)("ITEM_TAKEN_DATE") = "01/Jan/1900"
                ds.Tables(0).Rows(0)("ITEM_RETURN_DATE") = ""
                ds.Tables(0).Rows(0)("tempview") = ""
                ds.Tables(0).Rows(0)("FINE_AMOUNT") = ""
                ds.Tables(0).Rows(0)("CURRENCY_ID") = ""
                ds.Tables(0).Rows(0)("AUTHOR") = ""
                ds.Tables(0).Rows(0)("PUBLISHER") = ""
                ds.Tables(0).Rows(0)("RTN_DATE") = ""

                GrdUserTransaction.DataSource = ds
                GrdUserTransaction.DataBind()

                Dim columnCount As Integer = GrdUserTransaction.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                GrdUserTransaction.Rows(0).Cells.Clear()
                GrdUserTransaction.Rows(0).Cells.Add(New TableCell)
                GrdUserTransaction.Rows(0).Cells(0).ColumnSpan = columnCount
                GrdUserTransaction.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                GrdUserTransaction.Rows(0).Cells(0).Text = "No records available."
            Else


                GrdUserTransaction.DataSource = ds
                GrdUserTransaction.DataBind()
            End If


        Catch ex As Exception

        End Try
    End Sub

    Public Sub BindItemDue(ByVal stu_id As String)
        Try
            Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString
            Dim PARAM(2) As SqlParameter
            PARAM(0) = New SqlParameter("@STU_ID", stu_id)

            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "OPL.GETSTU_DUE_DETAILS", PARAM)

            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())

                ds.Tables(0).Rows(0)("STOCK_ID") = 0
                ds.Tables(0).Rows(0)("PRODUCT_IMAGE_URL") = ""
                ds.Tables(0).Rows(0)("ITEM_TITLE") = ""
                ds.Tables(0).Rows(0)("LIBRARY_DIVISION_DES") = ""
                ds.Tables(0).Rows(0)("ITEM_TAKEN_DATE") = "01/Jan/1900"
                ds.Tables(0).Rows(0)("ITEM_RETURN_DATE") = ""
                ds.Tables(0).Rows(0)("FINE_AMOUNT") = ""
                ds.Tables(0).Rows(0)("tempview") = ""
                ds.Tables(0).Rows(0)("CURRENCY_ID") = ""
                ds.Tables(0).Rows(0)("AUTHOR") = ""
                ds.Tables(0).Rows(0)("PUBLISHER") = ""
                ds.Tables(0).Rows(0)("RTN_DATE") = ""
                GridDue.DataSource = ds
                GridDue.DataBind()
                Dim columnCount As Integer = GridDue.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                GridDue.Rows(0).Cells.Clear()
                GridDue.Rows(0).Cells.Add(New TableCell)
                GridDue.Rows(0).Cells(0).ColumnSpan = columnCount
                GridDue.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                GridDue.Rows(0).Cells(0).Text = "No records available."
            Else




                GridDue.DataSource = ds
                GridDue.DataBind()
            End If

        Catch ex As Exception

        End Try
    End Sub

    Public Sub BindReservations(ByVal stu_id As String)
        Try
            Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionStringLibrary").ConnectionString
            Dim PARAM(2) As SqlParameter
            PARAM(0) = New SqlParameter("@STU_ID", stu_id)

            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "OPL.GETSTU_RES_DETAILS", PARAM)


            If ds.Tables(0).Rows.Count = 0 Then
                ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())

                ds.Tables(0).Rows(0)("PRODUCT_IMAGE_URL") = ""
                ds.Tables(0).Rows(0)("ITEM_TITLE") = ""
                ds.Tables(0).Rows(0)("LIBRARY_DIVISION_DES") = ""
                ds.Tables(0).Rows(0)("ENTRY_DATE") = "01/Jan/1900"
                ds.Tables(0).Rows(0)("RESERVE_START_DATE") = "01/Jan/1900"
                ds.Tables(0).Rows(0)("RESERVE_END_DATE") = "01/Jan/1900"
                ds.Tables(0).Rows(0)("CANCEL_RESERVATION") = ""
                ds.Tables(0).Rows(0)("tempview") = ""
                GridReservations.DataSource = ds
                GridReservations.DataBind()
                Dim columnCount As Integer = GridReservations.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                GridReservations.Rows(0).Cells.Clear()
                GridReservations.Rows(0).Cells.Add(New TableCell)
                GridReservations.Rows(0).Cells(0).ColumnSpan = columnCount
                GridReservations.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                GridReservations.Rows(0).Cells(0).Text = "No records available."


            Else



                GridReservations.DataSource = ds
                GridReservations.DataBind()
            End If

        Catch ex As Exception

        End Try
    End Sub
    Protected Function GetNavigateUrl1(ByVal pId As String) As String
        'Return String.Format("javascript:var popup = window.showModalDialog('stu_ActionViewDetails.aspx?Info_id={0}', '','dialogHeight:800px;dialogWidth:950px;scroll:yes;resizable:no;'); return false; ", pId)
        Return String.Format("javascript: return ShowWindowWithClose('/studprof/stu_ActionViewDetails.aspx?Info_id={0}', 'search', '45%', '85%'); return false; ", pId)
    End Function
    Private Sub BindGrid_neg(ByVal stu_id As String)
        Try
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim dsDetails As DataSet
            Dim PARAM(2) As SqlParameter
            PARAM(0) = New SqlParameter("@STU_ID", stu_id)
            PARAM(1) = New SqlParameter("@CAT_ID", 3)
            dsDetails = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "BM.GET_ACHIEVEMENT_BEHAVIOUR", PARAM)


            'Dim str_query = " SELECT A.BM_ID,A.BM_ENTRY_DATE,(ISNULL(EMP_FNAME,'') + ' ' + ISNULL(EMP_LNAME,'')) As EMPNAME," & _
            '                " BM_INCIDENT_TYPE,BM_CATEGORY_SCORE as BM_SCORE,T.BM_CATEGORYNAME,V.BM_STU_ID,S.STU_ID," & _
            '                " S.STU_GRD_ID,(ISNULL(S.STU_FIRSTNAME,'') + ' ' + ISNULL(STU_MIDNAME,'')+ ' ' + ISNULL(STU_LASTNAME,'')) As StudName FROM BM.BM_MASTER A " & _
            '                " INNER JOIN  EMPLOYEE_M B ON A.BM_REPORTING_STAFF_ID = B.EMP_ID  " & _
            '                " INNER JOIN  BM.BM_STUDENTSINVOLVED V ON V.BM_ID=A.BM_ID  " & _
            '                " INNER JOIN  STUDENT_M S ON S.STU_ID=V.BM_STU_ID" & _
            '                " INNER JOIN BM.BM_CATEGORY T ON T.BM_CATEGORYID=A.BM_CATEGORYID " & _
            '                " WHERE isNULL(BM_CONFIDENTIAL,0) =0 AND S.STU_ID =" & stu_id & "  AND BM_CATEGORYHRID=3 ORDER BY BM_ENTRY_DATE DESC"


            ' Dim dsDetails As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            gvStudGrade.DataSource = dsDetails

            If dsDetails.Tables(0).Rows.Count = 0 Then
                dsDetails.Tables(0).Rows.Add(dsDetails.Tables(0).NewRow())
                gvStudGrade.DataBind()
                Dim columnCount As Integer = gvStudGrade.Rows(0).Cells.Count
                gvStudGrade.Rows(0).Cells.Clear()
                gvStudGrade.Rows(0).Cells.Add(New TableCell)
                gvStudGrade.Rows(0).Cells(0).ColumnSpan = columnCount
                gvStudGrade.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvStudGrade.Rows(0).Cells(0).Text = "No record available."

            Else
                gvStudGrade.DataBind()
                'For Each GrdvRow As GridViewRow In gvStudGrade.Rows
                '    If GrdvRow.RowType = DataControlRowType.DataRow Then
                '        DirectCast(GrdvRow.FindControl("lblStudentNo"), Label).Text = Format(CDate(DirectCast(GrdvRow.FindControl("lblStudentNo"), Label).Text), "dd-MMM-yyyy")
                '        'If DirectCast(GrdvRow.FindControl("lblShift"), Label).Text = "FI" Then
                '        '    DirectCast(GrdvRow.FindControl("lblShift"), Label).Text = "For Information"
                '        '    DirectCast(GrdvRow.FindControl("lblShift"), Label).ForeColor = Drawing.Color.Blue
                '        '    GrdvRow.ForeColor = Drawing.Color.Blue
                '        'Else
                '        '    DirectCast(GrdvRow.FindControl("lblShift"), Label).Text = "For Action"
                '        '    DirectCast(GrdvRow.FindControl("lblShift"), Label).ForeColor = Drawing.Color.Red
                '        '    GrdvRow.ForeColor = Drawing.Color.Red
                '        'End If
                '    End If
                'Next
            End If

        Catch ex As Exception

        End Try

    End Sub


    Private Sub BindGrid_POS(ByVal stu_id As String)
        Try
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim dsDetails As DataSet
            Dim PARAM(2) As SqlParameter
            PARAM(0) = New SqlParameter("@STU_ID", stu_id)
            PARAM(1) = New SqlParameter("@CAT_ID", 2)
            dsDetails = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "BM.GET_ACHIEVEMENT_BEHAVIOUR", PARAM)
            'Dim str_query = " SELECT A.BM_ID,A.BM_ENTRY_DATE,(ISNULL(EMP_FNAME,'') + ' ' + ISNULL(EMP_LNAME,'')) As EMPNAME," & _
            '                " BM_INCIDENT_TYPE,BM_CATEGORY_SCORE as BM_SCORE,T.BM_CATEGORYNAME,V.BM_STU_ID,S.STU_ID," & _
            '                " S.STU_GRD_ID,(ISNULL(S.STU_FIRSTNAME,'') + ' ' + ISNULL(STU_MIDNAME,'')+ ' ' + ISNULL(STU_LASTNAME,'')) As StudName FROM BM.BM_MASTER A " & _
            '                " INNER JOIN  EMPLOYEE_M B ON A.BM_REPORTING_STAFF_ID = B.EMP_ID  " & _
            '                " INNER JOIN  BM.BM_STUDENTSINVOLVED V ON V.BM_ID=A.BM_ID  " & _
            '                " INNER JOIN  STUDENT_M S ON S.STU_ID=V.BM_STU_ID" & _
            '                " INNER JOIN BM.BM_CATEGORY T ON T.BM_CATEGORYID=A.BM_CATEGORYID " & _
            '                " WHERE isNULL(BM_CONFIDENTIAL,0) =0 AND S.STU_ID =" & stu_id & "  AND BM_CATEGORYHRID=2 ORDER BY BM_ENTRY_DATE DESC"


            'Dim dsDetails As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            gvStudGrade_POS.DataSource = dsDetails

            If dsDetails.Tables(0).Rows.Count = 0 Then
                dsDetails.Tables(0).Rows.Add(dsDetails.Tables(0).NewRow())
                gvStudGrade_POS.DataBind()
                Dim columnCount As Integer = gvStudGrade_POS.Rows(0).Cells.Count
                gvStudGrade_POS.Rows(0).Cells.Clear()
                gvStudGrade_POS.Rows(0).Cells.Add(New TableCell)
                gvStudGrade_POS.Rows(0).Cells(0).ColumnSpan = columnCount
                gvStudGrade_POS.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvStudGrade_POS.Rows(0).Cells(0).Text = "No record available."

            Else
                gvStudGrade_POS.DataBind()
                'For Each GrdvRow As GridViewRow In gvStudGrade_POS.Rows
                '    If GrdvRow.RowType = DataControlRowType.DataRow Then
                '        DirectCast(GrdvRow.FindControl("lblStudentNo"), Label).Text = Format(CDate(DirectCast(GrdvRow.FindControl("lblStudentNo"), Label).Text), "dd-MMM-yyyy")
                '        'If DirectCast(GrdvRow.FindControl("lblShift"), Label).Text = "FI" Then
                '        '    DirectCast(GrdvRow.FindControl("lblShift"), Label).Text = "For Information"
                '        '    DirectCast(GrdvRow.FindControl("lblShift"), Label).ForeColor = Drawing.Color.Blue
                '        '    GrdvRow.ForeColor = Drawing.Color.Blue
                '        'Else
                '        '    DirectCast(GrdvRow.FindControl("lblShift"), Label).Text = "For Action"
                '        '    DirectCast(GrdvRow.FindControl("lblShift"), Label).ForeColor = Drawing.Color.Red
                '        '    GrdvRow.ForeColor = Drawing.Color.Red
                '        'End If
                '    End If
                'Next
            End If

        Catch ex As Exception

        End Try

    End Sub


    Private Sub BindGrid_ACHV(ByVal stu_id As String)
        Try
            Dim str_conn = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            Dim dsDetails As DataSet
            Dim PARAM(2) As SqlParameter
            PARAM(0) = New SqlParameter("@STU_ID", stu_id)
            PARAM(1) = New SqlParameter("@CAT_ID", 1)
            '' dsDetails = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "GET_ACHIEVEMENT_BEHAVIOUR", PARAM)
            dsDetails = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "BM.GET_ACHIEVEMENT_BEHAVIOUR", PARAM)
            'Dim str_query = " SELECT A.BM_ID,A.BM_ENTRY_DATE,(ISNULL(EMP_FNAME,'') + ' ' + ISNULL(EMP_LNAME,'')) As EMPNAME," & _
            '                " BM_INCIDENT_TYPE,BM_CATEGORY_SCORE as BM_SCORE,T.BM_CATEGORYNAME,V.BM_STU_ID,S.STU_ID," & _
            '                " S.STU_GRD_ID,(ISNULL(S.STU_FIRSTNAME,'') + ' ' + ISNULL(STU_MIDNAME,'')+ ' ' + ISNULL(STU_LASTNAME,'')) As StudName,BM_REPORT_ON_INCIDENT FROM BM.BM_MASTER A " & _
            '                " INNER JOIN  EMPLOYEE_M B ON A.BM_REPORTING_STAFF_ID = B.EMP_ID  " & _
            '                " INNER JOIN  BM.BM_STUDENTSINVOLVED V ON V.BM_ID=A.BM_ID  " & _
            '                " INNER JOIN  STUDENT_M S ON S.STU_ID=V.BM_STU_ID" & _
            '                " INNER JOIN BM.BM_CATEGORY T ON T.BM_CATEGORYID=A.BM_CATEGORYID " & _
            '                " WHERE isNULL(BM_CONFIDENTIAL,0) =0 AND S.STU_ID =" & stu_id & "  AND BM_CATEGORYHRID=1 ORDER BY BM_ENTRY_DATE DESC"


            'Dim dsDetails As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            gvStudGrade_ACHV.DataSource = dsDetails

            If dsDetails.Tables(0).Rows.Count = 0 Then
                dsDetails.Tables(0).Rows.Add(dsDetails.Tables(0).NewRow())
                gvStudGrade_ACHV.DataBind()
                Dim columnCount As Integer = gvStudGrade_ACHV.Rows(0).Cells.Count
                gvStudGrade_ACHV.Rows(0).Cells.Clear()
                gvStudGrade_ACHV.Rows(0).Cells.Add(New TableCell)
                gvStudGrade_ACHV.Rows(0).Cells(0).ColumnSpan = columnCount
                gvStudGrade_ACHV.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvStudGrade_ACHV.Rows(0).Cells(0).Text = "No record available."

            Else
                gvStudGrade_ACHV.DataBind()
                'For Each GrdvRow As GridViewRow In gvStudGrade_ACHV.Rows
                '    If GrdvRow.RowType = DataControlRowType.DataRow Then
                '        DirectCast(GrdvRow.FindControl("lblStudentNo"), Label).Text = Format(CDate(DirectCast(GrdvRow.FindControl("lblStudentNo"), Label).Text), "dd-MMM-yyyy")
                '        'If DirectCast(GrdvRow.FindControl("lblShift"), Label).Text = "FI" Then
                '        '    ' DirectCast(GrdvRow.FindControl("lblShift"), Label).Text = "For Information"
                '        '    ' DirectCast(GrdvRow.FindControl("lblShift"), Label).ForeColor = Drawing.Color.Blue
                '        '    'GrdvRow.ForeColor = Drawing.Color.Blue
                '        'Else
                '        '    ' DirectCast(GrdvRow.FindControl("lblShift"), Label).Text = "For Action"
                '        '    'DirectCast(GrdvRow.FindControl("lblShift"), Label).ForeColor = Drawing.Color.Red
                '        '    GrdvRow.ForeColor = Drawing.Color.Red
                '        'End If
                '    End If
                'Next
            End If

        Catch ex As Exception

        End Try

    End Sub


    Protected Sub GrdUserTransaction_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GrdUserTransaction.PageIndexChanging
        GrdUserTransaction.PageIndex = e.NewPageIndex
        BindTransactions(Session("STU_ID"))

    End Sub

    Protected Sub GridDue_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridDue.PageIndexChanging
        GridReservations.PageIndex = e.NewPageIndex
        BindItemDue(Session("STU_ID"))
    End Sub

    Protected Sub GridReservations_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridReservations.PageIndexChanging
        GridReservations.PageIndex = e.NewPageIndex
        BindReservations(Session("STU_ID"))
    End Sub

    Protected Sub GridMSMS_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        GridMSMS.PageIndex = e.NewPageIndex
        BindGrid(Session("STU_ID"))
    End Sub


    Protected Sub GridEmails_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridEmails.PageIndexChanging
        GridEmails.PageIndex = e.NewPageIndex
        BindGridEmail(Session("STU_ID"))
    End Sub


    Protected Sub gvSubjects_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvSubjects.PageIndexChanging
        gvSubjects.PageIndex = e.NewPageIndex
        BindSubjectList(Session("STU_ID"))
    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            If Session("username") Is Nothing Then
                Session("Active_tab") = ""
                Session("Site_Path") = ""
                Response.Redirect("~\Login.aspx")
            ElseIf Session("bPasswdChanged") = "False" Then
                Response.Redirect("~\UpdateInfo\ForcePasswordChange.aspx", False)
            ElseIf Session("bUpdateContactDetails") = "False" Then
                Response.Redirect(Session("ForceUpdate_stud"), False)
            End If
            lbChildName.Text = (Session("STU_NAME"))
            studentdetails(Session("STU_ID"))
            binddetails(Session("STU_ID"))
            GridBindFeeDetails(Session("STU_ID"))
            GridBindPayHist(Session("STU_ID"))
            BindSubjectList(Session("STU_ID"))
            bindMain_details(Session("STU_ID"))

            BindMemerships(Session("STU_ID"))
            BindTransactions(Session("STU_ID"))
            BindItemDue(Session("STU_ID"))
            BindReservations(Session("STU_ID"))
            ''commented by nahyan on 27nov2018
            'BindGrid_neg(Session("STU_ID")) ' Negative
            'BindGrid_POS(Session("STU_ID")) ' Psoitive
            BindGrid_ACHV(Session("STU_ID"))
            BindGrid(Session("STU_ID"))
            BindGridEmail(Session("STU_ID"))

            bindAttChart(Session("STU_ID"))
            bindABSChart(Session("STU_ID"))
            bindLATEChart(Session("STU_ID"))

            ReadShowPastoral()
            If BshowPastoral <> 3 Then
                BindGrid_neg(Session("STU_ID")) ' Negative
                BindGrid_POS(Session("STU_ID")) ' Psoitive
                pbd2.Visible = True
                nbd2.Visible = True

            Else
                pbd2.Visible = False
                nbd2.Visible = False

            End If

            trShift.Visible = False
            trMins.Visible = False
            ViewState("SMS_row") = "FALSE"
        Catch ex As Exception

        End Try

        If (Page.IsPostBack) Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), "MyFun1", "KeepStyle()", True)
        End If
    End Sub

    Private Sub ReadShowPastoral()
        Dim connectionstring As String = ConnectionManger.GetOASISConnectionString
        Dim SqlText As String
        SqlText = " select isnull(BSU_bshowPastoral,0)BSU_bshowPastoral,*from oasis..businessunit_m inner join oasis..student_m  with(nolock) on stu_bsu_id=bsu_id where STU_id='" & Session("STU_ID") & "'"

        Dim connection As New SqlConnection(connectionstring)
        Dim command As New SqlCommand(SqlText, connection)
        'open the database and get a datareader
        connection.Open()
        Dim dr As SqlDataReader = command.ExecuteReader()

        If dr.Read() Then
            BshowPastoral = dr("BSU_bshowPastoral")
        End If
        connection.Close()

    End Sub
End Class
