﻿<%@ Page Language="VB" MasterPageFile="~/ParentMaster.master" AutoEventWireup="false" CodeFile="ProfCurr.aspx.vb" 
Inherits="Stud_Prof_ProfCurr" title="GEMS EDUCATION" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphParent" Runat="Server">
 <div class="mainheading">
      <div class="left">Curriculum</div>
       <div class="right"><asp:label ID="lbChildName" runat="server"  CssClass="lblChildNameCss" >
       </asp:label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
      </div>
    <div>   <table  width="100%" cellpadding="8" cellspacing="0" class="BlueTable_simple">
                  
                      <tr class="tdblankAll" >
                      <td  class="tdblankAll" >
    <asp:GridView ID="gvSubjects"  runat="server" AllowPaging="True" AutoGenerateColumns="False"
             EmptyDataText="No records available." 
             Width="100%"  CssClass="gridView" BorderStyle="None" BorderWidth="0px" 
             HeaderStyle-HorizontalAlign="Center">
             <EmptyDataRowStyle  Wrap="True" HorizontalAlign="Center" />
                            <Columns>
                              <asp:TemplateField HeaderText="Subject">
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                    <ItemStyle Width="300px" />
                    <ItemTemplate>
                      <asp:Label ID="lblSubject"  runat="server" Text='<%# Bind("SBG_DESCR") %>'></asp:Label> 
                    </ItemTemplate>
                </asp:TemplateField>
                 <asp:TemplateField  HeaderText="Group" Visible="False">
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                    <ItemStyle Width="300px" />
                    <ItemTemplate>
                  <asp:Label ID="lblGroup"  runat="server" Text='<%# Bind("SGR_DESCR") %>'></asp:Label>     
                    </ItemTemplate>
                    </asp:TemplateField>

                        <asp:TemplateField HeaderText="Tutor">
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                    <ItemStyle Width="300px" />
                    <ItemTemplate>
                 <asp:Label ID="lblTutor"  runat="server" Text='<%# Bind("TEACHERS") %>'></asp:Label>     
                    </ItemTemplate>
                    </asp:TemplateField>
                    </Columns>
                     <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
          <PagerStyle CssClass="gridpager" />   
                           
                        </asp:GridView>
    </td></tr></table>
       <asp:Label ID="lblerror" runat="server" CssClass="error"></asp:Label><br />
        <asp:HiddenField ID="HF_stuid" runat="server" /><asp:HiddenField ID="hfACD_ID" runat="server" />
        <asp:HiddenField ID="hfGRD_ID" runat="server" />
        <asp:HiddenField ID="hfCBSESchool" runat="server" />
    <cr:crystalreportsource id="rs" runat="server" cacheduration="1">
    </cr:crystalreportsource>
      </div>
      
</asp:Content>

