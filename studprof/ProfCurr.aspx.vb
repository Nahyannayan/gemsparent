﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Collections.Generic
Imports System.Collections
Imports System.Xml
Imports System.Data.SqlTypes
Partial Class Stud_Prof_ProfCurr
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If Session("username") Is Nothing Then
                Session("Active_tab") = "Home"
                Session("Site_Path") = ""
                Response.Redirect("~\login.aspx")
            ElseIf Session("bPasswdChanged") = "False" Then
                Response.Redirect("~\UpdateInfo\ForcePasswordChange.aspx", False)
            ElseIf Session("bUpdateContactDetails") = "False" Then
                Response.Redirect(Session("ForceUpdate_stud"), False)
            End If




            HF_stuid.Value = Session("STU_ID")
            hfACD_ID.Value = Session("STU_ACD_ID")
            lbChildName.Text = Session("STU_NAME")
            checkCBSESchool()
            BindSubjectList()
            '  BindProgressReport()
            getStudentGrade()

            'If Session("sbsuid") = "125005" Then

            'Else
            '    tblPer.Visible = False
            'End If

            'If hfCBSESchool.Value = "1" And hfGRD_ID.Value = "11" Or hfGRD_ID.Value = "12" Then
            '    tblPer.Visible = False
            'Else
            '    BindReports()
            'End If

        Catch ex As Exception
        End Try
    End Sub

    Sub checkCBSESchool()
        Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString
        Dim str_query As String = "SELECT COUNT(ACD_ID) FROM VW_ACADEMICYEAR_D WHERE ACD_CLM_ID=1 AND ACD_ID='" + Session("STU_ACD_ID") + "'"

        Dim count As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        If count > 0 Then
            hfCBSESchool.Value = "1"
        Else
            hfCBSESchool.Value = "0"
        End If
    End Sub
    'Sub BindReports()
    '    Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString


    '    Dim STR_QUERY As String

    '    If hfCBSESchool.Value = "1" Then
    '        STR_QUERY = "EXEC [RPT].[GETREPORTCARDBYYEAR_CBSE] " _
    '                      & "'" + Session("STU_PROF_BSU_ID") + "'," _
    '                      & "'" + hfGRD_ID.Value + "'," _
    '                      & Session("STU_PROF_CLM_ID").ToString
    '    Else
    '        STR_QUERY = "EXEC RPT.GETREPORTCARDBYYEAR " _
    '                       & "'" + Session("STU_PROF_BSU_ID") + "'," _
    '                       & "'" + hfGRD_ID.Value + "'," _
    '                       & Session("STU_PROF_CLM_ID").ToString
    '    End If

    '    Dim reader As SqlDataReader
    '    reader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, STR_QUERY)
    '    Dim sq As New SqlString
    '    Dim xmlData As New XmlDocument
    '    Dim str As String
    '    While reader.Read
    '        str += reader.GetString(0)
    '    End While
    '    Dim xl As New SqlString
    '    str = str.Replace("ACY_ID", "ID")
    '    str = str.Replace("ACY_DESCR", "TEXT")
    '    str = str.Replace("RPF_ID", "ID")
    '    str = str.Replace("RPF_DESCR", "TEXT")

    '    xl = "<root>" + str + "</root>"
    '    Dim xmlReader As New XmlTextReader(New StringReader(xl))
    '    reader.Close()

    '    tvReport.Nodes.Clear()
    '    xmlData.Load(xmlReader)
    '    tvReport.Nodes.Add(New TreeNode(xmlData.DocumentElement.GetAttribute("TEXT"), xmlData.DocumentElement.GetAttribute("ID"), "", "javascript:void(0)", "_self"))

    '    Dim tnode As TreeNode
    '    tnode = tvReport.Nodes(0)
    '    AddNode(xmlData.DocumentElement, tnode)


    '    tvReport.ExpandAll()

    'End Sub

    Private Sub AddNode(ByRef inXmlNode As XmlNode, ByRef inTreeNode As TreeNode)
        Dim xNode As XmlNode
        Dim tNode As TreeNode
        Dim nodeList As XmlNodeList
        Dim i As Long
        If inXmlNode.HasChildNodes() Then
            nodeList = inXmlNode.ChildNodes
            For i = 0 To nodeList.Count - 1
                xNode = inXmlNode.ChildNodes(i)
                Try
                    inTreeNode.ChildNodes.Add(New TreeNode(xNode.Attributes("TEXT").Value, xNode.Attributes("ID").Value, "", "javascript:void(0)", "_self"))
                    tNode = inTreeNode.ChildNodes(i)
                    If xNode.HasChildNodes Then
                        AddNode(xNode, tNode)
                    End If
                Catch ex As Exception
                End Try
            Next
        Else
            Try
                inTreeNode.ChildNodes.Add(New TreeNode(inXmlNode.Attributes("TEXT").Value, inXmlNode.Attributes("ID").Value, "", "javascript:void(0)", "_self"))
            Catch ex As Exception
            End Try
        End If
    End Sub

    'Function getReportCards() As String
    '    Dim str As String = ""
    '    Dim i As Integer

    '    For i = 0 To lstReportSchedule.Items.Count - 1
    '        If lstReportSchedule.Items(i).Selected = True Then
    '            If str <> "" Then
    '                str += "|"
    '            End If
    '            str += lstReportSchedule.Items(i).Text
    '        End If
    '    Next

    '    Return str
    'End Function
    'Function getReportCards() As String
    '    Dim node As TreeNode
    '    Dim cNode As TreeNode
    '    Dim ccNode As TreeNode

    '    Dim strRPF As String = ""

    '    Dim bRSM As Boolean = False

    '    For Each node In tvReport.Nodes
    '        For Each cNode In node.ChildNodes
    '            For Each ccNode In cNode.ChildNodes
    '                If ccNode.Checked = True Then
    '                    If strRPF <> "" Then
    '                        strRPF += "|"
    '                    End If
    '                    strRPF += cNode.Text + "_" + ccNode.Text
    '                End If
    '            Next
    '        Next
    '    Next


    '    Return strRPF.Trim
    'End Function
    'Sub CallReport()
    '    Dim param As New Hashtable
    '    param.Add("@IMG_BSU_ID", Session("SBSUID"))
    '    param.Add("@IMG_TYPE", "LOGO")



    '    param.Add("@BSU_ID", Session("SBSUID"))
    '    param.Add("@RPF_DESCR", getReportCards())
    '    param.Add("@STM_ID", "1")
    '    param.Add("@STU_ID", HF_stuid.Value)

    '    Dim rptClass As New rptClass
    '    With rptClass
    '        .crDatabase = "oasis_curriculum"

    '        If hfCBSESchool.Value = "1" Then
    '            .reportPath = Server.MapPath("../../Curriculum/Reports/Rpt/rptStudentPerformanceAcrossYear_CBSE.rpt")
    '        Else
    '            param.Add("UserName", Session("sUsr_name"))
    '            param.Add("CurrentDate", Format(Now.Date, "dd-MMM-yyyy"))
    '            param.Add("accYear", hfACD_ID.Value)
    '            param.Add("grade", hfGRD_ID.Value)
    '            .reportPath = Server.MapPath("../../Curriculum/Reports/Rpt/rptINTLStudentPerformanceAcrossYear.rpt")
    '        End If
    '        .reportParameters = param
    '    End With
    '    Session("rptClass") = rptClass
    '    ' Response.Redirect("~/Reports/ASPX Report/rptReportViewer_princi.aspx")
    '    'Dim rptDownload As New ReportDownload
    '    'rptDownload.LoadReports(rptClass, rs)
    '    'rptDownload = Nothing

    'End Sub
    Sub getStudentGrade()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT STU_GRD_ID FROM STUDENT_M WHERE STU_ID='" + HF_stuid.Value + "'"
        hfGRD_ID.Value = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
    End Sub

    Sub BindSubjectList()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString


        Dim str_query As String = " SELECT DISTINCT SBG_DESCR,SGR_DESCR, TEACHERS=ISNULL((SELECT " _
                               & "  STUFF((SELECT ', '+ ISNULL(EMP_FNAME,'')+' '+ISNULL(EMP_MNAME,' ')+' '+ " _
                               & " ISNULL(EMP_LNAME,'') FROM EMPLOYEE_M AS K INNER JOIN  GROUPS_TEACHER_S " _
                               & " AS L ON K.EMP_ID=L.SGS_EMP_ID   WHERE SGS_SGR_ID = C.SGR_ID" _
                               & " ORDER BY EMP_FNAME,EMP_MNAME,EMP_LNAME for xml path('')),1,1,'')),'')" _
                               & " FROM  STUDENT_GROUPS_S AS A INNER JOIN" _
                               & " SUBJECTS_GRADE_S AS B ON A.SSD_SBG_ID=B.SBG_ID" _
                               & " LEFT OUTER JOIN GROUPS_M AS C ON A.SSD_SGR_ID=C.SGR_ID" _
                               & " WHERE SSD_STU_ID=" + HF_stuid.Value + " AND SSD_ACD_ID='" + Session("STU_ACD_ID") + "' ORDER BY SBG_DESCR"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        If ds.Tables(0).Rows.Count = 0 Then
            ds.Tables(0).Rows.Add(ds.Tables(0).NewRow())
            ds.Tables(0).Rows(0)("SBG_DESCR") = ""
            ds.Tables(0).Rows(0)("SGR_DESCR") = ""
            ds.Tables(0).Rows(0)("TEACHERS") = ""
            gvSubjects.DataSource = ds
            gvSubjects.DataBind()
            Dim columnCount As Integer = gvSubjects.Rows(0).Cells.Count
            'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
            gvSubjects.Rows(0).Cells.Clear()
            gvSubjects.Rows(0).Cells.Add(New TableCell)
            gvSubjects.Rows(0).Cells(0).ColumnSpan = columnCount
            gvSubjects.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
            gvSubjects.Rows(0).Cells(0).Text = "No record available."
        Else
            gvSubjects.DataSource = ds
            gvSubjects.DataBind()
        End If

    End Sub

    Protected Sub gvSubjects_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvSubjects.PageIndexChanging
        gvSubjects.PageIndex = e.NewPageIndex
        BindSubjectList()
    End Sub
End Class
