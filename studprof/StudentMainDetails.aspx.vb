﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Collections.Generic
Imports System.Collections
Partial Class StudProf_StudentMainDetails
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            ''  Page.Title = OASISConstants.Gemstitle
            ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(btnSave)

            Try
                If Page.IsPostBack = False Then
                    If Session("username") Is Nothing Then
                        Session("Active_tab") = "Home"
                        Session("Site_Path") = ""
                        Response.Redirect("~\Login.aspx")
                    ElseIf Session("bPasswdChanged") = "False" Then
                        Response.Redirect("~\UpdateInfo\ForcePasswordChange.aspx", False)
                        'ElseIf Session("bUpdateContactDetails") = "False" Then
                        '    'commented by nahyan on 4thmay 2014...
                        '    'Response.Redirect(Session("ForceUpdate_stud"), False)
                        '    'commentend s here by nahyan on 4thmay 2014...
                        '    Response.Redirect("~\StudProf\StudentMainDetails.aspx", False)
                    End If
                    'Dim lnkMenu As LinkButton = DirectCast(contactDetails1.FindControl("lbtnStep1"), LinkButton)
                    'lnkMenu.Attributes.Add("class", "cssStepBtnActive")

                    GetReligion_info()
                    Call GetCountry_info()
                    GetNational_info()
                    bindLanguage_dropDown()
                    studentdetails(Session("STU_ID"))

                    ''check if the bsu having upload access.

                    

                    Call Student_M_Details(Session("STU_ID"))
                    student_documents_style(Session("STU_ID"))
                    lbChildName.Text = Session("STU_NAME")
                    lbChildNameTop.Text = Session("STU_NAME")
                    'trShift.Visible = False
                    'trMins.Visible = False
                End If
            Catch ex As Exception
                UtilityObj.Errorlog(ex.Message)
            End Try

        Catch ex As Exception

            End Try


    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim cssclass As String = String.Empty
        Dim msg As String = String.Empty
        Dim status As Integer
        Dim transaction As SqlTransaction
        'Dim lblmsg As Label = CType(Page.Master.FindControl("lblSiteMessage"), Label)
        lblmsg.Text = ""
        UpLoadPhoto()
        Try

            Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                transaction = conn.BeginTransaction("SampleTransaction")
                status = callTrans_Student_M(transaction)

                If status = 1000 Then
                    cssclass = "alert-warning alert"
                    msg = "Please upload photo"
                    transaction.Rollback()
                ElseIf status <> 0 Then
                    cssclass = "alert-danger alert"
                    msg = "Photo upload failed"
                    transaction.Rollback()
                Else
                   
                    cssclass = "alert-info alert"
                    msg = "Photo saved successfully"
                    transaction.Commit()
                End If

            End Using


            Call Student_M_Details(Session("STU_ID"))
            divUploadmsg.CssClass = ""
            divUploadmsg.Text = ""
            lblmsg.CssClass = cssclass
            lblmsg.Text = msg

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)

        End Try
    End Sub

    Protected Sub btnSaveContinue_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim status As Integer
        Dim transaction As SqlTransaction
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            status = callTrans_Student_M_Details(transaction)
            '  Dim lblmsg As Label = CType(Page.Master.FindControl("lblSiteMessage"), Label)
            lblmsg.Text = ""

            If status = 1000 Then
                lblmsg.CssClass = "alert-warning alert"
                lblmsg.Text = "Please enter the details"
                transaction.Rollback()
            ElseIf status <> 0 Then
                lblmsg.CssClass = "alert-warning alert"
                lblmsg.Text = "Please enter the details"
                transaction.Rollback()
            Else
                divUploadmsg.CssClass = ""
                divUploadmsg.Text = ""
                lblmsg.CssClass = "alert-warning alert"
                lblmsg.Text = "Data saved successfully"
                Session("bStuSteps") = 1
                UtilityObj.InsertAuditdetails(transaction, "edit", "STUDENT_M", "STU_ID", "STU_ID", "STU_ID=" + Session("STU_ID").ToString)
                transaction.Commit()
                Response.Redirect("~\StudProf\ParentDetails.aspx", False)
            End If

        End Using


    End Sub
    Sub student_documents_style(ByVal stu_id As String)
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim param(10) As SqlClient.SqlParameter
        param(0) = New SqlClient.SqlParameter("@stu_id", stu_id)
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "online.get_exp_document", param)
        Dim passport As Integer = ds.Tables(0).Rows(0).Item("passport")
        Dim visa As Integer = ds.Tables(0).Rows(0).Item("visa")
        Dim eid As Integer = ds.Tables(0).Rows(0).Item("emiratesid")

        If passport = 1 Then
            txtPExpDate.CssClass = "form-control blink"
        Else
            txtPExpDate.CssClass = "form-control"
        End If
        If visa = 1 Then
            txtVExpDate.CssClass = "form-control blink"
        Else
            txtVExpDate.CssClass = "form-control"
        End If
        If eid = 1 Then
            txtEmiratesIDExp.CssClass = "form-control blink"
        Else
            txtEmiratesIDExp.CssClass = "form-control"
        End If

    End Sub

    Sub studentdetails(ByVal stu_id As String)
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim param(10) As SqlClient.SqlParameter
            Dim temp_emgMOBILE As String
            Dim tempStuMobile As String
            Dim arInfo As String() = New String(2) {}
            Dim splitter As Char = "-"
            param(0) = New SqlClient.SqlParameter("@stu_id", stu_id)
            Using readerStudent_Detail As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "student_Details_FOR_GWA", param)

                If readerStudent_Detail.HasRows = True Then
                    While readerStudent_Detail.Read
                        'handle the null value returned from the reader incase  convert.tostring
                        lblChildName.Text = Session("STU_NAME")
                        txtStudentNamePP.Text = Convert.ToString(readerStudent_Detail("SPASPRTNAME"))
                        txtStudentNameGiven.Text = Convert.ToString(readerStudent_Detail("STU_KNOWNNAME"))
                        txtEmiratesID.Text = Convert.ToString(readerStudent_Detail("STU_EMIRATES_ID"))
                        txtPremisesId.Text = Convert.ToString(readerStudent_Detail("stu_PremisesID"))
                        'txtEmergencyContact.Text = Convert.ToString(readerStudent_Detail("SEMGCONTACT"))

                        
                        temp_emgMOBILE = Convert.ToString(readerStudent_Detail("SEMGCONTACT"))
                        arInfo = temp_emgMOBILE.Split(splitter)
                        If arInfo.Length > 2 Then
                            txtEmgMobile_Country.Text = arInfo(0)
                            txtEmgMobile_Area.Text = arInfo(1)
                            txtEmgMobile_No.Text = arInfo(2)
                        ElseIf arInfo.Length = 2 Then
                            txtEmgMobile_Area.Text = arInfo(0)
                            txtEmgMobile_No.Text = arInfo(1)

                        Else
                            txtEmgMobile_No.Text = temp_emgMOBILE
                        End If


                        'LTpob.Text = Convert.ToString(readerStudent_Detail("STU_POB"))

                        If Not ddlCountry_Birth.Items.FindByText(Convert.ToString(readerStudent_Detail("COB"))) Is Nothing Then
                            ddlCountry_Birth.ClearSelection()
                            ddlCountry_Birth.Items.FindByText(Convert.ToString(readerStudent_Detail("COB"))).Selected = True
                        End If
                        lblGradeSection.Text = Convert.ToString(readerStudent_Detail("GRD_ID_JOIN")) & "-" & Convert.ToString(readerStudent_Detail("SCT_DESCR_JOIN"))

                        lblFeeID.Text = Convert.ToString(readerStudent_Detail("FEE_ID"))
                        txtPPNo.Text = Convert.ToString(readerStudent_Detail("SPASPRTNO"))
                        txtPPIssuPlace.Text = Convert.ToString(readerStudent_Detail("SPASPRTISSPLACE"))
                        txtVsaNo.Text = Convert.ToString(readerStudent_Detail("STU_VISANO"))
                        txtVIssPlace.Text = Convert.ToString(readerStudent_Detail("STU_VISAISSPLACE"))
                        txtVIssAuth.Text = Convert.ToString(readerStudent_Detail("STU_VISAISSAUTH"))
                       
                        If Not ddlNationality_Birth.Items.FindByText(Convert.ToString(readerStudent_Detail("SNATIONALITY"))) Is Nothing Then
                            ddlNationality_Birth.ClearSelection()
                            ddlNationality_Birth.Items.FindByText(Convert.ToString(readerStudent_Detail("SNATIONALITY"))).Selected = True
                        End If

                        If Not ddlNationality_Birth1.Items.FindByText(Convert.ToString(readerStudent_Detail("SNATIONALITY2"))) Is Nothing Then
                            ddlNationality_Birth1.ClearSelection()
                            ddlNationality_Birth1.Items.FindByText(Convert.ToString(readerStudent_Detail("SNATIONALITY2"))).Selected = True
                        End If
                        txtStuPersonalEmail.Text = Convert.ToString(readerStudent_Detail("STU_EMAIL"))

                        tempStuMobile = Convert.ToString(readerStudent_Detail("STU_MOBILE"))
                        arInfo = tempStuMobile.Split(splitter)
                        If arInfo.Length > 2 Then
                            txtStuMobile_Country.Text = arInfo(0)
                            txtStuMobile_Area.Text = arInfo(1)
                            txtStuMobile_No.Text = arInfo(2)
                        ElseIf arInfo.Length = 2 Then
                            txtStuMobile_Area.Text = arInfo(0)
                            txtStuMobile_No.Text = arInfo(1)

                        Else
                            txtStuMobile_No.Text = tempStuMobile
                        End If


                        If Not ddlReligion.Items.FindByValue(Convert.ToString(readerStudent_Detail("RLG_ID"))) Is Nothing Then
                            ddlReligion.ClearSelection()
                            ddlReligion.Items.FindByValue(Convert.ToString(readerStudent_Detail("RLG_ID"))).Selected = True
                        End If
                        ViewState("temp_COB") = Convert.ToString(readerStudent_Detail("COB"))
                        ViewState("temp_HOUSE") = Convert.ToString(readerStudent_Detail("HOUSE"))
                        ViewState("temp_PrefContact") = Convert.ToString(readerStudent_Detail("STU_PREFCONTACT"))
                        ViewState("temp_Blood") = Convert.ToString(readerStudent_Detail("STU_BLOODGROUP"))
                        ViewState("ACD_ID") = Convert.ToString(readerStudent_Detail("ACD_ID"))
                        'Setting date
                        If IsDate(readerStudent_Detail("SDOJ")) = True Then
                            lblDateOfJoin.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((readerStudent_Detail("SDOJ"))))
                        End If

                        If IsDate(readerStudent_Detail("MINDOJ")) = True Then
                            'txtMINDOJ.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((readerStudent_Detail("MINDOJ"))))
                        End If

                        If IsDate(readerStudent_Detail("DOB")) = True Then
                            txtDob.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((readerStudent_Detail("DOB"))))
                        End If

                        If IsDate(readerStudent_Detail("STU_PASPRTISSDATE")) = True Then
                            txtPIssDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((readerStudent_Detail("STU_PASPRTISSDATE"))))
                        End If
                        If IsDate(readerStudent_Detail("STU_PASPRTEXPDATE")) = True Then
                            txtPExpDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((readerStudent_Detail("STU_PASPRTEXPDATE"))))
                        End If

                        If IsDate(readerStudent_Detail("STU_VISAISSDATE")) = True Then
                            txtVIssDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((readerStudent_Detail("STU_VISAISSDATE")))).Replace("01/Jan/1900", "")
                        End If
                        If IsDate(readerStudent_Detail("STU_VISAEXPDATE")) = True Then
                            txtVExpDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((readerStudent_Detail("STU_VISAEXPDATE")))).Replace("01/Jan/1900", "")
                        End If
                        If IsDate(readerStudent_Detail("STU_APPLEMIRATES_ID_EXPDATE")) = True Then
                            txtEmiratesIDExp.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((readerStudent_Detail("STU_APPLEMIRATES_ID_EXPDATE")))).Replace("01/Jan/1900", "")
                        End If


                        'If Convert.ToBoolean(readerStudent_Detail("BSU_bUploadStud_photo")) Then
                        '    fileUpload.Visible = True
                        '    divUploadmsg.Visible = True
                        '    spnUploadMsg.Visible = True
                        '    trUploadBtn.Visible = True
                        'Else
                        fileUpload.Visible = False
                        divUploadmsg.Visible = False
                        spnUploadMsg.Visible = False
                        trUploadBtn.Visible = False
                        ' End If

                        'Dim temp_Gender As String
                        'temp_Gender = Convert.ToString(readerStudent_Detail("GENDER"))
                        'If UCase(temp_Gender) = "F" Then
                        '    txtGender.Text = "Female"
                        'ElseIf UCase(temp_Gender) = "M" Then
                        '    txtGender.Text = "Male"
                        'End If
                        RBLgender.SelectedValue = Convert.ToString(readerStudent_Detail("GENDER"))
                        If Not ddlFLang.Items.FindByText(Convert.ToString(readerStudent_Detail("STU_FIRSTLANG"))) Is Nothing Then
                            ddlFLang.ClearSelection()
                            ddlFLang.Items.FindByText(Convert.ToString(readerStudent_Detail("STU_FIRSTLANG"))).Selected = True
                        End If

                        Dim arInfoLang() As String
                        Dim splitterLang As Char = ","
                        Dim new1 As String = String.Empty
                        arInfoLang = Convert.ToString(readerStudent_Detail("STU_OTHLANG")).Trim.Replace("''", "'").Split(splitterLang)
                        If arInfoLang.Length > 0 Then
                            For i As Integer = 0 To arInfoLang.Length - 1
                                If arInfoLang(i).Trim <> "" Then
                                    If Not chkOLang.Items.FindByText(arInfoLang(i)).Selected = True Then
                                        chkOLang.Items.FindByText(arInfoLang(i)).Selected = True
                                    End If
                                End If
                            Next
                        End If


                    End While
                Else
                End If
            End Using
        Catch ex As Exception
            Dim error1 As String = ex.Message
        End Try
    End Sub

    Function callTrans_Student_M_Details(ByVal trans As SqlTransaction) As Integer
        Try
            Dim Status As Integer = 0
            Dim STU_ID As String = Session("STU_ID")
            Dim STU_BSU_ID As String = Session("sBsuid")
            Dim strGivenName As String = txtStudentNameGiven.Text
            Dim gender As String = RBLgender.SelectedValue
            Dim strReligion As String = ddlReligion.SelectedItem.Value
            Dim STU_DOB As String = txtDob.Text
            Dim STU_COB As String = ddlCountry_Birth.SelectedItem.Value
            Dim STU_NATIONALITY As String = ddlNationality_Birth.SelectedItem.Value
            Dim STU_NATIONALITY1 As String = ddlNationality_Birth1.SelectedItem.Value

            Dim STU_PASPRTNAME As String = txtStudentNamePP.Text
            Dim STU_PASPRTNO As String = txtPPNo.Text
            Dim STU_PASPRTISSPLACE As String = txtPPIssuPlace.Text
            Dim STU_PASPRTISSDATE As String = txtPIssDate.Text
            Dim STU_PASPRTEXPDATE As String = txtPExpDate.Text
            Dim STU_VISANO As String = txtVsaNo.Text
            Dim STU_VISAISSPLACE As String = txtVIssPlace.Text
            Dim STU_VISAISSDATE As String = txtVIssDate.Text
            Dim STU_VISAEXPDATE As String = txtVExpDate.Text
            Dim STU_VISAISSAUTH As String = txtVIssAuth.Text
            Dim STU_EmiratesID As String = txtEmiratesID.Text
            Dim STU_EM_ID_EXP_DATE As String = txtEmiratesIDExp.Text
            Dim PremisesID As String = txtPremisesId.Text


            Dim STU_FIRSTLANG As String = ddlFLang.SelectedValue
            Dim STU_OTHLANG As String = String.Empty

            For Each item As ListItem In chkOLang.Items
                If (item.Selected) Then

                    STU_OTHLANG = STU_OTHLANG + item.Value + "|"
                End If
            Next

            Dim EmerGenyNumber As String = txtEmgMobile_Country.Text & "-" & txtEmgMobile_Area.Text & "-" & txtEmgMobile_No.Text
            Dim Stu_email As String = txtStuPersonalEmail.Text
            Dim Stu_phone As String = txtStuMobile_Country.Text & "-" & txtStuMobile_Area.Text & "-" & txtStuMobile_No.Text

            Dim pParms(29) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@STU_ID", STU_ID)
            pParms(1) = New SqlClient.SqlParameter("@STU_BSU_ID", STU_BSU_ID)
            pParms(2) = New SqlClient.SqlParameter("@strGivenName", strGivenName)

            pParms(3) = New SqlClient.SqlParameter("@gender", gender)
            pParms(4) = New SqlClient.SqlParameter("@strReligion", strReligion)
            pParms(5) = New SqlClient.SqlParameter("@STU_DOB", STU_DOB)
            pParms(6) = New SqlClient.SqlParameter("@STU_COB", STU_COB)
            pParms(7) = New SqlClient.SqlParameter("@STU_NATIONALITY", STU_NATIONALITY)
            pParms(8) = New SqlClient.SqlParameter("@STU_NATIONALITY1", STU_NATIONALITY1)

            pParms(26) = New SqlClient.SqlParameter("@STU_PASPRTNAME", STU_PASPRTNAME)
            pParms(9) = New SqlClient.SqlParameter("@STU_PASPRTNO", STU_PASPRTNO)
            pParms(10) = New SqlClient.SqlParameter("@STU_PASPRTISSPLACE", STU_PASPRTISSPLACE)
            pParms(11) = New SqlClient.SqlParameter("@STU_PASPRTISSDATE", STU_PASPRTISSDATE)
            pParms(12) = New SqlClient.SqlParameter("@STU_PASPRTEXPDATE", STU_PASPRTEXPDATE)
            pParms(13) = New SqlClient.SqlParameter("@STU_VISANO", STU_VISANO)
            pParms(14) = New SqlClient.SqlParameter("@STU_VISAISSPLACE", STU_VISAISSPLACE)
            pParms(15) = New SqlClient.SqlParameter("@STU_VISAISSDATE", STU_VISAISSDATE)
            pParms(16) = New SqlClient.SqlParameter("@STU_VISAEXPDATE", STU_VISAEXPDATE)
            pParms(27) = New SqlClient.SqlParameter("@STU_VISAISSAUTH", STU_VISAISSAUTH)

            pParms(17) = New SqlClient.SqlParameter("@STU_EmiratesID", STU_EmiratesID)
            pParms(18) = New SqlClient.SqlParameter("@STU_EM_ID_EXP_DATE", STU_EM_ID_EXP_DATE)
            pParms(19) = New SqlClient.SqlParameter("@PremisesID", PremisesID)
            pParms(20) = New SqlClient.SqlParameter("@STU_FIRSTLANG", STU_FIRSTLANG)
            pParms(21) = New SqlClient.SqlParameter("@STU_OTHLANG", STU_OTHLANG)
            pParms(22) = New SqlClient.SqlParameter("@EmerGenyNumber", EmerGenyNumber)
            pParms(23) = New SqlClient.SqlParameter("@Stu_email", Stu_email)
            pParms(24) = New SqlClient.SqlParameter("@Stu_phones", Stu_phone)
            pParms(25) = New SqlClient.SqlParameter("@OLU_ID", Convert.ToInt32(Session("OLU_ID").ToString))
            pParms(28) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(28).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "Update_Student_M_GWA", pParms)
            Dim ReturnFlag As Integer = pParms(28).Value
            Return ReturnFlag
        Catch ex As Exception
            Return 1000
        End Try
    End Function

    Private Sub UpLoadPhoto()


        If fileUpload.PostedFile.ContentLength > 0 Then
            ViewState("EMPPHOTOFILEPATHoldPath") = ""
            ViewState("EMPPHOTOFILEPATH") = ""
            divUploadmsg.CssClass = ""
            divUploadmsg.Text = ""

            Try


                Dim intDocFileLength As Integer = fileUpload.PostedFile.ContentLength ' get the file size

                Dim strPostedFileName As String = fileUpload.PostedFile.FileName 'get the file name
                Dim filename As String = System.IO.Path.GetFileName(strPostedFileName).ToLower
                Dim strExtn As String = System.IO.Path.GetExtension(strPostedFileName).ToLower
                Dim Tempath As String = "\" & Session("STU_BSU_ID") & "\" & Session("STU_ID") & "\Temp\"
                Dim UploadfileName As String = "STUPHOTO" & strExtn
                Dim ConFigPath As String = WebConfigurationManager.ConnectionStrings("EmpFilepath").ConnectionString


                Dim SqlCon As SqlConnection = ConnectionManger.GetOASISConnection
                Dim ds As New DataSet
                Dim sqlQuery As String = "SELECT SYS_ImgPhotoSize FROM dbo.vw_OSF_SYSINFO_S"
                ds = SqlHelper.ExecuteDataset(SqlCon, CommandType.Text, sqlQuery)
                If ds.Tables(0).Rows.Count > 0 Then
                    Dim iImgSize As Integer = ds.Tables(0).Rows(0)("SYS_ImgPhotoSize")

                    If intDocFileLength > iImgSize Then
                        divUploadmsg.CssClass = "diverrorUpload"
                        divUploadmsg.Text = "Select image size maximum 20KB"
                        Exit Sub
                    End If
                End If

                If (strExtn <> ".jpg") Then 'exten type
                    divUploadmsg.CssClass = "diverrorUpload"
                    divUploadmsg.Text = "File type is different than allowed!"
                    Exit Sub
                End If

                If (strPostedFileName <> String.Empty) Then

                    If Not Directory.Exists(ConFigPath + Tempath) Then
                        Directory.CreateDirectory(ConFigPath + Tempath)
                    Else
                        Dim d As New DirectoryInfo(ConFigPath + Tempath)

                        Dim fi() As System.IO.FileInfo
                        fi = d.GetFiles("*.*", SearchOption.TopDirectoryOnly)
                        If fi.Length > 0 Then '' If Having Attachments
                            For Each f As System.IO.FileInfo In fi
                                f.Delete()
                            Next
                        End If

                    End If


                    fileUpload.PostedFile.SaveAs(ConFigPath + Tempath & System.IO.Path.GetFileName(UploadfileName))

                    ViewState("EMPPHOTOFILEPATHoldPath") = ConFigPath + Tempath & UploadfileName
                    ViewState("EMPPHOTOFILEPATH") = "/" & Session("STU_BSU_ID") & "/" & Session("STU_ID") & "/" & "STUPHOTO" & strExtn

                    ' hf_reload.Value = "1"
                    divUploadmsg.CssClass = "divvalidUpload"
                    divUploadmsg.Text = filename
                    hfUploadPath.Value = ""

                Else
                    ViewState("EMPPHOTOFILEPATHoldPath") = ""
                    ViewState("EMPPHOTOFILEPATH") = ""
                    divUploadmsg.CssClass = "diverrorUpload"
                    divUploadmsg.Text = "There is no file to upload."
                End If


            Catch ex As Exception
                ViewState("EMPPHOTOFILEPATHoldPath") = ""
                ViewState("EMPPHOTOFILEPATH") = ""
                UtilityObj.Errorlog(ex.Message, "btnProcess_Click")

                divUploadmsg.CssClass = "diverrorUpload"
                divUploadmsg.Text = "Error while updating the file!"
                imgParentImage.ImageUrl = Server.MapPath("~/ParentLogin/Images/no_image.gif")
                imgParentImage.AlternateText = "No Image found"
            End Try

        End If



    End Sub

    Sub GetCountry_info()
        Try
           

            ddlCountry_Birth.Items.Add(New ListItem("", ""))
          
            Using AllCountry_reader As SqlDataReader = AccessRoleUser.GetCountry()
                Dim di_Country As ListItem


                If AllCountry_reader.HasRows = True Then
                    While AllCountry_reader.Read
                        di_Country = New ListItem(AllCountry_reader("CTY_DESCR"), AllCountry_reader("CTY_ID"))

                        ddlCountry_Birth.Items.Add(New ListItem(AllCountry_reader("CTY_DESCR"), AllCountry_reader("CTY_ID")))
                                                
                    End While
                   


                End If
            End Using

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "GetCountry_info")
        End Try
    End Sub

    Sub GetNational_info()
        Try



            ddlNationality_Birth.Items.Add(New ListItem("", ""))
            ddlNationality_Birth1.Items.Add(New ListItem("", ""))

            Using AllCountry_reader As SqlDataReader = studClass.GetNational()
                Dim di_Country As ListItem


                If AllCountry_reader.HasRows = True Then
                    While AllCountry_reader.Read
                        di_Country = New ListItem(AllCountry_reader("CTY_NATIONALITY"), AllCountry_reader("CTY_ID"))
                        ddlNationality_Birth.Items.Add(New ListItem(AllCountry_reader("CTY_NATIONALITY"), AllCountry_reader("CTY_ID")))
                        ddlNationality_Birth1.Items.Add(New ListItem(AllCountry_reader("CTY_NATIONALITY"), AllCountry_reader("CTY_ID")))

                    End While



                End If
            End Using

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "GetCountry_info")
        End Try
    End Sub

    Sub GetReligion_info()
        Try

            Using AllReligion_reader As SqlDataReader = GetReligion()


                ddlReligion.Items.Clear()
                'di_Religion = New ListItem("--", "--")
                ' ddlReligion.Items.Add(di_Religion)
                If AllReligion_reader.HasRows = True Then
                    While AllReligion_reader.Read
                        ddlReligion.Items.Add(New ListItem(AllReligion_reader("RLG_DESCR"), AllReligion_reader("RLG_ID")))

                    End While


                End If
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "GetReligion_info")
        End Try
    End Sub

    Sub Student_M_Details(ByVal Stud_No As String)

        Try
            'Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
            'Dim str_Sql As String = "SELECT STU_PHOTOPATH  FROM STUDENT_M WHERE STU_ID= " & Session("STU_ID") & ""
            'Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            'Dim strPath As String = Convert.ToString(ds.Tables(0).Rows(0)("STU_PHOTOPATH"))
            'Dim connPath As String = WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ConnectionString
            'Dim strImagePath As String = String.Empty

            Dim str_noimagepath As String = Server.MapPath("Images\no_image.gif")
            Dim str_path As String = GetStudentPhotoActualPath()
            If str_path.Trim = "" Then
                str_path = str_noimagepath
            End If

            imgParentImage.ImageUrl = str_path

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub
    Public Function GetStudentPhotoActualPath() As String
        Dim Virtual_Path As String = Web.Configuration.WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ToString()
        Dim Phy_Path As String = Web.Configuration.WebConfigurationManager.ConnectionStrings("EmpFilepath").ToString()
        Try


            Dim strqry As String = "SELECT isnull(STU_PHOTOPATH,'') as STU_PHOTOPATH  FROM STUDENT_M WHERE STU_ID= " & Session("STU_ID") & ""
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, strqry)
            Dim str_path As String = ds.Tables(0).Rows(0)(0)
            If File.Exists(Phy_Path + str_path) Then
                Return Virtual_Path + str_path + "?id=" & Encr_decrData.Encrypt(DateTime.Now).ToString()


            End If
            Return ""
        Catch ex As Exception
            Return ""
        End Try
    End Function
    Function callTrans_Student_M(ByVal trans As SqlTransaction) As Integer
        Try
            Dim status As Integer
            Dim STU_ID As String = Session("STU_ID")
            Dim STU_BSU_ID As String = Session("STU_BSU_ID")

            Dim PHOTO_PATH As String = String.Empty

            Dim ConFigPath As String = WebConfigurationManager.ConnectionStrings("EmpFilepath").ConnectionString

            If ViewState("EMPPHOTOFILEPATH") <> "" Then

                If Not Directory.Exists(ConFigPath & "\" & Session("STU_BSU_ID") & "\" & Session("STU_ID") & "\") Then
                    Directory.CreateDirectory(ConFigPath & "\" & Session("STU_BSU_ID") & "\" & Session("STU_ID") & "\")
                Else
                    Dim dold As New DirectoryInfo(ConFigPath & "\" & Session("STU_BSU_ID") & "\" & Session("STU_ID") & "\")

                    Dim fiold() As System.IO.FileInfo
                    fiold = dold.GetFiles("*.*", SearchOption.TopDirectoryOnly)
                    If fiold.Length > 0 Then '' If Having Attachments
                        For Each f As System.IO.FileInfo In fiold
                            f.Delete()
                        Next
                    End If




                    Dim d As New DirectoryInfo(ConFigPath & "\" & Session("STU_BSU_ID") & "\" & Session("STU_ID") & "\Temp\")

                    Dim fi() As System.IO.FileInfo
                    fi = d.GetFiles("*.*", SearchOption.TopDirectoryOnly)
                    If fi.Length > 0 Then '' If Having Attachments
                        For Each f As System.IO.FileInfo In fi

                            f.MoveTo(ConFigPath + ViewState("EMPPHOTOFILEPATH"))
                            PHOTO_PATH = ViewState("EMPPHOTOFILEPATH")


                        Next
                    End If

                    ViewState("EMPPHOTOFILEPATHoldPath") = ""
                    ViewState("EMPPHOTOFILEPATH") = ""
                End If
            End If





            If PHOTO_PATH <> "" Then
                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim param(3) As SqlClient.SqlParameter
                param(0) = New SqlClient.SqlParameter("@STU_ID", Session("STU_ID"))
                param(1) = New SqlClient.SqlParameter("@PHOTO_PATH", PHOTO_PATH)
                param(2) = New SqlClient.SqlParameter("@User", Session("username"))
                param(3) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
                param(3).Direction = ParameterDirection.ReturnValue
                'Dim cmd As New SqlCommand
                'cmd.CommandText = "saveStudentPhotoPath"
                'cmd.CommandType = CommandType.StoredProcedure
                'cmd.Connection = New SqlConnection(str_conn)
                'cmd.Parameters.AddRange(param)
                'cmd.ExecuteNonQuery()

                SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "saveStudentPhotoPath", param)

                status = param(3).Value
                'saveStudentPhotoPath
                'Dim str_Sql As String = "UPDATE STUDENT_M SET STU_PHOTOPATH='" & PHOTO_PATH & "'  WHERE STU_ID= " & Session("ACTIVE_STU_ID") & ""
                'status = SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_Sql)
                Return status
            Else
                Return 1000
            End If
        Catch ex As Exception
            Return 1000
        End Try
    End Function

    Public Shared Function GetReligion() As SqlDataReader
        'Author(--Lijo)
        'Date   --03/FEB/2008
        'Purpose--Get Religion data from Color_M
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetReligion As String = ""

        sqlGetReligion = "SELECT RLG_ID, RLG_DESCR FROM RELIGION_M order by RLG_DESCR"

        Dim command As SqlCommand = New SqlCommand(sqlGetReligion, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader
    End Function

    Private Sub bindLanguage_dropDown()
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String = "SELECT LNG_ID,LNG_DESCR  FROM LANGUAGE_M order by LNG_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        ddlFLang.DataSource = ds
        ddlFLang.DataTextField = "LNG_DESCR"
        ddlFLang.DataValueField = "LNG_ID"
        ddlFLang.DataBind()
        chkOLang.DataSource = ds
        chkOLang.DataTextField = "LNG_DESCR"
        chkOLang.DataValueField = "LNG_ID"
        chkOLang.DataBind()
        ddlFLang.Items.Add(New ListItem("", ""))
        ddlFLang.ClearSelection()
        ddlFLang.Items.FindByText("").Selected = True
    End Sub
End Class
