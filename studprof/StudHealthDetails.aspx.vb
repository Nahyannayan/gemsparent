﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Partial Class StudProf_StudHealthDetails
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Page.Title = OASISConstants.Gemstitle
            If Session("username") Is Nothing Then
                Session("Active_tab") = "Home"
                Session("Site_Path") = ""
                Response.Redirect("~\Login.aspx")
            ElseIf Session("bPasswdChanged") = "False" Then
                Response.Redirect("~\UpdateInfo\ForcePasswordChange.aspx", False)
                'ElseIf Session("bUpdateContactDetails") = "False" Then

                '    If String.IsNullOrEmpty(Convert.ToString(Session("bStuSteps"))) Then


                '        If (Convert.ToString(Session("isNewContact")) = "True") Then
                '            Response.Redirect("~\StudProf\StudentMainDetails.aspx", False)
                '        Else
                '            Response.Redirect(Session("ForceUpdate_stud"), False)
                '        End If
                '    End If
            End If
            If Page.IsPostBack = False Then
                binddetails(Session("STU_ID"))
                bindInfectiousDisease()
                bindNonInfectiousDisease()
                bindDiseaseDetails()
            End If

            lblChildName.Text = Session("STU_NAME")
            lbChildNameTop.Text = Session("STU_NAME")

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub rdbAllergyYes_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If rdbAllergyyes.Checked Then
            txtAllergies.Enabled = True
        ElseIf rdbAllergyNo.Checked Then
            txtAllergies.Enabled = False
        End If
    End Sub
    Protected Sub rdbAllergyNo_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If rdbAllergyyes.Checked Then
            txtAllergies.Enabled = True
        ElseIf rdbAllergyNo.Checked Then
            txtAllergies.Enabled = False
        End If
    End Sub
    Protected Sub rdbPhysicalEduYes_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If rdbPhysicalEduYes.Checked Then
            txtPhysicalEdu.Enabled = True
        ElseIf rdbPhysicalEduNo.Checked Then
            txtPhysicalEdu.Enabled = False
        End If
    End Sub
    Protected Sub rdbPhysicalEduNo_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If rdbPhysicalEduYes.Checked Then
            txtPhysicalEdu.Enabled = True
        ElseIf rdbPhysicalEduNo.Checked Then
            txtPhysicalEdu.Enabled = False
        End If
    End Sub


    Protected Sub rdbSchoolawareYes_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If rdbSchoolawareYes.Checked Then
            txtSchoolaware.Enabled = True
        ElseIf rdbSchoolawareNo.Checked Then
            txtSchoolaware.Enabled = False
        End If
    End Sub
    Protected Sub rdbSchoolawareNo_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If rdbSchoolawareYes.Checked Then
            txtSchoolaware.Enabled = True
        ElseIf rdbSchoolawareNo.Checked Then
            txtSchoolaware.Enabled = False
        End If
    End Sub

    Protected Sub rdbVisualDisabilityYes_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If rdbVisualDisabilityYes.Checked Then
            txtVisualDisability.Enabled = True
        ElseIf rdbVisualDisabilityNo.Checked Then
            txtVisualDisability.Enabled = False
        End If
    End Sub
    Protected Sub rdbVisualDisabilityNo_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If rdbVisualDisabilityYes.Checked Then
            txtVisualDisability.Enabled = True
        ElseIf rdbVisualDisabilityNo.Checked Then
            txtVisualDisability.Enabled = False
        End If
    End Sub

    Protected Sub btnPrev_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Response.Redirect("~\StudProf\ParentDetails.aspx", False)
    End Sub
    Protected Sub rdbEduNeedsYes_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If rdbEduNeedsYes.Checked Then
            txtEduNeeds.Enabled = True
        ElseIf rdbEduNeedsNo.Checked Then
            txtEduNeeds.Enabled = False
        End If
    End Sub
    Protected Sub rdbEduNeedsNo_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If rdbEduNeedsYes.Checked Then
            txtEduNeeds.Enabled = True
        ElseIf rdbEduNeedsNo.Checked Then
            txtEduNeeds.Enabled = False
        End If
    End Sub




    Protected Sub rdbPrescMedYes_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If rdbPrescMedYes.Checked Then
            txtPrescMedication.Enabled = True
        ElseIf rdbPrescMedNo.Checked Then
            txtPrescMedication.Enabled = False
        End If
    End Sub
    Protected Sub rdbPrescMedNo_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If rdbPrescMedYes.Checked Then
            txtPrescMedication.Enabled = True
        ElseIf rdbPrescMedNo.Checked Then
            txtPrescMedication.Enabled = False
        End If

    End Sub
    Protected Sub gvInfectious_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvInfectious.RowDataBound
        'Dim lblIsInf As Label = New Label
        'Dim rdbInfYes As RadioButton = New RadioButton
        'Dim rdbInfNo As RadioButton = New RadioButton
        'If e.Row.RowType = DataControlRowType.DataRow Then
        '    lblIsInf.Text = DirectCast(e.Row.FindControl("lblIsinfectious"), Label).Text()
        '    If lblIsInf.Text = 1 Then
        '        rdbInfYes.Checked = DirectCast(e.Row.FindControl("rdbInfYes"), RadioButton).Checked
        '    Else
        '        rdbInfNo.Checked = = DirectCast(e.Row.FindControl("rdbInfYes"), RadioButton).Checked
        '    End If
        'End If
    End Sub



    Protected Sub btnSaveContinue_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveContinue.Click
        Try
            Dim status As Integer
            Dim transaction As SqlTransaction
            Using conn As SqlConnection = ConnectionManger.GetOASISConnection
                Try
                    transaction = conn.BeginTransaction("SampleTransaction")
                    status = callTrans_Student_health_Details(transaction)
                    'Dim lblmsg As Label = CType(Page.Master.FindControl("lblSiteMessage"), Label)
                    lblmsg.Text = ""
                    If status = 1000 Then
                        lblmsg.CssClass = "alert-warning alert"
                        lblmsg.Text = "Please enter the details"
                        transaction.Rollback()
                    ElseIf status <> 0 Then
                        lblmsg.CssClass = "alert-warning alert"
                        lblmsg.Text = "Please enter the details"
                        transaction.Rollback()
                    Else
                        'divUploadmsg.CssClass = ""
                        'divUploadmsg.Text = ""
                        lblmsg.CssClass = "divvalid"
                        lblmsg.Text = "Data saved successfully"
                        Session("bStuSteps") = 3
                        UtilityObj.InsertAuditdetails(transaction, "edit", "STUDENT_M", "STU_ID", "STU_ID", "STU_ID=" + Session("STU_ID").ToString)
                        UtilityObj.InsertAuditdetailsWithoutGuid(transaction, "edit", "[OASIS_MED].[dbo].[STUDENT_DISEASE_M]", "STU_ID", "STU_ID", "STU_ID=" + Session("STU_ID").ToString)
                        transaction.Commit()
                        Response.Redirect("~\StudProf\StudentGeneralConsent.aspx", False)
                    End If
                Catch ex As Exception
                    transaction.Rollback()
                End Try


            End Using
        Catch ex As Exception

        End Try





    End Sub

    Sub bindInfectiousDisease()
        Try
            Dim CONN As String = ConnectionManger.GetOASIS_MEDConnectionString
            Dim ds As New DataSet
            Dim PARAM(2) As SqlClient.SqlParameter
            PARAM(0) = New SqlClient.SqlParameter("@STU_ID", Session("STU_ID"))
            PARAM(1) = New SqlClient.SqlParameter("@IsInfectious", True)
            ds = SqlHelper.ExecuteDataset(CONN, CommandType.StoredProcedure, "Get_Diseases_By_Type", PARAM)

            If ds.Tables(0).Rows.Count > 0 Then
                gvInfectious.DataSource = ds
                gvInfectious.DataBind()
            End If

        Catch ex As Exception

        End Try
    End Sub


    Sub bindDiseaseDetails()
        Try
            Dim CONN As String = ConnectionManger.GetOASIS_MEDConnectionString
            Dim ds As New DataSet
            Dim dt As DataTable
            Dim PARAM(1) As SqlClient.SqlParameter
            PARAM(0) = New SqlClient.SqlParameter("@STU_ID", Session("STU_ID"))

            ds = SqlHelper.ExecuteDataset(CONN, CommandType.StoredProcedure, "Get_Student_Disease_Details", PARAM)

            If ds.Tables(0).Rows.Count > 0 Then

                dt = ds.Tables(0)
                txtDiseaseNotes.Text = dt.Rows(0)("DIS_INF_DETAILS").ToString()
                chkDiabetes.Checked = Convert.ToBoolean(dt.Rows(0)("DIS_DIABETES").ToString())
                chkHyperTension.Checked = Convert.ToBoolean(dt.Rows(0)("DIS_HYPERTENSION").ToString())
                chkOther.Checked = Convert.ToBoolean(dt.Rows(0)("DIS_OTHER").ToString())
                chkStroke.Checked = Convert.ToBoolean(dt.Rows(0)("DIS_STROKE").ToString())
                chkTubor.Checked = Convert.ToBoolean(dt.Rows(0)("DIS_TUBERCULOSIS").ToString())
                txtFamilyHistoryOther.Text = dt.Rows(0)("DIS_OTHER_DETAILS").ToString()
            End If

        Catch ex As Exception

        End Try
    End Sub

    Sub bindNonInfectiousDisease()
        Try
            Dim CONN As String = ConnectionManger.GetOASIS_MEDConnectionString
            Dim ds As New DataSet
            Dim PARAM(1) As SqlClient.SqlParameter
            PARAM(0) = New SqlClient.SqlParameter("@STU_ID", Session("STU_ID"))
            PARAM(1) = New SqlClient.SqlParameter("@IsInfectious", False)

            ds = SqlHelper.ExecuteDataset(CONN, CommandType.StoredProcedure, "Get_Diseases_By_Type", PARAM)

            If ds.Tables(0).Rows.Count > 0 Then
                Gvnoninfectious.DataSource = ds
                Gvnoninfectious.DataBind()
            End If
        Catch ex As Exception

        End Try
    End Sub

    Sub binddetails(ByVal stu_id As String)

        Dim arInfo As String() = New String(2) {}
        Dim Temp_Phone_Split As String = String.Empty
        Dim splitter As Char = "-"
        'arInfo = info.Split(splitter)
        Try
            Dim CONN As String = ConnectionManger.GetOASISConnectionString

            Dim PARAM(1) As SqlClient.SqlParameter
            PARAM(0) = New SqlClient.SqlParameter("@STU_ID", stu_id)




            Using readerStudent_Detail As SqlDataReader = SqlHelper.ExecuteReader(CONN, CommandType.StoredProcedure, "STU.GETHEALTHINFO_For_GWA", PARAM)
                If readerStudent_Detail.HasRows = True Then
                    While readerStudent_Detail.Read

                        lblGradeSection.Text = Convert.ToString(readerStudent_Detail("GRD_ID_JOIN")) & "-" & Convert.ToString(readerStudent_Detail("SCT_DESCR_JOIN"))

                        lblFeeID.Text = Convert.ToString(readerStudent_Detail("FEE_ID"))
                        If IsDate(readerStudent_Detail("SDOJ")) = True Then
                            lblDateOfJoin.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((readerStudent_Detail("SDOJ"))))
                        End If


                        txtHealthCard.Text = Convert.ToString(readerStudent_Detail("STU_HCNO"))

                        If Not ddlBgroup.Items.FindByValue(Convert.ToString(readerStudent_Detail("STU_BLOODGROUP"))) Is Nothing Then
                            ddlBgroup.ClearSelection()
                            ddlBgroup.Items.FindByValue(Convert.ToString(readerStudent_Detail("STU_BLOODGROUP"))).Selected = True

                        End If
                        'rdbAllergy.SelectedItem.Text = Convert.ToString(readerStudent_Detail("STU_bALLERGIES"))
                        'rdbAllergy.SelectedValue = DirectCast(readerStudent_Detail("STU_bALLERGIES"), String)
                        'rdbAllergy.Items.FindByValue(Convert.ToString(readerStudent_Detail("STU_bALLERGIES"))).Selected = True
                        txtAllergies.Text = Convert.ToString(readerStudent_Detail("STU_ALLERGIES"))
                        If Not Convert.ToString(readerStudent_Detail("STU_bALLERGIES")) Is Nothing Then
                            If Convert.ToString(readerStudent_Detail("STU_bALLERGIES")).ToLower = "yes" Then
                                rdbAllergyyes.Checked = True
                                txtAllergies.Enabled = True
                            ElseIf Convert.ToString(readerStudent_Detail("STU_bALLERGIES")).ToLower = "no" Then
                                rdbAllergyNo.Checked = True
                                txtAllergies.Enabled = False
                            End If

                            'rdbAllergy.ClearSelection()
                            'rdbAllergy.Items.FindByValue(Convert.ToString(readerStudent_Detail("STU_bALLERGIES"))).Selected = True
                            'If Convert.ToString(readerStudent_Detail("STU_bALLERGIES")) = "Yes" Then
                            '    txtAllergies.Enabled = True
                            'Else
                            '    txtAllergies.Enabled = False
                            'End If
                        Else
                            txtAllergies.Enabled = False
                        End If


                        'rdbPrescMed.Items.FindByValue(Convert.ToString(readerStudent_Detail("STU_bRCVSPMEDICATION"))).Selected = True
                        txtPrescMedication.Text = Convert.ToString(readerStudent_Detail("STU_SPMEDICATION"))

                        If Not Convert.ToString(readerStudent_Detail("STU_bRCVSPMEDICATION")) Is Nothing Then
                            If Convert.ToString(readerStudent_Detail("STU_bRCVSPMEDICATION")).ToLower = "yes" Then
                                rdbPrescMedYes.Checked = True
                                txtPrescMedication.Enabled = True
                            ElseIf Convert.ToString(readerStudent_Detail("STU_bRCVSPMEDICATION")).ToLower = "no" Then
                                rdbPrescMedNo.Checked = True
                                txtPrescMedication.Enabled = False
                            End If


                        Else
                            txtPrescMedication.Enabled = False
                        End If



                        If Not Convert.ToString(readerStudent_Detail("STU_bPRESTRICTIONS")) Is Nothing Then
                            If Convert.ToString(readerStudent_Detail("STU_bPRESTRICTIONS")).ToLower = "yes" Then
                                rdbPhysicalEduYes.Checked = True
                                txtPhysicalEdu.Enabled = True
                            ElseIf Convert.ToString(readerStudent_Detail("STU_bPRESTRICTIONS")).ToLower = "no" Then
                                rdbPhysicalEduNo.Checked = True
                                txtPhysicalEdu.Enabled = False
                            End If


                        Else
                            txtPhysicalEdu.Enabled = False
                        End If
                        'rdbPhysicalEdu.Items.FindByValue(Convert.ToString(readerStudent_Detail("STU_bPRESTRICTIONS"))).Selected = True
                        txtPhysicalEdu.Text = Convert.ToString(readerStudent_Detail("STU_PHYSICAL"))



                        'If Not Convert.ToString(readerStudent_Detail("STU_bHRESTRICTIONS")) Is Nothing Then
                        '    If Convert.ToString(readerStudent_Detail("STU_bHRESTRICTIONS")).ToLower = "yes" Then
                        '        rdbSchoolawareYes.Checked = True
                        '        txtSchoolaware.Enabled = True
                        '    ElseIf Convert.ToString(readerStudent_Detail("STU_bHRESTRICTIONS")).ToLower = "no" Then
                        '        rdbSchoolawareNo.Checked = True
                        '        txtSchoolaware.Enabled = False
                        '    End If


                        'Else
                        '    txtSchoolaware.Enabled = False
                        'End If

                        'rdbSchoolaware.Items.FindByValue(Convert.ToString(readerStudent_Detail("STU_bHRESTRICTIONS"))).Selected = True
                        txtSchoolaware.Text = Convert.ToString(readerStudent_Detail("STU_HEALTH"))


                        If Not Convert.ToString(readerStudent_Detail("STU_bVisual_disability")) Is Nothing Then
                            If Convert.ToString(readerStudent_Detail("STU_bVisual_disability")).ToLower = "yes" Then
                                rdbVisualDisabilityYes.Checked = True
                                txtVisualDisability.Enabled = True
                            ElseIf Convert.ToString(readerStudent_Detail("STU_bVisual_disability")).ToLower = "no" Then
                                rdbVisualDisabilityNo.Checked = True
                                txtVisualDisability.Enabled = False
                            End If


                        Else
                            txtVisualDisability.Enabled = False
                        End If
                        'rdbVisualDisability.Items.FindByValue(Convert.ToString(readerStudent_Detail("STU_bVisual_disability"))).Selected = True
                        txtVisualDisability.Text = Convert.ToString(readerStudent_Detail("STU_Visual_Disability"))


                        If Not Convert.ToString(readerStudent_Detail("STU_bSEN")) Is Nothing Then
                            If Convert.ToString(readerStudent_Detail("STU_bSEN")).ToLower = "yes" Then
                                rdbEduNeedsYes.Checked = True
                                txtEduNeeds.Enabled = True
                            ElseIf Convert.ToString(readerStudent_Detail("STU_bSEN")).ToLower = "no" Then
                                rdbEduNeedsNo.Checked = True
                                txtEduNeeds.Enabled = False
                            End If


                        Else
                            txtEduNeeds.Enabled = False
                        End If
                        'rdbEduNeeds.Items.FindByValue(Convert.ToString(readerStudent_Detail("STU_bSEN"))).Selected = True
                        txtEduNeeds.Text = Convert.ToString(readerStudent_Detail("STU_SEN_REMARK"))



                    End While

                Else
                End If

            End Using
        Catch ex As Exception

        End Try

    End Sub

    Function callTrans_Student_health_Details(ByVal trans As SqlTransaction) As Integer
        Try
            Dim Status As Integer = 0

            ''starts infectious data
            Dim transaction As SqlTransaction
            Try
                Dim rdbInfyes As RadioButton = New RadioButton
                rdbInfyes.Checked = False
                Dim rdbInfNo As RadioButton = New RadioButton

                rdbInfNo.Checked = False
                Dim InfStatus As Boolean = False
                Dim rdbNonInfyes As RadioButton = New RadioButton
                rdbNonInfyes.Checked = False
                Dim rdbNonInfNo As RadioButton = New RadioButton
                rdbNonInfNo.Checked = False
                Dim infId As Label = New Label
                infId.Text = ""
                Dim NonInfId As Label = New Label
                NonInfId.Text = ""
                Dim NonInfStatus As Boolean = False

                Dim Conn As SqlConnection = ConnectionManger.GetOASIS_MEDConnection
                transaction = Conn.BeginTransaction("SampleTransaction1")

                For i As Integer = 0 To gvInfectious.Rows.Count - 1
                    Dim row As GridViewRow = gvInfectious.Rows(i)
                    infId.Text = DirectCast(row.FindControl("lblDisID"), Label).Text
                    rdbInfyes.Checked = DirectCast(row.FindControl("rdbInfYes"), RadioButton).Checked

                    If (rdbInfyes.Checked) Then
                        InfStatus = True
                    End If

                    rdbInfNo.Checked = DirectCast(row.FindControl("rdbInfNo"), RadioButton).Checked

                    If (rdbInfNo.Checked) Then
                        InfStatus = False
                    End If

                    Dim dParms(3) As SqlClient.SqlParameter
                    dParms(0) = New SqlClient.SqlParameter("@STU_ID", Session("STU_ID"))
                    dParms(1) = New SqlClient.SqlParameter("@DIS_ID", infId.Text)
                    dParms(2) = New SqlClient.SqlParameter("@DIS_STATUS", InfStatus)
                    SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "Save_Student_Disease", dParms)
                Next



                For i As Integer = 0 To Gvnoninfectious.Rows.Count - 1
                    Dim row As GridViewRow = Gvnoninfectious.Rows(i)

                    NonInfId.Text = DirectCast(row.FindControl("lblNonDisID"), Label).Text
                    rdbNonInfyes.Checked = DirectCast(row.FindControl("rdbNonInfYes"), RadioButton).Checked
                    rdbNonInfNo.Checked = DirectCast(row.FindControl("rdbNonInfNo"), RadioButton).Checked

                    If rdbNonInfyes.Checked Then
                        NonInfStatus = True
                    End If

                    If rdbNonInfNo.Checked Then
                        NonInfStatus = False
                    End If

                    Dim dParms1(3) As SqlClient.SqlParameter
                    dParms1(0) = New SqlClient.SqlParameter("@STU_ID", Session("STU_ID"))
                    dParms1(1) = New SqlClient.SqlParameter("@DIS_ID", NonInfId.Text)
                    dParms1(2) = New SqlClient.SqlParameter("@DIS_STATUS", NonInfStatus)
                    SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "Save_Student_Disease", dParms1)
                Next
                'save disease edetails
                Dim tParms(9) As SqlClient.SqlParameter
                tParms(0) = New SqlClient.SqlParameter("@STU_ID", Session("STU_ID"))
                tParms(1) = New SqlClient.SqlParameter("@DIS_INF_DETAILS", txtDiseaseNotes.Text)
                tParms(2) = New SqlClient.SqlParameter("@DIS_DIABETES", chkDiabetes.Checked)
                tParms(3) = New SqlClient.SqlParameter("@DIS_HYPERTENSION", chkHyperTension.Checked)
                tParms(4) = New SqlClient.SqlParameter("@DIS_STROKE", chkStroke.Checked)

                tParms(5) = New SqlClient.SqlParameter("@DIS_TUBERCULOSIS", chkTubor.Checked)
                tParms(6) = New SqlClient.SqlParameter("@DIS_OTHER", chkOther.Checked)
                tParms(7) = New SqlClient.SqlParameter("@DIS_OTHER_DETAILS", txtFamilyHistoryOther.Text)

                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "Save_Student_Disease_Details", tParms)


                transaction.Commit()
            Catch ex As Exception
                transaction.Rollback()
            End Try

            ''ends infectious data

            ''initial info info
            Dim healthCardNo As String = txtHealthCard.Text
            Dim bloodGroup As String = ddlBgroup.SelectedItem.Value
            Dim STU_bALLERGIES As String = String.Empty

            If rdbAllergyyes.Checked Then
                STU_bALLERGIES = True
            ElseIf rdbAllergyNo.Checked Then
                STU_bALLERGIES = False
            Else
                STU_bALLERGIES = String.Empty
            End If
            'If STU_bALLERGIES = "Yes" Then
            '    STU_bALLERGIES = True
            'Else
            '    STU_bALLERGIES = False
            'End If
            Dim txtSTU_bAllergies As String = txtAllergies.Text
            Dim STU_bRCVSPMEDICATION As String = String.Empty


            If rdbPrescMedYes.Checked Then
                STU_bRCVSPMEDICATION = True
            ElseIf rdbPrescMedNo.Checked Then
                STU_bRCVSPMEDICATION = False
            Else
                STU_bRCVSPMEDICATION = String.Empty
            End If
            Dim txtPMedication As String = txtPrescMedication.Text

            Dim STU_bPRESTRICTIONS As String = String.Empty

            If rdbPhysicalEduYes.Checked Then
                STU_bPRESTRICTIONS = True
            ElseIf rdbPhysicalEduNo.Checked Then
                STU_bPRESTRICTIONS = False
            Else
                STU_bPRESTRICTIONS = String.Empty
            End If
            Dim txtSTU_bPRESTRICTIONS As String = txtPhysicalEdu.Text
            Dim STU_bHealthAware As String = String.Empty

            'If rdbSchoolawareYes.Checked Then
            STU_bHealthAware = True
            'ElseIf rdbSchoolawareNo.Checked Then
            'STU_bHealthAware = False
            'Else
            'STU_bHealthAware = String.Empty
            'End If
            Dim txtbHealthAware As String = txtSchoolaware.Text

            Dim bVisualDisab As String = String.Empty
            If rdbVisualDisabilityYes.Checked Then
                bVisualDisab = True
            ElseIf rdbVisualDisabilityNo.Checked Then
                bVisualDisab = False
            Else
                bVisualDisab = String.Empty
            End If
            Dim txtVisual As String = txtVisualDisability.Text
            Dim STU_EduNeeds As String = String.Empty
            If rdbEduNeedsYes.Checked Then
                STU_EduNeeds = True
            ElseIf rdbEduNeedsNo.Checked Then

                STU_EduNeeds = False
            Else
                STU_EduNeeds = String.Empty
            End If
            Dim txtspclEduNeeds As String = txtEduNeeds.Text



            Dim pParms(18) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@STU_ID", Session("STU_ID"))
            pParms(1) = New SqlClient.SqlParameter("@STU_BSU_ID", Session("STU_BSU_ID"))
            pParms(2) = New SqlClient.SqlParameter("@healthCardNo", healthCardNo)

            pParms(3) = New SqlClient.SqlParameter("@bloodGroup", bloodGroup)
            pParms(4) = New SqlClient.SqlParameter("@STU_bALLERGIES", STU_bALLERGIES)
            pParms(5) = New SqlClient.SqlParameter("@txtSTU_bAllergies", txtSTU_bAllergies)
            pParms(6) = New SqlClient.SqlParameter("@STU_bRCVSPMEDICATION", STU_bRCVSPMEDICATION)
            pParms(7) = New SqlClient.SqlParameter("@txtPMedication", txtPMedication)
            pParms(8) = New SqlClient.SqlParameter("@STU_bPRESTRICTIONS", STU_bPRESTRICTIONS)
            pParms(9) = New SqlClient.SqlParameter("@txtSTU_bPRESTRICTIONS", txtSTU_bPRESTRICTIONS)
            pParms(10) = New SqlClient.SqlParameter("@STU_bHRESTRICTIONS", STU_bHealthAware)
            pParms(11) = New SqlClient.SqlParameter("@txtbHealthAware", txtbHealthAware)
            pParms(12) = New SqlClient.SqlParameter("@bVisualDisab", bVisualDisab)
            pParms(13) = New SqlClient.SqlParameter("@txtVisual", txtVisual)
            pParms(14) = New SqlClient.SqlParameter("@STU_EduNeeds", STU_EduNeeds)
            pParms(15) = New SqlClient.SqlParameter("@txtspclEduNeeds", txtspclEduNeeds)
            pParms(16) = New SqlClient.SqlParameter("@OLU_ID", Convert.ToInt32(Session("OLU_ID").ToString))
            pParms(17) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(17).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "Update_Student_Health_Details_GWA", pParms)
            Dim ReturnFlag As Integer = pParms(17).Value
            Return ReturnFlag
        Catch ex As Exception
            Return 1000

        End Try
    End Function
End Class
