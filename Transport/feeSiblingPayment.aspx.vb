﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data
Imports System.Data.Sql
Imports System.Web.Configuration
Imports System.Data.SqlClient
Imports UtilityObj
Partial Class Transport_feeSiblingPayment
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Private Property dDueTotal() As Double
        Get
            Return ViewState("dDueTotal")
        End Get
        Set(ByVal value As Double)
            ViewState("dDueTotal") = value
        End Set
    End Property
    Private Property dPayAmountTotal() As Double
        Get
            Return ViewState("dPayAmountTotal")
        End Get
        Set(ByVal value As Double)
            ViewState("dPayAmountTotal") = value
        End Set
    End Property

    Private Property dProcessingCharge() As Double
        Get
            Return ViewState("dProcessingChargeTotal")
        End Get
        Set(ByVal value As Double)
            ViewState("dProcessingChargeTotal") = value
        End Set
    End Property

    Private Property bPaymentInitiated() As Boolean
        Get
            Return ViewState("bPaymentInitiated")
        End Get
        Set(ByVal value As Boolean)
            ViewState("bPaymentInitiated") = value
        End Set
    End Property

    Private Property vpc_OrderInfo() As String
        Get
            Return ViewState("vpc_OrderInfo")
        End Get
        Set(ByVal value As String)
            ViewState("vpc_OrderInfo") = value
        End Set
    End Property

    Private Property vpc_ReturnURL() As String
        Get
            Return ViewState("vpc_ReturnURL")
        End Get
        Set(ByVal value As String)
            ViewState("vpc_ReturnURL") = value
        End Set
    End Property
    Private Property bPaymentConfirmMsgShown() As Boolean
        Get
            Return ViewState("bPaymentConfirmMsgShown")
        End Get
        Set(ByVal value As Boolean)
            ViewState("bPaymentConfirmMsgShown") = value
        End Set
    End Property
    Private Property TRANSPORT_BSU_ID() As String
        Get
            Return ViewState("TRANSPORT_BSU_ID")
        End Get
        Set(ByVal value As String)
            ViewState("TRANSPORT_BSU_ID") = value
        End Set
    End Property
    '
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        'Dim smScriptManager As New ScriptManager
        'smScriptManager = Master.FindControl("ScriptManager1")
        'smScriptManager.RegisterPostBackControl(btnSave)
        If Session("username") Is Nothing Then
            Response.Redirect("~\ParentLogin\Login.aspx")
        End If
        If Not IsPostBack Then
            bPaymentConfirmMsgShown = False
            If Not Request.UrlReferrer Is Nothing Then
                ViewState("ReferrerUrl") = Request.UrlReferrer.ToString()
            End If
            ViewState("datamode") = "add"
            Dim TRANS_BSU_ID As String = FeeCommon.GET_TRANSPORT_BSU(Session("STU_BSU_ID"), Session("STU_ID").ToString)
            TRANSPORT_BSU_ID = TRANS_BSU_ID
            lblTransportType.Text = FeeCommon.GET_BSU_NAME(TRANSPORT_BSU_ID)

            If TRANSPORT_BSU_ID = "900500" Then
                'lblTransportType.Text = " Provider Bright Bus Transport"
                transImage.ImageUrl = "Images\bbt_logo.png"
            ElseIf TRANSPORT_BSU_ID = "900501" Then
                'lblTransportType.Text = " Provider School Transport Services"
                transImage.ImageUrl = "Images\sts_logo.png"
            End If
            BindPaymentProviders(TRANSPORT_BSU_ID) 'Session("sBsuid")
            If rblPaymentGateway.Items.Count > 0 And rblPaymentGateway.SelectedIndex = -1 Then
                rblPaymentGateway.SelectedIndex = 0
            End If
            InitializeControls()
            Gridbind_Feedetails()
        End If
    End Sub

    Private Sub InitializeControls()
        lnkbtnMoreInfo.Attributes.Add("onClick", "return ShowSubWindowWithClose('whatsThis.htm?1=2', '', '80%', '50%');")
        Me.lblDate.Text = DateTime.Now.ToString("dd/MMM/yyyy")
        Me.lblSchool.Text = FeeCommon.GET_BSU_NAME(Session("STU_BSU_ID")) 'SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT BSU_NAME FROM dbo.BUSINESSUNIT_M WITH(NOLOCK) WHERE BSU_ID='" & Session("STU_BSU_ID") & "'")
        bPaymentInitiated = False
        btnSave.Text = "Proceed"

        lblTotal.Attributes.Add("readonly", "readonly")
        lblProcessingCharge.Attributes.Add("readonly", "readonly")
        lblNetPayable.Attributes.Add("readonly", "readonly")
    End Sub
    Public Sub BindPaymentProviders(ByVal BSU_ID As String)
        Me.HFCardCharge.Value = ""
        Dim pParms(1) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(0).Value = BSU_ID

        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISFINConnectionString, _
          CommandType.StoredProcedure, "GetMerchantDetails_BSU_ID", pParms)
        If Not dsData Is Nothing AndAlso dsData.Tables.Count > 0 Then
            For Each row As DataRow In dsData.Tables(0).Rows
                HFCardCharge.Value = HFCardCharge.Value & IIf(HFCardCharge.Value = "", "", "|") & row("CPS_ID") & "=" & row("CRR_CLIENT_RATE")
                Dim CardType As String = row("CPM_REMARKS").ToString()
                'rblPaymentGateway.Items.Add(New ListItem(String.Format("<img src='{0}' alt='{1}' align='absmiddle' onclick='javascript:return this.parentNode.previousSibling.checked=true;' />", row("CPM_IMAGEPATH").ToString(), row("CPM_DESCR").ToString()), row("CPS_ID").ToString()))
                Dim lst As New ListItem(String.Format("<img src='{0}' alt='{1}' id='" & row("CPS_ID").ToString() & "' title='{2}' align='absmiddle' onclick='javascript:return CobrandedCard(this);' class='feeImg' />", row("CPM_IMAGEPATH").ToString(), row("CPM_DESCR").ToString(), row("CPM_REMARKS").ToString()), row("CPS_ID").ToString())
                rblPaymentGateway.Items.Add(lst)
            Next
            For Each itm As ListItem In rblPaymentGateway.Items
                itm.Attributes.Add("onClick", "return RBL(this);")
            Next
        End If
    End Sub
    Protected Sub rblPaymentGateway_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) ' Handles rblPaymentGateway.SelectedIndexChanged
        Gridbind_Feedetails()
    End Sub
    Sub Gridbind_Feedetails()
        Try
            'Dim strPath As String = Convert.ToString(readerStudent_Detail("STU_PHOTOPATH"))
            Dim Virtual_Path As String = Web.Configuration.WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ToString()
            Dim str_noimagepath As String = Server.MapPath("Images\no_image.gif")
            Dim con As String = ConnectionManger.GetOASISTRANSPORTConnectionString
            Dim param(2) As SqlClient.SqlParameter

            param(0) = New SqlClient.SqlParameter("@STU_ID", Session("STU_ID").ToString)
            param(1) = New SqlClient.SqlParameter("@IMAGE_PATH", Virtual_Path)
            param(2) = New SqlClient.SqlParameter("@NO_IMAGE_PATH", str_noimagepath)

            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "ONLINE.GET_SIBLING_FEE_DETAILS", param)

            repInfo.DataSource = ds.Tables(0)
            repInfo.DataBind()
            SetGridTotal()
        Catch ex As Exception
            repInfo.DataBind()
            SetGridTotal()
        End Try
    End Sub
    Protected Sub repInfo_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles repInfo.ItemDataBound
        Dim hfSTU_ID As New HiddenField
        Dim hfSTU_ACD_ID As New HiddenField
        Dim gvFee As New GridView
        Dim lblGvrError As New Label
        Dim imgStudent As New Image
        Dim pce As New AjaxControlToolkit.PopupControlExtender
        Dim popPanel As New Panel
        Dim sDetail As New StringBuilder()
        Dim sHead As New StringBuilder()

        If e.Item.DataItem Is Nothing Then
            Return
        Else
            gvFee = DirectCast(e.Item.FindControl("gvFee"), GridView)
            hfSTU_ID = DirectCast(e.Item.FindControl("hfSTU_ID"), HiddenField)
            hfSTU_ACD_ID = DirectCast(e.Item.FindControl("hfSTU_ACD_ID"), HiddenField)
            lblGvrError = DirectCast(e.Item.FindControl("lblGvrError"), Label)
            imgStudent = DirectCast(e.Item.FindControl("imgStuImage"), Image)
            pce = DirectCast(e.Item.FindControl("PopupControlExtender1"), AjaxControlToolkit.PopupControlExtender)
            'imgStudent.ImageUrl = "GetStudentPhoto.aspx?id=" & Encr_decrData.Encrypt(hfSTU_ID.Value)
            If IsDate(lblDate.Text) Then
                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim dt As DataTable = FeeCollectionOnlineBB.F_GetFeeDetailsForCollectionTransport(lblDate.Text, hfSTU_ID.Value, "S", Session("STU_BSU_ID"), hfSTU_ACD_ID.Value)
                Session("gvfee") = dt
                gvFee.DataSource = Session("gvfee")
                gvFee.DataBind()
                SetGridTotal()
                'Session("gvfee") = Nothing
            End If
        End If
    End Sub

    Protected Sub txtAmountToPay_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        bPaymentConfirmMsgShown = False
        SetGridTotal()
    End Sub
    Sub SetGridTotal() 'Private
        Dim dAmount As Double = 0, dPayingNow As Double = 0, dProcessingChargeTotal As Double = 0
        For Each rptr As RepeaterItem In repInfo.Items
            dDueTotal = 0
            dPayAmountTotal = 0
            dProcessingCharge = 0
            Dim gvfee As New GridView
            gvfee = DirectCast(rptr.FindControl("gvFee"), GridView)
            Dim hfSTU_ID As HiddenField = CType(rptr.FindControl("hfSTU_ID"), HiddenField)
            Dim lblGvrError As New Label
            lblGvrError = DirectCast(rptr.FindControl("lblGvrError"), Label)
            For Each gvr As GridViewRow In gvfee.Rows
                Dim spanInfo As HtmlGenericControl = CType(gvr.FindControl("spanInfo"), HtmlGenericControl)
                Dim txtAmountToPay As TextBox = CType(gvr.FindControl("txtAmountToPay"), TextBox)
                Dim lblFSR_FEE_ID As Label = CType(gvr.FindControl("lblFSR_FEE_ID"), Label)
                Dim lbCancel As LinkButton = CType(gvr.FindControl("lbCancel"), LinkButton)
                Dim h_ProcessingCharge As HiddenField = CType(gvr.FindControl("h_ProcessingCharge"), HiddenField)
                Dim lblAmount As Label = CType(gvr.FindControl("lblAmount"), Label)

                If Not txtAmountToPay Is Nothing Then
                    txtAmountToPay.Text = Val(txtAmountToPay.Text)
                    If lblGvrError.Text <> "" Then
                        txtAmountToPay.Text = 0
                    End If
                    If IsNumeric(txtAmountToPay.Text) = False Then
                        lblGvrError.Text = "Invalid Amount..!"
                        txtAmountToPay.Text = "0.00"
                        lblTotal.Text = "0.00"
                        LlblTotal.Text = "0.00"
                        Exit Sub
                    End If
                    h_ProcessingCharge.Value = Format(Math.Ceiling(GetCardProcessingCharge(Val(Me.rblPaymentGateway.SelectedValue)) * CDbl(txtAmountToPay.Text)), "0.00")
                    'dPayingNow = dPayingNow + CDbl(txtAmountToPay.Text)
                End If
                If gvr.RowType = DataControlRowType.DataRow Then
                    dAmount = dAmount + IIf(Not lblAmount Is Nothing, Convert.ToDouble(lblAmount.Text), 0)
                    dPayingNow = dPayingNow + IIf(Not txtAmountToPay Is Nothing, Convert.ToDouble(txtAmountToPay.Text), 0)
                    dProcessingChargeTotal = dProcessingChargeTotal + IIf(Not h_ProcessingCharge Is Nothing, Convert.ToDouble(h_ProcessingCharge.Value), 0)
                    'Grid wise total calculation
                    dDueTotal = dDueTotal + IIf(Not lblAmount Is Nothing, Convert.ToDouble(lblAmount.Text), 0)
                    dPayAmountTotal = dPayAmountTotal + IIf(Not txtAmountToPay Is Nothing, Convert.ToDouble(txtAmountToPay.Text), 0)
                    dProcessingCharge = dProcessingCharge + IIf(Not h_ProcessingCharge Is Nothing, Convert.ToDouble(h_ProcessingCharge.Value), 0)
                End If
                'If gvr.RowType = DataControlRowType.Footer Then
                '    Dim lblAmounttoPayF As Label = CType(gvr.FindControl("lblAmounttoPayF"), Label)
                '    lblAmounttoPayF.Text = Format(dPayAmountTotal, "#,##0.00")
                'End If
            Next
            Dim lblAmountF As New Label
            Dim lblAmounttoPayF As New Label
            Dim h_ProcessingChargeF As New HiddenField
            If Not gvfee.FooterRow Is Nothing Then
                lblAmountF = CType(gvfee.FooterRow.FindControl("lblAmountF"), Label)
                lblAmounttoPayF = CType(gvfee.FooterRow.FindControl("lblAmounttoPayF"), Label)
                h_ProcessingChargeF = CType(gvfee.FooterRow.FindControl("h_ProcessingChargeF"), HiddenField)
                lblAmountF.Text = dDueTotal.ToString("#,##0.00")
                lblAmounttoPayF.Text = dPayAmountTotal.ToString("#,##0.00")
                h_ProcessingChargeF.Value = dProcessingCharge.ToString("#,##0.00")
            Else
                lblAmountF.Text = "0.00"
                lblAmounttoPayF.Text = "0.00"
                h_ProcessingChargeF.Value = "0.00"
            End If

            dDueTotal = 0
            dPayAmountTotal = 0
            dProcessingCharge = 0
        Next
        lblTotal.Text = Format(dPayingNow, "#,##0.00")
        LlblTotal.Text = Format(dPayingNow, "#,##0.00")
        lblProcessingCharge.Text = Format(dProcessingChargeTotal, "0.00")
        LlblProcessingCharge.Text = Format(dProcessingChargeTotal, "0.00")
        lblNetPayable.Text = Format(dPayingNow + dProcessingChargeTotal, "#,##0.00")
        LlblNetPayable.Text = Format(dPayingNow + dProcessingChargeTotal, "#,##0.00")
    End Sub

    Protected Sub gvFee_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        'Try
        '    If e.Row.RowType = DataControlRowType.DataRow Then
        '        Dim lblFSR_FEE_ID As New Label, popPanel As New Panel
        '        Dim hf_STU_ID As New HiddenField, hf_ACD_ID As New HiddenField
        '        hf_STU_ID = DirectCast(e.Row.FindControl("hf_STU_ID"), HiddenField)
        '        hf_ACD_ID = DirectCast(e.Row.FindControl("hf_ACD_ID"), HiddenField)
        '        lblFSR_FEE_ID = CType(e.Row.FindControl("lblFSR_FEE_ID"), Label)
        '        popPanel = DirectCast(e.Row.FindControl("pnlPop"), Panel)
        '        If Not Session("gvfee") Is Nothing And Not lblFSR_FEE_ID Is Nothing And Not hf_ACD_ID Is Nothing And Not hf_STU_ID Is Nothing And Not popPanel Is Nothing Then
        '            Dim Filter As String = "FEE_ID=" & lblFSR_FEE_ID.Text
        '            Dim dt As DataTable = DirectCast(Session("gvfee"), DataTable)
        '            If dt.Rows.Count > 0 Then
        '                popPanel.Controls.Add(New LiteralControl(GetHtmlContent(dt, lblFSR_FEE_ID.Text)))
        '            End If
        '        End If

        '    End If
        'Catch ex As Exception

        'End Try

        Try
            If e.Row.RowType = DataControlRowType.Header Then

                dDueTotal = 0
                dPayAmountTotal = 0
                dProcessingCharge = 0
            ElseIf e.Row.RowType = DataControlRowType.DataRow Then
                Dim h_BlockPayNow As HiddenField, txtAmountToPay As TextBox
                Dim lblAmount As New Label
                Dim lblDiscount As New Label
                lblAmount = CType(e.Row.FindControl("lblAmount"), Label)
                lblDiscount = CType(e.Row.FindControl("lblDiscount"), Label)
                h_BlockPayNow = CType(e.Row.FindControl("h_BlockPayNow"), HiddenField)
                txtAmountToPay = CType(e.Row.FindControl("txtAmountToPay"), TextBox)
                Dim h_ProcessingCharge As HiddenField = CType(e.Row.FindControl("h_ProcessingCharge"), HiddenField)

                If h_BlockPayNow IsNot Nothing And txtAmountToPay IsNot Nothing Then
                    If h_BlockPayNow.Value = "1" Or h_BlockPayNow.Value.ToLower = "true" Then
                        txtAmountToPay.Enabled = False
                    Else
                        txtAmountToPay.Enabled = True
                    End If
                End If
                'dDiscountTotal = dDiscountTotal + IIf(Not lblDiscount Is Nothing, Convert.ToDouble(lblDiscount.Text), 0)
                dDueTotal = dDueTotal + IIf(Not lblAmount Is Nothing, Convert.ToDouble(lblAmount.Text), 0)
                dPayAmountTotal = dPayAmountTotal + IIf(Not txtAmountToPay Is Nothing, Convert.ToDouble(txtAmountToPay.Text), 0)
                dProcessingCharge = dProcessingCharge + IIf(Not h_ProcessingCharge Is Nothing, Convert.ToDouble(h_ProcessingCharge.Value), 0)
            ElseIf e.Row.RowType = DataControlRowType.Footer Then
                Dim lblAmountF As New Label
                Dim lblDiscountF As New Label
                Dim lblAmounttoPayF As New Label
                Dim h_ProcessingChargeF As New HiddenField
                lblAmountF = CType(e.Row.FindControl("lblAmountF"), Label)
                lblDiscountF = CType(e.Row.FindControl("lblDiscountF"), Label)
                lblAmounttoPayF = CType(e.Row.FindControl("lblAmounttoPayF"), Label)
                h_ProcessingChargeF = CType(e.Row.FindControl("h_ProcessingChargeF"), HiddenField)

                lblAmountF.Text = dDueTotal
                'lblDiscountF.Text = dDiscountTotal
                lblAmounttoPayF.Text = dPayAmountTotal
                h_ProcessingChargeF.Value = dProcessingCharge

                'dDiscountTotal = 0
                dDueTotal = 0
                dPayAmountTotal = 0
                dProcessingCharge = 0
            End If
        Catch ex As Exception

        End Try
    End Sub
    Public Function GetCardProcessingCharge(ByVal CPS_ID As Integer) As Double
        Dim CardCharge As Double = 0
        If CPS_ID <> 0 Then
            Dim Temp As String()
            Temp = HFCardCharge.Value.Split("|")
            For i As Int16 = 0 To Temp.Length - 1
                If Temp(i).Split("=")(0) = CPS_ID Then
                    CardCharge = Temp(i).Split("=")(1) / 100
                    Exit For
                End If
            Next
        End If
        Return CardCharge
    End Function

    Private Function GetHtmlContent(ByVal dt As DataTable, ByVal FEE_ID As Integer) As String
        Dim sDetail As New StringBuilder()
        Dim sHead As New StringBuilder()
        Try
            Dim HasConcession As Boolean = False
            Dim HasAdjustment As Boolean = False
            If dt.Rows.Count = 0 Then
                Return ""
            End If
            For i As Integer = 0 To dt.Rows.Count - 1
                If dt.Rows(i)("FEE_ID") = FEE_ID Then
                    sDetail.Append("<tr>")
                    sDetail.Append("<td>" & dt.Rows(i)("OPENING").ToString & "</td>")
                    sDetail.Append("<td>" & dt.Rows(i)("MONTHLY_AMOUNT").ToString & "</td>")
                    If Convert.ToDecimal(dt.Rows(i)("CONC_AMOUNT")) > 0 Then
                        sDetail.Append("<td>" & dt.Rows(i)("CONC_AMOUNT").ToString & "</td>")
                        HasConcession = True
                    End If
                    If Convert.ToDecimal(dt.Rows(i)("ADJUSTMENT")) > 0 Then
                        sDetail.Append("<td>" & dt.Rows(i)("ADJUSTMENT").ToString & "</td>")
                        HasAdjustment = True
                    End If
                    sDetail.Append("<td>" & dt.Rows(i)("PAID_AMOUNT").ToString & "</td>")
                    sDetail.Append("<td>" & dt.Rows(i)("AMOUNT").ToString & "</td>")
                    sDetail.Append("</tr>")
                    Exit For
                End If
            Next
            sHead.Append("<table class='popdetails'>")
            sHead.Append("<tr>")
            sHead.Append("<td colspan='5'><b>Fee Details</b></td>")
            sHead.Append("</tr>")
            sHead.Append("<tr>")
            sHead.Append("<td><b>Opening</b></td>")
            sHead.Append("<td><b>Amount</b></td>")
            If HasConcession Then
                sHead.Append("<td><b>Concession</b></td>")
            End If
            If HasAdjustment Then
                sHead.Append("<td><b>Adjustment</b></td>")
            End If
            sHead.Append("<td><b>Paid</b></td>")
            sHead.Append("<td><b>Due</b></td>")
            sHead.Append("</tr>")
        Catch
        Finally
            sDetail.Append("</table>")
        End Try
        Return sHead.ToString() & sDetail.ToString
    End Function

    Protected Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Response.Redirect(ViewState("ReferrerUrl").ToString)
    End Sub

    Protected Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        'System.Threading.Thread.Sleep(5000)
        If bPaymentInitiated Then
            'btnSave.Text = "Save"
            'PaymentRedirect(vpc_ReturnURL, vpc_OrderInfo)
            Exit Sub
        End If
        If ValidateSave() Then
            Dim str_new_FCO_ID As Long
            Dim str_NEW_FCL_RECNO As String = ""
            Dim StuNos As String = ""
            Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
            Dim objConn As New SqlConnection(str_conn) '
            objConn.Open()
            Dim stTrans As SqlTransaction = objConn.BeginTransaction
            Try
                Dim retval As String = "0"
                Dim STR_TYPE As Char = "S"
                Dim FCO_FCO_ID As Int64 = 0
                Dim LoopTotal As Double = 0
                Dim Narration As String = ""
                Dim retvalmindue As Integer = 0, decAmount As Decimal = 0
                For Each repitm As RepeaterItem In repInfo.Items
                    Dim gvfee As New GridView
                    retvalmindue = 0 : decAmount = 0
                    gvfee = DirectCast(repitm.FindControl("gvFee"), GridView)
                    Dim hfSTU_ID As HiddenField = CType(repitm.FindControl("hfSTU_ID"), HiddenField)
                    Dim hfSTU_BSU_ID As HiddenField = CType(repitm.FindControl("hfSTU_BSU_ID"), HiddenField)
                    Dim hfSTU_ACD_ID As HiddenField = CType(repitm.FindControl("hfSTU_ACD_ID"), HiddenField)
                    Dim hfPICKUP_SBL_ID As HiddenField = CType(repitm.FindControl("hfPICKUP_SBL_ID"), HiddenField)
                    Dim hfDROPOFF_SBL_ID As HiddenField = CType(repitm.FindControl("hfDROPOFF_SBL_ID"), HiddenField)

                    Dim lblStuNo As Label = CType(repitm.FindControl("lbSNo"), Label)

                    Dim lblAmountF As New Label
                    Dim lblAmounttoPayF As New Label
                    Dim h_ProcessingChargeF As New HiddenField
                    If Not gvfee.FooterRow Is Nothing Then
                        lblAmountF = CType(gvfee.FooterRow.FindControl("lblAmountF"), Label) 'gives total due amount for current student
                        lblAmounttoPayF = CType(gvfee.FooterRow.FindControl("lblAmounttoPayF"), Label) 'gives total amount paying now of current student
                        h_ProcessingChargeF = CType(gvfee.FooterRow.FindControl("h_ProcessingChargeF"), HiddenField) 'gives total processing charge of current student
                    Else
                        lblAmountF.Text = "0.00"
                        lblAmounttoPayF.Text = "0.00"
                        h_ProcessingChargeF.Value = "0.00"
                    End If
                    str_new_FCO_ID = 0
                    str_NEW_FCL_RECNO = ""
                    Dim dblPayingNow As Double = Convert.ToDouble(lblAmounttoPayF.Text)

                    If dblPayingNow > 0 And retval = "0" Then
                        StuNos = StuNos & IIf(StuNos = "", TrimStudentNo(lblStuNo.Text), "," & TrimStudentNo(lblStuNo.Text))
                        Narration = FeeCollectionOnlineBB.F_GetFeeNarration(lblDate.Text, hfSTU_ACD_ID.Value, hfSTU_BSU_ID.Value)

                        'Dim TRANS_BSU_ID As String = FeeCommon.GET_TRANSPORT_BSU(Session("STU_BSU_ID"), Session("STU_ID").ToString)
                        'changed Session("sBsuid") to TRANS_BSU_ID

                        retval = FeeCollectionOnlineBB.F_SaveFEECOLLECTION_H_ONLINE_MULTI(0, "ONLINE", lblDate.Text, _
                        hfSTU_ACD_ID.Value, hfSTU_ID.Value, "S", lblAmounttoPayF.Text, False, str_new_FCO_ID, TRANSPORT_BSU_ID, _
                        Narration, "CR", "", Request.UserHostAddress.ToString, hfSTU_BSU_ID.Value, hfPICKUP_SBL_ID.Value, _
                        rblPaymentGateway.SelectedItem.Value, stTrans, FCO_FCO_ID)

                        If FCO_FCO_ID = 0 Then 'assign the first FCO_ID
                            FCO_FCO_ID = str_new_FCO_ID
                        End If

                        If retval = "0" Then
                            For Each gvr As GridViewRow In gvfee.Rows
                                Dim txtAmountToPay As TextBox = CType(gvr.FindControl("txtAmountToPay"), TextBox)
                                Dim lblFSR_FEE_ID As Label = CType(gvr.FindControl("lblFSR_FEE_ID"), Label)
                                Dim lblAmount As Label = CType(gvr.FindControl("lblAmount"), Label)
                                If Not txtAmountToPay Is Nothing Then
                                    If CDbl(txtAmountToPay.Text) > 0 Then
                                        retval = FeeCollectionOnlineBB.F_SaveFEECOLLSUB_ONLINE(0, str_new_FCO_ID, lblFSR_FEE_ID.Text, _
                                        txtAmountToPay.Text, -1, lblAmount.Text, stTrans)
                                        If retval <> 0 Then
                                            Exit For
                                        End If
                                    End If
                                End If
                            Next
                        ElseIf retval = "933" Then
                            retvalmindue = FeeCollectionOnlineBB.GetMinimumFeePayableStudent(lblDate.Text, hfSTU_BSU_ID.Value, _
                                     hfPICKUP_SBL_ID.Value, lblAmountF.Text, decAmount, hfSTU_ID.Value, "S")
                        End If

                        If retval = "0" And Val(h_ProcessingChargeF.Value) > 0 Then
                            retval = FeeCollectionOnlineBB.F_SaveFEECOLLSUB_ONLINE(0, str_new_FCO_ID, 162, _
                            Val(h_ProcessingChargeF.Value), -1, Val(h_ProcessingChargeF.Value), stTrans)
                        End If

                        If retval = "0" Then 'Card here
                            retval = FeeCollectionOnlineBB.F_SaveFEECOLLSUB_D_ONLINE(0, str_new_FCO_ID, COLLECTIONTYPE.CREDIT_CARD, _
                           dblPayingNow, "", lblDate.Text, 0, "", "", "", stTrans, Val(h_ProcessingChargeF.Value))
                        End If

                        If retval <> "0" Then
                            stTrans.Rollback()
                            If retval = "933" And retvalmindue = "0" And decAmount > 0 Then
                                lblError.CssClass = "alert alert-warning"
                                lblError.Text = getErrorMessage(retval) & " ( Minimum Payment Amount for " & lblStuNo.Text & " is AED: " & Format(decAmount, "0.00") & " )"
                            Else
                                lblError.CssClass = "alert alert-warning"
                                lblError.Text = getErrorMessage(retval)
                            End If
                            Exit For 'Rollback and exit the payment if error occured in any student payment
                        End If
                    End If
                Next 'End of for loop for repeater

                If retval = "0" Then
                    stTrans.Commit()
                    Dim message1 = "Click on Confirm & Proceed to continue with this payment for " & IIf(Session("BSU_CURRENCY") Is Nothing, "", Session("BSU_CURRENCY")) & " " & Format(LlblNetPayable.Text, Session("BSU_DataFormatString")) & ""
                    Dim message2 = "Please note reference no. <STRONG>" & FCO_FCO_ID & "</STRONG> of this transaction for any future communication."
                    Dim message3 = "Please do not close this browser or Log off until you get the final receipt."
                    'If Request.Browser.Type.ToUpper.Contains("IE") Then
                    '    Me.divboxpanelconfirm.Attributes("class") = "darkPanelIETop"
                    'Else
                    '    Me.divboxpanelconfirm.Attributes("class") = "darkPanelMTop"
                    'End If
                    Me.btnSave.Visible = False
                    Me.testpopup.Style.Item("display") = "block"
                    lblMsg.Text = message1 & "<br />" & message2
                    lblMsg2.Text = message3
                    'lblError.Text = "<br />Click on Proceed to continue with this payment AED :" & lblNetPayable.Text & "<br />" & _
                    '"Please note reference no. " & FCO_FCO_ID & " of this transaction for any future communication.<br /><br />"
                    Dim flagAudit As Integer = UtilityObj.operOnAudiTable("ONLINE SIBLING PAYMENT", FCO_FCO_ID, "Insert", Page.User.Identity.Name.ToString, Me.Page, Server.MachineName)
                    Session("vpc_Amount") = CStr(CDbl(lblNetPayable.Text) * 100).Split(".")(0)
                    Session("vpc_Amount") = CStr(CDbl(LlblNetPayable.Text) * 100).Split(".")(0)
                    Session("CPS_ID") = rblPaymentGateway.SelectedItem.Value
                    vpc_OrderInfo = "Payment for " & StuNos
                    If vpc_OrderInfo.Length > 34 Then
                        vpc_OrderInfo = Left(vpc_OrderInfo, 34)
                    End If
                    vpc_ReturnURL = Request.Url.ToString.Replace("feeSiblingPayment.aspx", "SiblingPaymentResultPage.aspx")
                    Session("vpc_MerchTxnRef") = FCO_FCO_ID
                    Session("vpc_ReturnURL") = vpc_ReturnURL
                    Session("vpc_OrderInfo") = vpc_OrderInfo
                    bPaymentInitiated = True
                    bPaymentConfirmMsgShown = True
                    tblFeeDetails.Disabled = True
                End If

            Catch ex As Exception
                stTrans.Rollback()
                lblError.CssClass = "alert alert-warning"
                lblError.Text = getErrorMessage(1000)
                Errorlog(ex.Message)
            Finally
                If objConn.State = ConnectionState.Open Then
                    objConn.Close()
                End If
            End Try
        End If
    End Sub

    Sub PaymentRedirect(ByVal vpc_ReturnURL As String, ByVal vpc_OrderInfo As String)
        Dim data As New NameValueCollection()
        data("virtualPaymentClientURL") = "https://migs.mastercard.com.au/vpcpay"
        data("vpc_Command") = "pay"
        data("vpc_Version") = "1"
        data("vpc_ReturnURL") = vpc_ReturnURL
        data("vpc_Locale") = "en"
        data("vpc_OrderInfo") = vpc_OrderInfo
        data("Title") = "ASP VPC 3-Party"
        HttpHelper.RedirectAndPOST(Me.Page, "CS_VPC_3Party_DO.aspx", data)
    End Sub

    Private Function ValidateSave() As Boolean
        ValidateSave = True
        SetGridTotal()
        Dim str_error As String = ""
        If Not IsDate(lblDate.Text) Then
            If str_error <> "" Then
                str_error = str_error & "<br /><br />"
                str_error = str_error & "Invalid from date"
            Else
                str_error = str_error & "Invalid from date"
            End If

        End If
        If Session("STU_ID") = "" Then
            If str_error <> "" Then
                str_error = str_error & "<br /><br />"
                str_error = str_error & "Please select student"
            Else
                str_error = str_error & "Please select student"
            End If
        End If
        Dim dblTotal As Decimal
        dblTotal = CDbl(lblNetPayable.Text)
        dblTotal = CDbl(LlblNetPayable.Text)
        If dblTotal <= 0 Then
            If str_error <> "" Then
                str_error = str_error & "<br /><br />"
                str_error = str_error & "Please enter valid amount"
            Else
                str_error = str_error & "Please enter valid amount"
            End If
        End If
        If rblPaymentGateway.SelectedIndex = -1 Then
            If str_error <> "" Then
                str_error = str_error & "<br /><br />"
                str_error = str_error & "Please select a payment gateway"
            Else
                str_error = str_error & "Please select a payment gateway"
            End If
        End If


        If str_error <> "" Then
            lblError.CssClass = "alert alert-warning"

            lblError.Text = str_error
            ValidateSave = False
        End If
    End Function
    Private Function TrimStudentNo(ByVal STUNO As String) As String
        TrimStudentNo = ""
        If STUNO.Length >= 14 Then
            TrimStudentNo = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT CAST(REPLACE('" & STUNO & "',SUBSTRING('" & STUNO & "',1,6),'')AS INTEGER)AS STUNO")
        End If
    End Function

    Protected Sub btnSkip_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSkip.Click
        Me.testpopup.Style.Item("display") = "none"
        'Me.btnSave.Visible = True
        'bPaymentConfirmMsgShown = False
        'bPaymentInitiated = False
        'tblFeeDetails.Disabled = False
        Response.Redirect("~/Transport/feeSiblingPayment.aspx")
    End Sub

    Protected Sub btnProceed_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnProceed.Click
        'Response.Redirect("~/Transport/FeePaymentRedirect.aspx")
        PaymentRedirect()
    End Sub

    Sub PaymentRedirect()
        '-------------------code added by Jacob on 29/jul/2019 for MPGS integration
        'Dim PAYMENT_GATEWAY_URL As String = "", GATEWAY_TYPE As String = ""
        'GATEWAY_TYPE = GetPaymentProvider(Session("CPS_ID"), PAYMENT_GATEWAY_URL)
        Dim GATEWAY_TYPE As String = "", GATEWAY_URL As String = ""
        PaymentGatewayClass.GetGatewayParameters(TRANSPORT_BSU_ID, Session("CPS_ID"), GATEWAY_TYPE, GATEWAY_URL)
        Session("CPM_GATEWAY_TYPE") = GATEWAY_TYPE
        Select Case GATEWAY_TYPE
            Case "MPGS", "DIRECPAY", "EBPG"
                Dim APP_RESULT_PAGE As String = Session("vpc_ReturnURL").ToString().ToLower().Replace("transport/siblingpaymentresultpage.aspx", "Fees/PaymentResultPage.aspx").Split("?")(0).Replace("http://", IIf(Request.Url.Host.Contains("localhost"), "http://", "https://"))
                Dim redirectUrl As String = GATEWAY_URL & "?SRC=" & Encr_decrData.Encrypt("TRANSPORT") & "&ID=" & Encr_decrData.Encrypt(Session("vpc_MerchTxnRef")) & "&APP=" & Encr_decrData.Encrypt("GEMSPARENT") & "&ARP=" & Encr_decrData.Encrypt(APP_RESULT_PAGE) & "&BSU_ID=" & Encr_decrData.Encrypt(TRANSPORT_BSU_ID)
                Response.Redirect(redirectUrl, True)
            Case Else
                Response.Redirect("~/Transport/FeePaymentRedirect.aspx")
        End Select

    End Sub

    'Private Function GetPaymentProvider(ByVal CPS_ID As String, ByRef pPAYMENT_GATEWAY_URL As String) As String
    '    Try
    '        Dim sqlStr As String
    '        GetPaymentProvider = ""
    '        sqlStr = "SELECT CPS_CPM_ID, ISNULL(CPS_PAYMENT_GATEWAY_TYPE ,'') NEW_GATEWAY_TYPE, ISNULL(CPM_PAYMENT_GATEWAY_URL, '') PAYMENT_GATEWAY_URL " & _
    '            "FROM dbo.CREDITCARD_PROVD_S WITH(NOLOCK) INNER JOIN dbo.CREDITCARD_PROVD_M WITH(NOLOCK) ON CPS_CPM_ID = CPM_ID WHERE CPS_ID='" & CPS_ID & "'"
    '        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISFINConnectionString, CommandType.Text, sqlStr)
    '        If dsData.Tables.Count > 0 Then
    '            If dsData.Tables(0).Rows.Count > 0 Then
    '                'CPM_ID = dsData.Tables(0).Rows(0).Item("CPS_CPM_ID")
    '                GetPaymentProvider = dsData.Tables(0).Rows(0).Item("NEW_GATEWAY_TYPE").ToString().Trim
    '                pPAYMENT_GATEWAY_URL = dsData.Tables(0).Rows(0).Item("PAYMENT_GATEWAY_URL").ToString().Trim
    '            End If
    '        End If
    '        Return GetPaymentProvider
    '    Catch ex As Exception
    '        Return ""
    '    End Try
    'End Function

End Class
