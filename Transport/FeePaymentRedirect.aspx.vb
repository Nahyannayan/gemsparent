﻿Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Partial Class Transport_FeePaymentRedirect
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
 If Session("username") & "" = "" Then
            Response.Redirect("~/login.aspx")
        End If
        PaymentRedirect()
    End Sub
    Sub PaymentRedirect()
        '-------------------code added by Jacob on 29/jul/2019 for MPGS integration
        Dim PAYMENT_GATEWAY_URL As String = ""
        If GetPaymentProvider(Session("CPS_ID"), PAYMENT_GATEWAY_URL) = "MPGS" Then
            Dim MPGSdata As New NameValueCollection()
            MPGSdata("SRC") = Encr_decrData.Encrypt("TRANSPORT")
            MPGSdata("ID") = Encr_decrData.Encrypt(Session("vpc_MerchTxnRef")) ' "KzriJLvsKss="
            MPGSdata("APP") = Encr_decrData.Encrypt("GEMSPARENT")
            MPGSdata("APP_RESULT_PAGE") = Session("vpc_ReturnURL").ToString().ToLower().Replace("transport/siblingpaymentresultpage.aspx", "Fees/PaymentResultPage.aspx").Split("?")(0).Replace("http://", IIf(Request.Url.Host.Contains("localhost"), "http://", "https://"))
            HttpHelper.RedirectAndPOST(Me.Page, PAYMENT_GATEWAY_URL, MPGSdata)
        Else '-------------------------------------------------------------------------
            Dim data As New NameValueCollection()
            data("virtualPaymentClientURL") = "https://migs-mtf.mastercard.com.au/"
            data("vpc_Command") = "pay"
            data("vpc_Version") = "1"
            data("vpc_ReturnURL") = Session("vpc_ReturnURL")
            data("vpc_Locale") = "en"
            data("vpc_OrderInfo") = Session("vpc_OrderInfo")
            data("Title") = "ASP VPC 3-Party"
            HttpHelper.RedirectAndPOST(Me.Page, "CS_VPC_3Party_DO.aspx", data)
        End If
    End Sub

    Private Function GetPaymentProvider(ByVal CPS_ID As String, ByRef pPAYMENT_GATEWAY_URL As String) As String
        Try
            Dim sqlStr As String
            GetPaymentProvider = ""
            sqlStr = "SELECT CPS_CPM_ID, ISNULL(CPS_PAYMENT_GATEWAY_TYPE ,'') NEW_GATEWAY_TYPE, ISNULL(CPM_PAYMENT_GATEWAY_URL, '') PAYMENT_GATEWAY_URL " & _
                "FROM dbo.CREDITCARD_PROVD_S WITH(NOLOCK) INNER JOIN dbo.CREDITCARD_PROVD_M WITH(NOLOCK) ON CPS_CPM_ID = CPM_ID WHERE CPS_ID='" & CPS_ID & "'"
            Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISFINConnectionString, CommandType.Text, sqlStr)
            If dsData.Tables.Count > 0 Then
                If dsData.Tables(0).Rows.Count > 0 Then
                    'CPM_ID = dsData.Tables(0).Rows(0).Item("CPS_CPM_ID")
                    GetPaymentProvider = dsData.Tables(0).Rows(0).Item("NEW_GATEWAY_TYPE").ToString().Trim
                    pPAYMENT_GATEWAY_URL = dsData.Tables(0).Rows(0).Item("PAYMENT_GATEWAY_URL").ToString().Trim
                End If
            End If
            Return GetPaymentProvider
        Catch ex As Exception
            Return ""
        End Try
    End Function
End Class
