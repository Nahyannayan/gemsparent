<%@ Page Language="VB" MasterPageFile="~/ParentMaster.master" AutoEventWireup="false" CodeFile="PaymentHistory.aspx.vb" Inherits="Transport_PaymentHistory" Title="Untitled Page" %>

<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<asp:Content ID="Content3" ContentPlaceHolderID="cphParent" runat="Server">
    <script type="text/javascript" src="../Scripts/jquery-1.10.2.js"></script>
    <script type="text/javascript" src="../Scripts/Fancybox/jquery.fancybox.js?v=2.1.5"></script>
    <script type="text/javascript" src="../Scripts/Fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
    <link rel="stylesheet" href="../Scripts/Fancybox/jquery.fancybox.css?1=2" type="text/css"
        media="screen" />
    <script type="text/javascript" src="../Scripts/Popup.js"></script>
    <script type="text/javascript" language="javascript">
        function CheckForPrint(id) {
            if (id != '') {
                //showModelessDialog('feereceipt.aspx?type=REC&id=' + id, '', "dialogWidth: 800px; dialogHeight: 700px; help: no; resizable: no; scroll: yes; status: no; unadorned: no;");
                return ShowSubWindowWithClose('feereceipt.aspx?type=REC&id=' + id, '', '80%', '80%');
            }
        }
    </script>

    
        <div class="content margin-top30 margin-bottom60">
                    <div class="container">
                        <div class="row">


     <!-- Posts Block -->
    <div class="posts-block col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="my-account">
            
            <div class="bottom-padding">
                <div class="title-box">
                    <h3> Payment History  <%= IIf(Session("BSU_CURRENCY") Is Nothing, "", "(Amount In " & Session("BSU_CURRENCY") & ")")%></h3>
                </div>
                <!-- Table  -->
                <div class="table-responsive">
    <table class="table table-striped table-bordered table-responsive text-left my-orders-table" width="100%" border="1" >
       <%-- <tr>
            <th colspan="5" align="left" class="BgHeading">Payment History</th>
        </tr>--%>
       <%-- <tr>
            <td align="left" colspan="5">&nbsp;
            </td>
        </tr>--%>
        <tr>
            <th>
                <strong class="matters">From Date</strong></th>
            <th>
                <asp:TextBox ID="txtFrom" runat="server" Width="100px"></asp:TextBox></th>
            <th align="center">
                <strong class="matters">To Date</strong></th>
            <th align="center">
                <asp:TextBox ID="txtTo" runat="server" Width="100px"></asp:TextBox></th>
            <th>
                <asp:Button ID="btnsearch" runat="server" Text="View History" ValidationGroup="s" CssClass="button" />
            </th>
        </tr>
        <tr>
            <td align="center" class="matters" colspan="5">
                <asp:GridView ID="gvFeeCollection" runat="server" AutoGenerateColumns="False" CssClass="table table-striped table-bordered table-responsive text-left my-orders-table" Width="99%">
                    <Columns>
                        <asp:TemplateField HeaderText="Rec.No.">
                            <ItemTemplate>
                                <asp:LinkButton ID="lbPrint" runat="server" OnClientClick="<%# &quot;javascript:CheckForPrint('&quot;& Receiptencrypt(Container.DataItem(&quot;FCO_FCL_RECNO&quot;))&&quot;');return false;&quot; %>" Text='<%# Bind("FCO_FCL_RECNO") %>'></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="FCO_DATE" HeaderText="Receipt Date" DataFormatString="{0:dd/MMM/yyyy}" HtmlEncode="False"></asp:BoundField>
                        <asp:BoundField DataField="FCO_NARRATION" HeaderText="Description"></asp:BoundField>
                        <asp:BoundField DataField="FCO_ID" HeaderText="Ref. No."></asp:BoundField>
                        <asp:BoundField DataField="FCO_AMOUNT" DataFormatString="{0:0.00}" HeaderText="Amount">
                            <ItemStyle HorizontalAlign="Right" />
                        </asp:BoundField>
                        <asp:BoundField DataField="FCO_STATUS" HeaderText="Status"></asp:BoundField>
                    </Columns>
                </asp:GridView>
                <asp:Label ID="lblerror" runat="server" CssClass="error"></asp:Label>
            </td>
        </tr>
    </table>
    <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server"
        TargetControlID="txtTo" Format="dd/MMM/yyyy">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server"
        TargetControlID="txtFrom" Format="dd/MMM/yyyy">
    </ajaxToolkit:CalendarExtender>

          </div>
                <!-- /Table  -->
            </div>

        </div>
    </div>

                        </div>
                    </div>
            </div>
  

    
    <script type="text/javascript" lang="javascript">
        function ShowWindowWithClose(gotourl, pageTitle, w, h) {
            $.fancybox({
                type: 'iframe',
                //maxWidth: 300,
                href: gotourl,
                //maxHeight: 600,
                fitToView: true,
                padding: 6,
                width: w,
                height: h,
                autoSize: false,
                openEffect: 'none',
                showLoading: true,
                closeClick: true,
                closeEffect: 'fade',
                'closeBtn': true,
                afterLoad: function () {
                    this.title = '';//ShowTitle(pageTitle);
                },
                helpers: {
                    overlay: { closeClick: false }, // prevents closing when clicking OUTSIDE fancybox 
                    title: { type: 'inside' }
                },
                onComplete: function () {
                    $("#fancybox-wrap").css({ 'top': '90px' });

                },
                onCleanup: function () {
                    var hfPostBack = $("#fancybox-frame").contents().find('#hfbPB').val();

                    if (hfPostBack == "Y")
                        window.location.reload(true);
                }
            });

            return false;
        }
        </script>
</asp:Content>


