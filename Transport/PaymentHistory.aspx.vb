Imports Microsoft.ApplicationBlocks.Data
Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Text

Partial Class Transport_PaymentHistory
    Inherits BasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("username") Is Nothing Then
            Response.Redirect("~\ParentLogin\Login.aspx")
        End If
        If Page.IsPostBack = False Then
            txtFrom.Text = Format(Date.Today.AddMonths(-1), "dd/MMM/yyyy")
            txtTo.Text = Format(Date.Today, "dd/MMM/yyyy")
            Gridbind_Feedetails()
        End If
    End Sub

    Protected Sub btnsearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsearch.Click
        Gridbind_Feedetails()
    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Gridbind_Feedetails()
    End Sub

    Sub Gridbind_Feedetails()
        Dim strStudNo As String = ""
        Dim bNoError As Boolean = True
        If IsDate(Date.Today) Then
            Dim dt As New DataTable
            dt = FeeCollectionOnlineBB.F_GetFeeCollectionHistory(Session("sBsuid"), txtFrom.Text, txtTo.Text, Session("STU_ID"))
            gvFeeCollection.DataSource = dt
            gvFeeCollection.DataBind()
        End If
    End Sub

    Public Function Receiptencrypt(ByVal recno As Object) As String
        Dim Encr_decrData As New Encryption64
        If Not recno Is DBNull.Value Then
            Return Encr_decrData.Encrypt(recno)
        Else : Return ""
        End If
    End Function


End Class
