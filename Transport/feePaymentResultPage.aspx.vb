﻿Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data.SqlTypes
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data
Partial Class Transport_PaymentResultPage
    Inherits System.Web.UI.Page
    Private Property bBSUTaxable() As Boolean
        Get
            Return ViewState("bBSUTaxable")
        End Get
        Set(ByVal value As Boolean)
            ViewState("bBSUTaxable") = value
        End Set
    End Property
    Dim debugData As String = String.Empty

    Private Function displayAVSResponse(ByVal vAVSResultCode As String) As String
        '
        '    <summary>Maps the vpc_AVSResultCode to a relevant description</summary>
        '    <param name="vAVSResultCode">The vpc_AVSResultCode returned by the transaction.</param>
        '    <returns>The corresponding description for the vpc_AVSResultCode.</returns>
        '  
        Dim result As String = "Unknown"

        If vAVSResultCode.Length > 0 Then
            If vAVSResultCode.Equals("Unsupported") Then
                result = "AVS not supported or there was no AVS data provided"
            Else
                Select Case vAVSResultCode
                    Case "X"
                        result = "Exact match - address and 9 digit ZIP/postal code"
                        Exit Select
                    Case "Y"
                        result = "Exact match - address and 5 digit ZIP/postal code"
                        Exit Select
                    Case "S"
                        result = "Service not supported or address not verified (international transaction)"
                        Exit Select
                    Case "G"
                        result = "Issuer does not participate in AVS (international transaction)"
                        Exit Select
                    Case "A"
                        result = "Address match only"
                        Exit Select
                    Case "W"
                        result = "9 digit ZIP/postal code matched, Address not Matched"
                        Exit Select
                    Case "Z"
                        result = "5 digit ZIP/postal code matched, Address not Matched"
                        Exit Select
                    Case "R"
                        result = "Issuer system is unavailable"
                        Exit Select
                    Case "U"
                        result = "Address unavailable or not verified"
                        Exit Select
                    Case "E"
                        result = "Address and ZIP/postal code not provided"
                        Exit Select
                    Case "N"
                        result = "Address and ZIP/postal code not matched"
                        Exit Select
                    Case "0"
                        result = "AVS not requested"
                        Exit Select
                    Case Else
                        result = "Unable to be determined"
                        Exit Select
                End Select
            End If
        End If
        Return result
    End Function

    Private Function displayCSCResponse(ByVal vCSCResultCode As String) As String        '
        '    <summary>Maps the vpc_CSCResultCode to a relevant description</summary>
        '    <param name="vCSCResultCode">The vpc_CSCResultCode returned by the transaction.</param>
        '    <returns>The corresponding description for the vpc_CSCResultCode.</returns>
        Dim result As String = "Unknown"
        If vCSCResultCode.Length > 0 Then
            If vCSCResultCode.Equals("Unsupported") Then
                result = "CSC not supported or there was no CSC data provided"
            Else

                Select Case vCSCResultCode
                    Case "M"
                        result = "Exact code match"
                        Exit Select
                    Case "S"
                        result = "Merchant has indicated that CSC is not present on the card (MOTO situation)"
                        Exit Select
                    Case "P"
                        result = "Code not processed"
                        Exit Select
                    Case "U"
                        result = "Card issuer is not registered and/or certified"
                        Exit Select
                    Case "N"
                        result = "Code invalid or not matched"
                        Exit Select
                    Case Else
                        result = "Unable to be determined"
                        Exit Select
                End Select
            End If
        End If
        Return result
    End Function

    '______________________________________________________________________________

    Private Function splitResponse(ByVal rawData As String) As System.Collections.Hashtable
        '
        '    * <summary>This function parses the content of the VPC response
        '    * <para>This function parses the content of the VPC response to extract the
        '    * individual parameter names and values. These names and values are then
        '    * returned as a Hashtable.</para>
        '    *
        '    * <para>The content returned by the VPC is a HTTP POST, so the content will
        '    * be in the format "parameter1=value&parameter2=value&parameter3=value".
        '    * i.e. key/value pairs separated by ampersands "&".</para>
        '    *
        '    * <param name="RawData"> data string containing the raw VPC response content
        '    * <returns> responseData - Hashtable containing the response data
        '    

        Dim responseData As New System.Collections.Hashtable()
        Try
            ' Check if there was a response containing parameters
            If rawData.IndexOf("=") > 0 Then
                ' Extract the key/value pairs for each parameter
                For Each pair As String In rawData.Split("&"c)
                    Dim equalsIndex As Integer = pair.IndexOf("=")
                    If equalsIndex > 1 AndAlso pair.Length > equalsIndex Then
                        Dim paramKey As String = System.Web.HttpUtility.UrlDecode(pair.Substring(0, equalsIndex))
                        Dim paramValue As String = System.Web.HttpUtility.UrlDecode(pair.Substring(equalsIndex + 1))
                        responseData.Add(paramKey, paramValue)
                    End If
                Next
            Else
                ' There were no parameters so create an error
                responseData.Add("vpc_Message", "The data contained in the response was not a valid receipt.<br/>" & vbLf & "The data was: <pre>" & rawData & "</pre><br/>" & vbLf)
            End If
            Return responseData
        Catch ex As Exception
            ' There was an exception so create an error
            responseData.Add("vpc_Message", (vbLf & "The was an exception parsing the response data.<br/>" & vbLf & "The data was: <pre>" & rawData & "</pre><br/>" & vbLf & "<br/>" & vbLf & "Exception: ") + ex.ToString() & "<br/>" & vbLf)
            Return responseData
        End Try
    End Function

    ' _________________________________________________________________________

    Private Function null2unknown(ByVal req As Object) As String
        Try
            If req Is Nothing Then
                Return String.Empty
            Else
                Return req.ToString()
            End If
        Catch ex As Exception
            Return String.Empty
        End Try
    End Function

    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("username") Is Nothing Then
            Session("Active_tab") = "Home"
            Session("Site_Path") = ""
            Response.Redirect("~\Login.aspx")
        ElseIf Session("bPasswdChanged") = "False" Then
            Response.Redirect("~\UpdateInfo\ForcePasswordChange.aspx", False)
        ElseIf Session("bUpdateContactDetails") = "False" Then
            Response.Redirect(Session("ForceUpdate_stud"), False)
        End If

        Dim SECURE_SECRET As String = FeeCollectionOnline.GetSECURE_SECRET(Session("sBsuid").ToString())

        Panel_Debug.Visible = False
        Panel_Receipt.Visible = False
        Panel_StackTrace.Visible = False

        Dim message As String = ""
        Dim errorExists As Boolean = False
        Dim txnResponseCode As String = ""

        Dim MyPaymentGatewayClass As New PaymentGatewayClass(Session("CPS_ID"))

        Dim rawHashData As String = SECURE_SECRET
        MyPaymentGatewayClass.vpc_Amount = Session("vpc_Amount")
        MyPaymentGatewayClass.vpc_MerchantID = Session("vpc_OrderInfo")
        Dim PageResponseValues As System.Collections.Specialized.NameValueCollection
        If MyPaymentGatewayClass.ResponseType = "POST" Then
            PageResponseValues = Request.Form
        Else
            PageResponseValues = Page.Request.QueryString
        End If
        MyPaymentGatewayClass.SetResponseCodeFromPaymentGateway(PageResponseValues)
        ' Initialise the Local Variables
        Label_HashValidation.Text = "<font color='orange'><b>NOT CALCULATED</b></font>"
        Dim hashValidated As Boolean = True

        Try

#If DEBUG Then
    lblError.Text=MyPaymentGatewayClass.getParameterValues
    debugData += "<br/><u>Start of Debug Data</u><br/><br/>"
#End If
            ' If we have a SECURE_SECRET then validate the incoming data using the MD5 hash
            'included in the incoming data
            If MyPaymentGatewayClass.vpc_SecureHashCode.Length > 0 Then

                ' collect debug information
#If DEBUG Then
            debugData += "<u>Data from Payment Server</u><br/>"
#End If
                If MyPaymentGatewayClass.CPM_GATEWAY_TYPE = "MIGS" Then
                    rawHashData = ""
                    Dim seperator As String = ""
                    For Each item As String In PageResponseValues
                        If Not item.Equals("vpc_SecureHash") AndAlso Not item.Equals("vpc_SecureHashType") Then
                            If item.StartsWith("vpc_") Or item.StartsWith("user_") Then
                                rawHashData &= seperator & item & "=" & PageResponseValues(item)
                                seperator = "&"
                            End If
                        End If
                    Next  'Nextde()
                Else
                    ' loop through all the data in the Page.Request.Form
                    For Each item As String In PageResponseValues

                        ' collect debug information
#If DEBUG Then
                debugData += (item & "=") + PageResponseValues(item) & "<br/>"
#End If

                        ' Collect the data required for the MD5 sugnature if required
                        If SECURE_SECRET.Length > 0 AndAlso Not item.Equals(MyPaymentGatewayClass.MySecueHashCodeName) Then
                            rawHashData += PageResponseValues(item)
                        End If
                    Next
                End If

            End If

            ' Create the MD5 signature if required
            Dim signature As String = ""
            If SECURE_SECRET.Length > 0 Then
                If MyPaymentGatewayClass.CPM_GATEWAY_TYPE = "MIGS" Then
                    ' create the signature and compare
                    signature = MyPaymentGatewayClass.CreateMIGS_SHA256Signature(rawHashData, SECURE_SECRET)
                Else
                    ' create the signature and add it to the query string
                    signature = MyPaymentGatewayClass.CreateMD5Signature(rawHashData)
                End If


                ' Collect debug information
#If DEBUG Then
            debugData += ("<br/><u>Hash Data Input</u>: " & rawHashData & "<br/><br/><u>Signature Created</u>: ") + signature & "<br/>"
#End If

                ' Validate the Secure Hash (remember MD5 hashes are not case sensitive)
                If MyPaymentGatewayClass.vpc_SecureHashCode.Equals(signature) Then
                    ' Secure Hash validation succeeded,
                    ' add a data field to be displayed later.
                    Label_HashValidation.Text = "<font color='#00AA00'><b>CORRECT</b></font>"
                Else
                    ' Secure Hash validation failed, add a data field to be displayed
                    ' later.
                    Label_HashValidation.Text = "<font color='#FF0066'><b>INVALID HASH</b></font>"
                    hashValidated = False
                End If
            End If
            ' Get the standard receipt data from the parsed response
            txnResponseCode = IIf(MyPaymentGatewayClass.vpc_TxnResponseCode.Length > 0, MyPaymentGatewayClass.vpc_TxnResponseCode, "Unknown")
            Label_TxnResponseCode.Text = txnResponseCode
            Label_TxnResponseCodeDesc.Text = MyPaymentGatewayClass.vpc_TxnResponseCodeDescr   'MyPaymentGatewayClass.getResponseDescription(txnResponseCode)
#If DEBUG Then
            debugData += ("<br/><u>Hash Data Input</u>: " & rawHashData & "<br/><br/><u>Signature Created</u>: ") + signature & "<br/>"
            lblMessage.Text &= MyPaymentGatewayClass.getParameterValues
#End If

            Label_Command.Text = IIf(MyPaymentGatewayClass.vpc_Command.ToString.Length > 0, MyPaymentGatewayClass.vpc_Command.ToString, "Unknown")
            Label_Version.Text = IIf(MyPaymentGatewayClass.vpc_Version.ToString.Length > 0, MyPaymentGatewayClass.vpc_Version.ToString, "Unknown")
            Label_OrderInfo.Text = IIf(MyPaymentGatewayClass.vpc_OrderInfo.ToString.Length > 0, MyPaymentGatewayClass.vpc_OrderInfo.ToString, "Unknown")
            Label_MerchantID.Text = IIf(MyPaymentGatewayClass.vpc_MerchantID.ToString.Length > 0, MyPaymentGatewayClass.vpc_MerchantID.ToString, "Unknown")
            Dim recno As String = "", msgServer As String = "", retval As String = "", MerchTxnRef As String = ""
            Try
                If Not Session("vpc_MerchTxnRef") Is Nothing AndAlso Convert.ToString(Session("vpc_MerchTxnRef")) <> "" Then
                    MerchTxnRef = Convert.ToString(Session("vpc_MerchTxnRef"))
                End If
            Catch ex As Exception
                MerchTxnRef = ""
            End Try

            If MyPaymentGatewayClass.vpc_MerchTxnRef.ToString = MerchTxnRef Then
                retval = FeeCollectionOnline.F_SaveFEECOLLECTION_H_ONLINE_PAYMENT_FEES(MerchTxnRef, _
                          MyPaymentGatewayClass.vpc_TxnResponseCode.ToString, MyPaymentGatewayClass.vpc_TxnResponseCodeDescr.ToString, MyPaymentGatewayClass.vpc_Message.ToString, MyPaymentGatewayClass.vpc_ReceiptNo.ToString, MyPaymentGatewayClass.vpc_TransactionNo.ToString, _
                          MyPaymentGatewayClass.vpc_AcqResponseCode.ToString, MyPaymentGatewayClass.vpc_AuthorizeID.ToString, MyPaymentGatewayClass.vpc_BatchNo.ToString, MyPaymentGatewayClass.vpc_CardType.ToString, hashValidated.ToString(), MyPaymentGatewayClass.vpc_Amount.ToString, _
                          MyPaymentGatewayClass.vpc_OrderInfo.ToString, MyPaymentGatewayClass.vpc_MerchantID.ToString, MyPaymentGatewayClass.vpc_Command.ToString, MyPaymentGatewayClass.vpc_Version.ToString, _
                          MyPaymentGatewayClass.vpc_VerType.ToString & "|" + MyPaymentGatewayClass.vpc_VerStatus.ToString.ToString & "|" + MyPaymentGatewayClass.vpc_Token.ToString & "|" + MyPaymentGatewayClass.vpc_VerSecurLevel.ToString & "|" + MyPaymentGatewayClass.vpc_Enrolled.ToString & "|" + MyPaymentGatewayClass.vpc_Xid.ToString & "|" + MyPaymentGatewayClass.vpc_AcqECI.ToString & "|" + MyPaymentGatewayClass.vpc_AuthStatus.ToString, _
                          recno, msgServer)
            End If
            Try
                FeeCollectionOnline.SAVE_ONLINE_PAYMENT_AUDIT("FEES", "RESPONSE", _
                Page.Request.Url.ToString, MerchTxnRef)
            Catch ex As Exception
                'lblMessage.Text &= "ex.message - " & ex.Message
            End Try

            If retval = "0" And recno.Trim <> "" Then
                lblMessage.ForeColor = Drawing.Color.Blue
                lblError.ForeColor = Drawing.Color.Blue
            Else
                lblMessage.ForeColor = Drawing.Color.Red
                lblError.ForeColor = Drawing.Color.Red
                lblMessage.Text = MyPaymentGatewayClass.vpc_TxnResponseCodeDescr.ToString & ", unfortunately the receipt number is not generated."
            End If

            ' End If
            Session("vpc_MerchTxnRef") = ""
            lnkReceipt.Text = recno.ToString
            ' lnkReceipt.OnClientClick = "CheckForPrint();return false;"
            Dim Encr_decrData As New Encryption64

            If msgServer.ToString <> "" Then
                lblError.Text = msgServer.ToString
            End If
            'lblHeader.Text = FeeCollectionOnlineBB.GetHeader()
            lblStudentNo.Text = Session("STU_NO").ToString()
            lblStudentName.Text = Session("STU_NAME").ToString()
            Label_MerchTxnRef.Text = MyPaymentGatewayClass.vpc_MerchTxnRef

            If IsNumeric(MyPaymentGatewayClass.vpc_ActualAmount) Then
                Label_Amount.Text = MyPaymentGatewayClass.vpc_ActualAmount
            Else
                Label_Amount.Text = ""
            End If

            lblDate.Text = Format(Date.Now, "dd/MMM/yyyy")
            ' if message was not provided locally then obtain value from server
            If message.Length = 0 Then
                message = IIf(MyPaymentGatewayClass.vpc_Message.ToString.Length > 0, MyPaymentGatewayClass.vpc_Message.ToString, "Unknown")
            End If
            bBSUTaxable = DirectCast(SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT ISNULL(BUS_bFEE_TAXABLE,0) FROM dbo.BUSINESSUNIT_SUB WHERE BUS_BSU_ID='" & Session("sBsuid") & "'"), Boolean)
            hfTaxable.Value = IIf(bBSUTaxable, "1", "0")
            If recno.Trim <> "" Then
                h_Recno.Value = Encr_decrData.Encrypt(recno.Trim)
                Dim PopForm As String = IIf(bBSUTaxable, "FeeReceipt_TAX.aspx", "feereceipt.aspx")
                lnkReceipt.Attributes.Add("onClick", "return ShowSubWindowWithClose('" & PopForm & "?type=REC&id=" & h_Recno.Value & "', '', '45%', '85%');")
                lblPrintMsg.Visible = True
                Dim ReplaceForm As String = IIf(bBSUTaxable, "FeeReceiptEMail_TAX.aspx", "FeeReceiptEMail.aspx")

                'Send mail + Set Message
                Dim str_sendmailurl As String = Page.Request.Url.ToString
                str_sendmailurl = UtilityObj.StringReplace(str_sendmailurl, "http://", "https://")
                str_sendmailurl = str_sendmailurl.Split("?"c)(0) & "?type=REC&id=" & h_Recno.Value & "&bsu_id=" & Encr_decrData.Encrypt(Session("sBsuid")) & "&user=" & Encr_decrData.Encrypt(Session("username"))
                str_sendmailurl = UtilityObj.StringReplace(str_sendmailurl, "feePaymentResultPage.aspx", ReplaceForm)
                If retval = "0" Then
                    frame1.Attributes("src") = str_sendmailurl
                End If
            End If
            'Dim uri As New Uri(str_sendmailurl)
            'Dim request As HttpWebRequest = HttpWebRequest.Create(uri)
            'request.Method = WebRequestMethods.Http.[Get]
            'Dim response As HttpWebResponse = request.GetResponse()
            'Dim reader As New StreamReader(response.GetResponseStream())
            'Dim tmp As String = reader.ReadToEnd()
            'response.Close()
            'Page.Response.Write(tmp)

        Catch ex As Exception
            message = "(51) Exception encountered. " & ex.Message
            UtilityObj.Errorlog(ex.Message)
            UtilityObj.Errorlog(ex.ToString())
            If ex.StackTrace.Length > 0 Then
                Label_StackTrace.Text = ex.ToString()
                Panel_StackTrace.Visible = True
            End If
            errorExists = True
            lblError.Text = ex.Message
        End Try


        ' output the message field
        Label_Message.Text = message.ToString

        ' The URL AgainLink and Title are only used for display purposes.
        '    * Note: This is ONLY used for this example and is not required for 
        '    * production code.  


        ' Create a link to the example's HTML order page
        Label_AgainLink.Text = "<a href=""" & MyPaymentGatewayClass.AgainLink & """>Another Transaction</a>"

        ' Determine the appropriate title for the receipt page
        Label_Title.Text = IIf((errorExists OrElse txnResponseCode.Equals("7") OrElse txnResponseCode.Equals("Unknown") OrElse hashValidated = False), Page.Request.QueryString("Title") & " Error Page", Page.Request.QueryString("Title") & " Receipt Page")
        MyPaymentGatewayClass = Nothing
        ' output debug data to the screen
#If DEBUG Then
    debugData += "<br/><u>End of debug information</u><br/>"
    Label_Debug.Text = debugData
#End If

        '
        '    **********************
        '    * END OF MAIN PROGRAM
        '    **********************
        '    *
        '    * FINISH TRANSACTION - Output the VPC Response Data
        '    * =====================================================
        '    * For the purposes of demonstration, we simply display the Result fields
        '    * on a web page.
        '
        Panel_Debug.Visible = True
        Dim BrwType As String = HttpContext.Current.Request.Browser.Browser
        If Request.Browser.Type.Contains("IE") = False Then
            ScriptManager.GetCurrent(Me.Page).RegisterPostBackControl(lnkReceipt)
        End If
    End Sub
End Class

