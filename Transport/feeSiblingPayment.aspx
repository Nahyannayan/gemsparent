﻿<%@ Page Language="VB" MasterPageFile="~/ParentMaster.master" AutoEventWireup="false" CodeFile="feeSiblingPayment.aspx.vb" Inherits="Transport_feeSiblingPayment" %>

<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphParent" runat="Server">

    <script src="../Scripts/jquery-1.9.1.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="../Scripts/jQuery-ui-1.10.3.css" />
    <link href="../css/Popup.css" rel="stylesheet" />
    <!-- Add fancyBox -->
    <script type="text/javascript" src="../Scripts/Fancybox/jquery.fancybox.js?v=2.1.5"></script>
    <script type="text/javascript" src="../Scripts/Fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
    <link rel="stylesheet" href="../Scripts/Fancybox/jquery.fancybox.css" type="text/css" media="screen" />
    <script type="text/javascript" src="../Scripts/PopupJQuery.js"></script>

    <script type="text/javascript" language="javascript">
        <%--   window.onload = function () {
            SetAnimation();
        };
        //On UpdatePanel Refresh
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    $('#<%=rblPaymentGateway.ClientID%>').find(":radio").click(function () {
                        RBL(this);
                    });
                    var cps = $('#<%=rblPaymentGateway.ClientID%>').find(":checked").val();
                    $('#' + cps).click();
                }
            });
        };
        function CobrandedCard(element) {
            element.parentNode.previousSibling.checked = true;
            if (element.alt == "NBAD")
                $('#divCoBr').hide(400);
            else if (element.alt == "NBAD GEMS Co-Branded")
                $('#divCoBr').show(500);
            SetAnimation();
            return true;
        }
        function RBL(pVal) {
            var cpsid = $('#' + pVal.id).val();
            $('#' + cpsid).click();

            return true;
        }--%>
        //function SetAnimation() {
        //    $('.demo').modernBlink({
        //        duration: 1500, // Duration specified in milliseconds (integer)
        //        iterationCount: "infinite", // Number of times the element should blink ("infinite" or integer)
        //        auto: true // Whether to start automatically or not (boolean)
        //    });
        //}




    </script>
     <script>
         if ($(window).width() < 979) {
             if ($(location).attr("href").indexOf("feeSiblingPayment_M.aspx") == -1) {
                 window.location = "\\Transport\\feeSiblingPayment_M.aspx";
             }
         }
         if ($(window).width() > 979) {
             if ($(location).attr("href").indexOf("feeSiblingPayment.aspx") == -1) {
                 window.location = "\\Transport\\feeSiblingPayment.aspx";
             }
         }
    </script>
    <style type="text/css">
        .feeImg {
            max-width: 120px;
            max-height: 60px;
        }
    </style>
    <%--  <asp:UpdatePanel ID="UP1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>--%>

    <div class="content margin-top30 margin-bottom60">
        <div class="container">
            <div class="row">

                <!-- Posts Block -->
                <div class="posts-block col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="my-account">

                        <div class="bottom-padding">
                            <div class="title-box">

                                <div class="container">
                                    <div class="row">
                                        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                                            <h3>Transport Fee Payment</h3>
                                        </div>
                                        <%-- <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 text-right"> <h4><asp:Label ID="lblTransportType" runat="server" Text="" CssClass="text-right"></asp:Label>   &nbsp;&nbsp;
                              <asp:Image ID="transImage" runat="server" class="img-circle"  ToolTip="transport logo" Width="50px" /></h4>
                         </div>--%>
                                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 text-left">
                                            <asp:Image ID="transImage" runat="server" ToolTip="transport logo" Width="50px" />
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <!-- Table  -->
                            <div class="table-responsive">
                                <asp:Label ID="lblError" runat="server" EnableViewState="False"></asp:Label>
                                <%--CssClass="ProceedMessage"--%>
                                <%--<table border="0" cellpadding="0" cellspacing="0" align="center" class="table table-striped table-bordered table-responsive text-left my-orders-table" style="width: 100%">
             
                <tr>
                    <td>--%>

                                <table cellspacing="0" align="center" style="width: 99%; padding: 0;" class="table table-striped table-bordered table-responsive text-left my-orders-table" id="tblFeeDetails" runat="server">
                                    <tr>
                                        <th class="matters">Date</th>
                                        <%--<td class="matters" align="center" style="width: 1pt;">:</td>--%>
                                        <td class="matters">
                                            <asp:Label ID="lblDate" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <th class="matters">Service Provider</th>
                                        <%--  <td class="matters">:</td>--%>
                                        <td class="matters">
                                            <asp:Label ID="lblTransportType" runat="server" Text="" CssClass="text-right" ForeColor="Maroon"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th class="matters">School</th>
                                        <%--  <td class="matters">:</td>--%>
                                        <td class="matters">
                                            <asp:Label ID="lblSchool" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <%-- <tr>
                                <td colspan="3" class="gridheader_pop">Payment Details
                                </td>
                            </tr>--%>
                                    <tr style="height: 95px !important;">
                                        <th class="matters" style="width: 20% !important;">Select Payment Gateway<br />
                                            <asp:LinkButton ID="lnkbtnMoreInfo" runat="server" Font-Bold="True" Font-Size="11px"
                                                Font-Underline="True" Text="More Info"></asp:LinkButton></th>
                                        <%--  <td class="matters" style="width: 2% !important;">:</td>--%>
                                        <td class="matters" style="width: 78% !important;">
                                            <div>
                                                <asp:HiddenField ID="HFCardCharge" runat="server" />
                                                <asp:RadioButtonList ID="rblPaymentGateway" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow">
                                                </asp:RadioButtonList>
                                            </div>
                                            <div style="display: none; margin-top: 7px; color: navy;" id="divCoBr">
                                                <span class="ui-icon ui-icon-info" style="float: left; margin: 0 7px 4px 0;"></span>
                                                <span class="demo">NBAD GEMS Cobranded Credit Card Only</span>
                                            </div>
                                        </td>
                                    </tr>

                                    <tr>
                                        <th colspan="3" class="gridheader_pop">Fee Details <%= IIf(Session("BSU_CURRENCY") Is Nothing, "", "(Amount In " & Session("BSU_CURRENCY") & ")")%>
                                        </th>
                                    </tr>
                                    <%-- <tr style="display:none;" ><td colspan="3">        </td>
                            </tr>--%>
                                    <tr>
                                        <td colspan="3" align="center">

                                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                <tr>
                                                    <td>
                                                        <asp:Repeater ID="repInfo" runat="server">
                                                            <HeaderTemplate>
                                                                <table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding: 0;">
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <tr>
                                                                    <td colspan="3">&nbsp;</td>
                                                                </tr>
                                                                <tr class="labelStudentNameS">
                                                                    <th>
                                                                        <asp:Label ID="lbSName" runat="server" Text='<%# Bind("STU_NAME")%>'></asp:Label>&nbsp;[<asp:Label ID="lbSNo" runat="server" Text='<%# Bind("STU_NO")%>'></asp:Label>]
                                                                    </th>
                                                                    <th>Bus No(&#9650;/&#9660;) :(<asp:Label ID="lblPickBusNo" runat="server" Text='<%# Bind("STU_PICKUP_BUSNO")%>'></asp:Label>/<asp:Label ID="lblDropBusNo" runat="server" Text='<%# Bind("STU_DROPOFF_BUSNO")%>'></asp:Label>)
                                                                    </th>
                                                                    <th align="right" class="text-right">
                                                                        <div style="clear: both;">
                                                                            <asp:Label ID="lbGrade" runat="server" Text='<%# Bind("GRD_DISPLAY")%>'></asp:Label>
                                                                            <asp:HiddenField ID="hfSTU_ID" Value='<%# Bind("STU_ID") %>' runat="server" />
                                                                            <asp:HiddenField ID="hfSTU_BSU_ID" Value='<%# Bind("STU_BSU_ID")%>' runat="server" />
                                                                            <asp:HiddenField ID="hfSTU_ACD_ID" Value='<%# Bind("STU_ACD_ID")%>' runat="server" />
                                                                            <asp:HiddenField ID="hfPICKUP_SBL_ID" Value='<%# Bind("PICKUP_SBL_ID")%>' runat="server" />
                                                                            <asp:HiddenField ID="hfDROPOFF_SBL_ID" Value='<%# Bind("DROPOFF_SBL_ID")%>' runat="server" />
                                                                        </div>
                                                                    </th>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="3">&nbsp;</td>
                                                                </tr>
                                                                <tr align="left" style="border: none 0px !important;">
                                                                    <td valign="top" colspan="3" align="left">
                                                                        <table cellpadding="2" width="100%" cellspacing="2" border="0" style="border-collapse: collapse !important; border-spacing: 2 !important; padding: 2 !important; border: none 0px !important;">
                                                                            <tr>
                                                                                <td valign="middle" align="left" width="10%">
                                                                                    <asp:Image ID="imgStuImage" runat="server" ImageUrl='<%# Bind("STU_PHOTOPATH") %>' ToolTip='<%# Bind("STU_NAME")%>' Width="60px" />
                                                                                </td>
                                                                                <td valign="top" width="90%">
                                                                                    <asp:GridView ID="gvFee" runat="server" AutoGenerateColumns="False" EmptyDataText="No Details Added"
                                                                                        SkinID="GridViewNormal" Width="100%" OnRowDataBound="gvFee_RowDataBound" ShowFooter="true" class="table table-striped table-bordered table-responsive text-left my-orders-table">
                                                                                        <RowStyle Height="20px" VerticalAlign="Top" />
                                                                                        <Columns>
                                                                                            <asp:TemplateField HeaderText="FSR_FEE_ID" Visible="False">
                                                                                                <ItemTemplate>
                                                                                                    <asp:Label ID="lblFSR_FEE_ID" runat="server" Text='<%# Bind("FEE_ID") %>'></asp:Label>
                                                                                                    <asp:HiddenField ID="hf_STU_ID" Value='<%# Bind("STU_ID")%>' runat="server" />
                                                                                                    <asp:HiddenField ID="hf_ACD_ID" Value='<%# Bind("ACD_ID")%>' runat="server" />
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderText="Fee" HeaderStyle-Width="60%">
                                                                                                <ItemTemplate>
                                                                                                    <asp:Label ID="lblFeeDescr" runat="server" Text='<%# Bind("FEE_DESCR") %>'></asp:Label>
                                                                                                    <%-- <asp:LinkButton ID="lblFeeDescr" runat="server" CausesValidation="False" OnClientClick="return false;"
                                                                                    Text='<%# Bind("FEE_DESCR") %>'></asp:LinkButton><ajaxToolkit:PopupControlExtender
                                                                                        ID="PopupControlExtender1" runat="server" PopupControlID="pnlPop"
                                                                                        Position="Right" TargetControlID="lblFeeDescr">
                                                                                    </ajaxToolkit:PopupControlExtender>--%>
                                                                                                    <%-- <asp:Panel ID="pnlPop" runat="server" Height="50px" Width="125px">
                                                                                </asp:Panel>--%>
                                                                                                </ItemTemplate>
                                                                                                <FooterTemplate>
                                                                                                    <asp:Label ID="lblTotals" runat="server" Text='TOTAL'></asp:Label>
                                                                                                </FooterTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderText="Due" HeaderStyle-Width="10%">
                                                                                                <ItemTemplate>
                                                                                                    <asp:Label ID="lblAmount" runat="server" Text='<%# Bind("CLOSING", "{0:0.00}") %>'></asp:Label>
                                                                                                </ItemTemplate>
                                                                                                <FooterTemplate>
                                                                                                    <asp:Label ID="lblAmountF" runat="server" Text=""></asp:Label>
                                                                                                </FooterTemplate>
                                                                                                <ItemStyle HorizontalAlign="Right" Width="89px" />
                                                                                                <FooterStyle HorizontalAlign="Right" />
                                                                                                <HeaderStyle HorizontalAlign="Right" />
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderText="Paying Now" HeaderStyle-Width="20%">
                                                                                                <ItemTemplate>
                                                                                                    <asp:TextBox ID="txtAmountToPay" AutoCompleteType="Disabled" runat="server" AutoPostBack="True" CssClass="form-control"
                                                                                                        onFocus="this.select();" OnTextChanged="txtAmountToPay_TextChanged" Style="text-align: right"
                                                                                                        TabIndex="52" Text='<%# Bind("Amount", "{0:0.00}")%>' autocomplete="off"></asp:TextBox>
                                                                                                    <ajaxToolkit:FilteredTextBoxExtender ID="ftbe" runat="server"
                                                                                                        TargetControlID="txtAmountToPay"
                                                                                                        FilterType="Custom, Numbers"
                                                                                                        ValidChars="." />
                                                                                                    <asp:LinkButton ID="lbCancel" Visible="false" runat="server">Cancel</asp:LinkButton>
                                                                                                    <asp:HiddenField ID="h_ProcessingCharge" runat="server" />
                                                                                                </ItemTemplate>
                                                                                                <FooterTemplate>
                                                                                                    <asp:Label ID="lblAmounttoPayF" runat="server" Text="0.00"></asp:Label>
                                                                                                    <asp:HiddenField ID="h_ProcessingChargeF" runat="server" />
                                                                                                </FooterTemplate>
                                                                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                                                                                <FooterStyle HorizontalAlign="Right" />
                                                                                                <ItemStyle HorizontalAlign="Right" Width="89px" />
                                                                                            </asp:TemplateField>
                                                                                        </Columns>
                                                                                        <FooterStyle Font-Bold="true" />
                                                                                    </asp:GridView>

                                                                                </td>
                                                                            </tr>
                                                                            <tr id="trgvrerror" runat="server" visible="false">
                                                                                <td colspan="2" align="center">
                                                                                    <asp:Label ID="lblGvrError" CssClass="linkText" Text="" runat="server"></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                </table>
                                                            </FooterTemplate>
                                                        </asp:Repeater>
                                                    </td>
                                                </tr>

                                                <tr class="tdblankAll">
                                                    <td align="right" valign="middle" class="tdfields">
                                                        <h4>Total :
            <asp:TextBox ID="lblTotal" CssClass="form-control text-right" runat="server" Width="15%" Visible="false">
            </asp:TextBox>
                                                            <asp:Label ID="LlblTotal" CssClass="form-control text-right" runat="server" Width="15%">
                                                            </asp:Label>
                                                        </h4>
                                                    </td>
                                                </tr>

                                                <tr class="tdblankAll">
                                                    <td align="right" valign="middle" class="tdfields">
                                                        <h4>Processing Charge :
            <asp:TextBox ID="lblProcessingCharge" CssClass="form-control text-right" runat="server" Width="15%" Visible="false">
            </asp:TextBox>
                                                            <asp:Label ID="LlblProcessingCharge" CssClass="form-control text-right" runat="server" Width="15%">
                                                            </asp:Label>
                                                        </h4>
                                                    </td>
                                                </tr>

                                                <tr class="tdblankAll">
                                                    <td align="right" valign="middle" class="tdfields">
                                                        <h4>Net Payable :
            <asp:TextBox ID="lblNetPayable" CssClass="form-control text-right" runat="server" Width="15%" Visible="false">
            </asp:TextBox>
                                                            <asp:Label ID="LlblNetPayable" CssClass="form-control text-right" runat="server" Width="15%">
            </asp:Label>
                                                        </h4>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <%--  <tr>
                                <td colspan="3" align="right">
                                    <table cellpadding="0" cellspacing="0" style="border: 0 none">--%>
                                    <%-- <tr>
                                            <td class="matters"><h4>Total</h4></td>
                                            <td class="matters"><h4>:</h4></td>
                                            <td class="matters" align="right"><h4>
                                                <asp:Label ID="lblTotal" runat="server" Text="0.00"></asp:Label></h4></td>
                                        </tr>--%>


                                    <%--  <tr>
                                            <td class="matters"><h4>Processing Charge</h4></td>
                                            <td class="matters"><h4>:</h4></td>
                                            <td class="matters" align="right"><h4>
                                                <asp:Label ID="lblProcessingCharge" runat="server" Text="0.00"></asp:Label></h4></td>
                                        </tr>--%>


                                    <%--  <tr>
                                            <td class="matters"><h4>Net Payable</h4></td>
                                            <td class="matters"><h4>:</h4></td>
                                            <td class="matters" align="right"><h4>
                                                <asp:Label ID="lblNetPayable" runat="server" Text="0.00"></asp:Label></h4></td>
                                        </tr>--%>



                                    <%--    </table>
                                </td>
                            </tr>--%>

                                    <tr>
                                        <td align="center" colspan="3">
                                            <asp:Button ID="btnSave" runat="server" CssClass="btn btn-info" TabIndex="155" Text="Confirm" />
                                            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="btn btn-info"
                                                Text="Cancel" TabIndex="158" />
                                        </td>
                                    </tr>
                                </table>
                                <%--   </td>
                </tr>
              
            </table>--%>
                                <div id="testpopup" class="darkPanelM anim" runat="server" style="display: none; z-index: 3000 !important;">
                                    <div id="divboxpanelconfirm" runat="server" style="height: 50%; width: 45%; margin-left: 30%; margin-top: 10%;" class="darkPanelMTop">
                                        <div class="holderInner" style="height: 90%; width: 98%;">
                                            <center>
                            <table cellpadding="5" cellspacing="2" border="0" style="height: 90% !important; width: 100% !important; overflow-y: scroll;"
                                class="tableNoborder">
                                <tr class="trSub_Header">
                                    <td>
                                        <span style="float: left; margin: 0 7px 50px 0;"></span>
                                        <h4>Confirm & Proceed</h4>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top">
                                        <span  style="float: left; margin: 0 7px 50px 0;"></span>
                                        <asp:Label ID="lblMsg"  runat="server"></asp:Label><br />   <%--Style="font-family: Verdana, Geneva, Tahoma, sans-serif; font-size: 12px;"--%>
                                        <br />
                                        <br />
                                        <span  style="float: left;"></span>
                                        &nbsp
                                <asp:Label ID="lblMsg2" runat="server" class="alert-success alert "
                                    ></asp:Label>  <%--BackColor="Beige" Font-Names="Verdana" ForeColor="Navy"  Font-Size="10pt"--%>
                                         <br />
                                        <br />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">
                                        <asp:Button ID="btnProceed" runat="server" CausesValidation="False" CssClass="btn btn-info"
                                            Text="Confirm & Proceed" />
                                        <asp:Button ID="btnSkip" runat="server" CausesValidation="False" CssClass="btn btn-info"
                                            Text="Cancel"  />
                                    </td>
                                </tr>
                            </table>
                        </center>
                                        </div>
                                    </div>
                                </div>


                            </div>
                            <!-- /Table  -->
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>
    <%--  </ContentTemplate>
    </asp:UpdatePanel>--%>
</asp:Content>

