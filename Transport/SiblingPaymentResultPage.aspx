﻿<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/ParentMaster.master" CodeFile="SiblingPaymentResultPage.aspx.vb" Inherits="Transport_SiblingPaymentResultPage" %>

<%@ Register Src="~/UserControl/urcStudentTransportPaidResult.ascx" TagPrefix="uc1" TagName="urcStudentTransportPaidResult" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphParent" runat="Server">
    <script type="text/javascript" src="../Scripts/jquery-1.10.2.js"></script>
    <script type="text/javascript" src="../Scripts/Fancybox/jquery.fancybox.js?v=2.1.5"></script>
    <script type="text/javascript" src="../Scripts/Fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
    <link rel="stylesheet" href="../Scripts/Fancybox/jquery.fancybox.css?1=2" type="text/css"
        media="screen" />
    <script type="text/javascript" src="../Scripts/Popup.js"></script>


    <div class="content margin-top30 margin-bottom60">
        <div class="container">
            <div class="row">

                <!-- Posts Block -->

                <div class="posts-block col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <h3>
                        <asp:Label ID="lblMessage" runat="server" EnableViewState="true"></asp:Label></h3>
                    <asp:Label ID="lblError" runat="server" EnableViewState="False" CssClass="alert alert-warning"></asp:Label>
                    <table align="center" cellpadding="0" cellspacing="0" class="BlueTable" width="90%">
                        <tr visible="false" style="display: none">
                            <td align="center">

                                <asp:Label ID="lblFCO_ID" runat="server" Style="width: 1pt;"></asp:Label>
                            </td>
                        </tr>
                        <tr visible="false" style="display: none">
                            <td align="center">
                                <uc1:urcStudentTransportPaidResult runat="server" ID="urcStudentTransportPaidResult1" />
                                <span style="align-content: center; align-items: center; text-align: center; width: 200px !important" runat="server" id="spanRedirect" class="divinfo">
                                    <asp:HyperLink ID="hlPaymentHistory" NavigateUrl="~/Transport/PaymentHistory.aspx" runat="server"> Click here to view Receipts</asp:HyperLink>
                                </span>
                            </td>
                        </tr>


                    </table>


                    <asp:Repeater ID="repInfo" runat="server">
                        <HeaderTemplate>
                            <table align="center" cellpadding="0" cellspacing="0" class="table table-striped table-bordered table-responsive text-left my-orders-table" width="70%">

                                <tr class="matters">
                                    <th align="left">Transaction Reference</th>
                                    <td align="left" valign="middle">
                                 
                                        <asp:Label ID="Label_MerchTxnRef" runat="server" Text='<%# Bind("FCO_FCL_RECNO")%>' /></td>
                                </tr>
                                <tr class="matters">
                                    <th align="left">Date
                                    </th>
                                    <td align="left">
                                        <asp:Label ID="lblDate" runat="server" CssClass="matters" Text='<%# Bind("FCO_DATE")%>'></asp:Label></td>
                                </tr>
                        </HeaderTemplate>
                        <ItemTemplate>

                            <%-- <tr class="subheader_img">
            <td colspan="2" style="height: 19px" align="left">
               </td>
        </tr>--%>

                            <tr class="matters">
                                <th align="left">Student ID</th>
                                <td align="left">
                                    <asp:Label ID="lblStudentNo" runat="server" Text='<%# Bind("STU_NO")%>'></asp:Label>
                                    
                                </td>
                            </tr>
                            <tr class="matters">
                                <th align="left">Student Name</th>
                                <td align="left">
                                    <asp:Label ID="lblStudentName" runat="server" Text='<%# Bind("STU_NAME")%>'></asp:Label></td>
                            </tr>
                            <%-- <tr class="matters">
            <th align="left">
                Grade</th>
            <td align="left">
                <asp:Label ID="lblgrade" runat="server" Text='<%# Bind("GRD_DISPLAY")%>'></asp:Label></td>
        </tr>

             <tr class="matters">
            <th align="left">
                PickUp Bus No</th>
            <td align="left">
                <asp:Label ID="lblpickup" runat="server" Text='<%# Bind("STU_PICKUP_BUSNO")%>'></asp:Label></td>
        </tr>

             <tr class="matters">
            <th align="left">
                DropOff Bus No</th>
            <td align="left">
                <asp:Label ID="lbldropoff" runat="server" Text='<%# Bind("STU_DROPOFF_BUSNO")%>'></asp:Label></td>
        </tr>--%>

                            <tr class="matters">
                                <th align="left">Amount <%= IIf(Session("BSU_CURRENCY") Is Nothing, "", "(Amount In " & Session("BSU_CURRENCY") & ")")%></th>
                                <td align="left" valign="middle">
                                    <asp:Label ID="Label_Amount" runat="server" Text='<%# Bind("AMOUNT", "{0:0.00}")%>' /></td>
                            </tr>


                            <tr style="border: none 0px !important;">
                                <td align="left" colspan="2" style="border: none 0px !important;">

                                    <%--<hr />--%>
                                </td>
                            </tr>
                            <%--<tr style="border: none 0px !important;">
                <td colspan="3" style="border: none 0px !important;">
                    <hr />
                </td>
            </tr>--%>
                        </ItemTemplate>
                        <FooterTemplate>
                            <tr>
                                <td align="right" colspan="2" class="text-right">Total Pay :    
                                    <asp:Label ID="lbl_Amount_F" Font-Bold="True" runat="server" Text="0.00" class="text-right" />
                                    <%--<hr />--%>
                                </td>
                            </tr>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>




                    <asp:Label ID="user_info" runat="server" />
                    <br />
                    <table align="center" border="0" width="70%" style="display: none">
                        <tr class="title">
                            <td colspan="2">
                                <iframe id="frame1" scrolling="auto" runat="server"></iframe>
                                <p><strong>&nbsp;Transaction Receipt Fields</strong></p>
                                <asp:Label ID="Label_Title" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right"><strong><i>VPC API Version: </i></strong></td>
                            <td>
                                <asp:Label ID="Label_Version" runat="server" /></td>
                        </tr>
                        <tr class='shade'>
                            <td align="right"><strong><i>Command: </i></strong></td>
                            <td>
                                <asp:Label ID="Label_Command" runat="server" /></td>
                        </tr>
                        <tr>
                            <td align="right"><strong><em>MerchTxnRef: </em></strong></td>
                            <td></td>
                        </tr>
                        <tr class="shade">
                            <td align="right"><strong><em>Merchant ID: </em></strong></td>
                            <td>
                                <asp:Label ID="Label_MerchantID" runat="server" /></td>
                        </tr>
                        <tr>
                            <td align="right"><strong><em>OrderInfo: </em></strong></td>
                            <td>
                                <asp:Label ID="Label_OrderInfo" runat="server" /></td>
                        </tr>
                        <tr class="shade">
                            <td align="right"><strong><em>Transaction Amount: </em></strong></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td colspan="2" align="center">
                                <div class='bl'>
                                    Fields above are the primary request values.<hr>
                                    Fields below are receipt data fields.
                                </div>
                            </td>
                        </tr>
                        <tr class="shade">
                            <td align="right"><strong><em>Transaction Response Code: </em></strong></td>
                            <td>
                                <asp:Label ID="Label_TxnResponseCode" runat="server" /></td>
                        </tr>
                        <tr>
                            <td align="right"><strong><em>QSI Response Code Description: </em></strong></td>
                            <td>
                                <asp:Label ID="Label_TxnResponseCodeDesc" runat="server" /></td>
                        </tr>
                        <tr class='shade'>
                            <td align="right"><strong><i>Message: </i></strong></td>
                            <td>
                                <asp:Label ID="Label_Message" runat="server" /></td>
                        </tr>
                        <asp:Panel ID="Panel_Receipt" runat="server" Visible="false">
                            <!-- only display these next fields if not an error -->
                            <tr>
                                <td align="right"><strong><em>Shopping Transaction Number: </em></strong></td>
                                <td>
                                    <asp:Label ID="Label_TransactionNo" runat="server" /></td>
                            </tr>
                            <tr class="shade">
                                <td align="right"><strong><em>Batch Number for this transaction: </em></strong></td>
                                <td>
                                    <asp:Label ID="Label_BatchNo" runat="server" /></td>
                            </tr>
                            <tr>
                                <td align="right"><strong><em>Acquirer Response Code: </em></strong></td>
                                <td>
                                    <asp:Label ID="Label_AcqResponseCode" runat="server" /></td>
                            </tr>
                            <tr class="shade">
                                <td align="right"><strong><em>Receipt Number: </em></strong></td>
                                <td>
                                    <asp:Label ID="Label_ReceiptNo" runat="server" /></td>
                            </tr>
                            <tr>
                                <td align="right"><strong><em>Authorization ID: </em></strong></td>
                                <td>
                                    <asp:Label ID="Label_AuthorizeID" runat="server" /></td>
                            </tr>
                            <tr class="shade">
                                <td align="right"><strong><em>Card Type: </em></strong></td>
                                <td>
                                    <asp:Label ID="Label_CardType" runat="server" /></td>
                            </tr>
                        </asp:Panel>
                        <tr>
                            <td colspan="2" style="height: 32px">
                                <hr />
                                &nbsp;</td>
                        </tr>
                        <tr class="title">
                            <td colspan="2" height="25">
                                <p><strong>&nbsp;Hash Validation</strong></p>
                            </td>
                        </tr>
                        <tr>
                            <td align="right"><strong><em>Hash Validated Correctly: </em></strong></td>
                            <td>
                                <asp:Label ID="Label_HashValidation" runat="server" /></td>
                        </tr>
                        <asp:Panel ID="Panel_StackTrace" runat="server">
                            <!-- only display these next fields if an stacktrace output exists-->
                            <tr>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                            <tr class="title">
                                <td colspan="2">
                                    <p><strong>&nbsp;Exception Stack Trace</strong></p>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <asp:Label ID="Label_StackTrace" runat="server" /></td>
                            </tr>
                        </asp:Panel>
                        <tr>
                            <td width="50%">&nbsp;</td>
                            <td width="50%">&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="2" align="center">
                                <asp:Label ID="Label_AgainLink" runat="server" /></td>
                        </tr>
                        <tr>
                            <td align="center" colspan="2">
                                <asp:Panel ID="Panel_Debug" runat="server" Visible="false">
                                    <!-- only display these next fields if debug enabled -->
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Label ID="Label_Debug" runat="server" /></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="Label_DigitalOrder" runat="server" /></td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                    <asp:HiddenField ID="hfFCO_ID" runat="server" />
                            <asp:HiddenField ID="hfFCO_FCL_RECNO" runat="server" />
                                     <asp:HiddenField ID="hfFCO_DATE" runat="server" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>

