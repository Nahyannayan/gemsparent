<%@ page language="VB" autoeventwireup="false" codefile="FeeReceipt.aspx.vb" inherits="Transport_FeeReceipt" %>

<%@ outputcache duration="1" location="None" varybyparam="none" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Online Payment Receipt</title>
    <base target="_self" />
    <style>
        img {
            display: block;
        }

        .ReceiptCaption {
            FONT-WEIGHT: bold;
            FONT-SIZE: 10pt;
            COLOR: #000095;
            FONT-FAMILY: Verdana;
            HEIGHT: 19px;
        }

        .matters_print {
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 10px;
            font-weight: bold;
            color: #000095;
        }

        .matters_heading {
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 13px;
            font-weight: bold;
            color: #000095;
        }

        .matters_normal {
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 10px;
            color: #000095;
        }

        .matters_grid {
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 10px;
            color: #000095;
            TEXT-INDENT: 20px;
        }

        .matters_small {
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 8px;
            color: #000095;
        }

        .Printbg {
            vertical-align: middle;
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 8px;
            color: #000095;
        }

        .PrintSource {
            vertical-align: bottom;
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 8px;
            color: #000095;
        }
    </style>
    <script language="javascript" type="text/javascript">
        function PrintReceipt() {
            document.getElementById('tr_Print').style.display = 'none';
            window.print();
            document.getElementById('tr_Print').style.display = 'inline';
        }
    </script>
</head>
<body style="background-color: white;">
    <form id="form1" runat="server">
        <table align="center" border="0" cellpadding="0" cellspacing="0" width="80%">
            <tr valign="top" id="tr_Print">
                <td align="right">
                    <img src="../Images/Fees/Receipt/print.gif" onclick="PrintReceipt();" style="cursor: hand" /></td>
            </tr>
            <tr valign="top">
                <td colspan="2" align="center" style="border-bottom: #000095 2pt solid">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td align="left" rowspan="4" width="10%">
                                <asp:image id="imgLogo" runat="server" />
                            </td>
                            <td class="matters_heading">
                                <asp:label id="lblSchool" runat="server"></asp:label>
                            </td>
                        </tr>
                        <tr>
                            <td class="matters_print" style="height: 12px">
                                <asp:label id="lblHeader1" runat="server"></asp:label>
                            </td>
                        </tr>
                        <tr>
                            <td class="matters_print">
                                <asp:label id="lblHeader2" runat="server"></asp:label>
                            </td>
                        </tr>
                        <tr>
                            <td class="matters_print">
                                <asp:label id="lblHeader3" runat="server"></asp:label>
                            </td>
                        </tr>
                    </table>
                </td>
                <td align="center" colspan="1" style="height: 90px"></td>
            </tr>
            <tr>
                <td height="20px">&nbsp;</td>
            </tr>
            <tr>
                <td valign="top" align="center">
                    <table align="center">
                        <tr>
                            <td align="center" class="ReceiptCaption">Online Payment Receipt<br />
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <!-- box starts here -->
                                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                    <tr>
                                        <td width="16" valign="top">
                                            <asp:image id="Image1" runat="server" imageurl="~/Images/Fees/Receipt/rounded1.gif" width="16px" height="16px" />
                                        </td>
                                        <td>
                                            <asp:image id="Image4" runat="server" imageurl="~/Images/Fees/Receipt/back1.gif" width="100%" height="16px" />
                                        </td>
                                        <td width="16" valign="top">
                                            <asp:image id="Image2" runat="server" imageurl="~/Images/Fees/Receipt/rounded2.gif" width="16px" height="16px" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top" align="left" style="height: 70px">
                                            <asp:image id="imgLogo1" runat="server"
                                                imageurl="~/Images/Fees/Receipt/back4.gif" width="16px" height="70px" />
                                        </td>
                                        <td>
                                            <!-- content starts here -->
                                            <table cellpadding="3" width="100%" border="0">
                                                <tr>
                                                    <td align="left" class="matters_normal" width="110">Receipt No</td>
                                                    <td align="center" class="matters_print" style="width: 1px;">:</td>
                                                    <td align="left" class="matters_print">
                                                        <asp:label id="lblRecno" runat="server"></asp:label>
                                                    </td>
                                                    <td align="left" class="matters_normal" width="50px">Date</td>
                                                    <td align="left" class="matters_print" style="width: 1px;">:</td>
                                                    <td align="left" class="matters_normal" width="70">
                                                        <asp:label id="lblDate" runat="server"></asp:label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" class="matters_normal">Student ID</td>
                                                    <td align="center" class="matters_print" style="width: 1px;">:</td>
                                                    <td align="left" class="matters_print">
                                                        <asp:label id="lblStudentNo" runat="server"></asp:label>
                                                    </td>
                                                    <td align="left" class="matters_normal">Grade</td>
                                                    <td align="left" class="matters_print" style="width: 1px;">:</td>
                                                    <td align="left" class="matters_normal">
                                                        <asp:label id="lblGrade" runat="server"></asp:label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" class="matters_normal">Name</td>
                                                    <td align="center" class="matters_print" style="width: 1px;">:</td>
                                                    <td align="left" class="matters_print" colspan="4">
                                                        <asp:label id="lblStudentName" runat="server"></asp:label>
                                                    </td>
                                                </tr>
                                            </table>
                                            <!-- content ends here -->
                                        </td>
                                        <td style="height: 70px" valign="top">
                                            <asp:image id="Image3" runat="server" imageurl="~/Images/Fees/Receipt/back2.gif"
                                                width="16px" height="70px" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:image id="Image5" runat="server" imageurl="~/Images/Fees/Receipt/rounded3.gif" width="16px" height="16px" />
                                        </td>
                                        <td>
                                            <asp:image id="Image7" runat="server" imageurl="~/Images/Fees/Receipt/back3.gif" width="100%" height="16px" />
                                        </td>
                                        <td>
                                            <asp:image id="Image6" runat="server" imageurl="~/Images/Fees/Receipt/rounded4.gif" width="16px" height="16px" />
                                        </td>
                                    </tr>
                                </table>
                                <!-- box ends here -->
                                <br />
                            </td>
                        </tr>
                        <tr class="ReceiptCaption" runat="server" id="tr_FeeHeader">
                            <td align="center" style="height: 19px">Fee Details</td>
                        </tr>
                        <tr>
                            <td align="center" class="matters_grid" height="120" valign="top">
                                <asp:gridview id="gvFeeDetails" runat="server" autogeneratecolumns="False" cellpadding="4" bordercolor="#7f83ee" borderwidth="1pt" width="100%">
                          <Columns>
                              <asp:BoundField DataField="FEE_DESCR" HeaderText="Fee" >
                                  <HeaderStyle HorizontalAlign="Left" />
                                  <ItemStyle HorizontalAlign="Left" />
                              </asp:BoundField>
                              <asp:BoundField DataField="FCS_AMOUNT" HeaderText="Amount" DataFormatString="{0:0.00}" >
                                  <HeaderStyle HorizontalAlign="Right" />
                                  <ItemStyle HorizontalAlign="Right"  />
                              </asp:BoundField>
                          </Columns>
                      </asp:gridview>
                            </td>
                        </tr>
                        <tr id="Tr2" runat="server">
                            <td align="right" style="height: 19px" class="matters_print">
                                <asp:label id="lblBalance" runat="server"></asp:label>
                                &nbsp;</td>
                        </tr>
                        <tr runat="server">
                            <td align="left" style="height: 19px" class="matters_normal">
                                <asp:label id="lblNarration" runat="server"></asp:label>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="middle" style="text-indent: 10px;" class="matters_normal">
                                <br />
                                <br />
                                <asp:label id="lblPaymentDetals" runat="server"></asp:label>
                                <br />
                                <br />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center" class="Printbg" valign="middle">
                    <asp:image id="ImgPrintLogo" runat="server" imageurl="~/Images/Fees/Receipt/receipt.gif" />
                    <br />
                    This receipt is electronically generated and does not require any signature.<br />
                    <asp:label id="lblProviderMessage" visible="true" text=" This receipt is subject to Network International LLC crediting the amount to our account." runat="server"> </asp:label>
                    <br />
                    <asp:label id="lblDiscount" visible="false" text="The discounted fee structure is applicable on the existing fee structure of the School.<br />Any  revision in fee for the year if approved by the regulatory authority later, the difference shall be payable by the parent" runat="server"> </asp:label>
                </td>
            </tr>
            <tr>
                <td align="center" class="Printbg" valign="middle">
                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%; height: 100%">
                        <tr>
                            <td align="left" class="PrintSource" valign="top">Source: PHOENIX
                            </td>
                            <td valign="top" align="right">
                                <asp:image id="imgSwooosh" runat="server" imageurl="~/Images/Fees/Receipt/gemsfooter.png" height="200px" width="200px" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="right" valign="bottom" class="matters_normal">
                    <table border="0" cellpadding="0" cellspacing="0" style="border-top: #003A63 1px solid; width: 100%; height: 100%">
                        <tr>
                            <td align="left" class="PrintSource" valign="top">
                                <asp:label id="lblPrintTime" runat="server" cssclass="matters_small"></asp:label>
                            </td>
                            <td>
                                <td valign="top" align="right" class="PrintSource">
                                    <asp:label id="lblLogged" runat="server" cssclass="matters_small" text="User : "></asp:label>
                                    <asp:label id="lbluserloggedin" runat="server" cssclass="matters_small"></asp:label>
                                </td>
                                <td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
