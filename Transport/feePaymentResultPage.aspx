﻿<%@ page title="" language="VB" masterpagefile="~/ParentMaster.master" autoeventwireup="false"
    codefile="feePaymentResultPage.aspx.vb" inherits="Transport_PaymentResultPage" %>

<asp:content id="Content1" contentplaceholderid="cphParent" runat="Server">

    <script src="../Scripts/jquery-1.9.1.js" type="text/javascript"></script>
    <!-- Add fancyBox -->
    <script type="text/javascript" src="../Scripts/Fancybox/jquery.fancybox.js?v=2.1.5"></script>
    <script type="text/javascript" src="../Scripts/Fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
    <link rel="stylesheet" href="../Scripts/Fancybox/jquery.fancybox.css" type="text/css" media="screen" />
    <script type="text/javascript" src="../Scripts/PopupJQuery.js"></script>
    <%--<script type="text/javascript" language="javascript">
        function CheckForPrint() {
            if (document.getElementById('<%=h_Recno.ClientID %>').value != '') {
                 var frmReceipt = "feereceipt.aspx";
                 if ($("#<%=hfTaxable.ClientID%>").val() == "1")
                    frmReceipt = "FeeReceipt_TAX.aspx";
                else
                    frmReceipt = "feereceipt.aspx";
                showModelessDialog(frmReceipt + '?type=REC&id=' + document.getElementById('<%=h_Recno.ClientID %>').value, '', "dialogWidth: 800px; dialogHeight: 700px; help: no; resizable: no; scroll: yes; status: no; unadorned: no;");
             return false;
         }
         else
             alert('Sorry. There is some problem with the transaction');
     }
    </script>--%>
          <div class="content margin-top30 margin-bottom60">
        <div class="container">
            <div class="row">

                <!-- Posts Block -->
                
                <div class="posts-block col-lg-12 col-md-12 col-sm-12 col-xs-12">
                     <asp:HiddenField ID="hfTaxable" runat="server" Value="0" />
                <asp:Label ID="lblError" runat="server" EnableViewState="False" ></asp:Label><br>
                <asp:Label ID="lblMessage" runat="server" EnableViewState="true"  style="font-weight: bold;
                    font-size: 12px; color: #ff0000"></asp:Label>

    <table align="center" cellpadding="0" cellspacing="0" class="table table-striped table-bordered table-responsive text-left my-orders-table" width="70%">
       <%-- <tr class="subheader_img">
            <td colspan="2" style="height: 19px" align="left">
               </td>
        </tr>--%>
        <tr class="matters">
            <td align="left">
                Date
            </td>
            <td align="left">
                <asp:Label ID="lblDate" runat="server" CssClass="matters"></asp:Label></td>
        </tr>
        <tr class="matters">
            <td align="left">
                Student ID</td>
            <td align="left">
                <asp:Label ID="lblStudentNo" runat="server"></asp:Label></td>
        </tr>
        <tr class="matters">
            <td align="left">
                Student Name</td>
            <td align="left">
                <asp:Label ID="lblStudentName" runat="server"></asp:Label></td>
        </tr>
        <tr class="matters">
            <td align="left">
                Transaction Reference</td>
            <td align="left" valign="middle">
                <asp:Label id="Label_MerchTxnRef" runat="server" /></td>
        </tr>
        <tr class="matters">
            <td align="left">
                Amount</td>
            <td align="left" valign="middle">
                <asp:Label id="Label_Amount" runat="server" /></td>
        </tr>
        <tr class="matters">
            <td align="left">
                Receipt Number</td>
            <td align="left" valign="middle">
                <asp:LinkButton ID="lnkReceipt" runat="server" CssClass="MenuLink"></asp:LinkButton>
                <asp:HiddenField ID="h_Recno" runat="server" />
            </td>
        </tr>
        <tr>
            <td align="left" colspan="2" style="font-weight: bold; font-size: 12px; color: #0026ff">
                <asp:Label ID="lblPrintMsg" runat="server" EnableViewState="False" Visible="False">Please print the receipt (Click on the receipt number) for your reference</asp:Label></td>
        </tr>
    </table>
    <br />
    <table align="center" border="0" width="70%" style="display: none;">
        <tr class="title">
            <td colspan="2">
                <iframe id="frame1" scrolling="auto" runat="server"></iframe>
                <p>
                    <strong>&nbsp;Transaction Receipt Fields</strong></p>
                <asp:Label id="Label_Title" runat="server" />
            </td>
        </tr>
        <tr>
            <td align="right">
                <strong><i>VPC API Version: </i></strong>
            </td>
            <td>
                <asp:Label id="Label_Version" runat="server" /></td>
        </tr>
        <tr class='shade'>
            <td align="right">
                <strong><i>Command: </i></strong>
            </td>
            <td>
                <asp:Label id="Label_Command" runat="server" /></td>
        </tr>
        <tr>
            <td align="right">
                <strong><em>MerchTxnRef: </em></strong>
            </td>
            <td>
            </td>
        </tr>
        <tr class="shade">
            <td align="right">
                <strong><em>Merchant ID: </em></strong>
            </td>
            <td>
                <asp:Label id="Label_MerchantID" runat="server" /></td>
        </tr>
        <tr>
            <td align="right">
                <strong><em>OrderInfo: </em></strong>
            </td>
            <td>
                <asp:Label id="Label_OrderInfo" runat="server" /></td>
        </tr>
        <tr class="shade">
            <td align="right">
                <strong><em>Transaction Amount: </em></strong>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <div class='bl'>
                    Fields above are the primary request values.<hr>
                    Fields below are receipt data fields.</div>
            </td>
        </tr>
        <tr class="shade">
            <td align="right">
                <strong><em>Transaction Response Code: </em></strong>
            </td>
            <td>
                <asp:Label id="Label_TxnResponseCode" runat="server" /></td>
        </tr>
        <tr>
            <td align="right">
                <strong><em>QSI Response Code Description: </em></strong>
            </td>
            <td>
                <asp:Label id="Label_TxnResponseCodeDesc" runat="server" /></td>
        </tr>
        <tr class='shade'>
            <td align="right">
                <strong><i>Message: </i></strong>
            </td>
            <td>
                <asp:Label id="Label_Message" runat="server" /></td>
        </tr>
        <asp:Panel id="Panel_Receipt" runat="server" Visible="false">
            <!-- only display these next fields if not an error -->
            <tr>
                <td align="right">
                    <strong><em>Shopping Transaction Number: </em></strong>
                </td>
                <td>
                    <asp:Label id="Label_TransactionNo" runat="server" /></td>
            </tr>
            <tr class="shade">
                <td align="right">
                    <strong><em>Batch Number for this transaction: </em></strong>
                </td>
                <td>
                    <asp:Label id="Label_BatchNo" runat="server" /></td>
            </tr>
            <tr>
                <td align="right">
                    <strong><em>Acquirer Response Code: </em></strong>
                </td>
                <td>
                    <asp:Label id="Label_AcqResponseCode" runat="server" /></td>
            </tr>
            <tr class="shade">
                <td align="right">
                    <strong><em>Receipt Number: </em></strong>
                </td>
                <td>
                    <asp:Label id="Label_ReceiptNo" runat="server" /></td>
            </tr>
            <tr>
                <td align="right">
                    <strong><em>Authorization ID: </em></strong>
                </td>
                <td>
                    <asp:Label id="Label_AuthorizeID" runat="server" /></td>
            </tr>
            <tr class="shade">
                <td align="right">
                    <strong><em>Card Type: </em></strong>
                </td>
                <td>
                    <asp:Label id="Label_CardType" runat="server" /></td>
            </tr>
        </asp:Panel>
        <tr>
            <td colspan="2" style="height: 32px">
                <hr />
                &nbsp;</td>
        </tr>
        <tr class="title">
            <td colspan="2" height="25">
                <p>
                    <strong>&nbsp;Hash Validation</strong></p>
            </td>
        </tr>
        <tr>
            <td align="right">
                <strong><em>Hash Validated Correctly: </em></strong>
            </td>
            <td>
                <asp:Label id="Label_HashValidation" runat="server" /></td>
        </tr>
        <asp:Panel id="Panel_StackTrace" runat="server">
            <!-- only display these next fields if an stacktrace output exists-->
            <tr>
                <td colspan="2">
                    &nbsp;</td>
            </tr>
            <tr class="title">
                <td colspan="2">
                    <p>
                        <strong>&nbsp;Exception Stack Trace</strong></p>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Label id="Label_StackTrace" runat="server" /></td>
            </tr>
        </asp:Panel>
        <tr>
            <td width="50%">
                &nbsp;</td>
            <td width="50%">
                &nbsp;</td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <asp:Label id="Label_AgainLink" runat="server" /></td>
        </tr>
        <tr>
            <td align="center" colspan="2">
                <asp:Panel id="Panel_Debug" runat="server" Visible="false">
                    <!-- only display these next fields if debug enabled -->
                    <table>
                        <tr>
                            <td>
                                <asp:Label id="Label_Debug" runat="server" /></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label id="Label_DigitalOrder" runat="server" /></td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
    </table>

 </div>
                 </div>
             </div>
               </div>

</asp:content>
