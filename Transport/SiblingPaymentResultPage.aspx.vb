﻿Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data.SqlTypes
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data
Partial Class Transport_SiblingPaymentResultPage
    Inherits BasePage

    Dim debugData As String = String.Empty

    '
    'Version 3.1
    '
    '---------------- Disclaimer --------------------------------------------------
    '
    'Copyright 2004 Dialect Solutions Holdings.  All rights reserved.
    '
    'This document is provided by Dialect Holdings on the basis that you will treat
    'it as confidential.
    '
    'No part of this document may be reproduced or copied in any form by any means
    'without the written permission of Dialect Holdings.  Unless otherwise
    'expressly agreed in writing, the information contained in this document is
    'subject to change without notice and Dialect Holdings assumes no
    'responsibility for any alteration to, or any error or other deficiency, in
    'this document.
    '
    'All intellectual property rights in the Document and in all extracts and
    'things derived from any part of the Document are owned by Dialect and will be
    'assigned to Dialect on their creation. You will protect all the intellectual
    'property rights relating to the Document in a manner that is equal to the
    'protection you provide your own intellectual property.  You will notify
    'Dialect immediately, and in writing where you become aware of a breach of
    'Dialect's intellectual property rights in relation to the Document.
    '
    'The names "Dialect", "QSI Payments" and all similar words are trademarks of
    'Dialect Holdings and you must not use that name or any similar name.
    '
    'Dialect may at its sole discretion terminate the rights granted in this
    'document with immediate effect by notifying you in writing and you will
    'thereupon return (or destroy and certify that destruction to Dialect) all
    'copies and extracts of the Document in its possession or control.
    '
    'Dialect does not warrant the accuracy or completeness of the Document or its
    'content or its usefulness to you or your merchant customers.   To the extent
    'permitted by law, all conditions and warranties implied by law (whether as to
    'fitness for any particular purpose or otherwise) are excluded.  Where the
    'exclusion is not effective, Dialect limits its liability to $100 or the
    'resupply of the Document (at Dialect's option).
    '
    'Data used in examples and sample data files are intended to be fictional and
    'any resemblance to real persons or companies is entirely coincidental.
    '
    'Dialect does not indemnify you or any third party in relation to the content
    'or any use of the content as contemplated in these terms and conditions.
    '
    'Mention of any product not owned by Dialect does not constitute an endorsement
    'of that product.
    '
    'This document is governed by the laws of New South Wales, Australia and is
    'intended to be legally binding.
    '
    'Author: Dialect Solutions Group Pty Ltd
    '
    '------------------------------------------------------------------------------


    '
    '<summary>ASP.NET C# 3-Party example for the Virtual Payment Client</summary>
    '<remarks>
    '
    '<para>
    'This example assumes that a transaction GET response has been sent to this 
    'example from the Payment Server with the required fields via a cardholder's 
    'browser redirect. The example then checks the value of an MD5 signature to 
    'ensure the data has not been changed during transmission.
    '</para>
    '
    '<para>
    'The to instantiate the MD5 signature check, the MD5 seed must be saved in the 
    'SECURE_SECRET value which is first parameter in the PageLoad() class. The 
    'SECURE_SECRET value can be found in Merchant Administration/Setup page on the 
    'Payment Server.
    '</para>
    '
    '</remarks>
    '


    ' _____________________________________________________________________________

    ' Declare the global variables
    'private string debugData = "";

    ' _____________________________________________________________________________

    Private Function getResponseDescription(ByVal vResponseCode As String) As String
        ' 
        '    <summary>Maps the vpc_TxnResponseCode to a relevant description</summary>
        '    <param name="vResponseCode">The vpc_TxnResponseCode returned by the transaction.</param>
        '    <returns>The corresponding description for the vpc_TxnResponseCode.</returns>
        ' 
        Dim result As String = "Unknown"
        If vResponseCode.Length > 0 Then
            Select Case vResponseCode
                Case "0"
                    result = "Transaction Successful"
                    Exit Select
                Case "1"
                    result = "Transaction Declined"
                    Exit Select
                Case "2"
                    result = "Bank Declined Transaction"
                    Exit Select
                Case "3"
                    result = "No Reply from Bank"
                    Exit Select
                Case "4"
                    result = "Expired Card"
                    Exit Select
                Case "5"
                    result = "Insufficient Funds"
                    Exit Select
                Case "6"
                    result = "Error Communicating with Bank"
                    Exit Select
                Case "7"
                    result = "Payment Server detected an error"
                    Exit Select
                Case "8"
                    result = "Transaction Type Not Supported"
                    Exit Select
                Case "9"
                    result = "Bank declined transaction (Do not contact Bank)"
                    Exit Select
                Case "A"
                    result = "Transaction Aborted"
                    Exit Select
                Case "B"
                    result = "Transaction Declined - Contact the Bank"
                    Exit Select
                Case "C"
                    result = "Transaction Cancelled"
                    Exit Select
                Case "D"
                    result = "Deferred transaction has been received and is awaiting processing"
                    Exit Select
                Case "F"
                    result = "3-D Secure Authentication failed"
                    Exit Select
                Case "I"
                    result = "Card Security Code verification failed"
                    Exit Select
                Case "L"
                    result = "Shopping Transaction Locked (Please try the transaction again later)"
                    Exit Select
                Case "N"
                    result = "Cardholder is not enrolled in Authentication scheme"
                    Exit Select
                Case "P"
                    result = "Transaction has been received by the Payment Adaptor and is being processed"
                    Exit Select
                Case "R"
                    result = "Transaction was not processed - Reached limit of retry attempts allowed"
                    Exit Select
                Case "S"
                    result = "Duplicate SessionID"
                    Exit Select
                Case "T"
                    result = "Address Verification Failed"
                    Exit Select
                Case "U"
                    result = "Card Security Code Failed"
                    Exit Select
                Case "V"
                    result = "Address Verification and Card Security Code Failed"
                    Exit Select
                Case Else
                    result = "Unable to be determined"
                    Exit Select
            End Select
        End If
        Return result
    End Function

    ' _________________________________________________________________________

    Private Function CreateMD5Signature(ByVal RawData As String) As String
        '
        '    <summary>Creates a MD5 Signature</summary>
        '    <param name="RawData">The string used to create the MD5 signautre.</param>
        '    <returns>A string containing the MD5 signature.</returns>
        '    


        Dim hasher As System.Security.Cryptography.MD5 = System.Security.Cryptography.MD5CryptoServiceProvider.Create()
        Dim HashValue As Byte() = hasher.ComputeHash(Encoding.ASCII.GetBytes(RawData))

        Dim strHex As String = ""
        For Each b As Byte In HashValue
            strHex += b.ToString("x2")
        Next
        Return strHex.ToUpper()
    End Function

    Private Function null2unknown(ByVal req As Object) As String
        Try
            If req Is Nothing Then
                Return String.Empty
            Else
                Return req.ToString()
            End If
        Catch ex As Exception
            Return String.Empty
        End Try
    End Function

    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("username") Is Nothing Then
            Response.Redirect("~\ParentLogin\Login.aspx")
        End If
        If Not IsPostBack AndAlso Not Session("vpc_MerchTxnRef") Is Nothing Then
            urcStudentTransportPaidResult1.Visible = True
            spanRedirect.Visible = False
            '
            '    <summary>Checks a VPC 3-Party transaction response from an incoming HTTP GET</summary>
            '    <remarks>
            '    <para>
            '    This program loops through the QueryString data and calcultaes an MD5 hash 
            '    signature can be calculated to check if the data has changed in
            '    transmission. Remember the data is returned back to the merchant via a 
            '    cardholder's browser redirect so the data has the potential to be changed. 
            '    </para>
            '
            '    <para>
            '    If the MD5 hash signature is not the same value as the incoming signature
            '    that was calculated by the Payment Server then the receipt data has changed 
            '    in transit and should be checked.
            '    </para>
            '
            '    <para>
            '    To find out what is included in your queryString data you can enable DEBUG 
            '    and run a test transaction. The postData string will then be output to the 
            '    screen. This debug code allows the user to see all the data associated with 
            '    the transaction. DEBUG should be disabled or removed entirely in production 
            '    code.
            '    </para>
            '
            '    <para>
            '    To enable DEBUG, change the ASP directive at the top of this file.
            '    <para>
            '
            '    <code>   <%@ Page Language="C#" DEBUG=false %>   </code>
            '    <para>                    to                     </para>
            '    <code>   <%@ Page Language="C#" DEBUG=true %>    </code>
            '
            '    </remarks>
            '
            '********************************************
            '    * Define Variables
            '    ********************************************
            ' This is secret for encoding the MD5 hash
            ' This secret will vary from merchant to merchant
            ' To not create a secure hash, let SECURE_SECRET be an empty string - ""
            ' SECURE_SECRET can be found in Merchant Administration/Setup page"48DE1105485E4D626588E69669A96392"
            Dim TRANS_BSU_ID As String = FeeCommon.GET_TRANSPORT_BSU(Session("STU_BSU_ID"), Session("STU_ID").ToString)
            Dim SECURE_SECRET As String = FeeCollectionOnlineBB.GetSECURE_SECRET(TRANS_BSU_ID) 'Session("sBsuid").ToString()

            Panel_Debug.Visible = False
            Panel_Receipt.Visible = False
            Panel_StackTrace.Visible = False

            ' define message string for errors
            Dim message As String = ""

            ' error exists flag
            Dim errorExists As Boolean = False

            ' transaction response code
            Dim txnResponseCode As String = ""

            Dim rawHashData As String = SECURE_SECRET

            ' Initialise the Local Variables
            Label_HashValidation.Text = "<font color='orange'><b>NOT CALCULATED</b></font>"
            Dim hashValidated As Boolean = True
            Dim MyPaymentGatewayClass As New PaymentGatewayClass(Session("CPS_ID"))
            Try
                '
                '        *************************
                '        * START OF MAIN PROGRAM *
                '        *************************
                '        


                ' collect debug information
#If DEBUG Then
        debugData += "<br/><u>Start of Debug Data</u><br/><br/>"
#End If
                Dim signature As String = ""
                ' If we have a SECURE_SECRET then validate the incoming data using the MD5 hash
                'included in the incoming data
                If Page.Request.QueryString("vpc_SecureHash").Length > 0 Then

                    ' collect debug information
#If DEBUG Then
            debugData += "<u>Data from Payment Server</u><br/>"
#End If
                    If MyPaymentGatewayClass.CPM_GATEWAY_TYPE = "MIGS" Then
                        rawHashData = ""
                        Dim seperator As String = ""
                        For Each item As String In Page.Request.QueryString
                            If Not item.Equals("vpc_SecureHash") AndAlso Not item.Equals("vpc_SecureHashType") Then
                                If item.StartsWith("vpc_") Or item.StartsWith("user_") Then
                                    rawHashData &= seperator & item & "=" & Page.Request.QueryString(item)
                                    seperator = "&"
                                End If
                            End If
                        Next
                        If SECURE_SECRET.Length > 0 Then
                            signature = MyPaymentGatewayClass.CreateMIGS_SHA256Signature(rawHashData, SECURE_SECRET)
                        End If
                    Else
                        If SECURE_SECRET.Length > 0 Then
                            For Each item As String In Page.Request.QueryString
                                ' collect debug information
#If DEBUG Then
                debugData += (item & "=") + Page.Request.QueryString(item) & "<br/>"
#End If

                                ' Collect the data required for the MD5 sugnature if required
                                If Not item.Equals("vpc_SecureHash") Then
                                    rawHashData += Page.Request.QueryString(item)
                                End If
                            Next
                            signature = CreateMD5Signature(rawHashData)
                        End If
                    End If
                End If


                If SECURE_SECRET.Length > 0 Then
                    ' Collect debug information
#If DEBUG Then
            debugData += ("<br/><u>Hash Data Input</u>: " & rawHashData & "<br/><br/><u>Signature Created</u>: ") + signature & "<br/>"
#End If
                    ' Validate the Secure Hash (remember MD5 hashes are not case sensitive)
                    If Page.Request.QueryString("vpc_SecureHash").Equals(signature) Then
                        ' Secure Hash validation succeeded,
                        ' add a data field to be displayed later.
                        Label_HashValidation.Text = "<font color='#00AA00'><b>CORRECT</b></font>"
                    Else
                        ' Secure Hash validation failed, add a data field to be displayed
                        ' later.
                        Label_HashValidation.Text = "<font color='#FF0066'><b>INVALID HASH</b></font>"
                        hashValidated = False
                    End If
                End If
                ' Get the standard receipt data from the parsed response
                txnResponseCode = IIf(Page.Request.QueryString("vpc_TxnResponseCode").Length > 0, Page.Request.QueryString("vpc_TxnResponseCode"), "Unknown")
                Label_TxnResponseCode.Text = txnResponseCode
                Label_TxnResponseCodeDesc.Text = getResponseDescription(txnResponseCode)

                'Label_Amount.Text = IIf(Page.Request.QueryString("vpc_Amount").Length > 0, Page.Request.QueryString("vpc_Amount"), "Unknown")
                Label_Command.Text = IIf(Page.Request.QueryString("vpc_Command").Length > 0, Page.Request.QueryString("vpc_Command"), "Unknown")
                Label_Version.Text = IIf(Page.Request.QueryString("vpc_Version").Length > 0, Page.Request.QueryString("vpc_Version"), "Unknown")
                Label_OrderInfo.Text = IIf(Page.Request.QueryString("vpc_OrderInfo").Length > 0, Page.Request.QueryString("vpc_OrderInfo"), "Unknown")
                Label_MerchantID.Text = IIf(Page.Request.QueryString("vpc_Merchant").Length > 0, Page.Request.QueryString("vpc_Merchant"), "Unknown")

                ' only display this data if not an error condition
                'If Not errorExists AndAlso Not txnResponseCode.Equals("7") Then
                '    Label_BatchNo.Text = IIf(Page.Request.QueryString("vpc_BatchNo").Length > 0, Page.Request.QueryString("vpc_BatchNo"), "Unknown")
                '    Label_CardType.Text = IIf(Page.Request.QueryString("vpc_Card").Length > 0, Page.Request.QueryString("vpc_Card"), "Unknown")
                '    Label_ReceiptNo.Text = IIf(Page.Request.QueryString("vpc_ReceiptNo").Length > 0, Page.Request.QueryString("vpc_ReceiptNo"), "Unknown")
                '    Label_AuthorizeID.Text = IIf(Page.Request.QueryString("vpc_AuthorizeId").Length > 0, Page.Request.QueryString("vpc_AuthorizeId"), "Unknown")
                '    Label_MerchTxnRef.Text = IIf(Page.Request.QueryString("vpc_MerchTxnRef").Length > 0, Page.Request.QueryString("vpc_MerchTxnRef"), "Unknown")
                '    Label_AcqResponseCode.Text = IIf(Page.Request.QueryString("vpc_AcqResponseCode").Length > 0, Page.Request.QueryString("vpc_AcqResponseCode"), "Unknown")
                '    Label_TransactionNo.Text = IIf(Page.Request.QueryString("vpc_TransactionNo").Length > 0, Page.Request.QueryString("vpc_TransactionNo"), "Unknown")
                '    Panel_Receipt.Visible = True
                'End If
                '        title    =null2unknown(Request.QueryString("title"))
                'againLink=null2unknown(Request.QueryString("AgainLink"))
                Dim amount As String = null2unknown(Page.Request.QueryString("vpc_Amount"))
                Dim locale As String = null2unknown(Page.Request.QueryString("vpc_Locale"))
                Dim batchNo As String = null2unknown(Page.Request.QueryString("vpc_BatchNo"))
                Dim command As String = null2unknown(Page.Request.QueryString("vpc_Command"))
                Dim version As String = null2unknown(Page.Request.QueryString("vpc_Version"))
                Dim cardType As String = null2unknown(Page.Request.QueryString("vpc_Card"))
                Dim orderInfo As String = null2unknown(Page.Request.QueryString("vpc_OrderInfo"))
                Dim receiptNo As String = null2unknown(Page.Request.QueryString("vpc_ReceiptNo"))
                Dim merchantID As String = null2unknown(Page.Request.QueryString("vpc_Merchant"))
                Dim authorizeID As String = null2unknown(Page.Request.QueryString("vpc_AuthorizeId"))
                Dim merchTxnRef As String = null2unknown(Page.Request.QueryString("vpc_MerchTxnRef"))
                Dim transactionNo As String = null2unknown(Page.Request.QueryString("vpc_TransactionNo"))
                Dim acqResponseCode As String = null2unknown(Page.Request.QueryString("vpc_AcqResponseCode"))
                ' string txnResponseCode=null2unknown(Page.Request.QueryString["vpc_TxnResponseCode"]);
                ' string   message    =null2unknown(Page.Request.QueryString["vpc_Message"));
                '' 3-D Secure Data
                Dim verType As String = null2unknown(Page.Request.QueryString("vpc_VerType"))
                Dim verStatus As String = null2unknown(Page.Request.QueryString("vpc_VerStatus"))
                Dim token As String = null2unknown(Page.Request.QueryString("vpc_VerToken"))
                Dim verSecurLevel As String = null2unknown(Page.Request.QueryString("vpc_VerSecurityLevel"))
                Dim enrolled As String = null2unknown(Page.Request.QueryString("vpc_3DSenrolled"))
                Dim xid As String = null2unknown(Page.Request.QueryString("vpc_3DSXID"))
                Dim acqECI As String = null2unknown(Page.Request.QueryString("vpc_3DSECI"))
                Dim authStatus As String = null2unknown(Page.Request.QueryString("vpc_3DSstatus"))

                Dim recno As String = "", msgServer As String = "", retval As String = "1000"
                If merchTxnRef = Session("vpc_MerchTxnRef").ToString() Then
                    retval = FeeCollectionOnlineBB.F_SaveFEECOLLECTION_H_ONLINE_PAYMENT_MULTI(Session("vpc_MerchTxnRef").ToString(), _
                             txnResponseCode, getResponseDescription(txnResponseCode), message, receiptNo, transactionNo, _
                              acqResponseCode, authorizeID, batchNo, cardType, hashValidated.ToString(), amount, _
                              orderInfo, merchantID, command, version, _
                              verType & "|" + verStatus & "|" + token & "|" + verSecurLevel & "|" + enrolled & "|" + xid & "|" + acqECI & "|" + authStatus, _
                              recno, msgServer)

                End If
                Try
                    FeeCollectionOnlineBB.SAVE_ONLINE_PAYMENT_AUDIT("TRANSPORT", "RESPONSE", _
                    Page.Request.Url.ToString, Session("vpc_MerchTxnRef").ToString())
                Catch ex As Exception
                End Try
                lblError.CssClass = "alert alert-warning"
                lblError.Text = msgServer

                'lblStudentNo.Text = Session("STU_NO").ToString()
                'lblStudentName.Text = Session("STU_NAME").ToString()
                'Label_MerchTxnRef.Text = Session("vpc_MerchTxnRef").ToString()

                'If IsNumeric(amount) Then
                '    Label_Amount.Text = Format(CDbl(amount) / 100, "0.00")
                'Else
                '    Label_Amount.Text = ""
                'End If

                'lblDate.Text = Format(Date.Now, "dd/MMM/yyyy")
                Gridbind_NewPayDetails(Session("vpc_MerchTxnRef").ToString, True)
                If txnResponseCode = "0" Then
                    user_info.Text = "Thank you for your payment against transport fees. You will shortly receive the receipt by email."
                    user_info.ForeColor = Drawing.Color.Blue
                Else
                    repInfo.Visible = False
                    'user_info.Text = "Your payment is cancelled."
                    'user_info.ForeColor = Drawing.Color.Red
                End If

                lblMessage.Text = "Transport Payment Details"

                'Dim dsData As DataSet = FeeCommon.GETTransportBusDetails(Session("STU_NO").ToString())
                'If Not dsData Is Nothing AndAlso dsData.Tables.Count > 0 AndAlso dsData.Tables(0).Rows.Count > 0 Then
                'Else
                'End If

                If retval = "0" And recno.Trim <> "" Then
                    'Logger.LogInfo("Save succeeded, retval=" & retval & ", recno=" & recno)
                    'UtilityObj.Errorlog("Save succeeded, retval=" & retval & ", recno=" & recno, "BBT_TEST")
                    urcStudentTransportPaidResult1.Gridbind_PayDetails(Session("vpc_MerchTxnRef").ToString, True)
                    '--------Emailing commented by Jacob on 29/Sep/2018, it will be send from email service
                    'Dim bEmailSuccess As Boolean
                    'Dim EmlStatus As String = SendEmail(recno, Session("vpc_MerchTxnRef").ToString, bEmailSuccess)
                    ''lblError.ForeColor = IIf(bEmailSuccess, Drawing.Color.Maroon, Drawing.Color.OrangeRed)
                    lblError.CssClass = "alert alert-success"
                    'lblError.Text = lblError.Text & " " & IIf(EmlStatus.Trim <> "", EmlStatus.Trim & "</br>", "")
                Else
                    'lblError.ForeColor = Drawing.Color.Red
                    lblError.CssClass = "alert alert-danger"
                    urcStudentTransportPaidResult1.Gridbind_PayDetails(Session("vpc_MerchTxnRef").ToString, False)

                End If

                ' if message was not provided locally then obtain value from server
                If message.Length = 0 Then
                    message = IIf(Page.Request.QueryString("vpc_Message").Length > 0, Page.Request.QueryString("vpc_Message"), "Unknown")
                End If
                Session("vpc_MerchTxnRef") = Nothing
            Catch ex As Exception
                message = "(51) Exception encountered. " & ex.Message
                'Logger.LogInfo(ex)
                UtilityObj.Errorlog(ex.Message)
                UtilityObj.Errorlog(ex.ToString())
                If ex.StackTrace.Length > 0 Then
                    Label_StackTrace.Text = ex.ToString()
                    Panel_StackTrace.Visible = True
                End If
                errorExists = True
            End Try

            ' output the message field
            Label_Message.Text = message

            ' The URL AgainLink and Title are only used for display purposes.
            '    * Note: This is ONLY used for this example and is not required for 
            '    * production code.  


            ' Create a link to the example's HTML order page
            Label_AgainLink.Text = "<a href=""" & Page.Request.QueryString("AgainLink") & """>Another Transaction</a>"

            ' Determine the appropriate title for the receipt page
            Label_Title.Text = IIf((errorExists OrElse txnResponseCode.Equals("7") OrElse txnResponseCode.Equals("Unknown") OrElse hashValidated = False), Page.Request.QueryString("Title") & " Error Page", Page.Request.QueryString("Title") & " Receipt Page")

            ' output debug data to the screen
#If DEBUG Then
    debugData += "<br/><u>End of debug information</u><br/>"
    Label_Debug.Text = debugData
#End If

            '
            '    **********************
            '    * END OF MAIN PROGRAM
            '    **********************
            '    *
            '    * FINISH TRANSACTION - Output the VPC Response Data
            '    * =====================================================
            '    * For the purposes of demonstration, we simply display the Result fields
            '    * on a web page.
            '
            Panel_Debug.Visible = True
        Else ''If it's a PostBack
            urcStudentTransportPaidResult1.Visible = False
            spanRedirect.Visible = True
        End If
    End Sub

    Public Sub Gridbind_NewPayDetails(ByVal FCO_ID As Int64, ByVal bPaymentSuccess As Boolean)
        Try
            'Dim strPath As String = Convert.ToString(readerStudent_Detail("STU_PHOTOPATH"))
            Dim connPath As String = WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ToString()
            Dim noImagePath As String = Server.MapPath("Images\no_image.gif")
            Dim con As String = ConnectionManger.GetOASISTRANSPORTConnectionString
            Dim param(4) As SqlClient.SqlParameter

            param(0) = New SqlClient.SqlParameter("@IMAGE_PATH", connPath)
            param(1) = New SqlClient.SqlParameter("@noImagePath", noImagePath)
            param(2) = New SqlClient.SqlParameter("@FCO_ID", FCO_ID)
            param(3) = New SqlClient.SqlParameter("@bPaymentSuccess", bPaymentSuccess)

            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "TRANSPORT.[GET_ALL_PAYMENT_RESULT]", param)

            hfFCO_FCL_RECNO.Value = ds.Tables(0).Rows(0)("FCO_FCL_RECNO")
            hfFCO_DATE.Value = ds.Tables(0).Rows(0)("FCO_DATE")

            repInfo.DataSource = ds.Tables(0)
            repInfo.DataBind()
        Catch ex As Exception
            repInfo.DataBind()
        End Try
    End Sub
    Dim ReptrSum As Double = 0
    Dim ReptrDate As String = ""
    Dim ReptrRefNo As String = ""
    Protected Sub repInfo_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles repInfo.ItemDataBound

        If e.Item.ItemType = ListItemType.Header Then
            Dim Label_MerchTxnRef As New Label
            Dim lblDate As New Label

            ReptrDate = hfFCO_DATE.Value
            ReptrRefNo = hfFCO_FCL_RECNO.Value

            Label_MerchTxnRef = DirectCast(e.Item.FindControl("Label_MerchTxnRef"), Label)
            lblDate = DirectCast(e.Item.FindControl("lblDate"), Label)
            If Not Label_MerchTxnRef Is Nothing Then
                Label_MerchTxnRef.Text = ReptrRefNo.ToString
            End If
            If Not lblDate Is Nothing Then
                lblDate.Text = ReptrDate.ToString
            End If
        End If

        If e.Item.DataItem Is Nothing Then
            Dim lbl_Amount_F As New Label

            'lbl_Amount_F = repInfo.Controls(repInfo.Controls.Count - 1).Controls(0).FindControl("lbl_Amount_F")
            If e.Item.ItemType = ListItemType.Footer Then
                lbl_Amount_F = DirectCast(e.Item.FindControl("lbl_Amount_F"), Label)
            End If


            If Not lbl_Amount_F Is Nothing Then
                lbl_Amount_F.Text = ReptrSum.ToString("#,##0.00")
            End If
            Return
        Else

            Dim ltAmount As New Label

            ltAmount = DirectCast(e.Item.FindControl("Label_Amount"), Label)
            ReptrSum = ReptrSum + IIf(Not ltAmount Is Nothing, CDbl(ltAmount.Text), 0)

            Dim lbl_Amount_F As New Label
            'lbl_Amount_F = repInfo.Controls(repInfo.Controls.Count - 1).Controls(0).FindControl("lbl_Amount_F")
            lbl_Amount_F = DirectCast(e.Item.FindControl("lbl_Amount_F"), Label)

            If Not lbl_Amount_F Is Nothing Then
                lbl_Amount_F.Text = ReptrSum.ToString("#,##0.00")
            End If

        End If
    End Sub

    Private Function SendEmail(ByVal RecNo As String, ByVal FCO_ID As Long, ByRef bEmailSuccess As Boolean) As String
        SendEmail = ""
        'Logger.LogInfo("Start sending Email")
        'UtilityObj.Errorlog("Start sending email", "BBT_TEST")
        Dim TRANS_BSU_ID As String = FeeCommon.GET_TRANSPORT_BSU(Session("STU_BSU_ID"), Session("STU_ID").ToString)
        Dim RptFile As String
        Dim str As String = "SELECT ISNULL(FCL_FCL_ID,0)AS FCL_FCL_ID FROM FEES.FEECOLLECTION_H WITH ( NOLOCK ) WHERE FCL_RECNO='" & RecNo & "' AND FCL_BSU_ID='" & TRANS_BSU_ID & "'" 'Session("sBsuid")
        Dim FCL_FCL_ID As Long = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.Text, str)
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim param As New Hashtable
        Dim rptClass As New rptClass
        param.Add("UserName", "SYSTEM")
        param.Add("@FCL_FCL_ID", FCL_FCL_ID)
        RptFile = Server.MapPath("../Reports/FeeReceipt/rptFeeReceipt.rpt")

        rptClass.reportPath = RptFile
        rptClass.reportParameters = param
        rptClass.crDatabase = ConnectionManger.GetOASISTransportConnection.Database
        Dim rptDownload As New DownloadOrEmail_Receipt
        rptDownload.LogoPath = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT ISNULL(BUS_BSU_GROUP_LOGO,'https://oasis.gemseducation.com/Images/Misc/TransparentLOGO.gif')GROUP_LOGO FROM dbo.BUSINESSUNIT_SUB WHERE BUS_BSU_ID='" & Session("sBsuid") & "'")
        rptDownload.BSU_ID = TRANS_BSU_ID 'Session("sBsuId")
        rptDownload.FCL_FCL_ID = FCL_FCL_ID
        rptDownload.FCO_ID = FCO_ID
        'UtilityObj.Errorlog("Calling Loadreports", "BBT_TEST")
        rptDownload.bEmailSuccess = False
        rptDownload.LoadReports(rptClass)
        SendEmail = rptDownload.EmailStatusMsg
        bEmailSuccess = rptDownload.bEmailSuccess
        rptDownload = Nothing
    End Function
End Class
