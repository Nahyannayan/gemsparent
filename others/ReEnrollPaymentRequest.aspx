﻿<%@ Page Language="VB" MasterPageFile="~/ParentMaster.master" AutoEventWireup="false" CodeFile="ReEnrollPaymentRequest.aspx.vb" Inherits="Others_ReEnrollPaymentRequest"  title="GEMS EDUCATION" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphParent" Runat="Server">
<div><asp:Label ID="lblPayMsg" runat="server" Font-Names="Verdana" Font-Size="11pt"
                                EnableViewState="False"></asp:Label>
                               <asp:Label ID="lblError" runat="server" Font-Names="Verdana" ForeColor="Red" Font-Size="11pt"
                                EnableViewState="False"></asp:Label >        
                                </div>
                <table  align="center" style="width: 98%; height: 110px;"  cellpadding="8" cellspacing="0" class="BlueTable_simple">
                                       <tr class="trSub_Header">
                        <td colspan="2" align="left" style="height: 18px">
                            Re-enrolment Fee Online Payment
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="tdfields" width="20%">
                            Date
                        </td>
                        <td align="left" >
                            <asp:Label ID="lblDate" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="tdfields">
                            Student ID
                        </td>
                        <td align="left" >
                            <asp:Label ID="txtStdNo" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="tdfields">
                            Student Name
                        </td>
                        <td align="left" >
                            <asp:Label ID="txtStudentname" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="tdfields">
                            Academic Year
                        </td>
                        <td align="left" >
                            <asp:Label ID="lblacademicyear" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="tdfields">
                            Grade &amp; Section
                        </td>
                        <td align="left" >
                            <asp:Label ID="stugrd" runat="server"></asp:Label>
                            -<strong></strong>
                            <asp:Label ID="lblsct" runat="server"></asp:Label>
                            &nbsp; &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="tdfields">
                            School
                        </td>
                        <td align="left" >
                            <asp:Label ID="lblschool" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="tdfields">
                            Re-enrolment Fee
                           <asp:Label ID="lblCurrency" runat="server"></asp:Label>                      </td>
                        <td align="left" >
                           <div ID="divAMT" runat="server"></div>
                        </td>
                    </tr>
                    </table>
                     <table id="Table1" style="width: 98%" runat="server" class="tableNoborder">
                    <tr id="Tr1" runat="server">
                        <td align="center" style="font-size: 12px; color: #ff0000; height: 18px">
                            &nbsp;<asp:Button ID="btnSave" runat="server" CssClass="buttons" TabIndex="155" Text="Confirm & Proceed For Online Payment" width="220px"/>
                          
                        </td>
                    </tr>
                </table>
</asp:Content>

