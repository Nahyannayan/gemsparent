<%@ Page Language="VB" MasterPageFile="~/ParentMaster.master" AutoEventWireup="false"
    CodeFile="ReEnroll_Process.aspx.vb" Inherits="Others_ReEnroll_Process" Title="GEMS EDUCATION"
    MaintainScrollPositionOnPostback="false" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphParent" runat="Server">
    <script  type="text/javascript" src="../Scripts/iframeResizer.min.js"></script>
    <script language="javascript" type="text/javascript">
    
   
    
    function CheckValid()
   {
   
 if (document.getElementById("<%=chkYes.ClientId %>").checked)
    {
     if (document.getElementById("<%=chkagree.ClientId %>").checked)
       {
       document.getElementById("<%=divNote.ClientId %>").style.display = 'none';
       return true;
       }
       else
       {
       document.getElementById("<%=chkagree.ClientId %>").focus();
      // alert("You must accept the Declaration by ticking on the checkbox");
       document.getElementById("<%=divNote.ClientId %>").style.display = 'block';
       document.getElementById("<%=lblError.ClientId %>").innerHTML = "";
       document.getElementById("<%=lblError.ClientId %>").innerHTML = "&nbsp;&nbsp;You must accept the Declaration by ticking on the checkbox <br />";
       return false;
       }
    }
   }
    
     function CheckStatus(obj,var1)
    {
  
if (var1==1)
{
 obj.checked=true;
 document.getElementById("<%=chkNo.ClientId %>").checked=false;
  document.getElementById("<%= divDeclaration.ClientId %>").style.display = 'block';
 document.getElementById("<%= divDeclaration2.ClientId %>").style.display = 'block';
}
else
{
obj.checked=true;
 document.getElementById("<%=chkYes.ClientId %>").checked=false;
 document.getElementById("<%= divDeclaration.ClientId %>").style.display = 'none';
 document.getElementById("<%= divDeclaration2.ClientId %>").style.display = 'none';
}
                
  }
 function CheckStatusStaff(obj,var1)
    {
  
if (var1==1)
{
 obj.checked=true;
 document.getElementById("<%=chkGEMS_N.ClientId %>").checked=false;
 
 if (document.getElementById("<%=chkYes.ClientId %>").checked)
 {
  document.getElementById("<%= divDeclaration.ClientId %>").style.display = 'block';
 document.getElementById("<%= divDeclaration2.ClientId %>").style.display = 'block';
 }
// document.getElementById("<%=trGEMStaff2.ClientId %>").style.display = "block";
}
else
{
obj.checked=true;
 document.getElementById("<%=chkGEMS_Y.ClientId %>").checked=false;
  if (document.getElementById("<%=chkYes.ClientId %>").checked)
 {
  document.getElementById("<%= divDeclaration.ClientId %>").style.display = 'block';
 document.getElementById("<%= divDeclaration2.ClientId %>").style.display = 'block';
 }
 //document.getElementById("<%=trGEMStaff2.ClientId %>").style.display = "none";
}
                
  }
    
     function CheckNBAD(obj,var1)
    {
  
if (var1==1)
{
 obj.checked=true;
 document.getElementById("<%=chkNBAD_NO.ClientId %>").checked=false;

 
}
else
{
obj.checked=true;
 document.getElementById("<%=chkNBAD_YES.ClientId %>").checked=false;

}
                
  }
    
    
   
    





 

    </script>
    <asp:HiddenField id="hdn_stuno" runat="server"/>
     <asp:HiddenField id="hdn_bsuid" runat="server"/>
     <div class="content margin-top30 margin-bottom60">
        <div class="container">
            <div class="row">

                <!-- Posts Block -->
                <div class="posts-block col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div id="divOasis_Reenrol" runat="server">
    <div class="mainheading">
        <div class="left">
            Re-Enrolment</div>
        <div class="right">
            <asp:label ID="lbChildName" runat="server" CssClass="lblChildNameCss"></asp:label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
    </div>
    <div align="center" style="text-align: center;">
        <asp:Label ID="lbmsgInfo" runat="server"></asp:Label>
        <div align="center" style="text-align: center;" id="divInitalState" runat="server">
            <table class="tableNoborder" width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td colspan="2" align="left">
                        <div style="text-align: left; font-family: segoe ui, verdana, arial, sans-serif;
                            font-size: 12px; font-weight: 700; color: #084777; padding-top: 4pt;">
                            We are currently enrolling for the next academic year. Please indicate your intention
                            regarding your child's re-enrolment/re-registration for <span id="SpMTN_FULL" runat="server">
                            </span> by submitting this form.
                        </div>
                        <br />
                    </td>
                </tr>
                <tr class="trSub_Header_detail" id="trCurrH" runat="server">
                    <td colspan="2">
                        Select the curriculum for the next academic year</td>
                </tr>
                <tr id="trCurrContent" runat="server" class="tdblankAll">
                    <td align="left" width="500px" class="tdblankAll" valign="top">
                        <font style="font-size: 12px !important; color: #084777;">Choose curriculum</font></td>
                    <td class="tdblankAll" align="left">
                        <asp:DropDownList ID="ddlStu_Curr" CssClass="dropDownList" runat="server">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr class="trSub_Header_detail" id="trEnroll" runat="server">
                    <td colspan="2">
                        PLEASE SELECT THE APPROPRIATE BOX:</td>
                </tr>
                <tr id="trGEMStaff1" runat="server" class="tdblankAll">
                    <td align="left" class="tdblankAll" valign="top">
                        <font style="font-size: 12px !important; color: #084777;">Is Father or Mother GEMS staff
                            ?</font></td>
                    <td class="tdblankAll" align="left">
                        <asp:CheckBox ID="chkGEMS_Y" runat="server" CssClass="checkboxAnsBold" Text=" YES"
                            onclick="CheckStatusStaff(this,1)" AutoPostBack="true" />
                        <asp:CheckBox ID="chkGEMS_N" runat="server" CssClass="checkboxAnsBold" Text=" NO"
                            onclick="CheckStatusStaff(this,2)" AutoPostBack="true" /><br />
                    </td>
                </tr>
                <tr id="trGEMStaff2" runat="server">
                    <td colspan="2">
                        <div style="border: solid 1px #d3e3ec !important; padding-left: 12px; padding-top: 12px;
                            padding-bottom: 12px;">
                            <div class="remark_info" style="padding-top: 4px; padding-bottom: 4px;">
                                If you choose 'YES', please enter GEMS GLG id and password</div>
                            <div style="padding-top: 4px; padding-bottom: 4px; font-size: 12px;">
                                Enter GEMS official id <span style="color: red;">*</span>
                                <asp:TextBox ID="txtUsr_id" runat="server" MaxLength="250" Width="160" CssClass="input">
                                </asp:TextBox>
                                Enter 8 digit Staff No<span style="color: red;">*</span>
                                <asp:TextBox ID="txtStaffNo" runat="server" MaxLength="8" Width="160" CssClass="input">
                                </asp:TextBox>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <asp:Button ID="btnVerify" runat="server" CssClass="buttonsTextSize" Text="Verify"
                                    Width="90px" Visible="false" />
                                <ajaxToolkit:FilteredTextBoxExtender ID="ftbeStaffNo" runat="server" TargetControlID="txtStaffNo"
                                    FilterType="Numbers">
                                </ajaxToolkit:FilteredTextBoxExtender>
                                <ajaxToolkit:FilteredTextBoxExtender ID="ftbusrId" runat="server" TargetControlID="txtUsr_id"
                                    FilterType="Custom,UppercaseLetters,Numbers,LowercaseLetters" InvalidChars="@"
                                    FilterMode="InvalidChars">
                                </ajaxToolkit:FilteredTextBoxExtender>
                                <div style="padding-top: 3px;">
                                    <div class="remark">
                                        Format ( e.g. mohd.i_cis / m.iqbal_cis)</div>
                                </div>
                            </div>
                            <asp:label ID="lblmsgverify" runat="server"></asp:label>
                        </div>
                        <br />
                    </td>
                </tr>
                <tr class="tdblankAll" id="trGEMStaff3" runat="server">
                    <td colspan="2">
                        <hr />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <div class="divEnroll">
                            <div class="divEnrollBox">
                                <div class="divEnrollBoxStatus">
                                    <asp:CheckBox ID="chkYes" runat="server" CssClass="checkboxAns" Text="YES" onclick="CheckStatus(this,1)" /></div>
                            </div>
                            <div id="spYes" runat="server" class="divEnrollText">
                            </div>
                        </div>
                        <hr />
                        <br />
                        <div class="divEnroll">
                            <div class="divEnrollBox">
                                <div class="divEnrollBoxStatus">
                                    <asp:CheckBox ID="chkNo" runat="server" CssClass="checkboxAns" Text="NO" onclick="CheckStatus(this,2)" /></div>
                            </div>
                            <div id="SpNo" runat="server" class="divEnrollText">
                            </div>
                        </div>
                    </td>
                </tr>
                <tr class="trSub_Header_detail" id="divDeclaration" runat="server" style="display: none;">
                    <td colspan="2">
                        Declaration
                    </td>
                </tr>
                <tr id="divDeclaration2" runat="Server" align="left" style="text-align: left; display: none;">
                    <td align="left" class="tdfields">
                        <div style="padding-top: 10px; padding-bottom: 5px;">
                            I understand and agree that:</div>
                        <div style="padding-top: 10px; padding-bottom: 3px; color: #084777; font-weight: normal;">
                            1. Unregistered children, as well as, children with an outstanding tuition fee balance
                            will not be able to be enrolled.
                        </div>
                        <div style="padding-top: 10px; padding-bottom: 3px; color: #084777; font-weight: normal;">
                            2. Tuition fees for the next academic year have not been determined.</div>
                        <div style="padding-top: 10px; padding-bottom: 3px; color: #084777; font-weight: normal;">
                            3. Terms and conditions communicated by the school in writing at the time of admissions,
                            continue to apply.
                        </div>
                        <div style="padding-top: 10px; padding-bottom: 3px; color: #084777; font-weight: normal;">
                            4. My child�s seat is not secured, unless I pay the re-enrolment fee by the date
                            indicated by the school.
                        </div>
                        <div style="padding-top: 10px; padding-bottom: 3px; color: #084777; font-weight: normal;">
                            <asp:CheckBox ID="chkagree" runat="server" CssClass="checkboxAns" />
                            Ticking the declaration box (as a substitute for your signature) is to confirm that
                            you agree to the above declaration.</div>
                        
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="2">
                    <div id="divNote" class="msgInfoBox msgInfoError2" runat="server" title="" style="display: none;">
                            <span class="msgInfoclose"></span>
                            <asp:Label ID="lblError" runat="server" Text="You must accept the Declaration by ticking on the checkbox"
                                EnableViewState="false"></asp:Label>
                        </div>
                        <asp:Button ID="btnProceed" runat="server" CssClass="buttons" Text="Proceed" onClientClick="javascript:return CheckValid();"
                            Width="90px" />
                    </td>
                </tr>
                <tr style="display: none;">
                    <td align="center" colspan="2" class="tdblankAll">
                        <div class="divinfoInnerBotEnrol">
                            <asp:Literal ID="lt_Footer3" runat="server"></asp:Literal>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <div align="center" style="text-align: left;" id="divACKState" runat="server">
        </div>
    </div>
    <asp:Panel ID="pnlSTBooking" runat="server" CssClass="darkPanlvisible" Visible="false">
        <div class="panelGEMSCard">
            <img width="100%" height="70px" src="../Images/Card/image001.png" alt="banner" style="margin-top: 0px !important;" />
            <div style="height: 300px; overflow: auto;">
                <table align="left" width="100%" class="tableNoborder" style="padding-top: 0px !important;">
                    <tr>
                        <td colspan="2" align="center">
                            <div style="padding: 15px 0px 12px 0px; font-weight: bold; font-size: 14px;">
                                Save on School fee with NBAD GEMS Co-brand Credit Card</div>
                            <div style="padding: 2px 0px 12px 0px; font-size: 12px; color: #2AA448; font-style: italic;">
                                Made especially for the GEMS Schools� parent community!</div>
                            <img src="../Images/Card/image013.png" alt="" style="width: 241px !important; height: 188px !important;" />
                            <div style="margin-top: -16px; width: 98%;">
                                <div style="width: 93%; font-size: 13px; text-align: justify; float: right; line-height: 116%;
                                    padding-bottom: 8px;">
                                    With the NBAD GEMS Credit Card, you can now provide your children the caliber of
                                    education that guarantees them a bright future, without worrying about the financial
                                    requirements. Here is a quick look at some of the great features that await you.</div>
                            </div>
                        <%--</td>--%>
                    </tr>
                    <tr>
                        <td width="40px" valign="top">
                            <img src="../Images/Card/image008_7Perc.png" alt="" style="width: 63px !important; height: 80px !important;" /></td>
                        <td valign="top" style="padding: 0px;">
                            <div style="padding-top: 0px; padding-left: 4px; text-align: justify; font-size: 10.0pt;">
                                <font style="font-family: Wingdings; color: #318E3D; font-weight: bold">�</font><b
                                    style="color: #318E3D; font-family: Tahoma;">Save on your GEMS School fee. The earlier
                                    you pay, the higher the discount!</b>
                                <div style="padding-left: 20px; line-height: 116%; width: 96%">
                                    By using the NBAD GEMS Credit Card at GEMS Schools to pay for your child�s tuition
                                    fees , you can benefit from a discount. Contact the Accounts Officer of your child�s
                                    GEMS School for further details.</div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td width="40px" valign="top">
                            <img src="../Images/Card/image010.png" alt="" style="width: 72px !important; height: 55px !important;" /></td>
                        <td valign="top" style="padding: 0px;">
                            <div style="padding-top: 0px; padding-left: 4px; text-align: justify; font-size: 10.0pt;">
                                <font style="font-family: Wingdings; color: #318E3D; font-weight: bold">�</font><div
                                    style="width: 95%; color: #318E3D; margin-top: -17px; font-family: Tahoma; font-weight: bold;
                                    padding-left: 18px; padding-right: 2px;">
                                    Convert the school fee paid into 0% interest Easy Payment Plan (EPP) for up to 12
                                    months</div>
                                <div style="padding-left: 20px; line-height: 116%; width: 96%; padding-bottom: 12px;">
                                    Over and above the discount you can also benefit from the convenience of converting
                                    the fees paid into a 0% interest EPP for up to 12 months. Once you have paid the
                                    fees simply call 800 2211 to convert the transaction into a 0% EPP.</div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td width="40px" valign="top">
                            <img src="../Images/Card/image003.png" alt="" style="width: 100% !important; height: 50px !important;" /></td>
                        <td valign="top" style="padding: 0px;">
                            <div style="padding-top: 0px; padding-left: 4px; text-align: justify; font-size: 10.0pt;">
                                <font style="font-family: Wingdings; color: #318E3D; font-weight: bold">�</font><b
                                    style="color: #318E3D; font-family: Tahoma;">Enjoy complimentary airport lounge
                                    access and Meet &amp; Greet services</b>
                                <div style="padding-left: 20px; line-height: 116%; width: 96%; padding-bottom: 12px;">
                                    Enjoy complimentary meet &amp; greet services at Dubai and Abu Dhabi with your NBAD
                                    GEMS Credit Card. You can also benefit from Airport lounge access at Abu Dhabi,
                                    Dubai, Kuwait, Cairo, Jeddah and Riyadh.</div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td width="40px" valign="top">
                            <img src="../Images/Card/image011.png" alt="" style="width: 85px !important; height: 32px !important;" /></td>
                        <td valign="top" style="padding: 0px;">
                            <div style="padding-top: 0px; padding-left: 4px; text-align: justify; font-size: 10.0pt;">
                                <font style="font-family: Wingdings; color: #318E3D; font-weight: bold">�</font><b
                                    style="color: #318E3D; font-family: Tahoma;">Earn 1.5 NBAD Stars on every AED 1
                                    purchase</b>
                                <div style="padding-left: 20px; line-height: 116%; width: 96%; padding-bottom: 12px;">
                                    NBAD Stars is the exciting rewards programme from NBAD that offers you greater choice,
                                    flexibility and benefits. You earn 1.5 NBAD Stars for every Dirham of purchase on
                                    your NBAD GEMS Credit Card. You can redeem your NBAD Stars for Emirates Skywards
                                    Miles, flydubai e-vouchers or at our numerous partners in the UAE.</div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td width="40px" valign="top">
                            <img src="../Images/Card/image012.jpg" alt="" style="width: 70px !important; height: 14px !important;" /><br />
                            <img src="../Images/Card/image005.png" alt="" style="width: 52px !important; height: 50px !important;" />
                            <img src="../Images/Card/image006.png" alt="" style="width: 40px !important; height: 39px !important;" />
                        </td>
                        <td valign="top" style="padding: 0px;">
                            <div style="padding-top: 0px; padding-left: 4px; text-align: justify; font-size: 10.0pt;">
                                <font style="font-family: Wingdings; color: #318E3D; font-weight: bold">�</font><b
                                    style="color: #318E3D; font-family: Tahoma;">Many other value added offers</b>
                                <div style="padding-left: 20px; line-height: 116%; width: 96%; font-size: 10.0pt !important;">
                                    The NBAD GEMS Credit Card also comes with many other value added benefits like:<div
                                        style="margin: 0in 0in .0001in 1.25in; margin-bottom: .0001pt; text-align: justify;
                                        text-indent: -.25in;">
                                        <span style="font-family: Symbol;">�</span>&nbsp;No annual fee; it�s free for life</div>
                                    <p style="margin: 0in 0in .0001in 1.25in; text-align: justify; text-indent: -.25in;">
                                        <span style="font-family: Symbol;">�</span>&nbsp;Watch movies from just AED 10</p>
                                    <p style="margin: 0in 0in .0001in 1.25in; text-align: justify; text-indent: -.25in;">
                                        <span style="font-family: Symbol;">�</span>&nbsp;First card in the UAE made from
                                        biodegradable material</p>
                                    <p style='margin-bottom: 0in; margin-bottom: .0001pt; text-align: justify'>
                                        To apply for the NBAD GEMS Credit Card or to get further information about the product
                                        features and the documentation requirements, write to us at <a href="mailto:GEMS@nbad.com"
                                            style="display: inline !important; font-size: 10.0pt; text-decoration: underline !important;">
                                            GEMS@nbad.com</a> or call us at 800 2211. Once your complete set of documentation
                                        is received, your card will be delivered to you within 2 weeks.</p>
                                    <p style="margin-bottom: 0in; margin-bottom: .0001pt; color: #2AA448;">
                                        The NBAD GEMS Team
                                    </p>
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="text-align: center; width: 100%; border-top: solid 1px #ccc; padding-top: 0px;
                overflow: auto;">
                <table class="tableNoborder" cellpadding="0" cellspacing="0" width="100%">
                    <tr class="tdblankAll">
                        <td align="left" class="tdblankAll" valign="middle">
                            <asp:label ID="lblAvailCard" runat="server" CssClass="tdfields" text="Do you want to apply for NBAD GEMS credit card ?">
                            </asp:label><asp:CheckBox ID="chkNBAD_YES" runat="server" CssClass="checkboxAnsBold"
                                Text=" YES" onclick="CheckNBAD(this,1)" />&nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:CheckBox ID="chkNBAD_NO" runat="server" CssClass="checkboxAnsBold" Text=" NO"
                                onclick="CheckNBAD(this,2)" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div style="text-align: center; width: 100%; border-top: solid 1px #ccc; padding-top: 4px;
                                padding-bottom: 4px;">
                                <asp:Button ID="btnPayment" runat="server" CssClass="buttons" Text="Proceed To Online Payment"
                                    Width="155px" />
                                &nbsp;<asp:Button ID="btnPaySchool" runat="server" CssClass="buttons" Text="Pay At School"
                                    Width="155px" />
                                &nbsp;
                                <asp:Button ID="btnConfirm" runat="server" CssClass="buttons" Text="Confirm" CausesValidation="False"
                                    Width="140px" />
                            </div>
                            <asp:Label ID="lblpopMsg" runat="server"></asp:Label>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="plTcReq" runat="server" CssClass="darkPanlConfirm" Visible="false">
        <div class="panelConfirm">
            <div class="confirmheading">
                <asp:Label ID="lblPopHead" runat="server" Text=" Transfer Certificate Request" Font-Size="11px"
                    Font-Bold="true"></asp:Label>
                <span style="clear: right; display: inline; float: right; visibility: visible; margin-top: 0px;
                    vertical-align: top;">
                    <asp:ImageButton ID="btnCloseTC" runat="server" ImageUrl="~/Images/Common/PageBody/error_s.png" /></span>
            </div>
            <div style="text-align: center; font-size: 15px;">
                <table class="tableNoborder" width="100%" cellpadding="8" style="height: 130px !important;">
                    <tr>
                        <td align="center" style="height: 60px; padding-top: 20px; color: #084777;">
                            <asp:Literal ID="ltTCmsg" runat="server"></asp:Literal>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" style="padding-top: 16px;">
                            <asp:Button ID="btntcYes" runat="server" Text="Yes" CssClass="buttons" Width="80px" />
                            <asp:Button ID="btnTCNo" runat="server" Text="No" CssClass="buttons" Width="80px" />
                            <asp:Button ID="btnTCClosemsg" runat="server" Text="Close" CssClass="buttons" Width="80px" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </asp:Panel>
</div>
    <div id="divSF_Reenrol" runat="server" visible="false">
    <iframe   id="ifReenrol" runat="server"  width="100%" frameborder="0" style="min-height:500px;"  >
 
</iframe>
        </div>

</div>
                <!-- /Posts Block -->
            </div>
        </div>
   </div>
    <script type="text/javascript">
        document.getElementById("<%=ifReenrol.ClientID%>").onload = resizeFrame;
        function resizeFrame() {
            iFrameResize({ log: false }, document.getElementById("<%=ifReenrol.ClientID%>"));
        }
        </script>
</asp:Content>
