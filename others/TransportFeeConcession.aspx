﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ParentMaster.master" AutoEventWireup="false" CodeFile="TransportFeeConcession.aspx.vb" Inherits="Others_TransportFeeConcession" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphParent" runat="Server">
   <script src="../Scripts/jquery-1.9.1.js" type="text/javascript"></script>
    <!-- Add fancyBox -->
    <script type="text/javascript" src="../Scripts/Fancybox/jquery.fancybox.js?v=2.1.5"></script>
    <script type="text/javascript" src="../Scripts/Fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
    <link rel="stylesheet" href="../Scripts/Fancybox/jquery.fancybox.css" type="text/css" media="screen" />
    <script type="text/javascript" src="../Scripts/PopupJQuery.js"></script>

    <script type="text/javascript" src="../Scripts/PopupJQuery.js"></script>
    <script type="text/javascript" lang="javascript">
     
        function CallPopup(ddlacd, lnkssv, stuid, empid) {
            var ssvid = $('#' + lnkssv).text();
            var acdid = $('#' + ddlacd).val();
            //if (CheckService(lnkssv) == true)
            //    ShowSubWindow('ApplyTransportConcession.aspx?EMP_ID=' + empid + '&STU_ID=' + stuid + '&SSV_ID=' + ssvid + '&ACD_ID=' + acdid + '', '', '50%', '50%');

            $.fancybox({
                type: 'iframe',
                maxWidth: 600,
                href: 'ApplyTransportConcession.aspx?EMP_ID=' + empid + '&STU_ID=' + stuid + '&SSV_ID=' + ssvid + '&ACD_ID=' + acdid ,
                maxHeight: 600,
                fitToView: false,
                width: '95%',
                height: '70%',
                autoSize: false,
                closeClick: false,
                openEffect: 'none',
                closeEffect: 'none',
                topRatio: 0,
                'onComplete': function () {
                    $("#fancybox-wrap").css({ 'top': '20px', 'bottom': 'auto' });
                }
            });
        }

    </script>


    <div class="content margin-top30 margin-bottom60">
        <div class="container">
            <div class="row">

                <!-- Posts Block -->
                <div class="posts-block col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="my-account">
                        <div class="mainheading">
                            <div class="left">Transport Fee Concession</div>
                            <div class="right">
                                <asp:Label ID="lbEmpName" runat="server" CssClass="lblChildNameCss"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </div>
                        </div>
                        <table class="table table-striped table-bordered table-responsive text-left my-orders-table"  width="100%">
                            <tr class="trSub_Header" align="left" style="">
                                <td class="tdblankAll" align="left" colspan="4">Concession Eligibility
                                </td>
                            </tr>
                            <tr class="tdblankAll">
                                <td colspan="4" style="text-align: center !important;">
                                    <div style="width: 100%; text-align: center !important; align-content: center; clear: both !important;" align="center">
                                        <asp:GridView ID="gvFeeBenefits" runat="server" AutoGenerateColumns="False" EmptyDataText="No Details Added"
                                             CssClass="table table-striped table-bordered table-responsive text-left my-orders-table" Width="100%" EnableModelValidation="True">
                                            <Columns>
                                                <asp:BoundField HeaderText="Fee Type" DataField="FEE_DESCR" />
                                                <asp:BoundField DataField="AMT" HeaderText="Amount/Percentage">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="NOS" HeaderText="No. of Children(Eligible)">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                            </Columns>

                                        </asp:GridView>
                                    </div>
                                </td>
                            </tr>
                            <tr class="trSub_Header" align="left" style="">
                                <td class="tdblankAll" align="left" colspan="4">Active Concessions
                                </td>
                            </tr>
                            <tr class="tdblankAll" align="center" style="">
                                <td class="tdblankAll" align="center" colspan="4">
                                    <asp:GridView runat="server" ID="gvFeeConcessionDetails" EmptyDataText="No details"
                                        Width="100%" AutoGenerateColumns="False"  CssClass="table table-striped table-bordered table-responsive text-left my-orders-table" EnableModelValidation="true">
                                        <Columns>
                                            <asp:BoundField DataField="STUDENT NO" HeaderText="Student Id." />
                                            <asp:BoundField DataField="NAME" HeaderText="Name" />
                                            <asp:BoundField DataField="GRADE" HeaderText="Grade" />
                                            <asp:BoundField DataField="CURRICULUM" HeaderText="Curriculum" />
                                            <asp:BoundField DataField="ACADEMIC YEAR" HeaderText="Academic Year" />
                                            <asp:BoundField DataField="FEE TYPE" HeaderText="Fee Type" />
                                            <asp:BoundField DataField="ACTUAL AMOUNT" HeaderText="Actual Fee" />
                                            <asp:BoundField DataField="CONCESSION AMOUNT" HeaderText="Concession Amount" />
                                            <asp:BoundField DataField="CONC PERC" HeaderText="Concession Percent" />
                                        </Columns>
                                    </asp:GridView>
                                </td>
                            </tr>
                            <tr class="trSub_Header" align="left" style="">
                                <td class="tdblankAll" align="left" colspan="4">Apply Concessions
                                </td>
                            </tr>
                            <tr class="tdblankAll" align="center" style="text-align: center !important;">
                                <td class="tdblankAll" align="center" style="text-align: center !important;" colspan="4">
                                    <% If Me.lblPopError.Text <> "" Then%>
                                    <div style="width: 100%; text-align: center !important;">

                                        <asp:Label ID="lblPopError" Width="80%" runat="server"></asp:Label>

                                    </div>
                                    <% End If%>
                                </td>
                            </tr>
                            <tr class="tdblankAll" align="left" style="">
                                <td class="tdblankAll" align="left" colspan="4">
                                    <asp:GridView runat="server" ID="gvApplyConcession" EmptyDataText="No Transaction details added yet."
                                        Width="100%" AutoGenerateColumns="False"  CssClass="table table-striped table-bordered table-responsive text-left my-orders-table" EnableModelValidation="True" DataKeyNames="STU_ID,STU_ACD_ID,SSV_ID,SCR_ID,APPR_STATUS,bOPEN">
                                        <Columns>
                                            <asp:BoundField DataField="STU_NO" HeaderText="StudentId" />
                                            <asp:BoundField DataField="STU_NAME" HeaderText="Name" />
                                            <asp:BoundField DataField="GRD_DISPLAY" HeaderText="Grade" />
                                            <asp:TemplateField HeaderText="Academic Year">
                                                <ItemTemplate>
                                                    <asp:DropDownList ID="ddlAcdYear" AutoPostBack="true" runat="server" DataValueField="ACD_ID" DataTextField="ACY_DESCR" OnSelectedIndexChanged="ddlAcdYear_SelectedIndexChanged">
                                                    </asp:DropDownList>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="FEE_TYPE" HeaderText="Fee Type" />

                                            <asp:TemplateField HeaderText="Service.RefNo">
                                                <ItemTemplate>
                                                    <%--<asp:LinkButton ID="lnkSSVID" CssClass="currLink" Text='<%# Bind("SSV_ID") %>' runat="server"></asp:LinkButton>--%>
                                                    <asp:Label ID="lnkSSVID" CssClass="currLink" runat="server" Text='<%# Bind("SSV_ID") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Status">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkApply" runat="server" CssClass="linkText" OnClick="lnkApply_Click">Apply</asp:LinkButton>
                                                    <asp:LinkButton ID="lnkStatus" runat="server" CssClass="linkText" Visible="false"></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        function CheckService(lnkSSV) {
            if ($(lnkSSV).val() == "" || $(lnkSSV).val() == "0") {
                alert("Please apply for Transport service");
                return false;
            }
            else
                return true;
        }
    </script>
</asp:Content>

