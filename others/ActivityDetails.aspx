﻿<%@ Page Language="VB" AutoEventWireup="true"  CodeFile="ActivityDetails.aspx.vb" Inherits="Others_ActivityDetails" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>


<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head runat="server">
<title ></title>   
    <%--<script type="text/javascript" src="/GEMSPARENTLITEV2/Scripts/jquery-1.8.2.min.js"></script>
    <script type="text/javascript" src="/GEMSPARENTLITEV2/Scripts/jquery-ui-1.10.2.js"></script>  
    <script type="text/javascript" src="/GEMSPARENTLITEV2/Scripts/Fancybox/jquery.fancybox.pack.js?1=2"></script>
    <script type="text/javascript" src="/GEMSPARENTLITEV2/Scripts/Fancybox/jquery.fancybox.js?1=2"></script>
    <link type="text/css" href="/GEMSPARENTLITEV2/Scripts/Fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
    <link href="/GEMSPARENTLITEV2/css/Popup.css" rel="stylesheet" />
    <link href="/GEMSPARENTLITEV2/css/SiteStyle.css?1=2" rel="stylesheet" type="text/css" />
    <link href="/GEMSPARENTLITEV2/css/MenuStyle.css?1=2" rel="stylesheet" type="text/css" />
    <link href="/GEMSPARENTLITEV2/css/StyleBox.css?1=2" rel="stylesheet" type="text/css" />
    <link href="/GEMSPARENTLITEV2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="/GEMSPARENTLITEV2/css/bootstrap.css" rel="stylesheet" type="text/css" />--%>
    
    
        

    <script type="text/javascript" src="/Scripts/jquery-1.8.2.min.js"></script>
    <script type="text/javascript" src="/Scripts/jquery-ui-1.10.2.js"></script>  
    <script type="text/javascript" src="/Scripts/Fancybox/jquery.fancybox.pack.js?1=2"></script>
    <script type="text/javascript" src="/Scripts/Fancybox/jquery.fancybox.js?1=2"></script>
    <link type="text/css" href="/Scripts/Fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
    <link href="/css/Popup.css" rel="stylesheet" />
    <link href="/css/SiteStyle.css?1=2" rel="stylesheet" type="text/css" />
    <link href="/css/MenuStyle.css?1=2" rel="stylesheet" type="text/css" />
    <link href="/css/StyleBox.css?1=2" rel="stylesheet" type="text/css" />
    <link href="/css/style.css" rel="stylesheet" type="text/css" />
    <link href="/css/bootstrap.css" rel="stylesheet" type="text/css" />

    <style>
            
          .textareaO_AD {
              width :80%; 
              border-radius :8px;
              height: 90px !important;            
          }   
           .textcntO_AD {
              width :20%; 
              border-radius :8px;              
          }     
            .btnWidthO_AD {
                background: rgb(179,220,237); /* Old browsers */
                background: -moz-linear-gradient(top, rgba(179,220,237,1) 0%, rgba(41,184,229,1) 50%, rgba(188,224,238,1) 100%); /* FF3.6-15 */
                background: -webkit-linear-gradient(top, rgba(179,220,237,1) 0%,rgba(41,184,229,1) 50%,rgba(188,224,238,1) 100%); /* Chrome10-25,Safari5.1-6 */
                background: linear-gradient(to bottom, rgba(179,220,237,1) 0%,rgba(41,184,229,1) 50%,rgba(188,224,238,1) 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
                filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#b3dced', endColorstr='#bce0ee',GradientType=0 ); /* IE6-9 */
                width:40%;
                height:32px;
                border-radius :4px;
                }

         .btnWidthO_AD:hover {
                background: rgb(132,209,237); /* Old browsers */
                background: -moz-linear-gradient(top, rgba(132,209,237,1) 0%, rgba(20,178,226,1) 50%, rgba(132,209,237,1) 100%); /* FF3.6-15 */
                background: -webkit-linear-gradient(top, rgba(132,209,237,1) 0%,rgba(20,178,226,1) 50%,rgba(132,209,237,1) 100%); /* Chrome10-25,Safari5.1-6 */
                background: linear-gradient(to bottom, rgba(132,209,237,1) 0%,rgba(20,178,226,1) 50%,rgba(132,209,237,1) 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
                filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#84d1ed', endColorstr='#84d1ed',GradientType=0 ); /* IE6-9 */
                }
         .gradO_AD {
                  background: linear-gradient(#e6f2ff, #99ccff);
                  font-size :10px;
                  padding:5px;
                  border-radius:4px;
                }
                      </style>
</head>
<body>
  <form id="form1" runat="server">
      <asp:ScriptManager ID="scriptmanager1" runat="server">
      </asp:ScriptManager>
      
      <asp:UpdatePanel ID="upd1" runat="server">
          <ContentTemplate>
              <div class="popup-wrap">
          <asp:UpdatePanel ID="UpdatePanel1" runat="server">
              <ContentTemplate>

                  <div class="mainheading" style="display:none;">
                      Dear
              <label id="lbChildName" runat="server" class="lblChildNamecss"></label>,
                  </div>
                  
                  <%--<hr runat="server" style="border: 1px solid  lightblue;" />--%>
      </div>
          <div id="divEnrollForm" class="">
              <asp:HiddenField ID="hidOnline" runat="server" />
              <asp:HiddenField ID="hidRegMode" runat="server" />
              <asp:HiddenField ID="hidamt" runat="server" />              
              <asp:HiddenField ID="mailid" runat="server" />
              <asp:HiddenField ID="hidTotalAmt" runat="server" />
              <asp:HiddenField ID="hidavail" runat="server" />

              <div class="title-box">
                            <h3>Enroll in Activity
                           </h3>
                        </div>
              <table class="table table-striped table-bordered table-responsive text-left my-orders-table">
                  <tr>
                      <td class="">Academic Year
                      </td>
                      <td class="">
                          <label id="lblACDYear" runat="server" />
                      </td>
                  </tr>
                  <tr>
                      <td class="">Event Name 
                      </td>
                      <td class="">
                          <label id="lbleventname" runat="server" />
                      </td>
                  </tr>
                  <tr>
                      <td class="">Event Summary 
                      </td>
                      <td class="">
                          <label id="lbleventdesc" runat="server" />
                      </td>
                  </tr>
                  <tr>
                      <td class="">Request Submission Period 
                      </td>
                      <td class="">
                          <label id="lblApplicationDate" runat="server" />
                      </td>
                  </tr>
                  <tr>
                      <td class="">Event Date
                      </td>
                      <td class="">
                          <label id="lbleventdt" runat="server" />
                      </td>
                  </tr>
                  <tr>
                      <td class="">Amount To be Paid 
                      </td>
                      <td class="">
                          <label id="lblamount" runat="server" />
                      </td>
                  </tr>
                  <tr id="rowcount" runat="server" visible="false">
                      <td>
                          Count
                      </td>
                      <td>
                          <%--<asp:TextBox runat="server" ID="txtcount" Text="1" OnTextChanged="txtcount_TextChanged" AutoPostBack="true" CssClass="textcntO_AD"></asp:TextBox>--%>
                          <asp:DropDownList runat="server" ID="ddlcount" OnSelectedIndexChanged ="ddlcount_SelectedIndexChanged" AutoPostBack="true" CssClass="textcntO_AD" ></asp:DropDownList>
                       </td>
                  </tr>
                  <tr>
                      <td class="">Comments 
                      </td>
                      <td class="">
                          <textarea id="txtCmmnts" runat="server" class="textareaO_AD" />
                      </td>
                  </tr>
              </table>
              <table class="table table-striped table-bordered table-responsive text-left my-orders-table" visible="false" runat="server" id="tblTerms">
                  <tr>                     
                      <td>
                           Terms and Conditions
                      </td>
                  </tr>
                  <tr>
                     
                      <td>
                           <asp:Label ID="lblterms" runat="server" Text="" align="center"></asp:Label>
                      </td>
                  </tr>
              </table>
              <table class="table table-striped table-bordered table-responsive text-left my-orders-table" visible="false" runat="server" id="tblTermsAccept">
                  <tr>
                      <td>
                          <asp:CheckBox ID="chkaccpt" runat="server" Text="I have read and accept these Terms and Conditions." />
                      </td>
                  </tr>
              </table>
              <br />
              <div class="text-center">
                  
                      <asp:Label ID="lblError" runat="server" />
              </div>
              <br />
              <div class="text-center">
                  <asp:Button ID="btnSubmit" OnClick="btnSubmit_OnClick" runat="server" Text="Enroll In Activity" cssClass="btn btn-info" OnClientClick="return Confirm()" />
              </div>

              <div class=" alert-success alert" runat ="server" visible ="false" id="divgrad">
                                                                  
                 
                      <asp:Label ID="lblMsgHead1" runat="server" Text="Please note the next steps :" Visible ="false"></asp:Label><br /><br />
                     
                      <asp:Label ID="lblMessage" runat="server" Visible ="false"  />                     
                                  
              </div>
              </ContentTemplate>
          </asp:UpdatePanel>
                                   
          </div>
          </ContentTemplate>
      </asp:UpdatePanel>
         
 
            
  </form>
    <script type="text/javascript" lang="javascript">
        function Confirm() {
            return confirm('Are you sure you want to continue?');
        }                                   
    </script>
    </body></html>



