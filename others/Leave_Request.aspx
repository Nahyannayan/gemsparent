<%@ Page Language="VB" MasterPageFile="~/ParentMaster.master" AutoEventWireup="false" CodeFile="Leave_Request.aspx.vb" Inherits="Others_Leave_Request" Title="GEMS EDUCATION" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphParent" runat="Server">

    <script language="javascript" type="text/javascript">


        function checkDate(sender, args) {

            var todate = document.getElementById("<%=hfDate.ClientID %>").value;
          var fromdate = document.getElementById("<%=hffromDate.ClientID %>").value;
          var displayDate = document.getElementById("<%=hfShowDate.ClientID %>").value;
          var displayFromDate = document.getElementById("<%=hfShowfromDate.ClientID %>").value;
          var Validdate = document.getElementById("<%=hfvalidFrmDate.ClientID %>").value;
          var Info = " You cannot select a day greater than " + displayDate;
          var FromInfo = " You cannot select a day less than " + displayFromDate;
          var lblPopError = document.getElementById("<%=lblPopError.ClientID %>");
    if (Validdate == "1") {
        if (sender._selectedDate > new Date(todate)) {
            lblPopError.setAttribute("class", "divinfoPopUp");

            lblPopError.innerHTML = Info;
            sender._selectedDate = new Date(todate);
            // set the date back to the current date
            sender._textbox.set_Value(sender._selectedDate.format(sender._format))
        }
        else if (sender._selectedDate < new Date(fromdate)) {
            //lblPopError.className += "divinfo";
            lblPopError.setAttribute("class", "divinfoPopUp");
            lblPopError.innerHTML = FromInfo;

            sender._selectedDate = new Date(todate);
            // set the date back to the current date
            sender._textbox.set_Value(sender._selectedDate.format(sender._format))
        }
        else {
            lblPopError.setAttribute("class", "");

            lblPopError.innerText = "";
        }
    }
}





function checkFromDate(sender, args) {

    var currenttime = document.getElementById("<%=hfDate.ClientID %>").value;

    var DisplayDt = document.getElementById("<%=hfShowDate.ClientID %>").value;
    var lblPopError = document.getElementById("<%=lblPopError.ClientID %>");
    var Info = " You cannot select a day less than " + DisplayDt;
    if (sender._selectedDate < new Date(currenttime)) {
        lblPopError.setAttribute("class", "divinfoPopUp");

        lblPopError.innerHTML = Info;
        ; //alert(Info);
        sender._selectedDate = new Date(currenttime);
        // set the date back to the current date
        sender._textbox.set_Value(sender._selectedDate.format(sender._format))
    }
    else {
        lblPopError.setAttribute("class", "");

        lblPopError.innerText = "";
    }

}



    </script>

     <script>
         if ($(window).width() < 979) {
             if ($(location).attr("href").indexOf("Leave_Request_M.aspx") == -1) {
                 window.location = "\\others\\Leave_Request_M.aspx";
             }
         }
         if ($(window).width() > 979) {
             if ($(location).attr("href").indexOf("Leave_Request.aspx") == -1) {
                 window.location = "\\others\\Leave_Request.aspx";
             }
         }
    </script>

    <div class="content margin-top30 margin-bottom60">
        <div class="container">
            <div class="row">

                <!-- Posts Block -->
                <div class="posts-block col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="my-account">
                        <%--<div class="mainheading" style="display: none;">
                            <div class="left">Leave Request/Letter</div>
                            <div class="right">
                                </div>
                        </div>--%>
                        <div class="title-box">
                            <h3>Leave Request/Letter
                            <span class="profile-right">
                                <asp:Label ID="lbChildName" runat="server"></asp:Label>
                            </span></h3>
                        </div>
                        <div class="alert alert-warning">
                            <button type="button" class="close" data-dismiss="alert">�</button>
                            <b>Leave Letter :</b><br />
                            Can be applied against the date where the approval status is Not Applied .<br />
                            <b>Leave Request :</b><br />
                            Can be applied for the current or future dates only .
                            <br />
                            Request will be notified to the authorized staff for approval .
                            <br />
                            Leave status will be updated once the request has been processed . 

                        </div>

                        <div>
                            <div>
                            <asp:Label ID="lblmsg" runat="server"  class="alert alert-warning margin-bottom0" EnableViewState="false" Visible="false"></asp:Label>
                                </div>

                            <table class="table table-bordered table-responsive text-left my-orders-table" width="100%" cellpadding="8" cellspacing="0">
                                
                                <tr class="tdblankAll">
                                    <td class="tdblankAll" width="22%" align="left">
                                        <asp:Label ID="lblSelect" runat="server" CssClass="tdfields" Text="Select option"> </asp:Label></td>
                                    <td class="tdblankAll" align="left">
                                        <asp:RadioButton ID="rbLNote" runat="server"
                                            Text=" Leave Letter" GroupName="leave" AutoPostBack="True" />&nbsp;&nbsp;&nbsp;
        <asp:RadioButton ID="rbLRequest" runat="server" Text=" Leave Request"
            GroupName="leave" AutoPostBack="True" /></td>
                                </tr>
                                <tr class="tdblankAll">
                                    <td class="tdblankAll" align="left" valign="bottom">
                                        <asp:Label ID="lblSearch" runat="server" Text="Search for approval status"> </asp:Label>

                                    </td>
                                    <td class="tdblankAll" align="left">
                                        <asp:DropDownList ID="ddlApp_Type"
                                            runat="server" CssClass="form-control" AutoPostBack="True">
                                            <asp:ListItem Value="0">All</asp:ListItem>
                                            <asp:ListItem Value="APP">Approved</asp:ListItem>
                                            <asp:ListItem Value="NAPP">Not Approved</asp:ListItem>
                                            <asp:ListItem Value="PEN">Pending</asp:ListItem>
                                            <asp:ListItem Value="NA">Not Applied</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr class="tdblankAll">
                                    <td class="tdblankAll" colspan="2">
                                        <asp:GridView ID="gvLeaveNote" runat="server" AllowPaging="True"
                                            EmptyDataText="No record available." AutoGenerateColumns="False"
                                            Width="100%" CssClass="table table-striped table-bordered table-responsive text-left my-orders-table" BorderStyle="None" BorderWidth="0px">
                                            <EmptyDataRowStyle Wrap="True" HorizontalAlign="Center" />
                                            <Columns>



                                                <asp:TemplateField HeaderText="SLA_ID" Visible="False">

                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <ItemStyle HorizontalAlign="RIGHT"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbldate" runat="server" Text='<%# Bind("DISPLAY_DATE") %>'></asp:Label>
                                                        <asp:Label ID="lblAPD_ID" runat="server" Text='<%# Bind("ATT_APD_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Attendance Date">
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAtt_date" runat="server" Text='<%# Bind("DISPLAY_DATE") %>' CssClass="inlineObj"></asp:Label>
                                                        <asp:Image ID="imgEntryFlag" runat="server" ImageUrl='<%# Bind("ENTRY_FLAG") %>'
                                                            Visible='<%# Bind("TOGGLE_BUTTON") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Attendance Leave Type">
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <ItemStyle HorizontalAlign="left" VerticalAlign="Middle"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAtt_param" runat="server" Text='<%# Bind("ATT_PARAM") %>'></asp:Label>

                                                    </ItemTemplate>
                                                </asp:TemplateField>



                                                <asp:TemplateField HeaderText="Remarks">
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <ItemStyle HorizontalAlign="left" VerticalAlign="Middle"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblREMARKS" runat="server" Text='<%# Bind("REMARKS") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>



                                                <asp:TemplateField HeaderText="Approval Status">
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <ItemStyle HorizontalAlign="left" VerticalAlign="Middle"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAPP_STATUS" runat="server" Text='<%# Bind("APP_STATUS") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>



                                                <asp:TemplateField HeaderText="Leave Letter">

                                                    <ItemTemplate>
                                                        <asp:Button ID="btnLApply" runat="server" CssClass="btn btn-info" Text="Write"
                                                            Enabled='<%# Bind("ENABLE_BUTTON") %>'
                                                            Visible='<%# Bind("ENABLE_BUTTON") %>' OnClick="btnLApply_Click" />
                                                        
                                                    </ItemTemplate>

                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>



                                            </Columns>
                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                            <PagerStyle CssClass="gridpager" />
                                        </asp:GridView>
                                        <asp:GridView ID="gvLeaveReq" runat="server" AllowPaging="True"
                                            EmptyDataText="No record available." AutoGenerateColumns="False"
                                            Width="100%" CssClass="table table-striped table-bordered table-responsive text-left my-orders-table" BorderStyle="None" BorderWidth="0px">
                                            <EmptyDataRowStyle Wrap="True" HorizontalAlign="Center" />
                                            <Columns>



                                                <asp:TemplateField HeaderText="SLA_ID" Visible="False">

                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <ItemStyle HorizontalAlign="RIGHT"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSLA_ID" runat="server" Text='<%# Bind("SLA_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="From Date">
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblFROMDT" runat="server" Text='<%# Bind("FROMDT") %>'
                                                            CssClass="inlineObj"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="To Date">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblTODT" runat="server" Text='<%# bind("TODT") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Attendance Leave Type">
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <ItemStyle HorizontalAlign="left" VerticalAlign="Middle"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAtt_param" runat="server" Text='<%# Bind("ATT_PARAM") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>



                                                <asp:TemplateField HeaderText="Remarks">
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <ItemStyle HorizontalAlign="left" VerticalAlign="Middle"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblREMARKS" runat="server" Text='<%# Bind("SLA_REMARKS") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>



                                                <asp:TemplateField HeaderText="Approval Status">
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <ItemStyle HorizontalAlign="left" VerticalAlign="Middle"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAPP_STATUS" runat="server" Text='<%# Bind("APP_STATUS") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Approver Comments">
                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <ItemStyle HorizontalAlign="left" VerticalAlign="Middle"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblApproverREmarks" runat="server" Text='<%# Bind("SLA_APPROVER_REMARKS") %>'></asp:Label>
                                                        <asp:Panel ID="T12Panel1" runat="server" Height="50px">
                                                            <%#Eval("SLA_APPROVER_REMARKS")%>
                                                        </asp:Panel>
                                                        <ajaxToolkit:CollapsiblePanelExtender ID="T12CollapsiblePanelExtender1" runat="server"
                                                            AutoCollapse="False" AutoExpand="False" CollapseControlID="lblApproverREmarks" Collapsed="true"
                                                            CollapsedSize="0" CollapsedText='<%#Eval("tempview")%>' ExpandControlID="lblApproverREmarks"
                                                            ExpandedSize="240" ExpandedText='<%#Eval("hide")%>' ScrollContents="true" TargetControlID="T12Panel1"
                                                            TextLabelID="lblApproverREmarks">
                                                        </ajaxToolkit:CollapsiblePanelExtender>

                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Edit">

                                                    <ItemTemplate>
                                                        <asp:Button ID="btnLREdit" runat="server" CssClass="btn btn-info" Text="Edit"
                                                            Enabled='<%# Bind("ENABLE_BUTTON") %>'
                                                            Visible='<%# Bind("ENABLE_BUTTON") %>' OnClick="btnLREdit_Click" />
                                                        
                                                    </ItemTemplate>

                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>



                                            </Columns>
                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                            <PagerStyle CssClass="gridpager" />
                                        </asp:GridView>
                                        <div style="width: 100%; text-align: center;">
                                            <asp:Button ID="btnAppLev_Req" runat="server" CssClass="btn btn-info" Text="Apply Leave Request" /></div>
                                    </td>
                                </tr>
                                <tr class="tdblankAll">
                                    <td class="tdblankAll" colspan="2">
                                        <div class="remark" runat="server" id="divEntry">
                                            Entered By :&nbsp;&nbsp;
                       <img src="../Images/parent.png" alt="tutor" style="display: inline;" />&nbsp;&nbsp;---Parent&nbsp;&nbsp;&nbsp;
                       <img src="../Images/tutor.png" alt="tutor" style="display: inline;" />&nbsp;&nbsp;---Tutor
                                        </div>
                                    </td>
                                </tr>

                            </table>

                            <div id="pldis" runat="server" style="display: none; overflow: visible; background-color: White; border-color: #b5cae7; border-style: solid; border-width: 4px;">
                                <div>
                                    <span style="clear: right; display: inline; float: right; visibility: visible; margin-top: 0px; vertical-align: top;">
                                        <asp:ImageButton ID="btnCloseedit" runat="server" ImageUrl="~/Images/close.png" /></span>
                                </div>
                                <div class="mainheading">
                                    <div>
                                        <asp:Label ID="lblPopHead" runat="server" Text=" Leave Note"></asp:Label></div>

                                </div>
                                <asp:Panel ID="plStudAtt" runat="server"  ScrollBars="Auto">
                                    <table class="table table-striped table-condensed table-responsive text-left my-orders-table" align="center" cellpadding="8" cellspacing="3">
                                        <tr>
                                            <td class="tdfields" align="left">Name</td>
                                            <td colspan="3">
                                                <asp:Label ID="lblPopchildName" runat="server"> </asp:Label></td>
                                        </tr>

                                        <tr>
                                            <td class="tdfields" align="left">From Date<span style="color: red">*</span></td>
                                            <td>
                                                <asp:TextBox ID="txtFromDt" runat="server" Width="122px" CssClass="form-control"></asp:TextBox>
                                                <asp:ImageButton ID="imgFromDt" runat="server" ImageUrl="~/Images/Calendar.png" CssClass="pos-absolute" />
                                                <asp:RequiredFieldValidator ID="rfvFRMDT" runat="server" ControlToValidate="txtFromDt"
                                                    Display="Dynamic" ErrorMessage="Enter from date" ForeColor="red" ValidationGroup="popValid">*</asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="REVFRMDT"
                                                    runat="server" ControlToValidate="txtFromDt" Display="Dynamic" EnableViewState="False"
                                                    ErrorMessage="Enter from  Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                                                    ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                                    ValidationGroup="popValid">*</asp:RegularExpressionValidator><asp:CustomValidator
                                                        ID="cvFrmDt" runat="server" ControlToValidate="txtFromDt"
                                                        Display="Dynamic" EnableViewState="False" ErrorMessage="From date entered is not a valid date"
                                                        ForeColor="red" ValidationGroup="popValid">*</asp:CustomValidator><div class="remark">(dd/mmm/yyyy)</div>




                                            </td>
                                            <td class="tdfields">To Date<span style="color: red">*</span></td>
                                            <td>
                                                <asp:TextBox ID="txtToDt" runat="server" Width="122px" CssClass="form-control"></asp:TextBox>
                                                <asp:ImageButton ID="imgToDt" runat="server" ImageUrl="~/Images/Calendar.png" CssClass="pos-absolute" />
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtToDt"
                                                    Display="Dynamic" ErrorMessage="Enter to date" ForeColor="red" ValidationGroup="popValid">*</asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator2"
                                                    runat="server" ControlToValidate="txtToDt" Display="Dynamic" EnableViewState="False"
                                                    ErrorMessage="Enter to  Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                                                    ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                                    ValidationGroup="popValid">*</asp:RegularExpressionValidator><asp:CustomValidator
                                                        ID="CustomValidator1" runat="server" ControlToValidate="txtToDt"
                                                        Display="Dynamic" EnableViewState="False" ErrorMessage="To date entered is not a valid date"
                                                        ForeColor="red" ValidationGroup="popValid">*</asp:CustomValidator><div class="remark">(dd/mmm/yyyy)</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" class="tdfields">Reason<span style="color: red">*</span></td>
                                            <td align="left" colspan="3">
                                                <asp:Label ID="lblAttStatus" runat="server"></asp:Label>
                                                <asp:DropDownList ID="ddlParam" runat="server" CssClass="form-control"></asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="rfvAttParam" runat="server" ControlToValidate="ddlparam" Display="Dynamic"
                                                    InitialValue="-1" ForeColor="red" ValidationGroup="popValid" ErrorMessage="Select Attendance leave type">*</asp:RequiredFieldValidator>
                                            </td>

                                        </tr>
                                        <tr>
                                            <td align="left" class="tdfields">Remark<span style="color: red">*</span></td>
                                            <td colspan="3">
                                                <asp:TextBox ID="txtremark" runat="server" CssClass="form-control" TextMode="MultiLine" Width="465px"
                                                    Height="80px" ></asp:TextBox><asp:RequiredFieldValidator ID="rfvRemarks" runat="server" ControlToValidate="txtremark"
                                                        Display="Dynamic" ErrorMessage="Remark required." ForeColor="red" ValidationGroup="popValid">*</asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="revRemarks" ControlToValidate="txtremark" ValidationExpression="[\s\S]{0,250}"
                                                    ErrorMessage="maximum character allowed is 250" ValidationGroup="popValid" ForeColor="red" runat="server" Text="*" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 241px; height: 19px" align="left" colspan="4"></td>

                                        </tr>
                                        <tr>
                                            <td align="center" colspan="4">
                                                <asp:Button ID="btnSaveAbs" runat="server" Text="Save" CssClass="btn btn-info" ValidationGroup="popValid" />&nbsp;
                        <asp:Button ID="btnCloseAbs" runat="server" Text="Close" CssClass="btn btn-info" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" colspan="4"></td>
                                        </tr>
                                        <tr>
                                            <td align="left" colspan="4">
                                                <asp:Label ID="lblPopError" runat="server"></asp:Label>
                                                <asp:ValidationSummary ID="vsVisaDetails" runat="server" ValidationGroup="popValid"
                                                    HeaderText="<div class='validationheader'>Please correct the following:</div>"
                                                    CssClass="text-danger" ForeColor="" />

                                            </td>
                                        </tr>
                                    </table>

                                </asp:Panel>
                            </div>




                            <ajaxToolkit:CalendarExtender ID="cexTodate" runat="server" Format="dd/MMM/yyyy"
                                PopupButtonID="imgToDt" TargetControlID="txtToDt" OnClientDateSelectionChanged="checkDate">
                            </ajaxToolkit:CalendarExtender>

                            <ajaxToolkit:CalendarExtender ID="ceFRMdate" runat="server" Format="dd/MMM/yyyy"
                                PopupButtonID="imgFromDt" TargetControlID="txtFromDt" OnClientDateSelectionChanged="checkFromDate">
                            </ajaxToolkit:CalendarExtender>
                            <asp:Button ID="btnShowPopup" runat="server" Style="display: none" />
                            <ajaxToolkit:ModalPopupExtender
                                ID="mdlPopup" runat="server" TargetControlID="btnShowPopup" PopupControlID="pldis"
                                BackgroundCssClass="modalBackground" DropShadow="false" RepositionMode="RepositionOnWindowResizeAndScroll">
                            </ajaxToolkit:ModalPopupExtender>
                            <asp:HiddenField ID="hfDate" runat="server" />
                            <asp:HiddenField ID="hfShowDate" runat="server" />
                            <asp:HiddenField ID="hffromDate" runat="server" />
                            <asp:HiddenField ID="hfShowfromDate" runat="server" />
                            <asp:HiddenField ID="hfvalidFrmDate" runat="server" />
                        </div>
                    </div>
                </div>
                <!-- /Posts Block -->
            </div>
        </div>
    </div>

</asp:Content>

