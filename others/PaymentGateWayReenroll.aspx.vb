﻿Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System
Imports System.IO
Partial Class Others_PaymentGateWayReenroll
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("username") & "" = "" Then
            Response.Redirect("~/login.aspx")
        End If
        PaymentRedirect()
    End Sub

    Sub PaymentRedirect()
        Dim data As New NameValueCollection()
        data("virtualPaymentClientURL") = "https://migs.mastercard.com.au/vpcpay"
        data("vpc_Command") = "pay"
        data("vpc_Version") = "1"
        data("vpc_ReturnURL") = Session("vpc_ReturnURL")
        data("vpc_Locale") = "en"
        data("vpc_OrderInfo") = Session("vpc_OrderInfo")
        data("Title") = "ASP VPC 3-Party"
        HttpHelper.RedirectAndPOST(Me.Page, "CS_VPC_3Party_DO.aspx", data)

        '<input id="Title" runat="server" name="Title" type="hidden" value="ASP VPC 3-Party" />
        '<input id="virtualPaymentClientURL" runat="server" name="virtualPaymentClientURL" type="hidden" value="https://migs.mastercard.com.au/vpcpay" />
        '<input id="vpc_Command" runat="server" name="vpc_Command" type="hidden" value="pay" />
        '<input id="vpc_Version" runat="server" name="vpc_Version" type="hidden" value="1" />
        '<input id="vpc_ReturnURL" runat="server" name="vpc_ReturnURL" type="hidden" />
        '<input id="vpc_Locale" runat="server" name="vpc_Locale" type="hidden" value="en" />
        '<input id="vpc_OrderInfo" runat="server" name="vpc_OrderInfo" type="hidden" />



    End Sub
End Class
