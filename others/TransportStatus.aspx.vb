Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Xml
Imports System.Collections.Generic
Imports CURRICULUM
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports System.Collections
Partial Class TransportStatus
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim url As String
        If Session("username") Is Nothing Then
            Response.Redirect("~\Login.aspx")
        End If
        Dim str_Query As String = "SELECT COUNT(SSV_ID) FROM " _
                                & " OASIS.DBO.STUDENT_SERVICES_D WITH(NOLOCK) WHERE SSV_STU_ID='" + Session("STU_ID") + "' AND SSV_SVC_ID=1 AND SSV_TODATE IS NULL "
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISCONNECTIONSTRING").ConnectionString
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Query)
        Dim QRY As String = "SELECT BU.BSU_SHORTNAME FROM STUDENT_M AS ST WITH(NOLOCK) INNER JOIN SERVICES_BSU_M AS SM WITH(NOLOCK) " & _
                            "ON ST.STU_BSU_ID = SM.SVB_BSU_ID AND ST.STU_ACD_ID = SM.SVB_ACD_ID AND SM.SVB_SVC_ID = 1 " & _
                            "INNER JOIN OASIS.dbo.BUSINESSUNIT_M AS BU WITH(NOLOCK) ON SM.SVB_PROVIDER_BSU_ID = BU.BSU_ID " & _
                            "WHERE STU_ID = " & Session("STU_ID")
        Session("providerheader") = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, QRY).ToString().ToLower()

        Dim strStudentID = Encr_decrData.Encrypt(Session("STU_ID"))
        Dim strProviderHeader = Encr_decrData.Encrypt(Convert.ToString(Session("providerheader")))
        Dim strFrom = Encr_decrData.Encrypt("PARLOGIN")
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                If ds.Tables(0).Rows(0).Item(0) = "0" Then
                    url = "https://school.gemsoasis.com/TransportRegistration/OnlineRegGEMSStudents.aspx?stu_id=" + strStudentID + "&header=" + strProviderHeader + "&from=" + strFrom

                    Response.Redirect(url, False)
                    Exit Sub


                End If
            End If
        End If
        lbChildNameTop.Text = Session("STU_NAME")

        Dim stuId As String = Session("STU_ID")
        lblTrancportSTatus.Text = "Transport has already been allocated to the chosen Student. Please find below the details: "
        BindTransportData(stuId)
    End Sub

    Sub BindTransportData(ByVal stuId As String)

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT SBL_DESCRIPTION+'-'+A.PNT_DESCRIPTION AS PICKUP,SBL_DESCRIPTION+'-'+B.PNT_DESCRIPTION,ISNULL(PBM.BNO_DESCR,'') SSV_PICKUP_BUSNO,ISNULL(DBM.BNO_DESCR,'') SSV_DROPOFF_BUSNO " _
                                & " FROM dbo.STUDENT_SERVICES_D AS SD WITH(NOLOCK) INNER JOIN OASIS_TRANSPORT.TRANSPORT.SUBLOCATION_M WITH(NOLOCK) ON SBL_ID=SSV_SBL_ID " _
                                & " INNER JOIN OASIS_TRANSPORT.TRANSPORT.PICKUPPOINTS_M A WITH(NOLOCK) ON A.PNT_ID=SSV_PICKUP AND SSV_SBL_ID=A.PNT_SBL_ID " _
                                & " INNER JOIN OASIS_TRANSPORT.TRANSPORT.PICKUPPOINTS_M B WITH(NOLOCK) ON B.PNT_ID=SSV_DROPOFF AND SSV_SBL_ID=B.PNT_SBL_ID " _
                                & " INNER JOIN OASIS_TRANSPORT.TRANSPORT.TRIPS_D AS PTD WITH(NOLOCK) ON PTD.TRD_TRP_ID=SSV_PICKUP_TRP_ID AND PTD.TRD_TODATE IS NULL" _
                                & " INNER JOIN OASIS_TRANSPORT.TRANSPORT.BUSNOS_M AS PBM WITH(NOLOCK) ON PBM.BNO_ID=PTD.TRD_BNO_ID" _
                                & " INNER JOIN OASIS_TRANSPORT.TRANSPORT.TRIPS_D AS DTD WITH(NOLOCK) ON DTD.TRD_TRP_ID=SSV_PICKUP_TRP_ID AND DTD.TRD_TODATE IS NULL" _
                                & " INNER JOIN OASIS_TRANSPORT.TRANSPORT.BUSNOS_M AS DBM WITH(NOLOCK) ON DBM.BNO_ID=DTD.TRD_BNO_ID" _
                                & " WHERE SSV_STU_ID='" + stuId + "' AND SSV_SVC_ID = 1 AND SSV_TODATE IS NULL "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                pickupArea.Text = ds.Tables(0).Rows(0).Item(0)
                dropoffArea.Text = ds.Tables(0).Rows(0).Item(1)
                pickupbus.Text = ds.Tables(0).Rows(0).Item(2)
                dropoffbus.Text = ds.Tables(0).Rows(0).Item(3)
            End If
        End If

    End Sub

End Class
