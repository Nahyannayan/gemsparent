﻿<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/ParentMaster.master"  CodeFile="ActivityPaymentResult.aspx.vb" Inherits="Others_ActivityPaymentResult" %>
<%@ Register Src="../UserControl/urcActvityPaidResult.ascx" TagName="PaidResult" TagPrefix="uar"%>

<asp:content id="Content1" contentplaceholderid="cphParent" runat="Server">

    <script src="../Scripts/jquery-1.9.1.js" type="text/javascript"></script>
    <!-- Add fancyBox -->
    <script type="text/javascript" src="../Scripts/Fancybox/jquery.fancybox.js?v=2.1.5"></script>
    <script type="text/javascript" src="../Scripts/Fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
    <link rel="stylesheet" href="../Scripts/Fancybox/jquery.fancybox.css" type="text/css" media="screen" />
    <script type="text/javascript" src="../Scripts/PopupJQuery.js"></script>
    <%--<script type="text/javascript" language="javascript">
        function CheckForPrint() {
            if (document.getElementById('<%=h_Recno.ClientID %>').value != '') {
                 var frmReceipt = "feereceipt.aspx";
                 if ($("#<%=hfTaxable.ClientID%>").val() == "1")
                    frmReceipt = "FeeReceipt_TAX.aspx";
                else
                    frmReceipt = "feereceipt.aspx";
                showModelessDialog(frmReceipt + '?type=REC&id=' + document.getElementById('<%=h_Recno.ClientID %>').value, '', "dialogWidth: 800px; dialogHeight: 700px; help: no; resizable: no; scroll: yes; status: no; unadorned: no;");
             return false;
         }
         else
             alert('Sorry. There is some problem with the transaction');
     }
    </script>--%>

    <div class="content margin-top30 margin-bottom60">
        <div class="container">
            <div class="row">
                <!-- Posts Block -->
                <div class="posts-block col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <table align="center" cellpadding="0" cellspacing="0" class="BlueTable" width="90%">
                        <tr class="subheader_img">
                            <td align="left">
                                <asp:HiddenField ID="hfTaxable" runat="server" Value="0" />
                                <asp:Label ID="lblError" runat="server" EnableViewState="False"></asp:Label>
                                <asp:Label ID="lblMessage" runat="server" EnableViewState="true"></asp:Label></td>
                        </tr>
                        <%--<tr class="matters">
            <td align="left" class="tdfields">Transaction Summary
                <asp:Label ID="lblEventName" runat="server" CssClass="matters" Visible="True" />
            </td>
        </tr>--%>
                        <tr>
                            <td align="left">
                                <%--<ucs:urcActvityPaidResult1 ID="urcActvityPaidResult1" runat="server" />--%>
                                <uar:PaidResult ID="urcActvityPaidResult1" runat="server" />
                            </td>
                        </tr>
                    </table>

                    <!-- Posts Block -->
                </div>
            </div>
        </div>
    </div> 
</asp:content>
