<%@ Page Language="VB" MasterPageFile="~/ParentMaster.master" AutoEventWireup="false"
    CodeFile="OtherRequests.aspx.vb" Inherits="Others_OtherRequests" Title="GEMS EDUCATION" %>

<%@ Register Src="../UserControl/urcParentShortProfile.ascx" TagName="urcParentShortProfile"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphParent" runat="Server">

    <script src="../Scripts/jquery-1.10.2.min.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-ui-1.10.2.min.js" type="text/javascript"></script>

    <!-- Add fancyBox -->
    <script type="text/javascript" src="../Scripts/Fancybox/jquery.fancybox.js?v=2.1.5"></script>
    <script type="text/javascript" src="../Scripts/Fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
    <link rel="stylesheet" href="../Scripts/Fancybox/jquery.fancybox.css" type="text/css"
        media="screen" />

    <script type="text/javascript" src="../Scripts/PopupJQuery.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            ViewEmpSearch();
        },
            CheckHidden());

        function CheckHidden() {
            if ($('#<%=h_EmpNo.ClientID%>').val() != '' && $('#<%=h_EmpNo.ClientID%>').val() != undefined) {
                $('#<%=tblEmp.ClientID%>').hide(500);
                $('.trQuestion').css("display", "none");
                //$('.trQuestion').hide(800);
            }
            else {
                $('.trQuestion').css("display", "block");
            }
        }
        function ViewEmpSearch() {

            if ($('#<%=rbYes.ClientID%>').is(":checked") == true) {
                $('#<%=tblEmp.ClientID%>').show(600);
            }
            else {
                $('#<%=tblEmp.ClientID%>').hide(0);
            }
        }
        function ShowFurtherSearch() {
            $('#<%=trFName.ClientID%>').show(600);
            $('#<%=trDOJ.ClientID%>').show(600);
            $('#<%=trDOB.ClientID%>').show(600);
            $('#<%=trbtnSearch.ClientID%>').show(600);
        }
        function ShowOTP() {
            $('#<%=trOTP.ClientID%>').show(600);
        }
    </script>

     <div class="content margin-top30 margin-bottom60">
        <div class="container">
            <div class="row">

                <!-- Posts Block -->
                <div class="posts-block col-lg-12 col-md-12 col-sm-12 col-xs-12">



    
    <div class="title-box">
          <h3>Other Requests 
               <span class="profile-right">
                 <asp:Label ID="lbChildNameTop" runat="server"></asp:Label>
               </span></h3>
    </div>
    <div>
        <table width="100%" border="0" cellpadding="8" cellspacing="0">
            <tr>
                <td align="left" class="tdblankAll" colspan="2" style="width: 50%">
                <asp:GridView runat="server" ID="gvApplyConcession" CssClass="table table-striped table-bordered table-responsive text-left my-orders-table" EmptyDataText="No Transaction details added yet."
                    Width="100%" AutoGenerateColumns="False" SkinID="GridViewNormal" EnableModelValidation="True"
                    DataKeyNames="STU_ID,STU_ACD_ID,SSV_ID,SCR_ID,APPR_STATUS">
                    <columns>
<asp:BoundField DataField="STU_NO" HeaderText="Student ID"></asp:BoundField>
<asp:BoundField DataField="STU_NAME" HeaderText="Name"></asp:BoundField>
<asp:BoundField DataField="GRD_DISPLAY" HeaderText="Grade"></asp:BoundField>
<asp:TemplateField HeaderText="Academic Year"><ItemTemplate>
<asp:DropDownList id="ddlAcdYear" runat="server" DataTextField="ACY_DESCR" CssClass="form-control left" DataValueField="ACD_ID" Enabled='False'></asp:DropDownList> 
</ItemTemplate>

<ItemStyle HorizontalAlign="left"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="FEE_TYPE" HeaderText="Fee Type"></asp:BoundField>
<asp:TemplateField HeaderText="Service.RefNo" Visible="False"><ItemTemplate>
                                
                                <asp:Label ID="lnkSSVID" CssClass="currLink" runat="server" Text='<%# Bind("SSV_ID") %>'></asp:Label>
                            
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="Status"><ItemTemplate>
                                <asp:LinkButton ID="lnkApply" runat="server" CssClass="linkText" ></asp:LinkButton>
                                <asp:LinkButton ID="lnkStatus" runat="server" CssClass="linkText" Visible="false" ></asp:LinkButton>
                            
</ItemTemplate>
</asp:TemplateField>
</columns>
                </asp:GridView>
                </td>
            </tr>
            <tr class="trQuestion" id="trQuestion">
                <td class="tdblankAll" align="left" style="width: 50%;">
                    Are You a GEMS Employee?
                </td>
                <td class="tdblankAll" align="left" style="width: 50%;">
                    <asp:RadioButton ID="rbYes" Text="Yes" GroupName="GEMS" runat="server" />
                    <asp:RadioButton ID="rbNo" GroupName="GEMS" Checked="true" Text="No" runat="server" />
                </td>
            </tr>
            <tr class="tdblankAll">
                <td align="center" colspan="2" class="tdblankAll">
                    <table id="tblEmp" class="tblEmp" runat="server" style="" width="70%">
                        <tr class="tdblankAll">
                            <td class="tdblankAll" align="left" width="35%">
                                <asp:Label ID="lblSelect" runat="server" CssClass="tdfields" Text="Employee No"></asp:Label>
                            </td>
                            <td class="tdblankAll" align="left">
                                <asp:TextBox ID="txtEmpno" runat="server"  CssClass="form-control">
                                </asp:TextBox>
                                <asp:Button ID="btnGO" runat="server" CssClass="btn btn-info" Text="Go"
                                    ValidationGroup="popValid" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtEmpno"
                                    Display="Dynamic" ErrorMessage="Employee no" ForeColor="red" ValidationGroup="popValid">*</asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr id="trOTP" class="trOTP" runat="server">
                            <td class="tdblankAll" align="left">
                                <asp:Label ID="Label4" runat="server" CssClass="tdfields" Text="One Time PIN (OTP)">
                                </asp:Label>
                            </td>
                            <td class="tdblankAll" align="left">
                                <asp:TextBox ID="txtOTP" runat="server" CssClass="form-control">
                                </asp:TextBox><br />
                                <asp:Label ID="Label5" runat="server" Style="color: red;" Text="*One Time PIN has been sent to your registered Email">
                                </asp:Label>
                            </td>
                        </tr>
                        <tr class="trFName" id="trFName" runat="server">
                            <td class="tdblankAll" align="left">
                                <asp:Label ID="Label2" runat="server" CssClass="tdfields" Text="First Name"></asp:Label>
                            </td>
                            <td class="tdblankAll" align="left">
                                <asp:TextBox ID="txtFName" runat="server"  CssClass="form-control">
                                </asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtFName"
                                    Display="Dynamic" ErrorMessage="Fist name" ForeColor="red" ValidationGroup="popValid">*</asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr class="trDOJ" id="trDOJ" runat="server">
                            <td class="tdblankAll" align="left">
                                <asp:Label ID="Label1" runat="server" CssClass="tdfields" Text="Date Of Join(Group)">
                                </asp:Label>
                            </td>
                            <td class="tdblankAll" align="left">
                                <asp:TextBox ID="txtDOJ" runat="server" CssClass="form-control">
                                </asp:TextBox>
                                <asp:ImageButton ID="imgDOJ" runat="server" ImageUrl="~/Images/Calendar.png" CssClass="pos-absolute" />
                                <asp:RequiredFieldValidator ID="rfvFRMDT" runat="server" ControlToValidate="txtDOJ"
                                    Display="Dynamic" ErrorMessage="Date of join" ForeColor="red" ValidationGroup="popValid">*</asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="REVFRMDT" runat="server" ControlToValidate="txtDOJ"
                                    Display="Dynamic" EnableViewState="False" ErrorMessage="Enter from  Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                                    ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                    ValidationGroup="popValid">*</asp:RegularExpressionValidator><asp:CustomValidator
                                        ID="cvFrmDt" runat="server" ControlToValidate="txtDOJ" Display="Dynamic" EnableViewState="False"
                                        ErrorMessage="From date entered is not a valid date" ForeColor="red" ValidationGroup="popValid">*</asp:CustomValidator>
                                <ajaxToolkit:CalendarExtender ID="ceDOJ" runat="server" Format="dd/MMM/yyyy"
                                        PopupButtonID="imgDOJ" TargetControlID="txtDOJ">
                                    </ajaxToolkit:CalendarExtender>
                                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MMM/yyyy"
                                    PopupButtonID="txtDOJ" TargetControlID="txtDOJ">
                                </ajaxToolkit:CalendarExtender>
                            </td>
                        </tr>
                        <tr class="trDOB" id="trDOB" runat="server">
                            <td class="tdblankAll" align="left">
                                <asp:Label ID="Label3" runat="server" CssClass="tdfields" Text="Date Of Birth"></asp:Label>
                            </td>
                            <td class="tdblankAll" align="left">
                                <asp:TextBox ID="txtDOB" runat="server"  CssClass="form-control">
                                </asp:TextBox>
                                 <asp:ImageButton ID="imgDOB" runat="server" ImageUrl="~/Images/Calendar.png" CssClass="pos-absolute" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtDOB"
                                    Display="Dynamic" ErrorMessage="Date of birth" ForeColor="red" ValidationGroup="popValid">*</asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtDOB"
                                    Display="Dynamic" EnableViewState="False" ErrorMessage="Enter from  Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                                    ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                    ValidationGroup="popValid">*</asp:RegularExpressionValidator><asp:CustomValidator
                                        ID="CustomValidator1" runat="server" ControlToValidate="txtDOB" Display="Dynamic"
                                        EnableViewState="False" ErrorMessage="From date entered is not a valid date"
                                        ForeColor="red" ValidationGroup="popValid">*</asp:CustomValidator>
                                <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd/MMM/yyyy"
                                        PopupButtonID="imgDOB" TargetControlID="txtDOB">
                                    </ajaxToolkit:CalendarExtender>
                                <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server" Format="dd/MMM/yyyy"
                                    PopupButtonID="txtDOB" TargetControlID="txtDOB">
                                </ajaxToolkit:CalendarExtender>
                                </td>
                        </tr>
                        <tr class="trbtnSearch" id="trbtnSearch" runat="server">
                            <td class="tdblankAll" align="center" colspan="2">
                                <asp:Button ID="btnEmpSearch" runat="server" CssClass="btn btn-info" ValidationGroup="popValid"
                                    Text="Continue" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tdblankAll" align="left" colspan="2">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr class="tdblankAll">
                <td class="tdblankAll" align="center" colspan="2">
                    <asp:Label ID="lblPopError" runat="server"></asp:Label>
                                <asp:ValidationSummary ID="vsEmpDetails" runat="server" ValidationGroup="popValid"
                                    HeaderText="<div class='validationheader'>Please correct the following:</div>"
                                    CssClass="divinfoPopUp" ForeColor="" />
                                <asp:HiddenField ID="h_EmpNo" Value="" runat="server" />
                </td>
            </tr>
            <tr class="tdblankAll">
                <td class="tdblankAll" align="center" colspan="2">
                    <uc1:urcParentShortProfile ID="urcParentShortProfile1" runat="server" />
                </td>
            </tr>
        </table>
    </div>

                </div>
                <!-- /Posts Block -->
            </div>
        </div>
   </div>
</asp:Content>
