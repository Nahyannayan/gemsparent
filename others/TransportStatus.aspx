<%@ Page Language="VB" MasterPageFile="~/ParentMaster.master" AutoEventWireup="false" CodeFile="TransportStatus.aspx.vb" Inherits="TransportStatus" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphParent" Runat="Server">

    <div class="content margin-top30 margin-bottom60">
        <div class="container">
            <div class="row">

                <!-- Posts Block -->
                <div class="posts-block col-lg-12 col-md-12 col-sm-12 col-xs-12">

    <div class="title-box">
          <h3>Transport Request 
               <span class="profile-right">
                 <asp:Label ID="lbChildNameTop" runat="server"></asp:Label>
               </span></h3>
    </div>

<table width="100%" class="table table-striped table-bordered table-responsive text-left my-orders-table" cellpadding="8" cellspacing="0">
<tr>
<td class="matters" align="left" colspan="2">
<asp:Label id="lblTrancportSTatus" runat="server" Font-Bold="True"  ></asp:Label>
</td>
</tr>
<tr>
<td>
    <asp:Label id="lblpickupArea" runat="server" text="Pickup Point  " Font-Bold="True" ></asp:Label>
</td>
<td>
<asp:Label id="pickupArea" runat="server"  ></asp:Label>
</td>
</tr>
<tr>
<td >
    <asp:Label id="lbldropoffArea" runat="server" Text="Dropoff Point    " Font-Bold="True" ></asp:Label>
</td>
<td>
<asp:Label id="dropoffArea" runat="server"  ></asp:Label>
</td>
</tr>
<tr>
<td>
    <asp:Label id="lblpickupbus" runat="server" Text="Pickup Bus No  " Font-Bold="True" ></asp:Label>
</td>
<td>
<asp:Label id="pickupbus" runat="server" ></asp:Label>
</td>
</tr>
<tr>
<td>
    <asp:Label id="lbldropoffbus" runat="server" Text="Dropoff Bus No    " Font-Bold="True" ></asp:Label>
</td>
<td>
    <asp:Label id="dropoffbus" runat="server" ></asp:Label>
</td>
</tr>
</table>

                </div>
                <!-- /Posts Block -->
            </div>
        </div>
    </div>

</asp:Content>
