﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Imports System.Collections.Generic
Partial Class others_FeePaymentPlan
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Session("username") Is Nothing Then
            Response.Redirect("~\Login.aspx")
        End If
        If Page.IsPostBack = False Then
            spError.Visible = False
            Gridbind()
            checkExist()
        End If
    End Sub
    Sub Gridbind()
        Try
            'Dim strPath As String = Convert.ToString(readerStudent_Detail("STU_PHOTOPATH"))
            Dim connPath As String = WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ConnectionString
            Dim noImagePath As String = "~/Images/Home/no_image.gif"
            Dim con As String = ConnectionManger.GetOASISConnectionString
            Dim param(4) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@OLU_ID", Session("OLU_ID").ToString)
            param(1) = New SqlClient.SqlParameter("@connPath", connPath)
            param(2) = New SqlClient.SqlParameter("@noImagePath", noImagePath)
            param(3) = New SqlClient.SqlParameter("@STU_BSU_ID", Session("STU_BSU_ID")) '@
            param(4) = New SqlClient.SqlParameter("@SELECTED_STU_ID", 0)
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "[OPL].[GETCHILD_HOME_INFO_paymentplan]", param)
            repInfo.DataSource = ds.Tables(0)
            repInfo.DataBind()

        Catch ex As Exception
            repInfo.DataBind()

        End Try
    End Sub

    Protected Sub btnSub_Click(sender As Object, e As EventArgs) Handles btnSub.Click
        Dim retflag As Integer = 100

        spError.InnerText = ""
       

        If chkcategory.SelectedIndex = -1 Then
            spError.Visible = True
            spError.InnerText = "Please select payment plan"
            Exit Sub
        End If


        If chkAggree.Checked = False Then
            spError.Visible = True
            spError.InnerText = "Please agree the terms and conditions"
            Exit Sub
        End If

        '  checkExist()
        For Each item As RepeaterItem In repInfo.Items

            Dim chkSelect As New CheckBox
            Dim hdnstu As New HiddenField
            Dim hdnName As New HiddenField

            chkSelect = DirectCast(item.FindControl("chkSelect"), CheckBox)
            hdnstu = DirectCast(item.FindControl("hdnstu"), HiddenField)
            hdnName = DirectCast(item.FindControl("hdnName"), HiddenField)
            If chkSelect.Checked Then
                retflag = saveData(hdnstu.Value)
                If spName.InnerText = "" Then
                    spName.InnerText = hdnName.Value
                Else
                    spName.InnerText = spName.InnerText + " ," + hdnName.Value
                End If

            End If


        Next

        If retflag = 0 Then
            ScriptManager.RegisterStartupScript(Me, Page.GetType, "Script_0", "PopupHome()", True)
        Else
            ' divSuccess.Visible = False
            Exit Sub
        End If

        Gridbind()
        checkExist()
        spError.Visible = False
    End Sub

    Sub checkExist()
        For Each item As RepeaterItem In repInfo.Items

            Dim chkSelect As New CheckBox
            Dim hdnStatus As New HiddenField

            chkSelect = DirectCast(item.FindControl("chkSelect"), CheckBox)
            hdnStatus = DirectCast(item.FindControl("hdnStatus"), HiddenField)
            If hdnStatus.Value <> "NEW" Then
                chkSelect.Checked = False
                chkSelect.Enabled = False
           
            End If

        Next
    End Sub


    Function saveData(ByVal stuid As String) As Integer



        Try
            Using connection As SqlConnection = ConnectionManger.GetOASIS_FEESConnection()
                Dim pParms(4) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@STU_ID", stuid)
                pParms(1) = New SqlClient.SqlParameter("@PORTAL_LOG_USER", Session("username"))
                pParms(2) = New SqlClient.SqlParameter("@PAY_MODE ", chkcategory.SelectedItem.Value)

                pParms(3) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                pParms(3).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "[FEES].[CREATE_PARENT_PAYMENT_PLAN]", pParms)
                Dim ReturnFlag As Integer = pParms(3).Value
                Return ReturnFlag
            End Using
        Catch ex As Exception
            spError.Visible = True
            spError.InnerText = "Some error occurred"
            Return -1
        End Try

    End Function


End Class
