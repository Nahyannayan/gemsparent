﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Net.Mail
Imports System.Text
Imports Mainclass
Partial Class Others_EnrolledActivityList
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim Message As String = String.Empty
    Private Property VSgridEnrolledActivities() As DataTable
        Get
            Return ViewState("EnrolledActivities")
        End Get
        Set(ByVal value As DataTable)
            ViewState("EnrolledActivities") = value
        End Set
    End Property
    Protected Sub Page_Load(ByVal sender As Object, _
      ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            If Session("username") & "" = "" Then
                Response.Redirect("~/login.aspx")
            End If

            Dim ChildName As String = Session("STU_NAME")
            lbChildName.Text = ChildName
            If Request.QueryString("ID") <> "" Then
                ViewState("ID") = Encr_decrData.Decrypt(Request.QueryString("ID").Replace(" ", "+"))
                LoadPagewithEnrolledList(ViewState("ID"))
            Else
                ViewState("ID") = ""
                lblError.Text = "Student details missing"
            End If
        Catch ex As Exception
            Message = Mainclass.getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From PageLoad: " + ex.Message, "ACTIVITY SERVICES")
        End Try
    End Sub
    Protected Sub LoadPagewithEnrolledList(ByVal StudentID As UInt64)
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim Param(0) As SqlParameter
            Param(0) = New SqlClient.SqlParameter("@STUID", StudentID)
            Dim DS As DataSet = SqlHelper.ExecuteDataset(str_conn, "[OASIS].[VIEW_ENROLLED_ACTIVITY_LIST]", Param)
            Dim dt As DataTable = DS.Tables(0)
            If dt.Rows.Count > 0 Then
                For Each rows In dt.Rows
                    rows("Event Amount") = rows("Event Amount") + " AED"
                Next
            End If
            VSgridEnrolledActivities = dt
            gridbind()
        Catch ex As Exception
            Message = Mainclass.getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From LoadPagewithEnrolledList: " + ex.Message, "ACTIVITY SERVICES")
        End Try
    End Sub
    Protected Sub gridbind()
        Try
            If Not VSgridEnrolledActivities Is Nothing Then
                gvEnrolledList.DataSource = VSgridEnrolledActivities
                gvEnrolledList.DataBind()
            End If
        Catch ex As Exception
            Message = Mainclass.getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From gridbind: " + ex.Message, "ACTIVITY SERVICES")
        End Try
    End Sub
    Sub ShowMessage(ByVal Message As String, ByVal bError As Boolean)
        If Message <> "" Then
            If bError Then
                lblError.CssClass = "alert alert-warning"
            Else
                lblError.CssClass = "alert alert-warning"
            End If
        Else
            lblError.CssClass = "alert alert-warning"
        End If
        lblError.Text = Message
    End Sub
    Protected Sub gvEnrolledList_RowDataBound(sender As Object, e As GridViewRowEventArgs)
        Try
            e.Row.Cells(2).HorizontalAlign = HorizontalAlign.Right
        Catch ex As Exception
            UtilityObj.Errorlog("From gvEnrolledList_RowDataBound: " + ex.Message, "ACTIVITY SERVICES")
        End Try
    End Sub
    Protected Sub gvEnrolledList_PageIndexChanging(sender As Object, e As GridViewPageEventArgs)
        Try
            Me.gvEnrolledList.PageIndex = e.NewPageIndex
            gridbind()
        Catch ex As Exception
            Message = Mainclass.getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From gvEnrolledList_PageIndexChanging: " + ex.Message, "ACTIVITY SERVICES")
        End Try

    End Sub
End Class