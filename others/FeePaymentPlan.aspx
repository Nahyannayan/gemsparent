﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ParentMaster.master" AutoEventWireup="false" CodeFile="FeePaymentPlan.aspx.vb" Inherits="others_FeePaymentPlan" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphParent" Runat="Server">
     
    <script src="<%= ResolveUrl("~/Scripts/fancybox/jquery.fancybox.pack.js")%>"></script>
    <script src="<%= ResolveUrl("~/Scripts/fancybox/jquery.fancybox.js")%>"></script>
    <link href="<%= ResolveUrl("~/Scripts/fancybox/jquery.fancybox.css")%>" rel="stylesheet" />
    
    <script>
        function PopupHome() {
            $.fancybox({
                'width': '50%',
                'height': '300',
                'autoScale': true,
                'transitionIn': 'fade',
                'transitionOut': 'fade',
                'type': 'iframe',
                'content' : $("#divSuccess").html()
            });
        }

    </script>
   
  <style>
      input[type="checkbox"] {
    
    height: 17px !important;
    width: 17px !important;
    text-align:center !important;
}
  </style>
       <div class="content margin-top30 margin-bottom60">
        <div class="container">
            <div class="row">

                <!-- Posts Block -->
                <div class="posts-block col-lg-12 col-md-12 col-sm-12 col-xs-12">

 <header class="py-3 bg-white">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 text-center">
                    <h2 class="m-0 text-primary">Payment plan Application</h2>
                </div>
            </div>
        </div>
    </header>

                    <div class="my-account">
                     

    <div class="fee-payement-plan" style="color: #003a63;">
      <p style="font-size:16px;font-weight:700;"><strong>I would like to request for payment plan towards tuition fees for below mentioned students subject to the below terms and conditions.</strong></p>
<p style="font-size:16px;font-weight:700;"><strong>Terms and conditions</strong></p>
<ol>
<li>Payment plans shall be for a maximum of ten (10) months for the monthly-charged schools and maximum of two (2) instalments in a term for termly-charged schools</li>
<li>The dates of payment plan shall be within the first 15 days of the month</li>
<li>Payment plans are agreed only if the past dues, if any, are cleared in full. Payment plans re accepted only for the future fees that are not yet due for payment</li>
<li>Requested payment plans shall be for the entire academic year and not just for a single term or month</li>
<li>Automated emails and SMS will be sent as reminders five (5) days prior to the agreed payment plan date</li>
<li>Payment plans shall always be supported by post-dated cheques or auto-debit authorisation</li>
<li>Payment plans, once committed to, shall not be revoked, dishonoured or requested for postponement. Parents shall ensure that sufficient balance is available on the due date of the commitment in their bank accounts.</li>
<li>In case of a cheque return or dishonoured auto debit authorisations for any reason whatsoever, the school will charge an administration fee of AED 500/- for each return by the bank.</li>
<li>If the payment plan is dishonoured, all agreed future-dated payment plans are cancelled and parents are required to re-enter into a new payment plan after paying the past due fee</li>
<li>If the payment plan is dishonoured, the parent will be listed as a fee defaulter and the <a href="https://school.gemsoasis.com/oasisfiles/Policies/School%20Fee%20Delinquent%20Policy.pdf" target="_blank">fee delinquent policy</a> will be applied.</li>
<li>GEMS schools reserve the right to accept, approve or reject the payment plan request based on the internal assessments and credit risk analysis</li>
</ol>
<p>&nbsp;</p>
<p><strong>&nbsp;I wish to avail the payment plan facility </strong></p>
    </div>
                         <%--<table style="width: 100%;">
            <tbody>
                <tr>
                    <td style="background-color:#003a63; color:#fff;  padding: 10px 14px; width:100%;">
                        <p style="margin-bottom: 0px;">Payment Plan</p>
                    </td>
                </tr>
            </tbody>
        </table>--%>
                        <div class="table-responsive">
                             <table width="100%" style="margin-top:10px;"><tr><td>
                             <asp:RadioButtonList ID="chkcategory" runat="server" RepeatDirection="Vertical" style="margin-right:10px;">
                                  <asp:ListItem Text="Post dated cheques" Value="0" Selected="True" > </asp:ListItem>
                                  

                             </asp:RadioButtonList>
                             </td>
                                 </tr>
                                 </table>
                                 
                                 </div>
                          <div class="table-responsive">
                              <asp:Repeater ID="repInfo" runat="server">
                                  <ItemTemplate>
                                       <table cellpadding="0" width="100%" cellspacing="0" border="0" style=" border-collapse: collapse !important; border-spacing: 0 !important; padding: 0 !important; border: none 0px !important; margin-top:10px;">
                                                                            <tr>
                                                                                <td width="30px" align="center">
                                                                                   
                                                                                    <asp:CheckBox id="chkSelect" runat="server" Checked="true" />
                                                                                       <asp:HiddenField ID="hdnstu" runat="server" Value='<%# Bind("STU_ID")%>' ></asp:HiddenField>
                                                                                      <asp:HiddenField ID="hdnStatus" runat="server" Value='<%# Bind("PlanStatus_val")%>' ></asp:HiddenField>
                                                                                    <asp:HiddenField ID="hdnName" runat="server" Value='<%# Bind("SNAME")%>' ></asp:HiddenField>
                                                                                </td>
                                                                                <td valign="top" align="left" width="10%" style=" padding: 10px 14px;">
                                                                                    <asp:Image ID="imgEmpImage" runat="server" Height="80px" ImageUrl='<%# Bind("PHOTOPATH") %>' ToolTip='<%# Bind("SNAME") %>'
                                                                                        Width="60px" Style="border-radius: 50px;border: 1px solid #003a63;" />
                                                                                </td>
                                                                                <td style=" padding: 10px 14px;">
                                                                                   <strong> <%# Eval("SNAME")%> </strong><br />
                                                                                    <%# Eval("STU_NO")%> / <%# Eval("GRM_DISPLAY")%> - <%# Eval("SCT_DESCR")%><br />
                                                                                   <span><%# Eval("PlanStatus")%></span> 
                                                                                </td>
                                                                                </tr>
                                           </table>
                                  </ItemTemplate>

                              </asp:Repeater>
                              </div>
                         <div class="table-responsive" style="padding: 10px 14px;">
                              <asp:CheckBox ID="chkAggree" runat="server" Text="I hereby agree to the above terms and conditions of payment plan."/>
                        </div>
                        <div class="col-12 text-center">
                             <span  style="padding-bottom:10px;" id="spError"  runat="server" class="alert alert-danger"> 
                            
                                 </span>
                        </div>
                        <div class="col-12 text-center" style=" padding: 10px 14px;">
                              <asp:Button ID="btnSub"  CssClass="btn btn-info" runat="server" Text="Submit" />
                        </div>





           
                    


</div>


                    </div>
                </div>
            </div>
               
           </div>


   <div id="divSuccess" style="display:none">
 <div class="modal-body text-center p-5">
                        <i class="fa fa-check-circle fa-4x mb-3 text-success"></i>
                        <h2 class="mb-4">Thank You</h2>
         </div>
    <div>
                        <p class="pb-2">Dear Parent , <br> Your application for the payment plan towards the tuition fee for <span id="spName" runat="server" style="font-size:16px;font-weight:700;"></span> is successfully submitted.<br /> School finance representative will get back to you as soon as possible.<br /> The result of your application will be communicated to you via email once the review process is completed.</p>
                        <p>Thanks and Regards<br><strong>GEMS Education</strong></p>
                    </div>
</div>

   
</asp:Content>

