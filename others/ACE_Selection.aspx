﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ParentMaster.master" AutoEventWireup="false" CodeFile="ACE_Selection.aspx.vb" Inherits="Others_ACE_Selection" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphParent" Runat="Server">
<script language="javascript" type="text/javascript">

    function showDoc() {
        window.open("ACE-CIRCULAR-2019-2020.pdf");
    }

</script>
    <div class="content margin-top30 margin-bottom60">
        <div class="container">
            <div class="row">

                <!-- Posts Block -->
                <div class="posts-block col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="my-account">
                        <div class="title-box">
                            <h3>Activities for Curriculum Enrichment
                            <span class="profile-right">
                                <asp:Label ID="lbChildName" runat="server"></asp:Label>
                            </span></h3>
                        </div>
<div class="mainheading" align="left">
       
        <div class="right">
            
            <asp:LinkButton id="lnkGuidelines" forecolor="Red" OnClientClick="javascript:showGuidlines();"   runat="server">Guidelines</asp:LinkButton>
            </div>
    </div>
    <div style="padding: 4px 12px; text-align: left;">
    <p>
        Dear Parent,</p>
             The 'ACTIVITIES FOR CURRICULUM ENRICHMENT' is a dynamic programme giving oppurtunities for physical and creative recreation.
             <p>
              We would like your child to choose his/her club & game from the list given below. This will be held from 
             </p>
             <p><b>2:00 to 4:00pm on the days mentioned in the attachment&nbsp;<asp:LinkButton ID="lnkTerms" Font-Underline="True"  OnClientClick="showDoc();" runat="server">Click Here</asp:LinkButton></b></p>
    <p>
        Thank you</p>
    <div id="lblPrincipal" runat="server" style="margin-top: 4px; padding-bottom: 3px;">
    Principal
    </div>
    
</div>

    <table id="tbl_AddGroup" runat="server"  width="100%" class="table table-bordered table-responsive text-left my-orders-table">
        <tr class="tdblankAll">
        <td align="left" class="trSub_Header_Small"><h4 class="margin-bottom0"><asp:Label ID="lblHdrGames" runat="server" Text="GAMES"></asp:Label></h4></td>
        </tr>
        <tr class="tdblankAll">        
            <td align="left">
        
            <asp:RadioButtonList ID="rblGames" runat="server" AutoPostBack="True" CssClass="radioAns" Width="100%"
                RepeatColumns="6" RepeatDirection="Horizontal" >
            </asp:RadioButtonList>
        
        </td>
       
        </tr>
        <tr class="tdblankAll">
        <td align="left" class="trSub_Header_Small">
            <h4 class="margin-bottom0">
        <asp:Label ID="lblHdrClubs" runat="server" Text="CLUBS"></asp:Label>
            </h4>
            </td>
            </tr>
        <tr class="tdblankAll">
        <td>
        <asp:RadioButtonList ID="rblClubs" runat="server" AutoPostBack="True" CssClass="radioAns" Width="100%"
                RepeatColumns="6" RepeatDirection="Horizontal">
        </asp:RadioButtonList></td>
        </tr>
                <tr class="tdblankAll" id="trCommL" runat="server">
        <td align="left" class="trSub_Header_Small">
            <h4 class="margin-bottom0">
        <asp:Label ID="lblHdrComm" runat="server" Text="Communication"></asp:Label>
            </h4>
            </td>
            </tr>
        <tr class="tdblankAll" id="trCommR" runat="server">
        <td>
        <asp:RadioButtonList ID="rblComm" runat="server" AutoPostBack="True" CssClass="radioAns" Width="100%"
                RepeatColumns="6" RepeatDirection="Horizontal">
        </asp:RadioButtonList></td>
        </tr>


        <tr>
            <td align="right">
        <asp:Button ID="btnClearSelection" runat="server" CssClass="btn btn-info" 
                    Text="Clear Selection" ValidationGroup="groupM1" TabIndex="7" />
        </td>
       
        </tr>
        <tr>
        <td>
                    <fieldset id="Fieldset1" runat="server" style="height:120px;">
                        <table width="100%">
                            <tr>
                                <td class="trSub_Header_Small" align="left">
                                     <h4 class="margin-bottom3"><asp:Label ID="lblMessage" runat="server" Text="You have selected"></asp:Label></h4>
                                </td>
                            </tr>
                            <tr>
                                <td class="tdfields" align="left" style="border-bottom: 1px solid rgba(0,0,0,0.09);">
                                    <table width="100%" style="background:transparent; margin-left:16px; margin-bottom: 6px;">
                                        <tr>
                                            <td align="left" width="10%">
                                                <strong><asp:Label ID="lblGameMessage" runat="server" Text="Game  "></asp:Label></strong>
                                            </td>
                                            <td align="left" width="20%">
                                                <asp:Label ID="lblGame" runat="server" Text="NONE"></asp:Label>
                                            </td>
                                            <td align="right" >
                                                <asp:Label ID="lblGameAvail" runat="server" Text="" Font-Bold="true"></asp:Label> &nbsp; <asp:Label ID="lblGameDays" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>
                                        
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="tdfields" align="left" style="border-bottom: 1px solid rgba(0,0,0,0.09);">
                                         <table width="100%" style="background:transparent; margin-left:16px; margin-bottom: 6px; margin-top:6px;">
                                        <tr>
                                            <td align="left" width="10%" >
                                                 <strong><asp:Label ID="lblClubMessage" runat="server" Text="Club  "></asp:Label></strong>
                                            </td>
                                            <td align="left" width="20%">
                                                <asp:Label ID="lblClub" runat="server" Text="NONE"></asp:Label>
                                            </td>
                                            <td align="right">
                                             <asp:Label ID="lblClubAvail" runat="server" Font-Bold="true"></asp:Label> &nbsp; <asp:Label ID="lblClubDays" runat="server" Text=""></asp:Label>                                             
                                            </td>
                                        </tr>
                                        
                                    </table>
                                
                                </td>
                            </tr>
                              <tr id="trCommSel" runat="server">
                                <td class="tdfields" align="left" style="border-bottom: 1px solid rgba(0,0,0,0.09);">
                                         <table width="100%" style="background:transparent; margin-left:16px; margin-bottom: 6px; margin-top:6px;">
                                        <tr>
                                            <td align="left" width="10%" >
                                                 <strong><asp:Label ID="lblCommMessage" runat="server" Text="Communication  "></asp:Label></strong>
                                            </td>
                                            <td align="left" width="20%">
                                                <asp:Label ID="lblComm" runat="server" Text="NONE"></asp:Label>
                                            </td>
                                            <td align="right">
                                             <asp:Label ID="lblCommAvail" runat="server" Font-Bold="true"></asp:Label> &nbsp; <asp:Label ID="lblCommDays" runat="server" Text=""></asp:Label>                                              
                                            </td>
                                        </tr>
                                        
                                    </table>
                                
                                </td>
                            </tr>
                            <tr>
                                <td class="tdfields" align="left" style="visibility:hidden">
                                    <asp:RadioButtonList ID="rblChoice" Style="background: transparent" runat="server"
                                        AutoPostBack="False" RepeatColumns="0" RepeatDirection="Vertical" CssClass="radioAns">
                                        <asp:ListItem Text="My child will not attend ACE on optional days" Value="0"></asp:ListItem>
                                        <asp:ListItem Text="My child will attend ACE on all five days" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="My child will attend ACE only on  the compulsory days and the  Clubs Day  ( if opted )" Value="2"></asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
        </tr>
        <tr>
        <td  align="center">
            <asp:Button ID="btnConfirm" runat="server" CssClass="btn btn-info" 
                    Text="Confirm Selection" ValidationGroup="groupM1"
                        TabIndex="7" />&nbsp;
            <asp:Button ID="btnCancel" runat="server" CssClass="btn btn-info" 
                    Text="Cancel" ValidationGroup="groupM1" Visible="False"
                        TabIndex="7" />
        </td>
        
        </tr>
        <tr id="trGameError" runat="server" visible="false" >
        <td align="left"  >
            <asp:Label ID="lblGameError" width="700px" runat="server"  CssClass="divvalid"></asp:Label>
            <br />
        </td>
        </tr>
        
        <tr id="trClubError" runat="server">
        <td align="left" >
            <asp:Label ID="lblClubError" width="700px" runat="server" CssClass="divvalid"></asp:Label>
            <br />
        </td>   
        </tr>
        <tr id="trCommError" runat="server">
        <td align="left" >
            <asp:Label ID="lblCommError" width="700px" runat="server" CssClass="divvalid"></asp:Label>
            <br />
        </td>   
        </tr>
        
        <tr id="trError" runat="server" visible="false">
        <td align="left" >
            <asp:Label ID="lblError" runat="server" width="700px" CssClass="diverror" ></asp:Label><br />
            <asp:Label ID="lblError1" runat="server" width="700px" CssClass="diverror" ></asp:Label><br />
            <asp:Label ID="lblError2" runat="server" width="700px" CssClass="diverror" ></asp:Label><br />
            <asp:Label ID="lblError3" runat="server" width="700px" CssClass="diverror" ></asp:Label>
        </td>
        </tr>
    </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <asp:HiddenField id="hfACD_ID_NEXT" runat="server">
        </asp:HiddenField>
        <asp:HiddenField id="hfGRD_ID_NEXT" runat="server">
        </asp:HiddenField>
        <asp:HiddenField id="hfGender" runat="server">
        </asp:HiddenField>
        <asp:HiddenField id="hfGameACLID" runat="server">
        </asp:HiddenField>
        <asp:HiddenField id="hfClubACLID" runat="server">
        </asp:HiddenField>
            <asp:HiddenField id="hfCommACLID" runat="server">
        </asp:HiddenField>
        <asp:HiddenField id="hfLevelID" runat="server">
        </asp:HiddenField>
</asp:Content>

