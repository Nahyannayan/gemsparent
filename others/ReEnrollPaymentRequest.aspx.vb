﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Partial Class Others_ReEnrollPaymentRequest
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

       
            If Session("username") Is Nothing Then
                Session("Active_tab") = "Home"
                Session("Site_Path") = ""
                Response.Redirect("~\login.aspx")
            ElseIf Session("bPasswdChanged") = "False" Then
                Response.Redirect("~\UpdateInfo\ForcePasswordChange.aspx", False)
            ElseIf Session("bUpdateContactDetails") = "False" Then
                Response.Redirect(Session("ForceUpdate_stud"), False)
            End If



            'ViewState("viewid") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
            If Page.IsPostBack = False Then
                If Session("Payment_status") Is Nothing Then
                    btnSave.Visible = True
                ElseIf Session("Payment_status") = "Hide" Then
                    btnSave.Visible = False
                Else
                    btnSave.Visible = True
                End If

                BindStudentDetail(Session("STU_ID"))

            End If

        Catch ex As Exception

        End Try

    End Sub

    Private Sub BindStudentDetail(ByVal stuid As String)
        Try


            Dim con As String = ConnectionManger.GetOASISConnectionString
            Dim param(2) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@STU_ID", stuid)
            param(1) = New SqlClient.SqlParameter("@ACD_ID", Session("STU_ACD_ID"))
            param(2) = New SqlClient.SqlParameter("@BSU_ID", Session("sBsuid"))
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "OPL.GETSTUD_Reenroll", param)
            If ds.Tables(0).Rows.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    txtStdNo.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("STU_NO"))
                    txtStudentname.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("STU_Name"))
                    lblacademicyear.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("ACY_DESCR"))
                    stugrd.Text = Convert.ToString((ds.Tables(0).Rows(0)("STU_GRD_ID")))
                    lblsct.Text = Convert.ToString(ds.Tables(0).Rows(0)("SCT_DESCR"))
                    lblschool.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("BSU_NAME"))
                    Session("STU_NO") = Convert.ToString(ds.Tables(0).Rows(0).Item("STU_NO"))
                    Session("STU_NAME") = Convert.ToString(ds.Tables(0).Rows(0).Item("STU_Name"))
                    lblacademicyear.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("ACY_DESCR"))
                    lblDate.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
                    Session("STU_GRD_ID") = Convert.ToString((ds.Tables(0).Rows(0)("STU_GRD_ID")))
                    Session("stu_section") = Convert.ToString(ds.Tables(0).Rows(0)("SCT_DESCR"))
                    Session("BSU_NAME") = Convert.ToString(ds.Tables(0).Rows(0).Item("BSU_NAME"))
                    divAMT.InnerHtml = Convert.ToString(ds.Tables(0).Rows(0).Item("FEE_AMT")).Split(".")(0) & "  " & Convert.ToString(ds.Tables(0).Rows(0).Item("BSU_CURRENCY")) & " <font color='red'>" & Convert.ToString(ds.Tables(0).Rows(0).Item("MSG")) & "</font>"
                    Session("Enroll_AMT") = Convert.ToString(ds.Tables(0).Rows(0).Item("FEE_AMT"))
                End If
            End If
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click

        'If Val(Session("Enroll_AMT")) <> "0" Then
        ProcessCollection()
        'Else
        '    lblError.Text = "Cannot proceed please contact school."
        'End If

    End Sub
    Private Sub ProcessCollection()
        Dim boolPaymentInitiated As Boolean = False
        Dim str_error As String = ""

        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
        Dim objConn As New SqlConnection(str_conn) '
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction

        Dim vpc_OrderInfo As String = String.Empty
        Dim vpc_ReturnURL As String = String.Empty
        Dim vpc_Amount As String = String.Empty
        Dim vpc_TxnRef As String = String.Empty

        'Session("STU_ACD_ID") = "611"
        'Session("Sess_FEE_AMT") = 2300
        ' Session("STU_ID") = "232323"
        ' Session("Curr_bsu_id") = "125018"
        ' Session("STU_NO") = "12501700070003"
        Session("vpc_MerchTxnRef") = 0 'not 
        'Session("STU_NAME") = "Test Name"
        ' Session("username") 

        Try
            Dim retval As String = "1000"
            Dim STR_TYPE As Char = "S"
            Dim str_new_FCL_ID As Long = 0
            Dim str_NEW_FCL_RECNO As String = ""
            Dim rblPaymentGateway As Integer = GetPaymentProvider(Session("sBsuid"))
            Dim dblTotal As Decimal
            dblTotal = CDbl(Session("Enroll_AMT"))
            Session("CPS_ID") = rblPaymentGateway
            Dim lblDate As String = Format(Now, "dd/MMM/yyyy")

            retval = FeeCollectionOnline.F_SaveFEECOLLECTION_H_ONLINE(0, "Online", lblDate, _
             Session("STU_ACD_ID"), Session("STU_ID"), "S", Session("Enroll_AMT"), False, str_new_FCL_ID, _
             Session("sBsuid"), "Online Fee Collection", "CR", str_NEW_FCL_RECNO, _
            Request.UserHostAddress.ToString, rblPaymentGateway, stTrans)

            Dim lblFSR_FEE_ID As String = 146
            Dim txtAmountToPay As String = Session("Enroll_AMT")
            Dim lblDiscount As String = "0"
            Dim lblAmount As String = Session("Enroll_AMT")
            Dim COLLECTIONTYPE As Integer = 1 'CASH As  = 1 CHEQUES = 2 CREDIT_CARD = 3 OTHER_MODES = 4 INTERNET = 5
            If retval = "0" Then
                retval = FeeCollectionOnline.F_SaveFEECOLLSUB_ONLINE(0, str_new_FCL_ID, lblFSR_FEE_ID, _
                    txtAmountToPay, -1, lblAmount, lblDiscount, stTrans)
                If retval = "0" Then
                    If dblTotal > 0 And retval = "0" Then 'cash  here
                        retval = FeeCollectionOnline.F_SaveFEECOLLSUB_D_ONLINE(0, str_new_FCL_ID, COLLECTIONTYPE, _
                        dblTotal, "", lblDate, 0, "", "", "", stTrans)
                    End If
                    If retval = "0" Then
                        'retval = FEECOLLECTION_H_ONLINE_Validation(str_new_FCL_ID, stTrans)
                    End If
                    If retval = "0" Then
                        stTrans.Commit()
                        'btnSave.Enabled = False
                        Session("vpc_Amount") = CStr(Session("Enroll_AMT") * 100).Split(".")(0) 'CStr(dblTotal * 100).Split(".")(0)
                        vpc_OrderInfo = Session("STU_NO")  'txtStdNo.Text.Replace("  ", " ")
                        If vpc_OrderInfo.Length > 34 Then
                            vpc_OrderInfo = Left(vpc_OrderInfo, 34)
                        End If
                        Session("vpc_OrderInfo") = vpc_OrderInfo
                        Session("vpc_ReturnURL") = Request.Url.ToString.Replace("ReEnrollPaymentRequest.aspx", "PaymentResultPageReenroll.aspx")
                        vpc_TxnRef = Session("STU_NO")
                        Session("vpc_MerchTxnRef") = str_new_FCL_ID
                        Dim flagAudit As Integer = UtilityObj.operOnAudiTable("Fee Online Payment", str_new_FCL_ID, "Insert", Page.User.Identity.Name.ToString, Me.Page, Server.MachineName)
                        'lblError.Text = "<br />" & "Please note reference no. <B>" & str_new_FCL_ID & "</B><br />" & "Now press Proceed to continue.you are about to pay a amount of<B> AED." & dblTotal & "</B> "
                        'lblPayMsg.Text = "Please do not close this window or Log off until you get the final receipt."

                        boolPaymentInitiated = True
                    End If
                End If
            End If
        Catch ex As Exception
            stTrans.Rollback()

        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
        If boolPaymentInitiated Then
            Response.Redirect("PaymentGateWayReenroll.aspx")
            'PaymentRedirect(vpc_ReturnURL, vpc_OrderInfo, vpc_Amount, vpc_TxnRef)
        End If

    End Sub
    Public Function GetPaymentProvider(ByVal BSU_ID As String) As Int16
        Dim pParms(1) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(0).Value = BSU_ID
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISFINConnectionString, _
          CommandType.StoredProcedure, "GetMerchantDetails_BSU_ID", pParms)
        If Not dsData Is Nothing AndAlso dsData.Tables.Count > 0 Then
            For Each row As DataRow In dsData.Tables(0).Rows
                If row("CPS_CPM_ID") = 2 Then
                    Return row("CPS_ID")
                End If
            Next
            Return 0
        End If
    End Function
    'Public Shared Function FEECOLLECTION_H_ONLINE_Validation(ByVal FCO_ID As Integer, ByVal p_stTrans As SqlTransaction) As String
    '    Dim pParms(2) As SqlClient.SqlParameter
    '    pParms(0) = New SqlClient.SqlParameter("@FCO_ID", SqlDbType.Int)
    '    pParms(0).Value = FCO_ID

    '    pParms(1) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
    '    pParms(1).Direction = ParameterDirection.ReturnValue
    '    Dim retval As Integer
    '    retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "FEES.FEECOLLECTION_H_ONLINE_Validation", pParms)
    '    FEECOLLECTION_H_ONLINE_Validation = pParms(1).Value
    'End Function
End Class
