﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ParentMaster.master" AutoEventWireup="false" CodeFile="SiblingLinking.aspx.vb" Inherits="Others_SiblingLinking" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphParent" runat="Server">
        <link href="../CSS/Popup.css" rel="stylesheet" />
    <div class="content margin-top30 margin-bottom60">
        <div class="container">
            <div class="row">

                <!-- Posts Block -->

                <div class="posts-block col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="my-account">
                        <div class="title-box">
                            <h3 >Sibling Linking</h3>

                        </div>

                        <div align="left" style="text-align: justify;">


                            <div  class="alert alert-warning">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <div>
                                &nbsp;&nbsp;To link siblings studying in any other GEMS Schools, please enter the details mentioned below and Submit. An OTP would be sent to the registered email id of that child and on entering the valid OTP, the children would be linked. Kindly note that, linking siblings using this option doesn’t change/update any details related to the parent/student. 

                            </div>
                                </div>


                            <table class="table table-striped table-bordered table-responsive text-left my-orders-table" width="100%" cellpadding="8" cellspacing="0">


                                <tr>
                                    <td class="tdfields">Primary contact email address as registered at school<font color="red" size="1px">*</font></td>
                                    <td align="left" colspan="3">
                                        <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server"
                                            ControlToValidate="txtEmail" Display="Dynamic" EnableViewState="False"
                                            Font-Italic="True" ForeColor="red" ErrorMessage="Email address required"
                                            ValidationGroup="info">*</asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="revMPri_Email"
                                            runat="server"
                                            ControlToValidate="txtEmail" Display="Dynamic"
                                            EnableViewState="False"
                                            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                            ValidationGroup="info" ErrorMessage="Invalid Email"
                                            ForeColor="red"></asp:RegularExpressionValidator>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="tdfields">Student ID ( 14 digit student number)  </td>

                                    <td align="left" colspan="3">
                                        <asp:TextBox ID="txtAppName" runat="server" CssClass="form-control" MaxLength="300"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="tdfields">Date of Birth<font color="red" size="1px">*</font></td>

                                    <td align="left" colspan="3">

                                        <asp:TextBox ID="txtDOB" CssClass="form-control" runat="server" ></asp:TextBox> <asp:ImageButton ID="ImageButton1" CssClass="pos-absolute" runat="server" ImageUrl="~/Images/Calendar.png"  />
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MMM/yyyy"
                                            TargetControlID="txtDOB" PopupButtonID="ImageButton1">
                                        </ajaxToolkit:CalendarExtender>
                                    </td>
                                </tr>
                                <tr id="trSave" runat="server">
                                    <td
                                        colspan="4" align="center">
                                        <asp:Button ID="btnSave" runat="server" CssClass="btn btn-info" Text="Submit"
                                          ValidationGroup="info"  CausesValidation="true" />

                                    </td>
                                </tr>
                                <tr id="trOTP" class="trOTP" runat="server">
                                    <td class="tdblankAll" align="left">
                                        <asp:Label ID="Label4" runat="server" CssClass="tdfields" Text="OTP">
                                        </asp:Label>
                                    </td>
                                    <td class="tdblankAll" align="left">
                                        <asp:TextBox ID="txtOTP" runat="server"  CssClass="form-control">
                                        </asp:TextBox><br />
                                        <asp:Label ID="Label5" runat="server" Style="color: red;" Text="">
                                        </asp:Label>
                                    </td>
                                </tr>
                                <tr class="trbtnSearch" id="trbtnSearch" runat="server">
                                    <td class="tdblankAll" align="center" colspan="2">
                                        <asp:Button ID="btnEmpSearch" runat="server" CssClass="btn btn-info" ValidationGroup="popValid"
                                            Text="Continue" />
                                    </td>
                                </tr>
                                <tr>
                                    <td
                                        align="center" colspan="4">
                                        <asp:ValidationSummary ID="vsRef" runat="server"
                                            ValidationGroup="info" HeaderText="<div class='validationheader'>Please correct the following:</div>"
                                            CssClass="text-danger" ForeColor="" />

                                        <asp:Label ID="lbmsgInfo" CssClass="text-danger" runat="server"></asp:Label>

                                    </td>
                                </tr>
                                <tr class="tdblankAll">
                                    <td class="tdblankAll" align="center" colspan="2">
                                        <asp:Label ID="lblPopError" runat="server" CssClass="text-danger"></asp:Label>
                                        <asp:ValidationSummary ID="vsEmpDetails" runat="server" ValidationGroup="popValid"
                                            HeaderText="<div class='validationheader'>Please correct the following:</div>"
                                            CssClass="alert alert-danger" ForeColor="" />
                                        <asp:HiddenField ID="h_EmpNo" Value="" runat="server" />
                                    </td>
                                </tr>

                            </table>
                            <div id="pldis" runat="server" style="display: none; overflow: visible; background-color: White; border-color: #b5cae7; border-style: solid; border-width: 4px; ">
                                <div style=" margin-top: 0px; vertical-align: middle; background-color: White;">
                                    <span style="clear: right; display: inline; float: right; visibility: visible; margin-top: 0px; vertical-align: top;">
                                        <asp:ImageButton ID="btnCloseedit" runat="server" ImageUrl="~/Images/Common/PageBody/close.png" /></span>
                                </div>
                                <div class="mainheading" style="margin-top: 15px; ">
                                    <div class="left">
                                        <asp:Label ID="lblPopHead" runat="server" Text="Referral History" Font-Size="11px" Font-Bold="true"></asp:Label></div>

                                </div>
                                <asp:Panel ID="plStudAtt" runat="server" Height="450px"
                                    BackColor="White" BorderColor="Transparent" BorderStyle="none" ScrollBars="none">
                                </asp:Panel>
                            </div>

                            <asp:Button ID="btnShowPopup" runat="server" Style="display: none" />
                            <ajaxToolkit:ModalPopupExtender
                                ID="mdlPopup" runat="server" TargetControlID="btnShowPopup" PopupControlID="pldis"
                                BackgroundCssClass="modalBackground" DropShadow="false" RepositionMode="RepositionOnWindowResizeAndScroll">
                            </ajaxToolkit:ModalPopupExtender>
                        </div>
                                 <div id="testpopup" class="darkPanelM anim" runat="server" style="display: none; z-index: 3000 !important;">
                <div id="divboxpanelconfirm" runat="server" style="height: 50%; width: 80%; " class="darkPanelMTop">
                    <div class="holderInner" style="height: 90%; width: 98%;">
                        <center>
                            <iframe src="SiblingLinkingPopUp.aspx" width="100%" height="600px" class="no-border"></iframe>
                         <%--   <asp:Button ID="btnCancel" runat="server" />--%>
                            </center>
                        </div>
                    <center><asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click" CssClass="btn btn-info" /></center>
                    </div>
                                     </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

