﻿<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/ParentMaster.master" CodeFile="ActivityPaymentResultOther.aspx.vb" Inherits="others_ActivityPaymentResultOther" %>

<%@ Register Src="../UserControl/urcActvityPaidResult.ascx" TagName="urcStudentPaidResult" TagPrefix="ucs" %>
<asp:content id="Content1" contentplaceholderid="cphParent" runat="Server">

    <script src="../Scripts/jquery-1.9.1.js" type="text/javascript"></script>
    <!-- Add fancyBox -->
    <script type="text/javascript" src="../Scripts/Fancybox/jquery.fancybox.js?v=2.1.5"></script>
    <script type="text/javascript" src="../Scripts/Fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
    <link rel="stylesheet" href="../Scripts/Fancybox/jquery.fancybox.css" type="text/css" media="screen" />
    <script type="text/javascript" src="../Scripts/PopupJQuery.js"></script>

    <div class="content margin-top30 margin-bottom60">
        <div class="container">
            <div class="row">

                <!-- Posts Block -->

                <table align="center" cellpadding="0" cellspacing="0" class="BlueTable" width="90%">
                    <tr class="subheader_img">
                        <td style="height: 19px" align="left">
                            <asp:HiddenField ID="hfTaxable" runat="server" Value="0" />
                            <asp:Label ID="lblError" runat="server" EnableViewState="False"></asp:Label>
                            <asp:Label ID="lblMessage" runat="server" EnableViewState="true"></asp:Label></td>
                    </tr>
                    <%--<tr class="matters">
            <td align="left" class="tdfields">Activity Transaction Reference - For The Activity
                <asp:Label ID="lblEventName" runat="server" CssClass="matters" Visible="True" />
            </td>
        </tr>--%>
                    <tr>
                        <td align="left">
                            <ucs:urcStudentPaidResult ID="urcStudentPaidResult1" runat="server" />
                            <%-- <ucs:urcStudentPaidResult ID="urcStudentPaidResult1" runat="server" />--%>

                        </td>
                    </tr>
                </table>

                <!-- Posts Block -->
            </div>
        </div>
    </div> 
</asp:content>
