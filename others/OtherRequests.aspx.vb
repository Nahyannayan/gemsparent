﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Net.Mail
Imports System.Text
Partial Class Others_OtherRequests
    Inherits System.Web.UI.Page
    Private FLAG As Integer = 0

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If Session("username") Is Nothing Then
                Session("Active_tab") = "Home"
                Session("Site_Path") = ""
                Response.Redirect("~\login.aspx")
            ElseIf Session("bPasswdChanged") = "False" Then
                Response.Redirect("~\UpdateInfo\ForcePasswordChange.aspx", False)
            ElseIf Session("bUpdateContactDetails") = "False" Then
                Response.Redirect(Session("ForceUpdate_stud"), False)
            End If
            'lbChildName.Text = Session("STU_NAME")
            lbChildNameTop.Text = Session("STU_NAME")

            'ViewState("viewid") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
            If Page.IsPostBack = False Then
                Me.rbNo.Attributes.Add("OnClick", "ViewEmpSearch();")
                Me.rbYes.Attributes.Add("OnClick", "ViewEmpSearch();")
                'Me.rbYes.Attributes.Add("onClick", "return FancyDIV();")
                Me.trOTP.Visible = False
                showadvancedsearch(False)
                AcademicYear()
                Me.gvApplyConcession.Visible = False
                Dim objcls As New clsEmpConcessionEligibility
                GetChildren(objcls)
                ViewState("EMP_ID") = 0
            Else
                ScriptManager.RegisterClientScriptBlock(Page, Me.[GetType](), "CallJS", "CheckHidden();", True)
            End If
        Catch ex As Exception

        End Try
    End Sub
    Sub showadvancedsearch(ByVal bshow As Boolean)
        Me.trFName.Visible = bshow
        Me.trDOJ.Visible = bshow
        Me.trDOB.Visible = bshow
        Me.trbtnSearch.Visible = bshow
    End Sub
    Private Shared Function ScreenScrapeHtml(ByVal url As String) As String
        Dim objRequest As System.Net.WebRequest = System.Net.HttpWebRequest.Create(url)
        Dim sr As New StreamReader(objRequest.GetResponse().GetResponseStream())
        Dim result As String = sr.ReadToEnd()
        sr.Close()
        Return result
    End Function 'ScreenScrapeHtml
    Private Sub AcademicYear()
        Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, "[SV].[GETACADEMIC_PREV_CURR_NEXT] @PREV_ACD_ID='" & Session("STU_ACD_ID") & "',@BSU_ID='" & Session("sBsuid") & "'")
        If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
            ViewState("ACD_YEAR") = ds.Tables(0)
        End If
    End Sub
    Protected Sub btnGO_Click(sender As Object, e As EventArgs) Handles btnGO.Click
        Dim errmsg As String = ""
        lblPopError.CssClass = ""
        lblPopError.Text = errmsg
        If Me.txtEmpno.Text.Trim <> "" Then
            Dim objETC As New clsEmpConcessionEligibility
            objETC.EMP_NO = Me.txtEmpno.Text.Trim
            objETC.GetEmpDetails()
            objETC.STU_NEWJOIN = objETC.bIsNewStudent(Session("STU_ID"))
            If objETC.EMP_ID <> 0 AndAlso (objETC.EMP_bNEWJOIN = True Or objETC.STU_NEWJOIN = True) Then
                If objETC.EMP_EMAIL = "" Then
                    'ScriptManager.RegisterClientScriptBlock(Page, Me.[GetType](), "SFS", "ShowFurtherSearch();", True)
                    showadvancedsearch(True)
                Else
                    If objETC.Genereate_Email_EMP_OTP = "0" Then
                        'ScriptManager.RegisterClientScriptBlock(Page, Me.[GetType](), "SOP", "ShowOTP();", True)
                        Me.trOTP.Visible = True
                        Me.trbtnSearch.Visible = True
                    Else
                        'ScriptManager.RegisterClientScriptBlock(Page, Me.[GetType](), "SFS", "ShowFurtherSearch();", True)
                        showadvancedsearch(True)
                    End If
                End If
                btnGO.Visible = False
                ViewState("EMP_ID") = objETC.EMP_ID
            Else
                objETC.EMP_NO = ""
                ViewState("EMP_ID") = 0
                urcParentShortProfile1.EMP_ID = ""
                lblPopError.CssClass = "diverrorPopUp"

                lblPopError.Text = IIf(objETC.EMP_ID = 0, "Employee not found", IIf(objETC.EMP_bNEWJOIN = False, UtilityObj.getErrorMessage("997"), ""))
            End If
        End If
    End Sub
    Protected Sub btnEmpSearch_Click(sender As Object, e As EventArgs) Handles btnEmpSearch.Click
        Dim errmsg As String = ""
        lblPopError.CssClass = ""
        lblPopError.Text = errmsg
        If Me.trOTP.Visible = True Then
            If Me.txtOTP.Text.Trim <> "" Then
                Dim objETC As New clsEmpConcessionEligibility
                objETC.OTP = Me.txtOTP.Text.Trim
                lblPopError.CssClass = ""
                lblPopError.Text = ""
                If objETC.IsValidOTP(ViewState("EMP_ID")) Then
                    h_EmpNo.Value = objETC.EMP_NO
                    'ScriptManager.RegisterClientScriptBlock(Page, Me.[GetType](), "CallJS", "CheckHidden();", True)
                    urcParentShortProfile1.EMP_NO = objETC.EMP_NO
                    urcParentShortProfile1.Bind_Employee_Info()
                Else
                    lblPopError.CssClass = "diverrorPopUp"
                    lblPopError.Text = "Wrong/Invalid OTP"
                End If
            End If
        Else
            If ValidateSearch(errmsg) = True Then
                Dim objETC As New clsEmpConcessionEligibility
                objETC.EMP_NO = Me.txtEmpno.Text.Trim
                objETC.EMP_FNAME = Me.txtFName.Text.Trim
                objETC.EMP_DOJ = Me.txtDOJ.Text.Trim
                objETC.EMP_DOB = Me.txtDOB.Text.Trim

                Dim dtEmp As DataTable = objETC.SearchEmpDetails
                If dtEmp Is Nothing OrElse dtEmp.Rows.Count <= 0 Then
                    objETC.EMP_NO = ""
                    ViewState("EMP_ID") = 0
                    urcParentShortProfile1.EMP_ID = ""
                    lblPopError.CssClass = "diverrorPopUp"
                    lblPopError.Text = "Employee not found"
                Else
                    ViewState("EMP_ID") = objETC.EMP_ID
                End If
                h_EmpNo.Value = objETC.EMP_NO
                'ScriptManager.RegisterClientScriptBlock(Page, Me.[GetType](), "CallJS", "CheckHidden();", True)
                urcParentShortProfile1.EMP_NO = objETC.EMP_NO
                urcParentShortProfile1.Bind_Employee_Info()
            Else
                lblPopError.CssClass = "diverrorPopUp"
                lblPopError.Text = errmsg
            End If
        End If
        
    End Sub

    Private Function ValidateSearch(ByRef CommStr As String) As Boolean
        ValidateSearch = True
        CommStr = ""
        Dim ErrorStatus As String = String.Empty
        CommStr = "<UL>"
        If Me.txtEmpno.Text.Trim = "" Then
            CommStr = CommStr & "<li>Employee No"
            ValidateSearch = False
        ElseIf Me.txtFName.Text.Trim = "" Then
            CommStr = CommStr & "<li>First Name"
            ValidateSearch = False
        ElseIf Me.txtDOJ.Text = "" Then
            CommStr = CommStr & "<li>Date of Join"
            ValidateSearch = False
        ElseIf Me.txtDOB.Text = "" Then
            CommStr = CommStr & "<li>Date of Birth"
            ValidateSearch = False
        End If
        CommStr += "</UL>"

    End Function

    Private Sub LoadAcademicYear(ByRef ddlACD As DropDownList)
        ddlACD.DataSource = ViewState("ACD_YEAR")
        ddlACD.DataBind()
    End Sub

    Private Sub GetChildren(ByVal objcls As clsEmpConcessionEligibility)
        Dim dt As DataTable = objcls.GetChildren(Session("OLU_ID"))
        If dt.Rows.Count > 0 Then
            Me.gvApplyConcession.Visible = True
            Me.gvApplyConcession.DataSource = dt
            Me.gvApplyConcession.DataBind()
        End If

    End Sub

    Protected Sub gvApplyConcession_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvApplyConcession.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim ddlACD As DropDownList = DirectCast(e.Row.FindControl("ddlAcdYear"), DropDownList)
            Dim lnkStatus As LinkButton = DirectCast(e.Row.FindControl("lnkStatus"), LinkButton)
            Dim ACDID As Int32 = Convert.ToInt32(Me.gvApplyConcession.DataKeys(e.Row.RowIndex)("STU_ACD_ID"))
            Dim APPR_STATUS As String = Convert.ToString(Me.gvApplyConcession.DataKeys(e.Row.RowIndex)("APPR_STATUS"))
            Dim SCR_ID As String = Convert.ToString(Me.gvApplyConcession.DataKeys(e.Row.RowIndex)("SCR_ID"))
            If Not ddlACD Is Nothing Then
                LoadAcademicYear(ddlACD)
                ddlACD.SelectedValue = ACDID
            End If
            If APPR_STATUS <> "" Then
                lnkStatus.Visible = True
                lnkStatus.Text = APPR_STATUS
                lnkStatus.Enabled = False
            End If
            If SCR_ID = "0" Then
                e.Row.Visible = False
            End If
        End If
    End Sub

End Class
