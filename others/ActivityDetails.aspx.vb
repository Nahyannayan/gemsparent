﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Windows.Forms
Imports System.Net.Mail
Imports System.Text
Imports Mainclass
Imports System.Threading
Imports System.Windows.Forms.MessageBox
Imports UtilityObj

Partial Class Others_ActivityDetails
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64    
    Dim Message As String = Nothing
    Private trd As Thread
    Protected Sub Page_Load(ByVal sender As Object, _
        ByVal e As System.EventArgs) Handles MyBase.Load
        Try

            If Session("username") & "" = "" Then
                Response.Redirect("~/login.aspx")
            End If
            If Not Page.IsPostBack Then
                Dim ChildName As String = Session("STU_NAME")
                lbChildName.InnerText = ChildName
                If Not (Request.QueryString("ID")) Is Nothing Then
                    Dim VIEWID As String = Encr_decrData.Decrypt(Request.QueryString("ID").Replace(" ", "+"))
                    ViewState("ViewId") = VIEWID
                    Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                    Dim ds As System.Data.DataSet
                    Dim param(1) As SqlClient.SqlParameter
                    param(0) = New SqlClient.SqlParameter("@VIEWID", Convert.ToInt32(VIEWID))
                    param(1) = New SqlClient.SqlParameter("@STUID", Session("STU_ID"))
                    ds = SqlHelper.ExecuteDataset(str_conn, "OASIS.GET_ACTIVITY_DETAILS_TO_PARENTPORTAL", param)
                    If ds.Tables(0).Rows.Count = 0 Then
                        ' lblError.Text = "Sorry! Some Problem Occured .. No Details To Display"
                        Message = UtilityObj.getErrorMessage("4001")
                        ShowMessage(Message, True)
                        UtilityObj.Errorlog(Message, "ACTIVITY SERVICES")
                    Else

                        LoadDetails(ds.Tables(0))
                    End If
                Else
                    Message = UtilityObj.getErrorMessage("4027")
                    ShowMessage(Message, True)
                    UtilityObj.Errorlog("From Load:" + Message, "ACTIVITY SERVICES")
                End If
            End If
        Catch ex As Exception
            Message = UtilityObj.getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From Load:" + ex.Message, "ACTIVITY SERVICES")
        End Try
    End Sub
    Protected Sub LoadDetails(ByVal dt As DataTable)
        Try


            If Not dt.Rows(0)("ACY_DESCR") Is Nothing Or Not dt.Rows(0)("ACY_DESCR") = "" Then
                lblACDYear.InnerText = dt.Rows(0)("ACY_DESCR").ToString()
            Else
                lblACDYear.InnerText = ""
            End If
            If Not dt.Rows(0)("ALD_EVENT_NAME") Is Nothing Or Not dt.Rows(0)("ALD_EVENT_NAME") = "" Then
                lbleventname.InnerText = dt.Rows(0)("ALD_EVENT_NAME").ToString()
                ViewState("ActivtyName") = dt.Rows(0)("ALD_EVENT_NAME").ToString()
            Else
                lbleventname.InnerText = ""
                ViewState("ActivtyName") = dt.Rows(0)("ALD_EVENT_NAME").ToString()
            End If
            If Not dt.Rows(0)("ALD_EVENT_DESCR") Is Nothing Or Not dt.Rows(0)("ALD_EVENT_DESCR") = "" Then
                lbleventdesc.InnerText = dt.Rows(0)("ALD_EVENT_DESCR").ToString()
            Else
                lbleventdesc.InnerText = ""
            End If
            If Not dt.Rows(0)("ALD_APPLREQ_ST_DT") = "#1/1/1900#" And Not dt.Rows(0)("ALD_APPLREQ_END_DT") = "#1/1/1900#" Then
                If (dt.Rows(0)("ALD_APPLREQ_ST_DT").ToString() = dt.Rows(0)("ALD_APPLREQ_END_DT").ToString()) Then
                    lblApplicationDate.InnerText = Format(dt.Rows(0)("ALD_APPLREQ_ST_DT"), "dd/MMM/yyyy")
                Else
                    lblApplicationDate.InnerText = Format(dt.Rows(0)("ALD_APPLREQ_ST_DT"), "dd/MMM/yyyy") + "  To  " + Format(dt.Rows(0)("ALD_APPLREQ_END_DT"), "dd/MMM/yyyy")
                End If
            Else
                lblApplicationDate.InnerText = ""
            End If
            If Not dt.Rows(0)("ALD_EVENT_ST_DT") = "#1/1/1900#" And Not dt.Rows(0)("ALD_EVENT_END_DT") = "#1/1/1900#" Then
                If (dt.Rows(0)("ALD_EVENT_ST_DT").ToString() = dt.Rows(0)("ALD_EVENT_END_DT").ToString()) Then
                    lbleventdt.InnerText = Format(dt.Rows(0)("ALD_EVENT_ST_DT"), "dd/MMM/yyyy")
                Else
                    lbleventdt.InnerText = Format(dt.Rows(0)("ALD_EVENT_ST_DT"), "dd/MMM/yyyy") + "  To  " + Format(dt.Rows(0)("ALD_EVENT_END_DT"), "dd/MMM/yyyy")
                End If
            Else
                lbleventdt.InnerText = ""
            End If

            If Not (dt.Rows(0)("STS_FEMAIL")) Is Nothing Or Not dt.Rows(0)("STS_FEMAIL") = "" Then
                mailid.Value = dt.Rows(0)("STS_FEMAIL")
            Else
                mailid.Value = ""
            End If
            If Not (dt.Rows(0)("ALD_EMAIL_TEMPLATE")) Is Nothing Or Not dt.Rows(0)("ALD_EMAIL_TEMPLATE") = "" Then
                ViewState("MailBody") = dt.Rows(0)("ALD_EMAIL_TEMPLATE")
            Else
                ViewState("MailBody") = ""
            End If

            If Not dt.Rows(0)("ALD_EVENT_AMT") Is Nothing Then

                Dim Amount As Double = GetDoubleVal(dt.Rows(0)("ALD_EVENT_AMT"))
                Amount = String.Format("{0:#.00}", Amount)
                hidamt.Value = Amount
                lblamount.InnerText = String.Format("{0:#.00}", Amount) + " " + Session("BSU_CURRENCY")
            Else
                lblamount.InnerText = "0.00" + " " + Session("BSU_CURRENCY")
                hidamt.Value = String.Format("{0:#.00}", 0)               
            End If

            If Not dt.Rows(0)("ALD_PAY_ONLINE") Is Nothing Then
                hidOnline.Value = dt.Rows(0)("ALD_PAY_ONLINE")
            Else
                hidOnline.Value = 0
            End If

            If Not dt.Rows(0)("ALD_FEE_REG_MODE") Is Nothing Then
                hidRegMode.Value = dt.Rows(0)("ALD_FEE_REG_MODE")
            Else
                hidRegMode.Value = 0.0
            End If

            'NEW CHANGE
            If dt.Rows(0)("ALD_ALLOW_MULTIPLE") = True Then
                rowcount.Visible = True               
            Else
                rowcount.Visible = False
            End If

            If Not dt.Rows(0)("ALD_AVAILABLE_SEAT") Is Nothing And dt.Rows(0)("ALD_AVAILABLE_SEAT") > 0 Then
                hidavail.Value = dt.Rows(0)("ALD_AVAILABLE_SEAT")               
            Else
                hidavail.Value = 0
            End If

            
            'NEW CHANGE
            If Not dt.Rows(0)("ROW_COUNT") Is Nothing Then
                If (dt.Rows(0)("ROW_COUNT") > 0) Then
                    If (dt.Rows(0)("ALD_PAY_ONLINE") = True) And (dt.Rows(0)("ALD_FEE_REG_MODE") = "FC") Then
                        btnSubmit.Visible = False
                        txtCmmnts.InnerText = dt.Rows(0)("APD_STU_CMMNTS")
                        txtCmmnts.Disabled = True
                        'lblError.Text = "You have already requested for this activity"
                        Message = UtilityObj.getErrorMessage("4003")
                        divgrad.Visible = True
                        lblMessage.Text = "<ul><li>You can pay Activity amount online from 'Enroll Activity Request' page.</li><li> To make payment please Click on 'Pay Online' button and follow the screen.</li></ul>"
                        lblMessage.Visible = True
                        lblMsgHead1.Visible = True
                        ShowMessage(Message, True)
                    ElseIf (dt.Rows(0)("ALD_FEE_REG_MODE") = "RO") Then
                        btnSubmit.Visible = False
                        txtCmmnts.InnerText = dt.Rows(0)("APD_STU_CMMNTS")
                        txtCmmnts.Disabled = True
                        lblMessage.Text = "<ul><li>You have already requested for this activity.</li></ul>"
                        lblMessage.Visible = True
                        lblMsgHead1.Visible = True
                        'lblError.Text = "You have already requested for this activity"                        
                        Message = UtilityObj.getErrorMessage("4003")
                        divgrad.Visible = False
                    Else
                        btnSubmit.Visible = False
                        txtCmmnts.InnerText = dt.Rows(0)("APD_STU_CMMNTS")
                        txtCmmnts.Disabled = True
                        'lblError.Text = "You have already requested for this activity"
                        Message = UtilityObj.getErrorMessage("4003")
                        divgrad.Visible = True
                        lblMessage.Text = "<ul><li>Your activity dont have an online payment option.</li><li>Please check on the approval status of activity by clicking 'Click here to see enrolled activities and status ' button ' On approval you can pay activity fee at school counter</li></ul>"
                        lblMessage.Visible = True
                        lblMsgHead1.Visible = True
                        ShowMessage(Message, True)
                    End If
                    If (dt.Rows(0)("APD_COUNT") > 0) And (Not dt.Rows(0)("APD_COUNT") Is Nothing) Then
                        If dt.Rows(0)("ALD_ALLOW_MULTIPLE") = True Then
                            Dim count_allow As Integer = Convert.ToInt16(dt.Rows(0)("ALD_ALLOW_MULTIPLE_MAXCOUNT"))
                            DDLPOPULATE_COUNT(count_allow)
                            ddlcount.SelectedItem.Text = dt.Rows(0)("APD_COUNT").ToString
                            ddlcount.Enabled = False
                        End If
                        Dim Amount As Double = GetDoubleVal(dt.Rows(0)("APD_TOTAL_AMOUNT"))
                        lblamount.InnerText = String.Format("{0:#.00}", Amount) + " " + Session("BSU_CURRENCY")
                        hidamt.Value = Amount
                    End If
                Else
                    '///// HIDING PORTION RELATED WITH TERMS AND CONDITIONS - REHIDE ON FUTURE
                    'If Not dt.Rows(0)("ALD_TERMS_CONDITIONS") Is Nothing Then
                    '    tblTerms.Visible = True
                    '    tblTermsAccept.Visible = True
                    '    lblterms.Text = dt.Rows(0)("ALD_TERMS_CONDITIONS")
                    'Else
                    '    tblTerms.Visible = True
                    '    tblTermsAccept.Visible = True
                    '    lblterms.Text = ""
                    'End If


                    If Not (dt.Rows(0)("ALD_AVAILABLE_SEAT")) Is Nothing And Not (dt.Rows(0)("ALD_ALLOW_MULTIPLE_MAXCOUNT") Is Nothing) Then
                        If dt.Rows(0)("ALD_ALLOW_MULTIPLE") = True Then
                            If hidavail.Value < Convert.ToInt16(dt.Rows(0)("ALD_ALLOW_MULTIPLE_MAXCOUNT")) Then
                                Dim count_allow As Integer = Convert.ToInt16(dt.Rows(0)("ALD_AVAILABLE_SEAT"))
                                DDLPOPULATE_COUNT(count_allow)
                            Else
                                Dim count_allow As Integer = Convert.ToInt16(dt.Rows(0)("ALD_ALLOW_MULTIPLE_MAXCOUNT"))
                                DDLPOPULATE_COUNT(count_allow)
                            End If
                        End If
                    End If
                    btnSubmit.Visible = True
                End If
            End If
        Catch ex As Exception
            Message = UtilityObj.getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From LoadDetails: " + ex.Message, "ACTIVITY SERVICES")
        End Try
    End Sub
    Protected Sub DDLPOPULATE_COUNT(ByVal MAX_COUNT As Integer)
        ddlcount.Items.Clear()
        'ddlcount.Items.Insert(0, New ListItem("SELECT", 0))
        'ddlcount.Items.FindByText("SELECT").Selected = True
        For i As Integer = 1 To MAX_COUNT
            ddlcount.Items.Add(New ListItem(i.ToString(), i.ToString()))
        Next i
    End Sub
    Protected Sub btnSubmit_OnClick(sender As Object, e As EventArgs)

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim strans As SqlTransaction = Nothing
        Dim param(9) As SqlClient.SqlParameter
        Dim ReturnVal As String = Nothing
        Dim Comments As String = ""
        Try
            If Not txtCmmnts.InnerText Is Nothing Then
                Comments = txtCmmnts.InnerText
            End If
            'If rowcount.Visible = True Then
            '    If ddlcount.SelectedValue = 0 Then
            '        ShowMessage("Please enter a valid count ranging from 1 to any", True)
            '        Exit Sub
            '    End If
            'End If

            ''/// HIDING TERMS AND CONDITIONS CHECK BOX VALIDATION
            'If chkaccpt.Checked = False Then
            '    ShowMessage("Please accept terms and conditions", True)
            '    Exit Sub
            'End If

            Dim objConn As New SqlConnection(str_conn)
            objConn.Open()
            strans = objConn.BeginTransaction
            param(0) = New SqlClient.SqlParameter("@VIEWID", Convert.ToInt64(ViewState("ViewId")))
            param(1) = New SqlClient.SqlParameter("@STU_ID", Session("STU_ID"))
            param(2) = New SqlClient.SqlParameter("@STU_NAME", Session("STU_NAME"))
            param(3) = New SqlClient.SqlParameter("@STU_SCT_ID", Session("STU_SCT_ID"))
            param(4) = New SqlClient.SqlParameter("@STU_ACD_ID", Session("STU_ACD_ID"))
            param(5) = New SqlClient.SqlParameter("@STU_BSU_ID", Session("STU_BSU_ID"))
            param(6) = New SqlClient.SqlParameter("@STU_GRD_ID", Session("STU_GRD_ID"))
            param(7) = New SqlClient.SqlParameter("@STU_COMMENTS", Comments)
            If rowcount.Visible = True Then
                param(8) = New SqlClient.SqlParameter("@STU_COUNT", Convert.ToInt64(ddlcount.SelectedValue))
            Else
                param(8) = New SqlClient.SqlParameter("@STU_COUNT", 1)
            End If
            If Not hidTotalAmt.Value Is Nothing And Not hidTotalAmt.Value = "" Then
                param(9) = New SqlClient.SqlParameter("@TOTAL_AMOUNT", Convert.ToDecimal(hidTotalAmt.Value))
            Else
                param(9) = New SqlClient.SqlParameter("@TOTAL_AMOUNT", Convert.ToDecimal(hidamt.Value))
            End If
            ReturnVal = SqlHelper.ExecuteScalar(strans, "[OASIS].[UPDATE_ACTIVITY_DETAILS_TO_PARENTPORTAL]", param)
            strans.Commit()
            If Not ReturnVal Is Nothing Then
                If ReturnVal = "Yes" Then
                    'lblError.Text = "Enrolling Request has been sent successfully for approval."
                    Message = UtilityObj.getErrorMessage("4002")
                    ShowMessage(Message, False)
                    btnSubmit.Visible = False
                    If (hidamt.Value > 0) And hidOnline.Value = True And hidRegMode.Value = "FC" Then
                        lblMsgHead1.Visible = True
                        divgrad.Visible = True
                        lblMessage.Text = "<ul><li>This activity needs approval and your request is in queue.</li><li> You can pay Activity amount online from 'Enroll Activity Request' page. </li><li> To make payment please Click on 'Pay Online' button and follow the screen.</li></ul>"
                        lblMessage.Visible = True
                    Else
                        lblMsgHead1.Visible = True
                        divgrad.Visible = True
                        lblMessage.Text = "<ul><li>This activity needs approval and your request is in queue.</li></ul>"
                        lblMessage.Visible = True
                    End If
                ElseIf ReturnVal = "No" Then
                    'lblError.Text = "Enrolled Successfully."
                    Message = UtilityObj.getErrorMessage("4004")
                    ShowMessage(Message, False)
                    btnSubmit.Visible = False
                    SendNotificationEmail()
                    If (hidamt.Value > 0) And hidOnline.Value = True And hidRegMode.Value = "FC" Then
                        divgrad.Visible = True
                        lblMessage.Text = "<ul><li>You can pay Activity amount online from 'Enroll Activity Request' page.</li><li> To make payment please Click on 'Pay Online' button and follow the screen.</li></ul>"
                        lblMessage.Visible = True
                        lblMsgHead1.Visible = True
                    ElseIf hidRegMode.Value = "RO" Then
                        divgrad.Visible = False
                    Else
                        divgrad.Visible = True
                        lblMessage.Text = "<ul><li> Now you can pay activity fee at school counter.</li></ul>"
                        lblMessage.Visible = True
                        lblMsgHead1.Visible = True
                    End If
                Else
                    'lblError.Text = "You Have Already Enrolled."
                    Message = UtilityObj.getErrorMessage("4003")
                    ShowMessage(Message, True)
                    btnSubmit.Visible = False
                    If (hidamt.Value > 0) And hidOnline.Value = True And hidRegMode.Value = "FC" Then
                        divgrad.Visible = True
                        lblMessage.Text = "<ul><li>You can pay Activity amount online from 'Enroll Activity Request' page.</li><li> To make payment please Click on 'Pay Online' button and follow the screen.</li></ul>"
                        lblMessage.Visible = True
                        lblMsgHead1.Visible = True
                    ElseIf hidRegMode.Value = "RO" Then
                        divgrad.Visible = False
                    Else
                        divgrad.Visible = True
                        lblMessage.Text = "<ul><li>You can pay activity fee at school counter.</li></ul>"
                        lblMessage.Visible = True
                        lblMsgHead1.Visible = True
                    End If
                End If
            Else
                'lblError.Text = "You Have Already Enrolled."
                Message = UtilityObj.getErrorMessage("4003")
                ShowMessage(Message, True)
                btnSubmit.Visible = False
                If (hidamt.Value > 0) And hidOnline.Value = True And hidRegMode.Value = "FC" Then
                    divgrad.Visible = True
                    lblMessage.Text = "<ul><li>You can pay Activity amount online from 'Enroll Activity Request' page.</li><li> To make payment please Click on 'Pay Online' button and follow the screen.</li></ul>"
                    lblMessage.Visible = True
                    lblMsgHead1.Visible = True
                ElseIf hidRegMode.Value = "RO" Then
                    divgrad.Visible = False
                Else
                    divgrad.Visible = True
                    lblMessage.Text = "<ul><li>You can pay activity fee at school counter.</li></ul>"
                    lblMessage.Visible = True
                    lblMsgHead1.Visible = True
                End If
            End If

        Catch ex As Exception
            Message = UtilityObj.getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From btnSubmit_OnClick:" + ex.Message, "ACTIVITY SERVICES")
        Finally

        End Try
    End Sub
    Protected Sub SendNotificationEmail()

        Dim tomail As String = String.Empty
        Dim sendAs As String = String.Empty
        Dim SendpWD As String = String.Empty
        Dim sendPort As String = String.Empty
        Dim FromEmail As String = String.Empty
        Dim SendHost As String = String.Empty
        Dim subject As String = String.Empty
        Dim mailbody As String = String.Empty
        Try
            tomail = mailid.Value.ToString()
            If Not tomail Is Nothing Then
                If Mainclass.isEmail(tomail) Then
                    subject = "Mail Notification : Your Activity Request for *" + ViewState("ActivtyName") + "* Has been APPROVED"
                    mailbody = ViewState("MailBody").ToString()
                    'Inserting into table [COM_EMAIL_SCHEDULE] for sending the email 
                    InsertintoMailTable(subject, mailbody, tomail)
                Else
                    'Check on this
                    ' Dim ErrorMessage As String = "Error: Email Id Is Not Valid"
                    Message = UtilityObj.getErrorMessage("4005")
                    UtilityObj.Errorlog(Message, "ACTIVITY SERVICES")
                End If
            End If
        Catch EX As Exception
            'failure sending email                    
            UtilityObj.Errorlog("From SendNotificationEmail" + EX.Message, "ACTIVITY SERVICES")
        Finally

        End Try
    End Sub
    Protected Sub InsertintoMailTable(ByVal subject As String, ByVal message As String, ByVal tomailid As String)
        Try
            Dim str_conn = ConnectionManger.GetOASISConnectionString
            Dim query As String = "[dbo].[InsertIntoEmailSendSchedule] '" & Session("sBsuid") & "','ActivityRequestStatusMail','SYSTEM','" & tomailid & "','" & subject & "','" & message & "'"
            Mainclass.getDataValue(query, str_conn)
        Catch ex As Exception
            message = UtilityObj.getErrorMessage("4000")
            ShowMessage(message, True)
            UtilityObj.Errorlog("From InsertIntoMailTable: " + ex.Message, "ACTIVITY SERVICES")
        End Try
    End Sub

    Sub ShowMessage(ByVal Message As String, ByVal bError As Boolean)
        If Message <> "" Then
            If bError Then
                lblError.CssClass = "alert alert-warning"
            Else
                lblError.CssClass = "alert alert-warning"
            End If
        Else
            lblError.CssClass = "alert alert-warning"
        End If
        lblError.Text = Message
    End Sub
    Public Function GetDoubleVal(ByVal Value As Object) As Double
        'TO CONVERT VALUE TO DECIMAL VALUE
        GetDoubleVal = 0
        Try
            If IsNumeric(Value) Then
                GetDoubleVal = Convert.ToDouble(Value)
            End If
        Catch ex As Exception
            GetDoubleVal = 0
        End Try
    End Function

    'Protected Sub txtcount_TextChanged(sender As Object, e As EventArgs)
    '    If hidavail.Value < Convert.ToInt64(txtcount.Text) Then
    '        ShowMessage("Available seats are only '" + hidavail.Value.ToString + "'. Please enter a valid count", True)
    '    Else
    '        ShowMessage("", False)
    '        lblError.CssClass = ""
    '        Dim total_amount_topay As Int64 = 0
    '        Dim count As Integer = GetDoubleVal(txtcount.Text)
    '        total_amount_topay = count * GetDoubleVal(hidamt.Value)
    '        lblamount.InnerText = GetDoubleVal(total_amount_topay).ToString + " " + Session("BSU_CURRENCY")
    '        hidTotalAmt.Value = GetDoubleVal(total_amount_topay)
    '    End If
    'End Sub

    Protected Sub ddlcount_SelectedIndexChanged(sender As Object, e As EventArgs)

        'Dim MaxDdlValue As Integer = 0
        'Dim MaxDdlIndx As Integer = 1
        'For Each Items As ListItem In ddlcount.Items
        '    If (Items.Value = 0) Then
        '    Else
        '        MaxDdlValue = Items.Value
        '    End If
        'Next
        'If hidavail.Value < MaxDdlValue Then
        '    ShowMessage("Available seats are only '" + hidavail.Value.ToString + "'. Please enter a valid count", True)
        'Else
        'ShowMessage("", False)
        'lblError.CssClass = ""

        If (ddlcount.SelectedValue <> 0) Then          
            Dim count As Integer = Convert.ToInt16(ddlcount.SelectedValue)            
            Dim total_amount_topay As Double = count * hidamt.Value
            lblamount.InnerText = String.Format("{0:#.00}", total_amount_topay) + " " + Session("BSU_CURRENCY")
            hidTotalAmt.Value = String.Format("{0:#.00}", total_amount_topay)
        Else            
            Dim amount As Double = hidamt.Value
            lblamount.InnerText = String.Format("{0:#.00}", amount) + " " + Session("BSU_CURRENCY")
            hidTotalAmt.Value = String.Format("{0:#.00}", amount)
        End If        
        'End If
    End Sub
End Class
