﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Net.Mail
Imports System.Text
Partial Class others_CovidTestResult
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

        If Session("username") Is Nothing Then
            Session("Active_tab") = "Home"
            Session("Site_Path") = ""
            Response.Redirect("~\login.aspx")
        ElseIf Session("bPasswdChanged") = "False" Then
            Response.Redirect("~\UpdateInfo\ForcePasswordChange.aspx", False)
        ElseIf Session("bUpdateContactDetails") = "False" Then
            Response.Redirect(Session("ForceUpdate_stud"), False)
        End If
        lbChildName.Text = Session("STU_NAME")
        If Page.IsPostBack = False Then
            Gridbind()
        End If

    End Sub
    Sub Gridbind()
        Try
            'Dim strPath As String = Convert.ToString(readerStudent_Detail("STU_PHOTOPATH"))
            Dim connPath As String = WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ConnectionString
            Dim noImagePath As String = "~/Images/Home/no_image.gif"
            Dim con As String = ConnectionManger.GetOASISConnectionString
            Dim param(5) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@OLU_ID", Session("OLU_ID").ToString)
            param(1) = New SqlClient.SqlParameter("@connPath", connPath)
            param(2) = New SqlClient.SqlParameter("@noImagePath", noImagePath)
            param(3) = New SqlClient.SqlParameter("@STU_BSU_ID", Session("STU_BSU_ID")) '@
            param(4) = New SqlClient.SqlParameter("@SELECTED_STU_ID", 0)
            param(5) = New SqlClient.SqlParameter("@type", 1)
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "[OPL].[GETCHILD_HOME_INFO_COVID_RESULT]", param)
            gvresult.DataSource = ds.Tables(0)
            gvresult.DataBind()

        Catch ex As Exception
            gvresult.DataBind()

        End Try
    End Sub
    Sub bindNames()
        Try
            'Dim strPath As String = Convert.ToString(readerStudent_Detail("STU_PHOTOPATH"))
            Dim connPath As String = WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ConnectionString
            Dim noImagePath As String = "~/Images/Home/no_image.gif"
            Dim con As String = ConnectionManger.GetOASISConnectionString
            Dim param(5) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@OLU_ID", Session("OLU_ID").ToString)
            param(1) = New SqlClient.SqlParameter("@connPath", connPath)
            param(2) = New SqlClient.SqlParameter("@noImagePath", noImagePath)
            param(3) = New SqlClient.SqlParameter("@STU_BSU_ID", Session("STU_BSU_ID")) '@
            param(4) = New SqlClient.SqlParameter("@SELECTED_STU_ID", 0)
            param(5) = New SqlClient.SqlParameter("@type", 0)
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "[OPL].[GETCHILD_HOME_INFO_COVID_RESULT]", param)
            ddlChildNames.DataSource = ds.Tables(0)

            ddlChildNames.DataValueField = "STU_ID"
            ddlChildNames.DataTextField = "SNAME"
            ddlChildNames.DataBind()

        Catch ex As Exception
            gvresult.DataBind()

        End Try
    End Sub
    Protected Sub btnCovidresult_Click(sender As Object, e As EventArgs) Handles btnCovidresult.Click
        bindNames()
        mdlPopup.Show()
    End Sub

    Protected Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim objConn As New SqlConnection(str_conn) '
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try

            Dim retval As String = "1000"
            Dim retMsg As String = ""
            



            Dim pParms(5) As SqlClient.SqlParameter 'V1.2 ,V1.3
            pParms(0) = Mainclass.CreateSqlParameter("@COV_STU_ID", ddlChildNames.SelectedValue, SqlDbType.BigInt)
            pParms(1) = Mainclass.CreateSqlParameter("@COV_CREATED_BY", Session("OLU_ID"), SqlDbType.BigInt)
            pParms(2) = Mainclass.CreateSqlParameter("@COV_TEST_DATE", txtFromDt.Text.Trim, SqlDbType.DateTime)
            pParms(3) = Mainclass.CreateSqlParameter("@COV_RESULT", ddlresult.SelectedValue, SqlDbType.VarChar)
            pParms(4) = Mainclass.CreateSqlParameter("@COV_UPLOAD_FILE", fileUpload.FileName, SqlDbType.VarChar)
            pParms(5) = Mainclass.CreateSqlParameter("@COV_ID", 0, SqlDbType.Int, True)


            SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "Save_STU_COVID_TESTS", pParms)

            If pParms(5).Value > 0 Then
               
                Dim str = UploadEvidence(pParms(5).Value)
            End If


            If pParms(5).Value > 0 Then
                retval = 0
                stTrans.Commit()
                lblPopError.Text = "Successfully Saved, Click close button to proceed"

            Else
                stTrans.Rollback()
            End If
            Gridbind()

        Catch ex As Exception

            lblPopError.Text = ex.Message

            stTrans.Rollback()

        End Try
    End Sub
    Function UploadEvidence(ByVal p As String) As String
        Dim str As String = ""
        Try



            Dim STU_ID As String = Session("STU_ID")
            Dim STU_BSU_ID As String = Session("sBsuid")
            Dim PHOTO_PATH As String = String.Empty
            'Dim ConFigPath As String = Convert.ToString(ConfigurationManager.AppSettings("covidreleif"))


            'If Not Directory.Exists(ConFigPath & "\" & STU_BSU_ID & "\" & STU_ID & "\" & p & "\") Then
            '    Directory.CreateDirectory(ConFigPath & "\" & STU_BSU_ID & "\" & STU_ID & "\" & p & "\")
            'End If

            'If fileUpload.HasFiles Then
            '    Dim tempDir As String = ConFigPath & "\" & STU_BSU_ID & "\" & STU_ID & "\" & p & "\"

            '    Dim tempFileName As String = Replace(fileUpload.FileName, " ", "")
            '    Dim tempFileNameUsed As String = tempDir + tempFileName
            '    If uploadFile.UploadedFiles.Count Then
            '        If File.Exists(tempFileNameUsed) Then
            '            File.Delete(tempFileNameUsed)
            '        End If
            '        Try
            '            uploadFile.UploadedFiles(0).SaveAs(tempFileNameUsed)

            '        Catch ex As Exception

            '        End Try
            '        Try
            '        Catch ex As Exception
            '            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)

            '        End Try
            '    End If
            '    str = "\" & STU_BSU_ID & "\" & STU_ID & "\" & p & "\" & tempFileName
            'End If


            Dim strPostedFileName As String = fileUpload.PostedFile.FileName 'get the file name
            Dim filename As String = System.IO.Path.GetFileName(strPostedFileName).ToLower
            Dim strExtn As String = System.IO.Path.GetExtension(strPostedFileName).ToLower

            



            Dim Tempath As String = "\" & Session("STU_BSU_ID") & "\" & Session("STU_ID") & "\" & p & "\"
            Dim UploadfileName As String = fileUpload.FileName
            Dim ConFigPath As String = WebConfigurationManager.ConnectionStrings("CovidTest").ConnectionString


            Dim iImgSize As Integer = 10485760
                If fileUpload.PostedFile.ContentLength > iImgSize Then

                lblPopError.Text = "Select image size maximum 10mb"
                    Exit Function
                End If



            If (strPostedFileName <> String.Empty) Then

                If Not Directory.Exists(ConFigPath + Tempath) Then
                    Directory.CreateDirectory(ConFigPath + Tempath)
                Else
                    Dim d As New DirectoryInfo(ConFigPath + Tempath)

                    Dim fi() As System.IO.FileInfo
                    fi = d.GetFiles("*.*", SearchOption.TopDirectoryOnly)
                    If fi.Length > 0 Then '' If Having Attachments
                        For Each f As System.IO.FileInfo In fi
                            f.Delete()
                        Next
                    End If

                End If


          
                fileUpload.PostedFile.SaveAs(ConFigPath + Tempath & System.IO.Path.GetFileName(UploadfileName))

            End If


            str = "\" & STU_BSU_ID & "\" & STU_ID & "\" & p & "\" & UploadfileName
            Return str
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "Covid result")
        End Try
        Return str
    End Function
End Class
