﻿Imports System.Data

Partial Class Others_ApplyTransportConcession
    Inherits System.Web.UI.Page

    Public Property EMP_ID() As String
        Get
            Return ViewState("EMP_ID")
        End Get
        Set(value As String)
            ViewState("EMP_ID") = value
        End Set
    End Property
    Public Property STU_ID() As String
        Get
            Return ViewState("STU_ID")
        End Get
        Set(value As String)
            ViewState("STU_ID") = value
        End Set
    End Property

    Public Property ACD_ID() As String
        Get
            Return ViewState("ACD_ID")
        End Get
        Set(value As String)
            ViewState("ACD_ID") = value
        End Set
    End Property

    Public Property SSV_ID() As String
        Get
            Return ViewState("SSV_ID")
        End Get
        Set(value As String)
            ViewState("SSV_ID") = value
        End Set
    End Property
    Protected Sub btnApplyConsn_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnApplyConsn.Click
        Dim btnApplyConsn As Button = DirectCast(sender, Button)
        If SSV_ID = 0 Then
            lblPopError.CssClass = "diverrorPopUp"
            lblPopError.Text = "Transport Service Ref.No not found, Please apply for Transport Service"
            Exit Sub
        End If
        If chkTermsAndCondition.Checked = False Then
            lblPopError.CssClass = "diverrorPopUp"
            lblPopError.Text = "Please select the confirmation checkbox to continue."
            Exit Sub
        End If
        Dim objcls As New clsEmpConcessionEligibility
        Dim RetVal As String = objcls.ApplyForConcession(Session("OLU_ID"), EMP_ID, STU_ID, Session("sBsuid"), ACD_ID, SSV_ID, Session("username"))
        If RetVal <> "0" Then
            lblPopError.CssClass = "diverrorPopUp"
            hfbPB.Value = "N"
        Else
            lblPopError.CssClass = "divvalid"
            hfbPB.Value = "Y"
            Me.btnApplyConsn.Visible = False
        End If
        lblPopError.Text = UtilityObj.getErrorMessage(RetVal)
    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Me.EMP_ID = IIf(Request.QueryString("EMP_ID").Replace(" ", "+") <> "", Request.QueryString("EMP_ID").Replace(" ", "+"), "0")
            Me.STU_ID = IIf(Request.QueryString("STU_ID").Replace(" ", "+") <> "", Request.QueryString("STU_ID").Replace(" ", "+"), "0")
            Me.ACD_ID = IIf(Request.QueryString("ACD_ID").Replace(" ", "+") <> "", Request.QueryString("ACD_ID").Replace(" ", "+"), "0")
            Me.SSV_ID = IIf(Request.QueryString("SSV_ID").Replace(" ", "+") <> "", Request.QueryString("SSV_ID").Replace(" ", "+"), "0")
            Dim CsnStartDT = "", CsnEndDT As String = ""
            Dim objcls As New clsEmpConcessionEligibility
            objcls.SetConcessionDates(CsnStartDT, CsnEndDT, EMP_ID, SSV_ID, DateTime.Now.ToString("dd/MMM/yyyy"))
            LoadStudentDetails()
            If lblStuno.Text <> "" AndAlso CsnStartDT <> "" Then
                Dim Startdt As Date = CDate(CsnStartDT)
                lblMessage.Text = UtilityObj.getErrorMessage("995").Replace("###", Format(CDate(CsnStartDT), OASISConstants.DateFormat))
            End If
            chkTermsAndCondition_CheckedChanged(sender, e)
            Me.btnCancelConsn.Attributes.Add("onClick", "fancyClose();")
            Me.btnCloseedit.Attributes.Add("onClick", "fancyClose();")
        End If
    End Sub

    Private Sub LoadStudentDetails()
        Dim qry As String = "SELECT C.STU_NO,C.STU_NAME,ISNULL(C.GRM_DISPLAY,'')+' - '+ISNULL(C.SCT_DESCR,'')AS GRADE," & _
        "C.PAREA,C.PICKUP,C.DAREA,C.DROPOFF,C.ONWARDBUS,C.RETURNBUS " & _
        "FROM TRANSPORT.VV_STUDTRANSPORTINFO_ALL C WHERE STU_ID = " & STU_ID & ""

        Dim dt As DataTable = Mainclass.getDataTable(qry, ConnectionManger.GetOASISTRANSPORTConnectionString)
        If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
            Me.lblStuno.Text = dt.Rows(0)("STU_NO").ToString
            Me.lblstuName.Text = dt.Rows(0)("STU_NAME").ToString
            Me.lblGrade.Text = dt.Rows(0)("GRADE").ToString
            Me.lblPickup.Text = dt.Rows(0)("PAREA").ToString & " - " & dt.Rows(0)("PICKUP").ToString & " - " & dt.Rows(0)("ONWARDBUS").ToString
            Me.lblDropoff.Text = dt.Rows(0)("DAREA").ToString & " - " & dt.Rows(0)("DROPOFF").ToString & " - " & dt.Rows(0)("RETURNBUS").ToString
        End If

    End Sub

    Protected Sub chkTermsAndCondition_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkTermsAndCondition.CheckedChanged
        btnApplyConsn.Enabled = chkTermsAndCondition.Checked
    End Sub
End Class
