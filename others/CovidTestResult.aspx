﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ParentMaster.master" AutoEventWireup="false" CodeFile="CovidTestResult.aspx.vb" Inherits="others_CovidTestResult" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphParent" Runat="Server">

     <div class="content margin-top30 margin-bottom60">
        <div class="container">
            <div class="row">

                <!-- Posts Block -->
                <div class="posts-block col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="my-account">
                        <%--<div class="mainheading" style="display: none;">
                            <div class="left">Leave Request/Letter</div>
                            <div class="right">
                                </div>
                        </div>--%>
                        <div class="title-box">
                            <h3>Covid Test Result
                            <span class="profile-right">
                                <asp:Label ID="lbChildName" runat="server"></asp:Label>
                            </span></h3>
                        </div>
                        <div class="alert alert-warning">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <b>Covid Test Result :</b><br />
                            Please Upload your latest COVID-19 test result.<br />
                                                      

                        </div>
                         <div>
                            <div>
                            <asp:Label ID="lblmsg" runat="server"  class="alert alert-warning margin-bottom0" EnableViewState="false" Visible="false"></asp:Label>
                                </div>

                             <table class="table table-bordered table-responsive text-left my-orders-table" width="100%" cellpadding="8" cellspacing="0">
                                  <tr class="tdblankAll">
                                    <td class="tdblankAll" colspan="2">
                                          <asp:GridView ID="gvresult" runat="server" AllowPaging="True"
                                            EmptyDataText="No record available." AutoGenerateColumns="False"
                                            Width="100%" CssClass="table table-striped table-bordered table-responsive text-left my-orders-table" BorderStyle="None" BorderWidth="0px">
                                            <EmptyDataRowStyle Wrap="True" HorizontalAlign="Center" />
                                            <Columns>



                                                <asp:TemplateField HeaderText="Student No" >

                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                   
                                                    <ItemTemplate>
                                                      <asp:Label ID="lbstu_ID" runat="server" Text='<%# Bind("STU_NO")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                 <asp:TemplateField HeaderText="Student Name" >

                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                   
                                                    <ItemTemplate>
                                                      <asp:Label ID="lbstu" runat="server" Text='<%# Bind("SNAME")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                 <asp:TemplateField HeaderText="Grade/Section" >

                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                   
                                                    <ItemTemplate>
                                                      <asp:Label ID="lbstu_gr" runat="server" Text='<%# Bind("GRM_DISPLAY")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Test Date" >

                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                   
                                                    <ItemTemplate>
                                                      <asp:Label ID="lbstu_dt" runat="server" Text='<%# Bind("cov_test_date")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                 <asp:TemplateField HeaderText="Test Result" >

                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                   
                                                    <ItemTemplate>
                                                      <asp:Label ID="lbstu_dre" runat="server" Text='<%# Bind("cov_result")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                </Columns>
                                              <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                            <PagerStyle CssClass="gridpager" />
                                        </asp:GridView>
                                         <div style="width: 100%; text-align: center;">
                                            <asp:Button ID="btnCovidresult" runat="server" CssClass="btn btn-info" Text="Submit Covid Test Result" /></div>
                                        </td>
                                      </tr>
                                 </table>
    <div id="pldis" runat="server" style="display: none; overflow: visible; background-color: White; border-color: #b5cae7; border-style: solid; border-width: 4px;">
                                <div>
                                    <span style="clear: right; display: inline; float: right; visibility: visible; margin-top: 0px; vertical-align: top;">
                                        <asp:ImageButton ID="btnCloseedit" runat="server" ImageUrl="~/Images/close.png" /></span>
                                </div>
                                <div class="mainheading">
                                    <div>
                                        <asp:Label ID="lblPopHead" runat="server" Text="Covid Result"></asp:Label></div>

                                </div>
                                <asp:Panel ID="plStudAtt" runat="server"  ScrollBars="Auto">
                                    <table class="table table-striped table-condensed table-responsive text-left my-orders-table" align="center" cellpadding="8" cellspacing="3">
                                        <tr>
                                            <td class="tdfields" align="left">Name</td>
                                            <td colspan="3">
                                                  <asp:DropDownList ID="ddlChildNames" runat="server" CssClass="form-control"></asp:DropDownList></td>
                                        </tr>

                                        <tr>
                                            <td class="tdfields" align="left">Test Date<span style="color: red">*</span></td>
                                            <td>
                                                <asp:TextBox ID="txtFromDt" runat="server" Width="122px" CssClass="form-control"></asp:TextBox>
                                                <asp:ImageButton ID="imgFromDt" runat="server" ImageUrl="~/Images/Calendar.png" CssClass="pos-absolute" />
                                                <asp:RequiredFieldValidator ID="rfvFRMDT" runat="server" ControlToValidate="txtFromDt"
                                                    Display="Dynamic" ErrorMessage="Enter Test date" ForeColor="red" ValidationGroup="popValid">*</asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="REVFRMDT"
                                                    runat="server" ControlToValidate="txtFromDt" Display="Dynamic" EnableViewState="False"
                                                    ErrorMessage="Enter from  Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                                                    ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                                    ValidationGroup="popValid">*</asp:RegularExpressionValidator><asp:CustomValidator
                                                        ID="cvFrmDt" runat="server" ControlToValidate="txtFromDt"
                                                        Display="Dynamic" EnableViewState="False" ErrorMessage="Test date entered is not a valid date"
                                                        ForeColor="red" ValidationGroup="popValid">*</asp:CustomValidator><div class="remark">(dd/mmm/yyyy)</div>




                                            </td>
                                            </tr>
                                        <tr>
                                            <td class="tdfields">Test result<span style="color: red">*</span></td>
                                            <td>
                                                <asp:DropDownList ID="ddlresult" runat="server" CssClass="form-control">
                                                    <asp:ListItem Value="0" Text="--Select--" Selected="True"></asp:ListItem>
                                                    <asp:ListItem value="NEGATIVE" Text="Negative"></asp:ListItem>
                                                     <asp:ListItem value="POSITIVE" Text="Positive"></asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="rfvAttParam" runat="server" ControlToValidate="ddlresult" Display="Dynamic"
                                                    InitialValue="0" ForeColor="red" ValidationGroup="popValid" ErrorMessage="Select test result">*</asp:RequiredFieldValidator>
                                               
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" class="tdfields">COVID Test Upload<span style="color: red"></span><span class="text-muted" style="font-size: xx-small !important;"><i>Note:File size max 10 MB</i></span></td>
                                            <td align="left" colspan="3">
                                                 <asp:FileUpload ID="fileUpload" runat="server" />
                                               
                                            </td>

                                        </tr>
                                       
                                        <tr>
                                            <td style="width: 241px; height: 19px" align="left" colspan="4"></td>

                                        </tr>
                                        <tr>
                                            <td align="center" colspan="4">
                                                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn btn-info" ValidationGroup="popValid" />&nbsp;
                        <asp:Button ID="btnCloseAbs" runat="server" Text="Close" CssClass="btn btn-info" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" colspan="4"></td>
                                        </tr>
                                        <tr>
                                            <td align="left" colspan="4">
                                                <asp:Label ID="lblPopError" runat="server"></asp:Label>
                                                <asp:ValidationSummary ID="vsVisaDetails" runat="server" ValidationGroup="popValid"
                                                    HeaderText="<div class='validationheader'>Please correct the following:</div>"
                                                    CssClass="text-danger" ForeColor="" />

                                            </td>
                                        </tr>
                                    </table>

                                </asp:Panel>
        <ajaxToolkit:CalendarExtender ID="ceFRMdate" runat="server" Format="dd/MMM/yyyy"
                                PopupButtonID="imgFromDt" TargetControlID="txtFromDt" >
                            </ajaxToolkit:CalendarExtender>

        <asp:Button ID="btnShowPopup" runat="server" Style="display: none" />
                            <ajaxToolkit:ModalPopupExtender
                                ID="mdlPopup" runat="server" TargetControlID="btnShowPopup" PopupControlID="pldis"
                                BackgroundCssClass="modalBackground" DropShadow="false" RepositionMode="RepositionOnWindowResizeAndScroll">
                            </ajaxToolkit:ModalPopupExtender>
                            </div>



                             </div>



                        </div>
                    </div>
                </div>
            </div>
         </div>
</asp:Content>

