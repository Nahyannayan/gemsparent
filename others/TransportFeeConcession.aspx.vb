﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web
Imports System.Web.Configuration
Imports System.IO
Imports System.Net.Mail
Imports System.Text
Imports AjaxControlToolkit

Partial Class Others_TransportFeeConcession
    Inherits System.Web.UI.Page
    Dim passwordEncr As New Encryption64
    Public Property EMP_ID() As String
        Get
            Return ViewState("EMP_ID")
        End Get
        Set(value As String)
            ViewState("EMP_ID") = value
        End Set
    End Property
    Public Property EMP_BSU_ID() As String
        Get
            Return ViewState("EMP_BSU_ID")
        End Get
        Set(value As String)
            ViewState("EMP_BSU_ID") = value
        End Set
    End Property
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Session("username") Is Nothing Then
            Session("Active_tab") = "Home"
            Session("Site_Path") = ""
            Response.Redirect("~\General\Home.aspx")
        ElseIf Session("bPasswdChanged") = "False" Then
            Response.Redirect("~\UpdateInfo\ForcePasswordChange.aspx", False)
        ElseIf Session("bUpdateContactDetails") = "False" Then
            Response.Redirect(Session("ForceUpdate_stud"), False)
        End If

        If Not Page.IsPostBack Then
            If Not Request.QueryString("EMP_ID").Replace(" ", "+") Is Nothing Then
                EMP_ID = passwordEncr.Decrypt(Request.QueryString("EMP_ID").Replace(" ", "+"))
                Dim objcls As New clsEmpConcessionEligibility
                objcls.EMP_ID = EMP_ID
                objcls.STU_BSU_ID = Session("sBsuid")
                objcls.GetEmpDetails()
                Me.lbEmpName.Text = objcls.EMP_NAME
                EMP_BSU_ID = objcls.EMP_BSU_ID

                AcademicYear()
                GetEligibility(objcls)
                FillConcessionGrid(objcls)
                GetChildren(objcls)
            End If
        End If
    End Sub

    Private Sub GetEligibility(ByVal objcls As clsEmpConcessionEligibility)
        gvFeeBenefits.DataSource = objcls.GetEligibility
        gvFeeBenefits.DataBind()
    End Sub

    Private Sub FillConcessionGrid(ByVal objcls As clsEmpConcessionEligibility)
        gvFeeConcessionDetails.DataSource = objcls.GetConcessions
        gvFeeConcessionDetails.DataBind()
    End Sub

    Private Sub GetChildren(ByVal objcls As clsEmpConcessionEligibility)
        Me.gvApplyConcession.DataSource = objcls.GetChildren(Session("OLU_ID"))
        Me.gvApplyConcession.DataBind()
    End Sub

    Private Sub AcademicYear()
        Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, "[SV].[GETACADEMIC_PREV_CURR_NEXT] @PREV_ACD_ID='" & Session("STU_ACD_ID") & "',@BSU_ID='" & Session("sBsuid") & "'")
        If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
            ViewState("ACD_YEAR") = ds.Tables(0)
        End If
    End Sub
    Private Sub LoadAcademicYear(ByRef ddlACD As DropDownList, ByVal STU_ACD_ID As Integer)
        Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, "EXEC [SV].[GET_STU_ACADEMIC_YEAR] @STU_ACD_ID='" & STU_ACD_ID & "',@BSU_ID='" & Session("sBsuid") & "'")
        If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
            ddlACD.DataSource = ds.Tables(0)
        End If
        ddlACD.DataBind()
    End Sub

    Protected Sub gvApplyConcession_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvApplyConcession.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim ddlACD As DropDownList = DirectCast(e.Row.FindControl("ddlAcdYear"), DropDownList)
            Dim lnkApply As LinkButton = DirectCast(e.Row.FindControl("lnkApply"), LinkButton)
            Dim lnkStatus As LinkButton = DirectCast(e.Row.FindControl("lnkStatus"), LinkButton)
            Dim lnkSSVID As Label = DirectCast(e.Row.FindControl("lnkSSVID"), Label)
            Dim ACDID As Int32 = Convert.ToInt32(Me.gvApplyConcession.DataKeys(e.Row.RowIndex)("STU_ACD_ID"))
            Dim STU_ID As Int64 = Convert.ToInt64(Me.gvApplyConcession.DataKeys(e.Row.RowIndex)("STU_ID"))
            Dim APPR_STATUS As String = Convert.ToString(Me.gvApplyConcession.DataKeys(e.Row.RowIndex)("APPR_STATUS")) '
            Dim SCR_ID As Int32 = Convert.ToInt32(Me.gvApplyConcession.DataKeys(e.Row.RowIndex)("SCR_ID"))
            Dim bOPEN As Boolean = Convert.ToBoolean(Me.gvApplyConcession.DataKeys(e.Row.RowIndex)("bOPEN"))
            If Not ddlACD Is Nothing Then
                LoadAcademicYear(ddlACD, ACDID)
                ddlACD.SelectedValue = ACDID
            End If
            If Not lnkSSVID Is Nothing Then
                lnkSSVID.Text = IIf(lnkSSVID.Text = "0", "", lnkSSVID.Text)
            End If
            If Not lnkApply Is Nothing Then
                If bOPEN AndAlso lnkSSVID.Text <> "" Then
                    lnkApply.Attributes.Add("onClick", "return CallPopup('" & ddlACD.ClientID & "','" & lnkSSVID.ClientID & "'," & STU_ID & "," & EMP_ID & ");")
                    'Else
                    '    lnkApply.Attributes.Add("onClick", "alert('Concession request has been closed, Please contact School');return false;")
                End If

                If (lnkSSVID.Text <> "" AndAlso APPR_STATUS <> "") Or SCR_ID <> 0 Then
                    lnkStatus.Visible = True
                    lnkStatus.Text = APPR_STATUS
                    lnkApply.Visible = False
                ElseIf bOPEN = False Then ' concession request is closed, please contact school
                    lnkStatus.Visible = True
                    lnkStatus.Text = "CLOSED"
                    lnkStatus.Attributes.Add("onClick", "alert('" & UtilityObj.getErrorMessage("997") & "');return false;")
                    lnkApply.Visible = False
                End If
            End If
        End If
    End Sub

    Protected Sub lnkApply_Click(sender As Object, e As EventArgs)
        'divApply.Visible = True
        'Dim lnkApply As LinkButton = DirectCast(sender, LinkButton)
        'Dim gvRow As GridViewRow = DirectCast(lnkApply.NamingContainer, GridViewRow)
        'Dim ddlAcdYear As DropDownList = DirectCast(gvRow.FindControl("ddlAcdYear"), DropDownList)
        'Dim lnkSSVID As LinkButton = DirectCast(gvRow.FindControl("lnkSSVID"), LinkButton)
        'Dim Rowindex As Int16 = gvRow.RowIndex
        'Dim ACD_ID As Integer = ddlAcdYear.SelectedValue
        'Dim STU_ID As Int64 = Convert.ToInt64(Me.gvApplyConcession.DataKeys(Rowindex)("STU_ID"))
        'Dim objcls As New clsEmpConcessionEligibility
        'Dim RetVal As String = objcls.ApplyForConcession(Session("OLU_ID"), EMP_ID, STU_ID, Session("sBsuid"), ACD_ID, lnkSSVID.Text.Trim, Session("username"))
        'If RetVal <> "0" Then
        '    lblPopError.CssClass = "diverrorPopUp"
        'Else
        '    lblPopError.CssClass = "divvalid"
        'End If
        'lblPopError.Text = UtilityObj.getErrorMessage(RetVal)
        'GetChildren()
    End Sub

    Protected Sub lnkStatus_Click(sender As Object, e As EventArgs)

    End Sub

    Protected Sub ddlAcdYear_SelectedIndexChanged(sender As Object, e As EventArgs)
        lblPopError.Text = ""
        Dim ddlACD As DropDownList = DirectCast(sender, DropDownList)
        Dim gvRow As GridViewRow = DirectCast(ddlACD.NamingContainer, GridViewRow)
        Dim lnkApply As LinkButton = DirectCast(gvRow.FindControl("lnkApply"), LinkButton)
        Dim lnkStatus As LinkButton = DirectCast(gvRow.FindControl("lnkStatus"), LinkButton)
        Dim lnkSSVID As Label = DirectCast(gvRow.FindControl("lnkSSVID"), Label)
        Dim Rowindex As Int16 = gvRow.RowIndex
        Dim ACD_ID As Integer = ddlACD.SelectedValue
        Dim STU_ID As Int64 = Convert.ToInt64(Me.gvApplyConcession.DataKeys(Rowindex)("STU_ID"))
        Dim objCC As New clsEmpConcessionEligibility
        Dim SSVID As Integer = objCC.bServiceExists(STU_ID, ACD_ID)
        Dim STATUS As String = objCC.GetConcessionRequestStatus(STU_ID, ACD_ID, SSVID)
        If SSVID = 0 Then
            lnkSSVID.Text = "0"
            lnkApply.Enabled = False
            lnkApply.Attributes.Clear()
            lnkApply.ToolTip = "Transport Service Ref.No not found, Please apply for Transport Service"
        Else
            lnkSSVID.Text = SSVID
            lnkApply.Enabled = True
            lnkApply.Attributes.Add("onClick", "return CallPopup('" & ddlACD.ClientID & "','" & lnkSSVID.ClientID & "'," & STU_ID & "," & EMP_ID & ");")
            lnkApply.ToolTip = "Click to apply for Transport fee Concession"
        End If
        lnkStatus.Text = STATUS
    End Sub
End Class
