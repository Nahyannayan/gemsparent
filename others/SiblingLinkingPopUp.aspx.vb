﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Net.Mail
Imports System.Text
Partial Class Others_SiblingLinking
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try

            If Session("username") Is Nothing Then
                Session("Active_tab") = "Home"
                Session("Site_Path") = ""
                Response.Redirect("\Login.aspx")
            ElseIf Session("bPasswdChanged") = "False" Then
                Response.Redirect("~\UpdateInfo\ForcePasswordChange.aspx", False)
            ElseIf Session("bUpdateContactDetails") = "False" Then
                Response.Redirect(Session("ForceUpdate_stud"), False)
            End If
            'lbChildName.Text = Session("STU_NAME")

            'ViewState("viewid") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
            If Page.IsPostBack = False Then
                BindEmirate_info(ddlFCOMEmirate)
                BindEmirate_info(ddlMCOMEmirate)
                BindEmirate_info(ddlFCOMEmirate2)
                BindEmirate_info(ddlMCOMEmirate2)
                BindEmirate_info(ddlFArea)
                BindEmirate_info(ddlMArea)
                BindEmirate_info(ddlFArea2)
                BindEmirate_info(ddlMArea2)
                GetCompany_Name()
                GetNational_info()
                Call Student_D_Details(Session("STU_ID"))
                Call Student_D_Details2(Session("SIB_STU_ID"))
              
            End If
        Catch ex As Exception

        End Try
    End Sub
    Private Sub BindEmirate_info(ByVal ddlEmirate As DropDownList)
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String = "SELECT EMR_CODE,EMR_DESCR  FROM EMIRATE_M order by EMR_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        ddlEmirate.DataSource = ds
        ddlEmirate.DataTextField = "EMR_DESCR"
        ddlEmirate.DataValueField = "EMR_CODE"
        ddlEmirate.DataBind()
    End Sub
    Sub GetCompany_Name()
        Try
            Using GetCompany_Name_reader As SqlDataReader = studClass.GetCompany_Name()
                ddlFCompany_Name.Items.Clear()
                ddlMCompany_Name.Items.Clear()
                ddlFCompany_Name2.Items.Clear()
                ddlMCompany_Name2.Items.Clear()

                ddlFCompany_Name.Items.Add(New ListItem("Other", "0"))
                ddlMCompany_Name.Items.Add(New ListItem("Other", "0"))
                ddlFCompany_Name2.Items.Add(New ListItem("Other", "0"))
                ddlMCompany_Name2.Items.Add(New ListItem("Other", "0"))

                If GetCompany_Name_reader.HasRows = True Then
                    While GetCompany_Name_reader.Read
                        ddlFCompany_Name.Items.Add(New ListItem(GetCompany_Name_reader("comp_Name"), GetCompany_Name_reader("comp_ID")))
                        ddlMCompany_Name.Items.Add(New ListItem(GetCompany_Name_reader("comp_Name"), GetCompany_Name_reader("comp_ID")))
                        ddlFCompany_Name2.Items.Add(New ListItem(GetCompany_Name_reader("comp_Name"), GetCompany_Name_reader("comp_ID")))
                        ddlMCompany_Name2.Items.Add(New ListItem(GetCompany_Name_reader("comp_Name"), GetCompany_Name_reader("comp_ID")))
                    End While
                End If

            End Using
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "GetCompany_Name()")
        End Try
    End Sub

    Sub Student_D_Details(ByVal STU_ID As String)

        Dim temp_MMOBILE As String
        Dim arInfo As String() = New String(2) {}
        Dim splitter As Char = "-"
        Dim sPrimaryContact As String = ""
        Dim CONN As String = ConnectionManger.GetOASISConnectionString
        Dim param(2) As SqlParameter
        param(0) = New SqlParameter("@STU_ID", STU_ID)


        Using readerStudent_D_Detail As SqlDataReader = SqlHelper.ExecuteReader(CONN, CommandType.StoredProcedure, "OPL.GETPROF_STUDENT_DETAILS_FOR_GWA", param)
            If readerStudent_D_Detail.HasRows = True Then
                While readerStudent_D_Detail.Read
                    lblStudentName.Text = Session("STU_NAME")
                    ''fatehrs info
                    lblFName.Text = Convert.ToString(readerStudent_D_Detail("STS_FFIRSTNAME")) & " " & Convert.ToString(readerStudent_D_Detail("STS_FMIDNAME")) & " " & Convert.ToString(readerStudent_D_Detail("STS_FLASTNAME"))
                    txtFResAddressStreet.Text = Convert.ToString(readerStudent_D_Detail("STS_FCOMSTREET"))
                    txtFResAddArea.Text = Convert.ToString(readerStudent_D_Detail("STS_FCOMAREA"))
                    txtFResAddBuilding.Text = Convert.ToString(readerStudent_D_Detail("STS_FCOMBLDG"))
                    txtFResApartment.Text = Convert.ToString(readerStudent_D_Detail("STS_FCOMAPARTNO"))
                    txtFPOBOX.Text = Convert.ToString(readerStudent_D_Detail("STS_FCOMPOBOX"))
                    txtFOccupation.Text = Convert.ToString(readerStudent_D_Detail("STS_FOCC"))
                    txtFEmiratesId.Text = Convert.ToString(readerStudent_D_Detail("STS_FEMIRATES_ID"))
                    lblPrimaryContact.Text = IIf(Convert.ToString(readerStudent_D_Detail("STU_PRIMARYCONTACT")) = "F", "Father", "Mother")
                    txtFemail.Text = Convert.ToString(readerStudent_D_Detail("STS_FEMAIL"))

                    If Not ddlFNationality_Birth.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_FNATIONALITY"))) Is Nothing Then
                        ddlFNationality_Birth.ClearSelection()
                        ddlFNationality_Birth.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_FNATIONALITY"))).Selected = True
                    End If
                    If Not ddlFNationality_Birth.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_MNATIONALITY"))) Is Nothing Then
                        ddlMNationality_Birth.ClearSelection()
                        ddlMNationality_Birth.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_MNATIONALITY"))).Selected = True
                    End If
                    If Not ddlFArea.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_FCOMAREA_ID"))) Is Nothing Then
                        ddlFArea.ClearSelection()
                        ddlFArea.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_FCOMAREA_ID"))).Selected = True
                    End If
                    If Not ddlMArea.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_MCOMAREA_ID"))) Is Nothing Then
                        ddlMArea.ClearSelection()
                        ddlMArea.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_MCOMAREA_ID"))).Selected = True
                    End If
                    temp_MMOBILE = Convert.ToString(readerStudent_D_Detail("STS_FMOBILE"))
                    arInfo = temp_MMOBILE.Split(splitter)
                    If arInfo.Length > 2 Then
                        txtFMobile_Country.Text = arInfo(0)
                        txtFMobile_Area.Text = arInfo(1)
                        txtFMobile_No.Text = arInfo(2)
                    ElseIf arInfo.Length = 2 Then
                        txtFMobile_Area.Text = arInfo(0)
                        txtFMobile_No.Text = arInfo(1)

                    Else
                        txtFMobile_No.Text = temp_MMOBILE
                    End If

                    temp_MMOBILE = Convert.ToString(readerStudent_D_Detail("STS_MMOBILE"))
                    arInfo = temp_MMOBILE.Split(splitter)
                    If arInfo.Length > 2 Then
                        txtMMobile_Country.Text = arInfo(0)
                        txtMMobile_Area.Text = arInfo(1)
                        txtMMobile_No.Text = arInfo(2)
                    ElseIf arInfo.Length = 2 Then
                        txtMMobile_Area.Text = arInfo(0)
                        txtMMobile_No.Text = arInfo(1)

                    Else
                        txtMMobile_No.Text = temp_MMOBILE
                    End If
                    ''mothers info
                    lblMName.Text = Convert.ToString(readerStudent_D_Detail("STS_MFIRSTNAME")) & " " & Convert.ToString(readerStudent_D_Detail("STS_MMIDNAME")) & " " & Convert.ToString(readerStudent_D_Detail("STS_MLASTNAME"))
                    txtMResAddressStreet.Text = Convert.ToString(readerStudent_D_Detail("STS_MCOMSTREET"))
                    txtMResAddArea.Text = Convert.ToString(readerStudent_D_Detail("STS_MCOMAREA"))
                    txtMResAddBuilding.Text = Convert.ToString(readerStudent_D_Detail("STS_MCOMBLDG"))
                    txtMResApartment.Text = Convert.ToString(readerStudent_D_Detail("STS_MCOMAPARTNO"))
                    txtMPOBOX.Text = Convert.ToString(readerStudent_D_Detail("STS_MCOMPOBOX"))
                    txtMemail.Text = Convert.ToString(readerStudent_D_Detail("STS_MEMAIL"))
                    txtMOccupation.Text = Convert.ToString(readerStudent_D_Detail("STS_MOCC"))
                    txtMEmiratesId.Text = Convert.ToString(readerStudent_D_Detail("STS_MEMIRATES_ID"))

                    If Not ddlFCOMEmirate.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_FEMIR"))) Is Nothing Then
                        ddlFCOMEmirate.ClearSelection()
                        ddlFCOMEmirate.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_FEMIR"))).Selected = True
                    End If
                    If Not ddlMCOMEmirate.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_MEMIR"))) Is Nothing Then
                        ddlMCOMEmirate.ClearSelection()
                        ddlMCOMEmirate.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_MEMIR"))).Selected = True
                    End If

                    If Not ddlFCompany_Name.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_F_COMP_ID"))) Is Nothing Then
                        ddlFCompany_Name.ClearSelection()
                        ddlFCompany_Name.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_F_COMP_ID"))).Selected = True
                    End If

                    If Not ddlMCompany_Name.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_M_COMP_ID"))) Is Nothing Then
                        ddlMCompany_Name.ClearSelection()
                        ddlMCompany_Name.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_M_COMP_ID"))).Selected = True
                    End If
                    txtFComp_Name.Text = Convert.ToString(readerStudent_D_Detail("STS_FCOMPANY"))
                    txtMComp_Name.Text = Convert.ToString(readerStudent_D_Detail("STS_MCOMPANY"))
                End While
            Else
            End If
        End Using
    End Sub
    Sub Student_D_Details2(ByVal SIB_STU_ID As String)

        Dim temp_MMOBILE As String
        Dim arInfo As String() = New String(2) {}
        Dim splitter As Char = "-"
        Dim sPrimaryContact As String = ""
        Dim CONN As String = ConnectionManger.GetOASISConnectionString
        Dim param(2) As SqlParameter
        param(0) = New SqlParameter("@STU_ID", SIB_STU_ID)


        Using readerStudent_D_Detail As SqlDataReader = SqlHelper.ExecuteReader(CONN, CommandType.StoredProcedure, "OPL.GETPROF_STUDENT_DETAILS_FOR_GWA", param)
            If readerStudent_D_Detail.HasRows = True Then
                While readerStudent_D_Detail.Read
                    lblStudentName2.Text = Session("SIB_STU_NAME")
                    ''fatehrs info
                    lblFName2.Text = Convert.ToString(readerStudent_D_Detail("STS_FFIRSTNAME")) & " " & Convert.ToString(readerStudent_D_Detail("STS_FMIDNAME")) & " " & Convert.ToString(readerStudent_D_Detail("STS_FLASTNAME"))
                    txtFResAddressStreet2.Text = Convert.ToString(readerStudent_D_Detail("STS_FCOMSTREET"))
                    txtFResAddArea2.Text = Convert.ToString(readerStudent_D_Detail("STS_FCOMAREA"))
                    txtFResAddBuilding2.Text = Convert.ToString(readerStudent_D_Detail("STS_FCOMBLDG"))
                    txtFResApartment2.Text = Convert.ToString(readerStudent_D_Detail("STS_FCOMAPARTNO"))
                    txtFPOBOX2.Text = Convert.ToString(readerStudent_D_Detail("STS_FCOMPOBOX"))
                    txtFOccupation2.Text = Convert.ToString(readerStudent_D_Detail("STS_FOCC"))
                    txtFEmiratesId2.Text = Convert.ToString(readerStudent_D_Detail("STS_FEMIRATES_ID"))
                    lblPrimaryContact2.Text = IIf(Convert.ToString(readerStudent_D_Detail("STU_PRIMARYCONTACT")) = "F", "Father", "Mother")
                    txtFemail2.Text = Convert.ToString(readerStudent_D_Detail("STS_FEMAIL"))

                    If Not ddlFNationality_Birth2.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_FNATIONALITY"))) Is Nothing Then
                        ddlFNationality_Birth2.ClearSelection()
                        ddlFNationality_Birth2.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_FNATIONALITY"))).Selected = True
                    End If
                    If Not ddlFNationality_Birth2.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_MNATIONALITY"))) Is Nothing Then
                        ddlMNationality_Birth2.ClearSelection()
                        ddlMNationality_Birth2.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_MNATIONALITY"))).Selected = True
                    End If

                    If Not ddlFArea.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_FCOMAREA_ID"))) Is Nothing Then
                        ddlFArea.ClearSelection()
                        ddlFArea.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_FCOMAREA_ID"))).Selected = True
                    End If
                    If Not ddlMArea.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_MCOMAREA_ID"))) Is Nothing Then
                        ddlMArea.ClearSelection()
                        ddlMArea.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_MCOMAREA_ID"))).Selected = True
                    End If
                    temp_MMOBILE = Convert.ToString(readerStudent_D_Detail("STS_FMOBILE"))
                    arInfo = temp_MMOBILE.Split(splitter)
                    If arInfo.Length > 2 Then
                        txtFMobile_Country2.Text = arInfo(0)
                        txtFMobile_Area2.Text = arInfo(1)
                        txtFMobile_No2.Text = arInfo(2)
                    ElseIf arInfo.Length = 2 Then
                        txtFMobile_Area2.Text = arInfo(0)
                        txtFMobile_No2.Text = arInfo(1)

                    Else
                        txtFMobile_No2.Text = temp_MMOBILE
                    End If

                    temp_MMOBILE = Convert.ToString(readerStudent_D_Detail("STS_MMOBILE"))
                    arInfo = temp_MMOBILE.Split(splitter)
                    If arInfo.Length > 2 Then
                        txtMMobile_Country2.Text = arInfo(0)
                        txtMMobile_Area2.Text = arInfo(1)
                        txtMMobile_No2.Text = arInfo(2)
                    ElseIf arInfo.Length = 2 Then
                        txtMMobile_Area2.Text = arInfo(0)
                        txtMMobile_No2.Text = arInfo(1)

                    Else
                        txtMMobile_No2.Text = temp_MMOBILE
                    End If
                    ''mothers info
                    lblMName2.Text = Convert.ToString(readerStudent_D_Detail("STS_MFIRSTNAME")) & " " & Convert.ToString(readerStudent_D_Detail("STS_MMIDNAME")) & " " & Convert.ToString(readerStudent_D_Detail("STS_MLASTNAME"))
                    txtMResAddressStreet2.Text = Convert.ToString(readerStudent_D_Detail("STS_MCOMSTREET"))
                    txtMResAddArea2.Text = Convert.ToString(readerStudent_D_Detail("STS_MCOMAREA"))
                    txtMResAddBuilding2.Text = Convert.ToString(readerStudent_D_Detail("STS_MCOMBLDG"))
                    txtMResApartment2.Text = Convert.ToString(readerStudent_D_Detail("STS_MCOMAPARTNO"))
                    txtMPOBOX2.Text = Convert.ToString(readerStudent_D_Detail("STS_MCOMPOBOX"))
                    txtMemail2.Text = Convert.ToString(readerStudent_D_Detail("STS_MEMAIL"))
                    txtMOccupation2.Text = Convert.ToString(readerStudent_D_Detail("STS_MOCC"))
                    txtMEmiratesId2.Text = Convert.ToString(readerStudent_D_Detail("STS_MEMIRATES_ID"))

                    If Not ddlFCOMEmirate2.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_FEMIR"))) Is Nothing Then
                        ddlFCOMEmirate2.ClearSelection()
                        ddlFCOMEmirate2.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_FEMIR"))).Selected = True
                    End If
                    If Not ddlMCOMEmirate2.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_MEMIR"))) Is Nothing Then
                        ddlMCOMEmirate2.ClearSelection()
                        ddlMCOMEmirate2.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_MEMIR"))).Selected = True
                    End If

                    If Not ddlFCompany_Name2.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_F_COMP_ID"))) Is Nothing Then
                        ddlFCompany_Name2.ClearSelection()
                        ddlFCompany_Name2.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_F_COMP_ID"))).Selected = True
                    End If

                    If Not ddlMCompany_Name2.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_M_COMP_ID"))) Is Nothing Then
                        ddlMCompany_Name2.ClearSelection()
                        ddlMCompany_Name2.Items.FindByValue(Convert.ToString(readerStudent_D_Detail("STS_M_COMP_ID"))).Selected = True
                    End If
                    txtFComp_Name2.Text = Convert.ToString(readerStudent_D_Detail("STS_FCOMPANY"))
                    txtMComp_Name2.Text = Convert.ToString(readerStudent_D_Detail("STS_MCOMPANY"))
                End While
            Else
            End If
        End Using
    End Sub
    Sub GetNational_info()
        Try



            ddlFNationality_Birth.Items.Add(New ListItem("", ""))
            ddlFNationality_Birth2.Items.Add(New ListItem("", ""))
            ddlMNationality_Birth.Items.Add(New ListItem("", ""))
            ddlMNationality_Birth2.Items.Add(New ListItem("", ""))

            Using AllCountry_reader As SqlDataReader = studClass.GetNational()
                Dim di_Country As ListItem


                If AllCountry_reader.HasRows = True Then
                    While AllCountry_reader.Read
                        di_Country = New ListItem(AllCountry_reader("CTY_NATIONALITY"), AllCountry_reader("CTY_ID"))
                        ddlFNationality_Birth.Items.Add(New ListItem(AllCountry_reader("CTY_NATIONALITY"), AllCountry_reader("CTY_ID")))
                        ddlFNationality_Birth2.Items.Add(New ListItem(AllCountry_reader("CTY_NATIONALITY"), AllCountry_reader("CTY_ID")))
                        ddlMNationality_Birth.Items.Add(New ListItem(AllCountry_reader("CTY_NATIONALITY"), AllCountry_reader("CTY_ID")))
                        ddlMNationality_Birth2.Items.Add(New ListItem(AllCountry_reader("CTY_NATIONALITY"), AllCountry_reader("CTY_ID")))

                    End While



                End If
            End Using

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "GetCountry_info")
        End Try
    End Sub

    Protected Sub btnMerge_Click(sender As Object, e As EventArgs)
        Dim connection As String = ConnectionManger.GetOASISConnectionString
        Dim pParms(30) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@STS_FEMIRATES_ID", txtFEmiratesId2.Text)
        pParms(1) = New SqlClient.SqlParameter("@STS_FNATIONALITY", ddlFNationality_Birth2.SelectedItem.Value)
        pParms(2) = New SqlClient.SqlParameter("@STS_FCOMAREA_ID", ddlFArea2.SelectedItem.Value)
        pParms(3) = New SqlClient.SqlParameter("@STS_FCOMAREA", txtFResAddArea2.Text)
        pParms(4) = New SqlClient.SqlParameter("@STS_FCOMSTREET", txtFResAddressStreet2.Text)
        pParms(5) = New SqlClient.SqlParameter("@STS_FCOMBLDG", txtFResAddBuilding2.Text)
        pParms(6) = New SqlClient.SqlParameter("@STS_FCOMAPARTNO", txtFResApartment2.Text)
        pParms(7) = New SqlClient.SqlParameter("@STS_FCOMPOBOX", txtFPOBOX2.Text)
        pParms(8) = New SqlClient.SqlParameter("@STS_FOCC", txtFOccupation2.Text)
        pParms(9) = New SqlClient.SqlParameter("@STS_FEMAIL", txtFemail2.Text)
        pParms(10) = New SqlClient.SqlParameter("@STS_FMOBILE", txtFMobile_Country2.Text + "-" + txtFMobile_Area2.Text + "-" + txtFMobile_No2.Text)
        pParms(11) = New SqlClient.SqlParameter("@STS_FEMIR", ddlFCOMEmirate2.SelectedItem.Value)
        pParms(12) = New SqlClient.SqlParameter("@STS_F_COMP_ID", ddlFCompany_Name2.SelectedItem.Value)
        pParms(13) = New SqlClient.SqlParameter("@STS_FCOMPANY", txtFComp_Name2.Text)
        pParms(14) = New SqlClient.SqlParameter("@STS_MEMIRATES_ID", txtMEmiratesId2.Text)
        pParms(15) = New SqlClient.SqlParameter("@STS_MNATIONALITY", ddlMNationality_Birth2.SelectedItem.Value)
        pParms(16) = New SqlClient.SqlParameter("@STS_MCOMAREA_ID", ddlMArea2.SelectedItem.Value)
        pParms(17) = New SqlClient.SqlParameter("@STS_MCOMAREA", txtMResAddArea2.Text)
        pParms(18) = New SqlClient.SqlParameter("@STS_MCOMSTREET", txtMResAddressStreet2.Text)
        pParms(19) = New SqlClient.SqlParameter("@STS_MCOMBLDG", txtMResAddBuilding2.Text)
        pParms(20) = New SqlClient.SqlParameter("@STS_MCOMAPARTNO", txtMResApartment2.Text)
        pParms(21) = New SqlClient.SqlParameter("@STS_MCOMPOBOX", txtMPOBOX2.Text)
        pParms(22) = New SqlClient.SqlParameter("@STS_MOCC", txtMOccupation2.Text)
        pParms(23) = New SqlClient.SqlParameter("@STS_MEMAIL", txtMemail2.Text)
        pParms(24) = New SqlClient.SqlParameter("@STS_MMOBILE", txtMMobile_Country2.Text + "-" + txtMMobile_Area2.Text + "-" + txtMMobile_No2.Text)
        pParms(25) = New SqlClient.SqlParameter("@STS_MEMIR", ddlMCOMEmirate2.SelectedItem.Value)
        pParms(26) = New SqlClient.SqlParameter("@STS_M_COMP_ID", ddlMCompany_Name2.SelectedItem.Value)
        pParms(27) = New SqlClient.SqlParameter("@STS_MCOMPANY", txtMComp_Name2.Text)
        pParms(28) = New SqlClient.SqlParameter("@STU_ID", Session("SIB_STU_ID"))
        Try
            SqlHelper.ExecuteDataset(connection, CommandType.StoredProcedure, "[OPL].[UPDATE_STUDENT_DETAILS_SIBLING]", pParms)
        Catch ex As Exception
            screen.InnerText = ex.Message
        End Try
        screen.InnerText = <B>Sibling Details Updated Successfully!!!!</B>

    End Sub
End Class
