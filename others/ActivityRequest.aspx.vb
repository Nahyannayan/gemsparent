﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Net.Mail
Imports System.Text
Imports Mainclass
Imports UtilityObj
Partial Class Others_ActivityRequest
    Inherits System.Web.UI.Page
    Dim templateType As ListItemType
    Dim Encr_decrData As New Encryption64
    Dim Message As String = Nothing
    Public Property DataResult() As DataTable
        Get
            Return Session("DataActivityResult")
        End Get
        Set(ByVal value As DataTable)
            Session("DataActivityResult") = value
        End Set
    End Property

    Private Property DiscountBreakUpXML() As String
        Get
            Return ViewState("DiscountBreakUpXML")
        End Get
        Set(ByVal value As String)
            ViewState("DiscountBreakUpXML") = value
        End Set
    End Property
    Private Property bPaymentConfirmMsgShown() As Boolean
        Get
            Return ViewState("bPaymentConfirmMsgShown")
        End Get
        Set(ByVal value As Boolean)
            ViewState("bPaymentConfirmMsgShown") = value

        End Set
    End Property
    Private Property Max_Limit_ForRptr() As Integer
        Get
            Return ViewState("Max_Limit_ForRptr")
        End Get
        Set(ByVal value As Integer)
            ViewState("Max_Limit_ForRptr") = value

        End Set
    End Property
    Private Property B_DayNotallow() As Integer
        Get
            Return ViewState("B_DayNotallow")
        End Get
        Set(ByVal value As Integer)
            ViewState("B_DayNotallow") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, _
        ByVal e As System.EventArgs) Handles MyBase.Load
        Try

            If Session("username") Is Nothing Then
                Session("Active_tab") = "Home"
                Session("Site_Path") = ""
                Response.Redirect("~\Login.aspx")
            ElseIf Session("bPasswdChanged") = "False" Then
                Response.Redirect("~\UpdateInfo\ForcePasswordChange.aspx", False)
            ElseIf Session("bUpdateContactDetails") = "False" Then
                Response.Redirect(Session("ForceUpdate_stud"), False)
            End If
            lbChildName.Text = Session("STU_NAME")           
            btnEnrolledLst.Attributes.Add("OnClick", "return ShowWindowWithClose('../Others/EnrolledActivityList.aspx?ID=" & Encr_decrData.Encrypt(Session("STU_ID")) & "', 'Activity Details', '50%', '70%', 'View');")
            bPaymentConfirmMsgShown = False

            If Not Page.IsPostBack Then
                B_DayNotallow = 0
                Max_Limit_ForRptr = 0
                bPaymentConfirmMsgShown = False
                loadGrpRepeater()
            End If
        Catch ex As Exception
            Message = UtilityObj.getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From Load: " + ex.Message, "ACTIVITY SERVICES")
        End Try
    End Sub
    Protected Sub loadGrpRepeater()
        'Session("DataActivityResult") = Nothing
        DataResult = Nothing
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim ds As System.Data.DataSet
        Dim param(2) As SqlClient.SqlParameter
        param(0) = New SqlClient.SqlParameter("@TYPE", "G")
        param(1) = New SqlClient.SqlParameter("@STUID", Session("STU_ID"))
        param(2) = New SqlClient.SqlParameter("@GROUPID", 0)

        ds = SqlHelper.ExecuteDataset(str_conn, "OASIS.GET_ACTIVITY_DETAILS_TO_PARENT", param)
        If ds.Tables(0).Rows.Count = 0 Then
            'lblError.Text = "Sorry! No Activities To Display"
            Message = UtilityObj.getErrorMessage("4007")
            ShowMessage(Message, True)
        Else
            Dim dt As DataTable = ds.Tables(0)
            rptrGroup.DataSource = ds.Tables(0)
            rptrGroup.DataBind()
        End If
    End Sub

    Protected Sub rptrGroup_ItemDataBound(sender As Object, e As RepeaterItemEventArgs)
        Try
            If e.Item.DataItem Is Nothing Then
                Return
            Else
                Max_Limit_ForRptr = 0
                Dim hidAPG_ID As HiddenField = DirectCast(e.Item.FindControl("hidAPG_ID"), HiddenField)
                Dim Repeater1 As Repeater = DirectCast(e.Item.FindControl("Repeater1"), Repeater)
                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                Dim lbllimitFulltxt As Label = DirectCast(e.Item.FindControl("lbllimitFulltxt"), Label)
                'Dim divLimitFullText As HtmlGenericControl = DirectCast(e.Item.FindControl("divLimitFullText"), HtmlGenericControl)
                Dim hidMaxlimit As HiddenField = DirectCast(e.Item.FindControl("hidMaxlimit"), HiddenField)
                Dim lblMaximumLimit As Label = DirectCast(e.Item.FindControl("lblMaximumLimit"), Label)
                Dim divRepeater As HtmlGenericControl = DirectCast(e.Item.FindControl("divRepeater"), HtmlGenericControl)

                If Not hidMaxlimit Is Nothing And hidMaxlimit.Value > 0 Then
                    lblMaximumLimit.Visible = True
                    lblMaximumLimit.Text = "( Maximum allowed :  " + hidMaxlimit.Value + " )"
                    'If hidMaxlimit.Value = 1 Then
                    'Else
                    '    lblMaximumLimit.Visible = True
                    '    lblMaximumLimit.Text = "This is a group activity. Maximum number of activity can be chosen per this group is:  " + hidMaxlimit.Value
                    'End If                   
                End If

                If hidAPG_ID.Value = 0 Then
                    divRepeater.Style.Add("Background-color", "white")
                    divRepeater.Style.Add("border", "none")
                End If
                'get max count of group 
                Dim Params(2) As SqlClient.SqlParameter
                Params(0) = New SqlClient.SqlParameter("@TYPE", "M")
                Params(1) = New SqlClient.SqlParameter("@STUID", Session("STU_ID"))
                Params(2) = New SqlClient.SqlParameter("@GROUPID", hidAPG_ID.Value)
                Dim max_limit As Integer = SqlHelper.ExecuteScalar(str_conn, "OASIS.GET_ACTIVITY_DETAILS_TO_PARENT", Params)
                Max_Limit_ForRptr = max_limit
                If max_limit = 0 Then
                    'divLimitFullText.Visible = True
                    lbllimitFulltxt.Text = "Activity Selection Limit Reached"
                Else
                    lbllimitFulltxt.Text = "&nbsp;"
                End If
                Dim ds_A As System.Data.DataSet
                Dim param(2) As SqlClient.SqlParameter
                param(0) = New SqlClient.SqlParameter("@TYPE", "A")
                param(1) = New SqlClient.SqlParameter("@STUID", Session("STU_ID"))
                param(2) = New SqlClient.SqlParameter("@GROUPID", hidAPG_ID.Value)
                ds_A = SqlHelper.ExecuteDataset(str_conn, "OASIS.GET_ACTIVITY_DETAILS_TO_PARENT", param)
                Dim Dt_Result As DataTable = ds_A.Tables(0)
                '   Dim d_Col As DataColumn = New DataColumn              
                If DataResult Is Nothing Then
                    DataResult = Dt_Result
                Else
                    DataResult.Merge(Dt_Result)
                End If

                Repeater1.DataSource = ds_A
                Repeater1.DataBind()

            End If

        Catch ex As Exception

        End Try
    End Sub

    Public Sub Repeater1_ItemDataBound(sender As Object, e As RepeaterItemEventArgs)
        Try
            If e.Item.DataItem Is Nothing Then
                Return
            Else
                B_DayNotallow = 0
                Dim count As Integer = 0
                Dim ViewID As Integer = Convert.ToInt32(DirectCast(e.Item.FindControl("lblViewId"), Label).Text.ToString())

                'Get specific row from datatable 
                Dim Dt_table As DataTable = DataResult
                Dim dRow As DataRow() = Dt_table.Select("ALD_ID = " & ViewID)
                '  Dim hidAPG_ID As HiddenField = DirectCast(sender.parent.FindControl("hidAPG_ID"), HiddenField)

                'activity id and parent table child activity requested id 


                ' Dim hidAPD_ID As HiddenField = CType(e.Item.FindControl("hidAPD_ID"), HiddenField)

                'controls which show in front for name, status , amount, approval flag, and buttons
                Dim labelName As Label = DirectCast(e.Item.FindControl("lblEventName"), Label)

                Dim labelStatus As Label = DirectCast(e.Item.FindControl("lblStatus"), Label)
                Dim lblamt As Label = DirectCast(e.Item.FindControl("lblamt"), Label)
                Dim lblApprvl As Label = DirectCast(e.Item.FindControl("lblApprvl"), Label)
                Dim lnkbtnapply As LinkButton = DirectCast(e.Item.FindControl("lnkbtnapply"), LinkButton)
                Dim lnkbtnPay As LinkButton = DirectCast(e.Item.FindControl("lnkbtnPay"), LinkButton)
                Dim lnkbtnUnReg As LinkButton = DirectCast(e.Item.FindControl("lnkbtnUnReg"), LinkButton)

                'hidden fields introduced on the main flow , like available seat payment status etc
                ' Dim hidAvaiableSeat As HiddenField = DirectCast(e.Item.FindControl("hidAvaiableSeat"), HiddenField)
                ' Dim hidpaymentStatus As HiddenField = DirectCast(e.Item.FindControl("hidpaymentStatus"), HiddenField)              
                'Dim hidallowmultiple As HiddenField = CType(e.Item.FindControl("hidallowmultiple"), HiddenField)
                'Dim hidAllowMultipleActDay As HiddenField = DirectCast(e.Item.FindControl("hidAllowMultipleActDay"), HiddenField)
                ' Dim hdn_count As HiddenField = DirectCast(e.Item.FindControl("hdn_count"), HiddenField)
                'Dim hidStatus As HiddenField = DirectCast(e.Item.FindControl("hidStatus"), HiddenField)
                'Dim hidPayOnlineFlag As HiddenField = DirectCast(e.Item.FindControl("hidALD_PAY_ONLINE"), HiddenField)
                'Dim hidCollMode As HiddenField = DirectCast(e.Item.FindControl("hidCollMode"), HiddenField)
                'Dim hidAmnt As HiddenField = DirectCast(e.Item.FindControl("hidAmnt"), HiddenField)

                'controls introduced for multiple allowable activities such as coupons
                Dim divcount As HtmlGenericControl = DirectCast(e.Item.FindControl("divcount"), HtmlGenericControl)
                Dim ddlcount As DropDownList = DirectCast(e.Item.FindControl("ddlcount"), DropDownList)

                'hidden fields introduced for multiple allowable activities such as coupons
                'Dim hidMulMaxCount As HiddenField = DirectCast(e.Item.FindControl("hidMulMaxCount"), HiddenField)
                'Dim hidtotalamount As HiddenField = CType(e.Item.FindControl("hidtotalamount"), HiddenField)
                'Dim hidcount As HiddenField = CType(e.Item.FindControl("hidcount"), HiddenField)

                'hidden  fields for schedule 
                'Dim hidschedule As HiddenField = CType(e.Item.FindControl("hidschedule"), HiddenField)              

                'hidden variable introduced for payment time calculation if any transaction error occured.
                'Dim hidPaytime As HiddenField = CType(e.Item.FindControl("hidPaytime"), HiddenField)


                'portion for controls introduced for terms and conditions display
                Dim lnkTermCond As LinkButton = CType(e.Item.FindControl("lnkTermCond"), LinkButton)
                Dim divgrad As HtmlGenericControl = DirectCast(e.Item.FindControl("divgrad"), HtmlGenericControl)
                Dim chkTerm As CheckBox = DirectCast(e.Item.FindControl("chkTerm"), CheckBox)
                Dim divTermMessage As HtmlGenericControl = DirectCast(e.Item.FindControl("divTermMessage"), HtmlGenericControl)

                'hidden field introduced to find whether activity is having auto enroll
                ' Dim hidAutoEnroll As HiddenField = DirectCast(e.Item.FindControl("hidAutoEnroll"), HiddenField)

                If dRow(0)("ALD_ALLOW_MULTIPLE") Then
                    divcount.Visible = True
                    ddlcount.Items.Clear()
                    'ddlcount.Items.Insert(0, New ListItem("SELECT", 0))
                    'ddlcount.Items.FindByText("SELECT").Selected = True
                    Dim mulmaxcount As Integer = 0
                    If (Not dRow(0)("ALD_ALLOW_MULTIPLE_MAXCOUNT") Is Nothing) And (Not dRow(0)("ALD_AVAILABLE_SEAT") Is Nothing) Then
                        If (Convert.ToInt32(dRow(0)("ALD_ALLOW_MULTIPLE_MAXCOUNT")) > Convert.ToInt32(dRow(0)("ALD_AVAILABLE_SEAT"))) Then
                            mulmaxcount = Convert.ToInt32(dRow(0)("ALD_AVAILABLE_SEAT"))
                        Else
                            mulmaxcount = Convert.ToInt32(dRow(0)("ALD_ALLOW_MULTIPLE_MAXCOUNT"))
                        End If
                    End If
                    If mulmaxcount <= 0 Then
                        ddlcount.Items.Add(New ListItem(0, 0))
                        ddlcount.Enabled = False
                    Else
                        For i As Integer = 1 To mulmaxcount
                            ddlcount.Items.Add(New ListItem(i.ToString(), i.ToString()))
                        Next i
                    End If
                Else
                    divcount.Visible = False
                End If
                'lnkbtnapply.Attributes.Add("OnClick", "return ShowWindowWithClose('/Others/ActivityDetails.aspx?ID=" & Encr_decrData.Encrypt(ViewID) & "', 'Activity Details', '50%', '75%');")
                count = CheckEnrolledStatus(ViewID)
                'SHOW IF SCHEDULES ARE THERE 
                Dim str_conn As String = ConnectionManger.GetOASISConnectionString
                ' Dim divSchdl As HtmlGenericControl = DirectCast(e.Item.FindControl("divSchdl"), HtmlGenericControl)
                Dim rdoSchdlDetails As RadioButtonList = DirectCast(e.Item.FindControl("rdoSchdlDetails"), RadioButtonList)
                Dim ribbonNotPaid As HtmlGenericControl = DirectCast(e.Item.FindControl("ribbonNotPaid"), HtmlGenericControl)
                If dRow(0)("ALD_IS_SCHEDULE") Then
                    Dim paramS(2) As SqlClient.SqlParameter
                    paramS(0) = New SqlClient.SqlParameter("@TYPE", "S")
                    paramS(1) = New SqlClient.SqlParameter("@STUID", Session("STU_ID"))
                    paramS(2) = New SqlClient.SqlParameter("@GROUPID", ViewID)
                    Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, "OASIS.GET_ACTIVITY_DETAILS_TO_PARENT", paramS)
                    rdoSchdlDetails.DataSource = ds.Tables(0)
                    rdoSchdlDetails.DataValueField = "ASH_ID"
                    rdoSchdlDetails.DataTextField = "ASH_WEEKS"
                    rdoSchdlDetails.DataBind()
                    rdoSchdlDetails.SelectedIndex = 0

                    For Each item As ListItem In rdoSchdlDetails.Items
                        If item.Text.Contains("Available Seat: 0") Then
                            item.Enabled = False
                        End If
                    Next
                End If

                If dRow(0)("ALD_IS_SCHEDULE") Then
                    If (rdoSchdlDetails.Items.Count > 0) Then
                        B_DayNotallow = valiatesameday(ViewID, rdoSchdlDetails.SelectedItem.Text.Split("<br/>")(0), rdoSchdlDetails.SelectedItem.Value)
                    End If
                End If
             
                If ((count > 0 And dRow(0)("APD_PAYMENT_STATUS") = "Pending")) Then
                    If dRow(0)("ALD_IS_SCHEDULE") Then
                        If rdoSchdlDetails.Items.Count > 0 Then
                            If Not rdoSchdlDetails.Items.FindByValue(dRow(0)("APD_SCHEDULE_ID")) Is Nothing Then
                                rdoSchdlDetails.SelectedValue = rdoSchdlDetails.Items.FindByValue(dRow(0)("APD_SCHEDULE_ID")).Value
                                rdoSchdlDetails.Enabled = False
                            End If
                        End If
                    End If
                  
                    If (dRow(0)("ALD_ALLOW_MULTIPLE")) Then
                        lblamt.Text = dRow(0)("APD_TOTAL_AMOUNT") & " " & Session("BSU_CURRENCY")
                        ddlcount.SelectedValue = dRow(0)("APD_COUNT")
                        ddlcount.Enabled = False
                    End If
                    If (dRow(0)("ALD_AVAILABLE_SEAT") <= 0) Then
                        lnkbtnapply.Visible = False
                        ' labelName.Text = labelName.Text
                        labelStatus.Text = "Full Booked"
                        ribbonNotPaid.Attributes.Add("class", "ribbonInActive")
                    Else

                        lnkbtnapply.Visible = False
                        lnkbtnUnReg.Visible = True
                        If (lblApprvl.Text = "No") Then
                            If Not dRow(0)("ALD_FEE_REG_MODE") Is Nothing And Not dRow(0)("ALD_EVENT_AMT") Is Nothing And (GetDoubleVal(dRow(0)("ALD_EVENT_AMT")) > 0) And (dRow(0)("ALD_PAY_ONLINE")) Then
                                lnkbtnPay.Visible = True
                                labelStatus.Text = "Pending"
                            Else
                                lnkbtnPay.Visible = False
                                labelStatus.Text = "Pay Offline"
                            End If
                            'labelName.Text = labelName.Text
                            ribbonNotPaid.Attributes.Add("class", "ribbonPending")
                        Else
                            If Not dRow(0)("APD_STATUS") Is Nothing And dRow(0)("APD_STATUS") = "A" Then
                                If Not dRow(0)("ALD_FEE_REG_MODE") Is Nothing And Not dRow(0)("ALD_EVENT_AMT") Is Nothing And (GetDoubleVal(dRow(0)("ALD_EVENT_AMT")) > 0) And (dRow(0)("ALD_PAY_ONLINE")) Then
                                    lnkbtnPay.Visible = True
                                    labelStatus.Text = "Pending"
                                Else
                                    lnkbtnPay.Visible = False
                                    labelStatus.Text = "Pay Offline"
                                End If
                                '  labelName.Text = labelName.Text                                
                                ribbonNotPaid.Attributes.Add("class", "ribbonPending")
                            Else
                                lnkbtnPay.Visible = False
                                ' labelName.Text = labelName.Text
                                labelStatus.Text = "Need Approval"
                                ribbonNotPaid.Attributes.Add("class", "ribbonNotPaid")
                            End If
                        End If
                        Dim btnname As String = lnkbtnapply.Text
                    End If

                    ' lnkbtnapply.Attributes.Add("OnClick", "return ShowWindowWithClose('../Others/ActivityDetails.aspx?ID=" & Encr_decrData.Encrypt(ViewID) & "', 'Activity Details', '50%', '75%', '" & btnname & "');")
                ElseIf (dRow(0)("ALD_AUTO_ENROLL") And count = 0) Then
                    If (dRow(0)("ALD_AVAILABLE_SEAT") <= 0) Then
                        lnkbtnapply.Visible = False
                        ' labelName.Text = labelName.Text
                        labelStatus.Text = "Full Booked"
                        ribbonNotPaid.Attributes.Add("class", "ribbonInActive")
                    Else
                        lnkbtnapply.Visible = False
                        lnkbtnUnReg.Visible = False
                        If (lblApprvl.Text = "No") Then
                            If Not dRow(0)("ALD_FEE_REG_MODE") Is Nothing And Not dRow(0)("ALD_EVENT_AMT") Is Nothing And (GetDoubleVal(dRow(0)("ALD_EVENT_AMT")) > 0) And (dRow(0)("ALD_PAY_ONLINE")) Then
                                lnkbtnPay.Visible = True
                                labelStatus.Text = "Pending"
                            Else
                                lnkbtnPay.Visible = False
                                labelStatus.Text = "Pay Offline"
                            End If
                            ' labelName.Text = labelName.Text

                            ribbonNotPaid.Attributes.Add("class", "ribbonPending")
                        End If
                        Dim btnname As String = lnkbtnapply.Text
                    End If
                ElseIf (count > 0 And dRow(0)("APD_PAYMENT_STATUS") = "Completed") Then
                    lnkbtnapply.Visible = False
                    lnkbtnPay.Visible = False

                    If dRow(0)("ALD_IS_SCHEDULE") Then
                        If rdoSchdlDetails.Items.Count > 0 Then
                            If Not rdoSchdlDetails.Items.FindByValue(dRow(0)("APD_SCHEDULE_ID")) Is Nothing Then
                                rdoSchdlDetails.SelectedValue = rdoSchdlDetails.Items.FindByValue(dRow(0)("APD_SCHEDULE_ID")).Value
                                rdoSchdlDetails.Enabled = False
                            End If
                        End If
                    End If
                    If (dRow(0)("ALD_ALLOW_MULTIPLE")) Then
                        lblamt.Text = dRow(0)("APD_TOTAL_AMOUNT") & " " & Session("BSU_CURRENCY")
                        ddlcount.SelectedValue = dRow(0)("APD_COUNT")
                        ddlcount.Enabled = False
                    End If

                    labelStatus.Text = "Enrolled"
                    ribbonNotPaid.Attributes.Add("class", "ribbonActive")
                    If GetDoubleVal(dRow(0)("APD_TOTAL_AMOUNT")) = 0.0 Then
                        lnkbtnUnReg.Visible = True
                    End If
                    'NEW CHANGES FOR PAYMENT FIXES START 
                ElseIf (count > 0 And dRow(0)("APD_PAYMENT_STATUS") = "INITIATED") Then

                    If dRow(0)("ALD_IS_SCHEDULE") Then
                        If rdoSchdlDetails.Items.Count > 0 Then
                            If Not rdoSchdlDetails.Items.FindByValue(dRow(0)("APD_SCHEDULE_ID")) Is Nothing Then
                                rdoSchdlDetails.SelectedValue = rdoSchdlDetails.Items.FindByValue(dRow(0)("APD_SCHEDULE_ID")).Value
                                rdoSchdlDetails.Enabled = False
                            End If
                        End If
                    End If

                    Dim TIMENOW As DateTime = DateTime.Now
                    Dim PAY_TIME As DateTime = Convert.ToDateTime(dRow(0)("APD_PAY_TIME"))
                    Dim TIMEDIFF = (TIMENOW - PAY_TIME).TotalMinutes
                    If (TIMEDIFF < 15) Then
                        lnkbtnPay.Text = "Processing.."
                        labelStatus.Text = "Processing.."
                        ribbonNotPaid.Attributes.Add("class", "ribbonPending")
                        lnkbtnapply.Attributes.Add("OnClick", "AlertInitError(); return false;")
                        lnkbtnapply.Text = "Processing.."
                        '  lnkbtnapply.Visible = False
                    Else
                        dRow(0)("APD_PAYMENT_STATUS") = "Pending"
                        Dim objConn As New SqlConnection(str_conn)
                        objConn.Open()
                        Dim stTrans As SqlTransaction = objConn.BeginTransaction
                        Try
                            Dim Param(1) As SqlParameter
                            ' Param(0) = New SqlClient.SqlParameter("@APD_ID", hidAPD_ID.Value)
                            Param(0) = New SqlClient.SqlParameter("@APD_ID", dRow(0)("APD_ID"))
                            Param(1) = New SqlClient.SqlParameter("@TYPE", "U")
                            SqlHelper.ExecuteNonQuery(str_conn, "[OASIS].[ACTIVITY_PAYMENT_INITIATION]", Param)
                            stTrans.Commit()
                        Catch ex As Exception
                            stTrans.Rollback()
                        End Try
                        loadGrpRepeater()
                    End If
                    ' 'NEW CHANGES FOR PAYMENT FIXES - END 
                Else
                    If dRow(0)("ALD_AVAILABLE_SEAT") <= 0 Then
                        lnkbtnapply.Visible = False
                        ' labelName.Text = labelName.Text
                        labelStatus.Text = "Full Booked"
                        ribbonNotPaid.Attributes.Add("class", "ribbonInActive")
                    Else
                        If dRow(0)("ALD_ISTERMS") Then
                            lnkTermCond.Visible = True
                            chkTerm.Visible = True
                        End If
                        lnkbtnapply.Text = "Enroll"
                        Dim btnname As String = lnkbtnapply.Text
                        If (Max_Limit_ForRptr = 0) Then
                            If dRow(0)("ALD_ISTERMS") Then
                                chkTerm.Enabled = False
                                lnkTermCond.Attributes.Add("onmouseover", "fnMouseOver(" & divgrad.ClientID & "); return false;")
                                lnkTermCond.Attributes.Add("onmouseout", "fnMouseOut(" & divgrad.ClientID & "); return false;")
                            End If
                            lnkbtnapply.Attributes.Add("OnClick", "AlertLimitReached(); return false;")
                            lnkbtnapply.Enabled = False
                        ElseIf (B_DayNotallow = 1) Then
                            If dRow(0)("ALD_ISTERMS") Then
                                chkTerm.Enabled = False
                                lnkTermCond.Attributes.Add("onmouseover", "fnMouseOver(" & divgrad.ClientID & "); return false;")
                                lnkTermCond.Attributes.Add("onmouseout", "fnMouseOut(" & divgrad.ClientID & "); return false;")
                            End If
                            lnkbtnapply.Attributes.Add("OnClick", "AlertMultiple(); return false;")
                            lnkbtnapply.Enabled = False
                        Else
                            If dRow(0)("ALD_IS_SCHEDULE") Then
                                lblEnrollMessage2.Text = rdoSchdlDetails.SelectedItem.Text.Split("<br/>")(0)
                            Else
                                lblEnrollMessage2.Text = ""
                            End If
                            If dRow(0)("ALD_ISTERMS") Then
                                lnkTermCond.Attributes.Add("onmouseover", "fnMouseOver(" & divgrad.ClientID & "); return false;")
                                lnkTermCond.Attributes.Add("onmouseout", "fnMouseOut(" & divgrad.ClientID & "); return false;")
                                lnkbtnapply.Attributes.Add("OnClick", "ConfirmTerms(" & divTermMessage.ClientID & "); return false;")
                            Else
                                lnkbtnapply.Attributes.Add("OnClick", "ConfirmEnroll('" & lnkbtnapply.ClientID & "','" & labelName.Text & "','" & lblEnrollMessage2.Text & "'); return false;")
                            End If
                            'If chkTerm.Checked Then
                            '    lnkbtnapply.Attributes.Add("OnClick", "ConfirmEnroll('" & lnkbtnapply.ClientID & "','" & labelName.Text & "','" & lblEnrollMessage2.Text & "'); return false;")
                            '    '    ' lnkbtnapply.Attributes.Add("OnClick", "return ShowWindowWithClose('../Others/ActivityDetails.aspx?ID=" & Encr_decrData.Encrypt(ViewID) & "', 'Activity Details', '50%', '75%', '" & btnname & "');")
                            'Else
                            '    lnkbtnapply.Attributes.Add("OnClick", "ConfirmTerms(" & divTermMessage.ClientID & "); return false;")
                            'End If

                        End If
                    End If
                End If
            End If

        Catch ex As Exception
            Message = UtilityObj.getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From Repeater1_ItemDataBound: " + ex.Message, "ACTIVITY SERVICES")
        End Try
    End Sub

    Protected Function valiatesameday(ByVal ViewID As Integer, ByVal selecteditemtext As String, ByVal selecteditem As Int64) As Integer
        'Select multiple activty happening on same day, if there and if do not allow flag is there then should not allow activity to enroll. flag is getting data for the same. 
        ' [OASIS].[VALIDATE_ACT_NOTTOALLOW_SAMEDAY]()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim paramSs(3) As SqlClient.SqlParameter
        paramSs(0) = New SqlClient.SqlParameter("@STUID", Session("STU_ID"))
        paramSs(1) = New SqlClient.SqlParameter("@VIEWID", ViewID)
        paramSs(2) = New SqlClient.SqlParameter("@WEEK", selecteditemtext)
        paramSs(3) = New SqlClient.SqlParameter("@SCH_ID", selecteditem)
        Dim ds_notallow As DataSet = SqlHelper.ExecuteDataset(str_conn, "[OASIS].[VALIDATE_ACT_NOTTOALLOW_SAMEDAY]", paramSs)
        If ds_notallow.Tables.Count > 0 Then
            If ds_notallow.Tables(0).Rows.Count > 0 Then
                B_DayNotallow = 1
            Else
                B_DayNotallow = 0
            End If
        End If
        Return B_DayNotallow
    End Function
    Protected Function CheckEnrolledStatus(ByVal ViewId As Integer) As Integer
        Dim rowcount As Integer = 0
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim Param(1) As SqlParameter
        Param(0) = New SqlClient.SqlParameter("@VIEWID", ViewId)
        Param(1) = New SqlClient.SqlParameter("@STUID", Session("STU_ID"))
        rowcount = SqlHelper.ExecuteScalar(str_conn, "[OASIS].[GET_ACT_REQ_DETAILS_PARENTTABLE]", Param)
        Return rowcount
    End Function

    Protected Sub lnkbtnPay_Command(ByVal sender As Object, ByVal e As CommandEventArgs)
        Dim myRow As Object
        ' Session("Activity_Name") = e.CommandArgument.ToString()
        myRow = sender.parent

        Dim lnkbtnapply As LinkButton = DirectCast(myRow.FindControl("lnkbtnapply"), LinkButton)
        Dim ViewID As Integer = Convert.ToInt32(DirectCast(myRow.FindControl("lblViewId"), Label).Text.ToString())
        'Get specific row from datatable 
        Dim Dt_table As DataTable = DataResult
        Dim dRow As DataRow() = Dt_table.Select("ALD_ID = " & ViewID)


        'auto enroll code
        If (dRow(0)("ALD_AUTO_ENROLL")) Then
            hidAutoStatus.Value = "true"
            lnkbtnapply_Click(lnkbtnapply, Nothing)
            loadGrpRepeater()
            Dt_table = DataResult
            dRow = Dt_table.Select("ALD_ID = " & ViewID)
        End If
       

        ' Dim hidFeetypeId As HiddenField = CType(myRow.FindControl("hidFeetypeId"), HiddenField)
        'Dim hidAmnt As HiddenField = CType(myRow.FindControl("hidAmnt"), HiddenField)
        'Dim hidAPD_ID As HiddenField = CType(myRow.Findcontrol("hidAPD_ID"), HiddenField)
        'Dim hidallowmultiple As HiddenField = CType(myRow.Findcontrol("hidallowmultiple"), HiddenField)
        'Dim hidtotalamount As HiddenField = CType(myRow.Findcontrol("hidtotalamount"), HiddenField)
        'Dim hidPaytime As HiddenField = CType(myRow.FindControl("hidPaytime"), HiddenField)
        ' Dim hidpaymentStatus As HiddenField = DirectCast(myRow.FindControl("hidpaymentStatus"), HiddenField)
        'Dim hidCollMode As HiddenField = CType(myRow.findcontrol("hidCollMode"), HiddenField)
        'Dim hidACTid As HiddenField = CType(myRow.FindControl("hidACTid"), HiddenField)
        'Dim hidEvtId As HiddenField = CType(myRow.FindControl("hidEvtId"), HiddenField)
        'Dim hidTaxCode As HiddenField = CType(myRow.FindControl("hidTaxCode"), HiddenField)

        If Not dRow(0)("ALD_EVENT_AMT") Is Nothing And (GetDoubleVal(dRow(0)("ALD_EVENT_AMT")) > 0) Then

            ' hidpaymentStatus.Value = "Pending"
            dRow(0)("APD_PAYMENT_STATUS") = "Pending"
            If bPaymentConfirmMsgShown = True Then
                If dRow(0)("ALD_FEE_REG_MODE") = "FC" Then
                    Response.Redirect("../Fees/feePaymentRedirectNew.aspx?SRC=" & Encr_decrData.Encrypt("ACTIVITY_FC"))
                ElseIf dRow(0)("ALD_FEE_REG_MODE") = "OC" Then
                    Response.Redirect("../Fees/feePaymentRedirectNew.aspx?SRC=" & Encr_decrData.Encrypt("ACTIVITY_OC"))
                End If

                Exit Sub
            End If

            Dim dt_cpsid As DataTable = Nothing
            Dim pParms(1) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
            pParms(0).Value = Session("STU_BSU_ID")

            Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISFINConnectionString, _
              CommandType.StoredProcedure, "dbo.GET_MERCHANTDETAILS_FOR_ACTIVITY_BSU_ID", pParms)
            If Not dsData Is Nothing AndAlso dsData.Tables.Count > 0 Then
                dt_cpsid = dsData.Tables(0)
                If Not dt_cpsid.Rows(0)("CPS_ID") Is Nothing And Not dt_cpsid.Rows(0)("CPS_ID") = 0 Then
                    Session("CPS_ID") = dt_cpsid.Rows(0)("CPS_ID")
                Else
                    Session("CPS_ID") = 0
                End If
            End If
            If (dRow(0)("ALD_FEE_REG_MODE") = "FC") Then
                Dim vpc_OrderInfo As String = String.Empty
                Dim boolPaymentInitiated As Boolean = False
                Dim dblTotal As Decimal
                If (dRow(0)("ALD_ALLOW_MULTIPLE") = True) Then
                    dblTotal = GetDoubleVal(dRow(0)("APD_TOTAL_AMOUNT"))
                Else
                    dblTotal = GetDoubleVal(dRow(0)("ALD_EVENT_AMT"))
                End If
                'Dim dtFrom As DateTime
                'dtFrom = CDate(DateTime.Today)
                Dim str_new_FCL_ID As Long
                Dim str_NEW_FCL_RECNO As String = ""
                Dim StuNos As String = "Activity (FC)"
                Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
                Dim objConn As New SqlConnection(str_conn)
                objConn.Open()
                Dim stTrans As SqlTransaction = objConn.BeginTransaction
                Try
                    DiscountBreakUpXML = ""
                    Dim retval As String = "0"
                    Dim STR_TYPE As Char = "S"
                    Dim FCO_FCO_ID As Int64 = 0
                    str_new_FCL_ID = 0
                    str_NEW_FCL_RECNO = ""
                    If retval = "0" Then
                        retval = FeeCollectionOnline.F_SaveFEECOLLECTION_H_ONLINE_MULTI(0, "Online", System.DateTime.Today.ToString(), _
                        Session("STU_ACD_ID"), Session("STU_ID"), "S", dblTotal.ToString(), False, str_new_FCL_ID, _
                        Session("sBsuid"), "Online Fee Collection", "CR", str_NEW_FCL_RECNO, _
                        Request.UserHostAddress.ToString, Session("CPS_ID"), FCO_FCO_ID, stTrans, DiscountBreakUpXML.ToString)
                        If retval = "0" Then
                            retval = FeecollectionTypeSetting(FCO_FCO_ID, stTrans, dRow(0)("APD_ID"))
                        End If
                        If retval = "0" Then
                            retval = FeeCollectionOnline.F_SaveFEECOLLSUB_ONLINE(0, str_new_FCL_ID, Convert.ToInt32(dRow(0)("ALD_FEETYPE_ID")), _
                                    GetDoubleVal(dblTotal), -1, dblTotal.ToString(), "0", stTrans)
                            If retval <> 0 Then
                            End If
                            If retval = "0" Then 'cash  here
                                retval = FeeCollectionOnline.F_SaveFEECOLLSUB_D_ONLINE(0, str_new_FCL_ID, COLLECTIONTYPE.CREDIT_CARD, _
                                dblTotal.ToString(), "", System.DateTime.Today.ToString(), 0, "", "", "", stTrans, 0)
                            End If
                            If retval = "0" Then
                                retval = FEECOLLECTION_H_ONLINE_Validation(str_new_FCL_ID, stTrans)
                            End If
                        End If
                        '[FEES].[SET_ACTIVITY_COLLECTIONTYPE]
                        
                    End If
                    If retval = "0" Then
                        stTrans.Commit()
                        Session("vpc_Amount") = CStr(dblTotal * 100).Split(".")(0)
                        vpc_OrderInfo = "RefNo-" & FCO_FCO_ID.ToString & "," & "Payment for " & StuNos
                        If vpc_OrderInfo.Length > 34 Then
                            vpc_OrderInfo = Left(vpc_OrderInfo, 34)
                        End If
                        Session("vpc_ReturnURL") = Request.Url.ToString.Replace("ActivityRequest.aspx", "ActivityPaymentResult.aspx").Replace("http://", IIf(Page.Request.Url.Host.Contains("localhost"), "http://", "https://"))
                        'Session("vpc_ReturnURL") = UtilityObj.StringReplace(Session("vpc_ReturnURL"), "http://", "https://") 
                        Session("vpc_MerchTxnRef") = FCO_FCO_ID
                        Session("activity_apd_id") = dRow(0)("APD_ID")
                        Session("vpc_OrderInfo") = vpc_OrderInfo
                        Dim flagAudit As Integer = UtilityObj.operOnAudiTable("Activity Online Payment", FCO_FCO_ID, "Insert", Page.User.Identity.Name.ToString, Me.Page, Server.MachineName)
                        Dim message1 = "Click on Confirm & Proceed to continue with this payment for " & IIf(Session("BSU_CURRENCY") Is Nothing, "", Session("BSU_CURRENCY")) & " " & Format(dblTotal.ToString(), Session("BSU_DataFormatString")) & ""
                        Dim message2 = "Please note reference no. <STRONG>" & FCO_FCO_ID & "</STRONG> of this transaction for any future communication."
                        Dim message3 = "Please do not close this browser or Log off until you get the final receipt."
                        If Request.Browser.Type.ToUpper.Contains("IE") Then
                            Me.divboxpanelconfirm.Attributes("class") = "darkPanelIETop"
                        Else
                            Me.divboxpanelconfirm.Attributes("class") = "darkPanelMTop"
                        End If
                        Me.testpopup.Style.Item("display") = "block"
                        lblMsg.Text = message1 & "<br />" & message2
                        lblMsg2.Text = message3

                        bPaymentConfirmMsgShown = True
                        boolPaymentInitiated = True
                    Else
                        stTrans.Rollback()
                        lblError.Text = UtilityObj.getErrorMessage(retval)
                    End If
                Catch ex As Exception
                    stTrans.Rollback()
                    lblError.Text = UtilityObj.getErrorMessage(1000)
                    UtilityObj.Errorlog(ex.Message)
                Finally
                    If objConn.State = ConnectionState.Open Then
                        objConn.Close()
                    End If
                End Try
            ElseIf (dRow(0)("ALD_FEE_REG_MODE") = "OC") Then
                '''''''''''''''''''''''''''
                Dim vpc_OrderInfo As String = String.Empty
                Dim boolPaymentInitiated As Boolean = False
                Dim dblTotal As Decimal
                If (dRow(0)("ALD_ALLOW_MULTIPLE") = True) Then
                    dblTotal = GetDoubleVal(dRow(0)("APD_TOTAL_AMOUNT"))
                Else
                    dblTotal = GetDoubleVal(dRow(0)("ALD_EVENT_AMT"))
                End If
                'Dim dtFrom As DateTime
                'dtFrom = CDate(DateTime.Today)
                Dim str_new_FCL_ID As Long
                Dim str_NEW_FCL_RECNO As String = ""
                Dim StuNos As String = "Activity (OC)"
                Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString
                Dim objConn As New SqlConnection(str_conn) '
                objConn.Open()
                Dim stTrans As SqlTransaction = objConn.BeginTransaction
                Try
                    DiscountBreakUpXML = ""
                    Dim retval As String = "0"
                    Dim STR_TYPE As Char = "S"
                    Dim OCO_OCO_ID As Int64 = 0
                    str_new_FCL_ID = 0
                    str_NEW_FCL_RECNO = ""

                    If retval = "0" Then
                        retval = FeeCollectionOnline.OTHER_SaveOTHERCOLLECTION_H_ONLINE_MULTI(0, dRow(0)("APD_ID"), "Online", System.DateTime.Today.ToString(), _
                        Session("STU_ACD_ID"), Session("STU_ID"), "S", dRow(0)("ALD_EVENT_AMT").ToString(), False, str_new_FCL_ID, _
                        Session("sBsuid"), "Online Fee Collection", "CR", str_NEW_FCL_RECNO, _
                        Request.UserHostAddress.ToString, Session("CPS_ID"), OCO_OCO_ID, stTrans, dRow(0)("ALD_ACT_ID").ToString, dRow(0)("ALD_OTH_EVT_ID").ToString, dRow(0)("ALD_OTH_TAX_CODE").ToString, DiscountBreakUpXML.ToString)

                        If retval = "0" Then

                            'retval = FeeCollectionOnline.F_SaveFEECOLLSUB_ONLINE(0, str_new_FCL_ID, Convert.ToInt32(hidFeetypeId.Value), _
                            '        GetDoubleVal(hidAmnt.Value), -1, hidAmnt.Value.ToString(), "0", stTrans)
                            'If retval <> 0 Then

                            'End If
                            If retval = "0" Then 'cash  here
                                retval = FeeCollectionOnline.F_SaveOTHERCOLLSUB_D_ONLINE(0, str_new_FCL_ID, COLLECTIONTYPE.CREDIT_CARD, _
                               dRow(0)("ALD_EVENT_AMT").ToString(), "", System.DateTime.Today.ToString(), 0, "", "", "", stTrans, 0)
                            End If
                            If retval = "0" Then
                                retval = OTHERCOLLECTION_H_ONLINE_Validation(str_new_FCL_ID, stTrans)
                            End If
                        End If
                    End If
                    If retval = "0" Then
                        stTrans.Commit()
                        Session("vpc_Amount") = CStr(dblTotal * 100).Split(".")(0)
                        vpc_OrderInfo = "RefNo-" & "OTH" + OCO_OCO_ID.ToString & "," & "Payment for " & StuNos
                        If vpc_OrderInfo.Length > 34 Then
                            vpc_OrderInfo = Left(vpc_OrderInfo, 34)
                        End If
                        'Session("OTH_ACT_ID") = dRow(0)("ALD_ACT_ID").ToString
                        'Session("OTH_EVT_ID") = dRow(0)("ALD_OTH_EVT_ID").ToString
                        'Session("OTH_TAX_CODE") = dRow(0)("ALD_OTH_TAX_CODE").ToString
                        Session("vpc_ReturnURL") = Request.Url.ToString.Replace("ActivityRequest.aspx", "ActivityPaymentResultOther.aspx").Replace("http://", IIf(Page.Request.Url.Host.Contains("localhost"), "http://", "https://"))
                        'Session("vpc_ReturnURL") = UtilityObj.StringReplace(Session("vpc_ReturnURL"), "http://", "https://") --------------------------------Remove this while giving to UAT
                        Session("vpc_MerchTxnRef") = "OTH" + Convert.ToString(OCO_OCO_ID)
                        Session("activity_apd_id") = dRow(0)("APD_ID")
                        Session("vpc_OrderInfo") = vpc_OrderInfo
                        Dim flagAudit As Integer = UtilityObj.operOnAudiTable("Activity Online Payment", OCO_OCO_ID, "Insert", Page.User.Identity.Name.ToString, Me.Page, Server.MachineName)
                        Dim message1 = "Click on Confirm & Proceed to continue with this payment for " & IIf(Session("BSU_CURRENCY") Is Nothing, "", Session("BSU_CURRENCY")) & " " & Format(dRow(0)("ALD_EVENT_AMT").ToString(), Session("BSU_DataFormatString")) & ""
                        Dim message2 = "Please note reference no. <STRONG>" & OCO_OCO_ID & "</STRONG> of this transaction for any future communication."
                        Dim message3 = "Please do not close this browser or Log off until you get the final receipt."
                        If Request.Browser.Type.ToUpper.Contains("IE") Then
                            Me.divboxpanelconfirm.Attributes("class") = "darkPanelIETop"
                        Else
                            Me.divboxpanelconfirm.Attributes("class") = "darkPanelMTop"
                        End If
                        Me.testpopup.Style.Item("display") = "block"
                        lblMsg.Text = message1 & "<br />" & message2
                        lblMsg2.Text = message3
                        bPaymentConfirmMsgShown = True
                        boolPaymentInitiated = True
                    Else
                        stTrans.Rollback()
                        lblError.Text = UtilityObj.getErrorMessage(retval)
                    End If
                Catch ex As Exception
                    stTrans.Rollback()
                    lblError.Text = UtilityObj.getErrorMessage(1000)
                    UtilityObj.Errorlog(ex.Message)
                Finally
                    If objConn.State = ConnectionState.Open Then
                        objConn.Close()
                    End If
                End Try
            End If
        End If

    End Sub
    Public Shared Function OTHERCOLLECTION_H_ONLINE_Validation(ByVal OCO_ID As Integer, ByVal p_stTrans As SqlTransaction) As String
        Dim pParms(2) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@OCO_ID", SqlDbType.Int)
        pParms(0).Value = OCO_ID
        pParms(1) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(1).Direction = ParameterDirection.ReturnValue
        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "FEES.OTHERCOLLECTION_H_ONLINE_Validation", pParms)
        OTHERCOLLECTION_H_ONLINE_Validation = pParms(1).Value
    End Function
    Public Shared Function FEECOLLECTION_H_ONLINE_Validation(ByVal FCO_ID As Integer, ByVal p_stTrans As SqlTransaction) As String
        Dim pParms(2) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@FCO_ID", SqlDbType.Int)
        pParms(0).Value = FCO_ID
        pParms(1) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(1).Direction = ParameterDirection.ReturnValue
        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "FEES.FEECOLLECTION_H_ONLINE_Validation", pParms)
        FEECOLLECTION_H_ONLINE_Validation = pParms(1).Value
    End Function
    Public Shared Function FeecollectionTypeSetting(ByVal FCO_ID As Integer, ByVal p_stTrans As SqlTransaction, ByVal APD_ID As Integer) As String
        Dim pParms(2) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@MERCH_TXN_REF", SqlDbType.Int)
        pParms(0).Value = FCO_ID
        pParms(1) = New SqlClient.SqlParameter("@ACT_APD_ID", SqlDbType.Int)
        pParms(1).Value = APD_ID
        pParms(2) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(2).Direction = ParameterDirection.ReturnValue
        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "[FEES].[SET_ACTIVITY_COLLECTIONTYPE]", pParms)
        FeecollectionTypeSetting = pParms(2).Value
    End Function
    Protected Sub btnSkip_Click(ByVal sender As Object, ByVal e As EventArgs)
        Me.testpopup.Style.Item("display") = "none"
        ' Response.Redirect(HttpContext.Current.Request.Url.ToString(), True)
        bPaymentConfirmMsgShown = False
    End Sub

    Protected Sub btnProceed_Click(ByVal sender As Object, ByVal e As EventArgs)
        '[OASIS].[ACTIVITY_PAYMENT_INITIATION]
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        ' Dim Dt_table As DataTable = DataResult
        ' Dim dRow As DataRow() = Dt_table.Select("ALD_ID = " & ViewID)

        Dim objConn As New SqlConnection(str_conn)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction

        Try
            Dim Param(1) As SqlParameter
            Param(0) = New SqlClient.SqlParameter("@APD_ID", Session("activity_apd_id"))
            Param(1) = New SqlClient.SqlParameter("@TYPE", "I")
            SqlHelper.ExecuteNonQuery(str_conn, "[OASIS].[ACTIVITY_PAYMENT_INITIATION]", Param)
            stTrans.Commit()
        Catch ex As Exception
            stTrans.Rollback()
        End Try

        Dim mertxnref As String = Session("vpc_MerchTxnRef")
        If mertxnref.Substring(0, 3) = "OTH" Then
            Response.Redirect("../Fees/feePaymentRedirectNew.aspx?SRC=" & Encr_decrData.Encrypt("ACTIVITY_OC"))
        Else
            Response.Redirect("../Fees/feePaymentRedirectNew.aspx?SRC=" & Encr_decrData.Encrypt("ACTIVITY_FC"))
        End If
    End Sub
    Public Function GetDoubleVal(ByVal Value As Object) As Double
        GetDoubleVal = 0
        Try
            If IsNumeric(Value) Then
                GetDoubleVal = Convert.ToDouble(Value)
            End If
        Catch ex As Exception
            GetDoubleVal = 0
        End Try
    End Function
    Sub ShowMessage(ByVal Message As String, ByVal bError As Boolean)
        If Message <> "" Then
            If bError Then
                lblError.CssClass = "alert alert-warning"
            Else
                lblError.CssClass = "alert alert-warning"
            End If
        Else
            lblError.CssClass = "alert alert-warning"
        End If
        lblError.Text = Message
    End Sub

    Protected Sub lnkbtnUnReg_Command(sender As Object, e As CommandEventArgs)
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim strans As SqlTransaction = Nothing
        Dim param(1) As SqlClient.SqlParameter
        Dim objConn As New SqlConnection(str_conn)
        objConn.Open()
        Try
            strans = objConn.BeginTransaction
            param(0) = New SqlClient.SqlParameter("@STU_ID", Session("STU_ID"))
            param(1) = New SqlClient.SqlParameter("@VIEWID", e.CommandArgument)
            SqlHelper.ExecuteNonQuery(strans, "[OASIS].[UNREGISSTER_ACTIVITY]", param)
            strans.Commit()
        Catch ex As Exception
            strans.Rollback()
            lblError.Text = UtilityObj.getErrorMessage(1000)
            UtilityObj.Errorlog(ex.Message)
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
        Response.Redirect(HttpContext.Current.Request.Url.ToString().Replace("http://", IIf(Page.Request.Url.Host.Contains("localhost"), "http://", "https://")), True)
    End Sub

    Protected Sub rdoSchdlDetails_SelectedIndexChanged(sender As Object, e As EventArgs)
       
        Dim ViewID As Integer = Convert.ToInt32(DirectCast(sender.parent.FindControl("lblViewId"), Label).Text.ToString())
        Dim Dt_table As DataTable = DataResult
        Dim dRow As DataRow() = Dt_table.Select("ALD_ID = " & ViewID)
        Dim rdoSchdlDetails As RadioButtonList = DirectCast(sender.parent.FindControl("rdoSchdlDetails"), RadioButtonList)
        Dim lnkbtnapply As LinkButton = DirectCast(sender.parent.FindControl("lnkbtnapply"), LinkButton)
        Dim chkTerm As CheckBox = DirectCast(sender.parent.FindControl("chkTerm"), CheckBox)
        Dim divTermMessage As HtmlGenericControl = DirectCast(sender.parent.FindControl("divTermMessage"), HtmlGenericControl)
        Dim labelName As Label = DirectCast(sender.parent.FindControl("lblEventName"), Label)

        lblEnrollMessage2.Text = rdoSchdlDetails.SelectedItem.Text.Split("<br/>")(0)
        B_DayNotallow = valiatesameday(ViewID, rdoSchdlDetails.SelectedItem.Text.Split("<br/>")(0), rdoSchdlDetails.SelectedItem.Value)
        If (B_DayNotallow = 1) Then
            lnkbtnapply.Attributes.Add("OnClick", "AlertMultiple(); return false;")
            lnkbtnapply.Enabled = False
            If dRow(0)("ALD_ISTERMS") Then
                chkTerm.Enabled = False
            End If

        Else
           
            If dRow(0)("ALD_ISTERMS") Then
                If chkTerm.Checked Then
                    divTermMessage.Style.Add("display", "none")
                    lnkbtnapply.Attributes.Add("OnClick", "ConfirmEnroll('" & lnkbtnapply.ClientID & "','" & labelName.Text & "','" & lblEnrollMessage2.Text & "'); return false;")
                Else
                    chkTerm.Enabled = True
                    lnkbtnapply.Enabled = True
                    lnkbtnapply.Attributes.Add("OnClick", "ConfirmTerms(" & divTermMessage.ClientID & "); return false;")
                End If
            Else
                divTermMessage.Style.Add("display", "none")
                lnkbtnapply.Attributes.Add("OnClick", "ConfirmEnroll('" & lnkbtnapply.ClientID & "','" & labelName.Text & "','" & lblEnrollMessage2.Text & "'); return false;")
            End If
           
        End If

    End Sub
    Protected Sub ddlcount_SelectedIndexChanged(sender As Object, e As EventArgs)

        Dim ViewID As Integer = Convert.ToInt32(DirectCast(sender.parent.FindControl("lblViewId"), Label).Text.ToString())

        'Get specific row from datatable 
        Dim Dt_table As DataTable = DataResult
        Dim dRow As DataRow() = Dt_table.Select("ALD_ID = " & ViewID)

        Dim ddlcount As DropDownList = DirectCast(sender.parent.findcontrol("ddlcount"), DropDownList)
        Dim lblamt As Label = DirectCast(sender.parent.findcontrol("lblamt"), Label)
        'Dim hidAmnt As HiddenField = DirectCast(sender.parent.findcontrol("hidAmnt"), HiddenField)
        Dim hidTotalAmt As HiddenField = DirectCast(sender.parent.findcontrol("hidTotalAmt"), HiddenField)
        If (ddlcount.SelectedValue <> 0) Then
            Dim count As Integer = Convert.ToInt16(ddlcount.SelectedValue)
            Dim total_amount_topay As Double = count * dRow(0)("ALD_EVENT_AMT")
            lblamt.Text = String.Format("{0:#.00}", total_amount_topay) + " " + Session("BSU_CURRENCY")
            hidTotalAmt.Value = String.Format("{0:#.00}", total_amount_topay)
        Else
            Dim amount As Double = dRow(0)("ALD_EVENT_AMT")
            lblamt.Text = String.Format("{0:#.00}", amount) + " " + Session("BSU_CURRENCY")
            hidTotalAmt.Value = String.Format("{0:#.00}", amount)
        End If
        'End If
    End Sub

    Protected Sub lnkbtnapply_Click(sender As Object, e As EventArgs)
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim param(8) As SqlClient.SqlParameter
        Dim ReturnVal As String = Nothing
        Dim Comments As String = ""
        Dim objConn As New SqlConnection(str_conn)
        objConn.Open()
        Dim strans As SqlTransaction = objConn.BeginTransaction
        Try
            Dim ViewID As Integer = Convert.ToInt32(DirectCast(sender.parent.FindControl("lblViewId"), Label).Text.ToString())
            'Get specific row from datatable 
            Dim Dt_table As DataTable = DataResult
            Dim dRow As DataRow() = Dt_table.Select("ALD_ID = " & ViewID)
            Dim divcount As HtmlGenericControl = DirectCast(sender.parent.findcontrol("divcount"), HtmlGenericControl)
            Dim ddlcount As DropDownList = DirectCast(sender.parent.findcontrol("ddlcount"), DropDownList)
            Dim hidTotalAmt As HiddenField = DirectCast(sender.parent.findcontrol("hidTotalAmt"), HiddenField)
            Dim rdoSchdlDetails As RadioButtonList = DirectCast(sender.parent.FindControl("rdoSchdlDetails"), RadioButtonList)
            Dim labelName As Label = DirectCast(sender.parent.FindControl("lblEventName"), Label)
            Dim hidAPG_ID As HiddenField = DirectCast(sender.parent.parent.parent.parent.FindControl("hidAPG_ID"), HiddenField)

            param(0) = New SqlClient.SqlParameter("@VIEWID", SqlDbType.BigInt)
            param(0).Value = ViewID
            param(1) = New SqlClient.SqlParameter("@STU_ID", SqlDbType.VarChar, 100)
            param(1).Value = Session("STU_ID")
            param(2) = New SqlClient.SqlParameter("@STU_NAME", SqlDbType.VarChar, 100)
            param(2).Value = Session("STU_NAME")
            param(3) = New SqlClient.SqlParameter("@STU_COUNT", SqlDbType.BigInt)
            If divcount.Visible = True Then
                param(3).Value = Convert.ToInt64(ddlcount.SelectedValue)
            Else
                param(3).Value = 1
            End If
            param(4) = New SqlClient.SqlParameter("@TOTAL_AMOUNT", SqlDbType.Decimal, 21)
            If Not hidTotalAmt.Value Is Nothing And Not hidTotalAmt.Value = "" Then
                param(4).Value = GetDoubleVal(hidTotalAmt.Value)
            Else
                param(4).Value = GetDoubleVal(dRow(0)("ALD_EVENT_AMT"))
            End If
            param(5) = New SqlClient.SqlParameter("@SCHEDULE", SqlDbType.VarChar, -1)
            param(6) = New SqlClient.SqlParameter("@SCH_ID", SqlDbType.BigInt)
            If dRow(0)("ALD_IS_SCHEDULE") Then
                If rdoSchdlDetails.Items.Count > 0 Then
                    param(5).Value = rdoSchdlDetails.SelectedItem.Text.Split("<br/>")(0).ToString
                    param(6).Value = rdoSchdlDetails.SelectedItem.Value
                End If
            Else
                param(5).Value = ""
                param(6).Value = Nothing
            End If
            param(7) = New SqlClient.SqlParameter("@GRP_ID", SqlDbType.BigInt)
            If hidAPG_ID.Value = 0 Or hidAPG_ID.Value = "0" Then
                param(7).Value = Nothing
            Else
                param(7).Value = hidAPG_ID.Value
            End If
            param(8) = New SqlClient.SqlParameter("@APD_ID", SqlDbType.VarChar, 50)
            param(8).Direction = ParameterDirection.Output

            SqlHelper.ExecuteNonQuery(strans, CommandType.StoredProcedure, "[OASIS].[UPDATE_ACTIVITY_DETAILS_TO_PARENTPORTAL]", param)
            If Not param(8).Value Is Nothing AndAlso param(8).Value.ToString.Trim <> "" Then
                strans.Commit()
                If ReturnVal = "No" Then
                    If dRow(0)("ALD_ISEMAIL") Then
                        SendNotificationEmail(dRow(0)("ALD_EMAIL_TEMPLATE"), dRow(0)("STS_FEMAIL"), labelName.Text)
                    End If
                End If
            Else
                strans.Rollback()
            End If
        Catch ex As Exception
            Message = UtilityObj.getErrorMessage("4000")
            ShowMessage(Message, True)
            UtilityObj.Errorlog("From btnSubmit_OnClick:" + ex.Message, "ACTIVITY SERVICES")
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
            If hidAutoStatus.Value <> "true" Then
                Response.Redirect(HttpContext.Current.Request.Url.ToString().Replace("http://", IIf(Page.Request.Url.Host.Contains("localhost"), "http://", "https://")), True)
            Else
                hidAutoStatus.Value = ""
            End If
        End Try
    End Sub

    Protected Sub SendNotificationEmail(ByVal emailtemplate As String, ByVal mailid As String, ByVal labelName As String)

        Dim tomail As String = String.Empty
        Dim sendAs As String = String.Empty
        Dim SendpWD As String = String.Empty
        Dim sendPort As String = String.Empty
        Dim FromEmail As String = String.Empty
        Dim SendHost As String = String.Empty
        Dim subject As String = String.Empty
        Dim mailbody As String = String.Empty
        Try
            tomail = mailid.ToString()
            If Not tomail Is Nothing Then
                If Mainclass.isEmail(tomail) Then
                    subject = "Mail Notification : Your Activity Request for *" + labelName + "* Has been APPROVED"
                    mailbody = emailtemplate.ToString()
                    'Inserting into table [COM_EMAIL_SCHEDULE] for sending the email 
                    InsertintoMailTable(subject, mailbody, tomail)
                Else
                    'Check on this
                    ' Dim ErrorMessage As String = "Error: Email Id Is Not Valid"
                    Message = UtilityObj.getErrorMessage("4005")
                    UtilityObj.Errorlog(Message, "ACTIVITY SERVICES")
                End If
            End If
        Catch EX As Exception
            'failure sending email                    
            UtilityObj.Errorlog("From SendNotificationEmail" + EX.Message, "ACTIVITY SERVICES")
        Finally

        End Try
    End Sub
    Protected Sub InsertintoMailTable(ByVal subject As String, ByVal message As String, ByVal tomailid As String)
        Try
            Dim str_conn = ConnectionManger.GetOASISConnectionString
            Dim query As String = "[dbo].[InsertIntoEmailSendSchedule] '" & Session("sBsuid") & "','ActivityRequestStatusMail','SYSTEM','" & tomailid & "','" & subject & "','" & message & "'"
            Mainclass.getDataValue(query, str_conn)
        Catch ex As Exception
            message = UtilityObj.getErrorMessage("4000")
            ShowMessage(message, True)
            UtilityObj.Errorlog("From InsertIntoMailTable: " + ex.Message, "ACTIVITY SERVICES")
        End Try
    End Sub

    Protected Sub btnConfirm_Click(sender As Object, e As EventArgs)
        For Each rptitem As RepeaterItem In rptrGroup.Items
            Dim Repeater1 As Repeater = DirectCast(rptitem.FindControl("Repeater1"), Repeater)
            For Each rptitem2 As RepeaterItem In Repeater1.Items
                Dim lnkbtnapply As LinkButton = DirectCast(rptitem2.FindControl("lnkbtnapply"), LinkButton)
                If (lnkbtnapply.ClientID = hidclientidbtn.Value) Then
                    lnkbtnapply_Click(lnkbtnapply, Nothing)
                    Exit Sub
                End If
            Next
        Next           
    End Sub

    Protected Sub chkTerm_CheckedChanged(sender As Object, e As EventArgs)
        Dim ViewID As Integer = Convert.ToInt32(DirectCast(sender.parent.FindControl("lblViewId"), Label).Text.ToString())

        'Get specific row from datatable 
        Dim Dt_table As DataTable = DataResult
        Dim dRow As DataRow() = Dt_table.Select("ALD_ID = " & ViewID)

        Dim chkTerm As CheckBox = sender
        Dim rdoSchdlDetails As RadioButtonList = DirectCast(sender.parent.FindControl("rdoSchdlDetails"), RadioButtonList)
        Dim lnkbtnapply As LinkButton = DirectCast(sender.parent.FindControl("lnkbtnapply"), LinkButton)
        Dim labelName As Label = DirectCast(sender.parent.FindControl("lblEventName"), Label)
        Dim divTermMessage As HtmlGenericControl = DirectCast(sender.parent.FindControl("divTermMessage"), HtmlGenericControl)

        If dRow(0)("ALD_IS_SCHEDULE") Then
            If (rdoSchdlDetails.Items.Count > 0) Then
                lblEnrollMessage2.Text = rdoSchdlDetails.SelectedItem.Text.Split("<br/>")(0)
            Else
                lblEnrollMessage2.Text = ""
            End If
        Else
            lblEnrollMessage2.Text = ""
        End If

        If chkTerm.Checked Then
            divTermMessage.Style.Add("display", "none")
            lnkbtnapply.Attributes.Add("OnClick", "ConfirmEnroll('" & lnkbtnapply.ClientID & "','" & labelName.Text & "','" & lblEnrollMessage2.Text & "'); return false;")
        Else
            lnkbtnapply.Attributes.Add("OnClick", "ConfirmTerms(" & divTermMessage.ClientID & "); return false;")
        End If
    End Sub
End Class