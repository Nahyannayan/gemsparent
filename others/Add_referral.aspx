<%@ Page Language="VB" MasterPageFile="~/ParentMaster.master" 
AutoEventWireup="false" CodeFile="Add_referral.aspx.vb"
 Inherits="Others_Add_referral" title="GEMS EDUCATION"  MaintainScrollPositionOnPostback="false"%>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="~/UserControl/urcRef_History.ascx" TagName="ref_History"
    TagPrefix="ucRef" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphParent" Runat="Server">
     <script  type="text/javascript" src="../Scripts/iframeResizer.min.js"></script>
    <script language="javascript" type="text/javascript">
//     function Refmsg(var1)
//    {
//    if(var1=='1')
//    {
//    document.getElementById('lblMsg').style.display='block';
//    document.getElementById('lblMsg').innerText='Your referral code will be SMS to the referred person.';
//     }
//     else{
//     document.getElementById('lblMsg').style.display='none';
//       document.getElementById('lblMsg').innerText='';
//     }
//    


//     } 
//    
//    function emailmsg(var1)
//    {
//    if(var1=='1')
//    {
//    document.getElementById('lblMsg').style.display='block';
//    document.getElementById('lblMsg').innerText='This email will be used to send your authorization !!';
//     }
//     else{
//     document.getElementById('lblMsg').style.display='none';
//       document.getElementById('lblMsg').innerText='';
//     }
//     }
//    
//     function isNumberKey(evt)
//      {
//         var charCode = (evt.which) ? evt.which : event.keyCode
//         if (charCode > 31 && (charCode < 48 || charCode > 57))
//         {
//            return false;
//}
//         return true;
//      }
    
    </script>
    <asp:HiddenField id="hdn_stuno" runat="server"/>
     <asp:HiddenField id="hdn_bsuid" runat="server"/>
    <div class="content margin-top30 margin-bottom60">
        <div class="container">
            <div class="row">

                <!-- Posts Block -->
                <div class="posts-block col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div id="divOasis_Reff" runat="server">
    <div class="mainheading">
      <div class="left">Refer to GEMS</div>
       
      </div>
   <div align="center" style="text-align:center;">
    
              
                 <div id="divNote" runat="server"   class="divinfoInner">
                  &nbsp;&nbsp;Select the school(s) you are referring this person to and click Add button.<br />&nbsp;&nbsp;You can refer more than one person at a time�just provide the next person�s details,select a school(s) and Add button again.<br />&nbsp;&nbsp;When you are done referring, please click Submit button.<br />&nbsp;&nbsp;Click on View Referrals to view all your referral status. 

                  </div>
              
             
                               <table  class="tableNoborder" width="100%"   cellpadding="8" cellspacing="0" >
        <tr>
                <td  class="tdfields"  colspan="3"  >Please enter the details of the parent you are referring.
                </td>
                <td align="right"><asp:Button ID="lbtnViewRef" runat="server" Text="View Referrals" CssClass="buttons"  Width="120px"
                 ToolTip="Click here to view all your referral status.">
                </asp:Button>
                 <telerik:RadToolTip ID="rttAccpage" runat="server" EnableShadow="true" RelativeTo="Element"
        Height="50px" Width="100px" TargetControlID="lbtnViewRef"  Position="BottomCenter" 
                  VisibleOnPageLoad="true"  AutoCloseDelay="40000" 
          Skin="Sunset"    >    </telerik:RadToolTip> 
                
                </td></tr>
         <tr>
                <td  class="tdfields" width="180px"   >
                 Full Name<font  color="red"  size="1px">*</font></td>
              
                <td  align="left" colspan="3">
                    <asp:DropDownList ID="ddlSlute" runat="server" Width="44px" CssClass="dropDownList">
                        <asp:ListItem>Mr</asp:ListItem>
                        <asp:ListItem>Mrs</asp:ListItem>
                        <asp:ListItem>Ms</asp:ListItem>
                        <asp:ListItem>Dr</asp:ListItem>
                    </asp:DropDownList> &nbsp;
                 <asp:TextBox ID="txtFname" runat="server" Width="150px"  CssClass="input" MaxLength="100"></asp:TextBox>   <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" 
                        ControlToValidate="txtFName" Display="Dynamic" EnableViewState="False"
                          ErrorMessage="First name required"
                      
                        ValidationGroup="info">*</asp:RequiredFieldValidator>&nbsp;&nbsp;
                    <asp:TextBox ID="txtLName" runat="server" Width="150px"  CssClass="input" MaxLength="100"></asp:TextBox>
                    <br />
                       <span class="remark">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;First Name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Last Name</span></td>
            </tr>
            <tr>
                <td class="tdfields" >
                  Email<font color="red"  size="1px">*</font></td>              
                <td  align="left" colspan="3">
                    <asp:TextBox ID="txtEmail" runat="server" Width="200px"  CssClass="input" ToolTip="Entered email address will be used to send your authorization !!"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                        ControlToValidate="txtEmail" Display="Dynamic" EnableViewState="False" 
                        Font-Italic="True"  ForeColor="red" ErrorMessage="Email address required"
                        ValidationGroup="info">*</asp:RequiredFieldValidator>
                          <asp:RegularExpressionValidator ID="revMPri_Email" 
                        runat="server" 
                                                                                                    
                        ControlToValidate="txtEmail" Display="Dynamic" 
                        EnableViewState="False" 
                                                                                                    
                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" 
                                                                                                    
                        ValidationGroup="info" ErrorMessage="Invalid Email"
                         ForeColor="red">*</asp:RegularExpressionValidator>
                         <telerik:RadToolTip ID="RadToolTip5" runat="server" EnableShadow="true" RelativeTo="Element"
        Height="30px" Width="350px" TargetControlID="txtEmail"  Position="Middleright" 
          BackColor="#feffb3" BorderStyle="Solid"  BorderWidth="1px"  ForeColor="Red"
           ShowEvent="OnFocus" CssClass="divinfo"  AutoCloseDelay="40000" Skin="Sunset">
        
    </telerik:RadToolTip>

                   
                </td>
            </tr>
            <tr>
                <td class="tdfields" >
                    Confirm Email<font color="red"  size="1px">*</font></td>
             
                <td  align="left" colspan="3">
                    <asp:TextBox ID="txtEmailConf" runat="server" Width="200px" CssClass="input" ></asp:TextBox> 
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" 
                        ControlToValidate="txtEmailConf" Display="Dynamic" EnableViewState="False"  ErrorMessage="Confirm Email Required"
                        Font-Italic="True"  ForeColor="red" 
                        ValidationGroup="info">*</asp:RequiredFieldValidator>
                          <asp:RegularExpressionValidator ID="RegularExpressionValidator1" 
                        runat="server" ControlToValidate="txtEmailConf" Display="Dynamic" 
                        EnableViewState="False" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" 
                         ValidationGroup="info"   ErrorMessage="Invalid Email"
                         ForeColor="red">*</asp:RegularExpressionValidator>
                    <asp:CompareValidator ID="CompareValidator1" runat="server" 
                        ControlToCompare="txtEmail" ControlToValidate="txtEmailConf" Display="Dynamic" 
                        EnableViewState="False" Font-Italic="True" Font-Names="Verdana" 
                        Font-Size="10px" ForeColor="red" ValidationGroup="info" ErrorMessage="Emails must match!">*</asp:CompareValidator>
                </td>
            </tr>
            <tr>
                <td  class="tdfields"  >
                  Mobile No<font color="red" size="1px">*</font></td>
               
                <td  colspan="3"><asp:TextBox ID="txtContact_Country" runat="server" Width="40px"   CssClass="input" MaxLength="3" ></asp:TextBox> <asp:RequiredFieldValidator ID="RequiredFieldValidator7" 
                runat="server" ControlToValidate="txtContact_Country" Display="Dynamic"  ErrorMessage="Country Code Required"  
                         ForeColor="red" ValidationGroup="info"  EnableViewState="False">*</asp:RequiredFieldValidator>
                                                                                                -
                                                                                                <asp:TextBox ID="txtContact_Area" runat="server" Width="40px" CssClass="input" MaxLength="4" ></asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" 
                        ControlToValidate="txtContact_Area" Display="Dynamic" EnableViewState="False" 
                        ErrorMessage="Area code Required"  ForeColor="red" ValidationGroup="info">*</asp:RequiredFieldValidator>
                                                                                                -
                                                                                                <asp:TextBox ID="txtContact_No" runat="server" Width="82px" CssClass="input" MaxLength="10" ToolTip="Your referral code will be SMS to the referred person."></asp:TextBox>
                                                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server"  ControlToValidate="txtContact_No" 
                                                                                                Display="Dynamic" EnableViewState="False"   ErrorMessage="Mobile No.Required"   ForeColor="red" ValidationGroup="info">*</asp:RequiredFieldValidator>
                  <br />&nbsp;<span class="remark">(Country-Area-Number)</span>
                      <telerik:RadToolTip ID="RadToolTip1" runat="server" EnableShadow="true" RelativeTo="Element"
        Height="30px" Width="300px" TargetControlID="txtContact_No"  Position="Middleright" 
          BackColor="#feffb3" BorderStyle="Solid"  BorderWidth="1px"  ForeColor="Red" BorderColor="Red"
           ShowEvent="OnFocus" CssClass="divinfo"  AutoCloseDelay="40000" 
          Skin="Sunset"    >    </telerik:RadToolTip> 
          <ajaxToolkit:FilteredTextBoxExtender ID="ftbxCty" runat="server" FilterType="Numbers"  FilterMode="ValidChars"
           TargetControlID="txtContact_Country"    >
           </ajaxToolkit:FilteredTextBoxExtender>                                                               
                 <ajaxToolkit:FilteredTextBoxExtender ID="ftxArea" runat="server" FilterType="Numbers"  FilterMode="ValidChars"
           TargetControlID="txtContact_Area"    >
           </ajaxToolkit:FilteredTextBoxExtender> 
            <ajaxToolkit:FilteredTextBoxExtender ID="ftxNo" runat="server" FilterType="Numbers"  FilterMode="ValidChars"
           TargetControlID="txtContact_No"    >
           </ajaxToolkit:FilteredTextBoxExtender>                                             
                </td>
            </tr>
              <tr>
               <td  class="tdfields"   >
                  Prospective Student Name  </td>
              
                <td  align="left" colspan="3"><asp:TextBox ID="txtAppName" runat="server" Width="200px" CssClass="input" MaxLength="300"></asp:TextBox><asp:RegularExpressionValidator
                            ID="revFirstname" runat="server"  Display="Dynamic" 
                        EnableViewState="false" ValidationGroup="info"  ControlToValidate="txtAppName" 
                        ErrorMessage="Invalid applicant name" ValidationExpression="^[a-zA-Z'.\s-\\\/]{1,100}$" 
                          ForeColor="red">*</asp:RegularExpressionValidator>
                </td>
              </tr>       
            <tr>
                <td  class="tdfields"  >
                    School(s)<font color="red" size="1px">*</font></td>
                    
                    <td  align="left" colspan="3"> 
                    
                            <asp:Panel ID="Panel1" runat="server" Width="390px" ScrollBars="Vertical"  
                                BorderStyle="Solid"  BorderColor="#d0cdcd" BorderWidth="1px"  Height="105px">
                                <asp:CheckBoxList ID="CHKbsu_ID" runat="server">
                                </asp:CheckBoxList>
                                                      </asp:Panel>
                                                      </td>
 
            </tr>
                <tr>
                <td align="center" colspan="4">
<asp:Button ID="btnAddM" runat="server" CssClass="button" Text="Add"  
ValidationGroup="info" Width="90px"/>&nbsp;&nbsp;&nbsp;
                                </td>
                </tr>  
           <tr class="trHeader" id="trrefHead" runat="server"><td colspan="4">&nbsp;Referral's</td>
                       </tr>    
            <tr class="tdblankAll">
                <td   align="center" colspan="4">
                <div style="width:770px;text-align:center;overflow:auto; height: 120px;">
        <asp:GridView ID="gvRef" runat="server" AutoGenerateColumns="False" 
                    DataKeyNames="ID" EmptyDataText="No record added yet" 
                        EnableModelValidation="True"  
                    Width="100%" CssClass="gridView" BorderStyle="None" BorderWidth="0px" 
             HeaderStyle-HorizontalAlign="Center">
                   <EmptyDataRowStyle  Wrap="True" HorizontalAlign="Center" />
                    <columns>
                        <asp:TemplateField HeaderText="Id" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lblId" runat="server" 
                                    Text='<%# Bind("id") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="RFS_RFM_ID" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lblRFS_RFM_ID" runat="server" 
                                Text='<%# Bind("RFS_RFM_ID") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                               
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Name">
                            <ItemTemplate>
                                <asp:Label ID="lblFULLNAME" runat="server" 
                                    Text='<%# Bind("FULLNAME") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Width="85px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Email">
                            <ItemTemplate>
                                <asp:Label ID="lblRFS_REF_EMAIL" runat="server" 
                                    Text='<%# Bind("RFS_REF_EMAIL") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Width="90px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Mob No">
                            <ItemTemplate>
                                <asp:Label ID="lblRFS_REF_MOB_NO" runat="server" Text='<%# Bind("RFS_REF_MOB_NO") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="School(s)">
                            <ItemTemplate>
                                <asp:Label ID="lblBSU_NAME" runat="server" Text='<%# BIND("BSU_NAME") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Width="180px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Delete">
                            <ItemTemplate>
                                <asp:Button ID="DeleteBtn" runat="server" 
                                    CommandArgument='<%# Eval("id") %>' CommandName="Delete" CssClass="buttons" Text="Delete" Width="60px"/>
                            </ItemTemplate>
                            <HeaderStyle Width="40px" />
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="5%" />
                        </asp:TemplateField>
                    </columns>
                   
                </asp:GridView>
            <asp:Label ID="lblError_grid" runat="server" CssClass="error" ></asp:Label>
            </div>
        
                </td>
            </tr>
              
             <tr id="trReward" runat="server">
             <td  class="tdfields"> Select the reward type<font color="red" size="1px">*</font></td>
                <td 
                    colspan="3" align="left">
                    
                    
                 
                     <asp:DropDownList ID="ddlREWARD" runat="server" CssClass="dropDownList" Width="340px" ToolTip="Choose your reward type">
        <asp:ListItem Value="FEES">Receive the 4% as a discount on my child�s tuition 
        fees</asp:ListItem>
        <asp:ListItem Value="CASH">Receive the 4% in cash</asp:ListItem>
        <asp:ListItem Value="DONATE">Donate equivalent to the Varkey GEMS Foundation</asp:ListItem>
    </asp:DropDownList> <telerik:RadToolTip ID="RadToolTip2" runat="server" EnableShadow="true" RelativeTo="Element"
        Height="30px" Width="150px" TargetControlID="ddlREWARD"  Position="TopCenter" VisibleOnPageLoad="true"
          BackColor="#feffb3" BorderStyle="Solid"  BorderWidth="1px"  ForeColor="Red" BorderColor="Red"
          AutoCloseDelay="40000" ShowEvent="OnFocus"  
          Skin="Sunset"    >    </telerik:RadToolTip> 
                        
                      
                </td>
            </tr>  
            
              
             <tr>
                <td 
                    colspan="4" align="center">
                    
                    
                 
                     <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Submit" 
                        Width="90px" CausesValidation="False" />
                        
                      
                </td>
            </tr>  
             <tr>
                <td 
                    align="center" colspan="4">
                     <asp:ValidationSummary ID="vsRef" runat="server" 
         ValidationGroup="info"  HeaderText="<div class='validationheader'>Please correct the following:</div>"
                                 CssClass="divinfoInner" ForeColor=""/>  
          
          <asp:Label ID="lbmsgInfo" runat="server"></asp:Label>
          
          </td></tr>     
                                   
    
         </table>
              <div id="pldis"  runat="server" style="display: none; overflow: visible;background-color:White; border-color: #b5cae7; 
          border-style:solid;border-width:4px;width:900px;">
            <div  style="width:901px;margin-top:0px;vertical-align:middle;background-color:White;">   
                       <span style="clear: right;display: inline; float: right; visibility: visible;margin-top:0px; vertical-align:top; ">
               <asp:ImageButton ID="btnCloseedit" runat="server" ImageUrl="~/Images/Common/PageBody/close.png" 
               /></span> 
           </div>
           <div class="mainheading" style="margin-top:15px;width:888px;">
      <div class="left"><asp:Label ID="lblPopHead" runat="server" Text="Referral History" Font-Size="11px" Font-Bold="true"></asp:Label></div>
   
      </div>
                <asp:Panel id="plStudAtt" runat="server" Width="901px" height="450px" 
                 BackColor="White" BorderColor="Transparent" BorderStyle="none"  ScrollBars="none">
                <ucRef:ref_History id="refHistory" runat="server">
        </ucRef:ref_History>

    </asp:Panel>
                </div>
               
                <asp:Button id="btnShowPopup" runat="server" style="display:none" />
            <ajaxToolkit:ModalPopupExtender 
             ID="mdlPopup" runat="server" TargetControlID="btnShowPopup" PopupControlID="pldis"
               BackgroundCssClass="modalBackground" DropShadow="false" RepositionMode="RepositionOnWindowResizeAndScroll"  
                  ></ajaxToolkit:ModalPopupExtender>
     </div>
    </div>

    <div id="divSF_reff" runat="server" visible="false">
         <iframe  id="ifReff" runat="server"  width="100%" frameborder="0" >
 
</iframe>
        </div>
 </div>
                <!-- /Posts Block -->
            </div>
        </div>
   </div>
    <script type="text/javascript">
        document.getElementById("<%=ifReff.ClientID%>").onload = resizeFrame;
        function resizeFrame() {
            iFrameResize({ log: false }, document.getElementById("<%=ifReff.ClientID%>"));
        }
        </script> 
</asp:Content>

