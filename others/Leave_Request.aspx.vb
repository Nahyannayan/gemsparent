﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Net.Mail
Imports System.Text
Partial Class Others_Leave_Request
    Inherits System.Web.UI.Page
    Private FLAG As Integer = 0

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If Session("username") Is Nothing Then
                Session("Active_tab") = "Home"
                Session("Site_Path") = ""
                Response.Redirect("~\login.aspx")
            ElseIf Session("bPasswdChanged") = "False" Then
                Response.Redirect("~\UpdateInfo\ForcePasswordChange.aspx", False)
            ElseIf Session("bUpdateContactDetails") = "False" Then
                Response.Redirect(Session("ForceUpdate_stud"), False)
            End If
            lbChildName.Text = Session("STU_NAME")
            lblPopchildName.Text = Session("STU_NAME")

          

            'ViewState("viewid") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
            If Page.IsPostBack = False Then
                rbLNote.Checked = True
                GetAbsent_Param()

                If rbLNote.Checked = True Then
                    hfvalidFrmDate.Value = "1"
                    btnAppLev_Req.Visible = False
                    gvLeaveReq.Visible = False
                    gvLeaveNote.Visible = True
                    ddlParam.Visible = False
                    lblAttStatus.Visible = True
                    rfvAttParam.Enabled = False
                    bindGrid()
                    lblPopHead.Text = " Leave Note"
                Else
                    lblPopHead.Text = " Leave Request"
                    rfvAttParam.Enabled = True
                    ddlParam.Visible = True
                    lblAttStatus.Visible = False
                    hfvalidFrmDate.Value = "0"
                    btnAppLev_Req.Visible = True
                    gvLeaveReq.Visible = True
                    gvLeaveNote.Visible = False
                    bindGrid_Req()

                End If
            End If

            ''by nahyan to disable apply leave if not en

            'Dim StuStatus = GetStudent_Status()
            'If StuStatus <> "EN" Then
            '    btnAppLev_Req.Visible = False
            'Else
            '    btnAppLev_Req.Visible = True
            'End If


        Catch ex As Exception

        End Try
    End Sub
    Private Sub bindGrid_Req()
        Try


            Dim dsLeaveReq As DataSet
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim param(10) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@ACD_ID", Session("STU_ACD_ID"))
            param(1) = New SqlClient.SqlParameter("@SCT_ID", Session("STU_SCT_ID"))
            param(2) = New SqlClient.SqlParameter("@GRD_ID", Session("STU_GRD_ID"))
            param(3) = New SqlClient.SqlParameter("@STU_ID", Session("STU_ID"))
            param(4) = New SqlClient.SqlParameter("@TYPE", "LR")
            If ddlApp_Type.SelectedValue = "0" Then
                param(5) = New SqlClient.SqlParameter("@LEAVE_APP", Nothing)
            ElseIf ddlApp_Type.SelectedValue = "NA" Then
                param(5) = New SqlClient.SqlParameter("@LEAVE_APP", "")
            Else
                param(5) = New SqlClient.SqlParameter("@LEAVE_APP", ddlApp_Type.SelectedValue)
            End If

            dsLeaveReq = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "OPL.GETOLINE_STUDENT_LEAVE_APPROVAL", param)

            'h_SliblingID



            divEntry.Visible = False
            'Call the clear m
            If dsLeaveReq.Tables(0).Rows.Count = 0 Then

                dsLeaveReq.Tables(0).Rows.Add(dsLeaveReq.Tables(0).NewRow())
                dsLeaveReq.Tables(0).Rows(0)(9) = False
                dsLeaveReq.Tables(0).Rows(0)(10) = False
                gvLeaveReq.Columns(6).Visible = False
                gvLeaveReq.DataSource = dsLeaveReq.Tables(0)
                Try
                    gvLeaveReq.DataBind()
                Catch ex As Exception
                End Try

                Dim columnCount As Integer = gvLeaveReq.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvLeaveReq.Rows(0).Cells.Clear()
                gvLeaveReq.Rows(0).Cells.Add(New TableCell)
                gvLeaveReq.Rows(0).Cells(0).ColumnSpan = columnCount
                gvLeaveReq.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvLeaveReq.Rows(0).Cells(0).Text = "No record available."
            Else
                gvLeaveReq.Columns(6).Visible = True
                gvLeaveReq.DataSource = dsLeaveReq.Tables(0)
                gvLeaveReq.DataBind()

            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub bindGrid()
        Try


            Dim dsLeave As DataSet
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim param(10) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@ACD_ID", Session("STU_ACD_ID"))
            param(1) = New SqlClient.SqlParameter("@STU_SCT_ID", Session("STU_SCT_ID"))
            param(2) = New SqlClient.SqlParameter("@STU_GRD_ID", Session("STU_GRD_ID"))
            param(3) = New SqlClient.SqlParameter("@STU_ID", Session("STU_ID"))
            If ddlApp_Type.SelectedValue = "0" Then
                param(4) = New SqlClient.SqlParameter("@LEAVE_APP", Nothing)
            ElseIf ddlApp_Type.SelectedValue = "NA" Then
                param(4) = New SqlClient.SqlParameter("@LEAVE_APP", "")
            Else
                param(4) = New SqlClient.SqlParameter("@LEAVE_APP", ddlApp_Type.SelectedValue)
            End If

            dsLeave = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "OPL.GETSTUATT_LEAVE_STATUS", param)

            'h_SliblingID




            'Call the clear m
            If dsLeave.Tables(0).Rows.Count = 0 Then
                divEntry.Visible = False
                dsLeave.Tables(0).Rows.Add(dsLeave.Tables(0).NewRow())
                dsLeave.Tables(0).Rows(0)(11) = True
                dsLeave.Tables(0).Rows(0)(12) = True
                dsLeave.Tables(0).Rows(0)(13) = True
                gvLeaveNote.DataSource = dsLeave.Tables(0)
                Try
                    gvLeaveNote.DataBind()
                Catch ex As Exception
                End Try

                Dim columnCount As Integer = gvLeaveNote.Rows(0).Cells.Count
                'Call the clear method to clear out any controls that you use in the columns.  I use a dropdown list in one of the column so this was necessary.
                gvLeaveNote.Rows(0).Cells.Clear()
                gvLeaveNote.Rows(0).Cells.Add(New TableCell)
                gvLeaveNote.Rows(0).Cells(0).ColumnSpan = columnCount
                gvLeaveNote.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
                gvLeaveNote.Rows(0).Cells(0).Text = "No record available."
            Else
                divEntry.Visible = True
                gvLeaveNote.DataSource = dsLeave.Tables(0)
                gvLeaveNote.DataBind()

            End If
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub gvLeaveReq_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvLeaveReq.PageIndexChanging
        gvLeaveReq.PageIndex = e.NewPageIndex
        bindGrid_Req()
    End Sub
    Protected Sub gvLeave_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvLeaveNote.PageIndexChanging

        gvLeaveNote.PageIndex = e.NewPageIndex
        bindGrid()

    End Sub

    Protected Sub rbLNote_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbLNote.CheckedChanged
        ' Dim lblmsg As Label = CType(Page.Master.FindControl("lblSiteMessage"), Label)
        lblmsg.CssClass = ""
        lblmsg.Text = ""

        lblPopHead.Text = " Leave Note"
        ddlParam.Visible = False
        lblAttStatus.Visible = True
        hfvalidFrmDate.Value = "1"
        btnAppLev_Req.Visible = False
        gvLeaveReq.Visible = False
        gvLeaveNote.Visible = True
        rfvAttParam.Enabled = False
        bindGrid()
    End Sub

    Protected Sub rbLRequest_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbLRequest.CheckedChanged
        'Dim lblmsg As Label = CType(Page.Master.FindControl("lblSiteMessage"), Label)
        lblmsg.CssClass = ""
        lblmsg.Text = ""
        lblPopHead.Text = " Leave Request"
        rfvAttParam.Enabled = True
        ddlParam.Visible = True
        lblAttStatus.Visible = False
        hfvalidFrmDate.Value = "0"
        btnAppLev_Req.Visible = True
        gvLeaveReq.Visible = True
        gvLeaveNote.Visible = False
        bindGrid_Req()

        ''by nahyan to disable apply leave if not en

        Dim StuStatus = GetStudent_Status()
        If StuStatus <> "EN" Then
            btnAppLev_Req.Visible = False
        End If
    End Sub

    Protected Sub ddlApp_Type_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlApp_Type.SelectedIndexChanged

        If rbLNote.Checked = True Then
            bindGrid()
        Else
            bindGrid_Req()
        End If

    End Sub

    Protected Sub btnLApply_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim lbldate As New Label
        Dim lblparam As New Label

        lbldate = TryCast(sender.FindControl("lbldate"), Label)
        lblAttStatus.Text = TryCast(sender.FindControl("lblAtt_param"), Label).Text
        ViewState("APD_ID") = TryCast(sender.FindControl("lblAPD_ID"), Label).Text
        txtFromDt.Text = lbldate.Text
        txtFromDt.Enabled = False
        imgFromDt.Visible = False
        txtToDt.Enabled = True
        imgToDt.Visible = True
        lblPopError.CssClass = ""
        lblPopError.Text = ""
        GetExtended_Date(lbldate.Text)
        lblPopHead.Text = " Leave Note"
        btnSaveAbs.Visible = True
       
        mdlPopup.Show()
    End Sub
    Protected Sub btnAppLev_Req_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAppLev_Req.Click
        Dim dtFrom As Date


        txtFromDt.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
        txtToDt.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
        dtFrom = txtFromDt.Text
        hfShowDate.Value = txtFromDt.Text
        hfDate.Value = dtFrom.ToString("G")


        txtremark.Text = ""
        txtFromDt.Enabled = True
        imgFromDt.Visible = True
        txtToDt.Enabled = True
        imgToDt.Visible = True
        lblPopHead.Text = " Leave Request"
        ddlParam.ClearSelection()
        ddlParam.Items.FindByValue("-1").Selected = True
        ViewState("bEdit") = 0
        btnSaveAbs.Visible = True
        lblPopError.CssClass = ""
        lblPopError.Text = ""
        mdlPopup.Show()
    End Sub
    Protected Sub btnLREdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lblFROMDT As New Label
        Dim lblTODT As New Label
        Dim lblREMARKS As New Label
        Dim lblAtt_param As New Label
        ViewState("SLA_ID") = TryCast(sender.FindControl("lblSLA_ID"), Label).Text
        lblFROMDT = TryCast(sender.FindControl("lblFROMDT"), Label)
        lblTODT = TryCast(sender.FindControl("lblTODT"), Label)
        lblREMARKS = TryCast(sender.FindControl("lblREMARKS"), Label)
        lblAtt_param = TryCast(sender.FindControl("lblAtt_param"), Label)

        If Not ddlParam.Items.FindByText(lblAtt_param.Text.Trim) Is Nothing Then

            ddlParam.ClearSelection()
            ddlParam.Items.FindByText(lblAtt_param.Text.Trim).Selected = True
        End If

        txtFromDt.Text = lblFROMDT.Text
        txtToDt.Text = lblTODT.Text
        txtremark.Text = lblREMARKS.Text
        txtFromDt.Enabled = False
        imgFromDt.Visible = False
        txtToDt.Enabled = False
        imgToDt.Visible = False
        ViewState("bEdit") = 1
        btnSaveAbs.Visible = True
        lblPopError.CssClass = ""
        lblPopError.Text = ""
        mdlPopup.Show()
    End Sub
    Private Sub GetAbsent_Param()
       
        Dim conn As String = ConnectionManger.GetOASISConnectionString
        Dim param(2) As SqlParameter
        param(0) = New SqlClient.SqlParameter("@ACD_ID", Session("STU_ACD_ID"))
        ddlParam.Items.Clear()
        Using DATAREADER As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "OPL.GETABSENT_PARAM", param)
            While DATAREADER.Read
                ddlParam.Items.Add(New ListItem(Convert.ToString(DATAREADER("APD_PARAM_DESCR")), Convert.ToString(DATAREADER("APD_ID"))))
            End While
        End Using
        ddlParam.Items.Add(New ListItem("SELECT", "-1"))
        ddlParam.ClearSelection()
        ddlParam.Items.FindByValue("-1").Selected = True


    End Sub

    Private Function GetStudent_Status() As String

        Dim conn As String = ConnectionManger.GetOASISConnectionString
        Dim param(2) As SqlParameter
        Dim stu_STATUS As String = String.Empty
        param(0) = New SqlClient.SqlParameter("@STU_ID", Session("STU_ID"))

        Using DATAREADER As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "opl.get_Student_status", param)
            While DATAREADER.Read
                stu_STATUS = Convert.ToString(DATAREADER("STU_CURRSTATUS"))
            End While
        End Using


        Return stu_STATUS
    End Function
    Private Sub GetExtended_Date(ByVal FROM_DT As String)
        Try

            ViewState("TODATE") = ""
            Dim dtTO As Date
            Dim dtFrom As Date
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString

            Dim param(10) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@ACD_ID", Session("STU_ACD_ID"))
            param(1) = New SqlClient.SqlParameter("@STU_SCT_ID", Session("STU_SCT_ID"))
            param(2) = New SqlClient.SqlParameter("@STU_GRD_ID", Session("STU_GRD_ID"))
            param(3) = New SqlClient.SqlParameter("@STU_ID", Session("STU_ID"))
            param(4) = New SqlClient.SqlParameter("@FROM_DT", FROM_DT)


            Using datareader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "[OPL].[GETSTUATT_LEAVE_NEXT_DATE]", param)
                If datareader.HasRows = True Then
                    While datareader.Read
                        ViewState("TODATE") = Convert.ToString(datareader("TODATE"))
                    End While

                End If
            End Using
            If (ViewState("TODATE") = FROM_DT) Or (ViewState("TODATE") = "") Then
                dtTO = FROM_DT
                hfShowDate.Value = FROM_DT
                hfShowfromDate.Value = FROM_DT
                hfDate.Value = dtTO.ToString("G")
                dtFrom = FROM_DT
                hffromDate.Value = dtFrom.ToString("G")
                lblPopError.CssClass = ""
                lblPopError.Text = ""
                txtToDt.Text = FROM_DT
                ViewState("TODATE") = FROM_DT
            ElseIf ViewState("TODATE") <> FROM_DT Then
                dtTO = ViewState("TODATE")
                hfShowDate.Value = ViewState("TODATE")
                hfDate.Value = dtTO.ToString("G")
                dtFrom = FROM_DT
                hfShowfromDate.Value = FROM_DT
                hffromDate.Value = dtFrom.ToString("G")
                txtToDt.Text = ViewState("TODATE")
                ' lblPopError.CssClass = "divinfo"
                lblPopError.CssClass = "divinfoPopUp"
                lblPopError.Text = "System has choosen the extended date !!! you can either continue with the extended date or you can update To Date with From Date "
            End If

            'h_SliblingID
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnSaveAbs_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveAbs.Click
        Dim commstr As String = String.Empty
        Dim dtFrom As Date
        Dim dtTo As Date
        Dim txtFDT As Date
        Dim txtTDT As Date
        Dim str_err As String = String.Empty
        Dim errorMessage As String = String.Empty
        Dim bedit As Boolean = False
        Dim OUT_SLA_ID As String = String.Empty


        mdlPopup.Show()

        If ValidateDate(commstr) = "-1" Then
            lblPopError.CssClass = "divvalidPopUp"
            lblPopError.Text = commstr
        Else
            ''added by nahyan on 15MAy2016 

            Dim leave_days_bsu As Integer = 0

            leave_days_bsu = getBsuLeaveDaysParameter()

            Dim Fromdate As Date
            Dim Todate As Date
            Dim totDays As Integer = 0
            Fromdate = txtFromDt.Text
            Todate = txtToDt.Text
            totDays = (Todate - Fromdate).Days

            If rbLNote.Checked = True Then
                txtFDT = txtFromDt.Text
                txtTDT = txtToDt.Text
                dtFrom = txtFromDt.Text
                dtTo = ViewState("TODATE")
                If DateTime.Compare(txtTDT, dtTo) > 0 Then
                    lblPopError.CssClass = "divinfoPopUp"
                    lblPopError.Text = " To Date entered is not a valid date and must be with in the system given extended date or equal to " & ViewState("TODATE")
                    txtToDt.Text = ViewState("TODATE")
                    'ElseIf leave_days_bsu > 0 AndAlso totDays > leave_days_bsu Then


                    '    lblPopError.CssClass = "divinfoPopUp"
                    '    lblPopError.Text = " Please contact the school to apply for leave more than " & leave_days_bsu & " days"

                Else
                    lblPopError.CssClass = ""
                    lblPopError.Text = ""
                    str_err = CallTransaction(errorMessage, bedit, OUT_SLA_ID)
                    If str_err = "0" Then
                        bindGrid()
                        btnSaveAbs.Visible = False
                        lblPopError.CssClass = "divvalidPopUp"
                        If bedit = False Then

                            callSend_mail_Notification(OUT_SLA_ID, Session("STU_ACD_ID"), Session("STU_ID"), Session("STU_GRD_ID"), Session("STU_SCT_ID"))

                            lblPopError.Text = "Record updated successfully.Email notification has been sent to the authorized staff for approval. "
                        Else
                            lblPopError.Text = "Record updated successfully."
                        End If
                    Else
                        lblPopError.CssClass = "diverrorPopUp"
                        lblPopError.Text = errorMessage
                    End If


                End If


            ElseIf rbLRequest.Checked = True Then
                If ViewState("bEdit") = 0 Then
                    txtFDT = txtFromDt.Text
                    dtFrom = String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)

                    If DateTime.Compare(dtFrom, txtFDT) > 0 Then
                        lblPopError.CssClass = "divinfoPopUp"
                        lblPopError.Text = " From Date entered is not a valid date and must be greater than or equal to " & String.Format("{0:" & OASISConstants.DateFormat & "}", DateTime.Now)
                    ElseIf leave_days_bsu > 0 AndAlso totDays > leave_days_bsu Then
                        lblPopError.CssClass = "divinfoPopUp"
                        lblPopError.Text = " Please contact the school to apply for leave more than " & leave_days_bsu & " days"

                    Else

                        lblPopError.CssClass = ""
                        lblPopError.Text = ""
                        str_err = CallTransaction(errorMessage, bedit, OUT_SLA_ID)
                        If str_err = "0" Then
                            bindGrid_Req()
                            btnSaveAbs.Visible = False
                            bedit = False
                            lblPopError.CssClass = "divvalidPopUp"
                            If bedit = False Then
                                callSend_mail_Notification(OUT_SLA_ID, Session("STU_ACD_ID"), Session("STU_ID"), Session("STU_GRD_ID"), Session("STU_SCT_ID"))

                                lblPopError.Text = "Record updated successfully.Email notification has been sent to the authorized staff for approval. "
                            Else
                                lblPopError.Text = "Record updated successfully."
                            End If
                        Else
                            lblPopError.CssClass = "diverrorPopUp"
                            lblPopError.Text = errorMessage
                        End If

                    End If
                Else

                    lblPopError.CssClass = ""
                    lblPopError.Text = ""
                    str_err = CallTransaction(errorMessage, bedit, OUT_SLA_ID)
                    If str_err = "0" Then
                        bindGrid_Req()
                        btnSaveAbs.Visible = False
                        lblPopError.CssClass = "divvalidPopUp"

                        If bedit = False Then
                            callSend_mail_Notification(OUT_SLA_ID, Session("STU_ACD_ID"), Session("STU_ID"), Session("STU_GRD_ID"), Session("STU_SCT_ID"))



                            lblPopError.Text = "Record updated successfully.Email notification has been sent to the authorized staff for approval. "
                        Else
                            lblPopError.Text = "Record updated successfully."
                        End If

                    Else
                        lblPopError.CssClass = "diverrorPopUp"
                        lblPopError.Text = errorMessage
                    End If
                End If

                End If





        End If

    End Sub

    Private Function CallTransaction(ByRef errorMessage As String, ByRef bEdit As Boolean, ByRef OUT_SLA_ID As String) As String
        Dim Status As Integer
        Dim RecordStatus As String = String.Empty
        Dim APD_ID As String = String.Empty
        Dim FROMDT As String = txtFromDt.Text
        Dim TODATE As String = txtToDt.Text
        Dim REMARK As String = txtremark.Text
        Dim SLA_ID As String = "0"

        Dim TYPE As String = String.Empty

        Dim html As String = String.Empty



        bEdit = False
        If rbLNote.Checked = True Then
            TYPE = "LN"
            bEdit = False
            APD_ID = ViewState("APD_ID")
        ElseIf rbLRequest.Checked = True Then
            TYPE = "LR"
            APD_ID = ddlParam.SelectedValue

            If ViewState("bEdit") = 1 Then
                bEdit = True
                SLA_ID = ViewState("SLA_ID")
            End If
        End If

        If TYPE = "LN" Then
            html = ScreenScrapeHtml(Server.MapPath("ParentTemplate_Note.html")) ' leave note 
        Else
            html = ScreenScrapeHtml(Server.MapPath("ParentTemplate_Req.html")) 'Leave Request
        End If
        html = html.Replace("@FRMDT", FROMDT)
        html = html.Replace("@TODT", TODATE)
        html = html.Replace("@REMARK", REMARK)
        html = html.Replace("@ATT_TYPE", IIf(ddlParam.SelectedIndex = -1, "", ddlParam.SelectedItem.Text))
        html = html.Replace("@SCH", Session("BSU_NAME"))
        html = html.Replace("@SNAME", Session("STU_NAME"))

        Dim param(14) As SqlClient.SqlParameter
        Dim transaction As SqlTransaction

        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                param(0) = New SqlClient.SqlParameter("@SLA_ID", SLA_ID)
                param(1) = New SqlClient.SqlParameter("@STU_ID", Session("STU_ID"))
                param(2) = New SqlClient.SqlParameter("@ACD_ID", Session("STU_ACD_ID"))
                param(3) = New SqlClient.SqlParameter("@GRD_ID", Session("STU_GRD_ID"))
                param(4) = New SqlClient.SqlParameter("@SCT_ID", Session("STU_SCT_ID"))
                param(5) = New SqlClient.SqlParameter("@FROMDT", FROMDT)
                param(6) = New SqlClient.SqlParameter("@TODT", TODATE)
                param(7) = New SqlClient.SqlParameter("@REMARK", REMARK)
                param(8) = New SqlClient.SqlParameter("@APD_ID", APD_ID)
                param(9) = New SqlClient.SqlParameter("@SLA_TYPE", TYPE)
                param(10) = New SqlClient.SqlParameter("@bEDIT", bEdit)
                param(11) = New SqlClient.SqlParameter("@htmlMsg", html.ToString())



                param(12) = New SqlClient.SqlParameter("@RETURN_TYPE", SqlDbType.Int)
                param(12).Direction = ParameterDirection.ReturnValue
                param(13) = New SqlClient.SqlParameter("@OUT_SLA_ID", SqlDbType.Int)
                param(13).Direction = ParameterDirection.Output

                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "OPL.SAVELEAVE_REQUEST", param)
                Status = param(12).Value




                If Status = -99 Then
                    CallTransaction = "1"
                    errorMessage = "Error while updating records."
                    Return "1"
                ElseIf Status = -111 Then
                    CallTransaction = "1"
                    errorMessage = "Record cannot be updated.From date entered is already set for leave approval !!!"
                    Return "1"
                ElseIf Status = -222 Then
                    CallTransaction = "1"
                    errorMessage = "Record cannot be updated.From date entered is already set for leave approval !!!"
                    Return "1"
                ElseIf Status <> 0 Then
                    CallTransaction = "1"
                    errorMessage = "Record could not be updated !!!"
                    Return "1"
                Else
                    If Status = 0 Then
                        OUT_SLA_ID = IIf(TypeOf (param(13).Value) Is DBNull, String.Empty, param(13).Value)


                        CallTransaction = "0"
                    End If


                End If



            Catch ex As Exception

                errorMessage = "Record could not be updated"
            Finally
                If CallTransaction <> "0" Then
                    UtilityObj.Errorlog(errorMessage)
                    transaction.Rollback()
                Else
                    errorMessage = ""
                    transaction.Commit()
                End If
            End Try

        End Using

    End Function
    Private Sub callSend_mail_Notification(ByVal SLA_ID As String, _
                                            ByVal ACD_ID As String, ByVal STU_ID As String, ByVal GRD_ID As String, _
                                            ByVal SCT_ID As String)

        Dim lintDaysAbs As String
        Dim lintDaysLeave As String
        Dim lintDaysPresent As String

        ' =============== To Get the Leave Days
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim param(2) As SqlClient.SqlParameter
        param(0) = New SqlClient.SqlParameter("@stu_id", STU_ID)
        param(1) = New SqlClient.SqlParameter("@Tacd_id", ACD_ID)

        Using readerStudent_Detail As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "GetStud_dashBoard_Att", param)
            If readerStudent_Detail.HasRows = True Then
                While readerStudent_Detail.Read

                    lintDaysAbs = readerStudent_Detail("tot_abs").ToString
                    lintDaysPresent = (readerStudent_Detail("tot_att").ToString)
                    lintDaysLeave = readerStudent_Detail("tot_leave").ToString
                End While
            Else

            End If
        End Using

        ' =============== End of To Get the Leave Days




        Dim MAILLIST As String = String.Empty
        Dim errormsg As String = String.Empty
        Dim status As String = String.Empty

        Dim htmlString As String = String.Empty
        Dim ds As New DataSet



        Dim conn As String = ConnectionManger.GetOASISConnectionString


        Dim BSC_HOST As String = String.Empty
        Dim BSC_USERNAME As String = String.Empty
        Dim BSC_PASSWORD As String = String.Empty
        Dim BSC_PORT As String = String.Empty
        Dim USR_EMAIL As String = String.Empty
        Dim Emp_ID As String = String.Empty
        Dim REMARK As String = String.Empty
        Dim strHtml As String = String.Empty
        Dim pParms(8) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@SLA_ID", SLA_ID)
        pParms(1) = New SqlClient.SqlParameter("@ACD_ID", ACD_ID)
        pParms(2) = New SqlClient.SqlParameter("@STU_ID", STU_ID)
        pParms(3) = New SqlClient.SqlParameter("@GRD_ID", GRD_ID)
        pParms(4) = New SqlClient.SqlParameter("@SCT_ID", SCT_ID)



        ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "OPL.GETSTUD_LEAVE_APPROVAL_MAIL_LIST", pParms)

        If ds.Tables(0).Rows.Count > 0 Then
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                Try
                    Dim html As String = ScreenScrapeHtml(Server.MapPath("LeaveTemplate.htm"))
                    Dim msg As New MailMessage

                    htmlString = "Dear @ENAME ,<br />" & _
      "<br />Parent has applied for the online leave approval for his/her child with the following detail given below<br /><br />" & _
     " <table cellpadding='7px' cellspacing='0px' width='550px' style='margin:0px;padding:0px;font-family:verdana, Arial, Helvetica, sans-serif; font-size:11px;' border='0' > " & _
    " <tr><td width='150px' style='font-weight:bold'>Name</td><td>@SNAME</td></tr> " & _
    " <tr><td style='font-weight:bold'>Grade & Section</td><td>@GRD_SCT</td></tr> " & _
    " <tr><td style='font-weight:bold'>Attendance type</td><td>@ATT_TYPE</td></tr> " & _
    " <tr><td style='font-weight:bold'>Leave Date</td><td>@LEAVE_DATE</td></tr> " & _
    " <tr><td colspan='2'><b>Remark :</b> @REMARK</td></tr> " & _
     " <tr><td colspan='2'><b>Leave History :</b> </td></tr> " & _
      " <tr><td style='font-weight:bold'>Days Present</td><td>@lintDaysPresent</td></tr> " & _
      " <tr><td style='font-weight:bold'>Days Absent</td><td>@lintDaysAbs</td></tr> " & _
      " <tr><td style='font-weight:bold'>Days on Leave</td><td>@lintDaysLeave</td></tr> " & _
    " </table><br /><br /> Please login to Student Management module to access Leave Approval menu to complete the approval process.<br/><br/> Thanks."
                    html = html.Replace("@Content", htmlString)
                    BSC_HOST = Convert.ToString(ds.Tables(0).Rows(i)("BSC_HOST"))
                    BSC_USERNAME = Convert.ToString(ds.Tables(0).Rows(i)("BSC_USERNAME"))
                    BSC_PASSWORD = Convert.ToString(ds.Tables(0).Rows(i)("BSC_PASSWORD"))
                    BSC_PORT = Convert.ToString(ds.Tables(0).Rows(i)("BSC_PORT"))


                    html = html.Replace("@ENAME", Convert.ToString(ds.Tables(0).Rows(i)("ENAME")))
                    html = html.Replace("@SNAME", Convert.ToString(ds.Tables(0).Rows(i)("SNAME")))
                    html = html.Replace("@GRD_SCT", Convert.ToString(ds.Tables(0).Rows(i)("GRADE")) & " & " & Convert.ToString(ds.Tables(0).Rows(i)("SCT_NAME")))
                    html = html.Replace("@ATT_TYPE", Convert.ToString(ds.Tables(0).Rows(i)("ATT_DESCR")))
                    html = html.Replace("@LEAVE_DATE", Convert.ToString(ds.Tables(0).Rows(i)("FROMDATE")) & " - " & Convert.ToString(ds.Tables(0).Rows(i)("TODATE")))
                    html = html.Replace("@REMARK", Convert.ToString(ds.Tables(0).Rows(i)("REMARK")))
                    html = html.Replace("@lintDaysPresent", Convert.ToString(lintDaysPresent))
                    html = html.Replace("@lintDaysAbs", Convert.ToString(lintDaysAbs))
                    html = html.Replace("@lintDaysLeave", Convert.ToString(lintDaysLeave))
                    REMARK = Convert.ToString(ds.Tables(0).Rows(i)("REMARK"))
                    USR_EMAIL = Convert.ToString(ds.Tables(0).Rows(i)("EMAIL"))
                    Emp_ID = Convert.ToString(ds.Tables(0).Rows(i)("EMP_ID"))

                    'Commented by MEGHA 18/9/2019
                    'status = SendPlainTextEmails(BSC_USERNAME, USR_EMAIL, "Online Student Leave Approval", html.ToString, BSC_USERNAME, BSC_PASSWORD, BSC_HOST, BSC_PORT, "0", False)

                    'ADDED by MEGHA 18/9/2019
                    Dim paramE(9) As SqlClient.SqlParameter
                    paramE(0) = New SqlClient.SqlParameter("@STU_ID", Session("STU_ID"))
                    paramE(1) = New SqlClient.SqlParameter("@FROM_EMAIL", BSC_USERNAME)
                    paramE(2) = New SqlClient.SqlParameter("@TO_EMAIL", USR_EMAIL)
                    paramE(3) = New SqlClient.SqlParameter("@SUBJECT", "Online Student Leave Approval")
                    paramE(4) = New SqlClient.SqlParameter("@MSG", html.ToString)
                    paramE(5) = New SqlClient.SqlParameter("@LOG_USERNAME", BSC_USERNAME)
                    paramE(6) = New SqlClient.SqlParameter("@LOG_PASSWORD", BSC_PASSWORD)
                    paramE(7) = New SqlClient.SqlParameter("@EmailHostNew", BSC_HOST)
                    paramE(8) = New SqlClient.SqlParameter("@PORT_NUM", BSC_PORT)
                    paramE(9) = New SqlClient.SqlParameter("@RETURN_TYPE", SqlDbType.Int)
                    paramE(9).Direction = ParameterDirection.ReturnValue
                    SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "OPL.SEND_PLAIN_TEXT_EMAIL", paramE)
                    status = paramE(9).Value

                    strHtml = html.ToString
                Catch ex As Exception
                    status = "0"  'CHNGD status -1 to 0
                Finally
                    If status = "0" Then  '-1 to 0
                        errormsg = "Error while sending email."
                    Else
                        errormsg = "Email successfully send."
                    End If
                    Dim Parms(5) As SqlClient.SqlParameter
                    Parms(0) = New SqlClient.SqlParameter("@SLA_ID", SLA_ID)
                    Parms(1) = New SqlClient.SqlParameter("@EMP_ID", Emp_ID)
                    Parms(2) = New SqlClient.SqlParameter("@EMAIL_ADD", USR_EMAIL)
                    Parms(3) = New SqlClient.SqlParameter("@EMAIL_STATUS", errormsg)
                    Parms(4) = New SqlClient.SqlParameter("@REMARK", REMARK)
                    Parms(5) = New SqlClient.SqlParameter("@CONTENT", strHtml)
                    SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "OPL.SAVESTUD_LEAVE_APPROVAL_MAIL_LOG", Parms)

                End Try


            Next


        End If






    End Sub
    Private Shared Function ScreenScrapeHtml(ByVal url As String) As String
        Dim objRequest As System.Net.WebRequest = System.Net.HttpWebRequest.Create(url)
        Dim sr As New StreamReader(objRequest.GetResponse().GetResponseStream())
        Dim result As String = sr.ReadToEnd()
        sr.Close()
        Return result
    End Function 'ScreenScrapeHtml

    Private Function SendPlainTextEmails(ByVal FromEmailId As String, ByVal ToEmailId As String, ByVal Subject As String, ByVal MailBody As String, ByVal Username As String, ByVal password As String, ByVal Host As String, ByVal Port As Integer, ByVal Templateid As String, ByVal HasAttachments As Boolean) As String
        Dim ReturnValue = ""

        Dim client As New System.Net.Mail.SmtpClient(Host, Port)
        Dim msg As New System.Net.Mail.MailMessage(FromEmailId, ToEmailId)
        Try
            msg.Subject = Subject

            msg.Body = MailBody

            msg.Priority = MailPriority.Normal

            msg.IsBodyHtml = True


            Dim HTMLView As AlternateView = AlternateView.CreateAlternateViewFromString(MailBody, Nothing, "text/html")

            msg.AlternateViews.Add(HTMLView)

            'client.DeliveryMethod = SmtpDeliveryMethod.PickupDirectoryFromIis
            If Username <> "" And password <> "" Then
                Dim creds As New System.Net.NetworkCredential(Username, password)
                client.Credentials = creds
            End If

            Dim s As String = client.Host

            client.Send(msg)

            ReturnValue = "0"
        Catch smptEx As System.Net.Mail.SmtpException
            Dim statuscode As System.Net.Mail.SmtpStatusCode
            statuscode = smptEx.StatusCode
            ReturnValue = "-1"
            'If (statuscode = Net.Mail.SmtpStatusCode.MailboxBusy) Or (statuscode = Net.Mail.SmtpStatusCode.MailboxUnavailable) Then
            '    System.Threading.Thread.Sleep(100)
            '    client.Send(msg)
            'Else
            '    UtilityObj.Errorlog(statuscode)
            '    ReturnValue = "-1"
            'End If

        Catch ex As Exception
            ReturnValue = "-1"
        End Try

        Return ReturnValue

    End Function

    Public Function getBsuLeaveDaysParameter() As Integer

        Dim LeaveDays As Integer = 0
        Dim conn As String = ConnectionManger.GetOASISConnectionString
        Dim param(2) As SqlParameter
        param(0) = New SqlClient.SqlParameter("@bsu_id", Session("OLU_bsuid"))

        Using DATAREADER As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "OPL.get_bsu_leave_days", param)
            While DATAREADER.Read
                LeaveDays = Convert.ToInt32(DATAREADER("bsu_max_leave_days"))
            End While
        End Using

        Return LeaveDays

    End Function

    Function ValidateDate(ByRef CommStr As String) As String
        Try



            Dim ErrorStatus As String = String.Empty
            CommStr = "<UL>"

            If txtFromDt.Text.Trim <> "" Then
                Dim strfDate As String = txtFromDt.Text.Trim
                Dim str_err As String = DateFunctions.checkdate(strfDate)
                If str_err <> "" Then
                    ErrorStatus = "-1"
                    CommStr = CommStr & "<li>From Date format is Invalid"
                Else
                    txtFromDt.Text = strfDate
                    Dim dateTime1 As String
                    dateTime1 = Date.ParseExact(txtFromDt.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                    'check for the leap year date
                    If Not IsDate(dateTime1) Then
                        ErrorStatus = "-1"
                        CommStr = CommStr & "<li>From Date format is Invalid"
                    End If
                End If
            Else
                CommStr = CommStr & "<li>From Date required"

            End If


            If txtToDt.Text.Trim <> "" Then

                Dim strfDate As String = txtToDt.Text.Trim
                Dim str_err As String = DateFunctions.checkdate(strfDate)
                If str_err <> "" Then
                    ErrorStatus = "-1"
                    CommStr = CommStr & "<li>To Date format is Invalid"
                Else
                    txtToDt.Text = strfDate
                    Dim DateTime2 As Date
                    Dim dateTime1 As Date
                    Dim strfDate1 As String = txtToDt.Text.Trim
                    Dim str_err1 As String = DateFunctions.checkdate(strfDate1)
                    If str_err1 <> "" Then
                    Else
                        DateTime2 = Date.ParseExact(txtToDt.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                        dateTime1 = Date.ParseExact(txtFromDt.Text, "dd/MMM/yyyy", New System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.None)
                        'check for the leap year date
                        If IsDate(DateTime2) Then
                            If DateTime.Compare(dateTime1, DateTime2) > 0 Then
                                ErrorStatus = "-1"
                                CommStr = CommStr & "<li>To Date entered is not a valid date and must be greater than or equal to From Date"
                            End If
                        Else
                            ErrorStatus = "-1"
                            CommStr = CommStr & "<li>To Date format is Invalid"
                        End If
                    End If
                End If
            Else
                CommStr = CommStr & "<li>To Date required"

            End If
            CommStr += "</UL>"

            Return ErrorStatus
        Catch ex As Exception
            UtilityObj.Errorlog("UNEXPECTED ERROR IN ADMISSION DATE SELECT", "rptAdmission_Details")
            Return "-1"
        End Try

    End Function

    Protected Sub gvLeaveReq_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvLeaveReq.RowDataBound
        'Dim lblmsg As Label = CType(Page.Master.FindControl("lblSiteMessage"), Label)
        
      
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim lblAPP_STATUS As Label = DirectCast(e.Row.FindControl("lblAPP_STATUS"), Label)
            If lblAPP_STATUS.Text.Contains("Pending") = True Then
                btnAppLev_Req.Enabled = False
                FLAG += 1

                If FLAG = 1 Then
                    lblmsg.Visible = True

                    lblmsg.CssClass = "divinfo"

                    lblmsg.Text = "No new leave approval can be added,since pending approval exist !!!"
                End If


            End If
        End If
    End Sub
End Class
