﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Net.Mail
Imports System.Text
Partial Class Others_ReEnroll_Process
    Inherits System.Web.UI.Page
    Private FLAG As Integer = 0
    Dim Encr_decrData As New Encryption64
    Dim encr_decr As New SFENCRYPTION
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If Session("username") Is Nothing Then
                Session("Active_tab") = "Home"
                Session("Site_Path") = ""
                Response.Redirect("~\login.aspx")
            ElseIf Session("bPasswdChanged") = "False" Then
                Response.Redirect("~\UpdateInfo\ForcePasswordChange.aspx", False)
            ElseIf Session("bUpdateContactDetails") = "False" Then
                Response.Redirect(Session("ForceUpdate_stud"), False)
            End If


            'ViewState("viewid") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
            If Page.IsPostBack = False Then
                ViewState("VERIFY_ID") = ""
                ViewState("CHECK_GEMS_STAFF") = ""
                ViewState("FLAG_STATUS") = ""
                ViewState("REENROLL_STATUS") = ""
                ViewState("EMP_ID") = ""
                lbChildName.Text = Session("STU_NAME")
                divInitalState.Visible = True


                divACKState.Visible = False
                pnlSTBooking.Visible = False
                trGEMStaff2.Visible = False



            End If

            If GET_isCRM_SF() = 0 Then
                divOasis_Reenrol.Visible = True
                divSF_Reenrol.Visible = False
                BIND_INFO()
            Else
                divOasis_Reenrol.Visible = False
                divSF_Reenrol.Visible = True
                hdn_stuno.Value = encr_decr.Encrypt(Session("STU_NO"))
                hdn_bsuid.Value = encr_decr.Encrypt(Session("sBsuid"))

                Dim url As String = "https://gems-edu.secure.force.com/GEMS/GEMS_reEnrollment?studentId=" + hdn_stuno.Value + "&BSU=" + hdn_bsuid.Value

                ifReenrol.Attributes.Add("src", url)
            End If


        Catch ex As Exception

        End Try
    End Sub
    Private Sub BIND_INFO()
        Dim CONN As String = ConnectionManger.GetOASISConnectionString
        Dim PARAM(1) As SqlParameter
        PARAM(0) = New SqlParameter("@STU_ID", Session("STU_ID"))
        PARAM(1) = New SqlParameter("@ACD_ID", Session("STU_ACD_ID"))
        Using DATAREADER As SqlDataReader = SqlHelper.ExecuteReader(CONN, CommandType.StoredProcedure, "OPL.GETSTUDENT_REENROLLMENT_DETAILS", PARAM)
            If DATAREADER.HasRows = True Then
                While DATAREADER.Read
                    If Convert.ToString(DATAREADER("FEE_DUE")) = "" Then
                        spYes.InnerHtml = Convert.ToString(DATAREADER("RET_YES"))
                        SpNo.InnerHtml = Convert.ToString(DATAREADER("RET_No"))
                        '  sp_Fee_bal.InnerHtml = Convert.ToString(DATAREADER("RET_MTH_FEE_BAL"))
                        SpMTN_FULL.InnerHtml = Convert.ToString(DATAREADER("RET_MTH_START_FULL"))
                        ViewState("CHECK_GEMS_STAFF") = Convert.ToString(DATAREADER("CHECK_GEMS_STAFF"))
                        ViewState("FLAG_STATUS") = Convert.ToString(DATAREADER("FLAG_STATUS"))
                        ViewState("REENROLL_STATUS") = Convert.ToString(DATAREADER("REENROLL_STATUS"))
                        ViewState("EMP_ID") = Convert.ToString(DATAREADER("EMP_ID"))
                        Session("Enroll_AMT") = Convert.ToString(DATAREADER("FEE_AMT"))
                        Session("RET_FOOTER_TEXT") = Convert.ToString(DATAREADER("RET_FOOTER_TEXT"))
                        lt_Footer3.Text = Session("RET_FOOTER_TEXT")
                        Session("NEXT_ACD_ID_EN") = Convert.ToString(DATAREADER("NEXT_ACD_ID"))
                        Session("ACD_REENROLL_DELINK_FEE") = Convert.ToString(DATAREADER("ACD_REENROLL_DELINK_FEE"))
                        ViewState("ALLOW_ONLINE_PAYMENT") = 0
                        ViewState("ALLOW_SCHOOL_PAYMENT") = 0
                        If ViewState("FLAG_STATUS") = "PENDING" Then
                            divInitalState.Visible = True
                            divACKState.Visible = False
                            pnlSTBooking.Visible = False
                            If ViewState("CHECK_GEMS_STAFF") = "YES" Then
                                trGEMStaff1.Visible = True
                                trGEMStaff2.Visible = False
                                trGEMStaff3.Visible = True
                            Else
                                trGEMStaff1.Visible = False
                                trGEMStaff2.Visible = False
                                trGEMStaff3.Visible = False
                            End If
                            If Convert.ToBoolean(DATAREADER("bShowCurr")) = True Then
                                ViewState("bShowCurr") = 1
                                bindCurr()
                                trCurrContent.Visible = True
                                trCurrH.Visible = True
                            Else
                                ViewState("bShowCurr") = 0
                                trCurrContent.Visible = False
                                trCurrH.Visible = False

                            End If

                            If Convert.ToBoolean(DATAREADER("ALLOW_ONLINE_PAYMENT")) = True Then
                                ViewState("ALLOW_ONLINE_PAYMENT") = 1
                            Else
                                ViewState("ALLOW_ONLINE_PAYMENT") = 0
                            End If

                            If Convert.ToBoolean(DATAREADER("ALLOW_SCHOOL_PAYMENT")) = True Then
                                ViewState("ALLOW_SCHOOL_PAYMENT") = 1
                            Else
                                ViewState("ALLOW_SCHOOL_PAYMENT") = 0
                            End If


                        ElseIf ViewState("FLAG_STATUS") = "FEE_PENDING" Then
                            Response.Redirect("ReEnrollPaymentRequest.aspx")
                        ElseIf ViewState("FLAG_STATUS") = "NOT_ENROL" Then
                            divInitalState.Visible = False
                            divACKState.Visible = True
                            pnlSTBooking.Visible = False
                            divACKState.InnerHtml = Convert.ToString(DATAREADER("RET_NOT_ENROLL_MAIL"))
                        ElseIf ViewState("FLAG_STATUS") = "ENROL" Then
                            divInitalState.Visible = False
                            divACKState.Visible = True
                            pnlSTBooking.Visible = False
                            '--1	GEMS Staff Enrolled 2 Company Staff Enrolled 3	Parent Enrolled 4	Not Enrolling 5	Pending ---null case
                            If ViewState("REENROLL_STATUS") = "1" Then
                                divACKState.InnerHtml = Convert.ToString(DATAREADER("RET_GEMS_ENROLL_MAIL"))
                            ElseIf ViewState("REENROLL_STATUS") = "2" Then
                                divACKState.InnerHtml = Convert.ToString(DATAREADER("RET_COMP_ENROLL_MAIL"))
                            ElseIf ViewState("REENROLL_STATUS") = "3" Then
                                divACKState.InnerHtml = Convert.ToString(DATAREADER("RET_ENROLL_MAIL"))
                            End If

                        End If
                    Else
                        divInitalState.Visible = False
                        divACKState.Visible = False
                        Dim lblmsg As Label = CType(Page.Master.FindControl("lblSiteMessage"), Label)
                        lblmsg.CssClass = "diverror"
                        lblmsg.Text = Convert.ToString(DATAREADER("FEE_DUE"))
                    End If
                End While
            Else
                divInitalState.Visible = False
                divACKState.Visible = False
                Dim lblmsg As Label = CType(Page.Master.FindControl("lblSiteMessage"), Label)
                lblmsg.CssClass = "diverror"
                lblmsg.Text = "Re-enrolment is not applicable for the selected student !!! "
            End If
        End Using



    End Sub
    Function GET_isCRM_SF() As Integer
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim pParms(3) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", Session("sBsuid"))

        Using reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "get_CRMonSF", pParms)
            While reader.Read
                Return reader("BSU_bIsCRMOnSF")

            End While
        End Using
    End Function
    Private Sub bindCurr()
        Dim CONN As String = ConnectionManger.GetOASISConnectionString
        Dim PARAM(1) As SqlParameter
        PARAM(0) = New SqlParameter("@STU_ID", Session("STU_ID"))
        PARAM(1) = New SqlParameter("@ACD_ID", Session("STU_ACD_ID"))
        Using DATAREADER As SqlDataReader = SqlHelper.ExecuteReader(CONN, CommandType.StoredProcedure, "OPL.GETSTUDENT_REENROL_CLM", PARAM)
            If DATAREADER.HasRows = True Then
                While DATAREADER.Read

                    ddlStu_Curr.Items.Add(New ListItem(Convert.ToString(DATAREADER("CLM_DESCR")), Convert.ToString(DATAREADER("ACD_ID"))))
                End While



            End If
        End Using
        ddlStu_Curr.Items.Add(New ListItem("-- Select --", "0"))
        ddlStu_Curr.DataBind()
        ddlStu_Curr.ClearSelection()
        ddlStu_Curr.Items.FindByValue("0").Selected = True





    End Sub

    Protected Sub btnProceed_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProceed.Click

        '' divNote.Attributes.Add("class", "divinfoInner")
        'divNote.InnerText = ""

        lbmsgInfo.CssClass = "divinfoInner"
        lbmsgInfo.Text = ""
        Dim strmsg As String = String.Empty
        Dim errCount As Integer = 0

        If ViewState("CHECK_GEMS_STAFF") = "YES" Then
            If chkGEMS_Y.Checked = True And chkGEMS_N.Checked = True Then
                errCount = errCount + 1
                strmsg += "&nbsp;&nbsp;Please select the appropriate box under GEMS staff <br />"

            ElseIf chkGEMS_Y.Checked = False And chkGEMS_N.Checked = False Then
                errCount = errCount + 1
                strmsg += "&nbsp;&nbsp;Please select the appropriate box under GEMS staff <br />"


            ElseIf chkGEMS_Y.Checked = True And chkGEMS_N.Checked = False Then
                trGEMStaff2.Visible = True
                If txtUsr_id.Text.Trim = "" Then
                    errCount = errCount + 1
                    strmsg += "&nbsp;&nbsp;GEMS offical id required <br />"
                End If
                If txtStaffNo.Text.Trim = "" Then
                    errCount = errCount + 1
                    strmsg += "&nbsp;&nbsp;Enter 8 digit Staff No <br />"
                End If

                If txtStaffNo.Text.Trim <> "" And txtUsr_id.Text.Trim <> "" Then
                    verifyGEMS_Staff()
                    If ViewState("VERIFY_STATUS") = "NO" Then
                        errCount = errCount + 1
                        strmsg += "&nbsp;&nbsp;Incorrect GEMS id or staff no !! <br />"
                    End If

                End If
            End If


            If chkYes.Checked = True And chkNo.Checked = True Then
                errCount = errCount + 1
                strmsg += "&nbsp;&nbsp;Select the appropriate re-enrolment box <br />"
            ElseIf chkYes.Checked = False And chkNo.Checked = False Then
                errCount = errCount + 1
                strmsg += "&nbsp;&nbsp;Select the appropriate re-enrolment box <br />"
            End If

        ElseIf ViewState("CHECK_GEMS_STAFF") = "NO" Then
            If chkYes.Checked = True And chkNo.Checked = True Then
                errCount = errCount + 1
                strmsg += "&nbsp;&nbsp;Select the appropriate re-enrolment box <br />"
            ElseIf chkYes.Checked = False And chkNo.Checked = False Then
                errCount = errCount + 1
                strmsg += "&nbsp;&nbsp;Select the appropriate re-enrolment box <br />"
            End If


        End If

        If ViewState("bShowCurr") = 1 Then
            If chkNo.Checked = False Then
                If ddlStu_Curr.SelectedValue = "0" Then
                    errCount = errCount + 1
                    strmsg += "&nbsp;&nbsp;Choose the curriculum for the next academic year <br />"

                End If
            End If

        End If

        If chkYes.Checked Then
            divDeclaration.Style.Add("display", "Block")
            divDeclaration2.Style.Add("display", "Block")

            If chkagree.Checked Then
                '' divNote.Visible = False
            Else
                divNote.Visible = True
                lblError.Text = ""
                errCount = errCount + 1
                strmsg += "&nbsp;&nbsp;You must accept the Declaration by ticking on the checkbox <br />"
                divNote.Attributes.Remove("style")
            End If
        End If

        If strmsg.Trim = "" Then

            If ViewState("CHECK_GEMS_STAFF") = "NO" Or ViewState("VERIFY_STATUS") = "YES" Then
                btnPayment.Visible = False And ViewState("ALLOW_ONLINE_PAYMENT") = "1"
                btnPaySchool.Visible = False And ViewState("ALLOW_SCHOOL_PAYMENT") = "1"
                btnConfirm.Visible = True
            ElseIf Session("ACD_REENROLL_DELINK_FEE") = "1" Then
                btnPayment.Visible = False And ViewState("ALLOW_ONLINE_PAYMENT") = "1"
                btnPaySchool.Visible = False And ViewState("ALLOW_SCHOOL_PAYMENT") = "1"
                btnConfirm.Visible = True
            Else

                If chkNo.Checked = True Then
                    btnPayment.Visible = False And ViewState("ALLOW_ONLINE_PAYMENT") = "1"
                    btnPaySchool.Visible = False And ViewState("ALLOW_SCHOOL_PAYMENT") = "1"
                    btnConfirm.Visible = True
                Else
                    btnPaySchool.Visible = True And ViewState("ALLOW_SCHOOL_PAYMENT") = "1"
                    btnPayment.Visible = True And ViewState("ALLOW_ONLINE_PAYMENT") = "1"
                    btnConfirm.Visible = False
                End If


            End If
            pnlSTBooking.Visible = True

        Else
            If chkYes.Checked Then
                divDeclaration.Style.Add("display", "Block")
                divDeclaration2.Style.Add("display", "Block")
            Else
                divDeclaration.Style.Add("display", "None")
                divDeclaration2.Style.Add("display", "None")

                'If chkagree.Checked Then
                '    divNote.Visible = False
                'Else
                '    divNote.Visible = True
                'End If
            End If
            lbmsgInfo.Text = strmsg
            ''to be done 28jan
            divNote.Attributes.Remove("style")
            divNote.Visible = True
            divNote.Style.Add("display", "Block")

            If errCount >= 1 Then
                divNote.Style.Add("Height", "25px")
            ElseIf errCount >= 2 Then
                divNote.Style.Add("Height", "50px")
            ElseIf errCount >= 3 Then
                divNote.Style.Add("Height", "75px")
            ElseIf errCount >= 4 Then
                divNote.Style.Add("Height", "95px")
            End If
            lblError.Text = strmsg
            Exit Sub

        End If
    End Sub
    Private Sub verifyGEMS_Staff()
        ViewState("VERIFY_STATUS") = "NO"
        ViewState("VERIFY_ID") = ""
        Dim CONN As String = ConnectionManger.GetOASISConnectionString
        Dim PARAM(1) As SqlParameter
        PARAM(0) = New SqlParameter("@USR_ID", txtUsr_id.Text.Trim)
        PARAM(1) = New SqlParameter("@EMPNO", txtStaffNo.Text.Trim)

        Using DATAREADER As SqlDataReader = SqlHelper.ExecuteReader(CONN, CommandType.StoredProcedure, "OPL.VERIFYGEMS_STAFF", PARAM)
            If DATAREADER.HasRows = True Then
                While DATAREADER.Read
                    ViewState("VERIFY_ID") = Convert.ToString(DATAREADER("EMP_ID"))
                    ViewState("VERIFY_STATUS") = "YES"
                End While
            End If
        End Using

    End Sub
    Protected Sub btnConfirm_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnConfirm.Click
        Dim lblmsg As Label = CType(Page.Master.FindControl("lblSiteMessage"), Label)
        lblpopMsg.CssClass = "divinfoInner"
        lblpopMsg.Text = ""
        Dim str_err As String = String.Empty
        Dim errorMessage As String = String.Empty
        Dim strmsg As String = String.Empty
        If chkNBAD_YES.Checked = True And chkNBAD_NO.Checked = True Then

            strmsg += "&nbsp;&nbsp;Please select the appropriate box to confirm NBAD GEMS credit card  <br />"

        ElseIf chkNBAD_YES.Checked = False And chkNBAD_NO.Checked = False Then

            strmsg += "&nbsp;&nbsp;Please select the appropriate box to confirm NBAD GEMS credit card  <br />"
        End If


        If strmsg.Trim = "" Then
            str_err = CallTransaction_NoPayment(errorMessage)
            If str_err = "0" Then
                lbmsgInfo.Text = ""

                lbmsgInfo.CssClass = ""
                BIND_INFO()
                lblmsg.CssClass = "divvalid"
                lblmsg.Text = "Record updated successfully.Email notification has been sent to your email address. "

                If ViewState("bENROLL_STATUS") = False Then
                    Enable_Disable_TCRequest()
                    plTcReq.Visible = True
                Else



                    plTcReq.Visible = False
                End If



            Else
                lbmsgInfo.Text = ""

                lbmsgInfo.CssClass = ""

                lblmsg.CssClass = "diverror"

                lblmsg.Text = errorMessage
            End If
            pnlSTBooking.Visible = False

        Else
            lblpopMsg.Text = strmsg
            Exit Sub
        End If

    End Sub


    Private Function CallTransaction_NoPayment(ByRef errorMessage As String) As String
        Dim Status As Integer
        'ViewState("CHECK_GEMS_STAFF")
        ' ViewState("VERIFY_ID")

        Dim bENROLL_STATUS As Boolean
        Dim bGEMS_STAFF As Boolean
        Dim VERIFY_ID As String = IIf(ViewState("VERIFY_ID") Is Nothing, "", ViewState("VERIFY_ID"))
        Dim EMP_ID As String = IIf(ViewState("EMP_ID") Is Nothing, "", ViewState("EMP_ID"))
        Dim bAVAIL_NBAD As Boolean
        Dim BSU_ID As String = IIf(Session("sBsuid") Is Nothing, "", Session("sBsuid"))
        Dim VERIFY_USR_ID As String = txtUsr_id.Text.Trim
        Dim VERIFY_EMP_NO As String = txtStaffNo.Text.Trim

        Dim STP_REENROL_CLM_ID As Integer
        If ddlStu_Curr.SelectedIndex <> -1 Then
            STP_REENROL_CLM_ID = ddlStu_Curr.SelectedValue

        Else
            STP_REENROL_CLM_ID = 0


        End If


        If chkYes.Checked = True Then
            bENROLL_STATUS = True
        ElseIf chkYes.Checked = False Then
            bENROLL_STATUS = False
        End If

        If chkGEMS_Y.Checked = True Then
            bGEMS_STAFF = True
        ElseIf chkGEMS_Y.Checked = False Then
            bGEMS_STAFF = False
        End If

        If bGEMS_STAFF = False Then
            VERIFY_USR_ID = ""
            VERIFY_EMP_NO = ""
        End If



        If chkNBAD_YES.Checked = True Then
            bAVAIL_NBAD = True
        ElseIf chkNBAD_YES.Checked = False Then
            bAVAIL_NBAD = False
        End If


        ViewState("bENROLL_STATUS") = bENROLL_STATUS

        Dim param(14) As SqlClient.SqlParameter
        Dim transaction As SqlTransaction

        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try

                param(0) = New SqlParameter("@STU_ID", Session("STU_ID"))
                param(1) = New SqlParameter("@ACD_ID", Session("STU_ACD_ID"))
                param(2) = New SqlClient.SqlParameter("@bENROLL_STATUS", bENROLL_STATUS)
                param(3) = New SqlClient.SqlParameter("@bGEMS_STAFF", bGEMS_STAFF)
                param(4) = New SqlClient.SqlParameter("@VERIFY_ID", VERIFY_ID)
                param(5) = New SqlClient.SqlParameter("@bAVAIL_NBAD", bAVAIL_NBAD)
                param(6) = New SqlClient.SqlParameter("@EMPNO", VERIFY_EMP_NO)
                param(7) = New SqlClient.SqlParameter("@EMP_ID", IIf(EMP_ID <> "", EMP_ID, System.DBNull.Value))
                param(8) = New SqlClient.SqlParameter("@BSU_ID", BSU_ID)
                param(9) = New SqlClient.SqlParameter("@USR_ID", VERIFY_USR_ID)
                param(10) = New SqlClient.SqlParameter("@STP_REENROL_CLM_ID", STP_REENROL_CLM_ID)
                param(11) = New SqlClient.SqlParameter("@RETURN_TYPE", SqlDbType.Int)
                param(11).Direction = ParameterDirection.ReturnValue

                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "OPL.SAVEReEnrolment_NoPayment", param)
                Status = param(11).Value

                If Status <> 0 Then
                    CallTransaction_NoPayment = "1"
                    errorMessage = "Record could not be updated !!!"
                    Return "1"
                Else

                    CallTransaction_NoPayment = "0"


                End If



            Catch ex As Exception

                errorMessage = "Record could not be updated"
            Finally
                If CallTransaction_NoPayment <> "0" Then
                    UtilityObj.Errorlog(errorMessage)
                    transaction.Rollback()
                Else
                    errorMessage = ""
                    transaction.Commit()
                End If
            End Try

        End Using

    End Function


    Protected Sub chkGEMS_N_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkGEMS_N.CheckedChanged
        If chkGEMS_N.Checked = True Then
            trGEMStaff2.Visible = False
            If chkYes.Checked Then
                divDeclaration.Attributes.Remove("Style")

                divDeclaration.Style.Add("Display", "block")
                divDeclaration2.Attributes.Remove("Style")

                divDeclaration2.Style.Add("Display", "block")
            Else
                divDeclaration.Attributes.Remove("Style")

                divDeclaration.Style.Add("Display", "none")
                divDeclaration2.Attributes.Remove("Style")

                divDeclaration2.Style.Add("Display", "none")
            End If
        ElseIf chkGEMS_Y.Checked = True Then
            trGEMStaff2.Visible = True

            If chkYes.Checked Then
                divDeclaration.Attributes.Remove("Style")

                divDeclaration.Style.Add("Display", "block")
                divDeclaration2.Attributes.Remove("Style")

                divDeclaration2.Style.Add("Display", "block")
            Else
                divDeclaration.Attributes.Remove("Style")

                divDeclaration.Style.Add("Display", "none")
                divDeclaration2.Attributes.Remove("Style")

                divDeclaration2.Style.Add("Display", "none")
            End If
        End If
    End Sub
    Protected Sub chkGEMS_Y_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkGEMS_Y.CheckedChanged
        If chkGEMS_N.Checked = True Then
            trGEMStaff2.Visible = False
            If chkYes.Checked Then
                divDeclaration.Attributes.Remove("Style")

                divDeclaration.Style.Add("Display", "block")
                divDeclaration2.Attributes.Remove("Style")

                divDeclaration2.Style.Add("Display", "block")
            End If
        ElseIf chkGEMS_Y.Checked = True Then
            trGEMStaff2.Visible = True

            If chkYes.Checked Then
                divDeclaration.Attributes.Remove("Style")

                divDeclaration.Style.Add("Display", "block")
                divDeclaration2.Attributes.Remove("Style")

                divDeclaration2.Style.Add("Display", "block")
            End If
        End If

    End Sub
   

    
    Protected Sub btnPayment_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPayment.Click
        Dim lblmsg As Label = CType(Page.Master.FindControl("lblSiteMessage"), Label)
        lblpopMsg.CssClass = "divinfoInner"
        lblpopMsg.Text = ""
        Dim str_err As String = String.Empty
        Dim errorMessage As String = String.Empty
        Dim strmsg As String = String.Empty
        If chkNBAD_YES.Checked = True And chkNBAD_NO.Checked = True Then

            strmsg += "&nbsp;&nbsp;Please select the appropriate box to confirm NBAD GEMS credit card  <br />"

        ElseIf chkNBAD_YES.Checked = False And chkNBAD_NO.Checked = False Then

            strmsg += "&nbsp;&nbsp;Please select the appropriate box to confirm NBAD GEMS credit card  <br />"
        End If



        If strmsg.Trim = "" Then
            str_err = CallTransaction_InitialSave(errorMessage, False)
            pnlSTBooking.Visible = False
            If str_err = "0" Then
                lbmsgInfo.Text = ""

                lbmsgInfo.CssClass = ""
                Session("Payment_status")="Show"
                Response.Redirect("ReEnrollPaymentRequest.aspx")
            Else

                lblmsg.CssClass = "diverror"

                lblmsg.Text = errorMessage
            End If


        Else
            lblpopMsg.Text = strmsg
            Exit Sub
        End If


    End Sub

    Protected Sub btnPaySchool_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPaySchool.Click

        Dim lblmsg As Label = CType(Page.Master.FindControl("lblSiteMessage"), Label)
        lblpopMsg.CssClass = "divinfoInner"
        lblpopMsg.Text = ""
        Dim str_err As String = String.Empty
        Dim errorMessage As String = String.Empty
        Dim strmsg As String = String.Empty
        If chkNBAD_YES.Checked = True And chkNBAD_NO.Checked = True Then

            strmsg += "&nbsp;&nbsp;Please select the appropriate box to confirm NBAD GEMS credit card  <br />"

        ElseIf chkNBAD_YES.Checked = False And chkNBAD_NO.Checked = False Then

            strmsg += "&nbsp;&nbsp;Please select the appropriate box to confirm NBAD GEMS credit card  <br />"
        End If


        If strmsg.Trim = "" Then
            str_err = CallTransaction_InitialSave(errorMessage, True)
            pnlSTBooking.Visible = False
            If str_err = "0" Then
                lbmsgInfo.Text = ""

                lbmsgInfo.CssClass = ""
                Session("Payment_status") = "Hide"
                Response.Redirect("ReEnrollPaymentRequest.aspx")
            Else

                lblmsg.CssClass = "diverror"

                lblmsg.Text = errorMessage
            End If


        Else
            lblpopMsg.Text = strmsg
            Exit Sub
        End If


    End Sub
    Private Function CallTransaction_InitialSave(ByRef errorMessage As String, ByVal bPayAtSchool As Boolean) As String
        Dim Status As Integer
        'ViewState("CHECK_GEMS_STAFF")
        ' ViewState("VERIFY_ID")

        Dim bAVAIL_NBAD As Boolean
        Dim BSU_ID As String = IIf(Session("sBsuid") Is Nothing, "", Session("sBsuid"))

        Dim STP_REENROL_CLM_ID As Integer
        If ddlStu_Curr.SelectedIndex <> -1 Then
            STP_REENROL_CLM_ID = ddlStu_Curr.SelectedValue

        Else
            STP_REENROL_CLM_ID = 0


        End If





        If chkNBAD_YES.Checked = True Then
            bAVAIL_NBAD = True
        ElseIf chkNBAD_YES.Checked = False Then
            bAVAIL_NBAD = False
        End If
        Dim param(14) As SqlClient.SqlParameter
        Dim transaction As SqlTransaction

        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try

                param(0) = New SqlParameter("@STU_ID", Session("STU_ID"))
                param(1) = New SqlParameter("@ACD_ID", Session("STU_ACD_ID"))
                param(2) = New SqlClient.SqlParameter("@bCARD_AVAIL", bAVAIL_NBAD)
                param(3) = New SqlClient.SqlParameter("@BSU_ID", BSU_ID)
                param(4) = New SqlClient.SqlParameter("@bPayAtSchool", bPayAtSchool)
                param(5) = New SqlClient.SqlParameter("@STP_REENROL_CLM_ID", STP_REENROL_CLM_ID)

                param(6) = New SqlClient.SqlParameter("@RETURN_TYPE", SqlDbType.Int)
                param(6).Direction = ParameterDirection.ReturnValue

                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "OPL.SAVEReEnrolment_Initial", param)
                Status = param(6).Value

                If Status <> 0 Then
                    CallTransaction_InitialSave = "1"
                    errorMessage = "Record could not be updated !!!"
                    Return "1"
                Else

                    CallTransaction_InitialSave = "0"


                End If



            Catch ex As Exception

                errorMessage = "Record could not be updated"
            Finally
                If CallTransaction_InitialSave <> "0" Then
                    UtilityObj.Errorlog(errorMessage)
                    transaction.Rollback()
                Else
                    errorMessage = ""
                    transaction.Commit()
                End If
            End Try

        End Using

    End Function
    'Private Sub CollectValue_BeforeRedirect()
    '    Dim ENROLL_STATUS As String = String.Empty
    '    Dim HAS_NBAD As String = String.Empty

    '    '0--false 1--true
    '    If chkYes.Checked = True Then
    '        ENROLL_STATUS = "1"
    '    ElseIf chkYes.Checked = False Then
    '        ENROLL_STATUS = "0"
    '    End If

    '    If chkNBAD_YES.Checked = True Then
    '        HAS_NBAD = "1"
    '    ElseIf chkNBAD_YES.Checked = False Then
    '        HAS_NBAD = "0"
    '    End If

    '    Session("bCARD_AVAIL") = HAS_NBAD

    '    Response.Redirect("ReEnrollPaymentRequest.aspx")
    'End Sub

   
    Protected Sub btnTCNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTCNo.Click
        plTcReq.Visible = False
    End Sub

    Protected Sub btntcYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btntcYes.Click
        Dim lblNavLink As Label = CType(Page.Master.FindControl("lblNavLink"), Label)
        plTcReq.Visible = False
        Session("Site_Path") = ">" & Session("Active_tab") & ">" & "TC Request"
        Session("Active_menu") = "TC Request"
        lblNavLink.Text = Session("Site_Path")
        Response.Redirect("OthTcRequest.aspx")

    End Sub

    Protected Sub btnCloseTC_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCloseTC.Click
        plTcReq.Visible = False
    End Sub

    Protected Sub btnTCClosemsg_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTCClosemsg.Click
        plTcReq.Visible = False
    End Sub

    Private Sub Enable_Disable_TCRequest()
        Dim CONN As String = ConnectionManger.GetOASISConnectionString
        Dim PARAM(2) As SqlParameter
        PARAM(0) = New SqlParameter("@BSU_ID", Session("sBsuid"))
        PARAM(1) = New SqlClient.SqlParameter("@RETURN_TYPE", SqlDbType.Int)
        PARAM(1).Direction = ParameterDirection.ReturnValue

        Using DATAREADER As SqlDataReader = SqlHelper.ExecuteReader(CONN, CommandType.StoredProcedure, "OPL.CHECKTC_REQUEST", PARAM)
            If DATAREADER.HasRows = True Then
                ltTCmsg.Text = "Would you like to apply for Transfer Certificate ?"
                btnTCNo.Visible = True
                btntcYes.Visible = True
                btnTCClosemsg.Visible = False
            Else
                ltTCmsg.Text = "Please approach the school for TC request."
                btnTCNo.Visible = False
                btntcYes.Visible = False
                btnTCClosemsg.Visible = True
            End If
        End Using
    End Sub
End Class
