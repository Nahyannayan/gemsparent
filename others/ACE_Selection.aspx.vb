﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Configuration
Imports System
Partial Class Others_ACE_Selection
    Inherits System.Web.UI.Page
    Dim Encr_decrData As New Encryption64
    Dim studClass As New studClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("username") Is Nothing Then
            Session("Active_tab") = "Home"
            Session("Site_Path") = ""
            Response.Redirect("~\General\Home.aspx")
        ElseIf Session("bPasswdChanged") = "False" Then
            Response.Redirect("~\UpdateInfo\ForcePasswordChange.aspx", False)
        ElseIf Session("bUpdateContactDetails") = "False" Then
            Response.Redirect(Session("ForceUpdate_stud"), False)
        End If
        If Not Page.IsPostBack Then
            'Session("STU_ID") = "20226077"
            'Session("STU_GRD_ID") = "08"
            'Session("STU_ACD_ID") = "962"
            'hfACD_ID_NEXT.Value = "881"

            Session("clm") = Session("ACD_CLM_ID")
            BindGender()
            lbChildName.Text = Session("STU_NAME")
            'GetNextACD_GRADE()

            GetNextACD_ID(Session("STU_ACD_ID"), Session("sBsuid"))

            GetNextGrade(Session("STU_GRD_ID"))
            'If hfGRD_ID_NEXT.Value = "05" Or hfGRD_ID_NEXT.Value = "06" Or hfGRD_ID_NEXT.Value = "07" Then
            'btnClearSelection.Visible = False
            'Else
            btnClearSelection.Visible = True
            'End If
            If Session("sbsuid") <> "125010" Then
                lnkGuidelines.Visible = False
            End If
            trClubError.Visible = False
            trGameError.Visible = False
            BindGames(Session("sBsuid"), hfGender.Value, hfGRD_ID_NEXT.Value)
            BindClubs(Session("sBsuid"), hfGender.Value, hfGRD_ID_NEXT.Value)
            BindComm(Session("sBsuid"), hfGender.Value, hfGRD_ID_NEXT.Value)
            CheckACEStatus()
        End If
    End Sub

    Public Sub CheckACEStatus()

        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
        Dim param(3) As SqlParameter
        param(0) = New SqlParameter("@ALD_STU_ID", Session("STU_ID"))
        param(1) = New SqlParameter("@ALD_STU_NEXTACD_ID", hfACD_ID_NEXT.Value)
        param(2) = New SqlParameter("@ALD_STU_NEXTGRD_ID", hfGRD_ID_NEXT.Value)
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "DBO.CHECK_ACE_STATUS_v2", param)
        Dim i As Integer
        Dim j As Integer
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then

                For i = 0 To ds.Tables(0).Rows.Count - 1
                    For j = 0 To rblGames.Items.Count - 1
                        If ds.Tables(0).Rows(i).Item(0) = rblGames.Items(j).Text Then
                            rblGames.Items(j).Selected = True
                            rblGames.Enabled = False
                            lblGame.Text = rblGames.Items(j).Text
                            If ds.Tables(0).Rows(i).Item(1) = "0" Then
                                rblChoice.Items(0).Selected = True
                            ElseIf ds.Tables(0).Rows(i).Item(1) = "1" Then
                                rblChoice.Items(1).Selected = True
                            Else
                                rblChoice.Items(2).Selected = True
                            End If
                            rblChoice.Enabled = False
                            'btnCancel.Enabled = False
                            btnConfirm.Enabled = False
                            btnClearSelection.Enabled = False
                        End If
                    Next
                    For j = 0 To rblClubs.Items.Count - 1
                        If ds.Tables(0).Rows(i).Item(0) = rblClubs.Items(j).Text Then
                            rblClubs.Items(j).Selected = True
                            rblClubs.Enabled = False
                            lblClub.Text = rblClubs.Items(j).Text
                            If ds.Tables(0).Rows(i).Item(1) = "0" Then
                                rblChoice.Items(0).Selected = True
                            ElseIf ds.Tables(0).Rows(i).Item(1) = "1" Then
                                rblChoice.Items(1).Selected = True
                            Else
                                rblChoice.Items(2).Selected = True
                            End If
                            rblChoice.Enabled = False
                            'btnCancel.Enabled = False
                            btnConfirm.Enabled = False
                            btnClearSelection.Enabled = False
                        End If
                    Next
                    For j = 0 To rblComm.Items.Count - 1
                        If ds.Tables(0).Rows(i).Item(0) = rblComm.Items(j).Text Then
                            rblComm.Items(j).Selected = True
                            rblComm.Enabled = False
                            lblComm.Text = rblComm.Items(j).Text
                            If ds.Tables(0).Rows(i).Item(1) = "0" Then
                                rblChoice.Items(0).Selected = True
                            ElseIf ds.Tables(0).Rows(i).Item(1) = "1" Then
                                rblChoice.Items(1).Selected = True
                            Else
                                rblChoice.Items(2).Selected = True
                            End If
                            rblChoice.Enabled = False
                            'btnCancel.Enabled = False
                            btnConfirm.Enabled = False
                            btnClearSelection.Enabled = False
                        End If
                    Next
                Next
            End If
        End If

    End Sub

    Public Sub GetNextACD_ID(ByVal vACD_ID As String, ByVal vBSU_ID As String)

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "select ACD_ID, ACY_DESCR from dbo.ACADEMICYEAR_D " & _
        " INNER JOIN ACADEMICYEAR_M ON ACY_ID = ACD_ACY_ID" & _
        " where ACD_ACY_ID =( Select (ACD_ACY_ID + 1) FROM ACADEMICYEAR_D WHERE " & _
        "ACD_BSU_ID = '" & vBSU_ID & "' and ACD_ID = " & vACD_ID & ") AND ACD_BSU_ID = '" & vBSU_ID & "' and " & _
        "ACD_CLM_ID = '" & Session("clm") & "'"


        Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        While (dr.Read())
            'lblAcdYear.Text = dr("ACY_DESCR")
            hfACD_ID_NEXT.Value = dr("ACD_ID")
        End While
        dr.Close()
    End Sub

    Sub BindComm(ByVal bsuid As String, ByVal gender As String, ByVal grdIdNext As String)

        rblComm.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
        Dim param(6) As SqlParameter
        param(0) = New SqlParameter("@BSU_ID", bsuid)
        param(1) = New SqlParameter("@STU_GENDER", gender)
        param(2) = New SqlParameter("@STU_NEXT_GRADE", grdIdNext)
        param(3) = New SqlParameter("@TYPE", "5")
        param(4) = New SqlParameter("@ACTION", "LOAD")
        param(5) = New SqlParameter("@ACD_ID", hfACD_ID_NEXT.Value)
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[DBO].[GET_ACE_LEVELS_v2]", param)
        If ds.Tables(0).Rows.Count > 0 Then
            btnClearSelection.Enabled = True
            rblComm.DataSource = ds
            rblComm.DataTextField = "ACM_DESCR"
            rblComm.DataValueField = "ACM_ID"
            rblComm.DataBind()
            hfLevelID.Value = ds.Tables(0).Rows(0).Item(2)

            Dim i As Integer
            Dim j As Integer

            param(0) = New SqlParameter("@BSU_ID", bsuid)
            param(1) = New SqlParameter("@STU_GENDER", gender)
            param(2) = New SqlParameter("@STU_NEXT_GRADE", grdIdNext)
            param(3) = New SqlParameter("@TYPE", "5")
            param(4) = New SqlParameter("@ACTION", "CHECK")
            param(5) = New SqlParameter("@ACD_ID", hfACD_ID_NEXT.Value)
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[DBO].[GET_ACE_LEVELS]", param)
            If ds.Tables(0).Rows.Count > 0 Then

                For i = 0 To rblComm.Items.Count - 1
                    For j = 0 To ds.Tables(0).Rows.Count - 1

                        If rblComm.Items(i).Text = ds.Tables(0).Rows(j).Item(0) Then
                            rblComm.Items(i).Enabled = False
                        End If

                    Next
                Next

            End If
        Else
            ''btnClearSelection.Enabled = False
            trCommL.Visible = False
            trCommR.Visible = False
            trCommSel.Visible = False
        End If

    End Sub


    Sub BindClubs(ByVal bsuid As String, ByVal gender As String, ByVal grdIdNext As String)

        rblClubs.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
        Dim param(6) As SqlParameter
        param(0) = New SqlParameter("@BSU_ID", bsuid)
        param(1) = New SqlParameter("@STU_GENDER", gender)
        param(2) = New SqlParameter("@STU_NEXT_GRADE", grdIdNext)
        param(3) = New SqlParameter("@TYPE", "2")
        param(4) = New SqlParameter("@ACTION", "LOAD")
        param(5) = New SqlParameter("@ACD_ID", hfACD_ID_NEXT.Value)
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[DBO].[GET_ACE_LEVELS_v2]", param)
        If ds.Tables(0).Rows.Count > 0 Then
            btnClearSelection.Enabled = True
            rblClubs.DataSource = ds
            rblClubs.DataTextField = "ACM_DESCR"
            rblClubs.DataValueField = "ACM_ID"
            rblClubs.DataBind()
            hfLevelID.Value = ds.Tables(0).Rows(0).Item(2)

            Dim i As Integer
            Dim j As Integer

            param(0) = New SqlParameter("@BSU_ID", bsuid)
            param(1) = New SqlParameter("@STU_GENDER", gender)
            param(2) = New SqlParameter("@STU_NEXT_GRADE", grdIdNext)
            param(3) = New SqlParameter("@TYPE", "2")
            param(4) = New SqlParameter("@ACTION", "CHECK")
            param(5) = New SqlParameter("@ACD_ID", hfACD_ID_NEXT.Value)
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[DBO].[GET_ACE_LEVELS]", param)
            If ds.Tables(0).Rows.Count > 0 Then

                For i = 0 To rblClubs.Items.Count - 1
                    For j = 0 To ds.Tables(0).Rows.Count - 1

                        If rblClubs.Items(i).Text = ds.Tables(0).Rows(j).Item(0) Then
                            rblClubs.Items(i).Enabled = False
                        End If

                    Next
                Next

            End If
        Else
            ''btnClearSelection.Enabled = False
        End If

    End Sub

    Sub BindGames(ByVal bsuid As String, ByVal gender As String, ByVal grdIdNext As String)

        rblGames.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
        Dim param(6) As SqlParameter
        param(0) = New SqlParameter("@BSU_ID", bsuid)
        param(1) = New SqlParameter("@STU_GENDER", gender)
        param(2) = New SqlParameter("@STU_NEXT_GRADE", grdIdNext)
        param(3) = New SqlParameter("@TYPE", "1")
        param(4) = New SqlParameter("@ACTION", "LOAD")
        param(5) = New SqlParameter("@ACD_ID", hfACD_ID_NEXT.Value)
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[DBO].[GET_ACE_LEVELS_v2]", param)
        If ds.Tables(0).Rows.Count > 0 Then
            rblGames.DataSource = ds
            rblGames.DataTextField = "ACM_DESCR"
            rblGames.DataValueField = "ACM_ID"
            rblGames.DataBind()
            hfLevelID.Value = ds.Tables(0).Rows(0).Item(2)

            Dim i As Integer
            Dim j As Integer

            param(0) = New SqlParameter("@BSU_ID", bsuid)
            param(1) = New SqlParameter("@STU_GENDER", gender)
            param(2) = New SqlParameter("@STU_NEXT_GRADE", grdIdNext)
            param(3) = New SqlParameter("@TYPE", "1")
            param(4) = New SqlParameter("@ACTION", "CHECK")
            param(5) = New SqlParameter("@ACD_ID", hfACD_ID_NEXT.Value)
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[DBO].[GET_ACE_LEVELS]", param)
            If ds.Tables(0).Rows.Count > 0 Then

                For i = 0 To rblGames.Items.Count - 1
                    For j = 0 To ds.Tables(0).Rows.Count - 1

                        If rblGames.Items(i).Text = ds.Tables(0).Rows(j).Item(0) Then
                            rblGames.Items(i).Enabled = False
                        End If

                    Next
                Next

            End If


        End If

    End Sub

    Public Sub GetNextGrade(ByVal vGRD_ID As String)

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim str_query As String = "SELECT GRD_ID FROM GRADE_M  WHERE GRD_DISPLAYORDER = " & _
        " (select (GRD_DISPLAYORDER + 1) from GRADE_M WHERE GRD_ID = '" & vGRD_ID & "')"

        Dim dr As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        While (dr.Read())
            hfGRD_ID_NEXT.Value = dr("GRD_ID")
        End While

    End Sub

    Sub BindGender()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT STU_GENDER FROM STUDENT_M WHERE STU_ID=" + Session("STU_ID")
        hfGender.Value = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
    End Sub


    Private Function isPageExpired() As Boolean

        If Session("TimeStamp") Is Nothing OrElse ViewState("TimeStamp") Is Nothing Then
            Return False
        ElseIf Session("TimeStamp") = ViewState("TimeStamp") Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub btnClearSelection_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClearSelection.Click
        rblClubs.ClearSelection()
        rblComm.ClearSelection()
        rblGames.ClearSelection()
        lblClub.Text = ""
        lblClubAvail.Text = ""
        lblClubDays.Text = ""
        lblComm.Text = ""
        lblCommAvail.Text = ""
        lblCommDays.Text = ""

        lblGame.Text = ""
        lblGameAvail.Text = ""
        lblGameDays.Text = ""
    End Sub

    Protected Sub rblGames_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rblGames.SelectedIndexChanged
        lblGame.Text = rblGames.SelectedItem.Text
        hfGameACLID.Value = rblGames.SelectedItem.Value

        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
        Dim param(9) As SqlParameter
        param(0) = New SqlParameter("@ALD_STU_NEXTGRD_ID", hfGRD_ID_NEXT.Value)
        param(1) = New SqlParameter("@ALD_STU_NEXTACD_ID", hfACD_ID_NEXT.Value)
        param(2) = New SqlParameter("@ALD_STU_ID", Session("STU_ID"))
        param(3) = New SqlParameter("@ALD_ACTIVITIES", rblGames.SelectedItem.Value)
        param(4) = New SqlParameter("@ALD_ACL_ID", hfLevelID.Value)
        param(5) = New SqlParameter("@ACTION", "CHECK")
        param(6) = New SqlParameter("@bOptionalDays", "0")
        param(7) = New SqlParameter("@ALD_BSU_ID", Session("sBsuid"))
        param(8) = New SqlParameter("@ALD_STATUS", "1")

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[DBO].[ACE_ALLOCATIONREQUEST_V2]", param)
        If ds.Tables(0).Rows.Count > 0 Then
            'tdGameAvail.Visible = True
            Dim strDays As String
            Dim strLen As Integer
            strDays = ds.Tables(0).Rows(0).Item(4).ToString
            strLen = strDays.Length
            Dim c As Char = strDays(strLen - 1)
            If c = "," Then
                strDays = strDays.Substring(0, strLen - 1)
            End If
            lblGameAvail.Text = ds.Tables(0).Rows(0).Item(2).ToString
            'lblGameDays.Text = ds.Tables(0).Rows(0).Item(4).ToString
            lblGameDays.Text = strDays
            If ds.Tables(0).Rows(0).Item(3).ToString = "True" Then
                lblGameAvail.ForeColor = Drawing.Color.Red
            Else
                lblGameAvail.ForeColor = Drawing.Color.Green
            End If
        Else

        End If


    End Sub

    Protected Sub rblClubs_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rblClubs.SelectedIndexChanged

        hfClubACLID.Value = rblClubs.SelectedItem.Value
        lblClub.Text = rblClubs.SelectedItem.Text
        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
        Dim param(9) As SqlParameter
        param(0) = New SqlParameter("@ALD_STU_NEXTGRD_ID", hfGRD_ID_NEXT.Value)
        param(1) = New SqlParameter("@ALD_STU_NEXTACD_ID", hfACD_ID_NEXT.Value)
        param(2) = New SqlParameter("@ALD_STU_ID", Session("STU_ID"))
        param(3) = New SqlParameter("@ALD_ACTIVITIES", rblClubs.SelectedItem.Value)
        param(4) = New SqlParameter("@ALD_ACL_ID", hfLevelID.Value)
        param(5) = New SqlParameter("@ACTION", "CHECK")
        param(6) = New SqlParameter("@bOptionalDays", "0")
        param(7) = New SqlParameter("@ALD_BSU_ID", Session("sBsuid"))
        param(8) = New SqlParameter("@ALD_STATUS", "1")

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[DBO].[ACE_ALLOCATIONREQUEST_V2]", param)
        If ds.Tables(0).Rows.Count > 0 Then
            lblClubAvail.Text = ds.Tables(0).Rows(0).Item(2).ToString
            Dim strDays As String
            Dim strLen As Integer
            strDays = ds.Tables(0).Rows(0).Item(4).ToString
            strLen = strDays.Length
            Dim c As Char = strDays(strLen - 1)
            If c = "," Then
                strDays = strDays.Substring(0, strLen - 1)
            End If
            'lblClubDays.Text = ds.Tables(0).Rows(0).Item(4).ToString
            lblClubDays.Text = strDays
            If ds.Tables(0).Rows(0).Item(3).ToString = "True" Then
                lblClubAvail.ForeColor = Drawing.Color.Red

            Else
                lblClubAvail.ForeColor = Drawing.Color.Green
            End If
        End If

    End Sub

    Protected Sub rblComm_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rblComm.SelectedIndexChanged

        hfCommACLID.Value = rblComm.SelectedItem.Value
        lblComm.Text = rblComm.SelectedItem.Text
        Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
        Dim param(9) As SqlParameter
        param(0) = New SqlParameter("@ALD_STU_NEXTGRD_ID", hfGRD_ID_NEXT.Value)
        param(1) = New SqlParameter("@ALD_STU_NEXTACD_ID", hfACD_ID_NEXT.Value)
        param(2) = New SqlParameter("@ALD_STU_ID", Session("STU_ID"))
        param(3) = New SqlParameter("@ALD_ACTIVITIES", rblComm.SelectedItem.Value)
        param(4) = New SqlParameter("@ALD_ACL_ID", hfLevelID.Value)
        param(5) = New SqlParameter("@ACTION", "CHECK")
        param(6) = New SqlParameter("@bOptionalDays", "0")
        param(7) = New SqlParameter("@ALD_BSU_ID", Session("sBsuid"))
        param(8) = New SqlParameter("@ALD_STATUS", "1")

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[DBO].[ACE_ALLOCATIONREQUEST_V2]", param)
        If ds.Tables(0).Rows.Count > 0 Then
            lblCommAvail.Text = ds.Tables(0).Rows(0).Item(2).ToString
            Dim strDays As String
            Dim strLen As Integer
            strDays = ds.Tables(0).Rows(0).Item(4).ToString
            strLen = strDays.Length
            Dim c As Char = strDays(strLen - 1)
            If c = "," Then
                strDays = strDays.Substring(0, strLen - 1)
            End If
            'lblClubDays.Text = ds.Tables(0).Rows(0).Item(4).ToString
            lblCommDays.Text = strDays
            If ds.Tables(0).Rows(0).Item(3).ToString = "True" Then
                lblCommAvail.ForeColor = Drawing.Color.Red

            Else
                lblCommAvail.ForeColor = Drawing.Color.Green
            End If
        End If

    End Sub



    Protected Sub btnConfirm_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnConfirm.Click
        Try

            Dim f1, f2, f3, f4 As Integer
            If rblGames.SelectedItem Is Nothing Then
                'trError.Visible = True
                'lblError.Text = "Please choose a Game"
                f1 = 1
                'Exit Sub
            Else
                f1 = 0
            End If

            'If hfGRD_ID_NEXT.Value = "05" Or hfGRD_ID_NEXT.Value = "06" Or hfGRD_ID_NEXT.Value = "07" Then
            '    If rblClubs.SelectedItem Is Nothing Then
            '        f2 = 1
            '    Else
            '        f2 = 0
            '    End If
            'End If
            f2 = 0
            'If rblComm.SelectedItem Is Nothing Then
            '    'trError.Visible = True
            '    'lblError.Text = "Please choose a Game"
            '    f4 = 1
            '    'Exit Sub
            'Else
            'End If
            f4 = 0 'Asked by Charan to remove
            'If rblChoice.SelectedItem Is Nothing Then
            '    'trError.Visible = True
            '    'lblError.Text = "Please choose an Option"
            '    'Exit Sub
            '    f3 = 1
            'Else
            '    f3 = 0
            '    'trError.Visible = False
            '    'lblError.Text = ""
            'End If
            f3 = 0      'Asked by Charan to remove
            If f1 = 1 Or f2 = 1 Or f3 = 1 Or f4 = 1 Then
                trError.Visible = True
                If f1 = 1 Then
                    lblError.Visible = True
                    lblError.Text = "Please choose a Game"

                Else
                    lblError.Visible = False
                    lblError.Text = ""
                End If
                If f2 = 1 Then
                    lblError1.Visible = True
                    lblError1.Text = "Please choose a Club"
                Else
                    lblError1.Visible = False
                    lblError1.Text = ""
                End If
                If f3 = 1 Then
                    lblError2.Visible = True
                    lblError2.Text = "Please choose an Option"
                Else
                    lblError2.Visible = False
                    lblError2.Text = ""
                End If
                If f4 = 1 Then
                    lblError3.Visible = True
                    lblError3.Text = "Please choose a Communication"
                Else
                    lblError3.Visible = False
                    lblError3.Text = ""
                End If
                Exit Sub
            Else
                trError.Visible = False
                lblError.Text = ""
                lblError1.Text = ""
                lblError2.Text = ""
                lblError3.Text = ""
            End If
            trClubError.Visible = True
            trGameError.Visible = True
            trCommError.Visible = True
            Dim strActivity As String
            If rblGames.Enabled = True Then
                strActivity = rblGames.SelectedItem.Value
            End If

            If rblClubs.Enabled = True Then
                If Not rblClubs.SelectedItem Is Nothing Then
                    strActivity = strActivity + "|" + rblClubs.SelectedItem.Value
                End If
            End If
            If rblComm.Enabled = True Then
                If Not rblComm.SelectedItem Is Nothing Then
                    strActivity = strActivity + "|" + rblComm.SelectedItem.Value
                End If
            End If

            Dim bOption As Integer
            If Not rblChoice.SelectedItem Is Nothing Then
                If rblChoice.SelectedIndex = 0 Then
                    bOption = 0

                ElseIf rblChoice.SelectedIndex = 1 Then
                    bOption = 1
                ElseIf rblChoice.SelectedIndex = 2 Then
                    bOption = 2
                Else
                    bOption = 3
                End If
            End If
            Dim str_conn As String = ConnectionManger.GetOASIS_CCAConnectionString
            Dim param(9) As SqlParameter
            param(0) = New SqlParameter("@ALD_STU_NEXTGRD_ID", hfGRD_ID_NEXT.Value)
            param(1) = New SqlParameter("@ALD_STU_NEXTACD_ID", hfACD_ID_NEXT.Value)
            param(2) = New SqlParameter("@ALD_STU_ID", Session("STU_ID"))
            param(3) = New SqlParameter("@ALD_ACTIVITIES", strActivity)
            param(4) = New SqlParameter("@ALD_ACL_ID", hfLevelID.Value)
            param(5) = New SqlParameter("@ACTION", "INSERT")
            param(6) = New SqlParameter("@bOptionalDays", bOption)
            param(7) = New SqlParameter("@ALD_BSU_ID", Session("sBsuid"))
            param(8) = New SqlParameter("@ALD_STATUS", "1")


            Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[DBO].[ACE_ALLOCATIONREQUEST_V2]", param)
            Dim i As Integer
            Dim flag As Integer
            If ds.Tables(0).Rows.Count > 0 Then
                For i = 0 To ds.Tables(0).Rows.Count - 1
                    If ds.Tables(0).Rows(i).Item(0).ToString = "1" Then
                        trGameError.Visible = True
                        lblGameError.Text = ds.Tables(0).Rows(i).Item(2).ToString
                        If ds.Tables(0).Rows(i).Item(3).ToString = "True" Then
                            rblGames.Enabled = True
                            flag = 1
                        Else
                            rblGames.Enabled = False
                        End If
                    ElseIf ds.Tables(0).Rows(i).Item(0).ToString = "2" Then
                        trClubError.Visible = True
                        lblClubError.Text = ds.Tables(0).Rows(i).Item(2).ToString
                        If ds.Tables(0).Rows(i).Item(3).ToString = "True" Then
                            rblClubs.Enabled = True
                            btnClearSelection.Enabled = True
                            flag = 1
                        Else
                            rblClubs.Enabled = False
                            btnClearSelection.Enabled = False
                        End If
                    Else
                        trCommError.Visible = True
                        lblCommError.Text = ds.Tables(0).Rows(i).Item(2).ToString
                        If ds.Tables(0).Rows(i).Item(3).ToString = "True" Then
                            rblComm.Enabled = True
                            btnClearSelection.Enabled = True
                            flag = 1
                        Else
                            rblComm.Enabled = False
                            btnClearSelection.Enabled = False
                        End If
                    End If
                    If flag = 1 Then
                        btnConfirm.Enabled = True
                    Else
                        btnConfirm.Enabled = False
                    End If
                Next
            End If
            lblCommAvail.Text = ""
            lblClubAvail.Text = ""
            lblGameAvail.Text = ""
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
            trError.Visible = True
            lblError.Text = "Request could not be processed"
        End Try

    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect("~\General\Home.aspx")
    End Sub

End Class


