﻿Imports Microsoft.ApplicationBlocks.Data
Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Net.Mail

Partial Class Others_OthTcRequest
    Inherits System.Web.UI.Page
    Dim encr_decr As New SFENCRYPTION

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ScriptManager.RegisterStartupScript(Me, Page.GetType, "Script", "ApplyClass();", True)
        If Session("username") Is Nothing Then
            Session("Active_tab") = "Home"
            Session("Site_Path") = ""
            Response.Redirect("~\login.aspx")
        ElseIf Session("bPasswdChanged") = "False" Then
            Response.Redirect("~\UpdateInfo\ForcePasswordChange.aspx", False)
        ElseIf Session("bUpdateContactDetails") = "False" Then
            Response.Redirect(Session("ForceUpdate_stud"), False)
        End If

        If Not Page.IsPostBack Then
            hfSave.Value = "0"
            lbChildName.Text = Session("STU_NAME")
            hfTCM_ID.Value = "0"
            BindBsu()
            BindTCReason()
            BindCountry()
            getLast_AttDT()
            GetStudentDetails()
            BindTCType()
            If Session("STU_CURRSTATUS") = "SO" Then  ' This is not relevant anymore as SO cases menu will be supressed from Procedure
                txtLastDt.Enabled = False
                imgLastDt.Visible = False
            End If
            GetData()
            BindExitForm()
            pnlExit.Visible = False
        End If
        '
        If GET_isCRM_SF() = 0 Then
            divOasis_TC.Visible = True
            divSF_TC.Visible = False
        Else
            divOasis_TC.Visible = False
            divSF_TC.Visible = True
            ' Dim s As String = encr_decr.Encrypt("1234")
            hdn_stuno.Value = encr_decr.Encrypt(Session("STU_NO"))
            hdn_bsuid.Value = encr_decr.Encrypt(Session("sBsuid"))

            Dim url As String = "https://gems-edu.secure.force.com/GEMS/GEMS_TCRequestpage?studentID=" + hdn_stuno.Value + "&BSU=" + hdn_bsuid.Value
            ifTc.Attributes.Add("src", url)
        End If
        Select Case Session("STU_BSU_ID")
            Case "121013", "121012", "121014", "121009", "123006", "123004", "111001"
                lbltcMsg.Text = "The last date of attendance is to be entered as 31st March 2016 for students who are leaving at the end of the Academic year 2015-2016."
                lbltcMsg.CssClass = "divinfo"
                lbltcMsg.Width = 760
            Case "133006"
                lbltcMsg.Text = "The last date of attendance is to be entered as 13th March 2016 for students who are leaving at the end of the Academic year 2015-2016."
                lbltcMsg.CssClass = "divinfo"
                lbltcMsg.Width = 760
            Case "131001", "131002"
                lbltcMsg.Text = "Last date of attendance for students who are leaving at the end of Academic year 2015-16  : Grade 10 & Grade 12 -31st March 2016, For other grades – 13th March 2016."
                lbltcMsg.CssClass = "divinfo"
                lbltcMsg.Width = 760
        End Select

        If Session("STU_BSU_ID") = "131001" Then
            lblLastDtText.Text = "Please confirm your wards' last date of attendance.: For Grade 10 & Grade 12 year end TC---31st March 2016, For Other Grades year end TC --- 13th March 2016"
        Else
            lblLastDtText.Text = "Please confirm your wards' last date of attendance"
        End If
        'pnlmain.visible = False
        'pnlExit.Visible = True

    End Sub

#Region "Private Methods"
    Function GET_isCRM_SF() As Integer
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim pParms(3) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", Session("sBsuid"))

        Using reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "get_CRMonSF", pParms)
            While reader.Read
                Return reader("BSU_bIsCRMOnSF")

            End While
        End Using
    End Function
    Function ShowTC() As Boolean
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "EXEC showTCONLINE " _
                                  & " @BSU_ID='" + Session("SBSUID") + "'," _
                                  & " @STU_ID=" + Session("STU_ID") + "," _
                                  & " @ACD_ID=" + Session("STU_ACD_ID").ToString
        Dim bShow As Boolean = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        Return bShow
    End Function
    Sub GetData()

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT TCM_APPLYDATE,TCM_LASTATTDATE,ISNULL(TCM_bPREAPPROVED,'false'),ISNULL(TCM_bCLEARANCE,'false')," _
                               & " CASE WHEN TCM_ISSUEDATE IS NULL THEN 'FALSE' ELSE 'TRUE' END AS ISSUED," _
                               & " ISNULL(TCM_bREADYFORCOLLECTION,'false') TCM_bREADYFORCOLLECTION," _
                               & " CASE WHEN TCM_EXITTYPE='ONLINE' AND TCM_EXITSTATUS='PENDING' THEN 'PENDING' ELSE '' END AS EXITSTATUS,TCM_ID  FROM TCM_M AS A" _
                               & " WHERE TCM_STU_ID=" + Session("STU_ID").ToString + " AND TCM_CANCELDATE IS NULL AND TCM_TCSO='TC'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        If ds.Tables(0).Rows.Count > 0 Then
            gvTC.DataSource = ds
            gvTC.DataBind()
            divbutton.Visible = False
            divBlockTc.Visible = False
            Dim lblStatus As Label

            lblStatus = gvTC.Rows(0).FindControl("lblStatus")

            With ds.Tables(0).Rows(0)
                If .Item(5).ToString.ToLower = "true" Then
                    lblStatus.Text = "TC ready for collection"
                ElseIf .Item(4).ToString.ToLower = "true" Then
                    '' lblStatus.Text = "Ministry attestation awaited"
                    lblStatus.Text = "Awaiting TC issuance"
                ElseIf .Item(3).ToString.ToLower = "true" Then
                    lblStatus.Text = "Clearance Completed"
                ElseIf .Item(2).ToString.ToLower = "true" Then
                    lblStatus.Text = "Approved at school"
                Else
                    lblStatus.Text = "Applied / Approval Pending"
                End If



                hfTCM_ID.Value = .Item(7)


                gvTC.Columns(3).Visible = False


            End With
            divgrid.Visible = True
        Else
            If ShowTC() = True Then
                divgrid.Visible = False
                divBlockTc.Visible = False
            Else
                divbutton.Visible = False
                divgrid.Visible = False

            End If
        End If
    End Sub

    Protected Sub lnkStatus_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        'mdlPopup.TargetControlID = "hfTCM_ID"
        'mdlPopup.Show()

        pnlMain.Visible = False
        pnlExit.Visible = True
    End Sub
    Sub GetStudentDetails()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT ISNULL(STU_PASPRTNAME,''),ISNULL(STS_FFIRSTNAME,'')+' '+ISNULL(STS_FMIDNAME,'')+' '+ISNULL(STS_FLASTNAME,'')," _
                     & " CASE  STU_PRIMARYCONTACT WHEN 'F' THEN  ISNULL(STS_FFIRSTNAME,'')+' '+ISNULL(STS_FMIDNAME,'')+' '+ISNULL(STS_FLASTNAME,'')" _
                     & " WHEN 'M' THEN ISNULL(STS_MFIRSTNAME,'')+' '+ISNULL(STS_MMIDNAME,'')+' '+ISNULL(STS_MLASTNAME,'') WHEN 'G' THEN " _
                     & " ISNULL(STS_FFIRSTNAME,'')+' '+ISNULL(STS_FMIDNAME,'')+' '+ISNULL(STS_FLASTNAME,'') END ," _
                     & " CASE  STU_PRIMARYCONTACT WHEN 'F' THEN STS_FMOBILE WHEN 'M' THEN STS_MMOBILE WHEN 'G' THEN STS_GMOBILE END AS MOBILE," _
                     & " CASE  STU_PRIMARYCONTACT WHEN 'F' THEN STS_FEMAIL WHEN 'M' THEN STS_MEMAIL WHEN 'G' THEN STS_GEMAIL END AS EMAIL," _
                     & " GRM_DISPLAY,SCT_DESCR " _
                     & " FROM STUDENT_M AS A INNER JOIN STUDENT_D ON STU_SIBLING_ID=STS_STU_ID" _
                     & " INNER JOIN GRADE_BSU_M ON STU_GRM_ID=GRM_ID " _
                     & " INNER JOIN SECTION_M ON STU_SCT_ID=SCT_ID" _
                     & " WHERE STU_ID='" + Session("STU_ID").ToString + "'"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        With ds.Tables(0).Rows(0)
            lblName.Text = .Item(0)
            lblFather.Text = .Item(1)
            lblPrimaryName.Text = .Item(2)
            lblPhone.Text = Convert.ToString(.Item(3))
            lblEmail.Text = Convert.ToString(.Item(4))
            lblGrade.Text = Convert.ToString(.Item(5)) + " " + Convert.ToString(.Item(6))
        End With
    End Sub

    Sub BindTCType()
        ddlTransferType.Items.Clear()
        Dim i As Integer
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        Dim ds As DataSet
        Dim li As ListItem
        str_query = "SELECT UPPER(BSU_CITY) FROM BUSINESSUNIT_M WHERE BSU_ID='" + Session("sbsuid") + "'"
        Dim strCity As String = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)

        If Session("SBSUID") <> "800017" Then
            Select Case Session("STU_BSU_ID")
                Case "121013", "121012", "121014", "121009", "123006", "123004", "111001", "133006", "131001", "131002"
                    'For Asian schools grade 12 show only english outside UAE
                    If Session("STU_GRD_ID") = "12" Then
                        str_query = "SELECT [TCT_CODE],[TCT_DESCR]  FROM [OASIS].[dbo].[TC_TFRTYPE_M] WHERE TCT_DESCR " _
                        & " IN('ENGLISH-OUTSIDE UAE') order by [TCT_DESCR]"
                    Else
                        str_query = "SELECT [TCT_CODE],[TCT_DESCR]  FROM [OASIS].[dbo].[TC_TFRTYPE_M] WHERE TCT_DESCR " _
                         & " IN('ARABIC - WITHIN ZONE','ENGLISH-OUTSIDE UAE','ARABIC- OUTSIDE ZONE') order by [TCT_DESCR]"
                    End If
                Case Else
                    str_query = "SELECT [TCT_CODE],[TCT_DESCR]  FROM [OASIS].[dbo].[TC_TFRTYPE_M] WHERE TCT_DESCR " _
                     & " IN('ARABIC - WITHIN ZONE','ENGLISH-OUTSIDE UAE','ARABIC- OUTSIDE ZONE') order by [TCT_DESCR]"
            End Select


            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

            For i = 0 To ds.Tables(0).Rows.Count - 1
                With ds.Tables(0).Rows(i)
                    li = New ListItem
                    If .Item(1).ToString.ToUpper = "ARABIC - WITHIN ZONE" Then

                        'Select Case strCity
                        '    Case "DXB", "DUBAI"
                        '        li.Text = "Dubai"
                        '    Case "SHJ", "SHARJAH"
                        '        li.Text = "Sharjah"
                        '    Case "AUH", "ABU DHABI"
                        '        li.Text = "Abu Dhabi"
                        '    Case "ALN", "AL AIN"
                        '        li.Text = "Al Ain"
                        '    Case "FUJ", "FUJAIRAH"
                        '        li.Text = "Fujairah"
                        '    Case "RAK", "RAS AL KHAIMAH"
                        '        li.Text = "Ras Al Khaimah"
                        '    Case "UAQ", "UMM AL QUWAIN"
                        '        li.Text = "Umm Al Quwain"
                        'End Select
                        li.Text = "A school in the same emirate"
                    ElseIf .Item(1).ToString.ToUpper = "ARABIC- OUTSIDE ZONE" Then
                        li.Text = "A school in a different emirate"
                    ElseIf .Item(1).ToString.ToUpper = "ENGLISH-OUTSIDE UAE" Then
                        li.Text = "A school out of UAE"
                    End If
                    li.Value = .Item(0)
                End With
                ddlTransferType.Items.Add(li)
            Next

        Else
            str_query = "SELECT [TCT_CODE],[TCT_DESCCR_AQABA]  as TCT_DESCR FROM [OASIS].[dbo].[TC_TFRTYPE_M] WHERE TCT_DESCCR_AQABA IS NOT NULL order by [TCT_DESCR]"
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
            ddlTransferType.DataSource = ds
            ddlTransferType.DataTextField = "TCT_DESCR"
            ddlTransferType.DataValueField = "TCT_CODE"
            ddlTransferType.DataBind()
        End If

        li = New ListItem
        li.Text = "Select"
        li.Value = "0"

        ddlTransferType.Items.Insert(0, li)
    End Sub

    Sub getLast_AttDT()
        Try
            Dim dt As String = String.Empty

            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim pParms(9) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@ACD_ID", Session("STU_ACD_ID"))
            pParms(1) = New SqlClient.SqlParameter("@STU_ID", Session("STU_ID"))
            If txtLastDt.Text <> "" Then
                dt = Format(Date.Parse(txtLastDt.Text), "dd/MMM/yyyy")
            End If
            If dt <> "" Then
                '    pParms(2) = New SqlClient.SqlParameter("@LASTDT", DBNull.Value)
                'Else
                pParms(2) = New SqlClient.SqlParameter("@LASTDT", dt)
            End If


            Dim str_date As String = String.Empty
            'Using readerStudent_Detail As SqlDataReader = AccessStudentClass.GetStudent_M_DDetails(ViewState("viewid"), Session("sBsuid"))
            Using readerStudent_ATTDetail As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "[ATT].[GETLAST_ATT_DT_LASTYR_CURR]", pParms)

                If readerStudent_ATTDetail.HasRows = True Then
                    While readerStudent_ATTDetail.Read
                        str_date = Convert.ToString(readerStudent_ATTDetail("AVAIL_DATE"))
                        hfDaysP.Value = Convert.ToString(readerStudent_ATTDetail("tot_prs_days"))
                        hfDaysT.Value = Convert.ToString(readerStudent_ATTDetail("tot_days"))
                    End While
                End If
            End Using

            If txtLastDt.Text.Trim = "" Then
                If IsDate(str_date) = True Then
                    txtLastDt.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((str_date))).Replace("01/Jan/1900", "")
                    hfLastDt.Value = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((str_date))).Replace("01/Jan/1900", "")
                Else
                    txtLastDt.Text = ""
                    hfLastDt.Value = Format(Now.Date, "dd-MMM-yyyy")
                End If
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "TC WithDraw")

        End Try

    End Sub

    Sub getLast_AttDT_SO()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_query As String = "SELECT TCM_DAYST,TCM_DAYSP,TCM_LASTATTDATE FROM TCM_M WHERE TCM_STU_ID=" + Session("STU_ID") + " AND TCM_TCSO='SO' AND TCM_CANCELDATE IS NULL"
            Dim dt As String = String.Empty

            Dim str_date As String = String.Empty
            'Using readerStudent_Detail As SqlDataReader = AccessStudentClass.GetStudent_M_DDetails(ViewState("viewid"), Session("sBsuid"))
            Using readerStudent_ATTDetail As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)

                If readerStudent_ATTDetail.HasRows = True Then
                    While readerStudent_ATTDetail.Read
                        str_date = Convert.ToString(readerStudent_ATTDetail("TCM_LASTATTDATE"))
                        hfDaysP.Value = Convert.ToString(readerStudent_ATTDetail("TCM_DAYSP"))
                        hfDaysT.Value = Convert.ToString(readerStudent_ATTDetail("TCM_DAYST"))
                    End While
                End If
            End Using

            If txtLastDt.Text.Trim = "" Then
                If IsDate(str_date) = True Then
                    txtLastDt.Text = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((str_date))).Replace("01/Jan/1900", "")
                    hfLastDt.Value = String.Format("{0:" & OASISConstants.DateFormat & "}", Convert.ToDateTime((str_date))).Replace("01/Jan/1900", "")
                Else
                    txtLastDt.Text = ""
                    hfLastDt.Value = Format(Now.Date, "dd-MMM-yyyy")
                End If
            End If

        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "TC WithDraw")

        End Try

    End Sub


    Function ccheckWorkingDay() As Integer
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "exec ATT.checkWorkingDay " _
                                & "@STU_ID=" + Session("STU_ID").ToString + "," _
                                & "@ACD_ID=" + Session("STU_ACD_ID") + "," _
                                & "@LASTDT='" + Format(Date.Parse(txtLastDt.Text), "dd/MMM/yyyy") + "'"
        Dim bWorking As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        Return bWorking
    End Function

    Function checkFeedue() As Integer
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "exec checkONEMONTHFEEDUEFORTC " _
                                & "@STU_ID=" + Session("STU_ID").ToString + "," _
                                & "@LASTDATE='" + Format(Date.Parse(txtLastDt.Text), "dd/MMM/yyyy") + "'"
        Dim bDue As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        Return bDue
    End Function
    Sub BindBsu()
        Dim str_query As String = "select bsu_name,BSU_ID from (select BSU_ID+'|GEMS' BSU_ID,bsu_name from businessunit_m where isnull(BSU_bTC_TRANSFER,0)=1 "
        str_query += " union "
        str_query += "SELECT CONVERT(VARCHAR(100),BCC_ID)+'|NONGEMS' ,BCC_NAME FROM BSU_COMPETITORS ) P"
        str_query += " order by bsu_name"
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlTrans_School.DataSource = ds
        ddlTrans_School.DataTextField = "bsu_name"
        ddlTrans_School.DataValueField = "BSU_ID"
        ddlTrans_School.DataBind()
        Dim li As New ListItem
        li.Text = "Others"
        li.Value = "0"
        ddlTrans_School.Items.Insert(0, li)
    End Sub

    Sub BindTCReason()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        str_query = "SELECT [TCR_CODE],[TCR_DESCR]  FROM [OASIS].[dbo].[TC_REASONS_M] where isnull(TCR_bSUPRESSONLINE,0)=0 order by [TCR_ORDER]"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlReason.DataSource = ds
        ddlReason.DataTextField = "TCR_DESCR"
        ddlReason.DataValueField = "TCR_CODE"
        ddlReason.DataBind()

        Dim li As New ListItem
        li.Text = "Select"
        li.Value = "0"
        ddlReason.Items.Insert(0, li)

        'chkExitReason.DataSource = ds
        'chkExitReason.DataTextField = "TCR_DESCR"
        'chkExitReason.DataValueField = "TCR_CODE"
        'chkExitReason.DataBind()


        '   ddlReason.Items.FindByValue(1).Selected = True
    End Sub

    Sub BindCountry()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query = "Select CTY_ID,CASE CTY_DESCR WHEN '-' THEN 'Select' ELSE CTY_DESCR END AS CTY_DESCR from Country_m order by CTY_DESCR"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlCountry.DataSource = ds
        ddlCountry.DataTextField = "CTY_DESCR"
        ddlCountry.DataValueField = "CTY_ID"
        ddlCountry.DataBind()
        ddlCountry.Items.FindByValue("5").Selected = True

    End Sub

    Sub SendEmailNotification(ByVal Comm_BSU As String, ByVal Comm_Type As String, ByVal EmailText As String, ByVal ToEmail As String)
        Dim MAILLIST As String = String.Empty
        Dim errormsg As String = String.Empty
        Dim status As String = String.Empty

        Dim ds As New DataSet

        Dim html As String = ScreenScrapeHtml(Server.MapPath("LeaveTemplate.htm"))
        Dim msg As New MailMessage
        Dim BSC_HOST As String = String.Empty
        Dim BSC_USERNAME As String = String.Empty
        Dim BSC_PASSWORD As String = String.Empty
        Dim BSC_PORT As String = String.Empty
        Dim BSC_FROMEMAIL As String = String.Empty
        Dim Emp_ID As String = String.Empty
        Dim REMARK As String = String.Empty

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT BSC_HOST,BSC_USERNAME,BSC_PASSWORD,BSC_PORT,BSC_FROMEMAIL FROM BSU_COMMUNICATION_M WHERE BSC_TYPE='" + Comm_Type + "' AND BSC_BSU_ID='" + Comm_BSU + "'"

        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        If ds.Tables(0).Rows.Count > 0 Then
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                Try
                    html = html.Replace("@Content", EmailText)
                    BSC_HOST = Convert.ToString(ds.Tables(0).Rows(i)("BSC_HOST"))
                    BSC_USERNAME = Convert.ToString(ds.Tables(0).Rows(i)("BSC_USERNAME"))
                    BSC_PASSWORD = Convert.ToString(ds.Tables(0).Rows(i)("BSC_PASSWORD"))
                    BSC_PORT = Convert.ToString(ds.Tables(0).Rows(i)("BSC_PORT"))
                    BSC_FROMEMAIL = Convert.ToString(ds.Tables(0).Rows(i)("BSC_FROMEMAIL"))
                    status = SendPlainTextEmails(BSC_FROMEMAIL, ToEmail, "Application For TC", html.ToString, BSC_USERNAME, BSC_PASSWORD, BSC_HOST, BSC_PORT, "0", False)
                Catch ex As Exception
                    status = "-1"
                Finally

                End Try
            Next
        End If
    End Sub

    Private Function SendPlainTextEmails(ByVal FromEmailId As String, ByVal ToEmailId As String, ByVal Subject As String, ByVal MailBody As String, ByVal Username As String, ByVal password As String, ByVal Host As String, ByVal Port As Integer, ByVal Templateid As String, ByVal HasAttachments As Boolean) As String
        Dim ReturnValue = ""
        Dim client As New System.Net.Mail.SmtpClient(Host, Port)
        Dim msg As New System.Net.Mail.MailMessage(FromEmailId, ToEmailId)
        Try
            msg.Subject = Subject

            msg.Body = MailBody

            msg.Priority = MailPriority.Normal

            msg.IsBodyHtml = True


            Dim HTMLView As AlternateView = AlternateView.CreateAlternateViewFromString(MailBody, Nothing, "text/html")

            msg.AlternateViews.Add(HTMLView)

            'client.DeliveryMethod = SmtpDeliveryMethod.PickupDirectoryFromIis
            If Username <> "" And password <> "" Then
                Dim creds As New System.Net.NetworkCredential(Username, password)
                client.Credentials = creds
            End If

            Dim s As String = client.Host

            client.Send(msg)

            ReturnValue = "0"
        Catch smptEx As System.Net.Mail.SmtpException
            Dim statuscode As System.Net.Mail.SmtpStatusCode
            statuscode = smptEx.StatusCode
            ReturnValue = "-1"
        Catch ex As Exception
            ReturnValue = "-1"
        End Try

        Return ReturnValue

    End Function

    Private Shared Function ScreenScrapeHtml(ByVal url As String) As String
        Dim objRequest As System.Net.WebRequest = System.Net.HttpWebRequest.Create(url)
        Dim sr As New StreamReader(objRequest.GetResponse().GetResponseStream())
        Dim result As String = sr.ReadToEnd()
        sr.Close()
        Return result
    End Function 'ScreenScrapeHtml

    Function GetEmpDetails(ByVal designation As String) As String
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String

        If designation = "FORMTUTOR" Then
            str_query = "SELECT ISNULL(EMP_FNAME,'')+' '+ISNULL(EMP_MNAME,'')+' '+ISNULL(EMP_LNAME,''),EMD_EMAIL FROM " _
                              & " EMPLOYEE_M AS A INNER JOIN SECTION_M AS B ON A.EMP_ID=B.SCT_EMP_ID " _
                              & " INNER JOIN EMPLOYEE_D AS C ON A.EMP_ID=C.EMD_EMP_ID" _
                              & " WHERE SCT_ID='" + Session("STU_SCT_ID") + "'"
        Else
            str_query = "SELECT ISNULL(EMP_FNAME,'')+' '+ISNULL(EMP_MNAME,'')+' '+ISNULL(EMP_LNAME,''),EMD_EMAIL FROM " _
                              & " EMPLOYEE_M AS A INNER JOIN EMPDESIGNATION_M AS B ON A.EMP_DES_ID=B.DES_ID" _
                              & " INNER JOIN EMPLOYEE_D AS C ON A.EMP_ID=C.EMD_EMP_ID" _
                              & "  WHERE EMP_BSU_ID='" + Session("SBSUID") + "'  AND DES_DESCR='" + designation + "'"
        End If

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        If ds.Tables(0).Rows.Count > 0 Then
            Return ds.Tables(0).Rows(0).Item(0) + "|" + ds.Tables(0).Rows(0).Item(1)
        Else
            Return ""
        End If
    End Function

    Function GetLeaveDate(ByVal lastDate As String) As String
        Dim strLeaveDate As String
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT ACD_CURRENT,ACD_ENDDT FROM ACADEMICYEAR_D WHERE ACD_ID=" + Session("STU_ACD_ID").ToString

        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        'if the student academic year is within the current academic year then 
        'if the last date is >= current date then leave date =last att date
        'else leavedate=currrent date

        With ds.Tables(0).Rows(0)
            If .Item(0) = True Then
                If CInt(DateDiff(DateInterval.Day, Now.Date, Date.Parse(txtLastDt.Text))) > 0 Then
                    strLeaveDate = Format(Date.Parse(txtLastDt.Text), "yyyy-MM-dd")
                Else
                    strLeaveDate = Format(Now.Date, "yyyy-MM-dd")
                End If
            Else
                strLeaveDate = Format(.Item(1), "yyyy-MM-dd")
            End If
        End With

        Return strLeaveDate
    End Function

    Function SaveData() As Boolean

        Dim bError As Boolean = True
        Dim bSave As Boolean = False
        Dim lintTCMID As Integer
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim i As Integer
        Dim strReason As String = ddlReason.SelectedValue.ToString
        'For i = 0 To ddlReason.Items.Count - 1
        '    If ddlReason.Items(i).Selected = True Then
        '        If strReason <> "" Then
        '            strReason += "|"
        '        End If
        '        strReason += ddlReason.Items(i).Value
        '    End If
        'Next

        Dim strExitType As String

        If rdOnline.Checked = True Then
            strExitType = "ONLINE"
        ElseIf rdPrinicipal.Checked = True Then
            strExitType = "PRINCIPAL"
        ElseIf rdCorporate.Checked = True Then
            strExitType = "CORPORATE"

        End If

        Dim strGems As String = ""
        Dim strTfrSchool As String = ""
        Dim str As String()
        If ddlTrans_School.SelectedValue <> "0" Then
            str = ddlTrans_School.SelectedValue.Split("|")
            If str(1) = "GEMS" Then
                strGems = "'" + str(0) + "'"
                strTfrSchool = "NULL"
            Else
                strGems = "NULL"
                strTfrSchool = "'" + ddlTrans_School.SelectedItem.Text + "'"
            End If
        Else
            strGems = "NULL"
            strTfrSchool = "'" + txtOthers.Text.Replace("'", "''") + "'"
        End If


        Dim strLeaveDate As String = GetLeaveDate(txtLastDt.Text)
        lintTCMID = hfTCM_ID.Value
        Dim str_query As String = " EXEC saveTCENTRY " _
                                 & "@TCM_ID=" + hfTCM_ID.Value + "," _
                                 & "@TCM_BSU_ID='" + Session("SBSUID") + "'," _
                                 & "@TCM_DOCDATE='" + Format(Now, "yyyy-MM-dd") + "'," _
                                 & "@TCM_ACD_ID='" + Session("STU_ACD_ID").ToString + "'," _
                                 & "@TCM_STU_ID=" + Session("STU_ID").ToString + "," _
                                 & "@TCM_REFNO=''," _
                                 & "@TCM_APPLYDATE='" + Format(Now, "yyyy-MM-dd") + "'," _
                                 & "@TCM_LEAVEDATE='" + strLeaveDate + "'," _
                                 & "@TCM_LASTATTDATE='" + Format(Date.Parse(txtLastDt.Text), "yyyy-MM-dd") + "'," _
                                 & "@TCM_REASON='" + strReason + "'," _
                                 & "@TCM_DAYSP=" + Val(hfDaysP.Value).ToString + "," _
                                 & "@TCM_DAYST=" + Val(hfDaysT.Value).ToString + "," _
                                 & "@TCM_TCTYPE='" + ddlTransferType.SelectedValue.ToString + "'," _
                                 & "@TCM_SUBJECTS=''," _
                                 & "@TCM_RESULT=''," _
                                 & "@TCM_REMARKS=''," _
                                 & "@TCM_bTFRGEMS='" + IIf(strGems = "NULL", "false", "true") + "'," _
                                 & "@TCM_GEMS_UNIT=" + strGems + "," _
                                 & "@TCM_TFRSCHOOL=" + strTfrSchool + "," _
                                 & "@TCM_TFRCOUNTRY='" + ddlCountry.SelectedValue.ToString + "'," _
                                 & "@TCM_TFRGRADE=''," _
                                 & "@TCM_TFRZONE=N'" + ddlTransferType.SelectedItem.Text + "'," _
                                 & "@TCM_CONDUCT=''," _
                                 & "@TCM_FATHERNAME='" + lblFather.Text.Replace("'", "''") + "'," _
                                 & "@TCM_ENTRY_TYPE='PARENT'," _
                                 & "@TCM_FEEDBACK='" + txtExitFeedback.Text.Replace("'", "''") + "'," _
                                 & "@TCM_EXITTYPE='" + strExitType + "'," _
                                 & "@TCM_OTHERREASON='" + txtOtherReason.Text.Replace("'", "''") + "'"

        Dim transaction As SqlTransaction
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            transaction = conn.BeginTransaction("SampleTransaction")
            Try

                hfTCM_ID.Value = SqlHelper.ExecuteScalar(transaction, CommandType.Text, str_query)
                '    UtilityObj.InsertAuditdetails(transaction, "edit", "STUDENT_M", "STU_ID", "STU_ID", "STU_ID=" + Session("STU_ID"))
                '  UtilityObj.InsertAuditdetails(transaction, "add", "TCM_M", "TCM_ID", "TCM_ID", "TCM_ID=" + hfTCM_ID.Value.ToString, "TCM_M_PARENT")
                transaction.Commit()
                bSave = True
                'Return True
            Catch myex As ArgumentException
                transaction.Rollback()
                bError = False
                lbltcMsg.Text = myex.Message
                lbltcMsg.CssClass = "diverror"
                lbltcMsg.Width = 760
                UtilityObj.Errorlog(myex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                ' Return False
            Catch ex As Exception
                transaction.Rollback()
                bError = False
                lbltcMsg.Text = ex.Message ' "Request could not be processed."
                lbltcMsg.CssClass = "diverror"
                lbltcMsg.Width = 760
                UtilityObj.Errorlog(ex.Message, System.Reflection.MethodBase.GetCurrentMethod().Name)
                ' Return False
            End Try
        End Using

        If bSave = True Then
            str_query = "EXEC INSERTEMAILS_TC " _
                   & " @BSU_ID ='" + Session("SBSUID") + "'," _
                   & " @STU_ID ='" + Session("STU_ID").ToString + "'," _
                   & " @EMAILTYPE='TC_APPLICATION'," _
                   & " @SENDER='SCHOOL'"

            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)

            str_query = "EXEC INSERTEMAILS_TC " _
                 & " @BSU_ID ='" + Session("SBSUID") + "'," _
                 & " @STU_ID ='" + Session("STU_ID").ToString + "'," _
                 & " @EMAILTYPE='TC_APPLICATION_TRANSPORT'," _
                 & " @SENDER='TRANSPORT'"

            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)


            If rdPrinicipal.Checked = True Or rdCorporate.Checked = True Then
                str_query = "EXEC INSERTEMAILS_TC " _
                & " @BSU_ID ='" + Session("SBSUID") + "'," _
                & " @STU_ID ='" + Session("STU_ID").ToString + "'," _
                & " @EMAILTYPE='TC_PARENT_SUBMIT'," _
                & " @SENDER='SCHOOL'"
                SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
            End If
            'End If

            'If bError = True Then
            Dim fee As String = GetTcFee().ToString.Replace(".00", "")

            '  If fee = "0" Or fee = "" Or fee = "0.00" Then
            lbltcMsg.Text = "Application for TC is sucessfully saved."
            '   Else
            ' lbltcMsg.Text = "Application for TC is sucessfully saved.Please pay an amount of " & IIf(Session("BSU_CURRENCY") Is Nothing, "", Session("BSU_CURRENCY")) & " " + fee + " towards TC"
            '  End If

            lbltcMsg.CssClass = "divvalid"
            lbltcMsg.Width = 760
        End If

        If bSave = True Then
            btnSave.Enabled = False
        End If
        Return bSave
    End Function



    Sub SaveExitForm()
        Try
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim str_query As String
            Dim i As Integer
            Dim rdSA As RadioButton
            Dim rdA As RadioButton
            Dim rdDA As RadioButton
            Dim rdSDA As RadioButton
            Dim rdNA As RadioButton
            Dim lblTqsId As Label
            Dim sAnswer As String
            If rdOnline.Checked = True Then
                For i = 0 To dlExitQ.Items.Count - 1
                    With dlExitQ.Items(i)
                        sAnswer = ""

                        rdSA = .FindControl("rdSA")
                        rdA = .FindControl("rdA")
                        rdDA = .FindControl("rdDA")
                        rdSDA = .FindControl("rdSDA")
                        rdNA = .FindControl("rdNA")
                        lblTqsId = .FindControl("lblTqsId")

                        If rdSA.Checked = True Then
                            sAnswer = rdSA.Text
                        End If

                        If rdA.Checked = True Then
                            sAnswer = rdA.Text
                        End If

                        If rdDA.Checked = True Then
                            sAnswer = rdDA.Text
                        End If

                        If rdSDA.Checked = True Then
                            sAnswer = rdSDA.Text
                        End If

                        If rdNA.Checked = True Then
                            sAnswer = rdNA.Text
                        End If
                        str_query = "saveTCM_STUDENT_EXITQUESTIONS " _
                                & " @STU_ID=" + Session("STU_ID").ToString + "," _
                                & " @TCM_ID=" + hfTCM_ID.Value + "," _
                                & " @TQS_ID=" + lblTqsId.Text + "," _
                                & " @ANSWER='" + sAnswer + "'"
                        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
                    End With
                Next
            End If
            'mdlPopup.Show()
            str_query = " exec updateTCFEEDBACK " _
                                 & "@STU_ID =" + Session("STU_ID").ToString + "," _
                                 & "@TCM_ID=" + hfTCM_ID.Value + "," _
                                 & "@FEEDBACK='" + txtExitFeedback.Text.Replace("'", "''") + "'," _
                                 & "@TYPE ='PARENT'"
            SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)

            If rdOnline.Checked = True Then
                str_query = "EXEC INSERTEMAILS_TC " _
                      & " @BSU_ID ='" + Session("SBSUID") + "'," _
                      & " @STU_ID ='" + Session("STU_ID").ToString + "'," _
                      & " @EMAILTYPE='TC_PARENT_EXITINTERVIEW'," _
                      & " @SENDER='SCHOOL'"
                SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
            End If
        Catch ex As Exception
        Finally
            lbltcMsg.CssClass = "divvalid"
            lbltcMsg.Text = "Your TC Application is successfully saved.Thank you for your time and valued feedback."
            lbltcMsg.Width = 760
            btnSave.Enabled = False
            btnSave1.Enabled = False
            'mdlPopup.Show()
            'End If
        End Try
    End Sub

    Function checkExitFormCompleted() As Boolean
        Dim i As Integer
        Dim rdSA As RadioButton
        Dim rdA As RadioButton
        Dim rdDA As RadioButton
        Dim rdSDA As RadioButton
        Dim rdNA As RadioButton
        Dim lblTqsId As Label
        Dim sAnswer As String
        For i = 0 To dlExitQ.Items.Count - 1
            With dlExitQ.Items(i)
                sAnswer = ""

                rdSA = .FindControl("rdSA")
                rdA = .FindControl("rdA")
                rdDA = .FindControl("rdDA")
                rdSDA = .FindControl("rdSDA")
                rdNA = .FindControl("rdNA")
                lblTqsId = .FindControl("lblTqsId")

                If rdSA.Checked = True Then
                    sAnswer = rdSA.Text
                End If

                If rdA.Checked = True Then
                    sAnswer = rdA.Text
                End If

                If rdDA.Checked = True Then
                    sAnswer = rdDA.Text
                End If

                If rdSDA.Checked = True Then
                    sAnswer = rdSDA.Text
                End If

                If rdNA.Checked = True Then
                    sAnswer = rdNA.Text
                End If

                If sAnswer = "" Then
                    Return False
                End If
            End With
        Next
        Return True
    End Function

    Function GetTcFee() As Double
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "EXEC getStudentFeePending " _
                                & "@STU_ID =" + Session("STU_ID").ToString + "," _
                                & "@TYPE ='TC'"
        Dim fee As Double = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        Return fee
    End Function

    Function ValidateContols() As Boolean

        If rdOnline.Checked = False And rdCorporate.Checked = False And rdPrinicipal.Checked = False Then
            lbltcMsg.Text = "Please choose one option for Exit Interview"
            lbltcMsg.CssClass = "diverror"
            lbltcMsg.Width = 760
            'mdlPopup.Show()
            Return False
        End If

        If txtLastDt.Text = "" Then
            lbltcMsg.Text = "Please enter the last date of attendance"
            lbltcMsg.CssClass = "diverror"
            lbltcMsg.Width = 760
            'mdlPopup.Show()
            Return False
        End If

        If Session("sbsuid") = "131001" Then
            If checkFeedue() = 1 Then
                lbltcMsg.Text = "Please clear all the oustanding fees before applying the TC"
                lbltcMsg.CssClass = "diverror"
                lbltcMsg.Width = 760
                'mdlPopup.Show()
                ' Return False
            End If
        End If

        If ccheckWorkingDay() = 0 Then
            lbltcMsg.Text = "Last date of attendance shoud be a working day"
            lbltcMsg.CssClass = "diverror"
            lbltcMsg.Width = 760
            'mdlPopup.Show()
            Return False
        End If

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim pParms(3) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@TCM_STU_ID", Session("STU_ID"))
        pParms(1) = New SqlClient.SqlParameter("@TCM_LASTATTDATE", Format(Date.Parse(txtLastDt.Text), "yyyy-MM-dd"))
        Using reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "TCSO_LastAttDate_Validate", pParms)
            While reader.Read
                Dim lstrRet = Convert.ToString(reader("Ret"))
                If lstrRet = "0" Then
                    lbltcMsg.Text = "Last date of attendance shoud be a date higher than or equal to  " + hfLastDt.Value
                    lbltcMsg.CssClass = "diverror"
                    lbltcMsg.Width = 760
                    'mdlPopup.Show()
                    Return False
                ElseIf lstrRet = "2" Then
                    lbltcMsg.Text = "Last date of attendance shoud be a date higher than the Date of Join"
                    lbltcMsg.CssClass = "diverror"
                    lbltcMsg.Width = 760
                    'mdlPopup.Show()
                    Return False
                End If
            End While
        End Using

        If ddlTrans_School.SelectedItem.Text.ToLower = "others" And txtOthers.Text = "" Then
            lbltcMsg.Text = "Please select the name of the school to be transferred to "
            lbltcMsg.CssClass = "diverror"
            lbltcMsg.Width = 760
            'mdlPopup.Show()
            Return False
        End If

        If ddlTransferType.SelectedValue = "0" Then
            lbltcMsg.Text = "Please select transferring to"
            lbltcMsg.CssClass = "diverror"
            lbltcMsg.Width = 760
            'mdlPopup.Show()
            Return False
        End If

        If ddlCountry.SelectedItem.Text = "Select" Then
            lbltcMsg.Text = "Please select the school located in"
            lbltcMsg.CssClass = "diverror"
            lbltcMsg.Width = 760
            'mdlPopup.Show()
            Return False
        End If



        'Dim bReason As Boolean = False
        'Dim i As Integer
        'For i = 0 To ddlReason.Items.Count - 1
        '    If ddlReason.Items(i).Selected = True Then
        '        bReason = True
        '        Exit For
        '    End If
        'Next

        If ddlReason.SelectedValue.ToString = "0" Then
            lbltcMsg.Text = "Please select the reason for withdrawal"
            lbltcMsg.CssClass = "diverror"
            lbltcMsg.Width = 760
            'mdlPopup.Show()
            Return False
        End If

        If txtExitFeedback.Text = "" Then
            lbltcMsg.Text = "Please enter your feedback"
            lbltcMsg.CssClass = "diverror"
            lbltcMsg.Width = 760
            'mdlPopup.Show()
            Return False
        End If

        If rdOnline.Checked = True Then
            If checkExitFormCompleted() = False Then
                lblExitMsg.Text = "Please complete all the exit interview fields"
                lblExitMsg.CssClass = "diverror"
                lblExitMsg.Width = 760

                lbltcMsg.Text = "Please complete all the exit interview fields"
                lbltcMsg.CssClass = "diverror"
                lbltcMsg.Width = 760

                'mdlPopup.Show()
                Return False
            End If
        End If


        Return True
    End Function
#End Region


    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        lbltcMsg.Text = ""
        lbltcMsg.CssClass = ""
        lblExitMsg.Text = ""
        lblExitMsg.CssClass = ""

        If ValidateContols() = True Then
            Dim bSave As Boolean = SaveData()
            'mdlPopup.Show()
            If bSave = True And hfTCM_ID.Value <> "" Then
                SaveExitForm()
            End If
        End If
    End Sub

    Protected Sub btnSave1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave1.Click
        lbltcMsg.Text = ""
        lbltcMsg.CssClass = ""
        lblExitMsg.Text = ""
        lblExitMsg.CssClass = ""

        If ValidateContols() = True Then
            Dim bSave As Boolean = SaveData()
            'mdlPopup.Show()
            If bSave = True Then
                SaveExitForm()
            End If
        End If
    End Sub

    Sub BindExitForm()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "select dbo.fn_GetPrincipal ('" + Session("STU_BSU_ID") + "') "
        lblPrincipal.InnerText = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)

        Dim ds As DataSet

        'Dim i As Integer

        'str_query = "SELECT STR_REASON_ID FROM TCM_STU_REASON_S WHERE STR_STU_ID=" + Session("STU_ID").ToString _
        '        & " AND STR_TCM_ID=" + hfTCM_ID.Value
        'Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        'For i = 0 To ds.Tables(0).Rows.Count - 1
        '    If Not chkExitReason.Items.FindByValue(ds.Tables(0).Rows(i).Item(0)) Is Nothing Then
        '        chkExitReason.Items.FindByValue(ds.Tables(0).Rows(i).Item(0)).Selected = True
        '    End If
        'Next

        str_query = "SELECT TQS_ID,TQS_SLNO,TQS_QUESTION FROM TC_EXITQUESTIONS_M ORDER BY TQS_SLNO"
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        dlExitQ.DataSource = ds
        dlExitQ.DataBind()
    End Sub


    'Protected Sub btnCloseedit_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCloseedit.Click
    '    GetData()
    'End Sub

    'Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
    '    GetData()
    'End Sub

    Protected Sub btnAccept_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAccept.Click
        If ccheckWorkingDay() = 0 Then
            lbltcMsg.Text = "Last date of attendance shoud be a working day"
            lbltcMsg.CssClass = "diverror"
            lbltcMsg.Width = 760
            'mdlPopup2.Show()
            Exit Sub
        End If

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim pParms(3) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@TCM_STU_ID", Session("STU_ID"))
        pParms(1) = New SqlClient.SqlParameter("@TCM_LASTATTDATE", Format(Date.Parse(txtLastDt.Text), "yyyy-MM-dd"))
        Using reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "TCSO_LastAttDate_Validate", pParms)
            While reader.Read
                Dim lstrRet = Convert.ToString(reader("Ret"))
                If lstrRet = "0" Then
                    lbltcMsg.Text = "Last date of attendance shoud be a date higher than or equal to  " + hfLastDt.Value
                    lbltcMsg.CssClass = "diverror"
                    lbltcMsg.Width = 760
                    'mdlPopup2.Show()
                    Exit Sub
                End If
            End While
        End Using

    End Sub

    Protected Sub btnCancelConfirm_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelConfirm.Click
        'mdlPopup.Show()
    End Sub



    Protected Sub rdCorporate_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdCorporate.CheckedChanged
        If rdCorporate.Checked = True Then
            pnlExit.Visible = False
            'mdlPopup.Show()
        End If
    End Sub

    Protected Sub rdOnline_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdOnline.CheckedChanged
        If rdOnline.Checked = True Then
            pnlExit.Visible = True
            'mdlPopup.Show()
        End If
    End Sub

    Protected Sub rdPrinicipal_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdPrinicipal.CheckedChanged
        If rdPrinicipal.Checked = True Then
            pnlExit.Visible = False
            'mdlPopup.Show()
        End If
    End Sub

    Protected Sub ddlTrans_School_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTrans_School.SelectedIndexChanged
        If ddlTrans_School.SelectedItem.Text.ToLower = "others" Then
            txtOthers.Enabled = True
            'mdlPopup.Show()
        Else
            txtOthers.Text = ""
            txtOthers.Enabled = False
            'mdlPopup.Show()
        End If
    End Sub
    Protected Sub btnTC_Click(sender As Object, e As EventArgs)
        divbutton.Visible = False
        divTC.Visible = True
    End Sub
End Class
