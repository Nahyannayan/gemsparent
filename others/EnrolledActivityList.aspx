﻿<%@ Page Language="VB" AutoEventWireup="false"  CodeFile="EnrolledActivityList.aspx.vb" Inherits="Others_EnrolledActivityList" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head runat="server">
<title ></title>   
     <script type="text/javascript" src="/GEMSPARENTLITEV2/Scripts/jquery-1.8.2.min.js"></script>
    <script type="text/javascript" src="/GEMSPARENTLITEV2/Scripts/jquery-ui-1.10.2.js"></script>  
    <script type="text/javascript" src="/GEMSPARENTLITEV2/Scripts/Fancybox/jquery.fancybox.pack.js?1=2"></script>
    <script type="text/javascript" src="/GEMSPARENTLITEV2/Scripts/Fancybox/jquery.fancybox.js?1=2"></script>
    <link type="text/css" href="/GEMSPARENTLITEV2/Scripts/Fancybox/jquery.fancybox.css?1=2" rel="stylesheet" />
    <link href="/GEMSPARENTLITEV2/css/Popup.css" rel="stylesheet" />
    <link href="/GEMSPARENTLITEV2/css/SiteStyle.css?1=2" rel="stylesheet" type="text/css" />
    <link href="/GEMSPARENTLITEV2/css/MenuStyle.css?1=2" rel="stylesheet" type="text/css" />
    <link href="/GEMSPARENTLITEV2/css/StyleBox.css?1=2" rel="stylesheet" type="text/css" />
    <link href="/GEMSPARENTLITEV2/css/style.css" rel="stylesheet" type="text/css" />
    <link href="/GEMSPARENTLITEV2/css/bootstrap.css" rel="stylesheet" type="text/css" />
</head>
<body >
    <form id="enrolledlist" runat="server">
        
    <div class ="popup-wrap">

    <asp:Label ID="lblError" runat="server" cssClass="error" Visible ="false"></asp:Label>
        <div class="title-box">
      <h3>Enrolled Activities
       <span class="profile-right"><asp:label ID="lbChildName" runat="server" cssClass="lblChildNamecss" style="display:none;"></asp:label></span>
           </h3>
       </div>
     
    <asp:GridView  ID="gvEnrolledList" runat="server" class="table table-striped table-bordered table-responsive text-left my-orders-table" EmptyDataText="No enrolled activity found"
                                AutoGenerateColumns="True" AllowPaging ="true" Width ="100%" PageSize="10" OnPageIndexChanging ="gvEnrolledList_PageIndexChanging" OnRowDataBound="gvEnrolledList_RowDataBound">
       
        </asp:GridView>
    </div>
    </form>
</body>
</html>


