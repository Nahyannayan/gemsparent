﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ParentMaster.master" AutoEventWireup="false" CodeFile="CovidReliefForm.aspx.vb" Inherits="others_CovidReliefForm" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphParent" Runat="Server">
     <script type="text/javascript" src="../Scripts/iframeResizer.min.js"></script>
     <asp:HiddenField ID="hdn_stuno" runat="server" />
    <div class="content margin-top30 margin-bottom60">
        <div class="container">
            <div class="row">

                <!-- Posts Block -->
                <div class="posts-block col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <a href="../Covid/CovidReliefForm_Det.aspx">Test</a>
                    <div id="divCovid" runat="server" >
                        <iframe id="ifCovid" runat="server" width="100%" frameborder="0"  style="height:2700px;" ></iframe>
                    </div>
                </div>
                <!-- /Posts Block -->
            </div>
        </div>
    </div>
    <script type="text/javascript">
        document.getElementById("<%=ifCovid.ClientID%>").onload = resizeFrame;
        function resizeFrame() {
            iFrameResize({ log: false }, document.getElementById("<%=ifCovid.ClientID%>"));
        }
    </script>
</asp:Content>

