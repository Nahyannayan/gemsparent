﻿<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/ParentMaster.master" CodeFile="ActivityRequest.aspx.vb" Inherits="Others_ActivityRequest" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphParent" runat="Server">
    <link href="../css/Ribbon.css" rel="stylesheet" />
    <style>
        input[type="radio"] {
            vertical-align: top !important
        }

        label {
            font-weight: normal !important;
        }

        .divbodyO_AR {
            /*font-size:12px;*/
        }

        .divnoteO_AR {
            text-align: center;
        }

        .linkbtnPositionO_AR {
            float: right;
            color: blue;
        }

        .OverFlowO_AR {
            overflow: auto;
        }

        .LabelEventNameO_AR {
            /*position: absolute;*/
            color: #39A8C5;
            font-weight: bold;
            font-size:large ;
            padding-top: 15px;
        }

        .DisplayNoneO_AR {
            display: none;
        }

        .hrStyleO_AR {
            border: 1px solid lightblue;
        }

        .lblShowO_AR {
            background-color: lightBlue;
            border: 2px solid #e7e7e7;
            color: white;
            float: right;
            padding: 10px 22px;
            text-align: center;
            border-radius: 6px;
            text-decoration: none;
            display: inline-block;
            /*font-size: 12px;*/
            margin: 4px 2px;
            -webkit-transition-duration: 0.4s;
            transition-duration: 0.4s;
            cursor: pointer;
        }
        /*.lblShowO_AR:hover {background-color: #e7e7e7; color:black;}*/
        .lblShow1O_AR {
            border: 1px solid #e7e7e7;
            padding: 5px 5px 0 5px;
            border-radius: 6px;
            margin: 10px 2px;
            background-color: #f9f9f9;
        }

        .lblShow1O_AR-inner {
            background-color: #ffffff;
            border: 1px solid #e7e7e7;
            padding: 10px;
            border-radius: 8px;
            margin: 0 2px 10px 2px;
        }

        .LabelEventStatus {
            color: #39A8C5;
            font-style: italic;
        }

        .badge {
            background-color: orange !important;
            /*transform: rotate(45deg) !important;*/
            /*-webkit-transform: rotate(45deg) !important;*/
            padding: 8px 17px !important;
            /*box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);*/
        }


        .sched-txt {
            font-size: 16px;
            font-weight: bold;
        }

        .act-desc-title {
            font-size: 14px;
            font-weight: bold;
        }

        .act-title {
            font-size: 2rem;
            font-weight: bold;
        }

        .act-error {
            /*color: #2b88a0;*/
            font-style: italic;
            /*background-color: #f9f9e1;*/
            display: inline;
            padding: 2px 24px;
            margin-right: 4px;
            /*font-weight: 600;*/
        }

         .textcntO_AD {
              width :40%; 
              border-radius :8px;              
          }  

        /*.lblShow1O_AR:hover {background-color: lightblue; color:white !important;}*/

               

        .toggle-msg {
            position: absolute;
            width: 60%;
            background-color: rgb(237,237,237);
            color: #333;
            padding: 2px;
            z-index: 1000;
            border-radius: 8px;
            box-shadow: rgba(0, 0, 0, 0.3) 0px 5px 9px;
            border-color: #efefef;
        }
        .toggle-padding {
            padding:6px 10px;
        }
    </style>
    <script >
        function fnMouseOver(div) {            
            div.style.display = "block";
        }

        function fnMouseOut(div) {           
            div.style.display = "none";
        }
    </script>
    <div class="content margin-top30 margin-bottom60">
        <div class="container">
            <div class="row">
                 <asp:HiddenField ID="hidclientidbtn" Value="" runat="server" />
                <!-- Posts Block -->
                <div class="posts-block col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="my-account">

                        <div id="divbody" class="divbodyO_AR">
                            <div id="divNote" runat="server" class="divnoteO_AR">
                                <img src="/Images/activity1.png" alt="No Image" />
                                <br />
                                <asp:LinkButton ID="btnEnrolledLst" runat="server" Visible="true" Text="Click Here to see enrolled activities & status" CssClass="btn btn-info rightEnd pull-right" />
                            </div>

                            <div class="clearfix"></div>

                            <div class="mainheading" style="display: none;">
                                <div class="left">Enroll In Activity/Activities</div>
                                <div class="right">
                                    <asp:Label ID="lbChildName" runat="server" CssClass="lblChildNameCss"></asp:Label>
                                </div>
                                <br />
                            </div>
                            <div class="OverFlowO_AR">
                                <div class="divnoteO_AR" >
                                    <asp:Label ID="lblError" runat="server" Text="" />
                                </div>
                                <asp:Repeater ID="rptrGroup" OnItemDataBound="rptrGroup_ItemDataBound" runat="server">
                                    <ItemTemplate>                                      
                                        <%--  <asp:RadioButton ID="radSelectAct" runat="server" GroupName="SelectControl" />
                                        <asp:CheckBox ID="chkSelectAct" runat="server"  />--%>
                                        <asp:HiddenField ID="hidAPG_ID" runat="server" Value='<%# Eval("APG_ID")%>' />
                                        <div class="lblShow1O_AR" id="divRepeater" runat="server">
                                            <div class="container-fluid">
                                                <div class="row">
                                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                                        <div class="act-title">
                                                            <asp:Label ID="lblHeaderrptr" runat="server" Text='<%# Eval("APG_NAME")%>'></asp:Label> 
                                                            <asp:Label ID="lblMaximumLimit" runat="server" Text="" Visible="false" style="float:right;font-size:small !important;"></asp:Label>                                                         
                                                        </div>                                                         
                                                    </div>
                                                    <div class="col-lg-12 col-md-12 col-sm-12 text-right">
                                                        <div  id="divLimitFullText"  style="color: burlywood;font-style: italic;">
                                                            <asp:Label ID="lbllimitFulltxt" runat="server" Text="" />
                                                            <asp:HiddenField ID="hidMaxlimit" runat="server" Value='<%# Eval("APG_MAXLIMIT")%>' />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                             
                                            <asp:Repeater ID="Repeater1" OnItemDataBound="Repeater1_ItemDataBound" runat="server">
                                                <ItemTemplate>
                                                    <div  runat="server" id="ribbonNotPaid" >
                                                    <asp:Label ID="lblStatus" runat="server" Text="" CssClass="LabelEventStatus"></asp:Label>                                                    
                                                   </div>
                                                  <%--   <div  runat="server" class="ribbonActive">
                                                    <asp:Label ID="lblDone" runat="server" Text="ENROLLED"  Style="display: none;"></asp:Label>
                                                         </div>--%>
                                                    <div class="lblShow1O_AR-inner" pagesize="2">
                                                       
                                                        <%--<asp:RadioButton ID="radSelectAct" runat="server" GroupName="SelectControl" />
                                    <asp:CheckBox ID="chkSelectAct" runat="server" />--%>
                                                        <asp:Label ID="lblViewId" runat="server" Visible="FALSE" Text='<%# Eval("ALD_ID")%>' />
                                                        <img src='<%# Eval("AM_ACTIVITY_ICON_PATH")%>' width="50px" style="display: none;" />
                                                        <asp:Label ID="lblEventName" CssClass="LabelEventNameO_AR" runat="server" Text='<%# Eval("ALD_EVENT_NAME")%>'></asp:Label>                                                                                                               
                                                        <%--<table align="right" id="tblSuccess" runat="server">
                                                            <tr>
                                                                <td>                                                                    
                                                                    
                                                                </td>
                                                            </tr>
                                                        </table>--%>
                                                        <div class="row">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 margin-bottom20">
                                                                <asp:Label ID="lblDescr" runat="server" Text='<%# Eval("ALD_EVENT_DESCR")%>'  />
                                                            </div>
                                                        </div>
                                                        <table width="100%">
                                                            <tr>
                                                                <td width="40%" valign="top">
                                                                    <div class="container-fluid margin-bottom10 margin-top10">
                                                                        <div class="row border-bottom-act margin-bottom5">
                                                                            <div class="col-lg-6 col-md-2 col-sm-6"><span class="act-desc-title">Amount :</span></div>
                                                                            <div class="col-lg-6 col-md-9 col-sm-6">
                                                                                <asp:Label ID="lblamt" runat="server" Text='<%# String.Concat(Eval("ALD_EVENT_AMT"), " ", "AED")%>'> </asp:Label>
                                                                                <span style="display: none;">
                                                                                    <asp:Label ID="lblApprvl" runat="server" Text='<%# Eval("ALD_APPRVLREQ_FLAG")%>' value='<%# Eval("ALD_APPRVLREQ_FLAG")%>'></asp:Label></span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row border-bottom-act margin-bottom5">
                                                                            <div class="col-lg-6 col-md-2 col-sm-6"><span class="act-desc-title">Event on :</span></div>
                                                                            <div class="col-lg-6 col-md-9 col-sm-6">
                                                                                <asp:Label ID="lblLogDt" runat="server" Text='<%# Eval("EVENT_DATE")%>' value='<%# Eval("EVENT_DATE")%>' />
                                                                            </div>
                                                                        </div>
                                                                        <div class="row border-bottom-act margin-bottom5">
                                                                            <div class="col-lg-6 col-md-2 col-sm-6"><span class="act-desc-title">Requesting Period :</span></div>
                                                                            <div class="col-lg-6 col-md-9 col-sm-6">
                                                                                <asp:Label ID="Label1" runat="server" Text='<%# Eval("REQ_DATE")%>' value='<%# Eval("EVENT_DATE")%>' />
                                                                            </div>
                                                                        </div>
                                                                        <div class="row border-bottom-act margin-bottom5">
                                                                            <div class="col-lg-6 col-md-2 col-sm-6">
                                                                                <span class="act-desc-title">Total Available Seats :</span>
                                                                            </div>
                                                                            <div class="col-lg-6 col-md-9 col-sm-6">
                                                                                <asp:Label ID="lblAvailble" runat="server" Text='<%# Eval("ALD_AVAILABLE_SEAT")%>' value='<%# Eval("ALD_AVAILABLE_SEAT")%>'></asp:Label>
                                                                            </div>
                                                                        </div>
                                                                       
                                                                        <%--<asp:HiddenField ID="hdn_count" runat="server" Value='<%# Eval("ALD_ALLOW_MULTIPLE")%>' />--%>
                                                                        <div class="row" id="divcount" runat="server" visible="false">
                                                                            <div class="col-lg-6 col-md-2 col-sm-6">
                                                                                <span class="act-desc-title">Count :</span>
                                                                            </div>
                                                                            <div class="col-lg-6 col-md-9 col-sm-6">
                                                                                <asp:DropDownList runat="server" ID="ddlcount" OnSelectedIndexChanged="ddlcount_SelectedIndexChanged" AutoPostBack="true" CssClass="textcntO_AD"></asp:DropDownList>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td width="60%">
                                                                    <div id="divSchdl" runat="server" visible='<%# Eval("ALD_IS_SCHEDULE")%>'>
                                                                        <div class="container-fluid margin-bottom10 margin-top10">
                                                                            <div class="row">
                                                                                <div class="col-lg-12 col-md-12 col-sm-12">
                                                                                    <div class="sched-txt">Schedules :</div>
                                                                                    <asp:RadioButtonList ID="rdoSchdlDetails" runat="server" OnSelectedIndexChanged="rdoSchdlDetails_SelectedIndexChanged" AutoPostBack="true"></asp:RadioButtonList>                                                                                   
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2">
                                                                   <asp:CheckBox  id="chkTerm" runat="server" text ="I Accept" OnCheckedChanged="chkTerm_CheckedChanged" AutoPostBack="true" Visible="false"></asp:CheckBox>
                                                                   <asp:LinkButton ID="lnkTermCond" runat="server" Text="Terms and conditions." Visible="false"></asp:LinkButton>  
                                                                   <div style="display:none" id="divTermMessage" runat="server">Please accept terms and conditions</div>                                                               
                                                                        <div runat="server" id="divgrad" style="display: none" class="table table-striped table-bordered toggle-msg">
                                                                            <div class="margin-bottom0 toggle-padding">
                                                                                <asp:Literal ID="ltrlTerm" runat="server" Text='<%# Eval("ALD_TERMS_CONDITIONS")%>'></asp:Literal>
                                                                            </div>                                                                            
                                                                        </div>                                                                                                                                                                                                                                                                                                                              
                                                                </td>
                                                            </tr>
                                                        </table>

                                                        <div class="container-fluid margin-bottom10 margin-top10">
                                                            <div class="row">
                                                                <div class="col-lg-12 col-md-12 col-sm-12">
                                                                    <asp:LinkButton ID="lnkbtnUnReg" runat="server" Text="UnSubscribe" CssClass="btn btn-info submit pull-right bottom-pad" Visible="false" OnCommand="lnkbtnUnReg_Command" CommandArgument='<%# Eval("ALD_ID")%>' OnClientClick="return ConfirmUnregister();"></asp:LinkButton>
                                                                    <asp:LinkButton ID="lnkbtnPay" runat="server" Text="Pay Online" CssClass="btn btn-info submit pull-right bottom-pad" Visible="false" OnCommand="lnkbtnPay_Command" CommandArgument='<%# Eval("ALD_EVENT_NAME")%>' />
                                                                    <asp:LinkButton ID="lnkbtnapply" runat="server" Text="Enroll" CssClass="btn btn-info submit pull-right bottom-pad" />

                                                                    <%--<asp:HiddenField ID="hidStatus" runat="server" Value='<%# Eval("APD_STATUS")%>' />--%>
                                                                    <%--<asp:HiddenField ID="hidCollMode" runat="server" Value='<%# Eval("ALD_FEE_REG_MODE")%>' />--%>
                                                                    <%--<asp:HiddenField ID="hidFeetypeId" runat="server" Value='<%# Eval("ALD_FEETYPE_ID")%>' />--%>
                                                                    <%--<asp:HiddenField ID="hidAmnt" runat="server" Value='<%# Eval("ALD_EVENT_AMT")%>' />--%>
                                                                    <%--<asp:HiddenField ID="hidAPD_ID" runat="server" Value='<%# Eval("APD_ID")%>' />--%>
                                                                    <%--<asp:HiddenField ID="hidALD_PAY_ONLINE" runat="server" Value='<%# Eval("ALD_PAY_ONLINE")%>' />--%>
                                                                    <%-- new fields--%>
                                                                    <%--<asp:HiddenField ID="hidAvaiableSeat" runat="server" Value='<%# Eval("ALD_AVAILABLE_SEAT")%>' />--%>
                                                                    <%--<asp:HiddenField ID="hidtotalamount" runat="server" Value='<%# Eval("APD_TOTAL_AMOUNT")%>' />--%>
                                                                    <%--<asp:HiddenField ID="hidcount" runat="server" Value='<%# Eval("APD_COUNT")%>' />--%>
                                                                    <%--<asp:HiddenField ID="hidallowmultiple" runat="server" Value='<%# Eval("ALD_ALLOW_MULTIPLE")%>' />--%>
                                                                    <%--<asp:HiddenField ID="maxcount" runat="server" Value='<%# Eval("ALD_MAX_COUNT")%>' />--%>
                                                                    <%--<asp:HiddenField ID="hidpaymentStatus" runat="server" Value='<%# Eval("APD_PAYMENT_STATUS")%>' />--%>
                                                                    <%--<asp:HiddenField ID="hidAllowMultipleActDay" runat="server" Value='<%# Eval("ALD_BNOT_ALLWDAY")%>' />--%>
                                                                    <asp:HiddenField ID="hidTotalAmt" runat="server" />
                                                                    <%--<asp:HiddenField ID="hidMulMaxCount" runat="server" Value='<%# Eval("ALD_ALLOW_MULTIPLE_MAXCOUNT")%>' />--%>
                                                                    <%--<asp:HiddenField ID="hidmailid" runat="server" Value='<%# Eval("STS_FEMAIL")%>' />--%>
                                                                    <%--<asp:TextBox ID="txtEmailTemplate" runat="server" Text='<%# Eval("ALD_EMAIL_TEMPLATE")%>' Visible="false"></asp:TextBox>--%>                                                                   
                                                                    <%--<asp:HiddenField ID="hidschedule" runat="server" Value='<%# Eval("APD_SCHEDULE_ID")%>' />--%>
                                                                    <%--<asp:HiddenField ID="hidPaytime" runat="server" Value='<%# Eval("APD_PAY_TIME")%>' />--%>
                                                                    <%--<asp:HiddenField ID="hidACTid" runat="server" Value='<%# Eval("ALD_ACT_ID")%>' />--%>
                                                                    <%--<asp:HiddenField ID="hidEvtId" runat="server" Value='<%# Eval("ALD_OTH_EVT_ID")%>' />--%>
                                                                    <%--<asp:HiddenField ID="hidTaxCode" runat="server" Value='<%# Eval("ALD_OTH_TAX_CODE")%>' />--%>
                                                                    <%--<asp:HiddenField ID="hidAutoEnroll" runat="server" Value='<%# Eval("ALD_AUTO_ENROLL")%>' />--%>
                                                                </div>
                                                            </div>
                                                       </div>                                                       

                                                        <div id="dialog" class="DisplayNoneO_AR">
                                                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                                <ContentTemplate>
                                                                </ContentTemplate>
                                                            </asp:UpdatePanel>
                                                        </div>

                                                        <%-- <hr runat="server" class ="hrStyleO_AR"/> --%>
                                                    </div>

                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </div>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <div id="testpopup" class="darkPanelM anim" runat="server" style="display: none; z-index: 3000 !important;">
                                    <div id="divboxpanelconfirm" runat="server" style="height: 50%; width: 40%;" class="darkPaneTop">
                                        <div class="holderInner" style="height: 90%; width: 98%;">                                           
                                            <table border="0" style="height: 90% !important; width: 100% !important; overflow-y: scroll;"
                                                class="tableNoborder">
                                                <tr class="trSub_Header">
                                                    <td>
                                                        <span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 50px 0;"></span>
                                                        Confirm & Proceed
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top">
                                                        <span class="ui-icon ui-icon-info" style="float: left; margin: 0 7px 7px 0;"></span>
                                                        <asp:Label ID="lblMsg" runat="server" Style="float: left; margin: 0 7px 7px 0;"></asp:Label><br />
                                                        <br />
                                                        <asp:Label ID="lblMsg2" runat="server" BackColor="Beige" Style="float: left; margin: 0 7px 7px 0;" class="alert-success alert"></asp:Label>
                                                        <br />
                                                        <br />
                                                       
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right">
                                                        <asp:Button ID="btnProceed" runat="server" CausesValidation="False" CssClass="btn btn-info"
                                                            Text="Confirm & Proceed" OnClick="btnProceed_Click" />
                                                        <asp:Button ID="btnSkip" runat="server" CausesValidation="False" CssClass="btn btn-info"
                                                            Text="Cancel" OnClick="btnSkip_Click" />
                                                    </td>
                                                </tr>
                                            </table>                        
                                        </div>
                                    </div>
                                </div>

                                <div id="divEnroll" class="darkPanelM anim" runat="server" style="display: none; z-index: 3000 !important;">
                                    <div id="div2" runat="server" style="height: 50%; width: 40%;" class="darkPanelMTop">
                                        <div class="holderInner" style="height: 90%; width: 98%;">                                           
                                            <table border="0" style="height: 90% !important; width: 100% !important; overflow-y: scroll;"
                                                class="tableNoborder">
                                                <tr class="trSub_Header">
                                                    <td>
                                                       <span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 50px 0;"></span>
                                                       <span style="font-size :medium ;font:bold ;"> Confirm & Proceed</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top">
                                                        <span class="ui-icon ui-icon-info" style="float: left; margin: 0 7px 7px 0;"></span>
                                                        <asp:Label ID="lblEnrollMessage" runat="server" Style="float: left; margin: 0 7px 7px 0;" ></asp:Label><br />
                                                        <br />
                                                        <asp:Label ID="lblEnrollMessage2" runat="server" BackColor="Beige" Style="float: left; margin: 0 7px 7px 0;" class="alert-success alert" ></asp:Label>
                                                        <br />
                                                        <br />
                                                        
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right">
                                                        <asp:Button ID="btnConfirm" runat="server" CausesValidation="False" CssClass="btn btn-info"
                                                            Text="Confirm & Proceed" OnClick="btnConfirm_Click" />
                                                        <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="btn btn-info"
                                                            Text="Cancel" OnClick="btnSkip_Click" />
                                                    </td>
                                                </tr>
                                            </table>                        
                                        </div>
                                    </div>
                                </div>

                                <div id="divDayError" class="darkPanelM anim" runat="server" style="display: none; z-index: 3000 !important;">
                                    <div id="div3" runat="server" style="height: 50%; width: 40%;" class="darkPanelMTop">
                                        <div class="holderInner" style="height: 90%; width: 98%;">                                           
                                            <table border="0" style="height: 90% !important; width: 100% !important; overflow-y: scroll;"
                                                class="tableNoborder">
                                                <tr class="trSub_Header">
                                                    <td align="center">
                                                        <span class="ui-icon ui-icon-alert" style=" margin: 0 7px 50px 0;"></span>
                                                       <span style="font-size :medium ;font:bold ;"> Alert</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" align="center">
                                                        <span class="ui-icon ui-icon-info" style=" margin: 0 7px 7px 0;"></span>
                                                        <asp:Label ID="lbl" runat="server" Style=" margin: 0 7px 7px 0;" Text="Can't enroll, Another activity has been requested on the same day!" ></asp:Label><br />                                                      
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center">                                                     
                                                        <asp:Button ID="Button2" runat="server" CausesValidation="False" CssClass="btn btn-info"
                                                            Text="Cancel" OnClick="btnSkip_Click" />
                                                    </td>
                                                </tr>
                                            </table>                        
                                        </div>
                                    </div>
                                </div>

                                <div id="divMaxlimitError" class="darkPanelM anim" runat="server" style="display: none; z-index: 3000 !important;">
                                    <div id="div4" runat="server" style="height: 50%; width: 40%;" class="darkPanelMTop">
                                        <div class="holderInner" style="height: 90%; width: 98%;">                                           
                                            <table border="0" style="height: 90% !important; width: 100% !important; overflow-y: scroll;"
                                                class="tableNoborder">
                                                <tr class="trSub_Header">
                                                    <td align="center">
                                                        <span class="ui-icon ui-icon-alert" style=" margin: 0 7px 50px 0;"></span>
                                                       <span style="font-size :medium ;font:bold ;"> Alert</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" align="center">
                                                        <span class="ui-icon ui-icon-info" style=" margin: 0 7px 7px 0;"></span>
                                                        <asp:Label ID="Label2" runat="server" Style=" margin: 0 7px 7px 0;" Text="Can't enroll, Activity selection limit has been reached!" ></asp:Label><br />                                                       
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center">                                                     
                                                        <asp:Button ID="Button1" runat="server" CausesValidation="False" CssClass="btn btn-info"
                                                            Text="Cancel" OnClick="btnSkip_Click" />
                                                    </td>
                                                </tr>
                                            </table>                        
                                        </div>
                                    </div>
                                </div>
                                
                                <div id="divInitError" class="darkPanelM anim" runat="server" style="display: none; z-index: 3000 !important;">
                                    <div id="divInitError2" runat="server" style="height: 50%; width: 40%;" class="darkPanelMTop">
                                        <div class="holderInner" style="height: 90%; width: 98%;">                                           
                                            <table border="0" style="height: 90% !important; width: 100% !important; overflow-y: scroll;"
                                                class="tableNoborder">
                                                <tr class="trSub_Header">
                                                    <td align="center">
                                                        <span class="ui-icon ui-icon-alert" style=" margin: 0 7px 50px 0;"></span>
                                                       <span style="font-size :medium ;font:bold ;"> Alert</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" align="center">
                                                        <span class="ui-icon ui-icon-info" style=" margin: 0 7px 7px 0;"></span>
                                                        <asp:Label ID="Label3" runat="server" Style=" margin: 0 7px 7px 0;" Text="Initiated transaction already under processing. Please retry after some time. Thanks!" ></asp:Label><br />                                                       
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center">                                                     
                                                        <asp:Button ID="Button3" runat="server" CausesValidation="False" CssClass="btn btn-info"
                                                            Text="Cancel" OnClick="btnSkip_Click" />
                                                    </td>
                                                </tr>
                                            </table>                        
                                        </div>
                                    </div>
                                </div>
                                                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <%--hidden field to track auto enroll status while registering activity--%>
        <asp:HiddenField ID="hidAutoStatus" runat="server" value="" />
    </div>

    <script type="text/javascript" language="javascript">

        function ShowWindowWithClose(gotourl, pageTitle, w, h, btnname) {

            $.fancybox({
                type: 'iframe',
                //maxWidth: 300,
                href: gotourl,
                //maxHeight: 600,
                fitToView: true,
                padding: 6,
                width: w,
                height: h,
                autoSize: false,
                openEffect: 'none',
                showLoading: true,
                closeClick: true,
                //closeEffect: 'fade',
                'closeBtn': true,
                afterLoad: function () {
                    this.title = '';//ShowTitle(pageTitle);
                },
                helpers: {
                    overlay: { closeClick: false }, // prevents closing when clicking OUTSIDE fancybox 
                    title: { type: 'inside' }
                },
                onComplete: function () {
                    $("#fancybox-wrap").css({ 'top': '90px' });
                },
                onCleanup: function () {
                    var hfPostBack = $("#fancybox-frame").contents().find('#hfbPB').val();

                    if (hfPostBack == "Y")
                        window.location.reload(true);
                },
                afterClose: function () {
                    if (btnname == "View and Enroll") {
                        window.location.reload();
                    }

                }
            });
            return false;
        }
        function ConfirmTerms(div) {
            div.style.display = "block";
        }
       
        function ConfirmEnroll(lnkbutton,eventname,schedule) {
           
            //return confirm('Do  you want to enroll for this activiy?');          
           
            $('#<%= hidclientidbtn.ClientID%>').val(lnkbutton);
            var labelmessage1 = "Please confirm to continue with registration of the activity " + eventname            
            $('#<%= lblEnrollMessage.ClientID%>').text(labelmessage1);         
            var labelmessage2
         
            if (schedule == "")
            {
               // labelmessage2 = "Please note : There is no schedules for this activity"
                // $('#<%= lblEnrollMessage2.ClientID%>').text(labelmessage2);
                $('#<%= lblEnrollMessage2.ClientID%>').removeClass('alert-success alert');

            }
            else
            {
                labelmessage2 = "Please note : Activity schedule selected is  " + "\n" + schedule
                $('#<%= lblEnrollMessage2.ClientID%>').text(labelmessage2);
            }                       
            document.getElementById("<%=divEnroll.ClientID%>").style.display = "block";
            return false;
        }

        function ConfirmUnregister() {
            return confirm('Please confirm to unsubscribe from this activity?');
        }

        function AlertMultiple() {
            //alert("Can't enroll, Activity has been requested on the same day! ");
            document.getElementById("<%=divDayError.ClientID%>").style.display = "block";
        }

        function AlertLimitReached() {
            // alert("Can't enroll, Activity selection limit has been reached! ");
            document.getElementById("<%=divMaxlimitError.ClientID%>").style.display = "block";
        }
        function AlertInitError() {
            // alert("Can't enroll, Activity selection limit has been reached! ");
            document.getElementById("<%=divInitError.ClientID%>").style.display = "block";
        }
               
    </script>
    <link href="../css/Popup.css" rel="stylesheet" />
   

</asp:Content>
