﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ApplyTransportConcession.aspx.vb"
    Inherits="Others_ApplyTransportConcession" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
   <%-- <link type="text/css" href="../CSS/Popup.css" rel="stylesheet" />
    <link type="text/css" href="../CSS/SiteStyle.css" rel="stylesheet" />--%>
    <script src="../Scripts/jquery-1.9.1.js" type="text/javascript"></script>
      <link href="../css/bootstrap.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        function fancyClose() {
            parent.$.fancybox.close();
            parent.location.reload();
        }

    </script>
    <style type="text/css">
        .divWaiting {
            position: absolute;
            background-color: #FAFAFA;
            z-index: 2147483647 !important;
            opacity: 0.8;
            overflow: hidden;
            text-align: center;
            top: 0;
            left: 0;
            height: 100%;
            width: 100%;
            padding-top: 20%;
        }
    </style>
</head>
<body>
    <form id="form1" name="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>

        <div>
            <div style="width: 100%; margin-top: 0px; vertical-align: middle; background-color: White;">
                <span style="clear: right; display: inline; float: right; visibility: visible; margin-top: 0px; vertical-align: top;">
                    <asp:ImageButton ID="btnCloseedit" runat="server" ImageUrl="~/Images/Common/PageBody/close.png" /></span>
            </div>
            <div  class="trSub_Header">
                <div class="sub-heading">
                    Apply Transport Fee Concession
                </div>
            </div>
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <table runat="server" class="table table-striped table-bordered table-responsive text-left my-orders-table" style="width: 100%">
                        <tr>
                            <td colspan="2"></td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <% If Me.lblPopError.Text <> "" Then%>
                                <div style="width: 100%; text-align: center !important;">
                                    <asp:Label ID="lblPopError" Width="80%" runat="server"></asp:Label>
                                </div>
                                <% End If%>
                            </td>
                        </tr>
                        <tr class="title-box">
                            <td colspan="2">&nbsp;Student Info
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="">Student Name
                            </td>
                            <td align="left">
                                <asp:Label ID="lblstuName" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="">StudentId
                            </td>
                            <td align="left">
                                <asp:Label ID="lblStuno" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="">Grade
                            </td>
                            <td align="left">
                                <asp:Label ID="lblGrade" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="">Area-Pickup-BusNo
                            </td>
                            <td align="left">
                                <asp:Label ID="lblPickup" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="">Area-DropOff-Busno
                            </td>
                            <td align="left">
                                <asp:Label ID="lblDropoff" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" colspan="2">
                                <asp:Label ID="lblMessage" runat="server" Font-Size="Small" ForeColor="Red" BackColor="#FFFFC0"></asp:Label></td>
                        </tr>
                        <tr>
                            <td align="left" colspan="2">
                                <asp:CheckBox ID="chkTermsAndCondition" runat="server" Checked="False" AutoPostBack="True" />
                                I confirm the above details of my child is correct. If found incorrect,the provider
                        has rights to cancel the concession.
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="2">
                                <asp:Button ID="btnApplyConsn" runat="server"  Text="Apply"  Width="60px"
                                    Enabled="False" />
                                <asp:Button ID="btnCancelConsn" runat="server" CausesValidation="False" Text="Close"
                                    Width="60px"  />
                                <asp:HiddenField ID="hfbPB" runat="server" />
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:UpdateProgress ID="UpdateProgress1"
                AssociatedUpdatePanelID="UpdatePanel1" runat="server">
                <ProgressTemplate>
                    <div class="divWaiting">
                        <asp:Image ID="img1" runat="server" ImageUrl="~/Images/Misc/loading1.gif" />Processing, Please Wait....
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </div>
    </form>
</body>
</html>
