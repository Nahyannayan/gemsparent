﻿<%@ Page Language="VB" MasterPageFile="~/ParentMaster.master" AutoEventWireup="false"
    CodeFile="OthTcRequest.aspx.vb" Inherits="Others_OthTcRequest" Title="GEMS EDUCATION" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphParent" runat="Server">
    <script type="text/javascript" src="../Scripts/iframeResizer.min.js"></script>

     <style type="text/css">
        .table-subheader-striped {
            background-color: #0061c3;
        }

        td {
            font-weight: bold;
        }

        .rdn {
            display: inline;
        }
    </style>

    <script type="text/javascript" language="javascript">

        function ApplyClass() {
            $('[for="cphParent_rdCorporate"]').attr('style', 'display:inline');
        }
        function checkLastDate(sender, args) {

            var currenttime = document.getElementById("<%=hfLastDt.ClientID %>").value;

            var DisplayDt = document.getElementById("<%=hfLastDt.ClientID %>").value;
            var lblPopError = document.getElementById("<%=lbltcMsg.ClientID %>");
            var Info = " You cannot select a day less than " + DisplayDt;
            if (sender._selectedDate < new Date(currenttime)) {
                lblPopError.setAttribute("class", "diverror");

                lblPopError.innerHTML = Info;
                ; //alert(Info);
                sender._selectedDate = new Date(currenttime);
                // set the date back to the current date
                sender._textbox.set_Value(sender._selectedDate.format(sender._format))
            }
            else {
                lblPopError.setAttribute("class", "");

                lblPopError.innerText = "";
            }


        }


    </script>
    <asp:HiddenField ID="hdn_stuno" runat="server" />
    <asp:HiddenField ID="hdn_bsuid" runat="server" />
    <div class="content margin-top30 margin-bottom60">
        <div class="container">
            <div class="row">

                <!-- Posts Block -->
                <div class="posts-block col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div id="divOasis_TC" runat="server">
                        <div class="mainheading">
                            <div class="left">
                                TC Request
                            </div>
                            <div class="right">
                                <asp:Label ID="lbChildName" runat="server">
                                </asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </div>
                        </div>

                        <%--working--%>



                        <div id="divgrid" runat="server">
                            <table class="BlueTable_simple" width="100%" cellpadding="8" cellspacing="0">
                                <tr>
                                    <td>
                                        <asp:GridView ID="gvTC" runat="server" AllowPaging="false" EmptyDataText="No record available."
                                            AutoGenerateColumns="False" Width="100%" CssClass="gridView" BorderStyle="None"
                                            BorderWidth="0px">
                                            <EmptyDataRowStyle Wrap="True" HorizontalAlign="Center" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="Apply Date">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Center" Width="80px"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblApplyDate" runat="server" Text='<%# Bind("TCM_APPLYDATE", "{0:dd/MMM/yyyy}") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Last Attendance Date">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Center" Width="150px"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblLastDate" runat="server" Text='<%# Bind("TCM_LASTATTDATE", "{0:dd/MMM/yyyy}") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Status">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStatus" runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Exit Interview">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Center" Width="150px"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkStatus" ForeColor="Red" runat="server" Text='Pending(Please click here to update)'
                                                            OnClick="lnkStatus_Click"></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div id="divbutton" runat="server" style="width: 100%; text-align: left;" align="left">
                            <div style="padding: 4px 12px; text-align: left; font-size: 13px">
                                <p>
                                    Within the T/C application parents are offered the following choices for Exit Interview
                when requesting a T/C
                                </p>
                                <p>
                                    <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/Common/PageBody/arrow.png" />
                                    Complete online form only
                                </p>
                                <p>
                                    <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/Common/PageBody/arrow.png" />
                                    Request meeting with Principal
                                </p>
                                <p>
                                    <asp:Image ID="Image3" runat="server" ImageUrl="~/Images/Common/PageBody/arrow.png" />
                                    Request meeting with Corporate Office
                                </p>
                                <p>
                                </p>
                                <p>
                                    There is also a short questionnaire to complete for your valuable feedback
                                </p>
                            </div>
                            <asp:Button ID="btnTC" runat="server" OnClick="btnTC_Click" CssClass="buttons" Width="260px" Text="Click here to apply for TC" />
                        </div>
                        <div id="divBlockTc" runat="server" style="width: 100%; text-align: left;" align="left">
                            <div style="padding: 4px 12px; text-align: left; font-size: 13px">
                                <p>
                                    <b>For Transfer Certificate application, please approach the school directly</b>
                                </p>
                            </div>
                        </div>
                        <div id="divTC" runat="server" visible="false">
                            <%--  <div style="width: 941px; margin-top: 0px; vertical-align: middle; background-color: White;">
            <span style="clear: right; display: inline; float: right; visibility: visible; margin-top: 0px;
                vertical-align: top;">
                <asp:ImageButton ID="btnCloseedit" runat="server" ImageUrl="~/Images/Common/PageBody/close.png" /></span>
        </div>--%>
                            <asp:Panel ID="plTC" runat="server" Width="941px" Height="580px" BackColor="White"
                                BorderColor="Transparent" BorderStyle="none" ScrollBars="None">
                                <asp:Panel ID="pnlMain" runat="server">
                                    <div class="content margin-bottom60">
                                        <div class="container">
                                            <div class="row">
                                                <!-- Posts Block -->
                                                <div class="posts-block col-lg-12 col-md-12 col-sm-12 col-xs-12">

                                                    <div class="table-responsive">
                                                        <table runat="server" class="table table-striped table-bordered table-responsive">
                                                            <tr id="Message" runat="server">
                                                                <th colspan="3" runat="server">
                                                                    <asp:Label ID="lbltcMsg" Width="940px" runat="server"></asp:Label>
                                                                </th>
                                                                <th runat="server" style="width: 180px; text-align: center;">
                                                                    <asp:Button ID="btnSave1" runat="server" Text="Save" CssClass="buttons" Width="60px" />
                                                                </th>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <div class="">

                                                        <table runat="server" class="table table-striped table-bordered table-responsive ">

                                                            <tr class="sub-heading ">
                                                                <th colspan="6" class="table-subheader-striped">&nbsp;Student Info
                                                                </th>
                                                            </tr>
                                                            <tr>
                                                                <th colspan="4">
                                                                    <table>
                                                                        <tr>
                                                                            <th align="left" class="tdfields">Passport Name
                                                                            </th>
                                                                            <th align="left" style="padding-left: 10px">
                                                                                <asp:Label ID="lblName" runat="server" Text="Label"></asp:Label>
                                                                            </th>
                                                                            <th align="left" class="tdfields" style="padding-left: 10px">Grade & Section
                                                                            </th>
                                                                            <th align="left" colspan="2" style="padding-left: 10px">
                                                                                <asp:Label ID="lblGrade" runat="server" Text="Label"></asp:Label>
                                                                            </th>
                                                                        </tr>
                                                                        <tr>
                                                                            <th align="left" class="tdfields">Name of the Primary Contact
                                                                            </th>
                                                                            <th align="left" style="padding-left: 10px">
                                                                                <asp:Label ID="lblPrimaryName" runat="server" Text="Label"></asp:Label>
                                                                                <asp:Label ID="lblFather" Visible="false" runat="server" Text="Label"></asp:Label>
                                                                            </th>
                                                                            <th align="left" class="tdfields" style="padding-left: 10px">Mob.No
                                                                            </th>
                                                                            <th align="left" style="padding-left: 10px">
                                                                                <asp:Label ID="lblPhone" runat="server" Text="Label"></asp:Label>
                                                                            </th>
                                                                            <th align="left" class="tdfields" style="padding-left: 10px">Email ID
                                                                            </th>
                                                                            <th align="left" style="padding-left: 10px">
                                                                                <asp:Label ID="lblEmail" runat="server" Text="Label"></asp:Label>
                                                                            </th>
                                                                        </tr>
                                                                    </table>
                                                                </th>
                                                            </tr>
                                                            <tr class="sub-heading ">
                                                                <th colspan="6" class="table-subheader-striped">&nbsp;Details
                                                                </th>
                                                            </tr>

                                                            <tr>
                                                                <th align="left" class="tdfields">Please choose one of the following options<span style="color: red">*</span>
                                                                </th>
                                                                <th colspan="3" runat="server">
                                                                    <asp:RadioButton ID="rdOnline" Text="For Exit interview I will only complete the online form"
                                                                        GroupName="exit" runat="server" CssClass="radioAns" AutoPostBack="true" /><br />
                                                                    <asp:RadioButton ID="rdPrinicipal" Text="I would like a meeting with the Principal to complete the exit interview"
                                                                        GroupName="exit" runat="server" CssClass="radioAns" AutoPostBack="true" /><br />
                                                                    <asp:RadioButton ID="rdCorporate" Text="I would like a meeting with a member of Corporate office customer care team to complete the exit interview"
                                                                        GroupName="exit" runat="server" Style="display: inline" AutoPostBack="true" />
                                                                </th>
                                                            </tr>
                                                            <tr>
                                                                <th align="left">Last Date of Attendance<span style="color: red">*</span>
                                                                </th>
                                                                <th align="left" colspan="3">
                                                                    <asp:TextBox ID="txtLastDt" runat="server" Width="110px" CssClass="input"></asp:TextBox>
                                                                    <asp:ImageButton ID="imgLastDt" runat="server" ImageUrl="~/Images/Common/PageBody/Calendar.png"
                                                                        CssClass="imgCal" />
                                                                    <asp:RequiredFieldValidator ID="rfvLastDT" runat="server" ControlToValidate="txtLastDt"
                                                                        Display="Dynamic" ErrorMessage="Please enter the last date of attendance" ForeColor="red"
                                                                        ValidationGroup="popValid">*</asp:RequiredFieldValidator>
                                                                    <asp:RegularExpressionValidator ID="REVLastDT" runat="server" ControlToValidate="txtLastDt"
                                                                        Display="Dynamic" EnableViewState="False" ErrorMessage="Enter Last Attendance  Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                                                                        ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                                                        ValidationGroup="popValid">*</asp:RegularExpressionValidator><asp:CustomValidator
                                                                            ID="cvFrmDt" runat="server" ControlToValidate="txtLastDt" Display="Dynamic" EnableViewState="False"
                                                                            ErrorMessage="Last attendance date entered is not a valid date" ForeColor="red"
                                                                            ValidationGroup="popValid">*</asp:CustomValidator><div class="remark">
                                                                                (dd/mmm/yyyy)
                                                                            </div>
                                                                </th>
                                                            </tr>
                                                            <tr>
                                                                <th align="left" class="tdfields" valign="top">Name of the school to be transferred to
                                                                </th>
                                                                <th align="left" colspan="3">
                                                                    <asp:DropDownList ID="ddlTrans_School" AutoPostBack="true" CssClass="dropDownList"
                                                                        runat="server" Width="400px">
                                                                    </asp:DropDownList>
                                                                    <br />
                                                                    <br />
                                                                    (If you choose other,please specify the school)<br />
                                                                    <asp:TextBox ID="txtOthers" runat="server" Width="400px"></asp:TextBox>
                                                                </th>
                                                            </tr>
                                                            <tr>
                                                                <th align="left" class="tdfields">School located in<span style="color: red">*</span>
                                                                </th>
                                                                <th align="left" colspan="3">
                                                                    <asp:DropDownList ID="ddlCountry" CssClass="dropDownList" runat="server" Width="143px">
                                                                    </asp:DropDownList>
                                                                </th>
                                                            </tr>
                                                            <tr>
                                                                <th align="left" class="tdfields">Transferring To<span style="color: red">*</span>
                                                                </th>
                                                                <th align="left" colspan="3">
                                                                    <asp:DropDownList ID="ddlTransferType" CssClass="dropDownList" runat="server" Width="250px">
                                                                    </asp:DropDownList>
                                                                </th>
                                                            </tr>
                                                            <tr>
                                                                <th align="left" class="tdfields">Reason For Transfer / Withdrawal<span style="color: red">*</span>
                                                                </th>
                                                                <th colspan="2">
                                                                    <asp:DropDownList ID="ddlReason" Width="300px" CssClass="dropDownList" runat="server">
                                                                    </asp:DropDownList>
                                                                    <!--<br />
                            (If you choose other,please specify)<br />-->
                                                                    <asp:TextBox ID="txtOtherReason" Visible="false" runat="server" Width="200px"></asp:TextBox>
                                                                </th>
                                                            </tr>
                                                            <tr>
                                                                <th align="left" class="tdfields">We appreciate your feedback. Please let us know if you have anything you wish to
                            comment on<span style="color: red">*</span>
                                                                </th>

                                                                <th>
                                                                    <asp:TextBox ID="txtExitFeedback" TextMode="MultiLine" SkinID="MultiText" Width="500px"
                                                                        runat="server"></asp:TextBox>
                                                                </th>
                                                            </tr>
                                                            <tr id="pnlExit" runat="server">
                                                                <th colspan="6">
                                                                    <div>
                                                                        <div class="sub-heading">
                                                                            <div class="left">
                                                                                STUDENT EXIT INTERVIEW
                                                                            </div>
                                                                        </div>
                                                                        <div style="padding: 4px 12px; text-align: left;">
                                                                            <p>
                                                                                Dear Parent,
                                                                            </p>
                                                                            As your child/ren prepares to leave the school we would value your feedback in order
                                    for us to continue meeting your expectations. Please take a few minutes to fill
                                    out the short questionnaire below.
                                    <p>
                                        Thank you
                                    </p>
                                                                            <div id="lblPrincipal" runat="server" style="margin-top: 4px; padding-bottom: 3px;">
                                                                            </div>
                                                                            Principal
                                                                        </div>
                                                                        <br />
                                                                        <table class="tableNoborder" style="width: 100%">


                                                                            <tr>
                                                                                <th colspan="2">
                                                                                    <asp:Label ID="lblExitMsg" runat="server"></asp:Label>
                                                                                </th>
                                                                            </tr>
                                                                            <tr>
                                                                                <th colspan="2" class="sub-heading">
                                                                                    <label>
                                                                                        <asp:Label ID="lblpleaserate" Text="Feedback" runat="server"></asp:Label>
                                                                                    </label>
                                                                                </th>
                                                                            </tr>



                                                                            <tr>
                                                                                <th colspan="2">Please rate the following :-
                                                                                </th>
                                                                            </tr>
                                                                            <tr>

                                                                                <th colspan="2">
                                                                                    <asp:DataList ID="dlExitQ" runat="server" RepeatColumns="1" RepeatDirection="Horizontal"
                                                                                        ShowHeader="False" Width="100%">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="lblTqsId" runat="server" Text='<%# Bind("TQS_ID") %>' Visible="false"></asp:Label>
                                                                                            <table border="0" class="table borderless">
                                                                                                <tr>
                                                                                                    <th>
                                                                                                        <asp:Label ID="lblSlno" runat="server" Text='<%# Bind("TQS_SLNO") %>'></asp:Label>
                                                                                                        .
                                                                                                    </th>
                                                                                                    <th>
                                                                                                        <asp:Label ID="lblQuestion" runat="server" Text='<%# Bind("TQS_QUESTION") %>'></asp:Label>
                                                                                                    </th>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <th></th>
                                                                                                    <th>
                                                                                                        <asp:RadioButton ID="rdSA" GroupName='<%# Bind("TQS_SLNO") %>' runat="server" Text="Strongly agree"
                                                                                                            CssClass="radioAns" />
                                                                                                        &nbsp;&nbsp;
                                                                <asp:RadioButton ID="rdA" GroupName='<%# Bind("TQS_SLNO") %>' runat="server" Text="Agree"
                                                                    CssClass="radioAns" />&nbsp;&nbsp;
                                                                <asp:RadioButton ID="rdDA" GroupName='<%# Bind("TQS_SLNO") %>' runat="server" Text="Disagree"
                                                                    CssClass="radioAns" />&nbsp;&nbsp;
                                                                <asp:RadioButton ID="rdSDA" GroupName='<%# Bind("TQS_SLNO") %>' runat="server" Text="Strongly Disagree"
                                                                    CssClass="radioAns" />&nbsp;&nbsp;
                                                                <asp:RadioButton ID="rdNA" GroupName='<%# Bind("TQS_SLNO") %>' runat="server" Text="NA"
                                                                    CssClass="radioAns" />
                                                                                                    </th>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </ItemTemplate>
                                                                                    </asp:DataList>
                                                                                </th>


                                                                            </tr>

                                                                        </table>
                                                                    </div>
                                                                </th>
                                                            </tr>
                                                            <tr runat="server">
                                                                <th colspan="6" style="height: 45px;">
                                                                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="buttons" Width="60px" />
                                                                </th>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </asp:Panel>
                            </asp:Panel>
                        </div>
                        <div id="dvTc2" runat="server" style="display: none; overflow: visible; background-color: White; border-color: #b5cae7; border-style: solid; border-width: 4px; width: 300px; height: 200px;">
                            <asp:Panel ID="PnlTc2" runat="server">
                                <table id="Table1" runat="server" class="tableNoborder" style="width: 100%" cellspacing="7">
                                    <tr>
                                        <td align="left" colspan="2" class="tdfields">
                                            <asp:Label ID="lblLastDtText" runat="server" Text="Label"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" class="tdfields">Last Date of Attendance<span style="color: red">*</span>
                                        </td>
                                        <td align="left" width="200px">
                                            <asp:TextBox ID="txtLastDtConfirm" runat="server" Width="110px" CssClass="input"></asp:TextBox>
                                            <asp:ImageButton ID="imgLastDtConfirm" runat="server" ImageUrl="~/Images/Common/PageBody/Calendar.png"
                                                CssClass="imgCal" />
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtLastDtConfirm"
                                                Display="Dynamic" ErrorMessage="Please enter the last date of attendance" ForeColor="red"
                                                ValidationGroup="popValid">*</asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtLastDtConfirm"
                                                Display="Dynamic" EnableViewState="False" ErrorMessage="Enter Last Attendance  Date in given format dd/mmm/yyyy e.g.  21/Sep/2007"
                                                ValidationExpression="^([012]?\d|3[01])/([Jj][Aa][Nn]|[Ff][Ee][bB]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][lL]|[aA][Uu][gG]|[Ss][eE][pP]|[oO][Cc][Tt]|[Nn][oO][Vv]|[Dd][Ee][Cc]|[012]?\d)/(19|20)\d\d$"
                                                ValidationGroup="popValid">*</asp:RegularExpressionValidator><asp:CustomValidator
                                                    ID="CustomValidator1" runat="server" ControlToValidate="txtLastDtConfirm" Display="Dynamic"
                                                    EnableViewState="False" ErrorMessage="Last attendance date entered is not a valid date"
                                                    ForeColor="red" ValidationGroup="popValid">*</asp:CustomValidator><div class="remark">
                                                        (dd/mmm/yyyy)
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3" align="center">
                                            <asp:Button ID="btnAccept" runat="server" Text="Accept" CssClass="buttons" Width="60px" />
                                            <asp:Button ID="btnCancelConfirm" runat="server" CausesValidation="False" Text="Cancel"
                                                Width="60px" CssClass="buttons" />
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </div>
                    </div>
                    <div id="divSF_TC" runat="server" visible="false">
                        <iframe id="ifTc" runat="server" width="100%" frameborder="0"></iframe>
                    </div>
                </div>
                <!-- /Posts Block -->
            </div>
        </div>
    </div>
    <script type="text/javascript">
        document.getElementById("<%=ifTc.ClientID%>").onload = resizeFrame;
        function resizeFrame() {
            iFrameResize({ log: false }, document.getElementById("<%=ifTc.ClientID%>"));
        }
    </script>
    <ajaxToolkit:CalendarExtender ID="ceLastDt" runat="server" Format="dd/MMM/yyyy" PopupButtonID="imgLastDt"
        TargetControlID="txtLastDt" OnClientDateSelectionChanged="checkLastDate">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="ceLastDtConfirm" runat="server" Format="dd/MMM/yyyy"
        PopupButtonID="imgLastDtConfirm" TargetControlID="txtLastDtConfirm" OnClientDateSelectionChanged="checkLastDate">
    </ajaxToolkit:CalendarExtender>
    <%--    <ajaxToolkit:ModalPopupExtender ID="mdlPopup" runat="server" TargetControlID="btnTC"
        PopupControlID="divTC" BackgroundCssClass="modalBackground" DropShadow="false"
        RepositionMode="RepositionOnWindowResizeAndScroll">
    </ajaxToolkit:ModalPopupExtender>--%>
    <ajaxToolkit:ModalPopupExtender ID="mdlPopup2" runat="server" TargetControlID="hfSave"
        PopupControlID="dvTc2" BackgroundCssClass="modalBackground" DropShadow="false"
        RepositionMode="RepositionOnWindowResizeAndScroll">
    </ajaxToolkit:ModalPopupExtender>
    <asp:HiddenField ID="hfTCM_ID" runat="server" />
    <asp:HiddenField ID="hfDaysP" runat="server" />
    <asp:HiddenField ID="hfDaysT" runat="server" />
    <asp:HiddenField ID="hfLastDt" runat="server" />
    <asp:HiddenField ID="hfSave" runat="server" />
</asp:Content>
