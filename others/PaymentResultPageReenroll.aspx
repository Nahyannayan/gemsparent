<%@ Page Language="VB" AutoEventWireup="false" CodeFile="PaymentResultPageReenroll.aspx.vb"
    Inherits="Others_PaymentResultPageReenroll" Title="::GEMS EDUCATION::" %>

<%@ OutputCache Duration="1" Location="None" VaryByParam="none" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>GEMS Education</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
   <link href="../CSS/SiteStyle.css" rel="stylesheet" type="text/css" />
    <link href="../CSS/MenuStyle.css" rel="stylesheet" type="text/css" />
    <meta content="GEMS Education" name="description" />
    <meta content="GEMS Education" name="keywords" />
   
        <script type="text/javascript" language="javascript">
        function CheckForPrint() {
            if (document.getElementById('<%=h_Recno.ClientID %>').value != '') {
                showModelessDialog('FeeReceiptReEnrol.aspx?type=REC&id=' + document.getElementById('<%=h_Recno.ClientID %>').value, '', "dialogWidth: 800px; dialogHeight: 700px; help: no; resizable: no; scroll: yes; status: no; unadorned: no;");
                return false;
            }
            else
                alert('Sorry. There is some problem with the transaction');
        }              
    </script>
</head>
<body>
    <form runat="server" id="SignIn">
    <div style="text-align:center !important; width:100%;">
<div class="wrapper">
<div class="header">

&nbsp;</div>

 <div class="leftBox">
                <asp:Label ID="lblError" runat="server" EnableViewState="False"></asp:Label>
         
                <table  align="center" style="width: 98%; height: 110px;"  cellpadding="8" cellspacing="0" class="BlueTable_simple">
                    <tr class="trSub_Header">
                        <td colspan="2" align="left" style="height: 18px">
                            Re-enrolment Fee Online Payment
                        </td>
                    </tr>
                    <tr class="tdfields" > 
                        <td align="left" width ="30%">
                            Date
                        </td>
                        <td align="left">
                            <asp:Label ID="lblDate" runat="server" ></asp:Label>
                        </td>
                    </tr>
                    <tr class="tdfields">
                        <td align="left">
                            Student ID
                        </td>
                        <td align="left">
                            <asp:Label ID="lblStudentNo" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr class="tdfields">
                        <td align="left">
                            Student Name
                        </td>
                        <td align="left">
                            <asp:Label ID="lblStudentName" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr class="tdfields">
                        <td align="left">
                            Transaction Reference
                        </td>
                        <td align="left" valign="middle">
                            <asp:Label ID="Label_MerchTxnRef" runat="server" />
                        </td>
                    </tr>
                    <tr class="tdfields">
                        <td align="left">
                            Amount
                        </td>
                        <td align="left" valign="middle">
                            <asp:Label ID="Label_Amount" runat="server" />
                        </td>
                    </tr>
                    <tr class="tdfields">
                        <td align="left">
                            Receipt Number
                        </td>
                        <td align="left" valign="middle" >
                            &nbsp;<asp:LinkButton ID="lnkReceipt" runat="server"
                             OnClientClick="CheckForPrint();return false;" CssClass="linkFee"
                              ></asp:LinkButton>
                            <asp:HiddenField ID="h_Recno" runat="server" />
                        </td>
                    </tr>
                    <tr><td style="font-weight: bold; font-size: 12px; color: #ff0000" align="left" colspan="2">
                      <asp:Label ID="lblPrintMsg" runat="server" EnableViewState="False"
                       Visible="False">Please print the receipt (Click on the receipt number) for your reference.</asp:Label>
                       </td></tr>
                       <tr>
            <td align="center" style="font-weight: bold; font-size: 12px; color: #ff0000" colspan="2">
            <div style="width:100%;text-align:center;">
                            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="button"
                                Text="Back" TabIndex="158" /></div>
            </td>
        </tr>
                </table>
         
    <br />
    <table align="center" border="0" width="70%" style="display: none">
        <tr class="title">
            <td colspan="2">
                <iframe id="frame1" scrolling="auto" runat="server"></iframe>
                <p>
                    <strong>&nbsp;Transaction Receipt Fields</strong></p>
                <asp:Label ID="Label_Title" runat="server" />
            </td>
        </tr>
        <tr>
            <td align="right">
                <strong><i>VPC API Version: </i></strong>
            </td>
            <td>
                <asp:Label ID="Label_Version" runat="server" />
            </td>
        </tr>
        <tr class='shade'>
            <td align="right">
                <strong><i>Command: </i></strong>
            </td>
            <td>
                <asp:Label ID="Label_Command" runat="server" />
            </td>
        </tr>
        <tr>
            <td align="right">
                <strong><em>MerchTxnRef: </em></strong>
            </td>
            <td>
            </td>
        </tr>
        <tr class="shade">
            <td align="right">
                <strong><em>Merchant ID: </em></strong>
            </td>
            <td>
                <asp:Label ID="Label_MerchantID" runat="server" />
            </td>
        </tr>
        <tr>
            <td align="right">
                <strong><em>OrderInfo: </em></strong>
            </td>
            <td>
                <asp:Label ID="Label_OrderInfo" runat="server" />
            </td>
        </tr>
        <tr class="shade">
            <td align="right">
                <strong><em>Transaction Amount: </em></strong>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <div class='bl'>
                    Fields above are the primary request values.<hr>
                    Fields below are receipt data fields.</div>
            </td>
        </tr>
        <tr class="shade">
            <td align="right">
                <strong><em>Transaction Response Code: </em></strong>
            </td>
            <td>
                <asp:Label ID="Label_TxnResponseCode" runat="server" />
            </td>
        </tr>
        <tr>
            <td align="right">
                <strong><em>QSI Response Code Description: </em></strong>
            </td>
            <td>
                <asp:Label ID="Label_TxnResponseCodeDesc" runat="server" />
            </td>
        </tr>
        <tr >
            <td align="right">
                <strong><i>Message: </i></strong>
            </td>
            <td>
                <asp:Label ID="Label_Message" runat="server" />
            </td>
        </tr>
        <asp:Panel ID="Panel_Receipt" runat="server" Visible="false">
            <!-- only display these next fields if not an error -->
            <tr>
                <td align="right">
                    <strong><em>Shopping Transaction Number: </em></strong>
                </td>
                <td>
                    <asp:Label ID="Label_TransactionNo" runat="server" />
                </td>
            </tr>
            <tr class="shade">
                <td align="right">
                    <strong><em>Batch Number for this transaction: </em></strong>
                </td>
                <td>
                    <asp:Label ID="Label_BatchNo" runat="server" />
                </td>
            </tr>
            <tr>
                <td align="right">
                    <strong><em>Acquirer Response Code: </em></strong>
                </td>
                <td>
                    <asp:Label ID="Label_AcqResponseCode" runat="server" />
                </td>
            </tr>
            <tr class="shade">
                <td align="right">
                    <strong><em>Receipt Number: </em></strong>
                </td>
                <td>
                    <asp:Label ID="Label_ReceiptNo" runat="server" />
                </td>
            </tr>
            <tr>
                <td align="right">
                    <strong><em>Authorization ID: </em></strong>
                </td>
                <td>
                    <asp:Label ID="Label_AuthorizeID" runat="server" />
                </td>
            </tr>
            <tr class="shade">
                <td align="right">
                    <strong><em>Card Type: </em></strong>
                </td>
                <td>
                    <asp:Label ID="Label_CardType" runat="server" />
                </td>
            </tr>
        </asp:Panel>
        <tr>
            <td colspan="2" style="height: 32px">
                <hr />
                &nbsp;
            </td>
        </tr>
        <tr >
            <td colspan="2" height="25">
                <p>
                    <strong>&nbsp;Hash Validation</strong></p>
            </td>
        </tr>
        <tr>
            <td align="right">
                <strong><em>Hash Validated Correctly: </em></strong>
            </td>
            <td>
                <asp:Label ID="Label_HashValidation" runat="server" />
            </td>
        </tr>
        <asp:Panel ID="Panel_StackTrace" runat="server">
            <!-- only display these next fields if an stacktrace output exists-->
            <tr>
                <td colspan="2">
                    &nbsp;
                </td>
            </tr>
            <tr class="title">
                <td colspan="2">
                    <p>
                        <strong>&nbsp;Exception Stack Trace</strong></p>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Label ID="Label_StackTrace" runat="server" />
                </td>
            </tr>
        </asp:Panel>
        <tr>
            <td width="50%">
                &nbsp;
            </td>
            <td width="50%">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <asp:Label ID="Label_AgainLink" runat="server" />
            </td>
        </tr>
        <tr>
            <td align="center" colspan="2">
                <asp:Panel ID="Panel_Debug" runat="server" Visible="false">
                    <!-- only display these next fields if debug enabled -->
                    <table>
                        <tr>
                            <td>
                                <asp:Label ID="Label_Debug" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label_DigitalOrder" runat="server" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
    </table>
   
    <div class="footer"><div class="in">Copyright © 2011 GEMS Education. All Rights Reserved.</div></div>
     </div>
    </div>
 </div>
    </form>
</body>
</html>
