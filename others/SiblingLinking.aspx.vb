﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Net.Mail
Imports System.Text
Partial Class Others_SiblingLinking
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try

            If Session("username") Is Nothing Then
                Session("Active_tab") = "Home"
                Session("Site_Path") = ""
                Response.Redirect("~\General\Home.aspx")
            ElseIf Session("bPasswdChanged") = "False" Then
                Response.Redirect("~\UpdateInfo\ForcePasswordChange.aspx", False)
            ElseIf Session("bUpdateContactDetails") = "False" Then
                Response.Redirect(Session("ForceUpdate_stud"), False)
            End If
            'lbChildName.Text = Session("STU_NAME")

            'ViewState("viewid") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
            If Page.IsPostBack = False Then
               
                'Me.rbYes.Attributes.Add("onClick", "return FancyDIV();")
                Me.trOTP.Visible = False
                Me.trbtnSearch.Visible = False
              
            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        lblPopError.CssClass = ""
        lblPopError.Text = ""

        check_sibling()
        If ViewState("isExist") = 1 Then
            save_sibling()

            If ViewState("FINAL_STATUS") = "CONFIRMED" Then
                trOTP.Visible = False
                trbtnSearch.Visible = False
                trSave.Visible = False
                lblPopError.CssClass = "text-danger"

                lblPopError.Text = "Sibling already added.."
            Else
                trOTP.Visible = True
                trbtnSearch.Visible = True
                trSave.Visible = False
                Label5.Text = "*OTP has been sent to your registered Email " + getEmail(txtEmail.Text)
            End If

        Else
            trOTP.Visible = False
            trbtnSearch.Visible = False
            trSave.Visible = True
            lblPopError.CssClass = "text-danger"

            lblPopError.Text = "Student not found"
        End If

    End Sub

    Sub check_sibling()
        Dim connection As String = ConnectionManger.GetOASISConnectionString
        Dim pParms(5) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@EMAIL", txtEmail.Text)
        pParms(1) = New SqlClient.SqlParameter("@DOB", txtDOB.Text)
        pParms(2) = New SqlClient.SqlParameter("@STU_NO", txtAppName.Text)
        pParms(3) = New SqlClient.SqlParameter("@BSU_ID", Session("STU_BSU_ID"))

        Dim ds As DataSet = SqlHelper.ExecuteDataset(connection, CommandType.StoredProcedure, "[OPL].[CHECK_SIBLING_LINK_NEW]", pParms)
        If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
            ViewState("isExist") = ds.Tables(0).Rows(0).Item("RESULT")
            ViewState("MOBNO") = ds.Tables(0).Rows(0).Item("MOB_NO")
            ViewState("SEL_OLU_NAME") = ds.Tables(0).Rows(0).Item("OLU_NAME")
            Session("SIB_STU_ID") = ds.Tables(0).Rows(0).Item("STU_ID")
            Session("SIB_STU_NAME") = ds.Tables(0).Rows(0).Item("STU_NAME")
        End If
    End Sub
    Sub save_sibling()
        Dim connection As String = ConnectionManger.GetOASISConnectionString
        Dim pParms(3) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@SBS_OLU_NAME", Session("username"))
        pParms(1) = New SqlClient.SqlParameter("@SBS_SIB_OLU_NAME", ViewState("SEL_OLU_NAME"))
        

        Dim ds As DataSet = SqlHelper.ExecuteDataset(connection, CommandType.StoredProcedure, "[OPL].[SAVE_SIBLINGS_SET]", pParms)
        If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
            ViewState("SBS_ID") = ds.Tables(0).Rows(0).Item("SBS_ID")
            ViewState("OTP") = ds.Tables(0).Rows(0).Item("OTP")
            ViewState("FINAL_STATUS") = ds.Tables(0).Rows(0).Item("FINAL_STATUS")
        End If
    End Sub
    Sub update_sibling()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "update opl.SIBLINGS_SET set SBS_CONFIRM='TRUE' where SBS_ID=" & ViewState("SBS_ID")
        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)

    End Sub
    Protected Sub btnEmpSearch_Click(sender As Object, e As EventArgs) Handles btnEmpSearch.Click
        lblPopError.CssClass = ""
        lblPopError.Text = ""

        If ViewState("OTP") = txtOTP.Text Then
            update_sibling()
            Me.testpopup.Style.Item("display") = "block"
            lblPopError.CssClass = "alert alert-danger"
            lblPopError.Text = "Sibling Added sucessfully..."
        Else
            lblPopError.CssClass = "alert alert-danger"
            lblPopError.Text = "OTP is not matching.."
        End If



    End Sub
    Function getEmail(ByVal e As String) As String
        Dim s As String = ""
        s = Left(e, 3) + "*****@" + e.Substring(e.IndexOf("@") + 1, 2) + "*******" + Right(e, 4)

        Return s
    End Function

    Protected Sub btnCancel_Click(sender As Object, e As EventArgs)
        Me.testpopup.Style.Item("display") = "none"
    End Sub
End Class
