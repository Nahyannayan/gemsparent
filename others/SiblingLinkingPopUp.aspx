﻿<%@ Page Title="" Language="VB"  AutoEventWireup="false" CodeFile="SiblingLinkingPopUp.aspx.vb" Inherits="Others_SiblingLinking" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%--<asp:Content ID="Content1" ContentPlaceHolderID="cphParent" runat="Server">--%>
<head>
           <!-- Library CSS -->
        <link rel="stylesheet" href="../css/bootstrap.css"/>
        <link rel="stylesheet" href="../css/bootstrap-theme.css"/>
        <link rel="stylesheet" href="../css/fonts/font-awesome/css/font-awesome.css"/>
        <link rel="stylesheet" href="../css/animations.css" media="screen"/>
    <link rel="stylesheet" href="../css/animate.css" media="screen"/>
        
       
        <!-- Theme CSS -->
        <link rel="stylesheet" href="../css/style.css"/>
        <!-- Skin -->
        <link rel="stylesheet" href="../css/colors/blue.css" class="colors"/>
        <!-- Responsive CSS -->
        <link rel="stylesheet" href="../css/theme-responsive.css"/>
        <!-- Switcher CSS -->
        <link href="../css/switcher.css" rel="stylesheet"/>
        <link href="../css/spectrum.css" rel="stylesheet"/>
    <style>
        .FirstChild{
            background-color:rgba(255, 255, 255, .50);
        }
         .SecondChild{
            background-color:rgba(216, 216, 216, .5);
        }
    </style>
    <script>

        // father--------------------------------------------------------------------------------
        function copytxtFEmiratesId(chk) {
           
            if (chk.checked) {
                document.getElementById('<%=txtFEmiratesId2.ClientID%>').value = document.getElementById('<%=txtFEmiratesId.ClientID%>').value;
            }
            else {
                document.getElementById('<%=txtFEmiratesId2.ClientID%>').value = ""
            }
        }

        function copytxtFResAddressStreet(chk) {
           
            if (chk.checked) {
               
                document.getElementById('<%=txtFResAddressStreet2.ClientID%>').value = document.getElementById('<%=txtFResAddressStreet.ClientID%>').value;
            }
            else {
                document.getElementById('<%=txtFResAddressStreet2.ClientID%>').value = ""
            }
        }
        function copytxtFResAddArea(chk) {
           
            if (chk.checked) {
                
                document.getElementById('<%=ddlFArea2.ClientID%>').selectedIndex = document.getElementById('<%=ddlFArea.ClientID%>').selectedIndex;
                document.getElementById('<%=txtFResAddArea2.ClientID%>').value = document.getElementById('<%=txtFResAddArea.ClientID%>').value;
            }
            else {
                document.getElementById('<%=txtFResAddArea2.ClientID%>').value = ""
                document.getElementById('<%=ddlFArea.ClientID%>').selectedIndex = 0
            }
        }
        function copytxtFResAddBuilding(chk) {
           
            if (chk.checked) {
               
                document.getElementById('<%=txtFResAddBuilding2.ClientID%>').value = document.getElementById('<%=txtFResAddBuilding.ClientID%>').value;
            }
            else {
                document.getElementById('<%=txtFResAddBuilding2.ClientID%>').value = ""
            }
        }
        function copytxtFResApartment(chk) {
           
            if (chk.checked) {
               
                document.getElementById('<%=txtFResApartment2.ClientID%>').value = document.getElementById('<%=txtFResApartment.ClientID%>').value;
            }
            else {
                document.getElementById('<%=txtFResApartment2.ClientID%>').value = ""
            }
        }
        function copyddlFCOMEmirate(chk) {
           
            if (chk.checked) {
               
                document.getElementById('<%=ddlFCOMEmirate2.ClientID%>').selectedIndex = document.getElementById('<%=ddlFCOMEmirate.ClientID%>').selectedIndex;
            }
            else {
                document.getElementById('<%=ddlFCOMEmirate2.ClientID%>').selectedIndex = 0
            }
        }
        function copytxtFPOBOX(chk) {
           
            if (chk.checked) {
               
                document.getElementById('<%=txtFPOBOX2.ClientID%>').value = document.getElementById('<%=txtFPOBOX.ClientID%>').value;
            }
            else {
                document.getElementById('<%=txtFPOBOX2.ClientID%>').value = ""
            }
        }
 function copytxtFPhone(chk) {
           
            if (chk.checked) {
               
                document.getElementById('<%=txtFMobile_Country2.ClientID%>').value = document.getElementById('<%=txtFMobile_Country.ClientID%>').value;
                document.getElementById('<%=txtFMobile_Area2.ClientID%>').value = document.getElementById('<%=txtFMobile_Area.ClientID %>').value;
                document.getElementById('<%=txtFMobile_No2.ClientID%>').value = document.getElementById('<%=txtFMobile_No.ClientID %>').value;
            }
            else {
                document.getElementById('<%=txtFMobile_Country2.ClientID%>').value = ""
                document.getElementById('<%=txtFMobile_Area2.ClientID%>').value = ""
                document.getElementById('<%=txtFMobile_No2.ClientID%>').value = ""
            }
        }
        function copytxtFOccupation(chk) {
           
            if (chk.checked) {
               
                document.getElementById('<%=txtFOccupation2.ClientID%>').value = document.getElementById('<%=txtFOccupation.ClientID%>').value;
            }
            else {
                document.getElementById('<%=txtFOccupation2.ClientID%>').value = ""
            }
        }
        function copyddlFCompany_Name(chk) {
           
            if (chk.checked) {

                document.getElementById('<%=ddlFCompany_Name2.ClientID%>').selectedIndex = document.getElementById('<%=ddlFCompany_Name.ClientID%>').selectedIndex;
                document.getElementById('<%=txtFComp_Name2.ClientID%>').value = document.getElementById('<%=txtFComp_Name.ClientID%>').value;
            }
            else {
                document.getElementById('<%=ddlFCompany_Name2.ClientID%>').selectedIndex = 0
                document.getElementById('<%=txtFComp_Name2.ClientID%>').value = ""
            }
        }
        function copyddlFNationality_Birth(chk) {
           
            if (chk.checked) {

                document.getElementById('<%=ddlFNationality_Birth2.ClientID%>').selectedIndex = document.getElementById('<%=ddlFNationality_Birth.ClientID%>').selectedIndex;
             }
             else {
                 document.getElementById('<%=ddlFNationality_Birth2.ClientID%>').selectedIndex = 0
             }
        }
        function copytxtFemail(chk) {
           
            if (chk.checked) {
               
                document.getElementById('<%=txtFemail2.ClientID%>').value = document.getElementById('<%=txtFemail.ClientID%>').value;
            }
            else {
                document.getElementById('<%=txtFemail2.ClientID%>').value = ""
            }
        }
        // Mother--------------------------------------------------------------------------------
        function copytxtMEmiratesId(chk) {

            if (chk.checked) {
                document.getElementById('<%=txtMEmiratesId2.ClientID%>').value = document.getElementById('<%=txtMEmiratesId.ClientID%>').value;
            }
            else {
                document.getElementById('<%=txtMEmiratesId2.ClientID%>').value = ""
            }
        }

        function copytxtMResAddressStreet(chk) {

            if (chk.checked) {

                document.getElementById('<%=txtMResAddressStreet2.ClientID%>').value = document.getElementById('<%=txtMResAddressStreet.ClientID%>').value;
            }
            else {
                document.getElementById('<%=txtMResAddressStreet2.ClientID%>').value = ""
            }
        }
        function copytxtMResAddArea(chk) {

            if (chk.checked) {
                document.getElementById('<%=ddlMArea2.ClientID%>').selectedIndex = document.getElementById('<%=ddlMArea.ClientID%>').selectedIndex;
                document.getElementById('<%=txtMResAddArea2.ClientID%>').value = document.getElementById('<%=txtMResAddArea.ClientID%>').value;
            }
            else {
                document.getElementById('<%=ddlMArea2.ClientID%>').selectedIndex = 0
                document.getElementById('<%=txtMResAddArea2.ClientID%>').value = ""
            }
        }
        function copytxtMResAddBuilding(chk) {

            if (chk.checked) {

                document.getElementById('<%=txtMResAddBuilding2.ClientID%>').value = document.getElementById('<%=txtMResAddBuilding.ClientID%>').value;
            }
            else {
                document.getElementById('<%=txtMResAddBuilding2.ClientID%>').value = ""
            }
        }
        function copytxtMResApartment(chk) {

            if (chk.checked) {

                document.getElementById('<%=txtMResApartment2.ClientID%>').value = document.getElementById('<%=txtMResApartment.ClientID%>').value;
            }
            else {
                document.getElementById('<%=txtMResApartment2.ClientID%>').value = ""
            }
        }
        function copyddlMCOMEmirate(chk) {

            if (chk.checked) {

                document.getElementById('<%=ddlMCOMEmirate2.ClientID%>').selectedIndex = document.getElementById('<%=ddlMCOMEmirate.ClientID%>').selectedIndex;
            }
            else {
                document.getElementById('<%=ddlMCOMEmirate2.ClientID%>').selectedIndex = 0
            }
        }
        function copytxtMPOBOX(chk) {

            if (chk.checked) {

                document.getElementById('<%=txtMPOBOX2.ClientID%>').value = document.getElementById('<%=txtMPOBOX.ClientID%>').value;
            }
            else {
                document.getElementById('<%=txtMPOBOX2.ClientID%>').value = ""
            }
        }
        function copytxtMPhone(chk) {

            if (chk.checked) {

                document.getElementById('<%=txtMMobile_Country2.ClientID%>').value = document.getElementById('<%=txtMMobile_Country.ClientID%>').value;
                document.getElementById('<%=txtMMobile_Area2.ClientID%>').value = document.getElementById('<%=txtMMobile_Area.ClientID%>').value;
                document.getElementById('<%=txtMMobile_No2.ClientID%>').value = document.getElementById('<%=txtMMobile_No.ClientID%>').value;
            }
            else {
                document.getElementById('<%=txtMMobile_Country2.ClientID%>').value = ""
                document.getElementById('<%=txtMMobile_Area2.ClientID%>').value = ""
                document.getElementById('<%=txtMMobile_No2.ClientID%>').value = ""
            }
        }
        function copytxtMOccupation(chk) {

            if (chk.checked) {

                document.getElementById('<%=txtMOccupation2.ClientID%>').value = document.getElementById('<%=txtMOccupation.ClientID%>').value;
            }
            else {
                document.getElementById('<%=txtMOccupation2.ClientID%>').value = ""
            }
        }
        function copyddlMCompany_Name(chk) {

            if (chk.checked) {

                document.getElementById('<%=ddlMCompany_Name2.ClientID%>').selectedIndex = document.getElementById('<%=ddlMCompany_Name.ClientID%>').selectedIndex;
                document.getElementById('<%=txtMComp_Name2.ClientID%>').value = document.getElementById('<%=txtMComp_Name.ClientID%>').value;
            }
            else {
                document.getElementById('<%=ddlMCompany_Name2.ClientID%>').selectedIndex = 0
                document.getElementById('<%=txtMComp_Name2.ClientID%>').value = ""
            }
        }
        function copyddlMNationality_Birth(chk) {

            if (chk.checked) {

                document.getElementById('<%=ddlMNationality_Birth2.ClientID%>').selectedIndex = document.getElementById('<%=ddlMNationality_Birth.ClientID%>').selectedIndex;
            }
            else {
                document.getElementById('<%=ddlMNationality_Birth2.ClientID%>').selectedIndex = 0
            }
        }
        function copytxtMemail(chk) {

            if (chk.checked) {

                document.getElementById('<%=txtMemail2.ClientID%>').value = document.getElementById('<%=txtMemail.ClientID%>').value;
            }
            else {
                document.getElementById('<%=txtMemail2.ClientID%>').value = ""
            }
        }
        function copyAll(chk) {

            if (chk.checked) {

                document.getElementById('<%=txtMemail2.ClientID%>').value = document.getElementById('<%=txtMemail.ClientID%>').value;
                document.getElementById('<%=ddlMNationality_Birth2.ClientID%>').selectedIndex = document.getElementById('<%=ddlMNationality_Birth.ClientID%>').selectedIndex;
                document.getElementById('<%=ddlMCompany_Name2.ClientID%>').selectedIndex = document.getElementById('<%=ddlMCompany_Name.ClientID%>').selectedIndex;
                document.getElementById('<%=txtMComp_Name2.ClientID%>').value = document.getElementById('<%=txtMComp_Name.ClientID%>').value;
                document.getElementById('<%=txtMOccupation2.ClientID%>').value = document.getElementById('<%=txtMOccupation.ClientID%>').value;
                document.getElementById('<%=txtMMobile_Country2.ClientID%>').value = document.getElementById('<%=txtMMobile_Country.ClientID%>').value;
                document.getElementById('<%=txtMMobile_Area2.ClientID%>').value = document.getElementById('<%=txtMMobile_Area.ClientID%>').value;
                document.getElementById('<%=txtMMobile_No2.ClientID%>').value = document.getElementById('<%=txtMMobile_No.ClientID%>').value;
                document.getElementById('<%=txtMPOBOX2.ClientID%>').value = document.getElementById('<%=txtMPOBOX.ClientID%>').value;
                document.getElementById('<%=ddlMCOMEmirate2.ClientID%>').selectedIndex = document.getElementById('<%=ddlMCOMEmirate.ClientID%>').selectedIndex;
                document.getElementById('<%=txtMResApartment2.ClientID%>').value = document.getElementById('<%=txtMResApartment.ClientID%>').value;
                document.getElementById('<%=txtMResAddBuilding2.ClientID%>').value = document.getElementById('<%=txtMResAddBuilding.ClientID%>').value;
                document.getElementById('<%=txtMResAddArea2.ClientID%>').value = document.getElementById('<%=txtMResAddArea.ClientID%>').value;
                document.getElementById('<%=txtMResAddressStreet2.ClientID%>').value = document.getElementById('<%=txtMResAddressStreet.ClientID%>').value;
                document.getElementById('<%=txtMEmiratesId2.ClientID%>').value = document.getElementById('<%=txtMEmiratesId.ClientID%>').value;
                document.getElementById('<%=ddlMArea2.ClientID%>').selectedIndex = document.getElementById('<%=ddlMArea.ClientID%>').selectedIndex;


                document.getElementById('<%=txtFemail2.ClientID%>').value = document.getElementById('<%=txtFemail.ClientID%>').value;
                document.getElementById('<%=ddlFNationality_Birth2.ClientID%>').selectedIndex = document.getElementById('<%=ddlFNationality_Birth.ClientID%>').selectedIndex;
                document.getElementById('<%=ddlFCompany_Name2.ClientID%>').selectedIndex = document.getElementById('<%=ddlFCompany_Name.ClientID%>').selectedIndex;
                document.getElementById('<%=txtFComp_Name2.ClientID%>').value = document.getElementById('<%=txtFComp_Name.ClientID%>').value;
                document.getElementById('<%=txtFOccupation2.ClientID%>').value = document.getElementById('<%=txtFOccupation.ClientID%>').value;
                document.getElementById('<%=txtFMobile_Country2.ClientID%>').value = document.getElementById('<%=txtFMobile_Country.ClientID%>').value;
                document.getElementById('<%=txtFMobile_Area2.ClientID%>').value = document.getElementById('<%=txtFMobile_Area.ClientID%>').value;
                document.getElementById('<%=txtFMobile_No2.ClientID%>').value = document.getElementById('<%=txtFMobile_No.ClientID%>').value;
                document.getElementById('<%=txtFPOBOX2.ClientID%>').value = document.getElementById('<%=txtFPOBOX.ClientID%>').value;
                document.getElementById('<%=ddlFCOMEmirate2.ClientID%>').selectedIndex = document.getElementById('<%=ddlFCOMEmirate.ClientID%>').selectedIndex;
                document.getElementById('<%=txtFResApartment2.ClientID%>').value = document.getElementById('<%=txtFResApartment.ClientID%>').value;
                document.getElementById('<%=txtFResAddBuilding2.ClientID%>').value = document.getElementById('<%=txtFResAddBuilding.ClientID%>').value;
                document.getElementById('<%=txtFResAddArea2.ClientID%>').value = document.getElementById('<%=txtFResAddArea.ClientID%>').value;
                document.getElementById('<%=txtFResAddressStreet2.ClientID%>').value = document.getElementById('<%=txtFResAddressStreet.ClientID%>').value;
                document.getElementById('<%=txtFEmiratesId2.ClientID%>').value = document.getElementById('<%=txtFEmiratesId.ClientID%>').value;
                document.getElementById('<%=ddlFArea2.ClientID%>').selectedIndex = document.getElementById('<%=ddlFArea.ClientID%>').selectedIndex;

                document.getElementById('<%=CheckBox1.ClientID%>').checked = true
                document.getElementById('<%=CheckBox2.ClientID%>').checked = true
                document.getElementById('<%=CheckBox3.ClientID%>').checked = true
                document.getElementById('<%=CheckBox4.ClientID%>').checked = true
                document.getElementById('<%=CheckBox5.ClientID%>').checked = true
                document.getElementById('<%=CheckBox6.ClientID%>').checked = true
                document.getElementById('<%=CheckBox7.ClientID%>').checked = true
                document.getElementById('<%=CheckBox8.ClientID%>').checked = true
                document.getElementById('<%=CheckBox9.ClientID%>').checked = true
                document.getElementById('<%=CheckBox10.ClientID%>').checked = true
                document.getElementById('<%=CheckBox11.ClientID%>').checked = true
                document.getElementById('<%=CheckBox12.ClientID%>').checked = true
                document.getElementById('<%=CheckBox13.ClientID%>').checked = true
                document.getElementById('<%=CheckBox14.ClientID%>').checked = true
                document.getElementById('<%=CheckBox15.ClientID%>').checked = true
                document.getElementById('<%=CheckBox16.ClientID%>').checked = true
                document.getElementById('<%=CheckBox17.ClientID%>').checked = true
                document.getElementById('<%=CheckBox18.ClientID%>').checked = true
                document.getElementById('<%=CheckBox19.ClientID%>').checked = true
                document.getElementById('<%=CheckBox20.ClientID%>').checked = true
                document.getElementById('<%=CheckBox21.ClientID%>').checked = true
                document.getElementById('<%=CheckBox22.ClientID%>').checked = true
                document.getElementById('<%=CheckBox23.ClientID%>').checked = true
                document.getElementById('<%=CheckBox25.ClientID%>').checked = true





            }
            else {
                document.getElementById('<%=txtMemail2.ClientID%>').value = ""
                document.getElementById('<%=txtMEmiratesId2.ClientID%>').value = ""
                document.getElementById('<%=txtMResAddressStreet2.ClientID%>').value = ""
                document.getElementById('<%=txtMResAddArea2.ClientID%>').value = ""
                document.getElementById('<%=txtMResAddBuilding2.ClientID%>').value = ""
                document.getElementById('<%=txtMResApartment2.ClientID%>').value = ""
                document.getElementById('<%=ddlMCOMEmirate2.ClientID%>').selectedIndex = 0
                document.getElementById('<%=txtMPOBOX2.ClientID%>').value = ""
                document.getElementById('<%=txtMMobile_Country2.ClientID%>').value = ""
                document.getElementById('<%=txtMMobile_Area2.ClientID%>').value = ""
                document.getElementById('<%=txtMMobile_No2.ClientID%>').value = ""
                document.getElementById('<%=txtMOccupation2.ClientID%>').value = ""
                document.getElementById('<%=ddlMCompany_Name2.ClientID%>').selectedIndex = 0
                document.getElementById('<%=txtMComp_Name2.ClientID%>').value = ""
                document.getElementById('<%=ddlMNationality_Birth2.ClientID%>').selectedIndex = 0
                document.getElementById('<%=txtMemail2.ClientID%>').value = ""
                document.getElementById('<%=ddlMArea2.ClientID%>').selectedIndex = 0

                document.getElementById('<%=txtFemail2.ClientID%>').value = ""
                document.getElementById('<%=txtFEmiratesId2.ClientID%>').value = ""
                document.getElementById('<%=txtFResAddressStreet2.ClientID%>').value = ""
                document.getElementById('<%=txtFResAddArea2.ClientID%>').value = ""
                document.getElementById('<%=txtFResAddBuilding2.ClientID%>').value = ""
                document.getElementById('<%=txtFResApartment2.ClientID%>').value = ""
                document.getElementById('<%=ddlFCOMEmirate2.ClientID%>').selectedIndex = 0
                document.getElementById('<%=txtFPOBOX2.ClientID%>').value = ""
                document.getElementById('<%=txtFMobile_Country2.ClientID%>').value = ""
                document.getElementById('<%=txtFMobile_Area2.ClientID%>').value = ""
                document.getElementById('<%=txtFMobile_No2.ClientID%>').value = ""
                document.getElementById('<%=txtFOccupation2.ClientID%>').value = ""
                document.getElementById('<%=ddlFCompany_Name2.ClientID%>').selectedIndex = 0
                document.getElementById('<%=txtFComp_Name2.ClientID%>').value = ""
                document.getElementById('<%=ddlFNationality_Birth2.ClientID%>').selectedIndex = 0
                document.getElementById('<%=txtFemail2.ClientID%>').value = ""
                document.getElementById('<%=ddlFArea2.ClientID%>').selectedIndex = 0

                document.getElementById('<%=CheckBox1.ClientID%>').checked   = false
                document.getElementById('<%=CheckBox2.ClientID%>').checked   = false
                document.getElementById('<%=CheckBox3.ClientID%>').checked   = false
                document.getElementById('<%=CheckBox4.ClientID%>').checked   = false
                document.getElementById('<%=CheckBox5.ClientID%>').checked   = false
                document.getElementById('<%=CheckBox6.ClientID%>').checked   = false
                document.getElementById('<%=CheckBox7.ClientID%>').checked   = false
                document.getElementById('<%=CheckBox8.ClientID%>').checked   = false
                document.getElementById('<%=CheckBox9.ClientID%>').checked   = false
                document.getElementById('<%=CheckBox10.ClientID%>').checked  = false
                document.getElementById('<%=CheckBox11.ClientID%>').checked  = false
                document.getElementById('<%=CheckBox12.ClientID%>').checked  = false
                document.getElementById('<%=CheckBox13.ClientID%>').checked  = false
                document.getElementById('<%=CheckBox14.ClientID%>').checked  = false
                document.getElementById('<%=CheckBox15.ClientID%>').checked  = false
                document.getElementById('<%=CheckBox16.ClientID%>').checked  = false
                document.getElementById('<%=CheckBox17.ClientID%>').checked  = false
                document.getElementById('<%=CheckBox18.ClientID%>').checked  = false
                document.getElementById('<%=CheckBox19.ClientID%>').checked  = false
                document.getElementById('<%=CheckBox20.ClientID%>').checked  = false
                document.getElementById('<%=CheckBox21.ClientID%>').checked  = false
                document.getElementById('<%=CheckBox22.ClientID%>').checked  = false
                document.getElementById('<%=CheckBox23.ClientID%>').checked  = false
                document.getElementById('<%=CheckBox25.ClientID%>').checked  = false


            }
        }
    </script>

</head>
<body>
    <form id="fromBody" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="True">
        </asp:ScriptManager>
    <div class="content margin-top30 margin-bottom60">
        <div class="container">
            <div class="row">

                <!-- Posts Block -->

                <div class="posts-block col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="my-account">
                      <div  class="alert alert-warning">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <div id="screen" runat="server">
                                Please go through the information of the student being linked and click on merge if you would like to keep it same across all students.
                            </div>
                        </div>
                        <table width="100%" cellspacing="0" cellpadding="3" class="table table-striped table-bordered table-responsive text-left my-orders-table">
                            <tr >
                                <td class="sub-heading"></td>
                                <td class="sub-heading"><asp:Label ID="lblStudentName" runat="server" Text="Student1"></asp:Label></td>
                                <td><asp:CheckBox ID="CheckBox24" runat="server" CssClass="checkboxHeader" onclick="javascript:return copyAll(this);" /> All</td>
                                <td class="sub-heading"><asp:Label ID="lblStudentName2" runat="server" Text="Student2"></asp:Label></td>
                            </tr>
                             <tr>
                                <td><strong>Primary Contact</strong></td>
                                <td class="FirstChild"><asp:Label ID="lblPrimaryContact" runat="server"></asp:Label></td>
                                 <td></td>
                                <td class="SecondChild"><asp:Label ID="lblPrimaryContact2" runat="server"></asp:Label></td>
                            </tr>
                            <tr>
                                    <td colspan="4" class="sub-heading">Father Details
                                    </td>
                                </tr>
                            <tr>
                             
                                    <td style="vertical-align: middle"><strong>Full Name as per passport</strong>
                                    </td>
                                    <td  style="vertical-align: middle" class="FirstChild">
                                        <asp:label ID="lblFName" runat="server" Text="Father1"></asp:label>
                                    </td>
                                <td></td>
                                                <td  style="vertical-align: middle" class="SecondChild">
                                        <asp:label ID="lblFName2" runat="server" Text="Father2"></asp:label>
                                    </td>
                                </tr>
                            <tr>
                             
                                    <td style="vertical-align: middle"><strong>Emirates ID</strong>
                                    </td>
                                    <td  style="vertical-align: middle" class="FirstChild">
                                        <asp:textbox ID="txtFEmiratesId" runat="server" CssClass="form-control" ReadOnly="true" ></asp:textbox>
                                    </td>
                                <td><asp:CheckBox ID="CheckBox25" runat="server" CssClass="checkboxHeader" onclick="javascript:return copytxtFEmiratesId(this);" /></td>
                                                <td  style="vertical-align: middle" class="SecondChild">
                                          <asp:textbox ID="txtFEmiratesId2" runat="server" CssClass="form-control" ></asp:textbox>
                                    </td>
                                </tr>
                                  <tr>
                             
                                    <td style="vertical-align: middle"><strong>Nationality</strong>
                                    </td>

                                    <td  style="vertical-align: middle" class="FirstChild">
                                        <asp:DropDownList ID="ddlFNationality_Birth" runat="server" CssClass="form-control" Enabled="false">
                                            </asp:DropDownList>
                                        
                                    </td>
                                      <td><asp:CheckBox ID="CheckBox22" runat="server" CssClass="checkboxHeader" onclick="javascript:return copyddlFNationality_Birth(this);" /></td>
                                                <td  style="vertical-align: middle" class="SecondChild">
                                          <asp:DropDownList ID="ddlFNationality_Birth2" runat="server" CssClass="form-control">
                                            </asp:DropDownList>
                                    </td>
                                </tr>
                            <tr>
                             
                                    <td style="vertical-align: middle"><strong>Residential Address Area</strong>
                                    </td>
                                    <td  style="vertical-align: middle" class="FirstChild">
                                        <asp:DropDownList ID="ddlFArea" runat="server" CssClass="form-control" Enabled="false">
                                            </asp:DropDownList>
                                        <asp:TextBox ID="txtFResAddArea" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                    </td>
                                <td><asp:CheckBox ID="CheckBox2" runat="server" CssClass="checkboxHeader" onclick="javascript:return copytxtFResAddArea(this);" /></td>
                                                <td  style="vertical-align: middle" class="SecondChild">
                                                    <asp:DropDownList ID="ddlFArea2" runat="server" CssClass="form-control">
                                            </asp:DropDownList>
                                          <asp:TextBox ID="txtFResAddArea2" runat="server" CssClass="form-control"></asp:TextBox>
                                    </td>
                                </tr>
                            <tr>
                             
                                    <td style="vertical-align: middle"><strong>Residential Address Street</strong>
                                    </td>
                                    <td  style="vertical-align: middle" class="FirstChild">
                                        <asp:TextBox ID="txtFResAddressStreet" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                    </td>
                                <td><asp:CheckBox ID="CheckBox1" runat="server" CssClass="checkboxHeader" onclick="javascript:return copytxtFResAddressStreet(this);" /></td>
                                                <td  style="vertical-align: middle" class="SecondChild">
                                          <asp:TextBox ID="txtFResAddressStreet2" runat="server" CssClass="form-control"></asp:TextBox>
                                    </td>
                                </tr>
                             <tr>
                             
                                    <td style="vertical-align: middle"><strong>Residential Address Building</strong>
                                    </td>
                                    <td  style="vertical-align: middle" class="FirstChild">
                                       <asp:TextBox ID="txtFResAddBuilding" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                    </td>
                                 <td><asp:CheckBox ID="CheckBox3" runat="server" CssClass="checkboxHeader" onclick="javascript:return copytxtFResAddBuilding(this);" /></td>
                                                <td  style="vertical-align: middle" class="SecondChild">
                                          <asp:TextBox ID="txtFResAddBuilding2" runat="server" CssClass="form-control"></asp:TextBox>
                                    </td>
                                </tr>

                             <tr>
                             
                                    <td style="vertical-align: middle"><strong>Residential Address Apartment No</strong>
                                    </td>
                                    <td  style="vertical-align: middle" class="FirstChild">
                                       <asp:TextBox ID="txtFResApartment" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                    </td>
                                 <td><asp:CheckBox ID="CheckBox4" runat="server" CssClass="checkboxHeader" onclick="javascript:return copytxtFResApartment(this);" /></td>
                                                <td  style="vertical-align: middle" class="SecondChild">
                                          <asp:TextBox ID="txtFResApartment2" runat="server" CssClass="form-control"></asp:TextBox>
                                    </td>
                                </tr>

                             <tr>
                             
                                    <td style="vertical-align: middle"><strong>City/Emirate</strong>
                                    </td>
                                    <td  style="vertical-align: middle" class="FirstChild">
                                        <asp:DropDownList ID="ddlFCOMEmirate" runat="server" CssClass="form-control" ReadOnly="true" Enabled="false"></asp:DropDownList>
                                    </td>
                                 <td><asp:CheckBox ID="CheckBox5" runat="server" CssClass="checkboxHeader" onclick="javascript:return copyddlFCOMEmirate(this);" /></td>
                                                <td  style="vertical-align: middle" class="SecondChild">
                                          <asp:DropDownList ID="ddlFCOMEmirate2" runat="server" CssClass="form-control"></asp:DropDownList>
                                    </td>
                                </tr>

                             <tr>
                             
                                    <td style="vertical-align: middle"><strong>P.O Box</strong>
                                    </td>
                                    <td  style="vertical-align: middle" class="FirstChild">
                                       <asp:TextBox ID="txtFPOBOX" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                    </td>
                                 <td><asp:CheckBox ID="CheckBox6" runat="server" CssClass="checkboxHeader" onclick="javascript:return copytxtFPOBOX(this);" /></td>
                                                <td  style="vertical-align: middle" class="SecondChild">
                                          <asp:TextBox ID="txtFPOBOX2" runat="server" CssClass="form-control"></asp:TextBox>
                                    </td>
                                </tr>

                             <tr>
                             
                                    <td style="vertical-align: middle"><strong>Father's Mobile</strong>
                                    </td>
                                    <td  style="vertical-align: middle" class="FirstChild">
                                      <asp:TextBox ID="txtFMobile_Country" runat="server" Width="60px" MaxLength="3" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                        -
                                        <asp:TextBox ID="txtFMobile_Area" runat="server" Width="60px" MaxLength="4" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                        -
                                        <asp:TextBox ID="txtFMobile_No" runat="server" Width="90px" MaxLength="10" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                        <div class="remark">
                                            (Country-Area-Number)
                                        </div>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server"
                                            TargetControlID="txtFMobile_Country" FilterType="Numbers">
                                        </ajaxToolkit:FilteredTextBoxExtender>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server"
                                            TargetControlID="txtFMobile_Area" FilterType="Numbers">
                                        </ajaxToolkit:FilteredTextBoxExtender>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server"
                                            TargetControlID="txtFMobile_No" FilterType="Numbers">
                                        </ajaxToolkit:FilteredTextBoxExtender>
                                    </td>
                                 <td><asp:CheckBox ID="CheckBox7" runat="server" CssClass="checkboxHeader" onclick="javascript:return copytxtFPhone(this);" /></td>
                                                <td  style="vertical-align: middle" class="SecondChild">
                                        <asp:TextBox ID="txtFMobile_Country2" runat="server" Width="60px" MaxLength="3" CssClass="form-control"></asp:TextBox>
                                        -
                                        <asp:TextBox ID="txtFMobile_Area2" runat="server" Width="60px" MaxLength="4" CssClass="form-control"></asp:TextBox>
                                        -
                                        <asp:TextBox ID="txtFMobile_No2" runat="server" Width="90px" MaxLength="10" CssClass="form-control"></asp:TextBox>
                                        <div class="remark">
                                            (Country-Area-Number)
                                        </div>
                                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server"
                                            TargetControlID="txtFMobile_Country2" FilterType="Numbers">
                                        </ajaxToolkit:FilteredTextBoxExtender>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server"
                                            TargetControlID="txtFMobile_Area2" FilterType="Numbers">
                                        </ajaxToolkit:FilteredTextBoxExtender>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server"
                                            TargetControlID="txtFMobile_No2" FilterType="Numbers">
                                        </ajaxToolkit:FilteredTextBoxExtender>
                                    </td>
                                </tr>
                               <tr>
                             
                                    <td style="vertical-align: middle"><strong>Email Address</strong>
                                    </td>
                                    <td  style="vertical-align: middle" class="FirstChild">
                                       <asp:TextBox ID="txtFemail" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                    </td>
                                   <td><asp:CheckBox ID="CheckBox23" runat="server" CssClass="checkboxHeader" onclick="javascript:return copytxtFemail(this);" /></td>
                                                <td  style="vertical-align: middle" class="SecondChild">
                                          <asp:TextBox ID="txtFemail2" runat="server" CssClass="form-control"></asp:TextBox>
                                    </td>
                                </tr>
                               <tr>
                             
                                    <td style="vertical-align: middle"><strong>Occupation</strong>
                                    </td>
                                    <td  style="vertical-align: middle" class="FirstChild">
                                       <asp:TextBox ID="txtFOccupation" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                    </td>
                                   <td><asp:CheckBox ID="CheckBox8" runat="server" CssClass="checkboxHeader" onclick="javascript:return copytxtFOccupation(this);" /></td>
                                                <td  style="vertical-align: middle" class="SecondChild">
                                          <asp:TextBox ID="txtFOccupation2" runat="server" CssClass="form-control"></asp:TextBox>
                                    </td>
                                </tr>
                               <tr>
                             
                                    <td style="vertical-align: middle"><strong>Company</strong>
                                    </td>
                                    <td  style="vertical-align: middle" class="FirstChild">
                                       <asp:DropDownList ID="ddlFCompany_Name" runat="server" CssClass="form-control" ReadOnly="true" Enabled="false"></asp:DropDownList>
                                        <div class="remark">
                                            (If you choose other,please specify the Company Name)
                                        </div>
                                        <asp:TextBox ID="txtFComp_Name" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                    </td>
                                   <td><asp:CheckBox ID="CheckBox9" runat="server" CssClass="checkboxHeader" onclick="javascript:return copyddlFCompany_Name(this);" /></td>
                                                <td  style="vertical-align: middle" class="SecondChild">
                                           <asp:DropDownList ID="ddlFCompany_Name2" runat="server" CssClass="form-control"></asp:DropDownList>
                                                    <div class="remark">
                                            (If you choose other,please specify the Company Name)
                                        </div>
                                        <asp:TextBox ID="txtFComp_Name2" runat="server" CssClass="form-control"></asp:TextBox>
                                    </td>
                                </tr>
                           <tr>                                                                    <%-- MOTHERS PORTION STARTS HERE--%>
                                    <td colspan="4" class="sub-heading">Mother Details
                                    </td>
                                </tr>
                            <tr>
                             
                                    <td style="vertical-align: middle"><strong>Full Name as per passport</strong>
                                    </td>
                                    <td  style="vertical-align: middle" class="FirstChild">
                                        <asp:label ID="lblMName" runat="server" Text="Mother1" ReadOnly="true"></asp:label>
                                    </td>
                                <td></td>
                                                <td  style="vertical-align: middle" class="SecondChild">
                                        <asp:label ID="lblMName2" runat="server" Text="Mother2"></asp:label>
                                    </td>
                                </tr>
                            <tr>
                             
                                    <td style="vertical-align: middle"><strong>Emirates ID</strong>
                                    </td>
                                    <td  style="vertical-align: middle" class="FirstChild">
                                        <asp:textbox ID="txtMEmiratesId" runat="server" CssClass="form-control" ReadOnly="true" ></asp:textbox>
                                    </td>
                                <td><asp:CheckBox ID="CheckBox10" runat="server" CssClass="checkboxHeader" onclick="javascript:return copytxtMEmiratesId(this);" /></td>
                                                <td  style="vertical-align: middle" class="SecondChild">
                                          <asp:textbox ID="txtMEmiratesId2" runat="server" CssClass="form-control" ></asp:textbox>
                                    </td>
                                </tr>
                                  <tr>
                             
                                    <td style="vertical-align: middle"><strong>Nationality</strong>
                                    </td>
                                    <td  style="vertical-align: middle" class="FirstChild">
                                        <asp:DropDownList ID="ddlMNationality_Birth" runat="server" CssClass="form-control" ReadOnly="true" Enabled="false">
                                            </asp:DropDownList>
                                    </td>
                                      <td><asp:CheckBox ID="CheckBox11" runat="server" CssClass="checkboxHeader" onclick="javascript:return copyddlMNationality_Birth(this);" /></td>
                                                <td  style="vertical-align: middle" class="SecondChild">
                                                    <asp:DropDownList ID="ddlMNationality_Birth2" runat="server" CssClass="form-control">
                                            </asp:DropDownList>
                                          
                                    </td>
                                </tr>
                            <tr>
                             
                                    <td style="vertical-align: middle"><strong>Residential Address Area</strong>
                                    </td>
                                    <td  style="vertical-align: middle" class="FirstChild">
                                        <asp:DropDownList ID="ddlMArea" runat="server" CssClass="form-control" Enabled="false">
                                            </asp:DropDownList>
                                        <asp:TextBox ID="txtMResAddArea" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                    </td>
                                <td><asp:CheckBox ID="CheckBox13" runat="server" CssClass="checkboxHeader" onclick="javascript:return copytxtMResAddArea(this);" /></td>
                                                <td  style="vertical-align: middle" class="SecondChild">
                                                    <asp:DropDownList ID="ddlMArea2" runat="server" CssClass="form-control" >
                                            </asp:DropDownList>
                                          <asp:TextBox ID="txtMResAddArea2" runat="server" CssClass="form-control"></asp:TextBox>
                                    </td>
                                </tr>
                            <tr>
                             
                                    <td style="vertical-align: middle"><strong>Residential Address Street</strong>
                                    </td>
                                    <td  style="vertical-align: middle" class="FirstChild">
                                        <asp:TextBox ID="txtMResAddressStreet" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                    </td>
                                <td><asp:CheckBox ID="CheckBox12" runat="server" CssClass="checkboxHeader" onclick="javascript:return copytxtMResAddressStreet(this);" /></td>
                                                <td  style="vertical-align: middle" class="SecondChild">
                                          <asp:TextBox ID="txtMResAddressStreet2" runat="server" CssClass="form-control"></asp:TextBox>
                                    </td>
                                </tr>
                             <tr>
                             
                                    <td style="vertical-align: middle"><strong>Residential Address Building</strong>
                                    </td>
                                    <td  style="vertical-align: middle" class="FirstChild">
                                       <asp:TextBox ID="txtMResAddBuilding" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                    </td>
                                 <td><asp:CheckBox ID="CheckBox14" runat="server" CssClass="checkboxHeader" onclick="javascript:return copytxtMResAddBuilding(this);" /></td>
                                                <td  style="vertical-align: middle" class="SecondChild">
                                          <asp:TextBox ID="txtMResAddBuilding2" runat="server" CssClass="form-control"></asp:TextBox>
                                    </td>
                                </tr>

                             <tr>
                             
                                    <td style="vertical-align: middle"><strong>Residential Address Apartment No</strong>
                                    </td>
                                    <td  style="vertical-align: middle" class="FirstChild">
                                       <asp:TextBox ID="txtMResApartment" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                    </td>
                                 <td><asp:CheckBox ID="CheckBox15" runat="server" CssClass="checkboxHeader" onclick="javascript:return copytxtMResApartment(this);" /></td>
                                                <td  style="vertical-align: middle" class="SecondChild">
                                          <asp:TextBox ID="txtMResApartment2" runat="server" CssClass="form-control"></asp:TextBox>
                                    </td>
                                </tr>

                             <tr>
                             
                                    <td style="vertical-align: middle"><strong>City/Emirate</strong>
                                    </td>
                                    <td  style="vertical-align: middle" class="FirstChild">
                                        <asp:DropDownList ID="ddlMCOMEmirate" runat="server" CssClass="form-control" ReadOnly="true" Enabled="false"></asp:DropDownList>
                                    </td>
                                 <td><asp:CheckBox ID="CheckBox16" runat="server" CssClass="checkboxHeader" onclick="javascript:return copyddlMCOMEmirate(this);" /></td>
                                                <td  style="vertical-align: middle" class="SecondChild">
                                          <asp:DropDownList ID="ddlMCOMEmirate2" runat="server" CssClass="form-control"></asp:DropDownList>
                                    </td>
                                </tr>

                             <tr>
                             
                                    <td style="vertical-align: middle"><strong>P.O Box</strong>
                                    </td>
                                    <td  style="vertical-align: middle" class="FirstChild">
                                       <asp:TextBox ID="txtMPOBOX" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                    </td>
                                 <td><asp:CheckBox ID="CheckBox17" runat="server" CssClass="checkboxHeader" onclick="javascript:return copytxtMPOBOX(this);" /></td>
                                                <td  style="vertical-align: middle" class="SecondChild">
                                          <asp:TextBox ID="txtMPOBOX2" runat="server" CssClass="form-control"></asp:TextBox>
                                    </td>
                                </tr>

                             <tr>
                             
                                    <td style="vertical-align: middle"><strong>Mother's Mobile</strong>
                                    </td>
                                    <td  style="vertical-align: middle" class="FirstChild">
                                      <asp:TextBox ID="txtMMobile_Country" runat="server" Width="60px" MaxLength="3" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                        -
                                        <asp:TextBox ID="txtMMobile_Area" runat="server" Width="60px" MaxLength="4" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                        -
                                        <asp:TextBox ID="txtMMobile_No" runat="server" Width="90px" MaxLength="10" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                        <div class="remark">
                                            (Country-Area-Number)
                                        </div>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server"
                                            TargetControlID="txtMMobile_Country" FilterType="Numbers">
                                        </ajaxToolkit:FilteredTextBoxExtender>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server"
                                            TargetControlID="txtMMobile_Area" FilterType="Numbers">
                                        </ajaxToolkit:FilteredTextBoxExtender>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server"
                                            TargetControlID="txtMMobile_No" FilterType="Numbers">
                                        </ajaxToolkit:FilteredTextBoxExtender>
                                    </td>
                                 <td><asp:CheckBox ID="CheckBox18" runat="server" CssClass="checkboxHeader" onclick="javascript:return copytxtMPhone(this);" /></td>
                                                <td  style="vertical-align: middle" class="SecondChild">
                                        <asp:TextBox ID="txtMMobile_Country2" runat="server" Width="60px" MaxLength="3" CssClass="form-control"></asp:TextBox>
                                        -
                                        <asp:TextBox ID="txtMMobile_Area2" runat="server" Width="60px" MaxLength="4" CssClass="form-control"></asp:TextBox>
                                        -
                                        <asp:TextBox ID="txtMMobile_No2" runat="server" Width="90px" MaxLength="10" CssClass="form-control"></asp:TextBox>
                                        <div class="remark">
                                            (Country-Area-Number)
                                        </div>
                                            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" runat="server"
                                            TargetControlID="txtMMobile_Country2" FilterType="Numbers">
                                        </ajaxToolkit:FilteredTextBoxExtender>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender11" runat="server"
                                            TargetControlID="txtMMobile_Area2" FilterType="Numbers">
                                        </ajaxToolkit:FilteredTextBoxExtender>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender12" runat="server"
                                            TargetControlID="txtMMobile_No2" FilterType="Numbers">
                                        </ajaxToolkit:FilteredTextBoxExtender>
                                    </td>
                                </tr>
                               <tr>
                             
                                    <td style="vertical-align: middle"><strong>Email Address</strong>
                                    </td>
                                    <td  style="vertical-align: middle" class="FirstChild">
                                       <asp:TextBox ID="txtMemail" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                    </td>
                                   <td><asp:CheckBox ID="CheckBox19" runat="server" CssClass="checkboxHeader" onclick="javascript:return copytxtMemail(this);" /></td>
                                                <td  style="vertical-align: middle" class="SecondChild">
                                          <asp:TextBox ID="txtMemail2" runat="server" CssClass="form-control"></asp:TextBox>
                                    </td>
                                </tr>
                               <tr>
                             
                                    <td style="vertical-align: middle"><strong>Occupation</strong>
                                    </td>
                                    <td  style="vertical-align: middle" class="FirstChild">
                                       <asp:TextBox ID="txtMOccupation" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                    </td>
                                   <td><asp:CheckBox ID="CheckBox20" runat="server" CssClass="checkboxHeader" onclick="javascript:return copytxtMOccupation(this);" /></td>
                                                <td  style="vertical-align: middle" class="SecondChild">
                                          <asp:TextBox ID="txtMOccupation2" runat="server" CssClass="form-control"></asp:TextBox>
                                    </td>
                                </tr>
                               <tr>
                             
                                    <td style="vertical-align: middle"><strong>Company</strong>
                                    </td>
                                    <td  style="vertical-align: middle" class="FirstChild">
                                       <asp:DropDownList ID="ddlMCompany_Name" runat="server" CssClass="form-control" ReadOnly="true" Enabled="false"></asp:DropDownList>
                                        <div class="remark">
                                            (If you choose other,please specify the Company Name)
                                        </div>
                                        <asp:TextBox ID="txtMComp_Name" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                    </td>
                                   <td><asp:CheckBox ID="CheckBox21" runat="server" CssClass="checkboxHeader" onclick="javascript:return copyddlMCompany_Name(this);" /></td>
                                                <td  style="vertical-align: middle" class="SecondChild">
                                           <asp:DropDownList ID="ddlMCompany_Name2" runat="server" CssClass="form-control"></asp:DropDownList>
                                                    <div class="remark">
                                            (If you choose other,please specify the Company Name)
                                        </div>
                                        <asp:TextBox ID="txtMComp_Name2" runat="server" CssClass="form-control"></asp:TextBox>
                                    </td>
                                </tr>
                               <tr>
                                    <td colspan="4" align="center">
                                        
                        <asp:Button ID="btnMerge" runat="server" Text="Merge" OnClick="btnMerge_Click" CssClass="btn btn-info"  />
                                    </td>
                                </tr>
                              
                            </table>
                    </div>
                </div>
            </div>
        </div>
        </div>
        </form>
    </body>
<%--</asp:Content>--%>

