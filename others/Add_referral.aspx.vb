﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Net.Mail
Imports System.Text
Partial Class Others_Add_referral
    Inherits System.Web.UI.Page
    Private FLAG As Integer = 0
    Dim Encr_decrData As New Encryption64
    Dim encr_decr As New SFENCRYPTION
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If Session("username") Is Nothing Then
                Session("Active_tab") = "Home"
                Session("Site_Path") = ""
                Response.Redirect("~\login.aspx")
            ElseIf Session("bPasswdChanged") = "False" Then
                Response.Redirect("~\UpdateInfo\ForcePasswordChange.aspx", False)
            ElseIf Session("bUpdateContactDetails") = "False" Then
                Response.Redirect(Session("ForceUpdate_stud"), False)
            End If


            If GET_isCRM_SF() = 0 Then
                divOasis_Reff.Visible = True
                divSF_reff.Visible = False
            Else
                divOasis_Reff.Visible = False
                divSF_reff.Visible = True
                hdn_stuno.Value = encr_decr.Encrypt(Session("STU_NO"))
                hdn_bsuid.Value = encr_decr.Encrypt(Session("sBsuid"))

                Dim url As String = "https://gems-edu.secure.force.com/GEMS/apex/GEMS_refer?studentID=" + hdn_stuno.Value + "&BSU=" + hdn_bsuid.Value
                ifReff.Attributes.Add("src", url)
            End If




            'ViewState("viewid") = Encr_decrData.Decrypt(Request.QueryString("viewid").Replace(" ", "+"))
            If Page.IsPostBack = False Then

                'txtContact_Area.Attributes.Add("onkeypress", "return isNumberKey(event)")
                'txtContact_Country.Attributes.Add("onkeypress", "return isNumberKey(event)")
                'txtContact_No.Attributes.Add("onkeypress", "return isNumberKey(event)")
                'txtContact_Country.Attributes.Add("onfocus", "Refmsg(1)")
                'txtContact_Country.Attributes.Add("onblur", "Refmsg(2)")
                bindSchool()
                Session("TABLE_REF") = CreateDataTable()
                Session("TABLE_REF").Rows.Clear()
                ViewState("id") = 1
                bindRFM_ID()
                gridbind()


            End If




        Catch ex As Exception

        End Try
    End Sub
    Function GET_isCRM_SF() As Integer
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim pParms(3) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", Session("sBsuid"))

        Using reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "get_CRMonSF", pParms)
            While reader.Read
                Return reader("BSU_bIsCRMOnSF")

            End While
        End Using
    End Function
    Private Sub bindSchool()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        ' Dim str_Sql As String = "select bsu_id,bsu_name from businessunit_m where  ISNULL(BSU_bREFERRAL,0)=1 order by bsu_name"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[OPL].[GETREF_BSU]")
        CHKbsu_ID.DataSource = ds
        CHKbsu_ID.DataTextField = "bsu_name"
        CHKbsu_ID.DataValueField = "bsu_id"
        CHKbsu_ID.DataBind()
    End Sub

    Private Sub bindRFM_ID()
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim PARAM(2) As SqlParameter
        PARAM(0) = New SqlParameter("@USR_NAME", Session("username"))
        Using DATAREADER As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "OPL.GETPARENT_REFERRAL_M", PARAM)
            If DATAREADER.HasRows = True Then
                While DATAREADER.Read

                    ViewState("RFM_ID") = Convert.ToString(DATAREADER("RFM_ID"))
                End While
            Else
                ViewState("RFM_ID") = 0
            End If

        End Using

    End Sub

    Protected Sub btnAddM_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddM.Click

        lbmsgInfo.CssClass = "divinfo"
        lbmsgInfo.Text = ""
        Try
            Dim Ibsu As Integer = 0
            Dim STRBSU_ID As String = String.Empty
            Dim STRBSU_NAME As String = String.Empty
            For Each LstItem As ListItem In CHKbsu_ID.Items
                If LstItem.Selected = True Then
                    STRBSU_NAME += "" + LstItem.Text + ","
                    STRBSU_ID += LstItem.Value + "|"
                    Ibsu = Ibsu + 1
                End If
            Next
            If Ibsu = 0 Then
                lbmsgInfo.CssClass = "divinfo"
                lbmsgInfo.Text = "Please select the school."
                Exit Sub
            End If

            For i As Integer = 0 To Session("TABLE_REF").Rows.Count - 1
                If ((UCase(Trim(Session("TABLE_REF").Rows(i)("RFS_REF_EMAIL"))).Trim = UCase(Trim(txtEmail.Text.Trim)))) And (UCase(Trim(Session("TABLE_REF").Rows(i)("STATUS"))).Trim <> "DELETED") Then
                    lbmsgInfo.CssClass = "divinfo"
                    lbmsgInfo.Text = "Duplicate entry not allowed !!!"

                  

                    Exit Sub
                End If
            Next

            Dim rDt As DataRow

            rDt = Session("TABLE_REF").NewRow
            rDt("ID") = ViewState("id")
            rDt("RFS_RFM_ID") = ViewState("RFM_ID")
            rDt("RFS_SALUTE") = ddlSlute.SelectedValue
            rDt("RFS_REF_FNAME") = txtFname.Text.Trim
            rDt("RFS_REF_LNAME") = txtLName.Text.Trim
            rDt("RFS_REF_EMAIL") = txtEmail.Text.Trim
            rDt("RFS_REF_MOB_NO") = txtContact_Country.Text.Trim & txtContact_Area.Text.Trim & txtContact_No.Text.Trim
            rDt("RFS_APPL_NAME") = txtAppName.Text.Trim
            rDt("RFS_BSU_IDS") = STRBSU_ID
            rDt("FULLNAME") = txtFname.Text.Trim & "  " & txtLName.Text.Trim
            rDt("BSU_NAME") = STRBSU_NAME.TrimEnd(",")
            rDt("STATUS") = "add"

            ViewState("id") = ViewState("id") + 1
            Session("TABLE_REF").Rows.Add(rDt)
            gridbind()
            clearall()
        Catch ex As Exception
            lbmsgInfo.CssClass = "divinfo"
            lbmsgInfo.Text = "Error in adding new record"
        End Try

    End Sub
    Sub clearall()
        txtFname.Text = ""
        txtLName.Text = ""
        ddlSlute.SelectedIndex = 0
        txtEmail.Text = ""
        txtEmailConf.Text = ""
        txtContact_Country.Text = ""
        txtContact_Area.Text = ""
        txtContact_No.Text = ""
        txtAppName.Text = ""
        For Each LstItem As ListItem In CHKbsu_ID.Items
            If LstItem.Selected = True Then
                LstItem.Selected = False
            End If
        Next


    End Sub
    Sub gridbind()
        Try
            lbmsgInfo.CssClass = ""

            lbmsgInfo.Text = ""

            Dim dtDt As New DataTable
            dtDt = CreateDataTable()
            ViewState("id") = 1
            If Not Session("TABLE_REF") Is Nothing Then


                If Session("TABLE_REF").Rows.Count > 0 Then
                    For i As Integer = 0 To Session("TABLE_REF").Rows.Count - 1
                        If Session("TABLE_REF").Rows(i)("STATUS") & "" <> "DELETED" Then
                            Dim rDt As DataRow
                            rDt = dtDt.NewRow
                            rDt("ID") = ViewState("id")
                            rDt("RFS_RFM_ID") = Session("TABLE_REF").Rows(i)("RFS_RFM_ID")
                            rDt("RFS_SALUTE") = Session("TABLE_REF").Rows(i)("RFS_SALUTE")
                            rDt("RFS_REF_FNAME") = Session("TABLE_REF").Rows(i)("RFS_REF_FNAME")
                            rDt("RFS_REF_LNAME") = Session("TABLE_REF").Rows(i)("RFS_REF_LNAME")

                            rDt("RFS_REF_EMAIL") = Session("TABLE_REF").Rows(i)("RFS_REF_EMAIL")
                            rDt("RFS_REF_MOB_NO") = Session("TABLE_REF").Rows(i)("RFS_REF_MOB_NO")
                            rDt("RFS_APPL_NAME") = Session("TABLE_REF").Rows(i)("RFS_APPL_NAME")

                            rDt("RFS_BSU_IDS") = Session("TABLE_REF").Rows(i)("RFS_BSU_IDS")
                            rDt("FULLNAME") = Session("TABLE_REF").Rows(i)("FULLNAME")
                            rDt("BSU_NAME") = Session("TABLE_REF").Rows(i)("BSU_NAME")

                            rDt("STATUS") = Session("TABLE_REF").Rows(i)("STATUS")

                            dtDt.Rows.Add(rDt)
                            ViewState("id") = ViewState("id") + 1
                        End If
                    Next

                End If

                If dtDt.Rows.Count > 0 Then
                    btnSave.Visible = True
                    trReward.Visible = True
                    gvRef.Visible = True
                    trrefHead.Visible = True


                Else

                    btnSave.Visible = False
                    trReward.Visible = False
                    gvRef.Visible = False
                    trrefHead.Visible = False
                End If
         


            Else
                btnSave.Visible = False
                trReward.Visible = False
                gvRef.Visible = False
                trrefHead.Visible = False
            End If


            gvRef.DataSource = dtDt
            gvRef.DataBind()


        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
        End Try
    End Sub

    Private Function CreateDataTable() As DataTable
        Dim dtDt As DataTable
        dtDt = New DataTable
        Try
            Dim ID As New DataColumn("ID", System.Type.GetType("System.String"))
            Dim RFS_RFM_ID As New DataColumn("RFS_RFM_ID", System.Type.GetType("System.String"))
            Dim RFS_SALUTE As New DataColumn("RFS_SALUTE", System.Type.GetType("System.String"))
            Dim RFS_REF_FNAME As New DataColumn("RFS_REF_FNAME", System.Type.GetType("System.String"))
            Dim RFS_REF_LNAME As New DataColumn("RFS_REF_LNAME", System.Type.GetType("System.String"))
            Dim RFS_REF_EMAIL As New DataColumn("RFS_REF_EMAIL", System.Type.GetType("System.String"))
            Dim RFS_REF_MOB_NO As New DataColumn("RFS_REF_MOB_NO", System.Type.GetType("System.String"))
            Dim RFS_APPL_NAME As New DataColumn("RFS_APPL_NAME", System.Type.GetType("System.String"))
            Dim RFS_BSU_IDS As New DataColumn("RFS_BSU_IDS", System.Type.GetType("System.String"))
            Dim FULLNAME As New DataColumn("FULLNAME", System.Type.GetType("System.String"))
            Dim BSU_NAME As New DataColumn("BSU_NAME", System.Type.GetType("System.String"))
            Dim STATUS As New DataColumn("STATUS", System.Type.GetType("System.String"))


            dtDt.Columns.Add(ID)
            dtDt.Columns.Add(RFS_RFM_ID)
            dtDt.Columns.Add(RFS_SALUTE)
            dtDt.Columns.Add(RFS_REF_FNAME)
            dtDt.Columns.Add(RFS_REF_LNAME)
            dtDt.Columns.Add(RFS_REF_EMAIL)
            dtDt.Columns.Add(RFS_REF_MOB_NO)
            dtDt.Columns.Add(RFS_APPL_NAME)
            dtDt.Columns.Add(RFS_BSU_IDS)
            dtDt.Columns.Add(FULLNAME)
            dtDt.Columns.Add(BSU_NAME)
            dtDt.Columns.Add(STATUS)
            Return dtDt
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)

            Return dtDt
        End Try
    End Function
    Protected Sub gvRef_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvRef.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim DeleteBtn As Button = DirectCast(e.Row.FindControl("DeleteBtn"), Button)
            DeleteBtn.OnClientClick = "if(!confirm('Are you sure you want to delete this?')) return false;"
        End If
    End Sub
    Protected Sub gvRef_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvRef.RowDeleting
        Dim COND_ID As Integer = CInt(gvRef.DataKeys(e.RowIndex).Value)

        Dim i As Integer = 0

        For i = 0 To Session("TABLE_REF").Rows.Count - 1
            If Session("TABLE_REF").rows(i)("ID") = COND_ID Then
                Session("TABLE_REF").Rows(i)("STATUS") = "DELETED"

            End If

        Next

        gridbind()
    End Sub

    Function calltransaction(ByRef errorMessage As String) As Integer

        Dim STATUS As String = "0"
        Dim transaction As SqlTransaction

        Using conn As SqlConnection = ConnectionManger.GetOASISConnection

            transaction = conn.BeginTransaction("SampleTransaction")
            Try
                Dim str As String = String.Empty
                Dim INVALIDMAIL As String = String.Empty
                Dim RFS_RFM_ID As String = String.Empty
                Dim RFS_SALUTE As String = String.Empty
                Dim RFS_REF_FNAME As String = String.Empty
                Dim RFS_REF_LNAME As String = String.Empty
                Dim RFS_REF_EMAIL As String = String.Empty
                Dim RFS_APPL_NAME As String = String.Empty
                Dim RFS_REF_MOB_NO As String = String.Empty
                Dim RFS_BSU_IDS As String = String.Empty
                Dim RFD_PRO_RFM_CODE As String = ""
                If Session("TABLE_REF").Rows.Count <> 0 Then
                    For i As Integer = 0 To Session("TABLE_REF").Rows.Count - 1
                        If Session("TABLE_REF").Rows(i)("STATUS") & "" <> "DELETED" Then
                            RFS_RFM_ID = Session("TABLE_REF").Rows(i)("RFS_RFM_ID")
                            RFS_SALUTE = Session("TABLE_REF").Rows(i)("RFS_SALUTE")
                            RFS_REF_FNAME = Session("TABLE_REF").Rows(i)("RFS_REF_FNAME")
                            RFS_REF_LNAME = Session("TABLE_REF").Rows(i)("RFS_REF_LNAME")
                            RFS_REF_EMAIL = Session("TABLE_REF").Rows(i)("RFS_REF_EMAIL")
                            RFS_APPL_NAME = Session("TABLE_REF").Rows(i)("RFS_APPL_NAME")
                            RFS_REF_MOB_NO = Session("TABLE_REF").Rows(i)("RFS_REF_MOB_NO")
                            RFS_BSU_IDS = Session("TABLE_REF").Rows(i)("RFS_BSU_IDS")

                            str += String.Format("<REFERRAL_S RFS_RFM_ID='{0}'  RFS_SALUTE='{1}' RFS_REF_FNAME='{2}' RFS_REF_LNAME='{3}' RFS_REF_EMAIL='{4}' RFS_REF_MOB_NO='{5}' RFS_BSU_IDS='{6}' RFS_APPL_NAME='{7}' />", RFS_RFM_ID, _
                                                 RFS_SALUTE, RFS_REF_FNAME, RFS_REF_LNAME, RFS_REF_EMAIL, RFS_REF_MOB_NO, RFS_BSU_IDS, RFS_APPL_NAME)

                        End If
 Next
                End If
                If str <> "" Then
                    str = "<REFERRAL>" + str + "</REFERRAL>"
                End If


                Dim arInfo As String() = New String(2) {}
                Dim splitter As Char = "|"
                Dim temp As String = String.Empty
                Dim pParms(9) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@STR", str)
                pParms(1) = New SqlClient.SqlParameter("@RFS_RFM_ID", RFS_RFM_ID)
                pParms(2) = New SqlClient.SqlParameter("@RFD_PRO_RFM_CODE", RFD_PRO_RFM_CODE)
                pParms(3) = New SqlClient.SqlParameter("@RFS_REWARD_TYPE", ddlREWARD.SelectedValue)


                pParms(4) = New SqlClient.SqlParameter("@REF_INFO", SqlDbType.VarChar, 900)
                pParms(4).Direction = ParameterDirection.Output
                pParms(5) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                pParms(5).Direction = ParameterDirection.ReturnValue

                SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "[REF].[SAVEREFERRAL_S]", pParms)
                Dim ReturnFlag As Integer = pParms(5).Value


                If ReturnFlag <> 0 Then

                    errorMessage = "Error occured while processing info."

                    STATUS = "1"
                Else

                    ViewState("RFS_ID") = IIf(TypeOf (pParms(4).Value) Is DBNull, String.Empty, pParms(4).Value)
                    callSend_mail_Refd(transaction, ViewState("RFS_ID"), INVALIDMAIL)

                    If INVALIDMAIL <> "" Then
                        ' STATUS = "1"
                        errorMessage = "Error while mailing following email address -- " & INVALIDMAIL.TrimEnd(",")
                    Else
                        ' STATUS = "0"
                    End If
                    callSend_mail_Ref(transaction, ViewState("RFS_ID"))


                    callSend_SMS_Refd(ViewState("RFS_ID"))

                End If


            Catch ex As Exception
                STATUS = "1"
                errorMessage = "Error occured while saving."
            Finally
                If STATUS <> "0" Then
                    UtilityObj.Errorlog(errorMessage)
                    transaction.Rollback()
                    calltransaction = 1
                Else

                    errorMessage = ""
                    transaction.Commit()
                    calltransaction = 0
                End If
            End Try


        End Using
    End Function
    Function callSend_mail_Ref(ByVal transaction As SqlTransaction, ByVal elist As String) As String
        Dim MAILLIST As String = String.Empty
        Try
            Dim htmlString_single As String = String.Empty
            Dim htmlString_Mult As String = String.Empty
            Dim html As String = ScreenScrapeHtml(Server.MapPath("emailTemplate.htm"))
            Dim msg As New MailMessage

            Dim str_query As String = String.Empty
            Dim str_conn As String = ConnectionManger.GetOASISConnectionString
            Dim status As String = String.Empty
            Dim USR_EMAIL As String = String.Empty
            Dim BSC_HOST As String = String.Empty
            Dim BSC_USERNAME As String = String.Empty
            Dim BSC_PASSWORD As String = String.Empty
            Dim BSC_PORT As String = String.Empty

            htmlString_single = "Dear @Refname<br />" & _
        "<br />Thank you for referring the following person to GEMS school(s)<br/> <br/>@Refdlist<br /><br />" & _
        "You can track this referral via the Refer a Friend login - &#39;  My Account Page &#39;.<br />" & _
         "<br />Click on the link - @Gemsref to refer a student.<br />" & _
        "If you have any queries, please do not hesitate to contact me." & _
 "<br /><br />Elmarie<br /> " & _
 "<a href='mailto:raf@gemseducation.com'>raf@gemseducation.com</a>"

            htmlString_Mult = "Dear @Refname<br />" & _
       "<br />Thank you for referring the following person(s) to GEMS school(s) <br/><br/> @Refdlist<br /><br />" & _
       "You can track this referral via the Refer a Friend login - &#39; My Account Page &#39;.<br />" & _
        "<br />Click on the link - @Gemsref to refer a student.<br />" & _
      "If you have any queries, please do not hesitate to contact me." & _
"<br /><br />Elmarie<br /> " & _
"<a href='mailto:raf@gemseducation.com'>raf@gemseducation.com</a>"
            Dim i As Integer = 0
            Dim pParms(4) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@elist", elist)


            Dim splitter As Char = "|"
            Dim temp As String() = elist.Split("|")
            If temp.Length > 1 Then
                html = html.Replace("@Content", htmlString_Mult)
            Else
                html = html.Replace("@Content", htmlString_single)
            End If


            'Using EmailHost_reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "[REF].[REFD_EMAIL_INFO]", pParms)
            '    If EmailHost_reader.HasRows Then

            '        While EmailHost_reader.Read
            '            i = i + 1
            '        End While
            '    End If
            'End Using


            Dim SCHOOLS As String = String.Empty
            Dim str_group As String = String.Empty



            Using EmailHost_reader As SqlDataReader = SqlHelper.ExecuteReader(transaction, CommandType.StoredProcedure, "[REF].[REFD_EMAIL_INFO_COPY]", pParms)
                If EmailHost_reader.HasRows Then

                    While EmailHost_reader.Read
                        BSC_HOST = Convert.ToString(EmailHost_reader("BSC_HOST"))
                        BSC_USERNAME = Convert.ToString(EmailHost_reader("BSC_USERNAME"))
                        BSC_PASSWORD = Convert.ToString(EmailHost_reader("BSC_PASSWORD"))
                        BSC_PORT = Convert.ToString(EmailHost_reader("BSC_PORT"))

                        html = html.Replace("@Refname", Convert.ToString(EmailHost_reader("Refname")))
                        html = html.Replace("@Refcode", Encr_decrData.Decrypt(Convert.ToString(EmailHost_reader("Refcode"))))
                        SCHOOLS = Convert.ToString(EmailHost_reader("School")).TrimEnd(",")
                        SCHOOLS = StrReverse(Replace(StrReverse(SCHOOLS), StrReverse(","), StrReverse(" and "), , 1))


                        str_group = str_group + Convert.ToString(EmailHost_reader("Refdname")) + " to " & SCHOOLS & "<br/>"

                        html = html.Replace("@Contactname", Convert.ToString(EmailHost_reader("CON_NAME")))
                        html = html.Replace("@Contactno", Convert.ToString(EmailHost_reader("CON_PHONE")))

                        html = html.Replace("@Gemsref", "<a href='https://oasis.gemseducation.com/general/Home.aspx?RID=" & Encr_decrData.Encrypt(ViewState("RFM_ID")) & "'>GEMS Refer a Friend</a>")
                        USR_EMAIL = Convert.ToString(EmailHost_reader("USR_EMAIL"))

                    End While
                End If
            End Using
            html = html.Replace("@Refdlist", str_group)

            status = SendPlainTextEmails(BSC_USERNAME, USR_EMAIL, "Thank you for referring a friend to GEMS", html.ToString, BSC_USERNAME, BSC_PASSWORD, BSC_HOST, BSC_PORT, "0", False)
            callSend_mail_Ref = "0"


        Catch ex As Exception

            callSend_mail_Ref = "-1"
            'lblError.Text = "Error while processing your request"
        End Try
    End Function

    Function callSend_mail_Refd(ByVal transaction As SqlTransaction, ByVal elist As String, ByRef INVALIDMAIL As String) As String
        Dim MAILLIST As String = String.Empty
        Try
            Dim htmlString As String = String.Empty
            Dim msg As New MailMessage

            Dim str_query As String = String.Empty

            Dim status As String = String.Empty

            Dim BSC_HOST As String = String.Empty
            Dim BSC_USERNAME As String = String.Empty
            Dim BSC_PASSWORD As String = String.Empty
            Dim BSC_PORT As String = String.Empty


            Dim SCHOOLS As String = String.Empty

            Dim pParms(4) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@elist", elist)

            Using EmailHost_reader As SqlDataReader = SqlHelper.ExecuteReader(transaction, CommandType.StoredProcedure, "[REF].[REFD_EMAIL_INFO]", pParms)
                If EmailHost_reader.HasRows Then
                    While EmailHost_reader.Read
                        htmlString = ""
                        Dim html As String = ScreenScrapeHtml(Server.MapPath("emailTemplate.htm"))

                        htmlString = "Dear @Refdname ,<br />" & _
                         "<br />@Refname (Ref.code @Refcode) has referred you for admission to @School.<br />" & _
                         "Kindly click on the name of the school to complete the registration process. " & _
                         "<br /><br /><br /> " & _
                  "If you have any queries, please do not hesitate to contact me." & _
                  "<br /><br />Elmarie<br /> " & _
                  "<a href='mailto:raf@gemseducation.com'>raf@gemseducation.com</a>"

                        html = html.Replace("@Content", htmlString)




                        SCHOOLS = ""
                        BSC_HOST = Convert.ToString(EmailHost_reader("BSC_HOST"))
                        BSC_USERNAME = Convert.ToString(EmailHost_reader("BSC_USERNAME"))
                        BSC_PASSWORD = Convert.ToString(EmailHost_reader("BSC_PASSWORD"))
                        BSC_PORT = Convert.ToString(EmailHost_reader("BSC_PORT"))
                        html = html.Replace("@Refdname", Convert.ToString(EmailHost_reader("Refdname")))
                        html = html.Replace("@Refname", Convert.ToString(EmailHost_reader("Refname")))
                        html = html.Replace("@Refcode", Convert.ToString(EmailHost_reader("Refcode")))
                        SCHOOLS = Convert.ToString(EmailHost_reader("School")).TrimEnd(",")
                        SCHOOLS = StrReverse(Replace(StrReverse(SCHOOLS), StrReverse(","), StrReverse(" and "), , 1))

                        html = html.Replace("@School", SCHOOLS)
                        'html = html.Replace("@Gemsref", "<a href='http://213.42.90.217/oasis/students/studEnqForm.aspx?RID=" & Encr_decrData.Encrypt(Convert.ToString(EmailHost_reader("RFS_ID"))) & "'>GEMS online enquiry</a>")

                        status = SendPlainTextEmails(BSC_USERNAME, Convert.ToString(EmailHost_reader("RFS_REF_EMAIL")), "You have been referred as a friend to a GEMS school", html.ToString, BSC_USERNAME, BSC_PASSWORD, BSC_HOST, BSC_PORT, "0", False)

                        If status <> "0" Then
                            MAILLIST = MAILLIST + Convert.ToString(EmailHost_reader("RFS_REF_EMAIL")) + ","
                        End If
                    End While
                End If
            End Using

            callSend_mail_Refd = "0"
            INVALIDMAIL = MAILLIST

        Catch ex As Exception
            INVALIDMAIL = MAILLIST
            callSend_mail_Refd = "-1"
            'lblError.Text = "Error while processing your request"
        End Try
    End Function
    Public Shared Function ScreenScrapeHtml(ByVal url As String) As String
        Dim objRequest As System.Net.WebRequest = System.Net.HttpWebRequest.Create(url)
        Dim sr As New StreamReader(objRequest.GetResponse().GetResponseStream())
        Dim result As String = sr.ReadToEnd()
        sr.Close()
        Return result
    End Function 'ScreenScrapeHtml
    Public Function SendPlainTextEmails(ByVal FromEmailId As String, ByVal ToEmailId As String, ByVal Subject As String, ByVal MailBody As String, ByVal Username As String, ByVal password As String, ByVal Host As String, ByVal Port As Integer, ByVal Templateid As String, ByVal HasAttachments As Boolean) As String
        Dim ReturnValue = ""
        Dim client As New System.Net.Mail.SmtpClient(Host, Port)
        Dim msg As New System.Net.Mail.MailMessage(FromEmailId, ToEmailId)
        Try
            msg.Subject = Subject

            msg.Body = MailBody

            msg.Priority = MailPriority.Normal

            msg.IsBodyHtml = True


            Dim HTMLView As AlternateView = AlternateView.CreateAlternateViewFromString(MailBody, Nothing, "text/html")

            msg.AlternateViews.Add(HTMLView)






            'client.DeliveryMethod = SmtpDeliveryMethod.PickupDirectoryFromIis
            If Username <> "" And password <> "" Then
                Dim creds As New System.Net.NetworkCredential(Username, password)
                client.Credentials = creds
            End If

            Dim s As String = client.Host

            client.Send(msg)

            ReturnValue = "0"
        Catch smptEx As System.Net.Mail.SmtpException
            Dim statuscode As System.Net.Mail.SmtpStatusCode
            statuscode = smptEx.StatusCode
            If (statuscode = Net.Mail.SmtpStatusCode.MailboxBusy) Or (statuscode = Net.Mail.SmtpStatusCode.MailboxUnavailable) Then
                System.Threading.Thread.Sleep(100)
                client.Send(msg)
            Else
                UtilityObj.Errorlog(statuscode)
                ReturnValue = "-1"
            End If

        Catch ex As Exception
            ReturnValue = "-1"
        End Try

        Return ReturnValue

    End Function
    
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim lblmsg As Label = CType(Page.Master.FindControl("lblSiteMessage"), Label)

        If Session("TABLE_REF").Rows.Count > 0 Then

            Dim str_err As String = String.Empty
            Dim errorMessage As String = String.Empty


            ' If txtProRefCode.Text.Trim <> "" Then

            '  If VaildRefCode() = True Then
            str_err = calltransaction(errorMessage)
            If str_err = "0" Then
                Session("TABLE_REF").Rows.Clear()
                ViewState("id") = 1
                gridbind()
                lblmsg.CssClass = "divvalid"
                lblmsg.Text = "Thank you for referring your friend(s) to GEMS. An email notification has been sent to the referred friend(s) with a copy to you. You can track their status from your account page."
            Else
                lblmsg.CssClass = "diverror"

                lblmsg.Text = errorMessage
            End If
            'Else
            '    lblMsg.InnerText = "Invalid referral Code !!!"
            'End If

            'Else
            '    str_err = calltransaction(errorMessage)
            '    If str_err = "0" Then
            '        Session("TABLE_REF").Rows.Clear()
            '        ViewState("id") = 1
            '        gridbind()
            '        lblMsg.InnerText = "Thank you for referring your friend(s) to GEMS. An email notification has been sent to the referred friend(s) with a copy to you. You can track their status from your account page."
            '    Else
            '        lblMsg.InnerText = errorMessage
            '    End If
            'End If

        Else
            lblmsg.CssClass = "diverror"

            lblmsg.Text = "No record added yet."
        End If
    End Sub

    Sub callSend_SMS_Refd(ByVal RFS_ID As String)
        Dim status As String
        Dim from As String = "GEMS"
        Dim password As String = "manoj"

        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim pParms(4) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@RFS_ID", RFS_ID)

        Using datareader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.StoredProcedure, "REF.GETREFERRAL_S_SMS", pParms)

            If datareader.HasRows = True Then
                While datareader.Read

                    Try
                        status = SmsService.sms.SendMessage(Convert.ToString(datareader("RFS_REF_MOB_NO")), Convert.ToString(datareader("msg")), from, "gemseducation", password)
                        SaveLog(Convert.ToString(datareader("RFS_ID")), Convert.ToString(datareader("RFS_REF_MOB_NO")), status, Convert.ToString(datareader("msg")))

                    Catch ex As Exception
                        UtilityObj.Errorlog(ex.Message)
                    End Try

                End While

            End If

        End Using
    End Sub
    Public Sub SaveLog(ByVal RSL_RFS_ID As String, ByVal RSL_MOB_NO As String, ByVal RSL_LOG_STATUS As String, ByVal RSL_SMS_TEXT As String)
        Dim str_conn As String = ConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim pParms(5) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@RSL_RFS_ID", RSL_RFS_ID)
        pParms(1) = New SqlClient.SqlParameter("@RSL_MOB_NO", RSL_MOB_NO)
        pParms(2) = New SqlClient.SqlParameter("@RSL_LOG_STATUS", RSL_LOG_STATUS)
        pParms(3) = New SqlClient.SqlParameter("@RSL_SMS_TEXT", RSL_SMS_TEXT)
        SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "ref.saveREFERRAL_S_SMS_LOG", pParms)
    End Sub

    Protected Sub lbtnViewRef_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbtnViewRef.Click
        mdlPopup.Show()
    End Sub
End Class
