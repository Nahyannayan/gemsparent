Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data.SqlTypes
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data 

Partial Class Others_PaymentResultPageReenroll
    Inherits System.Web.UI.Page

    Dim debugData As String = String.Empty

    Private Function getResponseDescription(ByVal vResponseCode As String) As String
        Dim result As String = "Unknown"
        If vResponseCode.Length > 0 Then
            Select Case vResponseCode
                Case "0"
                    result = "Transaction Successful"
                    Exit Select
                Case "1"
                    result = "Transaction Declined"
                    Exit Select
                Case "2"
                    result = "Bank Declined Transaction"
                    Exit Select
                Case "3"
                    result = "No Reply from Bank"
                    Exit Select
                Case "4"
                    result = "Expired Card"
                    Exit Select
                Case "5"
                    result = "Insufficient Funds"
                    Exit Select
                Case "6"
                    result = "Error Communicating with Bank"
                    Exit Select
                Case "7"
                    result = "Payment Server detected an error"
                    Exit Select
                Case "8"
                    result = "Transaction Type Not Supported"
                    Exit Select
                Case "9"
                    result = "Bank declined transaction (Do not contact Bank)"
                    Exit Select
                Case "A"
                    result = "Transaction Aborted"
                    Exit Select
                Case "B"
                    result = "Transaction Declined - Contact the Bank"
                    Exit Select
                Case "C"
                    result = "Transaction Cancelled"
                    Exit Select
                Case "D"
                    result = "Deferred transaction has been received and is awaiting processing"
                    Exit Select
                Case "F"
                    result = "3-D Secure Authentication failed"
                    Exit Select
                Case "I"
                    result = "Card Security Code verification failed"
                    Exit Select
                Case "L"
                    result = "Shopping Transaction Locked (Please try the transaction again later)"
                    Exit Select
                Case "N"
                    result = "Cardholder is not enrolled in Authentication scheme"
                    Exit Select
                Case "P"
                    result = "Transaction has been received by the Payment Adaptor and is being processed"
                    Exit Select
                Case "R"
                    result = "Transaction was not processed - Reached limit of retry attempts allowed"
                    Exit Select
                Case "S"
                    result = "Duplicate SessionID"
                    Exit Select
                Case "T"
                    result = "Address Verification Failed"
                    Exit Select
                Case "U"
                    result = "Card Security Code Failed"
                    Exit Select
                Case "V"
                    result = "Address Verification and Card Security Code Failed"
                    Exit Select
                Case Else
                    result = "Unable to be determined"
                    Exit Select
            End Select
        End If
        Return result
    End Function

    Private Function displayAVSResponse(ByVal vAVSResultCode As String) As String
        Dim result As String = "Unknown"

        If vAVSResultCode.Length > 0 Then
            If vAVSResultCode.Equals("Unsupported") Then
                result = "AVS not supported or there was no AVS data provided"
            Else
                Select Case vAVSResultCode
                    Case "X"
                        result = "Exact match - address and 9 digit ZIP/postal code"
                        Exit Select
                    Case "Y"
                        result = "Exact match - address and 5 digit ZIP/postal code"
                        Exit Select
                    Case "S"
                        result = "Service not supported or address not verified (international transaction)"
                        Exit Select
                    Case "G"
                        result = "Issuer does not participate in AVS (international transaction)"
                        Exit Select
                    Case "A"
                        result = "Address match only"
                        Exit Select
                    Case "W"
                        result = "9 digit ZIP/postal code matched, Address not Matched"
                        Exit Select
                    Case "Z"
                        result = "5 digit ZIP/postal code matched, Address not Matched"
                        Exit Select
                    Case "R"
                        result = "Issuer system is unavailable"
                        Exit Select
                    Case "U"
                        result = "Address unavailable or not verified"
                        Exit Select
                    Case "E"
                        result = "Address and ZIP/postal code not provided"
                        Exit Select
                    Case "N"
                        result = "Address and ZIP/postal code not matched"
                        Exit Select
                    Case "0"
                        result = "AVS not requested"
                        Exit Select
                    Case Else
                        result = "Unable to be determined"
                        Exit Select
                End Select
            End If
        End If
        Return result
    End Function

    Private Function displayCSCResponse(ByVal vCSCResultCode As String) As String        '
        Dim result As String = "Unknown"
        If vCSCResultCode.Length > 0 Then
            If vCSCResultCode.Equals("Unsupported") Then
                result = "CSC not supported or there was no CSC data provided"
            Else

                Select Case vCSCResultCode
                    Case "M"
                        result = "Exact code match"
                        Exit Select
                    Case "S"
                        result = "Merchant has indicated that CSC is not present on the card (MOTO situation)"
                        Exit Select
                    Case "P"
                        result = "Code not processed"
                        Exit Select
                    Case "U"
                        result = "Card issuer is not registered and/or certified"
                        Exit Select
                    Case "N"
                        result = "Code invalid or not matched"
                        Exit Select
                    Case Else
                        result = "Unable to be determined"
                        Exit Select
                End Select
            End If
        End If
        Return result
    End Function
    Private Function splitResponse(ByVal rawData As String) As System.Collections.Hashtable

        Dim responseData As New System.Collections.Hashtable()
        Try
            ' Check if there was a response containing parameters
            If rawData.IndexOf("=") > 0 Then
                ' Extract the key/value pairs for each parameter
                For Each pair As String In rawData.Split("&"c)
                    Dim equalsIndex As Integer = pair.IndexOf("=")
                    If equalsIndex > 1 AndAlso pair.Length > equalsIndex Then
                        Dim paramKey As String = System.Web.HttpUtility.UrlDecode(pair.Substring(0, equalsIndex))
                        Dim paramValue As String = System.Web.HttpUtility.UrlDecode(pair.Substring(equalsIndex + 1))
                        responseData.Add(paramKey, paramValue)
                    End If
                Next
            Else
                ' There were no parameters so create an error
                responseData.Add("vpc_Message", "The data contained in the response was not a valid receipt.<br/>" & vbLf & "The data was: <pre>" & rawData & "</pre><br/>" & vbLf)
            End If
            Return responseData
        Catch ex As Exception
            ' There was an exception so create an error
            responseData.Add("vpc_Message", (vbLf & "The was an exception parsing the response data.<br/>" & vbLf & "The data was: <pre>" & rawData & "</pre><br/>" & vbLf & "<br/>" & vbLf & "Exception: ") + ex.ToString() & "<br/>" & vbLf)
            Return responseData
        End Try
    End Function

    Private Function CreateMD5Signature(ByVal RawData As String) As String
        Dim hasher As System.Security.Cryptography.MD5 = System.Security.Cryptography.MD5CryptoServiceProvider.Create()
        Dim HashValue As Byte() = hasher.ComputeHash(Encoding.ASCII.GetBytes(RawData))

        Dim strHex As String = ""
        For Each b As Byte In HashValue
            strHex += b.ToString("x2")
        Next
        Return strHex.ToUpper()
    End Function

    Private Function null2unknown(ByVal req As Object) As String
        Try
            If req Is Nothing Then
                Return String.Empty
            Else
                Return req.ToString()
            End If
        Catch ex As Exception
            Return String.Empty
        End Try
    End Function

    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("username") & "" = "" Then
            Response.Redirect("~/login.aspx")
        End If

        Dim S_BSU_ID As String = IIf(Session("sBsuid") Is Nothing, "", Session("sBsuid"))
        Dim SECURE_SECRET As String = FeeCollectionOnline.GetSECURE_SECRET(S_BSU_ID)
        Dim MyPaymentGatewayClass As New PaymentGatewayClass(Session("CPS_ID"))

        Panel_Debug.Visible = False
        Panel_Receipt.Visible = False
        Panel_StackTrace.Visible = False

        Dim message As String = ""
        Dim errorExists As Boolean = False
        Dim txnResponseCode As String = ""
        Dim rawHashData As String = SECURE_SECRET
        MyPaymentGatewayClass.vpc_Amount = Session("vpc_Amount")
        MyPaymentGatewayClass.vpc_MerchantID = Session("vpc_OrderInfo")
        Dim PageResponseValues As System.Collections.Specialized.NameValueCollection
        If MyPaymentGatewayClass.ResponseType = "POST" Then
            PageResponseValues = Request.Form
        Else
            PageResponseValues = Page.Request.QueryString
        End If
        MyPaymentGatewayClass.SetResponseCodeFromPaymentGateway(PageResponseValues)
        Label_HashValidation.Text = "<font color='orange'><b>NOT CALCULATED</b></font>"
        Dim hashValidated As Boolean = True
        Try

            ' collect debug information
#If DEBUG Then
        debugData += "<br/><u>Start of Debug Data</u><br/><br/>"
#End If
            Dim signature As String = ""
            If Page.Request.QueryString("vpc_SecureHash").Length > 0 Then

#If DEBUG Then
            debugData += "<u>Data from Payment Server</u><br/>"
#End If
                If MyPaymentGatewayClass.CPM_GATEWAY_TYPE = "MIGS" Then
                    rawHashData = ""
                    Dim seperator As String = ""
                    For Each item As String In PageResponseValues
                        If Not item.Equals("vpc_SecureHash") AndAlso Not item.Equals("vpc_SecureHashType") Then
                            If item.StartsWith("vpc_") Or item.StartsWith("user_") Then
                                rawHashData &= seperator & item & "=" & PageResponseValues(item)
                                seperator = "&"
                            End If
                        End If
#If DEBUG Then
                debugData += (item & "=") + Page.Request.QueryString(item) & "<br/>"
#End If
                    Next
                Else
                    For Each item As String In Page.Request.QueryString
#If DEBUG Then
                debugData += (item & "=") + Page.Request.QueryString(item) & "<br/>"
#End If

                        If SECURE_SECRET.Length > 0 AndAlso Not item.Equals("vpc_SecureHash") Then
                            rawHashData += Page.Request.QueryString(item)
                        End If
                    Next
                    If SECURE_SECRET.Length > 0 Then
                        signature = MyPaymentGatewayClass.CreateMD5Signature(rawHashData)
                    End If
                End If
            End If

            ' Create the MD5 signature if required

            If SECURE_SECRET.Length > 0 Then
                ' Collect debug information
#If DEBUG Then
            debugData += ("<br/><u>Hash Data Input</u>: " & rawHashData & "<br/><br/><u>Signature Created</u>: ") + signature & "<br/>"
#End If

                ' Validate the Secure Hash (remember MD5 hashes are not case sensitive)
                If Page.Request.QueryString("vpc_SecureHash").Equals(signature) Then
                    ' Secure Hash validation succeeded,
                    ' add a data field to be displayed later.
                    Label_HashValidation.Text = "<font color='#00AA00'><b>CORRECT</b></font>"
                Else
                    ' Secure Hash validation failed, add a data field to be displayed
                    ' later.
                    Label_HashValidation.Text = "<font color='#FF0066'><b>INVALID HASH</b></font>"
                    hashValidated = False
                End If
            End If
            ' Get the standard receipt data from the parsed response
            txnResponseCode = IIf(Page.Request.QueryString("vpc_TxnResponseCode").Length > 0, Page.Request.QueryString("vpc_TxnResponseCode"), "Unknown")
            Label_TxnResponseCode.Text = txnResponseCode
            Label_TxnResponseCodeDesc.Text = getResponseDescription(txnResponseCode)

            'Label_Amount.Text = IIf(Page.Request.QueryString("vpc_Amount").Length > 0, Page.Request.QueryString("vpc_Amount"), "Unknown")
            Label_Command.Text = IIf(Page.Request.QueryString("vpc_Command").Length > 0, Page.Request.QueryString("vpc_Command"), "Unknown")
            Label_Version.Text = IIf(Page.Request.QueryString("vpc_Version").Length > 0, Page.Request.QueryString("vpc_Version"), "Unknown")
            Label_OrderInfo.Text = IIf(Page.Request.QueryString("vpc_OrderInfo").Length > 0, Page.Request.QueryString("vpc_OrderInfo"), "Unknown")
            Label_MerchantID.Text = IIf(Page.Request.QueryString("vpc_Merchant").Length > 0, Page.Request.QueryString("vpc_Merchant"), "Unknown")

            ' only display this data if not an error condition
            'If Not errorExists AndAlso Not txnResponseCode.Equals("7") Then
            '    Label_BatchNo.Text = IIf(Page.Request.QueryString("vpc_BatchNo").Length > 0, Page.Request.QueryString("vpc_BatchNo"), "Unknown")
            '    Label_CardType.Text = IIf(Page.Request.QueryString("vpc_Card").Length > 0, Page.Request.QueryString("vpc_Card"), "Unknown")
            '    Label_ReceiptNo.Text = IIf(Page.Request.QueryString("vpc_ReceiptNo").Length > 0, Page.Request.QueryString("vpc_ReceiptNo"), "Unknown")
            '    Label_AuthorizeID.Text = IIf(Page.Request.QueryString("vpc_AuthorizeId").Length > 0, Page.Request.QueryString("vpc_AuthorizeId"), "Unknown")
            '    Label_MerchTxnRef.Text = IIf(Page.Request.QueryString("vpc_MerchTxnRef").Length > 0, Page.Request.QueryString("vpc_MerchTxnRef"), "Unknown")
            '    Label_AcqResponseCode.Text = IIf(Page.Request.QueryString("vpc_AcqResponseCode").Length > 0, Page.Request.QueryString("vpc_AcqResponseCode"), "Unknown")
            '    Label_TransactionNo.Text = IIf(Page.Request.QueryString("vpc_TransactionNo").Length > 0, Page.Request.QueryString("vpc_TransactionNo"), "Unknown")
            '    Panel_Receipt.Visible = True
            'End If
            '        title    =null2unknown(Request.QueryString("title"))
            'againLink=null2unknown(Request.QueryString("AgainLink"))
            Dim amount As String = null2unknown(Page.Request.QueryString("vpc_Amount"))
            Dim locale As String = null2unknown(Page.Request.QueryString("vpc_Locale"))
            Dim batchNo As String = null2unknown(Page.Request.QueryString("vpc_BatchNo"))
            Dim command As String = null2unknown(Page.Request.QueryString("vpc_Command"))
            Dim version As String = null2unknown(Page.Request.QueryString("vpc_Version"))
            Dim cardType As String = null2unknown(Page.Request.QueryString("vpc_Card"))
            Dim orderInfo As String = null2unknown(Page.Request.QueryString("vpc_OrderInfo"))
            Dim receiptNo As String = null2unknown(Page.Request.QueryString("vpc_ReceiptNo"))
            Dim merchantID As String = null2unknown(Page.Request.QueryString("vpc_Merchant"))
            Dim authorizeID As String = null2unknown(Page.Request.QueryString("vpc_AuthorizeId"))
            Dim merchTxnRef As String = null2unknown(Page.Request.QueryString("vpc_MerchTxnRef"))
            Dim transactionNo As String = null2unknown(Page.Request.QueryString("vpc_TransactionNo"))
            Dim acqResponseCode As String = null2unknown(Page.Request.QueryString("vpc_AcqResponseCode"))
            ' string txnResponseCode=null2unknown(Page.Request.QueryString["vpc_TxnResponseCode"]);
            ' string   message    =null2unknown(Page.Request.QueryString["vpc_Message"));
            '' 3-D Secure Data
            Dim verType As String = null2unknown(Page.Request.QueryString("vpc_VerType"))
            Dim verStatus As String = null2unknown(Page.Request.QueryString("vpc_VerStatus"))
            Dim token As String = null2unknown(Page.Request.QueryString("vpc_VerToken"))
            Dim verSecurLevel As String = null2unknown(Page.Request.QueryString("vpc_VerSecurityLevel"))
            Dim enrolled As String = null2unknown(Page.Request.QueryString("vpc_3DSenrolled"))
            Dim xid As String = null2unknown(Page.Request.QueryString("vpc_3DSXID"))
            Dim acqECI As String = null2unknown(Page.Request.QueryString("vpc_3DSECI"))
            Dim authStatus As String = null2unknown(Page.Request.QueryString("vpc_3DSstatus"))
            Dim recno As String = "", msgServer As String = "", retval As String = ""
            If merchTxnRef = Session("vpc_MerchTxnRef").ToString() Then
                retval = FeeCollectionOnline.F_SaveFEECOLLECTION_H_ONLINE_PAYMENT_FEES(Session("vpc_MerchTxnRef").ToString(), _
                         txnResponseCode, getResponseDescription(txnResponseCode), message, receiptNo, transactionNo, _
                          acqResponseCode, authorizeID, batchNo, cardType, hashValidated.ToString(), amount, _
                          orderInfo, merchantID, command, version, _
                          verType & "|" + verStatus & "|" + token & "|" + verSecurLevel & "|" + enrolled & "|" + xid & "|" + acqECI & "|" + authStatus, _
                          recno, msgServer)
                'If retval = 0 Then
                '    Try
                '        Dim objConn As New SqlConnection(ConnectionManger.GetOASISConnectionString)
                '        Dim pParms(5) As SqlClient.SqlParameter
                '        objConn.Open()
                '        Dim stTrans As SqlTransaction = objConn.BeginTransaction
                '        pParms(0) = New SqlClient.SqlParameter("@STU_ID", Session("STU_ID"))
                '        pParms(1) = New SqlClient.SqlParameter("@ACD_ID", Session("STU_ACD_ID"))
                '        pParms(2) = New SqlClient.SqlParameter("@BSU_ID", Session("sBsuid"))
                '        pParms(3) = New SqlClient.SqlParameter("@ReturnValue", SqlDbType.VarChar)
                '        pParms(3).Direction = ParameterDirection.ReturnValue
                '        SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "[OPL].[SAVEReEnrolment_Payment]", pParms)
                '        If pParms(3).Value = 0 Then
                '            retval = pParms(3).Value
                '            stTrans.Commit()
                '        Else
                '            stTrans.Rollback()
                '        End If
                '    Catch ex As Exception
                '        lblError.CssClass = "diverror"
                '        lblError.Text = "System Error !!! ( " & ex.Message & " )"
                '    End Try

                'End If

            End If
            Try
                FeeCollectionOnline.SAVE_ONLINE_PAYMENT_AUDIT("FEES", "RESPONSE", _
                Page.Request.Url.ToString, Session("vpc_MerchTxnRef").ToString())
            Catch ex As Exception
            End Try
            Session("vpc_MerchTxnRef") = Nothing
            lnkReceipt.Text = recno
            ' lnkReceipt.OnClientClick = "CheckForPrint();return false;"
            lblError.CssClass = "divvalid"
            lblError.Text = msgServer
            'lblHeader.Text = FeeCollectionOnlineBB.GetHeader()
            lblStudentNo.Text = Session("STU_NO").ToString()
            lblStudentName.Text = Session("STU_NAME").ToString()
            Label_MerchTxnRef.Text = merchTxnRef
            Dim Encr_decrData As New Encryption64

            If IsNumeric(amount) Then
                Label_Amount.Text = Format(Convert.ToDecimal(amount) / 100, "0.00")
            Else
                Label_Amount.Text = ""
            End If

            lblDate.Text = Format(Date.Now, "dd/MMM/yyyy")
            ' if message was not provided locally then obtain value from server
            If message.Length = 0 Then
                message = IIf(Page.Request.QueryString("vpc_Message").Length > 0, Page.Request.QueryString("vpc_Message"), "Unknown")
            End If

            If recno.Trim <> "" Then
                h_Recno.Value = Encr_decrData.Encrypt(recno.Trim)
                lblPrintMsg.Visible = True
                ''Send mail + Set Message
                'Dim str_sendmailurl As String = Page.Request.Url.ToString
                'str_sendmailurl = str_sendmailurl.Split("?"c)(0) & "?type=REC&id=" & h_Recno.Value & "&bsu_id=" & Encr_decrData.Encrypt(Session("sBsuid")) & "&user=" & Encr_decrData.Encrypt(Session("SrvUsrName"))
                'str_sendmailurl = str_sendmailurl.Replace("PaymentResultPage.aspx", "FeeReceiptEMail.aspx")
                'If retval = 0 Then
                '    frame1.Attributes("src") = str_sendmailurl
                'End If

            End If

            'Dim uri As New Uri(str_sendmailurl)
            'Dim request As HttpWebRequest = HttpWebRequest.Create(uri)
            'request.Method = WebRequestMethods.Http.[Get]
            'Dim response As HttpWebResponse = request.GetResponse()
            'Dim reader As New StreamReader(response.GetResponseStream())
            'Dim tmp As String = reader.ReadToEnd()
            'response.Close()
            'Page.Response.Write(tmp)
        Catch ex As Exception
            message = "(51) Exception encountered. " & ex.Message
            UtilityObj.Errorlog(ex.Message)
            UtilityObj.Errorlog(ex.ToString())
            If ex.StackTrace.Length > 0 Then
                Label_StackTrace.Text = ex.ToString()
                Panel_StackTrace.Visible = True
            End If
            errorExists = True
        End Try

        ' output the message field
        Label_Message.Text = message

        ' The URL AgainLink and Title are only used for display purposes.
        '    * Note: This is ONLY used for this example and is not required for 
        '    * production code.  


        ' Create a link to the example's HTML order page
        Label_AgainLink.Text = "<a href=""" & Page.Request.QueryString("AgainLink") & """>Another Transaction</a>"

        ' Determine the appropriate title for the receipt page
        Label_Title.Text = IIf((errorExists OrElse txnResponseCode.Equals("7") OrElse txnResponseCode.Equals("Unknown") OrElse hashValidated = False), Page.Request.QueryString("Title") & " Error Page", Page.Request.QueryString("Title") & " Receipt Page")

        ' output debug data to the screen
#If DEBUG Then
    debugData += "<br/><u>End of debug information</u><br/>"
    Label_Debug.Text = debugData
#End If

        '
        '    **********************
        '    * END OF MAIN PROGRAM
        '    **********************
        '    *
        '    * FINISH TRANSACTION - Output the VPC Response Data
        '    * =====================================================
        '    * For the purposes of demonstration, we simply display the Result fields
        '    * on a web page.
        '
        Panel_Debug.Visible = True
    End Sub


    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect("ReEnroll_Process.aspx")
    End Sub
End Class
