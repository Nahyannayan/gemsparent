﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.Xml
<WebService(Namespace:="https://oasis.gemseducation.com/WebService/")> _
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Public Class wsAlertUser
    Inherits System.Web.Services.WebService

    <WebMethod()> _
     Public Function Check_Alert() As XmlDocument
        Dim Xdoc As New XmlDocument
        Dim ds As DataSet
        Dim strXdoc As String = String.Empty
        'Details info --D Short Info S
        ds = alertInfo()
        strXdoc = ds.GetXml
        Xdoc.LoadXml(strXdoc)
        Return Xdoc
    End Function

    Public Function alertInfo() As DataSet
        Dim ds As DataSet
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_ALERTConnectionString").ConnectionString
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[OPL].[GETONLINE_SYS_ALERT]")
        Return ds
    End Function

    <WebMethod(EnableSession:=True)> _
    Public Function Notify() As XmlDocument
        Dim Xdoc As New XmlDocument
        Dim OLU_ID As Integer
        Dim ACD_ID As Integer

        Dim ds As DataSet
        Dim strXdoc As String = String.Empty
        'Details info --D Short Info S
        If Not Context.Session("OLU_ID") Is Nothing Then
            OLU_ID = Context.Session("OLU_ID")
        Else
            OLU_ID = 1
        End If
        If Not Context.Session("STU_ACD_ID") Is Nothing Then
            ACD_ID = Context.Session("STU_ACD_ID")
        Else
            ACD_ID = 1
        End If
        ds = NotifyInfo(OLU_ID, ACD_ID)
        strXdoc = ds.GetXml
        Xdoc.LoadXml(strXdoc)
        Return Xdoc
    End Function

    Public Function NotifyInfo(ByVal OLU_ID As Integer, ByVal ACD_ID As Integer) As DataSet
        Dim ds As DataSet
        Dim PARAM(3) As SqlParameter
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_ALERTConnectionString").ConnectionString
        PARAM(0) = New SqlParameter("@OLU_ID", OLU_ID)
        PARAM(1) = New SqlParameter("@ACD_ID", ACD_ID)
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[OPL].[GETONLINE_STU_ALERT]", PARAM)
        Return ds
    End Function


    <WebMethod(EnableSession:=True)> _
   Public Function Notify_Curr_Child() As XmlDocument
        Dim Xdoc As New XmlDocument
        Dim STU_ID As Integer
        Dim ACD_ID As Integer
        Dim SNAME As String
        Dim ds As DataSet
        Dim strXdoc As String = String.Empty
        'Details info --D Short Info S

        If Not Context.Session("STU_ID") Is Nothing Then
            STU_ID = Context.Session("STU_ID")
        Else
            STU_ID = 1
        End If
        If Not Context.Session("STU_ACD_ID") Is Nothing Then
            ACD_ID = Context.Session("STU_ACD_ID")
        Else
            ACD_ID = 1
        End If
        If Not Context.Session("CURR_STU_FIRSTNAME") Is Nothing Then
            SNAME = Context.Session("CURR_STU_FIRSTNAME")
        Else
            SNAME = ""
        End If

        ds = NotifyInfo_Curr_Child(SNAME, STU_ID, ACD_ID)
        strXdoc = ds.GetXml
        Xdoc.LoadXml(strXdoc)
        Return Xdoc
    End Function

    Public Function NotifyInfo_Curr_Child(ByVal SNAME As String, ByVal STU_ID As Integer, ByVal ACD_ID As Integer) As DataSet
        Dim ds As DataSet
        Dim PARAM(4) As SqlParameter
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_ALERTConnectionString").ConnectionString
        PARAM(0) = New SqlParameter("@STU_ID", STU_ID)
        PARAM(1) = New SqlParameter("@ACD_ID", ACD_ID)
        PARAM(2) = New SqlParameter("@SNAME", SNAME)
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "[OPL].[GETCURRENT_ONLINE_STU_ALERT]", PARAM)
        Return ds
    End Function
End Class
