Imports Microsoft.VisualBasic
Imports Microsoft.ApplicationBlocks.Data
Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Data
Imports System.Data.OleDb
Imports System.Data.Odbc
Imports System.Web
Imports System.Web.Configuration
Imports System.Data.SqlClient
Imports System.Configuration

Public Class Mainclass
#Region "Functions"

    Shared Function SpellNumber(ByVal MyNumber) As String
        Dim Dirhams = Nothing, Fils = Nothing, Temp = Nothing
        Dim DecimalPlace = Nothing, Count = Nothing
        Dim Place As String() = New String(9) {}
        Place(2) = " Thousand "
        Place(3) = " Million "
        Place(4) = " Billion "
        Place(5) = " Trillion "
        ' String representation of amount.
        MyNumber = Trim(Str(MyNumber))
        ' Position of decimal place 0 if none.
        DecimalPlace = InStr(MyNumber, ".")
        ' Convert cents and set MyNumber to Dirhams amount.
        If DecimalPlace > 0 Then
            Fils = GetTens(Left(Mid(MyNumber, DecimalPlace + 1) & _
                      "00", 2))
            MyNumber = Trim(Left(MyNumber, DecimalPlace - 1))
        End If
        Count = 1
        Do While MyNumber <> ""
            Temp = GetHundreds(Right(MyNumber, 3))
            If Temp <> "" Then Dirhams = Temp & Place(Count) & Dirhams
            If Len(MyNumber) > 3 Then
                MyNumber = Left(MyNumber, Len(MyNumber) - 3)
            Else
                MyNumber = ""
            End If
            Count = Count + 1
        Loop
        Select Case Dirhams
            Case ""
                Dirhams = " "
            Case "One"
                'Dirhams = "One Dirhams"<%= IIf(Session("BSU_CURRENCY") Is Nothing, "", "(Amount In " & Session("BSU_CURRENCY") & ")")%>
                Dirhams = "One " & IIf(HttpContext.Current.Session("BSU_CURRENCY") Is Nothing, "", HttpContext.Current.Session("BSU_CURRENCY"))
            Case Else
                Dirhams = IIf(HttpContext.Current.Session("BSU_CURRENCY") Is Nothing, "", HttpContext.Current.Session("BSU_CURRENCY")) & " " & Dirhams
        End Select
        Select Case Fils
            Case ""
                Fils = " "
            Case "One"
                Fils = " and One " & IIf(HttpContext.Current.Session("CUR_DENOMINATION") Is Nothing, "", HttpContext.Current.Session("CUR_DENOMINATION"))
            Case Else
                Fils = " and " & Fils & " " & IIf(HttpContext.Current.Session("CUR_DENOMINATION") Is Nothing, "", HttpContext.Current.Session("CUR_DENOMINATION"))
        End Select
        SpellNumber = Dirhams & Fils
        Return SpellNumber & " Only"
    End Function

    ' Converts a number from 100-999 into text
    Shared Function GetHundreds(ByVal MyNumber)
        Dim Result As String
        If Val(MyNumber) = 0 Then Exit Function
        MyNumber = Right("000" & MyNumber, 3)
        ' Convert the hundreds place.
        If Mid(MyNumber, 1, 1) <> "0" Then
            Result = GetDigit(Mid(MyNumber, 1, 1)) & " Hundred "
        End If
        ' Convert the tens and ones place.
        If Mid(MyNumber, 2, 1) <> "0" Then
            Result = Result & GetTens(Mid(MyNumber, 2))
        Else
            Result = Result & GetDigit(Mid(MyNumber, 3))
        End If
        GetHundreds = Result
    End Function

    ' Converts a number from 10 to 99 into text.
    Shared Function GetTens(ByVal TensText)
        Dim Result As String
        Result = ""           ' Null out the temporary function value.
        If Val(Left(TensText, 1)) = 1 Then   ' If value between 10-19...
            Select Case Val(TensText)
                Case 10 : Result = "Ten"
                Case 11 : Result = "Eleven"
                Case 12 : Result = "Twelve"
                Case 13 : Result = "Thirteen"
                Case 14 : Result = "Fourteen"
                Case 15 : Result = "Fifteen"
                Case 16 : Result = "Sixteen"
                Case 17 : Result = "Seventeen"
                Case 18 : Result = "Eighteen"
                Case 19 : Result = "Nineteen"
                Case Else
            End Select
        Else                                 ' If value between 20-99...
            Select Case Val(Left(TensText, 1))
                Case 2 : Result = "Twenty "
                Case 3 : Result = "Thirty "
                Case 4 : Result = "Forty "
                Case 5 : Result = "Fifty "
                Case 6 : Result = "Sixty "
                Case 7 : Result = "Seventy "
                Case 8 : Result = "Eighty "
                Case 9 : Result = "Ninety "
                Case Else
            End Select
            Result = Result & GetDigit _
                (Right(TensText, 1))  ' Retrieve ones place.
        End If
        GetTens = Result
    End Function

    ' Converts a number from 1 to 9 into text.
    Shared Function GetDigit(ByVal Digit)
        Select Case Val(Digit)
            Case 1 : GetDigit = "One"
            Case 2 : GetDigit = "Two"
            Case 3 : GetDigit = "Three"
            Case 4 : GetDigit = "Four"
            Case 5 : GetDigit = "Five"
            Case 6 : GetDigit = "Six"
            Case 7 : GetDigit = "Seven"
            Case 8 : GetDigit = "Eight"
            Case 9 : GetDigit = "Nine"
            Case Else : GetDigit = ""
        End Select
    End Function

    Public Shared Function getDataTable(ByVal _query As String, ByVal _ConnStr As String) As DataTable
        Dim _conn As New SqlConnection(_ConnStr)
        Dim _RetTable As New DataTable
        Dim _command As New SqlCommand()
        _command.Connection = _conn
        _command.CommandText = _query
        _command.CommandTimeout = 0
        Dim _adapter As New SqlDataAdapter(_command)
        Dim _dataTable As New DataTable("_Tab1")
        Try
            _adapter.Fill(_dataTable)
            _RetTable = _dataTable
        Catch e As Exception
            Throw e
        Finally
            _command.Dispose()
            If _conn IsNot Nothing AndAlso _conn.State = ConnectionState.Open Then
                _conn.Close()
                _conn.Dispose()
            End If
            getDataTable = _RetTable
        End Try
    End Function
    Public Shared Function getDataTable(ByVal _query As String, ByVal _sqlParams() As SqlParameter, ByVal _ConnStr As String) As DataTable
        Dim _conn As New SqlConnection(_ConnStr)
        Dim _RetTable As New DataTable
        Dim _command As New SqlCommand()
        _command.Connection = _conn
        _command.CommandText = _query
        _command.CommandType = CommandType.StoredProcedure
        _command.CommandTimeout = 0
        Dim mSqlParam As SqlParameter
        For Each mSqlParam In _sqlParams
            If mSqlParam Is Nothing Then Continue For
            _command.Parameters.Add(mSqlParam)
        Next
        Dim _adapter As New SqlDataAdapter(_command)
        Dim _dataTable As New DataTable("_Tab1")
        Try
            _adapter.Fill(_dataTable)
            _RetTable = _dataTable
            For Each mSqlParam In _sqlParams
                If mSqlParam Is Nothing Then Continue For
                If mSqlParam.Direction = ParameterDirection.InputOutput Or mSqlParam.Direction = ParameterDirection.Output Then
                    mSqlParam.Value = _command.Parameters(mSqlParam.ParameterName).Value
                End If
            Next
        Catch e As Exception
            Throw e
        Finally
            _command.Dispose()
            If _conn IsNot Nothing AndAlso _conn.State = ConnectionState.Open Then
                _conn.Close()
                _conn.Dispose()
            End If
            getDataTable = _RetTable
        End Try
    End Function

    Public Shared Function isEmail(ByVal inputEmail As String) As Boolean
        If inputEmail.Trim = "" Then
            Return (False)
        Else
            'Dim strRegex As String = "^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" + "\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" + ".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"
            'Dim re As New Regex(strRegex)
            'If re.IsMatch(inputEmail) Then
            '    Return (True)
            'Else
            '    Return (False)
            'End If
            Dim pattern As String = "^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" + "\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" + ".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"  '"(.+@.+\.[a-z]+)"
            Dim expression As System.Text.RegularExpressions.Regex = New System.Text.RegularExpressions.Regex(pattern)
            Return expression.IsMatch(inputEmail)
        End If
    End Function

    Public Shared Function getDataValue(ByVal _query As String, ByVal _connStr As String) As String
        Dim _conn As New SqlConnection()
        _conn.ConnectionString = _connStr
        Dim _RetValue As String = ""
        Dim _command As New SqlCommand()
        _command.Connection = _conn
        _command.CommandText = _query
        Dim _adapter As New SqlDataAdapter(_command)
        Dim _dataTable As New DataTable("_Tab1")
        Try
            _adapter.Fill(_dataTable)
            If _dataTable.Rows.Count > 0 Then
                _RetValue = _dataTable.Rows(0).Item(0)
            Else
                _RetValue = ""
            End If
        Catch e As Exception
            Throw e
        Finally
            _command.Dispose()
            If _conn IsNot Nothing AndAlso _conn.State = ConnectionState.Open Then
                _conn.Close()
                _conn.Dispose()
            End If
            getDataValue = _RetValue
        End Try
    End Function

    Public Shared Function CreateSqlParameter(ByVal ParamName As String, ByVal ParamValue As String, ByVal ParamType As SqlDbType, Optional ByVal IsOutPutParam As Boolean = False, Optional ByVal MaxSize As Int16 = 0) As SqlParameter
        Try
            Dim mSqlParam As New SqlParameter
            mSqlParam.Value = ParamValue
            mSqlParam.ParameterName = ParamName
            mSqlParam.SqlDbType = ParamType
            'mSqlParam.DbType = ParamType
            If MaxSize <> 0 Then
                mSqlParam.Size = MaxSize
            End If
            If IsOutPutParam Then mSqlParam.Direction = ParameterDirection.InputOutput
            CreateSqlParameter = mSqlParam
        Catch ex As Exception

        End Try
    End Function
    Public Shared Function getErrorMessage(ByVal p_errorno As String) As String
        Try
            Dim str_conn As String = ConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            Dim str_Sql As String
            str_Sql = "select * FROM ERRORMESSAGE_M where ERR_NO='" & p_errorno & "' "
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
            If ds.Tables(0).Rows.Count > 0 Then
                Return ds.Tables(0).Rows(0)("ERR_MSG")
            Else
                Return ("Entry not found for error # " & p_errorno)
            End If
        Catch ex As Exception
            'Errorlog(ex.Message)
            Return ("Entry not found for error # " & p_errorno)
        End Try
    End Function
    Public Shared Function cleanString(ByVal p_searchtext As String) As String
        If p_searchtext Is Nothing Then Return p_searchtext
        If (p_searchtext.Length - p_searchtext.Replace("'", "").Replace(";", "").Replace("%", "").Length) > 1 Then
            'more than 2 suspicous char's 
            Dim str_conn As String = ConfigurationManager.ConnectionStrings("mainDB").ConnectionString
            UtilityObj.operOnAudiTable("", "", "ATTACK", , , p_searchtext)
            Return ""
        Else
            Return p_searchtext.Replace("'", "''")
        End If
    End Function
#End Region

End Class

