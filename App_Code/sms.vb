Imports Microsoft.VisualBasic
Imports System.Web.Configuration

Namespace SmsService
    Public Class sms
       
        Public Shared Function SendMessage(ByVal Mobilenumber As String, ByVal Message As String, ByVal From As String, ByVal Username As String, ByVal Password As String) As String
            Dim ReturnValue As String = ""

            Dim ServicePath = WebConfigurationManager.AppSettings("smsServicePath").ToString()
            Dim TestFlag As String = WebConfigurationManager.AppSettings("smsTestFlag").ToString()
            Dim myWebClient As New System.Net.WebClient
            'Dim wp As New System.Net.WebProxy("proxy1.emirates.net.ae", 8080)
            'wp.UseDefaultCredentials = True
            Dim ValueCollection As New System.Collections.Specialized.NameValueCollection
            ValueCollection.Add("UserName", Username)
            ValueCollection.Add("Password", Password)
            ValueCollection.Add("Message", Message)
            ValueCollection.Add("MobNumber", Mobilenumber)
            ValueCollection.Add("MsgType", "SMS")
            ValueCollection.Add("From", From)
            ValueCollection.Add("Test", TestFlag) '' 1- Test , 0- No Test (Live)
            ValueCollection.Add("Store", "1")
            'myWebClient.Proxy = wp
            Dim responseArray As Byte() = myWebClient.UploadValues(ServicePath, "POST", ValueCollection)
            ReturnValue = System.Text.Encoding.ASCII.GetString(responseArray)

            Return ReturnValue

        End Function
    End Class
End Namespace
