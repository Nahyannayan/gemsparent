Imports Microsoft.VisualBasic
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Imports UtilityObj
Imports System.Web.UI.WebControls

Public Class passwordchange

    Public Shared Function changepassword(ByVal OLU_ID As String, ByVal OLDPASSWORD As String, ByVal NEWPASSWORD As String, ByVal MSG As Label, ByVal trans As SqlTransaction)

        Dim param(6) As SqlClient.SqlParameter
        param(0) = New SqlClient.SqlParameter("@OLU_ID", OLU_ID)
        param(1) = New SqlClient.SqlParameter("@OLDPASSWORD", OLDPASSWORD)
        param(2) = New SqlClient.SqlParameter("@NEWPASSWORD", NEWPASSWORD)
        param(3) = New SqlClient.SqlParameter("@Return_value", SqlDbType.Int)
        param(3).Direction = ParameterDirection.ReturnValue
        param(4) = New SqlClient.SqlParameter("@MSG", SqlDbType.VarChar, 100)
        param(4).Direction = ParameterDirection.Output
        Dim status As Integer

        SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "ONLINE.CHANGEPASSWORD", param)
        status = param(3).Value
        MSG.Text = param(4).Value.ToString()
        Return status

    End Function

    Public Shared Function changepassword(ByVal OLU_ID As String, ByVal OLDPASSWORD As String, ByVal NEWPASSWORD As String, ByVal MSG As Label, ByVal NEW_PASSWORD_hash As String, ByVal trans As SqlTransaction)

        Dim param(6) As SqlClient.SqlParameter
        param(0) = New SqlClient.SqlParameter("@OLU_ID", OLU_ID)
        param(1) = New SqlClient.SqlParameter("@OLDPASSWORD", OLDPASSWORD)
        param(2) = New SqlClient.SqlParameter("@NEWPASSWORD", NEWPASSWORD)
        param(3) = New SqlClient.SqlParameter("@NEWHASHEDPASSWORD", NEW_PASSWORD_hash)
        param(4) = New SqlClient.SqlParameter("@Return_value", SqlDbType.Int)
        param(4).Direction = ParameterDirection.ReturnValue
        param(5) = New SqlClient.SqlParameter("@MSG", SqlDbType.VarChar, 100)
        param(5).Direction = ParameterDirection.Output
        Dim status As Integer

        SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "ONLINE.CHANGEPASSWORD_HASH", param)
        status = param(4).Value
        MSG.Text = param(5).Value.ToString()
        Return status

    End Function
End Class
