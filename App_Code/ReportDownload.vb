﻿Imports Microsoft.VisualBasic
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports Microsoft.ApplicationBlocks.Data
Imports System
Imports System.Data.SqlClient
Imports System.IO
Imports System.Data
Imports System.Collections
Imports System.Web
Imports System.Web.Configuration


Public Class ReportDownload
    Sub LoadReports(ByVal rptClass As rptClass, ByVal rs As CrystalDecisions.Web.CrystalReportSource)
        Try
            Dim iRpt As New DictionaryEntry
            Dim i As Integer


            Dim rptStr As String = ""

            Dim crParameterDiscreteValue As ParameterDiscreteValue
            Dim crParameterFieldDefinitions As ParameterFieldDefinitions
            Dim crParameterFieldLocation As ParameterFieldDefinition
            Dim crParameterValues As ParameterValues


            With rptClass

                'C:\Application\ParLogin\ParentLogin\ProgressReports\Rpt\rptTERM_REPORT_OOEHS_DUBAI_GRD05_08.rpt
                rs.ReportDocument.Load(.reportPath)

                Dim myConnectionInfo As ConnectionInfo = New ConnectionInfo()
                myConnectionInfo.ServerName = .crInstanceName
                myConnectionInfo.DatabaseName = .crDatabase
                myConnectionInfo.UserID = .crUser
                myConnectionInfo.Password = .crPassword


                SetDBLogonForSubreports(myConnectionInfo, rs.ReportDocument, .reportParameters)
                SetDBLogonForReport(myConnectionInfo, rs.ReportDocument, .reportParameters)


                'If .subReportCount <> 0 Then
                '    For i = 0 To .subReportCount - 1
                '        rs.ReportDocument.Subreports(i).VerifyDatabase()
                '    Next
                'End If


                crParameterFieldDefinitions = rs.ReportDocument.DataDefinition.ParameterFields
                If .reportParameters.Count <> 0 Then
                    For Each iRpt In .reportParameters
                        crParameterFieldLocation = crParameterFieldDefinitions.Item(iRpt.Key.ToString)
                        crParameterValues = crParameterFieldLocation.CurrentValues
                        crParameterDiscreteValue = New CrystalDecisions.Shared.ParameterDiscreteValue
                        crParameterDiscreteValue.Value = iRpt.Value
                        crParameterValues.Add(crParameterDiscreteValue)
                        crParameterFieldLocation.ApplyCurrentValues(crParameterValues)
                    Next
                End If

                '  rs.ReportDocument.VerifyDatabase()


                If .selectionFormula <> "" Then
                    rs.ReportDocument.RecordSelectionFormula = .selectionFormula
                End If
                '   Response.ClearContent()
                ' Response.ClearHeaders()
                ' Response.ContentType = "application/pdf"
                ' rs.ReportDocument.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, False, "Report")
                exportReport(rs.ReportDocument, CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat)
                rs.ReportDocument.Close()
                rs.Dispose()
                ' Response.Flush()
                'Response.Close()
            End With
        Catch ex As Exception
            rs.ReportDocument.Close()
            rs.Dispose()
        End Try
        'GC.Collect()
    End Sub

    Protected Sub exportReport(ByVal selectedReport As CrystalDecisions.CrystalReports.Engine.ReportDocument, ByVal eft As CrystalDecisions.Shared.ExportFormatType)
        selectedReport.ExportOptions.ExportFormatType = eft

        Dim contentType As String = ""
        ' Make sure asp.net has create and delete permissions in the directory
        Dim tempDir As String = Web.Configuration.WebConfigurationManager.AppSettings("ExportStaff").ToString()
        Dim tempFileName As String = HttpContext.Current.Session("susr_name") + Now.ToString.Replace("/", "_").Replace(":", "_").Replace(" ", "_") + "." ' HttpContext.Current.Session. HttpContext.Current.SessionID.ToString() & "."
        Select Case eft
            Case CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat
                tempFileName += "pdf"
                contentType = "application/pdf"
                Exit Select
            Case CrystalDecisions.[Shared].ExportFormatType.WordForWindows
                tempFileName += "doc"
                contentType = "application/msword"
                Exit Select
            Case CrystalDecisions.[Shared].ExportFormatType.Excel
                tempFileName += "xls"
                contentType = "application/vnd.ms-excel"
                Exit Select
            Case CrystalDecisions.[Shared].ExportFormatType.HTML32, CrystalDecisions.[Shared].ExportFormatType.HTML40
                tempFileName += "htm"
                contentType = "text/html"
                Dim hop As New CrystalDecisions.Shared.HTMLFormatOptions()
                hop.HTMLBaseFolderName = tempDir
                hop.HTMLFileName = tempFileName
                selectedReport.ExportOptions.FormatOptions = hop
                Exit Select
        End Select

        Dim dfo As New CrystalDecisions.Shared.DiskFileDestinationOptions()
        dfo.DiskFileName = tempDir + tempFileName
        selectedReport.ExportOptions.DestinationOptions = dfo
        selectedReport.ExportOptions.ExportDestinationType = CrystalDecisions.[Shared].ExportDestinationType.DiskFile
        selectedReport.Export()
        selectedReport.Close()

        Dim tempFileNameUsed As String
        If eft = CrystalDecisions.[Shared].ExportFormatType.HTML32 OrElse eft = CrystalDecisions.[Shared].ExportFormatType.HTML40 Then
            Dim fp As String() = selectedReport.FilePath.Split("\".ToCharArray())
            Dim leafDir As String = fp(fp.Length - 1)
            ' strip .rpt extension
            leafDir = leafDir.Substring(0, leafDir.Length - 4)
            tempFileNameUsed = String.Format("{0}{1}\{2}", tempDir, leafDir, tempFileName)
        Else
            tempFileNameUsed = tempDir + tempFileName
        End If

        'Response.ClearContent()
        'Response.ClearHeaders()
        'Response.ContentType = contentType

        'Response.WriteFile(tempFileNameUsed)
        'Response.Flush()
        'Response.Close()


        ' HttpContext.Current.Response.ContentType = "application/octect-stream"
        HttpContext.Current.Response.ContentType = "application/pdf"
        HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" & System.IO.Path.GetFileName(tempFileNameUsed))
        HttpContext.Current.Response.Clear()
        HttpContext.Current.Response.WriteFile(tempFileNameUsed)
        'HttpContext.Current.Response.Flush()
        'HttpContext.Current.Response.Close()
        HttpContext.Current.Response.End()
        System.IO.File.Delete(tempFileNameUsed)
    End Sub

    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument, ByVal reportParameters As Hashtable)
        Dim myTables As Tables = myReportDocument.Database.Tables
        Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
        Dim crParameterDiscreteValue As ParameterDiscreteValue
        'Dim crParameterFieldDefinitions As ParameterFieldDefinitions
        'Dim crParameterFieldLocation As ParameterFieldDefinition
        'Dim crParameterValues As ParameterValues
        'Dim iRpt As New DictionaryEntry

        ' Dim myTableLogonInfo As TableLogOnInfo
        ' myTableLogonInfo = New TableLogOnInfo
        'myTableLogonInfo.ConnectionInfo = myConnectionInfo

        For Each myTable In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
            myTable.Location = myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)

        Next


        'crParameterFieldDefinitions = myReportDocument.DataDefinition.ParameterFields
        'If reportParameters.Count <> 0 Then
        '    For Each iRpt In reportParameters
        '        Try
        '            crParameterFieldLocation = crParameterFieldDefinitions.Item(iRpt.Key.ToString)
        '            crParameterValues = crParameterFieldLocation.CurrentValues
        '            crParameterDiscreteValue = New CrystalDecisions.Shared.ParameterDiscreteValue
        '            crParameterDiscreteValue.Value = iRpt.Value
        '            crParameterValues.Add(crParameterDiscreteValue)
        '            crParameterFieldLocation.ApplyCurrentValues(crParameterValues)
        '        Catch ex As Exception
        '        End Try
        '    Next
        'End If

        'myReportDocument.DataSourceConnections(0).SetConnection(myConnectionInfo.ServerName, myConnectionInfo.DatabaseName, myConnectionInfo.UserID, myConnectionInfo.Password) '"LIJO\SQLEXPRESS", "OASIS", "sa", "xf6mt") '
        'myReportDocument.SetDatabaseLogon(myConnectionInfo.ServerName, myConnectionInfo.DatabaseName, myConnectionInfo.UserID, myConnectionInfo.Password, True) '"sa", "xf6mt", "LIJO\SQLEXPRESS", "OASIS") '

        myReportDocument.VerifyDatabase()


    End Sub

    Private Sub SetDBLogonForSubreports(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument, ByVal reportParameters As Hashtable)
        Dim mySections As Sections = myReportDocument.ReportDefinition.Sections
        Dim mySection As Section
        For Each mySection In mySections
            Dim myReportObjects As ReportObjects = mySection.ReportObjects
            Dim myReportObject As ReportObject
            For Each myReportObject In myReportObjects
                If myReportObject.Kind = ReportObjectKind.SubreportObject Then
                    Dim mySubreportObject As SubreportObject = CType(myReportObject, SubreportObject)
                    Dim subReportDocument As ReportDocument = mySubreportObject.OpenSubreport(mySubreportObject.SubreportName)

                    Select Case subReportDocument.Name
                        Case "studphotos"
                            Dim dS As New dsImageRpt
                            Dim fs As New FileStream(HttpContext.Current.Session("StudentPhotoPath"), System.IO.FileMode.Open, System.IO.FileAccess.Read)
                            Dim Image As Byte() = New Byte(fs.Length - 1) {}
                            fs.Read(Image, 0, Convert.ToInt32(fs.Length))
                            fs.Close()
                            Dim dr As dsImageRpt.StudPhotoRow = dS.StudPhoto.NewStudPhotoRow
                            dr.Photo = Image
                            'Add the new row to the dataset
                            dS.StudPhoto.Rows.Add(dr)
                            subReportDocument.SetDataSource(dS)
                        Case "rptSubPhoto.rpt"
                            Dim newWindow As String = String.Empty
                            Dim vrptClass As rptClass
                            Dim rptStr As String = ""
                            If Not newWindow Is String.Empty Then
                                rptStr = "rptClass" + newWindow
                            Else
                                rptStr = "rptClass"
                            End If
                            vrptClass = HttpContext.Current.Session.Item(rptStr)
                            If vrptClass.Photos IsNot Nothing Then
                                SetPhotoToReport(subReportDocument, vrptClass.Photos)
                            End If
                        Case "rptGWAElmtSub"
                            Dim newWindow As String = String.Empty
                            Dim vrptClass As rptClass
                            Dim rptStr As String = ""
                            If Not newWindow Is String.Empty Then
                                rptStr = "rptClass" + newWindow
                            Else
                                rptStr = "rptClass"
                            End If
                            vrptClass = HttpContext.Current.Session.Item(rptStr)
                            If vrptClass.Photos IsNot Nothing Then
                                SetGWAPhotoToReport(subReportDocument, vrptClass.Photos, reportParameters)
                            End If
                        Case Else
                            SetDBLogonForReport(myConnectionInfo, subReportDocument, reportParameters)
                    End Select

                    ' subReportDocument.VerifyDatabase()
                End If
            Next
        Next

    End Sub

    Private Sub SetPhotoToReport(ByVal subReportDocument As ReportDocument, ByVal vPhotos As OASISPhotos)
        Dim arrphotos As ArrayList = vPhotos.IDs
        vPhotos = UpdatePhotoPath(vPhotos)
        Dim dS As New dsImageRpt
        Dim fs As FileStream = Nothing
        Dim Image As Byte() = Nothing
        Dim ienum As IDictionaryEnumerator = vPhotos.vHTPhoto_ID.GetEnumerator
        While (ienum.MoveNext)
            Try
                fs = New FileStream(ienum.Value, System.IO.FileMode.Open, System.IO.FileAccess.Read)
                Image = New Byte(fs.Length - 1) {}
                fs.Read(Image, 0, Convert.ToInt32(fs.Length))
            Catch ex As Exception
            Finally
                If Not fs Is Nothing Then
                    fs.Close()
                End If
            End Try
            Dim dr As dsImageRpt.DSPhotosRow = dS.DSPhotos.NewDSPhotosRow

            dr.IMAGE = Image
            dr.ID = ienum.Key
            'Add the new row to the dataset
            dS.DSPhotos.Rows.Add(dr)
        End While
        subReportDocument.SetDataSource(dS)
        subReportDocument.VerifyDatabase()
    End Sub

    Private Sub SetGWAPhotoToReport(ByVal subReportDocument As ReportDocument, ByVal vPhotos As OASISPhotos, ByVal param As Hashtable)
        Dim arrphotos As ArrayList = vPhotos.IDs
        Dim RPF_ID As String = param.Item("@RPF_ID")
        vPhotos = UpdateGWAPhotoPath(vPhotos, RPF_ID)
        Dim dS As New dsImageRpt
        Dim fs As FileStream = Nothing
        Dim Image As Byte() = Nothing
        Dim ienum As IDictionaryEnumerator = vPhotos.vHTPhoto_ID.GetEnumerator
        While (ienum.MoveNext)
            Try
                fs = New FileStream(ienum.Value, System.IO.FileMode.Open, System.IO.FileAccess.Read)
                Image = New Byte(fs.Length - 1) {}
                fs.Read(Image, 0, Convert.ToInt32(fs.Length))
            Catch ex As Exception
            Finally
                If Not fs Is Nothing Then
                    fs.Close()
                End If
            End Try
            Dim dr As dsImageRpt.dsGWAImageRow = dS.dsGWAImage.NewdsGWAImageRow
            dr.IMAGE = Image
            dr.ID = ienum.Key
            'Add the new row to the dataset
            dS.dsGWAImage.Rows.Add(dr)
        End While
        subReportDocument.SetDataSource(dS)
        subReportDocument.VerifyDatabase()
    End Sub

    Private Function UpdatePhotoPath(ByVal vPhotos As OASISPhotos) As OASISPhotos
        Select Case vPhotos.PhotoType
            Case OASISPhotoType.STUDENT_PHOTO
                Dim Virtual_Path As String = Web.Configuration.WebConfigurationManager.AppSettings("StudentPhotoPath").ToString()  'Replace(WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ConnectionString, "http:", "")
                Dim dtFilePath As DataTable = GETStud_photoPath(vPhotos.IDs) 'get the image
                vPhotos.vHTPhoto_ID = New Hashtable

                For Each dr As DataRow In dtFilePath.Rows
                    If (dr("FILE_PATH") Is DBNull.Value) OrElse (dr("FILE_PATH") Is Nothing) OrElse (dr("FILE_PATH") = "") Then
                        If HttpContext.Current.Session("sbsuid") = "125017" Then
                            vPhotos.vHTPhoto_ID(dr("STU_ID")) = Virtual_Path + "/NOIMG/no_img_gwa.png"
                            ' vPhotos.vHTPhoto_ID(dr("STU_ID")) = Virtual_Path + "/NOIMG/EMPPHOTO.jpg"
                        ElseIf HttpContext.Current.Session("sbsuid") = "114003" Then
                            vPhotos.vHTPhoto_ID(dr("STU_ID")) = Virtual_Path + "/NOIMG/no_image.jpg"
                        Else
                            vPhotos.vHTPhoto_ID(dr("STU_ID")) = Virtual_Path + "/NOIMG/no_image.jpg"
                        End If

                    Else
                        vPhotos.vHTPhoto_ID(dr("STU_ID")) = Virtual_Path + dr("FILE_PATH").ToString
                    End If
                Next
        End Select
        Return vPhotos
    End Function

    Private Function UpdateGWAPhotoPath(ByVal vPhotos As OASISPhotos, ByVal RPF_ID As String) As OASISPhotos
        Select Case vPhotos.PhotoType
            Case OASISPhotoType.STUDENT_PHOTO
                Dim Virtual_Path As String = WebConfigurationManager.ConnectionStrings("STU_REPORT").ConnectionString  'Replace(WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ConnectionString, "http:", "")
                Dim dtFilePath As DataTable = GETStudGWA_photoPath(vPhotos.IDs, RPF_ID) 'get the image
                vPhotos.vHTPhoto_ID = New Hashtable
                For Each dr As DataRow In dtFilePath.Rows
                    If (dr("FILE_PATH") Is DBNull.Value) OrElse (dr("FILE_PATH") Is Nothing) OrElse (dr("FILE_PATH") = "") Then
                        vPhotos.vHTPhoto_ID(dr("STU_ID")) = Web.Configuration.WebConfigurationManager.AppSettings("StudentPhotoPath").ToString() + "/NOIMG/NO_IMG.png"
                    Else
                        vPhotos.vHTPhoto_ID(dr("STU_ID")) = Virtual_Path + dr("FILE_PATH").ToString
                    End If
                Next
        End Select
        Return vPhotos
    End Function

    Private Function GETStudGWA_photoPath(ByVal arrSTU_IDs As ArrayList, ByVal RPF_ID As String) As DataTable
        Dim comma As String = String.Empty
        Dim strStudIDs As String = String.Empty
        For Each ID As Object In arrSTU_IDs
            strStudIDs += comma + ID.ToString
            comma = ", "
        Next
        'Dim sqlString As String = "SELECT '\125017_'+SFU_GRD_ID+'_'+RIGHT(ACY_DESCR,2)+'_'+CONVERT(VARCHAR(100),SFU_RPF_ID)+'\'+SFU_FILEPATH AS FILE_PATH,SFU_STU_ID AS STU_ID" _
        '                                & " FROM CURR.STUDENT_FILEUPLOAD INNER JOIN VW_ACADEMICYEAR_D ON SFU_ACD_ID=ACD_ID " _
        '                                & " INNER JOIN VW_ACADEMICYEAR_M ON ACD_ACY_ID=ACY_ID WHERE SFU_STU_ID IN " _
        '                                & "(" + strStudIDs + ")"


        Dim sqlString As String = "SELECT CASE WHEN ISNULL(SFU_FILEPATH,'')='' THEN '' ELSE '\125017_'+SFU_GRD_ID+'_'+RIGHT(ACY_DESCR,2)+'_'+CONVERT(VARCHAR(100),SFU_RPF_ID)+'\'+SFU_FILEPATH END AS FILE_PATH " _
                                      & " ,STU_ID" _
                                      & " FROM STUDENT_M LEFT OUTER JOIN CURR.STUDENT_FILEUPLOAD ON STU_ID=SFU_STU_ID AND STU_ACD_ID=SFU_ACD_ID AND SFU_RPF_ID=" + RPF_ID _
                                      & " INNER JOIN VW_ACADEMICYEAR_D ON STU_ACD_ID=ACD_ID " _
                                      & " INNER JOIN VW_ACADEMICYEAR_M ON ACD_ACY_ID=ACY_ID WHERE STU_ID IN " _
                                      & "(" + strStudIDs + ")"


        Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_CURRICULUMConnectionString, CommandType.Text, sqlString)
        If ds IsNot Nothing Then
            Return ds.Tables(0)
        End If
        Return Nothing

    End Function

    Private Function GETStud_photoPath(ByVal arrSTU_IDs As ArrayList) As DataTable
        Dim comma As String = String.Empty
        Dim strStudIDs As String = String.Empty
        For Each ID As Object In arrSTU_IDs
            strStudIDs += comma + ID.ToString
            comma = ", "
        Next
        Dim sqlString As String = "SELECT DISTINCT isnull(STU_PHOTOPATH,'') FILE_PATH,STU_ID FROM STUDENT_M where  STU_ID in(" & strStudIDs & ")"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, sqlString)
        If ds IsNot Nothing Then
            Return ds.Tables(0)
        End If
        Return Nothing

    End Function
End Class
