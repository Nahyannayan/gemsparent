Imports System
Imports System.Web
Imports System.Web.Configuration
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls


''' <summary> 
''' Used as the base class for all ASP.NET Web Forms in the application 
''' </summary> 
Public Class BasePage
    Inherits System.Web.UI.Page 
    ' Constructors  
    ''' <summary> 
    ''' Creates a new instance of an BasePage object 
    ''' </summary> 
    Public Sub New()
        MyBase.New()
        ' Local Variables 
        ' Begin 
        AddHandler Me.PreInit, AddressOf Me.BasePage_PreInit
    End Sub



    ' Event Handlers 
    ''' <summary> 
    ''' Event handler for the PreInit event of the Page 
    ''' </summary> 
    Private Sub BasePage_PreInit(ByVal sender As Object, ByVal e As System.EventArgs)

        ' Local Variables 
        'Dim str_TransportProvider = Web.Configuration.WebConfigurationManager.AppSettings("TransportProvider").ToString()
        'If str_TransportProvider <> String.Empty Then
        '    MyBase.Theme = str_TransportProvider
        'Else
        '    MyBase.Theme = WebConfigurationManager.AppSettings("General")
        'End If

    End Sub
End Class
' end BasePage_PreInit 

' 
' Methods 
' 


' none 



' 
' Properties 
' 


' none 


' end class BasePage 
