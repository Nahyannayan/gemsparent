Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Threading
Imports System.IO
Imports System.Net.Mail
Imports EmailService
Imports System


Public Class Emailsending
    Dim passwordEncr As New Encryption64

    Public Function Emailsend(ByVal OLU_ID As String) As String

        Dim strconn As String = System.Configuration.ConfigurationManager.ConnectionStrings("OASISConnectionString").ToString
        Dim str_Qry = "Select BSC_HOST,BSC_USERNAME,BSC_PASSWORD,BSC_PORT,BSC_TYPE,BSC_FROMEMAIL,BSU_NAME,OLU_Email,OLU_ID,OLU_BSU_ID,OLU_PASSWORD,OLU_NAME,OLU_bEmailedPassword from ONLINE.ONLINE_USERS_M INNER JOIN dbo.BSU_COMMUNICATION_M ON BSC_BSU_ID=OLU_BSU_ID INNER JOIN dbo.BUSINESSUNIT_M ON BSU_ID=BSC_BSU_ID where OLU_ID=" + OLU_ID + " AND BSC_TYPE='COM'"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(strconn, CommandType.Text, str_Qry)
        'Dim serverpath As String = ConfigurationManager.AppSettings("EmailAttachments1").ToString
        Dim Subject As String = ""
        Dim fromemailid As String = ""
        Dim host As String = ""
        Dim port As String = ""
        Dim username As String = ""
        Dim password As String = ""
        Dim MailBody As String = ""
        Dim invitationtext As String = ""
        Dim remindertext As String = ""
        Dim SMS_MESSAGE As String = ""
        Dim Bsu_id As String = ""
        Dim Return_msg As String = ""
        If ds.Tables(0).Rows.Count > 0 Then
            If ds.Tables(0).Rows(0).Item("OLU_bEmailedPassword").ToString() = "False" Then
                fromemailid = "communication@gemsedu.com" 'ds.Tables(0).Rows(0).Item("BSC_FROMEMAIL").ToString()
                host = "172.16.10.8" 'ds.Tables(0).Rows(0).Item("BSC_HOST").ToString()
                port = "25" 'ds.Tables(0).Rows(0).Item("BSC_PORT").ToString()
                username = "communication" 'ds.Tables(0).Rows(0).Item("BSC_USERNAME").ToString()
                password = "Gems123" 'ds.Tables(0).Rows(0).Item("BSC_PASSWORD").ToString()
                Subject = "Password of Parent Login"
                Dim emailid = ds.Tables(0).Rows(0).Item("OLU_Email").ToString()
                Bsu_id = ds.Tables(0).Rows(0).Item("OLU_BSU_ID").ToString()

                If isEmail(emailid) Then
                    'MailBody = ScreenScrapeHtml(serverpath + "Survey_Alert" + "\News Letters\Survey.htm")


                    Dim messagedata As String '= " <table border='1'><tr><th colspan='4'align='center'>Parent Login</th></tr> " & _
                    '                           " <tr><td >User Name</td><td>Password</td></tr> "

                    messagedata = "<p>Dear Parent,<br />Please find below your login credentials </p> <p>User Name :#username#<br />Password :#password#</p><p >For any further assistance, please contact school.</p><p> </p><p >********************************************************************************************<br />This is a system generated mail. Please do not reply to this.<br />********************************************************************************************</p><p> </p><p >Disclaimer:<br />This message, together with any attachments, is confidential, is intended only for the use of the addressee(s) and may contain information which is covered by legal, professional or other privilege.If you are not the intended recipient, any copying, distribution or use of this email or its attachments, or of the information contained therein is strictly prohibited; please destroy this email and its attachments and notify us immediately.</p>"



                    Dim OLU_Username As String = ds.Tables(0).Rows(0).Item("OLU_NAME").ToString()
                    Dim OLU_Passwd As String = passwordEncr.Decrypt(ds.Tables(0).Rows(0).Item("OLU_PASSWORD").ToString())
                    messagedata = messagedata.Replace("#username#", OLU_Username)

                    messagedata = messagedata.Replace("#password#", OLU_Passwd)
                    'messagedata = messagedata + " <tr><td >" & OLU_Username & "</td><td >" & OLU_Passwd & "</td>"

                    'messagedata = messagedata + " </table> "

                    'MailBody = MailBody.Replace("<# Content #>", messagedata)
                    'MailBody = MailBody.Replace("<# schoolimage #>", GetBsuLogoPath(Bsu_id))
                    Dim AlternatreMailBody As AlternateView = Net.Mail.AlternateView.CreateAlternateViewFromString(messagedata, Nothing, "text/html")
                    Dim Responsevalue As String = Email.SendNewsLetters(fromemailid, emailid, Subject, AlternatreMailBody, username, password, host, port, "0", False)


                    Dim con As New SqlConnection(strconn)
                    con.Open()
                    Dim trans As SqlTransaction
                    trans = con.BeginTransaction("trans")
                    Try
                        Dim param(5) As SqlClient.SqlParameter
                        param(0) = New SqlClient.SqlParameter("@OEP_OLU_ID", OLU_ID)
                        param(1) = New SqlClient.SqlParameter("@OEP_OLU_Email", emailid)
                        param(2) = New SqlClient.SqlParameter("@OEP_SENDSTATUS", Responsevalue)
                        SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "ONLINE.SAVEONLINE_EMAILPASSWORD_LOG", param)

                        If Responsevalue = "Successfully sent" Then
                            Dim parm(5) As SqlClient.SqlParameter
                            parm(0) = New SqlClient.SqlParameter("@OLU_ID", OLU_ID)
                            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "ONLINE.UPDATEONLINE_USERS_M", parm)
                            Return_msg = "Your password has been sent to your Email address  " + emailid
                        Else
                            Return_msg = Responsevalue
                        End If

                        trans.Commit()
                        con.Close()
                    Catch ex As Exception
                        trans.Rollback()
                        con.Close()
                    End Try

                End If
            Else
                Dim emailid2 = ds.Tables(0).Rows(0).Item("OLU_Email").ToString()
                Return_msg = "Your password has been already sent to your Email address  " + emailid2
            End If
        Else
            Dim emailid = ds.Tables(0).Rows(0).Item("OLU_Email").ToString()
            Return_msg = "Your password has been already sent to your Email address  " + emailid

        End If
        Return Return_msg
    End Function

    Public Function isEmail(ByVal inputEmail As String) As Boolean
        If inputEmail.Trim = "" Then
            Return (False)
        Else
            'Dim strRegex As String = "^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" + "\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" + ".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"
            'Dim re As New Regex(strRegex)
            'If re.IsMatch(inputEmail) Then
            '    Return (True)
            'Else
            '    Return (False)
            'End If
            Dim pattern As String = "^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" + "\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" + ".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"  '"(.+@.+\.[a-z]+)"
            Dim expression As System.Text.RegularExpressions.Regex = New System.Text.RegularExpressions.Regex(pattern)
            Return expression.IsMatch(inputEmail)
        End If
    End Function
    Public Function ScreenScrapeHtml(ByVal url As String) As String
        On Error Resume Next
        Dim objRequest As System.Net.WebRequest = System.Net.HttpWebRequest.Create(url)
        Dim sr As New StreamReader(objRequest.GetResponse().GetResponseStream(), System.Text.Encoding.Default)
        Dim result As String = sr.ReadToEnd()
        sr.Close()
        Return result
    End Function
    Public Function GetBsuLogoPath(ByVal Bsuid) As String
        Dim path = ""

        Dim sql_Connection = System.Configuration.ConfigurationManager.ConnectionStrings("OASISConnectionString").ToString()
        Dim sql_query = "select REPLACE(ISNULL(BSU_BB_LOGO,''),'..\','https://school.gemsoasis.com/') IMGPATH FROM dbo.BUSINESSUNIT_M where BSU_ID='" & Bsuid & "'"

        path = SqlHelper.ExecuteScalar(sql_Connection, CommandType.Text, sql_query)

        Return path
    End Function
End Class
