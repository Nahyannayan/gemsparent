Imports Microsoft.VisualBasic
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System
Imports System.Data
Imports System.IO
Imports System.Collections
Imports System.Reflection
Imports System.Collections.Generic
Imports System.Web.UI.WebControls

Public Class studClass

    Function PopulateCurriculum(ByVal ddlClm As DropDownList, ByVal bsuId As String) As DropDownList
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT DISTINCT CLM_ID,CLM_DESCR FROM CURRICULUM_M AS A" _
                                & " INNER JOIN ACADEMICYEAR_D AS B ON A.CLM_ID=B.ACD_CLM_ID" _
                                & " WHERE ACD_BSU_ID='" + bsuId + "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddlClm.DataSource = ds
        ddlClm.DataTextField = "CLM_DESCR"
        ddlClm.DataValueField = "CLM_ID"
        ddlClm.DataBind()
        Return ddlClm
    End Function

    Public Shared Function SAVESTU_COMM_DETAIL_PUSR(ByVal STU_ID As String, ByVal COMSTREET As String, ByVal COMAREA As String, _
      ByVal COMBLDG As String, ByVal COMAPARTNO As String, ByVal COMPOBOX As String, ByVal COMCITY As String, ByVal COMSTATE As String, _
      ByVal COMCOUNTRY As String, ByVal OFFPHONE As String, ByVal RESPHONE As String, ByVal FAX As String, ByVal Mobile As String, _
      ByVal PRMADDR1 As String, ByVal PRMADDR2 As String, ByVal PRMPOBOX As String, ByVal PRMCITY As String, ByVal PRMCOUNTRY As String, _
      ByVal PRMPHONE As String, ByVal EMAIL As String, ByVal EMIR As String, ByVal transaction As SqlTransaction) As Integer

        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Dim pParms(25) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@STU_ID", STU_ID)
            pParms(1) = New SqlClient.SqlParameter("@COMSTREET", COMSTREET)
            pParms(2) = New SqlClient.SqlParameter("@COMAREA", COMAREA)
            pParms(3) = New SqlClient.SqlParameter("@COMBLDG", COMBLDG)
            pParms(4) = New SqlClient.SqlParameter("@COMAPARTNO", COMAPARTNO)
            pParms(5) = New SqlClient.SqlParameter("@COMPOBOX", COMPOBOX)
            pParms(6) = New SqlClient.SqlParameter("@COMCITY", COMCITY)
            pParms(7) = New SqlClient.SqlParameter("@COMSTATE", COMSTATE)
            pParms(8) = New SqlClient.SqlParameter("@COMCOUNTRY", COMCOUNTRY)
            pParms(9) = New SqlClient.SqlParameter("@OFFPHONE", OFFPHONE)
            pParms(10) = New SqlClient.SqlParameter("@RESPHONE", RESPHONE)
            pParms(11) = New SqlClient.SqlParameter("@FAX", FAX)
            pParms(12) = New SqlClient.SqlParameter("@MOBILE", Mobile)
            pParms(13) = New SqlClient.SqlParameter("@PRMADDR1", PRMADDR1)
            pParms(14) = New SqlClient.SqlParameter("@PRMADDR2", PRMADDR2)
            pParms(15) = New SqlClient.SqlParameter("@PRMPOBOX", PRMPOBOX)
            pParms(16) = New SqlClient.SqlParameter("@PRMCITY", PRMCITY)
            pParms(17) = New SqlClient.SqlParameter("@PRMCOUNTRY", PRMCOUNTRY)
            pParms(18) = New SqlClient.SqlParameter("@PRMPHONE", PRMPHONE)
            pParms(19) = New SqlClient.SqlParameter("@EMAIL", EMAIL)
            pParms(20) = New SqlClient.SqlParameter("@EMIR", EMIR)
            pParms(21) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(21).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(transaction, CommandType.StoredProcedure, "SAVESTU_COMM_DETAIL_PUSR", pParms)
            Dim ReturnFlag As Integer = pParms(21).Value
            Return ReturnFlag
        End Using

    End Function

    Public Shared Function GetSTU_COMM_DETAIL_USR(ByVal STU_ID As String) As SqlDataReader
        'Author(--Lijo)
        'Date   --24/oct/2008
        'Purpose--To get GETSTU_COMM_DETAIL data based 


        Try


            Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
            Dim pParms(1) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@STU_ID", STU_ID)
            'pParms(1) = New SqlClient.SqlParameter("@Contact", contact)
            Dim reader As SqlDataReader = SqlHelper.ExecuteReader(connection, CommandType.StoredProcedure, "GETSTU_COMM_DETAIL", pParms)

            Return reader






        Catch ex As Exception
            Return Nothing
        End Try
    End Function
    Public Function PopulateAcademicYear(ByVal ddlAcademicYear As DropDownList, ByVal clm As String, ByVal bsuid As String)
        ddlAcademicYear.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim str_query As String = " SELECT ACY_DESCR,ACD_ID FROM ACADEMICYEAR_M AS A INNER JOIN ACADEMICYEAR_D AS B" _
                                  & " ON B.ACD_ACY_ID=A.ACY_ID WHERE ACD_BSU_ID='" + bsuid + "' AND ACD_CLM_ID=" + clm _
                                  & " ORDER BY ACY_ID"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddlAcademicYear.DataSource = ds
        ddlAcademicYear.DataTextField = "acy_descr"
        ddlAcademicYear.DataValueField = "acd_id"
        ddlAcademicYear.DataBind()

        str_query = " SELECT ACY_DESCR,ACD_ID FROM ACADEMICYEAR_M AS A INNER JOIN ACADEMICYEAR_D AS B" _
                                 & " ON B.ACD_ACY_ID=A.ACY_ID WHERE ACD_CURRENT=1 AND ACD_BSU_ID='" + bsuid + "' AND ACD_CLM_ID=" + clm
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        Dim li As New ListItem
        li.Text = ds.Tables(0).Rows(0).Item(0)
        li.Value = ds.Tables(0).Rows(0).Item(1)
        ddlAcademicYear.Items(ddlAcademicYear.Items.IndexOf(li)).Selected = True
        'Dim rdr As SqlDataReader
        'Using rdr
        '    rdr = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        '    ' Using rdr As SqlDataReader = SqlHelper.ExecuteReader(con, CommandType.Text, str_query)
        '    Dim li As New ListItem
        '    While rdr.Read
        '        li.Text = rdr.GetString(0)
        '        li.Value = rdr.GetValue(1)
        '        ddlAcademicYear.Items(ddlAcademicYear.Items.IndexOf(li)).Selected = True
        '    End While
        '    ' End Using
        '    rdr.Close()
        'End Using
        Return ddlAcademicYear
    End Function

    Function CheckRateExists(ByVal startDate As DateTime, ByVal sbl_id As String, ByVal acd_id As String) As Boolean
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim str_query As String = "SELECT COUNT(SBL_ID) FROM TRANSPORT.FN_getAREARATE(" + acd_id + ") " _
                                & " WHERE SBL_ID=" + sbl_id + " AND " _
                                 & "'" + Format(Date.Parse(startDate), "yyyy-MM-dd") + "'" _
                                & " BETWEEN STARTDATE AND ENDDATE AND AMOUNT<>0"
        Dim count As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        If count = 0 Then
            Return False
        Else
            Return True
        End If

    End Function

    Function checkDATACORREXIST(ByVal STU_ID As String) As Boolean
        Dim str_query As String
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString


        str_conn = ConnectionManger.GetOASISConnectionString
        str_query = "SELECT count(*) from STUDENT_SERVICES_D WHERE SSV_SVC_ID=1 AND  " _
                  & " convert(datetime,SSV_FROMDATE) >=GetDate() AND " _
                  & " SSV_TODATE IS NULL AND SSV_bACTIVE=0 AND SSV_STU_ID='" + STU_ID + "'  "
        Dim count As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        If count >= 1 Then
            Return True
        Else
            Return False
        End If

    End Function
    Public Function PopulateGrade(ByVal ddl As DropDownList, ByVal acdid As String, Optional ByVal SHF_id As String = "", Optional ByVal STM_id As String = "")
        ddl.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_filter As String = String.Empty
        If SHF_id <> "" Then
            str_filter = " AND grm_shf_id=" + SHF_id + ""

        Else
            str_filter = " AND grm_shf_id<>''"
        End If
        If STM_id <> "" Then
            str_filter += " AND grm_STM_id=" + STM_id + ""

        Else
            str_filter += " AND grm_STM_id<>''"
        End If

        Dim str_query As String = "SELECT distinct grm_display,grm_grd_id,grd_displayorder FROM grade_bsu_m,grade_m WHERE" _
                               & " grade_bsu_m.grm_grd_id=grade_m.grd_id and " _
                           & "  grm_acd_id=" + acdid + str_filter + " order by grd_displayorder"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddl.DataSource = ds
        ddl.DataTextField = "grm_display"
        ddl.DataValueField = "grm_grd_id"
        ddl.DataBind()
        Return ddl
    End Function

    Public Function PopulateGradeShift(ByVal ddl As DropDownList, ByVal grdid As String, ByVal acdid As String)
        ddl.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT distinct shf_descr,shf_id FROM grade_bsu_m,shifts_m WHERE" _
                                 & " grade_bsu_m.grm_shf_id=shifts_m.shf_id and " _
                             & " grm_acd_id=" + acdid + " and grm_grd_id='" + grdid + "'"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddl.DataSource = ds
        ddl.DataTextField = "shf_descr"
        ddl.DataValueField = "shf_id"
        ddl.DataBind()
        Return ddl
    End Function

    Public Function PopulateGradeStream(ByVal ddl As DropDownList, ByVal grdid As String, ByVal acdid As String, ByVal shfid As String)
        ddl.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT stm_descr,grm_id FROM grade_bsu_m,stream_m WHERE" _
                                 & " grade_bsu_m.grm_stm_id=stream_m.stm_id and " _
                             & " grm_acd_id=" + acdid + " and grm_grd_id='" + grdid + "' and grm_shf_id=" + shfid
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

        ddl.DataSource = ds
        ddl.DataTextField = "stm_descr"
        ddl.DataValueField = "grm_id"
        ddl.DataBind()
        If Not ddl.Items.FindByValue("1") Is Nothing Then
            ddl.Items.FindByValue("1").Selected = True
        End If

        Return ddl
    End Function


    Public Function PopulateShift(ByVal ddl As DropDownList, ByVal bsuid As String, Optional ByVal acd_id As String = "")
        ddl.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = String.Empty

        If acd_id <> "" Then
            str_query = "select SHF_ID,SHF_DESCR from SHIFTS_M WHERE SHF_ID IN(select distinct " _
                   & " grm_shf_id from grade_bsu_m where grm_acd_id='" & acd_id & "')"
        Else

            str_query = "Select shf_descr,shf_id from shifts_m where shf_bsu_id='" + bsuid + "'"

        End If

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddl.DataSource = ds
        ddl.DataTextField = "shf_descr"
        ddl.DataValueField = "shf_id"
        ddl.DataBind()
        Return ddl
    End Function

    Public Function PopulateStream(ByVal ddl As DropDownList, Optional ByVal acd_id As String = "")
        ddl.Items.Clear()
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = String.Empty
        If acd_id <> "" Then
            str_query = "SELECT STM_ID,STM_DESCR  FROM STREAM_M WHERE STM_ID IN( " _
                    & " select distinct grm_STM_id from grade_bsu_m where grm_acd_id='" & acd_id & "')"
        Else
            str_query = "Select stm_id,stm_descr from stream_m"

        End If

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)
        ddl.DataSource = ds
        ddl.DataTextField = "stm_descr"
        ddl.DataValueField = "stm_id"
        ddl.DataBind()
        Return ddl
    End Function
    Public Function EnquiryChange(ByVal eqsid As String, ByVal eqs_status As String, ByVal offerdate As Date, ByVal current_status As Integer, ByVal pra_stg_id As Integer, ByVal pra_bcompleted As Boolean)

        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim pParms(7) As SqlClient.SqlParameter

        pParms(0) = New SqlClient.SqlParameter("@EQS_ID", eqsid)

        pParms(1) = New SqlClient.SqlParameter("@EQS_STATUS", eqs_status)

        pParms(2) = New SqlClient.SqlParameter("@OFFER_DATE", offerdate)

        pParms(3) = New SqlClient.SqlParameter("@CURRENT_STATUS", current_status)

        pParms(4) = New SqlClient.SqlParameter("@PRA_STG_ID", pra_stg_id)

        pParms(5) = New SqlClient.SqlParameter("@PRA_bCOMPLETED", pra_bcompleted)

        pParms(6) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)

        pParms(6).Direction = ParameterDirection.ReturnValue

        SqlHelper.ExecuteNonQuery(str_conn, CommandType.StoredProcedure, "StudentEnqChange", pParms)

        Dim returnflag As Integer = pParms(6).Value

        Return returnflag

    End Function


    Public Sub SetChk(ByVal gvGrid As GridView, ByVal li As List(Of String))
        Try
            Dim i As Integer
            Dim chk As CheckBox
            If gvGrid.Rows.Count > 0 Then
                For i = 0 To gvGrid.Rows.Count - 1
                    chk = gvGrid.Rows(i).FindControl("chkSelect")
                    If chk.Checked = True Then
                        If list_add(chk.ClientID + "-" + gvGrid.PageIndex.ToString, li) = False Then
                            chk.Checked = True
                            gvGrid.Rows(i).BackColor = Drawing.Color.FromName("#f6deb2")
                        End If
                    Else
                        If list_exist(chk.ClientID + "-" + gvGrid.PageIndex.ToString, li) = True Then
                            chk.Checked = True
                            gvGrid.Rows(i).BackColor = Drawing.Color.FromName("#f6deb2")
                        End If
                        list_remove(chk.ClientID + "-" + gvGrid.PageIndex.ToString, li)
                    End If

                Next

            End If
        Catch ex As Exception
        End Try
    End Sub
    Private Function list_exist(ByVal p_userid As String, ByVal li As List(Of String)) As Boolean
        If li.Contains(p_userid) Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Function list_add(ByVal p_userid As String, ByVal li As List(Of String)) As Boolean
        If li.Contains(p_userid) Then
            Return False
        Else
            li.Add(p_userid)
            Return False
        End If
    End Function

    Private Sub list_remove(ByVal p_userid As String, ByVal li As List(Of String))
        If li.Contains(p_userid) Then
            li.Remove(p_userid)
        End If
    End Sub

    Public Function GetEmpId(ByVal userName As String) As Integer
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT USR_EMP_ID FROM USERS_M WHERE USR_NAME='" + userName + "'"
        Dim empId As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        Return empId
    End Function

    Public Function isEmpTeacher(ByVal empId As Integer) As Boolean
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT EMP_ECT_ID FROM EMPLOYEE_M WHERE EMP_ID=" + empId.ToString
        Dim emp_ect_Id As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        If emp_ect_Id = 1 Then
            Return True
        Else
            Return False
        End If
    End Function


    Public Sub BindDocumentsList(ByVal lst As ListBox, ByVal stgId As String, ByVal bcomplete As String, ByVal eqsId As String)
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString

        Dim str_query As String
        Dim li As ListItem
        Dim reader As SqlDataReader
        If bcomplete = "false" Then

            str_query = "SELECT DOC_DESCR=CASE DCE_COPIES WHEN 1 THEN DOC_DESCR ELSE CONVERT(VARCHAR(10),DCE_COPIES)+' '+DOC_DESCR END," _
                        & " DOC_ID=CONVERT(VARCHAR(100),DOC_ID)+'|'+CONVERT(VARCHAR(100),DCE_STG_ID),DCE_STG_ID FROM DOCREQD_M AS A INNER JOIN" _
                        & " ENQUIRY_JOINDOCUMENTS_S AS B ON A.DOC_ID=B.DCE_DOC_ID" _
                        & " WHERE  DCE_EQS_ID=" + eqsId + " AND DCE_STG_ID IN (" _
                        & " SELECT PRA_STG_ID FROM PROCESSFO_APPLICANT_S WHERE PRA_STG_ORDER<=(" _
                        & " SELECT PRA_STG_ORDER FROM PROCESSFO_APPLICANT_S WHERE PRA_STG_ID=" + stgId + " AND PRA_EQS_ID=" + eqsId + ")" _
                        & " AND PRA_EQS_ID=" + eqsId + ")" _
                        & " AND DCE_bCOMPLETE='false'"
            reader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)

        Else
            str_query = "SELECT DOC_DESCR=CASE DCE_COPIES WHEN 1 THEN DOC_DESCR ELSE CONVERT(VARCHAR(10),DCE_COPIES)+' '+DOC_DESCR END," _
                        & " DOC_ID=CONVERT(VARCHAR(100),DOC_ID)+'|'+CONVERT(VARCHAR(100),DCE_STG_ID),DCE_STG_ID FROM DOCREQD_M AS A INNER JOIN" _
                        & " ENQUIRY_JOINDOCUMENTS_S AS B ON A.DOC_ID=B.DCE_DOC_ID" _
                        & " WHERE  DCE_EQS_ID=" + eqsId + " AND DCE_bCOMPLETE='true'"

            reader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)

        End If

        While reader.Read
            li = New ListItem
            li.Text = reader.GetString(0)
            li.Value = reader.GetString(1)
            li.Attributes.Add("title", reader.GetString(0))
            If bcomplete = "false" Then
                If reader.GetValue(2).ToString <> stgId Then
                    li.Attributes.Add("style", "color:red")
                End If
            End If
            lst.Items.Add(li)
        End While
        reader.Close()
    End Sub

    Public Sub UpdateDocListItems(ByVal lstFrom As ListBox, ByVal lstTo As ListBox, ByVal complete As String, ByVal eqsId As String)
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        Dim li As ListItem
        Dim lstTemp As New ListBox


        For Each li In lstFrom.Items
            If li.Selected = True Then
                lstTo.Items.Add(li)
                lstTemp.Items.Add(li)
                Dim ids As String()
                ids = li.Value.Split("|")
                str_query = "exec updateENQUIRYJOINDOCUMENTS " _
                        & eqsId + "," _
                        & ids(1) + "," _
                        & ids(0) + "," _
                        & "'" + complete + "'"
                SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
            End If
        Next

        For Each li In lstTemp.Items
            lstFrom.Items.Remove(li)
        Next

        For Each li In lstFrom.Items
            li.Attributes.Add("title", li.Text)
        Next

        For Each li In lstTo.Items
            li.Attributes.Add("title", li.Text)
        Next


        lstTo.ClearSelection()
        lstFrom.ClearSelection()
    End Sub

    Public Sub UpdateEnquiryStage(ByVal mode As String, ByVal eqsId As String)
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String
        str_query = "EXEC updateENQUIRYDOCUMENTSTAGE " + eqsId + ",'" + mode + "'"
        SqlHelper.ExecuteNonQuery(str_conn, CommandType.Text, str_query)
    End Sub

    Function GetBsuShowDocs(ByVal bsuid As String) As Boolean
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT isnull(BSU_bSHOWDOCS,'false') FROM BUSINESSUNIT_M WHERE BSU_ID='" + bsuid + "'"
        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(str_conn, CommandType.Text, str_query)
        Dim bShow As Boolean
        While reader.Read
            bShow = reader.GetBoolean(0)
        End While
        reader.Close()
        Return bShow
    End Function

    Function checkFeeClosingDate(ByVal BSU_ID As String, ByVal acd_id As Integer, ByVal currDate As DateTime) As Boolean
        Dim str_query As String
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        str_query = "SELECT SVB_PROVIDER_BSU_ID FROM SERVICES_BSU_M WHERE SVB_SVC_ID=1 AND SVB_BSU_ID='" + BSU_ID + "' AND SVB_ACD_ID=" + acd_id.ToString
        Dim providerBsu As String = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)

        str_conn = ConnectionManger.GetOASISTRANSPORTConnectionString
        str_query = "select count(*) from FEES.FEE_TRNSALL where " _
                  & " convert(datetime,FTA_TRANDT) >=convert(datetime,'" + Format(currDate, "yyyy-MM-dd") + "') and " _
                  & " (FTA_BSU_ID='" + providerBsu + "' or   FTA_STU_BSU_ID='" + BSU_ID + "') and FTA_TRANTYPE='MONTHEND' and isnull(FTA_BCLOSE,0)=1"
        Dim count As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        If count >= 1 Then
            Return False
        End If
        str_conn = ConnectionManger.GetOASISConnectionString
        str_query = "select count(*) from FEES.FEE_TRNSALL where " _
             & " convert(datetime,FTA_TRANDT) >=convert(datetime,'" + Format(currDate, "yyyy-MM-dd") + "') and " _
             & " (FTA_BSU_ID='" + BSU_ID + "') and FTA_TRANTYPE='MONTHEND' and isnull(FTA_BCLOSE,0)=1"

        count = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        If count >= 1 Then
            Return False
        End If
        Return True
    End Function


    Function isFeeIdAuto(ByVal acdId As String) As Boolean
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim str_query As String = "SELECT ISNULL(ACD_bGENFEEID,'TRUE') FROM ACADEMICYEAR_D WHERE ACD_ID=" + acdId
        Dim auto As Boolean
        auto = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, str_query)
        If auto = True Then
            Return True
        Else
            Return False
        End If
    End Function

    Function GetACD_ID(ByVal sDate As String, ByVal bsu_id As String, ByVal clm As String) As Integer
        Dim str_conn As String = ConnectionManger.GetOASISConnectionString
        Dim STR_query As String = "SELECT ACD_ID FROM ACADEMICYEAR_D WHERE ACD_CLM_ID=" + clm _
                                & " AND ACD_BSU_ID='" + bsu_id + "' AND " _
                                & "'" + Format(Date.Parse(sDate), "yyyy-MM-dd") + "'" _
                                & " BETWEEN ACD_STARTDT AND ACD_ENDDT"
        Dim acd_id As Integer = SqlHelper.ExecuteScalar(str_conn, CommandType.Text, STR_query)
        Return acd_id
    End Function

    ''added by Suriya on 26-jan-10
    Public Shared Function GetStudent_D(ByVal sView_ID As String, ByVal sCurBusUnit As String) As SqlDataReader

        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetStudent_D As String = ""

        sqlGetStudent_D = " SELECT  STS_FFIRSTNAME  , STS_FMIDNAME , STS_FLASTNAME , STS_FNATIONALITY  , STS_FNATIONALITY2, STS_FCOMADDR1 ,   " & _
                         " STS_FCOMADDR2 ,STS_FCOMPOBOX , STS_FCOMCITY STS_FCOMCITY, STS_FCOMSTATE ,STS_FCOMCOUNTRY  , STS_FOFFPHONE , STS_FRESPHONE,STS_FFAX, " & _
                         " STS_FMOBILE , STS_FPRMADDR1 , STS_FPRMADDR2 , STS_FPRMPOBOX , STS_FPRMCITY , STS_FPRMCOUNTRY , STS_FPRMPHONE , STS_FOCC , " & _
                         " STS_FEMAIL,STS_FCOMPANY_ADDR, STS_bFGEMSEMP, STS_F_BSU_ID , STS_MFIRSTNAME , STS_MMIDNAME , STS_MLASTNAME , STS_MNATIONALITY ,   " & _
                         " STS_MNATIONALITY2 , STS_MCOMADDR1, STS_MCOMADDR2 , STS_MCOMPOBOX , STS_MCOMCITY , STS_MCOMSTATE , STS_MCOMCOUNTRY ,  " & _
                         " STS_MOFFPHONE  , STS_MRESPHONE , STS_MFAX , STS_MMOBILE, STS_MPRMADDR1 , STS_MPRMADDR2 , STS_MPRMPOBOX , STS_MPRMCITY ,   " & _
                         " STS_MPRMCOUNTRY , STS_MPRMPHONE , STS_MOCC , STS_MCOMPANY ,STS_MEMAIL,STS_MCOMPANY_ADDR, STS_bMGEMSEMP, STS_M_BSU_ID , STS_GFIRSTNAME , STS_GMIDNAME ," & _
                         " STS_GLASTNAME , STS_GNATIONALITY , STS_GNATIONALITY2 , STS_GCOMADDR1 , STS_GCOMADDR2 ,STS_GCOMPOBOX , STS_GCOMCITY ," & _
                         " STS_GCOMSTATE , STS_GCOMCOUNTRY , STS_GOFFPHONE , STS_GRESPHONE , STS_GFAX , STS_GMOBILE , STS_GPRMADDR1 , STS_GPRMADDR2 ,  " & _
                         " STS_GPRMPOBOX , STS_GPRMCITY , STS_GPRMCOUNTRY , STS_GPRMPHONE , STS_GOCC , STS_GCOMPANY ,STS_GEMAIL,STS_GCOMPANY_ADDR, " & _
                         " STS_FCOMPANY,STS_F_COMP_ID, STS_M_COMP_ID, STS_MCOMPANY,STS_G_COMP_ID, STS_GCOMPANY, " & _
                         " STS_FCOMSTREET,STS_FCOMAREA,STS_FCOMBLDG,STS_FCOMAPARTNO,STS_MCOMSTREET,STS_MCOMAREA,STS_MCOMBLDG,STS_MCOMAPARTNO, " & _
                         " STS_GCOMSTREET,STS_GCOMAREA,STS_GCOMBLDG,STS_GCOMAPARTNO,STS_FEMIR,STS_MEMIR,STS_GEMIR,STS_FEESPONSOR,Left(STU_PRIMARYCONTACT,1) as STU_PRIMARYCONTACT,STU_PREFCONTACT, BSU_CITY,isNULL(STU_EMGCONTACT,'') as STU_EMGCONTACT ,STS_FCOMSTATE_ID ,STS_FCOMAREA_ID,STS_MCOMSTATE_ID ,STS_MCOMAREA_ID,STS_GCOMSTATE_ID ,STS_GCOMAREA_ID" & _
                         "   FROM   STUDENT_D SD INNER JOIN STUDENT_M SM ON SD.STS_STU_ID=SM.STU_SIBLING_ID INNER JOIN BUSINESSUNIT_M BM ON BM.BSU_ID=SM.STU_BSU_ID  where STU_ID='" & sView_ID & "' AND STU_BSU_ID='" & sCurBusUnit & "'"


        Dim command As SqlCommand = New SqlCommand(sqlGetStudent_D, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader
    End Function
    Public Shared Function GetNational() As SqlDataReader
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetNational As String = ""

        sqlGetNational = "Select CTY_ID,case CTY_ID when '5' then 'Not Available' else isnull(CTY_NATIONALITY,'') end CTY_NATIONALITY from Country_m  order by CTY_NATIONALITY"

        Dim command As SqlCommand = New SqlCommand(sqlGetNational, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader
    End Function
    Public Shared Function GetCompany_Name() As SqlDataReader

        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetComp_Name As String = ""

        sqlGetComp_Name = "Select comp_ID,comp_Name from comp_Listed_M order by comp_Name"


        Dim command As SqlCommand = New SqlCommand(sqlGetComp_Name, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)

        Return reader
    End Function
    Public Shared Function UpdateSTUDENT_M(ByVal STU_ID As String, ByVal STU_PREFCONTACT As String, ByVal STU_EMGCONTACT As String, ByVal trans As SqlTransaction) As Integer

        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Try
                Dim pParms(6) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@STU_ID", STU_ID)
                pParms(1) = New SqlClient.SqlParameter("@STU_PREFCONTACT", STU_PREFCONTACT)
                pParms(2) = New SqlClient.SqlParameter("@STU_EMGCONTACT", STU_EMGCONTACT)
                pParms(3) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                pParms(3).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "UpdateStudent_M_PrimaryContact", pParms)
                Dim ReturnFlag As Integer = pParms(3).Value
                Return ReturnFlag
            Catch ex As Exception
                Return 1000
            End Try
        End Using
    End Function
    Public Shared Function UpdateSTUDENT_M_PassportVisa(ByVal STU_ID As String, ByVal STU_PASPRTNO As String, _
    ByVal STU_PASPRTISSPLACE As String, ByVal STU_PASPRTISSDATE As String, ByVal STU_PASPRTEXPDATE As String, _
    ByVal STU_VISANO As String, ByVal STU_VISAISSPLACE As String, ByVal STU_VISAISSDATE As String, _
    ByVal STU_VISAEXPDATE As String, ByVal STU_VISAISSAUTH As String, ByVal STU_EMIRATES_ID As String, _
 ByVal stu_PremisesID As String, ByVal STU_EMIRATES_ID_EXPDATE As String, ByVal trans As SqlTransaction) As Integer

        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Try
                Dim pParms(14) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@STU_ID", STU_ID)
                pParms(1) = New SqlClient.SqlParameter("@STU_PASPRTNO", STU_PASPRTNO)
                pParms(2) = New SqlClient.SqlParameter("@STU_PASPRTISSPLACE", STU_PASPRTISSPLACE)
                pParms(3) = New SqlClient.SqlParameter("@STU_PASPRTISSDATE", STU_PASPRTISSDATE)
                pParms(4) = New SqlClient.SqlParameter("@STU_PASPRTEXPDATE", STU_PASPRTEXPDATE)
                pParms(5) = New SqlClient.SqlParameter("@STU_VISANO", STU_VISANO)
                pParms(6) = New SqlClient.SqlParameter("@STU_VISAISSPLACE", STU_VISAISSPLACE)
                pParms(7) = New SqlClient.SqlParameter("@STU_VISAISSDATE", STU_VISAISSDATE)
                pParms(8) = New SqlClient.SqlParameter("@STU_VISAEXPDATE", STU_VISAEXPDATE)
                pParms(9) = New SqlClient.SqlParameter("@STU_VISAISSAUTH", STU_VISAISSAUTH)
                pParms(10) = New SqlClient.SqlParameter("@STU_EMIRATES_ID", STU_EMIRATES_ID)
                pParms(11) = New SqlClient.SqlParameter("@stu_PremisesID", stu_PremisesID)
                pParms(12) = New SqlClient.SqlParameter("@STU_EMIRATES_ID_EXPDATE", IIf(STU_EMIRATES_ID_EXPDATE = "", System.DBNull.Value, STU_EMIRATES_ID_EXPDATE))

                pParms(13) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                pParms(13).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "UpdateStudent_M_PassportVisa", pParms)
                Dim ReturnFlag As Integer = pParms(13).Value
                Return ReturnFlag
            Catch ex As Exception
                Return 1000
            End Try
        End Using
    End Function
    Public Shared Function UpdateSTUDENT_D(ByVal STS_STU_ID As String, ByVal STS_FFIRSTNAME As String, ByVal STS_FMIDNAME As String, _
  ByVal STS_FLASTNAME As String, ByVal STS_FNATIONALITY As String, ByVal STS_FNATIONALITY2 As String, ByVal STS_FCOMADDR1 As String, _
  ByVal STS_FCOMADDR2 As String, ByVal STS_FCOMPOBOX As String, ByVal STS_FCOMCITY As String, ByVal STS_FCOMSTATE As String, _
  ByVal STS_FCOMCOUNTRY As String, ByVal STS_FOFFPHONE As String, ByVal STS_FRESPHONE As String, ByVal STS_FFAX As String, _
  ByVal STS_FMOBILE As String, ByVal STS_FPRMADDR1 As String, ByVal STS_FPRMADDR2 As String, ByVal STS_FPRMPOBOX As String, _
  ByVal STS_FPRMCITY As String, ByVal STS_FPRMCOUNTRY As String, ByVal STS_FPRMPHONE As String, ByVal STS_FOCC As String, _
  ByVal STS_FCOMPANY As String, ByVal STS_MFIRSTNAME As String, _
  ByVal STS_MMIDNAME As String, ByVal STS_MLASTNAME As String, ByVal STS_MNATIONALITY As String, ByVal STS_MNATIONALITY2 As String, _
  ByVal STS_MCOMADDR1 As String, ByVal STS_MCOMADDR2 As String, ByVal STS_MCOMPOBOX As String, ByVal STS_MCOMCITY As String, _
  ByVal STS_MCOMSTATE As String, ByVal STS_MCOMCOUNTRY As String, ByVal STS_MOFFPHONE As String, ByVal STS_MRESPHONE As String, _
  ByVal STS_MFAX As String, ByVal STS_MMOBILE As String, ByVal STS_MPRMADDR1 As String, ByVal STS_MPRMADDR2 As String, _
  ByVal STS_MPRMPOBOX As String, ByVal STS_MPRMCITY As String, ByVal STS_MPRMCOUNTRY As String, ByVal STS_MPRMPHONE As String, _
  ByVal STS_MOCC As String, ByVal STS_MCOMPANY As String, _
  ByVal STS_GFIRSTNAME As String, ByVal STS_GMIDNAME As String, ByVal STS_GLASTNAME As String, ByVal STS_GNATIONALITY As String, _
  ByVal STS_GNATIONALITY2 As String, ByVal STS_GCOMADDR1 As String, ByVal STS_GCOMADDR2 As String, ByVal STS_GCOMPOBOX As String, _
  ByVal STS_GCOMCITY As String, ByVal STS_GCOMSTATE As String, ByVal STS_GCOMCOUNTRY As String, ByVal STS_GOFFPHONE As String, _
  ByVal STS_GRESPHONE As String, ByVal STS_GFAX As String, ByVal STS_GMOBILE As String, ByVal STS_GPRMADDR1 As String, _
  ByVal STS_GPRMADDR2 As String, ByVal STS_GPRMPOBOX As String, ByVal STS_GPRMCITY As String, ByVal STS_GPRMCOUNTRY As String, _
  ByVal STS_GPRMPHONE As String, ByVal STS_GOCC As String, ByVal STS_GCOMPANY As String, _
  ByVal STS_FCOMPANY_ADDR As String, ByVal STS_FEMAIL As String, ByVal STS_MCOMPANY_ADDR As String, ByVal STS_MEMAIL As String, ByVal STS_GCOMPANY_ADDR As String, ByVal STS_GEMAIL As String, _
  ByVal STS_F_COMP_ID As String, ByVal STS_M_COMP_ID As String, ByVal STS_G_COMP_ID As String, _
  ByVal STS_FCOMSTREET As String, ByVal STS_FCOMAREA As String, ByVal STS_FCOMBLDG As String, ByVal STS_FCOMAPARTNO As String, _
  ByVal STS_MCOMSTREET As String, ByVal STS_MCOMAREA As String, ByVal STS_MCOMBLDG As String, ByVal STS_MCOMAPARTNO As String, _
  ByVal STS_GCOMSTREET As String, ByVal STS_GCOMAREA As String, ByVal STS_GCOMBLDG As String, ByVal STS_GCOMAPARTNO As String, _
  ByVal STS_FEMIR As String, ByVal STS_MEMIR As String, ByVal STS_GEMIR As String, _
   ByVal STS_FEMIRATES_ID As String, ByVal STS_MEMIRATES_ID As String, ByVal STS_GEMIRATES_ID As String, _
  ByVal STS_FEMIRATES_ID_EXPDATE As String, ByVal STS_MEMIRATES_ID_EXPDATE As String, ByVal STS_GEMIRATES_ID_EXPDATE As String, _
    ByVal STS_FCOMAREA_ID As String, ByVal STS_FCOMSTATE_ID As String, ByVal STS_MCOMAREA_ID As String, ByVal STS_MCOMSTATE_ID As String, ByVal STS_GCOMAREA_ID As String, ByVal STS_GCOMSTATE_ID As String, _
  ByVal trans As SqlTransaction) As Integer
        Dim SESSION_USER As String
        SESSION_USER = HttpContext.Current.Session("username")

        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Try
                Dim pParms(121) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@STS_STU_ID", STS_STU_ID)
                pParms(1) = New SqlClient.SqlParameter("@STS_FFIRSTNAME", STS_FFIRSTNAME)
                pParms(2) = New SqlClient.SqlParameter("@STS_FMIDNAME", STS_FMIDNAME)
                pParms(3) = New SqlClient.SqlParameter("@STS_FLASTNAME", STS_FLASTNAME)
                pParms(4) = New SqlClient.SqlParameter("@STS_FNATIONALITY", STS_FNATIONALITY)
                pParms(5) = New SqlClient.SqlParameter("@STS_FNATIONALITY2", STS_FNATIONALITY2)
                pParms(6) = New SqlClient.SqlParameter("@STS_FCOMADDR1", STS_FCOMADDR1)
                pParms(7) = New SqlClient.SqlParameter("@STS_FCOMADDR2", STS_FCOMADDR2)
                pParms(8) = New SqlClient.SqlParameter("@STS_FCOMPOBOX", STS_FCOMPOBOX)
                pParms(9) = New SqlClient.SqlParameter("@STS_FCOMCITY", STS_FCOMCITY)
                pParms(10) = New SqlClient.SqlParameter("@STS_FCOMSTATE", STS_FCOMSTATE)
                pParms(11) = New SqlClient.SqlParameter("@STS_FCOMCOUNTRY", STS_FCOMCOUNTRY)
                pParms(12) = New SqlClient.SqlParameter("@STS_FOFFPHONE", STS_FOFFPHONE)
                pParms(13) = New SqlClient.SqlParameter("@STS_FRESPHONE", STS_FRESPHONE)
                pParms(14) = New SqlClient.SqlParameter("@STS_FFAX", STS_FFAX)
                pParms(15) = New SqlClient.SqlParameter("@STS_FMOBILE", STS_FMOBILE)
                pParms(16) = New SqlClient.SqlParameter("@STS_FPRMADDR1", STS_FPRMADDR1)
                pParms(17) = New SqlClient.SqlParameter("@STS_FPRMADDR2", STS_FPRMADDR2)
                pParms(18) = New SqlClient.SqlParameter("@STS_FPRMPOBOX", STS_FPRMPOBOX)
                pParms(19) = New SqlClient.SqlParameter("@STS_FPRMCITY", STS_FPRMCITY)
                pParms(20) = New SqlClient.SqlParameter("@STS_FPRMCOUNTRY", STS_FPRMCOUNTRY)
                pParms(21) = New SqlClient.SqlParameter("@STS_FPRMPHONE", STS_FPRMPHONE)
                pParms(22) = New SqlClient.SqlParameter("@STS_FOCC", STS_FOCC)
                pParms(23) = New SqlClient.SqlParameter("@STS_FCOMPANY", STS_FCOMPANY)
                'pParms(24) = New SqlClient.SqlParameter("@STS_bFGEMSEMP", STS_bFGEMSEMP)
                'pParms(25) = New SqlClient.SqlParameter("@STS_F_BSU_ID", STS_F_BSU_ID)
                pParms(24) = New SqlClient.SqlParameter("@STS_MFIRSTNAME", STS_MFIRSTNAME)
                pParms(25) = New SqlClient.SqlParameter("@STS_MMIDNAME", STS_MMIDNAME)
                pParms(26) = New SqlClient.SqlParameter("@STS_MLASTNAME", STS_MLASTNAME)
                pParms(27) = New SqlClient.SqlParameter("@STS_MNATIONALITY", STS_MNATIONALITY)
                pParms(28) = New SqlClient.SqlParameter("@STS_MNATIONALITY2", STS_MNATIONALITY2)
                pParms(29) = New SqlClient.SqlParameter("@STS_MCOMADDR1", STS_MCOMADDR1)
                pParms(30) = New SqlClient.SqlParameter("@STS_MCOMADDR2", STS_MCOMADDR2)
                pParms(31) = New SqlClient.SqlParameter("@STS_MCOMPOBOX", STS_MCOMPOBOX)
                pParms(32) = New SqlClient.SqlParameter("@STS_MCOMCITY", STS_MCOMCITY)
                pParms(33) = New SqlClient.SqlParameter("@STS_MCOMSTATE", STS_MCOMSTATE)
                pParms(34) = New SqlClient.SqlParameter("@STS_MCOMCOUNTRY", STS_MCOMCOUNTRY)
                pParms(35) = New SqlClient.SqlParameter("@STS_MOFFPHONE", STS_MOFFPHONE)
                pParms(36) = New SqlClient.SqlParameter("@STS_MRESPHONE", STS_MRESPHONE)
                pParms(37) = New SqlClient.SqlParameter("@STS_MFAX", STS_MFAX)
                pParms(38) = New SqlClient.SqlParameter("@STS_MMOBILE", STS_MMOBILE)
                pParms(39) = New SqlClient.SqlParameter("@STS_MPRMADDR1", STS_MPRMADDR1)
                pParms(40) = New SqlClient.SqlParameter("@STS_MPRMADDR2", STS_MPRMADDR2)
                pParms(41) = New SqlClient.SqlParameter("@STS_MPRMPOBOX", STS_MPRMPOBOX)
                pParms(42) = New SqlClient.SqlParameter("@STS_MPRMCITY", STS_MPRMCITY)
                pParms(43) = New SqlClient.SqlParameter("@STS_MPRMCOUNTRY", STS_MPRMCOUNTRY)
                pParms(44) = New SqlClient.SqlParameter("@STS_MPRMPHONE", STS_MPRMPHONE)
                pParms(45) = New SqlClient.SqlParameter("@STS_MOCC", STS_MOCC)
                pParms(46) = New SqlClient.SqlParameter("@STS_MCOMPANY", STS_MCOMPANY)
                'pParms(49) = New SqlClient.SqlParameter("@STS_bMGEMSEMP", STS_bMGEMSEMP)
                ' pParms(50) = New SqlClient.SqlParameter("@STS_M_BSU_ID", STS_M_BSU_ID)
                pParms(47) = New SqlClient.SqlParameter("@STS_GFIRSTNAME", STS_GFIRSTNAME)
                pParms(48) = New SqlClient.SqlParameter("@STS_GMIDNAME", STS_GMIDNAME)
                pParms(49) = New SqlClient.SqlParameter("@STS_GLASTNAME", STS_GLASTNAME)
                pParms(50) = New SqlClient.SqlParameter("@STS_GNATIONALITY", STS_GNATIONALITY)
                pParms(51) = New SqlClient.SqlParameter("@STS_GNATIONALITY2", STS_GNATIONALITY2)
                pParms(52) = New SqlClient.SqlParameter("@STS_GCOMADDR1", STS_GCOMADDR1)
                pParms(53) = New SqlClient.SqlParameter("@STS_GCOMADDR2", STS_GCOMADDR2)
                pParms(54) = New SqlClient.SqlParameter("@STS_GCOMPOBOX", STS_GCOMPOBOX)
                pParms(55) = New SqlClient.SqlParameter("@STS_GCOMCITY", STS_GCOMCITY)
                pParms(56) = New SqlClient.SqlParameter("@STS_GCOMSTATE", STS_GCOMSTATE)
                pParms(57) = New SqlClient.SqlParameter("@STS_GCOMCOUNTRY", STS_GCOMCOUNTRY)
                pParms(58) = New SqlClient.SqlParameter("@STS_GOFFPHONE", STS_GOFFPHONE)
                pParms(59) = New SqlClient.SqlParameter("@STS_GRESPHONE", STS_GRESPHONE)
                pParms(60) = New SqlClient.SqlParameter("@STS_GFAX", STS_GFAX)
                pParms(61) = New SqlClient.SqlParameter("@STS_GMOBILE", STS_GMOBILE)
                pParms(62) = New SqlClient.SqlParameter("@STS_GPRMADDR1", STS_GPRMADDR1)
                pParms(63) = New SqlClient.SqlParameter("@STS_GPRMADDR2", STS_GPRMADDR2)

                pParms(64) = New SqlClient.SqlParameter("@STS_GPRMPOBOX", STS_GPRMPOBOX)
                pParms(65) = New SqlClient.SqlParameter("@STS_GPRMCITY", STS_GPRMCITY)
                pParms(66) = New SqlClient.SqlParameter("@STS_GPRMCOUNTRY", STS_GPRMCOUNTRY)
                pParms(67) = New SqlClient.SqlParameter("@STS_GPRMPHONE", STS_GPRMPHONE)
                pParms(68) = New SqlClient.SqlParameter("@STS_GOCC", STS_GOCC)
                pParms(69) = New SqlClient.SqlParameter("@STS_GCOMPANY", STS_GCOMPANY)
                pParms(70) = New SqlClient.SqlParameter("@STS_FCOMPANY_ADDR", STS_FCOMPANY_ADDR)
                pParms(71) = New SqlClient.SqlParameter("@STS_FEMAIL", STS_FEMAIL)
                pParms(72) = New SqlClient.SqlParameter("@STS_MCOMPANY_ADDR", STS_MCOMPANY_ADDR)
                pParms(73) = New SqlClient.SqlParameter("@STS_MEMAIL", STS_MEMAIL)
                pParms(74) = New SqlClient.SqlParameter("@STS_GCOMPANY_ADDR", STS_GCOMPANY_ADDR)
                pParms(75) = New SqlClient.SqlParameter("@STS_GEMAIL", STS_GEMAIL)
                pParms(76) = New SqlClient.SqlParameter("@STS_F_COMP_ID", STS_F_COMP_ID)
                pParms(77) = New SqlClient.SqlParameter("@STS_M_COMP_ID", STS_M_COMP_ID)
                pParms(78) = New SqlClient.SqlParameter("@STS_G_COMP_ID", STS_G_COMP_ID)

                pParms(79) = New SqlClient.SqlParameter("@STS_FCOMSTREET", STS_FCOMSTREET)
                pParms(80) = New SqlClient.SqlParameter("@STS_FCOMAREA", STS_FCOMAREA)
                pParms(81) = New SqlClient.SqlParameter("@STS_FCOMBLDG", STS_FCOMBLDG)
                pParms(82) = New SqlClient.SqlParameter("@STS_FCOMAPARTNO", STS_FCOMAPARTNO)


                pParms(83) = New SqlClient.SqlParameter("@STS_MCOMSTREET", STS_MCOMSTREET)
                pParms(84) = New SqlClient.SqlParameter("@STS_MCOMAREA", STS_MCOMAREA)
                pParms(85) = New SqlClient.SqlParameter("@STS_MCOMBLDG", STS_MCOMBLDG)
                pParms(86) = New SqlClient.SqlParameter("@STS_MCOMAPARTNO", STS_MCOMAPARTNO)

                pParms(87) = New SqlClient.SqlParameter("@STS_GCOMSTREET", STS_GCOMSTREET)
                pParms(88) = New SqlClient.SqlParameter("@STS_GCOMAREA", STS_GCOMAREA)
                pParms(89) = New SqlClient.SqlParameter("@STS_GCOMBLDG", STS_GCOMBLDG)
                pParms(90) = New SqlClient.SqlParameter("@STS_GCOMAPARTNO", STS_GCOMAPARTNO)

                pParms(91) = New SqlClient.SqlParameter("@STS_FEMIR", STS_FEMIR)
                pParms(92) = New SqlClient.SqlParameter("@STS_MEMIR", STS_MEMIR)
                pParms(93) = New SqlClient.SqlParameter("@STS_GEMIR", STS_GEMIR)

                pParms(94) = New SqlClient.SqlParameter("@STS_FEMIRATES_ID", STS_FEMIRATES_ID)
                pParms(95) = New SqlClient.SqlParameter("@STS_MEMIRATES_ID", STS_MEMIRATES_ID)
                pParms(96) = New SqlClient.SqlParameter("@STS_GEMIRATES_ID", STS_GEMIRATES_ID)

                pParms(97) = New SqlClient.SqlParameter("@STS_FEMIRATES_ID_EXPDATE", IIf(STS_FEMIRATES_ID_EXPDATE = "", System.DBNull.Value, STS_FEMIRATES_ID_EXPDATE))
                pParms(98) = New SqlClient.SqlParameter("@STS_MEMIRATES_ID_EXPDATE", IIf(STS_MEMIRATES_ID_EXPDATE = "", System.DBNull.Value, STS_MEMIRATES_ID_EXPDATE))
                pParms(99) = New SqlClient.SqlParameter("@STS_GEMIRATES_ID_EXPDATE", IIf(STS_GEMIRATES_ID_EXPDATE = "", System.DBNull.Value, STS_GEMIRATES_ID_EXPDATE))

                pParms(108) = New SqlClient.SqlParameter("@STS_FCOMAREA_ID", STS_FCOMAREA_ID)
                pParms(109) = New SqlClient.SqlParameter("@STS_FCOMSTATE_ID", STS_FCOMSTATE_ID)
                pParms(110) = New SqlClient.SqlParameter("@STS_MCOMAREA_ID", STS_MCOMAREA_ID)

                pParms(111) = New SqlClient.SqlParameter("@STS_MCOMSTATE_ID", STS_MCOMSTATE_ID)
                pParms(112) = New SqlClient.SqlParameter("@STS_GCOMAREA_ID", STS_GCOMAREA_ID)
                pParms(113) = New SqlClient.SqlParameter("@STS_GCOMSTATE_ID", STS_GCOMSTATE_ID)

                pParms(114) = New SqlClient.SqlParameter("@SESSION_USER", SESSION_USER)

                pParms(100) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                pParms(100).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "UpdateStudent_D_PrimaryContact", pParms)
                Dim ReturnFlag As Integer = pParms(100).Value
                Return ReturnFlag
            Catch ex As Exception
                Return 1000
            End Try
        End Using
    End Function
    Public Shared Function GetBSU_M_form_staff() As SqlDataReader
        'Purpose--Get BSU_M data from BUSINESSUNIT_M excluding Dummy Business Unit
        Dim sqlBusinessUnit As String = "select bsu_id,bsu_name from businessunit_m where bsu_id not in('XXXXXX') order by bsu_name"
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection()
        Dim command As SqlCommand = New SqlCommand(sqlBusinessUnit, connection)
        command.CommandType = CommandType.Text
        Dim readerBusinessUnit As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return readerBusinessUnit
    End Function
    Public Shared Function GetStudent_M_DDetails(ByVal Stud_No As String) As SqlDataReader

        'Purpose--Get student passport/visa details from Student_M 

        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetStudent_Details As String = ""

        sqlGetStudent_Details = " SELECT STU_PASPRTNO, STU_PASPRTISSPLACE, STU_VISANO, STU_VISAISSPLACE, STU_VISAISSAUTH, STU_PASPRTISSDATE, STU_PASPRTEXPDATE, " & _
                                " STU_VISAISSDATE, STU_VISAEXPDATE FROM  STUDENT_M M INNER JOIN STUDENT_D D " & _
                                " ON M.STU_SIBLING_ID=D.STS_STU_ID WHERE M.STU_ID ='" & Stud_No & "'"

        Dim command As SqlCommand = New SqlCommand(sqlGetStudent_Details, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader
    End Function
    ''over
End Class
