Imports Microsoft.ApplicationBlocks.Data
Imports Microsoft.VisualBasic
Imports System
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Text
Imports UtilityObj


Public Class AccessRoleUser


#Region " All Get function"

    Public Shared Function GetRoleID_user(ByVal userName As String) As SqlDataReader
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetRol_ID As String = ""

        sqlGetRol_ID = "SELECT USR_ROL_ID FROM USERS_M where USR_NAME='" & userName & "'"

        Dim command As SqlCommand = New SqlCommand(sqlGetRol_ID, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader
    End Function


    Public Shared Function GetTabRights(ByVal RoleId As String, ByVal BusUnit As String, ByVal MNU_CODE As String) As SqlDataReader

        Dim sqlTabRights As String = "select TAB_CODE,TAR_RIGHT from TABSRIGHTS_S where TAR_ROL_ID='" & RoleId & "' and  TAR_BSU_ID='" & BusUnit & "' and TAR_TAB_MNU_CODE ='" & MNU_CODE & "'"
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection()
        Dim command As SqlCommand = New SqlCommand(sqlTabRights, connection)
        command.CommandType = CommandType.Text
        Dim readerTabRights As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return readerTabRights

    End Function

    Public Shared Function GetBusinessUnitsByCurrency(ByVal Bsu_id As String) As SqlDataReader

        Dim sqlBusinessUnit As String = "select BSU_CURRENCY from businessunit_m where bsu_id='" & Bsu_id & "'"
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection()
        Dim command As SqlCommand = New SqlCommand(sqlBusinessUnit, connection)
        command.CommandType = CommandType.Text
        Dim readerBusinessUnit As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return readerBusinessUnit
    End Function
    Public Shared Function GetempStatus() As SqlDataReader
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetempStatus As String = ""

        sqlGetempStatus = "SELECT EST_ID ,EST_DESCR  FROM EMPSTATUS_M order by EST_ID"

        Dim command As SqlCommand = New SqlCommand(sqlGetempStatus, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader
    End Function
    Public Shared Function GetCurrency() As SqlDataReader
        Dim connection As SqlConnection = ConnectionManger.GetOASISFinConnection
        Dim sqlGetCurrency As String = ""

        sqlGetCurrency = "SELECT CUR_ID,CUR_DESCR FROM CURRENCY_M order by CUR_ID"

        Dim command As SqlCommand = New SqlCommand(sqlGetCurrency, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader
    End Function

    Public Shared Function GetCity() As SqlDataReader
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetCity As String = ""

        sqlGetCity = "Select CIT_ID,CIT_DESCR from CITY_M order by CIT_DESCR"

        Dim command As SqlCommand = New SqlCommand(sqlGetCity, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader
    End Function
    Public Shared Function GetCountry() As SqlDataReader
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetCountry As String = ""

        sqlGetCountry = "Select CTY_ID,case CTY_ID when '5' then 'Not Available' else CTY_DESCR end CTY_DESCR  from Country_m order by CTY_DESCR"

        Dim command As SqlCommand = New SqlCommand(sqlGetCountry, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader
    End Function
    Public Shared Function GetCountry_City(ByVal Cid As String, ByVal Etype As String) As SqlDataReader
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetCountryCityType As String = ""
        If Etype = "C" Then
            sqlGetCountryCityType = "SELECT     CITY_M.CIT_ID as CIT_ID, CITY_M.CIT_DESCR as CIT_DESCR, COUNTRY_M.CTY_DESCR as CTY_DESCR, CITY_M.CIT_CTY_ID  as CIT_CTY_ID " & _
                                    " FROM CITY_M INNER JOIN COUNTRY_M ON CITY_M.CIT_CTY_ID = COUNTRY_M.CTY_ID and CIT_ID='" & Cid & "'"

        ElseIf Etype = "G" Then
            sqlGetCountryCityType = "Select CTY_ID,CTY_DESCR from Country_m where CTY_ID='" & Cid & "'"
        End If
        Dim command As SqlCommand = New SqlCommand(sqlGetCountryCityType, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader
    End Function

    Public Shared Function getCollectBank(ByVal bsu_id As String) As SqlDataReader
        Dim str_sql As String = "SELECT DISTINCT  bsu.BSU_COLLECTBANK_ACT_ID as CollectBank, ACCOUNTS_M.ACT_NAME As Collect_name, bsu.BSU_ID FROM vw_OSO_BUSINESSUNIT_M " & _
        " AS bsu INNER JOIN ACCOUNTS_M ON bsu.BSU_COLLECTBANK_ACT_ID = ACCOUNTS_M.ACT_ID " & _
        " where bsu.BSU_ID='" & bsu_id & "'"


        Dim connection As SqlConnection = ConnectionManger.GetOASISFinConnection()
        Dim command As SqlCommand = New SqlCommand(str_sql, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader
    End Function

    Public Shared Function GetAud_ID_Voucher(ByVal Aud_ID As String, ByVal DocNo As String, ByVal Sel_Flag As String) As SqlDataReader

        Dim sqlAud_ID As String = String.Empty
        If Sel_Flag = "B" Then
            sqlAud_ID = "SELECT  top 1 AUD_ID AS ID,AUD_DOCTYPE as DOCTYPE ,AUD_DOCNO AS DocNo, AUD_LOGDT AS LogDT, AUD_DT AS AudDT, " & _
            " AUD_USER AS AudUser, AUD_REMARKS AS Remark " & _
             " FROM AUDITDATA WHERE AUD_DOCNO= '" & DocNo & "' AND  AUD_ID<'" & Aud_ID & "' ORDER BY AUDITDATA.AUD_ID DESC"

        Else
            sqlAud_ID = "SELECT  top 1 AUD_ID AS ID,AUD_DOCTYPE as DOCTYPE ,AUD_DOCNO AS DocNo, AUD_LOGDT AS LogDT, AUD_DT AS AudDT, " & _
                        " AUD_USER AS AudUser, AUD_REMARKS AS Remark " & _
                         " FROM AUDITDATA WHERE AUD_DOCNO= '" & DocNo & "' AND AUDITDATA.AUD_ID>'" & Aud_ID & "'ORDER BY AUDITDATA.AUD_ID DESC"

        End If

        Dim connection As SqlConnection = ConnectionManger.GetOASISAuditConnection
        Dim command As SqlCommand = New SqlCommand(sqlAud_ID, connection)
        command.CommandType = CommandType.Text
        Dim readerAud_ID As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return readerAud_ID
    End Function
    Public Shared Function GetAud_ID_Voucher1(ByVal Aud_ID As String, ByVal DocNo As String, ByVal Sel_Flag As String) As SqlDataReader

        Dim sqlAud_ID As String = String.Empty
        If Sel_Flag = "A" Then
            sqlAud_ID = "SELECT  top 1 AUD_ID AS ID,AUD_DOCTYPE as DOCTYPE ,AUD_DOCNO AS DocNo, AUD_LOGDT AS LogDT, AUD_DT AS AudDT, " & _
            " AUD_USER AS AudUser, AUD_REMARKS AS Remark " & _
             " FROM AUDITDATA WHERE AUD_DOCNO= '" & DocNo & "' AND AUDITDATA.AUD_ID>'" & Aud_ID & "'ORDER BY AUDITDATA.AUD_ID asc"

        End If

        Dim connection As SqlConnection = ConnectionManger.GetOASISAuditConnection
        Dim command As SqlCommand = New SqlCommand(sqlAud_ID, connection)
        command.CommandType = CommandType.Text
        Dim readerAud_ID As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return readerAud_ID
    End Function

    Public Shared Function GetRoles() As SqlDataReader
        Dim sqlRoleDetail As String = "select rol_id,Rol_Descr from roles_m order by Rol_Descr "
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection()
        Dim command As SqlCommand = New SqlCommand(sqlRoleDetail, connection)
        command.CommandType = CommandType.Text
        Dim readerRoleDetail As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return readerRoleDetail
    End Function

    Public Shared Function GetResponsibility() As SqlDataReader
        Dim sqlRoleDetail As String = "SELECT RES_ID,RES_DESCR FROM EMPJOBRESPONSILITY_M order by RES_DESCR"
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection()
        Dim command As SqlCommand = New SqlCommand(sqlRoleDetail, connection)
        command.CommandType = CommandType.Text
        Dim readerRoleDetail As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return readerRoleDetail
    End Function
    Public Shared Function GetEmpPP(ByVal CodeType As String, ByVal IDType As Integer) As SqlDataReader
        Dim connection As SqlConnection = ConnectionManger.GetOASISFinConnection
        Dim sqlGetEmpPPType As String = ""
        If IDType = 1 Then
            sqlGetEmpPPType = "Select ACT_NAME from Accounts_m where ACT_ID='" & CodeType & "'"

        ElseIf IDType = 2 Then
            sqlGetEmpPPType = "Select ACT_NAME from Accounts_m where ACT_ID='" & CodeType & "'"

        End If
        Dim command As SqlCommand = New SqlCommand(sqlGetEmpPPType, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader

    End Function


    Public Shared Function GetRedirectPage(ByVal MainMmnr_bsu_id As String, ByVal MainMmnu_code As String, ByVal MainMmnr_rol_id As String, ByVal userSupper As Boolean) As SqlDataReader
        Dim sqlGetRedirectPage As String
        If userSupper = False Then
            sqlGetRedirectPage = "select mnr_right,mnu_text,mnu_name from menus_m join " & _
                                                "menurights_s on menurights_s.mnr_mnu_id= menus_m.mnu_code " & _
                                                "where mnr_bsu_id='" & MainMmnr_bsu_id & "' and mnu_code='" & MainMmnu_code & "' and mnr_rol_id='" & MainMmnr_rol_id & "'"

        Else
            sqlGetRedirectPage = "select mnu_text,mnu_name from menus_m where mnu_code='" & MainMmnu_code & "'"

        End If
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim command As SqlCommand = New SqlCommand(sqlGetRedirectPage, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader
    End Function
    Public Shared Function GetRecruit_Exp(ByVal ID As String, ByVal Etype As String) As SqlDataReader
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetEmpType As String = ""
        If Etype = "RE" Then
            sqlGetEmpType = "select R_ID,BSU,C_DESCR,AccName,R_Amt,R_Descr,C_ID,A_ID,Bsu_ID from(SELECT     EMPRECRUTMENTEXP_M.RXM_ID AS R_ID, BUSINESSUNIT_M.BSU_NAME AS BSU, EMPCATEGORY_M.ECT_DESCR AS C_DESCR, " & _
                    "  vw_OSF_ACCOUNTS_M.ACT_NAME AS AccName, EMPRECRUTMENTEXP_M.RXM_AMOUNt AS R_Amt, " & _
                     " EMPRECRUTMENTEXP_M.RXM_DESCR AS R_DESCR, EMPRECRUTMENTEXP_M.RXM_CAT_ID as C_ID, EMPRECRUTMENTEXP_M.RXM_BSU_ID as Bsu_ID, " & _
                     " EMPRECRUTMENTEXP_M.RXM_ACT_ID as A_ID FROM EMPRECRUTMENTEXP_M INNER JOIN " & _
                     " BUSINESSUNIT_M ON EMPRECRUTMENTEXP_M.RXM_BSU_ID = BUSINESSUNIT_M.BSU_ID INNER JOIN " & _
                    "  EMPCATEGORY_M ON EMPRECRUTMENTEXP_M.RXM_CAT_ID = EMPCATEGORY_M.ECT_ID INNER JOIN " & _
                    "  vw_OSF_ACCOUNTS_M ON EMPRECRUTMENTEXP_M.RXM_ACT_ID = vw_OSF_ACCOUNTS_M.ACT_ID)a where  a.r_id='" & ID & "'"
        End If
        Dim command As SqlCommand = New SqlCommand(sqlGetEmpType, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader
    End Function


    Public Shared Function GetEMPSAL_ID(ByVal ID As String, ByVal Etype As String) As SqlDataReader
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetEmpType As String = ""
        If Etype = "E" Then
            sqlGetEmpType = "Select id,descr,paid,eob,Acc_ID,includeSal, bLOP from (SELECT EMPSALCOMPO_M.ERN_ID as id, EMPSALCOMPO_M.ERN_DESCR as descr,  vw_OSF_ACCOUNTS_M.ACT_NAME as paid, EMPSALCOMPO_M.ERN_BIncludeInLeaveSal as includeSal, EMPSALCOMPO_M.ERN_bLOP as bLOP, EMPSALCOMPO_M.ERN_Flag as eob,EMPSALCOMPO_M.ERN_TYP as ERN_TYP, EMPSALCOMPO_M.ERN_ACC_ID as Acc_ID FROM  vw_OSF_ACCOUNTS_M INNER JOIN  EMPSALCOMPO_M ON vw_OSF_ACCOUNTS_M.ACT_ID = EMPSALCOMPO_M.ERN_ACC_ID)a where a.ERN_TYP=1 and  a.ID='" & ID & "'"
        ElseIf Etype = "D" Then
            sqlGetEmpType = "Select id,descr,paid,eob,Acc_ID from (SELECT EMPSALCOMPO_M.ERN_ID as id, EMPSALCOMPO_M.ERN_DESCR as descr,  vw_OSF_ACCOUNTS_M.ACT_NAME as paid, EMPSALCOMPO_M.ERN_Flag as eob,EMPSALCOMPO_M.ERN_TYP as ERN_TYP,EMPSALCOMPO_M.ERN_ACC_ID as Acc_ID FROM  vw_OSF_ACCOUNTS_M INNER JOIN  EMPSALCOMPO_M ON vw_OSF_ACCOUNTS_M.ACT_ID = EMPSALCOMPO_M.ERN_ACC_ID)a where a.ERN_TYP=0 and  a.ID='" & ID & "'"

        End If
        Dim command As SqlCommand = New SqlCommand(sqlGetEmpType, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader
    End Function


    Public Shared Function GetEmpComByID(ByVal ID As String, ByVal Etype As String) As SqlDataReader
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetEmpType As String = ""
        If Etype = "C" Then
            sqlGetEmpType = "select * from (Select ECT_ID as ID,ECT_DESCR as Descr from empcategory_m)a where a.ID='" & ID & "'"
        ElseIf Etype = "SD" Then
            sqlGetEmpType = "select * from (Select DES_ID as ID,DES_DESCR as Descr,DES_RES_ID as Res_ID from empdesignation_m)a where a.ID='" & ID & "'"
        ElseIf Etype = "ME" Then
            sqlGetEmpType = "select * from (Select DES_ID as ID,DES_DESCR as Descr,DES_RES_ID as Res_ID from empdesignation_m)a where a.ID='" & ID & "'"
        ElseIf Etype = "R" Then
            sqlGetEmpType = "select * from (Select RLG_ID as ID,RLG_DESCR as Descr from Religion_m)a where a.ID='" & ID & "'"
        ElseIf Etype = "ML" Then
            sqlGetEmpType = "select * from (Select DES_ID as ID,DES_DESCR as Descr,DES_RES_ID as Res_ID from empdesignation_m)a where a.ID='" & ID & "'"
        ElseIf Etype = "G" Then
            sqlGetEmpType = "select * from (Select EGD_ID as ID,EGD_DESCR as Descr from empgrades_m)a where a.ID='" & ID & "'"
        ElseIf Etype = "T" Then
            sqlGetEmpType = "Select * from (SELECT TGD_ID AS ID, TGD_DESCR AS DESCR FROM EMPTEACHINGGRADES_M )a where a.ID='" & ID & "'"
        ElseIf Etype = "J" Then
            'sqlGetEmpType = "Select * from (SELECT Res_ID AS ID, Res_DESCR AS DESCR, RES_ERN_ID as ERN_ID, FROM EMPJOBRESPONSILITY_M )a where a.ID='" & ID & "'"
            sqlGetEmpType = "Select * from (SELECT EMPJOBRESPONSILITY_M.RES_ID as ID, EMPJOBRESPONSILITY_M.RES_DESCR as DESCR,  EMPJOBRESPONSILITY_M.RES_ERN_ID as ERN_ID," & _
            " EMPSALCOMPO_M.ERN_DESCR FROM EMPJOBRESPONSILITY_M LEFT OUTER JOIN EMPSALCOMPO_M ON EMPJOBRESPONSILITY_M.RES_ERN_ID = EMPSALCOMPO_M.ERN_ID)a where a.ID='" & ID & "'"
        ElseIf Etype = "L" Then
            sqlGetEmpType = "Select * from (SELECT ELT_ID AS ID, ELT_DESCR AS DESCR,ELT_BPAID as PAID,ELT_EOB as EOB,ELT_MAX as M_month,ELT_PERIOD as MY_Period FROM EMPLEAVETYPE_M )a where a.ID='" & ID & "'"
        ElseIf Etype = "DM" Then
            sqlGetEmpType = "Select * from (SELECT ESD_ID AS ID,ESD_DESCR AS DESCR,ESD_bCHKEXPIRY as paid,0 as eob FROM EMPSTATDOCUMENTS_M )a where a.ID='" & ID & "'"

        ElseIf Etype = "DP" Then
            sqlGetEmpType = "Select * from (SELECT DPT_ID AS ID, DPT_DESCR AS DESCR,0 as paid,0 as eob FROM DEPARTMENT_M )a where id='" & ID & "'"

        End If

        Dim command As SqlCommand = New SqlCommand(sqlGetEmpType, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader
    End Function
    'updated on 3-feb-2008 --order by
    Public Shared Function GetEmpCom(ByVal Etype As String) As SqlDataReader
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sqlGetEmpType As String = ""
        If Etype = "C" Then
            sqlGetEmpType = "Select ECT_ID,ECT_DESCR from empcategory_m order by ECT_DESCR"
        ElseIf Etype = "D" Then
            sqlGetEmpType = "Select DES_ID,DES_DESCR from empdesignation_m order by DES_DESCR"
        ElseIf Etype = "G" Then
            sqlGetEmpType = "Select EGD_ID,EGD_DESCR from empgrades_m order by EGD_DESCR"
        End If
        Dim command As SqlCommand = New SqlCommand(sqlGetEmpType, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader
    End Function
    Public Shared Function GetEGrade(ByVal var_EGD_ID As Integer) As SqlDataReader
        Dim sqlGetEGrade As String = "select EGD_id,EGD_descr from Emp_Grade where EGD_id='" & var_EGD_ID & "'"
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim command As SqlCommand = New SqlCommand(sqlGetEGrade, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader
    End Function
    Public Shared Function GetEDesignation(ByVal var_Des_id As Integer) As SqlDataReader
        Dim sqlEDesignation As String = "select Des_id,Des_descr from EMPDESIGNATION_M where Des_id='" & var_Des_id & "'"
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim command As SqlCommand = New SqlCommand(sqlEDesignation, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader
    End Function

    Public Shared Function GetECategory(ByVal var_ect_id As Integer) As SqlDataReader
        Dim sqlBECategory As String = "select ect_id,ect_descr from EMPCATEGORY_M where ect_id='" & var_ect_id & "'"
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim command As SqlCommand = New SqlCommand(sqlBECategory, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader
    End Function

    Public Shared Function GetMandCostCenter(ByVal EAct_id As String) As SqlDataReader

        Dim sqlCostCenter As String = "SELECT POLICY_M.PLY_BMANDATORY as BMandatory, ACCOUNTS_M.ACT_PLY_ID as PLY_ID," & _
            "isnull(POLICY_M.PLY_COSTCENTER,'AST') PLY_COSTCENTER " & _
          "FROM ACCOUNTS_M INNER JOIN POLICY_M ON ACCOUNTS_M.ACT_PLY_ID = POLICY_M.PLY_ID AND " & _
        " ACCOUNTS_M.ACT_ID = '" & EAct_id & "'"
        Dim connection As SqlConnection = ConnectionManger.GetOASISFinConnection
        Dim command As SqlCommand = New SqlCommand(sqlCostCenter, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader

    End Function
    '    Public Shared Function GetBunit4Roles(ByVal roleid As String) As SqlDataReader
    '        Dim sqlBunit4Role As String = "Select um.usr_bsu_id as B_unit,bu.bsu_name as bname from users_m as um join " & _
    '" businessunit_m as bu on um.usr_bsu_id=bu.bsu_id where  um.usr_id in(select usr_id from users_m where usr_rol_id='" & roleid & "')" & _
    '       " union SELECT   USERACCESS_S.USA_BSU_ID as B_unit, BUSINESSUNIT_M.BSU_NAME as bname " & _
    '" FROM  BUSINESSUNIT_M INNER JOIN USERACCESS_S ON BUSINESSUNIT_M.BSU_ID = USERACCESS_S.USA_BSU_ID INNER JOIN " & _
    '                     " USERS_M ON USERACCESS_S.USA_USR_ID = USERS_M.USR_ID where  USERACCESS_S.USA_USR_ID in(select usr_id from users_m where usr_rol_id='" & roleid & "')" & _
    '  " union SELECT distinct menuRights_s.mnr_BSU_ID as B_unit, BUSINESSUNIT_M.BSU_NAME as bname " & _
    '" FROM  BUSINESSUNIT_M INNER JOIN menuRights_s ON BUSINESSUNIT_M.BSU_ID = menuRights_s.mnr_BSU_ID INNER JOIN " & _
    '  "  roleS_M ON roleS_M.rol_ID = menuRights_s.mnr_rol_id where roleS_M.rol_ID='" & roleid & "' order by bu.bsu_name"

    '        'code  modified by lijo add order by ---11-jun-2008
    '        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
    '        Dim command As SqlCommand = New SqlCommand(sqlBunit4Role, connection)
    '        command.CommandType = CommandType.Text
    '        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
    '        SqlConnection.ClearPool(connection)
    '        Return reader
    '    End Function
    Public Shared Function GetUserNamepassword(ByVal username As String, ByVal password As String) As SqlDataReader

        Dim sqlUserInfo As String = "SELECT USR_ID, USR_NAME, USR_PASSWORD, USR_ROL_ID, USR_EMP_ID, " _
        & " USR_bSuper, USR_BSU_ID,  isnull(USR_EXPDAYS,0) as USR_EXPDAYS , USR_MIS, USR_bDisable, " _
        & " datediff(day, getdate(),isnull(USR_EXPDATE,getdate())) Days , USR_MODULEACCESS, USR_REQROLE,case when usr_Display_Name is null or usr_Display_name='' then " _
        & "(select upper(emp_fname) from employee_m where emp_id=usr_Emp_ID) else upper(usr_Display_name) end Display_Name  FROM USERS_M " _
        & " where ISNULL(USERS_M.USR_BDISABLE,0) =0 and usr_name='" & username & "' and usr_password= '" & password & "'"

        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection()
        Dim command As SqlCommand = New SqlCommand(sqlUserInfo, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader
    End Function

    Public Shared Function GetUserDetails(ByVal p_USR_NAME As String, ByVal p_USR_PASSWORD As String, _
    ByVal p_AUD_HOST As String, ByVal p_AUD_WINUSER As String, ByRef dtUserData As DataTable, _
     ByRef USR_TRYCOUNT As Integer, ByVal stTrans As SqlTransaction) As Integer
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        'EXEC	@return_value = [dbo].[GetUserDetails]
        '@USR_NAME = N'guru',
        '@USR_PASSWORD = N'wr92h2HbWq4=',
        '@USR_TRYCOUNT = @USR_TRYCOUNT OUTPUT,
        '@AUD_HOST = N'x',
        '@AUD_WINUSER = N'xx'

        Try

            Dim pParms(5) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@USR_NAME", SqlDbType.VarChar, 20)
            pParms(0).Value = p_USR_NAME
            pParms(1) = New SqlClient.SqlParameter("@USR_PASSWORD", SqlDbType.VarChar, 100)
            pParms(1).Value = p_USR_PASSWORD
            pParms(2) = New SqlClient.SqlParameter("@USR_TRYCOUNT", SqlDbType.Int)
            pParms(2).Direction = ParameterDirection.Output

            pParms(3) = New SqlClient.SqlParameter("@AUD_HOST", SqlDbType.VarChar, 100)
            pParms(3).Value = p_AUD_HOST
            pParms(4) = New SqlClient.SqlParameter("@AUD_WINUSER", SqlDbType.VarChar, 100)
            pParms(4).Value = p_AUD_WINUSER
            pParms(5) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
            pParms(5).Direction = ParameterDirection.ReturnValue
            'Dim stTrans As SqlTransaction = objConn.BeginTransaction
            Dim retval As Integer
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(stTrans, CommandType.StoredProcedure, "GetUserDetails", pParms)
            'retval = SqlHelper.ExecuteNonQuery(objConn, CommandType.StoredProcedure, "GetUserDetails", pParms)
            If pParms(5).Value = "0" Then
                dtUserData = ds.Tables(0)
                USR_TRYCOUNT = pParms(2).Value

            End If
            USR_TRYCOUNT = pParms(2).Value
            GetUserDetails = pParms(5).Value

        Catch ex As Exception
            Errorlog(ex.Message)
            GetUserDetails = "1000" 

        End Try




    End Function

    Public Shared Function GetFINANCIALYEAR() As SqlDataReader
        Dim sqlFINANCIALYEAR As String = "select FYR_ID,FYR_Descr, case when (getdate() between FYR_FROMDT and FYR_TODT  )then getdate() else FYR_TODT end as FYR_TODT  from FINANCIALYEAR_S order by bdefault "
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection()
        Dim command As SqlCommand = New SqlCommand(sqlFINANCIALYEAR, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader
    End Function


    Public Shared Function GetFinancialYearDate(ByVal Fin_ID As String) As String
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String = "SELECT  case when (getdate() between FYR_FROMDT and FYR_TODT  )then getdate() else FYR_TODT end as FYR_TODT FROM FINANCIALYEAR_S" _
            & " WHERE    (FYR_ID = " & Fin_ID & ")  "
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        If CDate(ds.Tables(0).Rows(0)(0)) > Now.Date Then
            Return Format(Now.Date, OASISConstants.DateFormat)
        Else
            Return Format(ds.Tables(0).Rows(0)(0), OASISConstants.DateFormat)
        End If
    End Function

    Public Shared Function GetFinancialYearEndDate(ByVal Fin_ID As String) As String
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim str_Sql As String = "SELECT FYR_FROMDT FROM FINANCIALYEAR_S" _
            & " WHERE    (FYR_ID = " & Fin_ID & ")  "
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        If CDate(ds.Tables(0).Rows(0)(0)) > Now.Date Then
            Return Format(Now.Date, OASISConstants.DateFormat)
        Else
            Return Format(ds.Tables(0).Rows(0)(0), OASISConstants.DateFormat)
        End If
    End Function
    
    Public Shared Function GetPrepaymentDetail_Aud_ID(ByVal AUD_ID As String) As SqlDataReader
        Dim sqlPrePaymentDocID As String = "SELECT  PREPAYMENTS_H.Guid as Guid,PREPAYMENTS_H.PRP_ID as DocNo,PREPAYMENTS_H.PRP_SUB_ID as subID," & _
                    " PREPAYMENTS_H.PRP_FYEAR as Fyear,PREPAYMENTS_H.PRP_BSU_ID as BSU_ID, PREPAYMENTS_H.PRP_PARTY as PRP_PARTY, vw_OSA_ACCOUNTS_M.ACT_NAME as PARTY_NAME, PREPAYMENTS_H.PRP_CUT_ID as CostID, vw_OSA_COSTUNIT_M.CUT_DESCR as CostDescr, " & _
                     " PREPAYMENTS_H.PRP_PREPAYACC as PreAcc, ACCOUNTS_M_1.ACT_NAME AS PreAccDescr, PREPAYMENTS_H.PRP_EXPENSEACC as ExpAcc, " & _
                     " ACCOUNTS_M_3.ACT_NAME AS ExpDescr,PREPAYMENTS_H.PRP_DOCDT As DOCDT, PREPAYMENTS_H.PRP_FTFROM as FTFrom, " & _
                    " PREPAYMENTS_H.PRP_DTTO as FTto, PREPAYMENTS_H.PRP_AMOUNT as TAmt, PREPAYMENTS_H.PRP_INVOICENO as InVoice, PREPAYMENTS_H.PRP_JVNO as JvNo, " & _
                    " PREPAYMENTS_H.PRP_CRNARRATION as NR1, PREPAYMENTS_H.PRP_DRNARRATION as NR2, PREPAYMENTS_H.PRP_LPO as LPO " & _
                     " FROM vw_OSA_ACCOUNTS_M right JOIN" & _
                     " PREPAYMENTS_H ON vw_OSA_ACCOUNTS_M.ACT_ID = PREPAYMENTS_H.PRP_PARTY INNER JOIN " & _
                     " vw_OSA_COSTUNIT_M ON PREPAYMENTS_H.PRP_CUT_ID =vw_OSA_COSTUNIT_M.CUT_ID INNER JOIN " & _
                     " vw_OSA_ACCOUNTS_M AS ACCOUNTS_M_1 ON " & _
                     " PREPAYMENTS_H.PRP_PREPAYACC = ACCOUNTS_M_1.ACT_ID INNER JOIN " & _
                     " vw_OSA_ACCOUNTS_M AS ACCOUNTS_M_2 ON PRP_PREPAYACC = ACCOUNTS_M_2.ACT_ID INNER JOIN " & _
                     " vw_OSA_ACCOUNTS_M AS ACCOUNTS_M_3 ON PREPAYMENTS_H.PRP_EXPENSEACC = ACCOUNTS_M_3.ACT_ID " & _
                     " WHERE PREPAYMENTS_H.PRP_AUD_ID = '" & AUD_ID & "'"
        Dim connection As SqlConnection = ConnectionManger.GetOASISAuditConnection
        Dim command As SqlCommand = New SqlCommand(sqlPrePaymentDocID, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader

    End Function
    Public Shared Function GetPrepaymentDetail() As SqlDataReader
        Dim sqlPrePayment As String = "SELECT PREPAYMENTS_H.PRP_ID as DocNo,PREPAYMENTS_H.PRP_FYEAR as FYear,PREPAYMENTS_H.PRP_PARTY as P_Party, ACCOUNTS_M.ACT_NAME AS PARTYNAME,PREPAYMENTS_H.PRP_PREPAYACC as P_PreAcc, ACCOUNTS_M_1.ACT_NAME AS PREPAYACCNAME, PREPAYMENTS_H.PRP_EXPENSEACC,ACCOUNTS_M_2.ACT_NAME AS EXPACCNAME, PREPAYMENTS_H.PRP_DOCDT as DOCDT, PREPAYMENTS_H.PRP_AMOUNT as P_Amt,PREPAYMENTS_H.PRP_bPosted() FROM " & _
        " PREPAYMENTS_H left JOIN ACCOUNTS_M ON PREPAYMENTS_H.PRP_PARTY = ACCOUNTS_M.ACT_ID INNER JOIN ACCOUNTS_M AS ACCOUNTS_M_1 ON PREPAYMENTS_H.PRP_PREPAYACC = ACCOUNTS_M_1.ACT_ID INNER JOIN ACCOUNTS_M AS ACCOUNTS_M_2 ON PREPAYMENTS_H.PRP_EXPENSEACC = ACCOUNTS_M_2.ACT_ID  where isnull(PREPAYMENTS_H.PRP_bDeleted,0)=0"
        Dim connection As SqlConnection = ConnectionManger.GetOASISFinConnection
        Dim command As SqlCommand = New SqlCommand(sqlPrePayment, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader

    End Function
    Public Shared Function GetBUnitImage(ByVal bUnit As String) As SqlDataReader
        Dim sqlBUnitImg As String = "select bsu_logo,BSU_NAME,BSU_ROUNDOFF,BSU_CURRENCY,BSU_PAYMONTH,BSU_PAYYEAR from businessunit_m where bsu_id='" & bUnit & "'"
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection
        Dim command As SqlCommand = New SqlCommand(sqlBUnitImg, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader
    End Function

    Public Shared Function GetTotalBUnit(ByVal usrId As String) As SqlDataReader

        Dim sqlTotalBUnit As String = "select us.usa_bsu_id as B_unit,us.usa_usr_id as U_User," & _
 "bu.bsu_name as bname,bu.BSU_ROUNDOFF as Roundoff, bu.BSU_CURRENCY as BSU_CURRENCY,bu.BSU_PAYMONTH as BSU_PAYMONTH,bu.BSU_PAYYEAR as BSU_PAYYEAR from useraccess_s as us " & _
 "join businessunit_m as bu on us.usa_bsu_id=bu.bsu_id where usa_usr_id='" & usrId & "'  union " & _
"select um.usr_bsu_id as B_unit,um.usr_id as U_User,bu.bsu_name as bname,bu.BSU_ROUNDOFF as Roundoff, bu.BSU_CURRENCY as BSU_CURRENCY,bu.BSU_PAYMONTH as BSU_PAYMONTH,bu.BSU_PAYYEAR as BSU_PAYYEAR from users_m as um join " & _
"businessunit_m as bu on um.usr_bsu_id=bu.bsu_id where usr_id='" & usrId & "' order by bname"

        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection()
        Dim command As SqlCommand = New SqlCommand(sqlTotalBUnit, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        'Dim reader As SqlDataReader = SqlHelper.ExecuteReader(connection, CommandType.Text, sqlTotalBUnit)
        SqlConnection.ClearPool(connection)
        Return reader
    End Function
    Public Shared Function GetBusUnitRole(ByVal busUnit As String) As SqlDataReader
        Dim sqlBusUnitRole As String = "select distinct roles_m.rol_descr as rol_descr,roles_m.rol_id as rol_id from roles_m join users_m on roles_m.rol_id=users_m.usr_rol_id where users_m.usr_bsu_id='" & busUnit & "'"
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection()
        Dim command As SqlCommand = New SqlCommand(sqlBusUnitRole, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader

    End Function

    Public Shared Function GetQuickAccess_menu(ByVal Mnu_module As String, ByVal rol_id As String, ByVal bsu_id As String, ByVal usr_id As String) As SqlDataReader
        Dim sqlQuickAccess As String = "SELECT  MENUS_M.MNU_CODE as code,MENUACCESS_S.MNA_ROL_ID as ROL_ID,MENUACCESS_S.MNA_BSU_ID as BSU_ID," & _
            " MENUS_M.MNU_TEXT as MNU_TEXT,MENUACCESS_S.MNA_USR_ID as usr_id, MENUACCESS_S.MNA_MODULE as M_Module, " & _
             " MENUS_M.MNU_NAME as MNU_Name FROM  MENUACCESS_S INNER JOIN MENUS_M " & _
             " ON MENUACCESS_S.MNA_MNU_ID = MENUS_M.MNU_CODE where MENUACCESS_S.MNA_USR_ID='" & usr_id & "'" & _
             " and MENUACCESS_S.MNA_BSU_ID='" & bsu_id & "' and MENUACCESS_S.MNA_ROL_ID='" & rol_id & "' and MENUACCESS_S.MNA_MODULE = '" & Mnu_module & "'"



        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection()
        Dim command As SqlCommand = New SqlCommand(sqlQuickAccess, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader

    End Function

    Public Shared Function GetTabRightsOnRole(ByVal bsu_id As String, ByVal rol_id As String) As SqlDataReader
        Dim sqlBusUnitRole As String = " SELECT   DISTINCT  TABS_M.TAB_CODE AS TAB_CODE, TABS_M.TAB_TEXT AS TAB_TEXT, TABS_M.TAB_NAME AS TAB_NAME, TABS_M.TAB_MNU_CODE as TAB_PARENTID," & _
                     " TABS_M.TAB_MODULE AS TAB_MODULE, CASE WHEN str(TABSRIGHTS_S.TAR_ROL_ID) IS NULL THEN '" & rol_id & "' else ltrim(str(TABSRIGHTS_S.TAR_ROL_ID)) end TAR_ROL_ID," & _
" case when str(TABSRIGHTS_S.TAR_RIGHT) IS NULL " & _
"  then '0' else ltrim(str(TABSRIGHTS_S.TAR_RIGHT)) end TAB_RIGHT," & _
"  case ltrim(str(TABSRIGHTS_S.TAR_RIGHT)) when '0' then 'Access Denied' " & _
"   when '1' then 'View' " & _
"   when '2' then 'View/Print'" & _
 "  when '3' then 'View/Print/Add'" & _
 "  when '4' then 'View/Print/Add/Edit'" & _
 "  when '5' then 'View/Print/Add/Edit/Delete'" & _
"  when '6' then 'Full Access'" & _
"  else 'Access Denied'" & _
"  end TAB_Descr " & _
"  FROM TABS_M LEFT OUTER JOIN TABSRIGHTS_S ON TABS_M.TAB_CODE = TABSRIGHTS_S.TAB_CODE and TABSRIGHTS_S.TAR_ROL_ID='" & rol_id & "' and TABSRIGHTS_S.TAR_BSU_ID='" & bsu_id & "' " & _
"   order by TAB_RIGHT desc"

        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection()
        Dim command As SqlCommand = New SqlCommand(sqlBusUnitRole, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader

    End Function

    Public Shared Function GetMenuRightsOnRole(ByVal bsu_id As String, ByVal rol_id As String) As SqlDataReader
        Dim sqlBusUnitRole As String = "SELECT distinct MENUS_M.MNU_CODE as MNU_CODE,MENUS_M.MNU_TEXT as MNU_TEXT," & _
 "MENUS_M.MNU_NAME as MNU_NAME, MENUS_M.MNU_PARENTID as MNU_PARENTID," & _
 " MENUS_M.MNU_MODULE as mnu_module,case when str(MENURIGHTS_S.MNR_ROL_ID) IS NULL " & _
" then '" & rol_id & "' else ltrim(str(MENURIGHTS_S.MNR_ROL_ID)) end MNR_ROL_ID," & _
" case when str(MENURIGHTS_S.MNR_RIGHT) IS NULL " & _
 " then '0' else ltrim(str(MENURIGHTS_S.MNR_RIGHT)) end MNR_RIGHT," & _
" case ltrim(str(MENURIGHTS_S.MNR_RIGHT)) when '0' then 'Access Denied' " & _
 "when '1' then 'View'" & _
 "when '2' then 'View/Print'" & _
 "when '3' then 'View/Print/Add'" & _
 "when '4' then 'View/Print/Add/Edit'" & _
 "when '5' then 'View/Print/Add/Edit/Delete'" & _
 "when '6' then 'Full Access'" & _
"else 'Access Denied'" & _
"end MNR_Descr FROM  MENUS_M left JOIN MENURIGHTS_S ON MENUS_M.MNU_CODE = MENURIGHTS_S.MNR_MNU_ID " & _
" and (MENURIGHTS_S.MNR_BSU_ID = '" & bsu_id & "') AND (MENURIGHTS_S.MNR_ROL_ID = '" & rol_id & "' " & _
" and MENUS_M.MNU_CODE = MENURIGHTS_S.MNR_MNU_ID)or MENURIGHTS_S.MNR_MNU_ID is null order by MNR_RIGHT desc"

        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection()
        Dim command As SqlCommand = New SqlCommand(sqlBusUnitRole, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader

    End Function
    Public Shared Function GetTabText(ByVal Tabcode As String) As SqlDataReader
        Dim sqlmenutext As String = "select TAB_TEXT,TAB_MODULE,TAB_NAME from TABS_M where TAB_CODE='" & Tabcode & "'"


        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection()
        Dim command As SqlCommand = New SqlCommand(sqlmenutext, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        SqlConnection.ClearPool(connection)
        Return reader
    End Function


    Public Shared Function GetMenuText(ByVal menucode As String) As SqlDataReader
        Dim sqlmenutext As String = "select mnu_text,mnu_module,mnu_name from menus_m where mnu_code='" & menucode & "'"
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection()

        Dim command As SqlCommand = New SqlCommand(sqlmenutext, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        SqlConnection.ClearPool(connection)
        Return reader
    End Function


    Public Shared Function GetMaxRole() As Integer
        Dim sqlString As String = "select max(ROL_ID) from ROLES_M"
        Dim result As Object
        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()

            Dim command As SqlCommand = New SqlCommand(sqlString, connection)
            command.CommandType = Data.CommandType.Text
            result = command.ExecuteScalar
            SqlConnection.ClearPool(connection)

        End Using
        Return CInt(result)
    End Function
    Public Shared Function GetnewRoleId(ByVal newrole As String) As Integer
        Dim sqlstring As String = "INSERT INTO ROLES_M(ROL_ID, ROL_DESCR)SELECT  (SELECT  ISNULL(MAX(isnull(ROL_ID, 0)) + 1, 1) AS Id FROM ROLES_M) AS id,  '" & newrole & "' "
        Dim rowAffected As Integer
        Try

            Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
                Dim command As SqlCommand = New SqlCommand(sqlstring, connection)
                command.CommandType = Data.CommandType.Text
                rowAffected = command.ExecuteNonQuery
                SqlConnection.ClearPool(connection)
            End Using
            Return rowAffected

        Catch ex As Exception
            Return -1

        End Try

    End Function

    Public Shared Function getUserInfo() As DataSet
        Dim ds As DataSet = New DataSet()
        Using Connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Dim sqlstring As String = "select users_m.usr_id as usr_id,users_m.usr_name as usr_name ,isnull(emp_fname,'')+'  '+isnull(emp_mname,'')+'  '+isnull(emp_lname,'') as Full_Name,users_m.usr_emp_id,users_m.usr_rol_id, roles_m.ROL_DESCR as DESCR  , businessunit_m.BSU_NAME as BSU_NAME , users_m.usr_bsu_id from users_m join businessunit_m  on users_m.usr_bsu_id=businessunit_m.BSU_ID join roles_m on users_m.usr_rol_id=roles_m.ROL_ID left outer join vw_OSO_EMPLOYEE_M on users_m.usr_emp_id=vw_OSO_EMPLOYEE_M.Emp_ID order by  DESCR"
            Dim command As SqlCommand = New SqlCommand(sqlstring, Connection)
            command.CommandType = CommandType.Text
            Dim adapter As SqlDataAdapter = New SqlDataAdapter(command)
            adapter.Fill(ds, "users_m")
            SqlConnection.ClearPool(Connection)
        End Using
        Return ds

    End Function

    Public Shared Function GetUserIDDetail(ByVal usr_id As String) As SqlDataReader
        Dim sqlUserDetail As String = "select ISNULL(USERS_M.USR_BDISABLE,0) USR_Active,  " & _
        "users_m.usr_id as usr_id,users_m.usr_name as usr_name ," & _
        "users_m.usr_password as usr_password,isnull(users_M.USR_DISPLAY_NAME,'') as Display_Name," & _
        "isnull(emp_fname,'')+'  '+isnull(emp_mname,'')+'  '+isnull(emp_lname,'') as Full_Name," & _
        "users_m.usr_emp_id as Emp_id,users_m.usr_rol_id as rol_id, roles_m.ROL_DESCR as DESCR  , " & _
        "businessunit_m.BSU_NAME as BSU_NAME , users_m.usr_bsu_id as usr_bsu_id, " & _
        "FEES.FEECOUNTER_M.FCM_DESCR, USERS_M.USR_FCM_ID from users_m " & _
        "join businessunit_m  on users_m.usr_bsu_id=businessunit_m.BSU_ID join roles_m on " & _
        "users_m.usr_rol_id=roles_m.ROL_ID LEFT OUTER JOIN" & _
        " FEES.FEECOUNTER_M ON USERS_M.USR_FCM_ID = FEES.FEECOUNTER_M.FCM_ID " & _
        "left outer join vw_OSO_EMPLOYEE_M on " & _
        "users_m.usr_emp_id=vw_OSO_EMPLOYEE_M.EMP_ID where usr_id='" & usr_id & "'"
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection()
        Dim command As SqlCommand = New SqlCommand(sqlUserDetail, connection)
        command.CommandType = CommandType.Text
        Dim readerIDDetail As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return readerIDDetail

    End Function

    Public Shared Function GetRoleIDs() As DataSet
        'Dim sqlRoleDetail As String = "select rol_id,Rol_Descr from roles_m order by Rol_Descr "
        'Dim connection As SqlConnection = ConnectionManger.GetOASISConnection()
        'Dim command As SqlCommand = New SqlCommand(sqlRoleDetail, connection)
        'command.CommandType = CommandType.Text
        'Dim readerRoleDetail As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        'Return readerRoleDetail

        Dim str_conn As String = ConnectionManger.GetOASISConnection().ConnectionString
        Dim str_Sql As String = "select rol_id,Rol_Descr from roles_m order by Rol_ID "

        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        Return ds

    End Function
    Public Shared Function GetEmpEarnRules() As SqlDataReader

        Dim sqlBusinessUnit As String = "select EMR_ID,EMR_Description from EMPEARNRULES_M"
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection()
        Dim command As SqlCommand = New SqlCommand(sqlBusinessUnit, connection)
        command.CommandType = CommandType.Text
        Dim readerBusinessUnit As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        Return readerBusinessUnit
    End Function
    Public Shared Function GetBusinessUnits() As SqlDataReader

        Dim sqlBusinessUnit As String = "select bsu_id,bsu_name from businessunit_m order by bsu_name"
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection()
        Dim command As SqlCommand = New SqlCommand(sqlBusinessUnit, connection)
        command.CommandType = CommandType.Text
        Dim readerBusinessUnit As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return readerBusinessUnit
    End Function
    Public Shared Function GetBusinessUnitsByUser(ByVal Bsu_id As String) As SqlDataReader

        Dim sqlBusinessUnit As String = "Select id,e_name from(select bsu_id as id,bsu_name as e_name from businessunit_m)a where a.id<>'" & Bsu_id & "'"
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection()
        Dim command As SqlCommand = New SqlCommand(sqlBusinessUnit, connection)
        command.CommandType = CommandType.Text
        Dim readerBusinessUnit As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return readerBusinessUnit
    End Function
    Public Shared Function GetBusinessUnitsByUser_Filter(ByVal Bsu_id As String, ByVal strFilter As String) As SqlDataReader

        Dim sqlBusinessUnit As String = "Select id,e_name from(select bsu_id as id,bsu_name as e_name from businessunit_m)a where a.id<>'" & Bsu_id & "'" & strFilter
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection()
        Dim command As SqlCommand = New SqlCommand(sqlBusinessUnit, connection)
        command.CommandType = CommandType.Text
        Dim readerBusinessUnit As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return readerBusinessUnit
    End Function
    Public Shared Function GetEmployeeIds() As DataSet
        Dim str_conn As String = ConnectionManger.GetOASISConnection().ConnectionString
        'Dim str_Sql As String = "select rol_id,Rol_Descr from roles_m order by Rol_ID "

        'Dim ds As New DataSet
        'ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        'Return ds


        Dim sqlEmployeeID As String = "Select id,e_name from(SELECT emp_ID as ID,isnull(emp_fname,'')+'  '+isnull(emp_mname,'')+'  '+isnull(emp_lname,'') as E_Name FROM vw_OSO_EMPLOYEE_M)a"
        ' Dim connection As SqlConnection = ConnectionManger.GetOASISConnection()
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, sqlEmployeeID)
        Return ds

    End Function
    Public Shared Function GetBusinessUnits_SEG() As SqlDataReader

        Dim sqlBusinessUnit As String = "select * from (" _
                & " SELECT BSU_ID, '|�� '+ BSU_NAME AS BSU_NAME, BUS_BSG_ID, 1 as UNIT" _
                & " FROM BUSINESSUNIT_M WHERE BUS_BSG_ID <>0" _
                & " union" _
                & " SELECT 'BSG_'+ LTRIM(RTRIM(STR(BSG_ID))), '� � �'+BSG_DESCR+'� � �',BSG_ID,0 as UNIT" _
                & " FROM         BUSSEGMENT_M ) a order by BUS_BSG_ID ,UNIT,BSU_NAME"


        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection()
        Dim command As SqlCommand = New SqlCommand(sqlBusinessUnit, connection)
        command.CommandType = CommandType.Text
        Dim readerBusinessUnit As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        Return readerBusinessUnit
    End Function
    Public Shared Function GetEmployee_BsuUnit(ByVal user_Bsu As String) As SqlDataReader


        Dim sqlUserDetail As String = "Select id,e_name from(SELECT emp_ID as ID,isnull(emp_fname,'')+'  '+isnull(emp_mname,'')+'  '+isnull(emp_lname,'') as E_Name FROM vw_OSO_EMPLOYEE_M)a " & _
  " where id in (select distinct eal_emp_id from empallocation where eal_bsu_id='" & user_Bsu & "' and isnull(eal_todt,getdate())=getdate() and eal_fromdt<=getdate())"

        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection()
        Dim command As SqlCommand = New SqlCommand(sqlUserDetail, connection)
        command.CommandType = CommandType.Text
        Dim readerBusinessUnit As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return readerBusinessUnit

    End Function

    Public Shared Function GetFixedAsset_BsuUnit(ByVal user_Bsu As String) As SqlDataReader


        Dim sqlUserDetail As String = "Select id,e_name from(SELECT FAS_CODE as ID,FAS_DESCRIPTION as E_Name FROM FIXEDASSET_M)a " & _
  " where id in (select distinct FAL_FAS_CODE from FIXEDASSETALLOC_S where FAL_bsu_id='" & user_Bsu & "' and isnull(FAL_DTTO,getdate())=getdate() and FAL_DTFROM<=getdate())"

        Dim connection As SqlConnection = ConnectionManger.GetOASISFinConnection()
        Dim command As SqlCommand = New SqlCommand(sqlUserDetail, connection)
        command.CommandType = CommandType.Text
        Dim readerBusinessUnit As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return readerBusinessUnit

    End Function

    Public Shared Function GetEmployee_BsuUnit_Filter(ByVal user_Bsu As String, ByVal user_date As String, ByVal strFilter As String) As SqlDataReader


        Dim sqlUserDetail As String = "Select id,e_name from(SELECT emp_ID as ID,isnull(emp_fname,'')+'  '+isnull(emp_mname,'')+'  '+isnull(emp_lname,'') as E_Name FROM vw_OSO_EMPLOYEE_M)a " & _
  " where a.id in (select distinct eal_emp_id from empallocation where eal_bsu_id='" & user_Bsu & "' and isnull(eal_todt,getdate())=getdate() and eal_fromdt<='" & user_date & "') " & strFilter

        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection()
        Dim command As SqlCommand = New SqlCommand(sqlUserDetail, connection)
        command.CommandType = CommandType.Text
        Dim readerBusinessUnit As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return readerBusinessUnit


    End Function

    Public Shared Function GetFixedAsset_BsuUnit_Filter(ByVal user_Bsu As String, ByVal user_date As String, ByVal strFilter As String) As SqlDataReader


        Dim sqlUserDetail As String = "Select id,e_name from(SELECT FAS_CODE as ID,FAS_DESCRIPTION as E_Name FROM FIXEDASSET_M)a " & _
  " where id in (select distinct FAL_FAS_CODE from FIXEDASSETALLOC_S where FAL_bsu_id='" & user_Bsu & "' and isnull(FAL_DTTO,getdate())=getdate() and FAL_DTFROM<='" & user_date & "') " & strFilter

        Dim connection As SqlConnection = ConnectionManger.GetOASISFinConnection()
        Dim command As SqlCommand = New SqlCommand(sqlUserDetail, connection)
        command.CommandType = CommandType.Text
        Dim readerBusinessUnit As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return readerBusinessUnit


    End Function

    Public Shared Function GetPly_CostCenter(ByVal ExpCode As String) As SqlDataReader
        Dim sqlPly_Detail As String = "SELECT   POLICY_M.PLY_COSTCENTER as P_Cost " & _
                                        "FROM  ACCOUNTS_M INNER JOIN POLICY_M ON ACCOUNTS_M.ACT_PLY_ID = POLICY_M.PLY_ID and ACCOUNTS_M.ACT_ID='" & ExpCode & "'"
        Dim connection As SqlConnection = ConnectionManger.GetOASISFinConnection()
        Dim command As SqlCommand = New SqlCommand(sqlPly_Detail, connection)
        command.CommandType = CommandType.Text
        Dim readerBusinessUnit As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return readerBusinessUnit
    End Function
    Public Shared Function GetDocName_Audit() As SqlDataReader
        Dim sqlDocName As String = " SELECT Distinct  AUDITDATA.AUD_DOCTYPE as Dtype, vw_OSA_DOCUMENT_M.DOC_NAME as DName " & _
"FROM   AUDITDATA INNER JOIN  vw_OSA_DOCUMENT_M ON AUDITDATA.AUD_DOCTYPE = vw_OSA_DOCUMENT_M.DOC_ID"

        Dim connection As SqlConnection = ConnectionManger.GetOASISAuditConnection()
        Dim command As SqlCommand = New SqlCommand(sqlDocName, connection)
        command.CommandType = CommandType.Text
        Dim readerBusinessUnit As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return readerBusinessUnit
    End Function

    Public Shared Function GetBSUInformation(ByVal BSU_ID As String) As SqlDataReader

        Dim sqlCURRICULUM As String = "SELECT BSU.BSU_LOGO, BSU.BSU_NAME, BSU.BSU_ROUNDOFF, BSU.BSU_CURRENCY, BSU.BSU_PAYMONTH, " _
            & " BSU.BSU_PAYYEAR, BSU.BSU_CLM_ID, BSU.BSU_COLLECTBANK_ACT_ID AS COLLECTBANK, " _
            & " ACT.ACT_NAME AS COLLECT_NAME FROM BUSINESSUNIT_M AS BSU LEFT OUTER JOIN" _
            & " VW_OSF_ACCOUNTS_M AS ACT ON BSU.BSU_COLLECTBANK_ACT_ID = ACT.ACT_ID" _
            & " WHERE     (BSU.BSU_ID = '" & BSU_ID & "') "
        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection()
        Dim command As SqlCommand = New SqlCommand(sqlCURRICULUM, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader
    End Function


    Public Shared Function GetBunit4Roles(ByVal roleid As String) As SqlDataReader
        Dim sqlBunit4Role As String = " Select distinct um.usr_bsu_id as B_unit,bu.bsu_name as bname " & _
" from users_m as um join  businessunit_m as bu on um.usr_bsu_id=bu.bsu_id " & _
" where  um.usr_rol_id ='" & roleid & "'   union SELECT  distinct USERACCESS_S.USA_BSU_ID as B_unit, BUSINESSUNIT_M.BSU_NAME as bname " & _
 "  FROM  BUSINESSUNIT_M INNER JOIN USERACCESS_S ON BUSINESSUNIT_M.BSU_ID = USERACCESS_S.USA_BSU_ID " & _
"  where USERACCESS_S.USA_USR_ID in (select distinct USR_ID from users_m where usr_rol_id='" & roleid & "') " & _
" union SELECT distinct menuRights_s.mnr_BSU_ID as B_unit, BUSINESSUNIT_M.BSU_NAME as bname  " & _
" FROM  BUSINESSUNIT_M INNER JOIN menuRights_s ON BUSINESSUNIT_M.BSU_ID = menuRights_s.mnr_BSU_ID INNER JOIN  " & _
" roleS_M ON roleS_M.rol_ID = menuRights_s.mnr_rol_id where roleS_M.rol_ID='" & roleid & "' order by bu.bsu_name "



        Dim connection As SqlConnection = ConnectionManger.GetOASISConnection()
        Dim command As SqlCommand = New SqlCommand(sqlBunit4Role, connection)
        command.CommandType = CommandType.Text
        Dim reader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
        SqlConnection.ClearPool(connection)
        Return reader

    End Function

#End Region


#Region " All Delete function"

    Public Shared Function DeleteChartAcc(ByVal Eid As String, ByVal trans As SqlTransaction) As Integer
        Using connection As SqlConnection = ConnectionManger.GetOASISFinConnection()
            Dim pParms(1) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@ACT_ID", Eid)
            pParms(1) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(1).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "DeleteACCOUNTS_M", pParms)
            Dim ReturnFlag As Integer = pParms(1).Value
            Return ReturnFlag

        End Using
    End Function
    Public Shared Function DeletePymnt(ByVal Eid As String, ByVal trans As SqlTransaction) As Integer
        Using connection As SqlConnection = ConnectionManger.GetOASISFinConnection()
            Dim pParms(1) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@PTM_ID", Eid)
            pParms(1) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(1).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "DeletePAYMENTTERM_M", pParms)
            Dim ReturnFlag As Integer = pParms(1).Value
            Return ReturnFlag
            SqlConnection.ClearPool(connection)
        End Using
    End Function
    Public Shared Function DeleteChkBook(ByVal Eid As String, ByVal trans As SqlTransaction) As Integer
        Using connection As SqlConnection = ConnectionManger.GetOASISFinConnection()
            Dim pParms(1) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@CHB_ID", Eid)
            pParms(1) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(1).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "DELETECHQBOOK_M", pParms)
            Dim ReturnFlag As Integer = pParms(1).Value
            Return ReturnFlag
        End Using
    End Function

 

    Public Shared Function DeleteAccess(ByVal usr_id As String, ByVal trans As SqlTransaction) As Integer
        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Dim pParms(1) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@usr_ID", usr_id)

            pParms(1) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(1).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "DeleteUserAccess", pParms)
            Dim ReturnFlag As Integer = pParms(1).Value
            Return ReturnFlag
            SqlConnection.ClearPool(connection)
        End Using

    End Function
    Public Shared Function DeleteUser(ByVal usr_ID As String, ByVal trans As SqlTransaction) As Integer
        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Dim pParms(1) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@usr_ID", usr_ID)

            pParms(1) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(1).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "proDeleteUser", pParms)
            Dim ReturnFlag As Integer = pParms(1).Value
            Return ReturnFlag
            SqlConnection.ClearPool(connection)
        End Using

    End Function


    Public Shared Function DeleteCountryCity(ByVal Cid As String, ByVal Etype As String, ByVal trans As SqlTransaction) As Integer

        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Dim pParms(2) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@E_id", Cid)
            pParms(1) = New SqlClient.SqlParameter("@E_type", Etype)
            pParms(2) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(2).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "DeleteCountryCityID", pParms)
            Dim ReturnFlag As Integer = pParms(2).Value
            Return ReturnFlag
            SqlConnection.ClearPool(connection)
        End Using
    End Function
    Public Shared Function DeleteEMPRECRUTMENTEXP_M(ByVal RXM_ID As String, ByVal trans As SqlTransaction) As Integer
        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Dim pParms(1) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@RXM_ID", RXM_ID)
            ' pParms(1) = New SqlClient.SqlParameter("@E_type", Etype)

            pParms(1) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(1).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "DeleteEMPRECRUTMENTEXP_M", pParms)
            Dim ReturnFlag As Integer = pParms(1).Value
            Return ReturnFlag
        End Using
    End Function

    Public Shared Function DeleteEMPSALCOMPO_M(ByVal ID As String, ByVal Etype As String, ByVal trans As SqlTransaction, Optional ByVal C_ID As String = "") As Integer
        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Dim pParms(2) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@E_id", ID)
            pParms(1) = New SqlClient.SqlParameter("@E_type", Etype)
            pParms(2) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(2).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "DeleteEMPSALCOMPO_M", pParms)
            Dim ReturnFlag As Integer = pParms(2).Value
            Return ReturnFlag
            SqlConnection.ClearPool(connection)
        End Using
    End Function


    Public Shared Function DeleteEMPSTATDOCUMENTS_M(ByVal ID As String, ByVal Etype As String, ByVal trans As SqlTransaction, Optional ByVal C_ID As String = "") As Integer
        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Dim pParms(3) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@E_id", ID)
            pParms(1) = New SqlClient.SqlParameter("@E_type", Etype)
            pParms(2) = New SqlClient.SqlParameter("@C_ID", C_ID)
            pParms(3) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(3).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "DeleteEMPSTATDOCUMENTS_M", pParms)
            Dim ReturnFlag As Integer = pParms(3).Value
            Return ReturnFlag
            SqlConnection.ClearPool(connection)
        End Using
    End Function


    Public Shared Function DeleteEmpComByID(ByVal ID As String, ByVal Etype As String, ByVal trans As SqlTransaction, Optional ByVal C_ID As String = "") As Integer
        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Dim pParms(3) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@E_id", ID)
            pParms(1) = New SqlClient.SqlParameter("@E_type", Etype)
            pParms(2) = New SqlClient.SqlParameter("@C_ID", C_ID)
            pParms(3) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(3).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "ProDeleteEmpComByID", pParms)
            Dim ReturnFlag As Integer = pParms(3).Value
            Return ReturnFlag
            SqlConnection.ClearPool(connection)
        End Using
    End Function

#End Region

#Region " All Update function"

    Public Shared Function UpdateCountryCityID(ByVal ID As String, ByVal Descr As String, ByVal Etype As String, ByVal trans As SqlTransaction, ByVal cit_cty_id As String) As Integer
        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Dim pParms(4) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@C_id", ID)
            pParms(1) = New SqlClient.SqlParameter("@C_Descr", Descr)
            pParms(2) = New SqlClient.SqlParameter("@E_type", Etype)
            pParms(3) = New SqlClient.SqlParameter("@C_Cty_ID", cit_cty_id)
            pParms(4) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(4).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "UpdateCountryCityID", pParms)
            Dim ReturnFlag As Integer = pParms(4).Value
            Return ReturnFlag
            SqlConnection.ClearPool(connection)
        End Using

    End Function

    'SaveEMPSTATDOCUMENTS_M(txtID.Text, Descr, ViewState("Etype"), editBit, transaction)

    Public Shared Function SaveEMPSTATDOCUMENTS_M(ByVal ID As String, ByVal Descr As String, ByVal Etype As String, ByVal editBit As Boolean, ByVal trans As SqlTransaction, Optional ByVal Bpaid As Boolean = False, Optional ByVal eob As Boolean = False, Optional ByVal C_ID As String = "") As Integer
        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Dim pParms(7) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@E_id", ID)
            pParms(1) = New SqlClient.SqlParameter("@E_Descr", Descr)
            pParms(2) = New SqlClient.SqlParameter("@E_type", Etype)
            pParms(3) = New SqlClient.SqlParameter("@Bpaid", Bpaid)
            pParms(4) = New SqlClient.SqlParameter("@EoB", eob)
            pParms(5) = New SqlClient.SqlParameter("@editBit", editBit)
            pParms(6) = New SqlClient.SqlParameter("@C_ID", C_ID)
            pParms(7) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(7).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "SaveEMPSTATDOCUMENTS_M", pParms)
            Dim ReturnFlag As Integer = pParms(7).Value
            Return ReturnFlag

        End Using

    End Function
    Public Shared Function SaveEMPRECRUTMENTEXP_M(ByVal RXM_ID As String, ByVal RXM_BSU_ID As String, _
 ByVal RXM_DESCR As String, ByVal RXM_CAT_ID As String, ByVal RXM_AMOUNT As String, ByVal RXM_ACT_ID As String, _
 ByVal editBit As Boolean, ByVal trans As SqlTransaction) As Integer
        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Dim pParms(7) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@RXM_ID", RXM_ID)
            ' pParms(1) = New SqlClient.SqlParameter("@E_type", Etype)
            pParms(1) = New SqlClient.SqlParameter("@RXM_BSU_ID", RXM_BSU_ID)
            pParms(2) = New SqlClient.SqlParameter("@RXM_DESCR", RXM_DESCR)
            pParms(3) = New SqlClient.SqlParameter("@RXM_CAT_ID", RXM_CAT_ID)
            pParms(4) = New SqlClient.SqlParameter("@RXM_AMOUNT", RXM_AMOUNT)
            pParms(5) = New SqlClient.SqlParameter("@RXM_ACT_ID", RXM_ACT_ID)
            pParms(6) = New SqlClient.SqlParameter("@bEdit", editBit)
            pParms(7) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(7).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "SaveEMPRECRUTMENTEXP_M", pParms)
            Dim ReturnFlag As Integer = pParms(7).Value
            Return ReturnFlag
        End Using
    End Function
    Public Shared Function SaveEMPSALCOMPO_M(ByVal ID As String, ByVal Descr As String, _
    ByVal Etype As String, ByVal bEdit As Boolean, ByVal trans As SqlTransaction, _
     ByVal ERN_TYP As Boolean, ByVal ERN_Flag As Integer, ByVal ERN_ACC_ID As String, ByVal bLOP As Boolean, ByVal bIncludeLeaveSal As Boolean) As Integer
        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Dim pParms(8) As SqlClient.SqlParameter



            pParms(0) = New SqlClient.SqlParameter("@ERN_ID", ID)
            pParms(1) = New SqlClient.SqlParameter("@ERN_DESCR", Descr)
            ' pParms(2) = New SqlClient.SqlParameter("@E_type", Etype)
            pParms(2) = New SqlClient.SqlParameter("@ERN_ACC_ID", ERN_ACC_ID)

            pParms(3) = New SqlClient.SqlParameter("@ERN_Flag", ERN_Flag)
            pParms(4) = New SqlClient.SqlParameter("@bEdit", bEdit)

            pParms(5) = New SqlClient.SqlParameter("@ERN_TYP", ERN_TYP)
            pParms(6) = New SqlClient.SqlParameter("@ERN_bLOP", bLOP)
            pParms(7) = New SqlClient.SqlParameter("@ERN_BIncludeInLeaveSal", bIncludeLeaveSal)

            pParms(8) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(8).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "SaveEMPSALCOMPO_M", pParms)
            Dim ReturnFlag As Integer = pParms(8).Value
            Return ReturnFlag
        End Using

    End Function
    Public Shared Function UpdateEmpComByID(ByVal ID As String, ByVal Descr As String, ByVal Etype As String, ByVal trans As SqlTransaction, _
 Optional ByVal Bpaid As Boolean = False, Optional ByVal eob As Boolean = False, Optional ByVal C_ID As String = "", Optional ByVal M_month As String = "0", Optional ByVal MY_period As String = "0") As Integer
        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Dim pParms(8) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@E_id", ID)
            pParms(1) = New SqlClient.SqlParameter("@E_Descr", Descr)
            pParms(2) = New SqlClient.SqlParameter("@E_type", Etype)
            pParms(3) = New SqlClient.SqlParameter("@Bpaid", Bpaid)
            pParms(4) = New SqlClient.SqlParameter("@EoB", eob)
            pParms(5) = New SqlClient.SqlParameter("@C_ID", C_ID)
            pParms(6) = New SqlClient.SqlParameter("@M_month", M_month)
            pParms(7) = New SqlClient.SqlParameter("@MY_period", MY_period)

            pParms(8) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(8).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "ProUpdateEmpComByID", pParms)
            Dim ReturnFlag As Integer = pParms(8).Value
            Return ReturnFlag
            SqlConnection.ClearPool(connection)
        End Using

    End Function
    Public Shared Function UpdateSessionID(ByVal usr_name As String, ByVal USR_SESSION As String) As Integer

        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Dim pParms(2) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@usr_name", usr_name)
            pParms(1) = New SqlClient.SqlParameter("@USR_SESSION", USR_SESSION)
            pParms(2) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(2).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "ProUpdateSessionID", pParms)
            Dim ReturnFlag As Integer = pParms(2).Value
            Return ReturnFlag
            SqlConnection.ClearPool(connection)
        End Using

    End Function

    Public Shared Function UpdateUsers(ByVal usr_id As String, ByVal usr_name As String, _
    ByVal usr_password As String, ByVal usr_DisplayName As String, ByVal usr_rol_id As Integer, ByVal usr_emp_id As String, _
    ByVal vFCM_ID As String, ByVal usr_bsu_id As String, ByVal USR_bDisable As Boolean, ByVal trans As SqlTransaction) As Integer

        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()

            Dim pParms(9) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@usr_id", usr_id)
            pParms(1) = New SqlClient.SqlParameter("@usr_name", usr_name)
            pParms(2) = New SqlClient.SqlParameter("@usr_password", usr_password)
            pParms(3) = New SqlClient.SqlParameter("@usr_rol_id", usr_rol_id)
            pParms(4) = New SqlClient.SqlParameter("@usr_emp_id", usr_emp_id)
            pParms(5) = New SqlClient.SqlParameter("@usr_bsu_id", usr_bsu_id)

            pParms(6) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(6).Direction = ParameterDirection.ReturnValue
            pParms(7) = New SqlClient.SqlParameter("@USR_bDisable", USR_bDisable)
            pParms(8) = New SqlClient.SqlParameter("@USR_DISPLAY_NAME", usr_DisplayName)

            Dim Val As Object = DBNull.Value
            If vFCM_ID <> "" AndAlso vFCM_ID <> "0" Then
                Val = CInt(vFCM_ID)
            End If
            pParms(9) = New SqlClient.SqlParameter("@usr_fcm_id", Val)
            'Insert User
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "proUpdateUsers", pParms)
            Dim ReturnFlag As Integer = pParms(6).Value
            Return ReturnFlag

            SqlConnection.ClearPool(connection)
        End Using
    End Function

    Public Shared Function UpdateUserBusinessUnit(ByVal usa_usr_id As String, ByVal usa_bsu_d As String) As Integer
        Dim rowsAffected As Integer
        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Dim pParms(1) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@usa_usr_id", usa_usr_id)
            pParms(1) = New SqlClient.SqlParameter("@usa_bsu_d", usa_bsu_d)
            Try
                'perform update
                rowsAffected = SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "spUpdateUsersBussiness", pParms)
                SqlConnection.ClearPool(connection)
            Catch ex As Exception

            End Try

        End Using
    End Function
#End Region

#Region " All Insert function"

    Public Shared Function Multi_BSU_CopyRight(ByVal MNR_ROL_ID As Integer, ByVal ADD_BSU_ID As String, ByVal MNR_BSU_ID As String, ByVal trans As SqlTransaction) As Integer
        Try
            Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
                Dim pParms(3) As SqlClient.SqlParameter

                pParms(0) = New SqlClient.SqlParameter("@MNR_ROL_ID", MNR_ROL_ID)
                pParms(1) = New SqlClient.SqlParameter("@ADD_BSU_ID", ADD_BSU_ID)
                pParms(2) = New SqlClient.SqlParameter("@MNR_BSU_ID", MNR_BSU_ID)
                pParms(3) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                pParms(3).Direction = ParameterDirection.ReturnValue
                SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "Save_Multi_BSU_CopyRight", pParms)
                Dim ReturnFlag As Integer = pParms(3).Value
                Return ReturnFlag

            End Using
        Catch ex As Exception
            Return -1
        End Try
    End Function

    Public Shared Function BunitCopyrightsInsert(ByVal MNR_BSU_ID As String, ByVal MNR_ROL_ID As Integer, ByVal NEWMNR_BSU_ID As String, ByVal NEWMNR_ROL_ID As String) As Integer
        Try
            Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
                Dim pParms(4) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@MNR_BSU_ID", MNR_BSU_ID)
                pParms(1) = New SqlClient.SqlParameter("@MNR_ROL_ID", MNR_ROL_ID)
                pParms(2) = New SqlClient.SqlParameter("@NEWMNR_BSU_ID", NEWMNR_BSU_ID)
                pParms(3) = New SqlClient.SqlParameter("@NEWMNR_ROL_ID", NEWMNR_ROL_ID)
                pParms(4) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                pParms(4).Direction = ParameterDirection.ReturnValue
                'Insert User
                SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "dbo.Copyrights", pParms)
                Dim ReturnFlag As Integer = pParms(4).Value
                Return ReturnFlag

            End Using
        Catch ex As Exception
            Return -1
        End Try
    End Function


    Public Shared Function SAVEEMPLOYEE_M(ByVal userName As String, ByVal varEmpNo As String, ByVal intAppNo As String, ByVal salutation As String, ByVal Fname As String, _
 ByVal Mname As String, ByVal Lname As String, ByVal intSD As String, ByVal intCat As String, ByVal intGrade As String, _
 ByVal varBSU As String, ByVal intTgrade As String, ByVal intVtype As String, ByVal intVstatus As String, ByVal dateJoin As String, ByVal dateJoinBSU As String, _
 ByVal dateViss As String, ByVal dateVexp As String, ByVal dateLrejoin As String, ByVal boverSeas As Boolean, ByVal intEmpStatus As String, _
 ByVal dateResig As String, ByVal LastWorkdate As String, ByVal dateProb As String, ByVal intAccom As String, ByVal bitBactive As Boolean, _
 ByVal varCur As String, ByVal varPay As String, ByVal varPayMode As String, ByVal varBank As String, ByVal varAccCode As String, _
ByVal varBankCode As String, ByVal intME As String, ByVal intML As String, ByVal varIU As String, _
ByVal varMarital As String, ByVal curGrossSal As String, ByVal varPassport As String, ByVal varRemark As String, _
ByVal intCountry As String, ByVal bitBmale As Boolean, ByVal intQlf_ID As String, ByVal intSalGrade As String, _
ByVal intDept As String, ByVal varBlood As String, ByVal varFather As String, ByVal varMother As String, _
ByVal intReportTo As String, ByVal VacPolicy As String, ByVal staffNo As String, ByVal cat As String, ByVal allowOT As Boolean, ByVal empid As String, ByVal TempEMP As Boolean, ByVal availCompanyTransportation As Boolean, _
ByVal empDOB As DateTime, ByVal empReligion As String, ByVal ApprovalPolicy As String, ByVal ContractType As Integer, ByVal maxChildConcession As Integer, ByVal LOPOpening As Integer, ByVal FTE As Double, ByVal bPunching As Boolean, _
ByVal airtktFlag As Integer, ByVal airTktClass As String, ByVal airTktCount As Integer, ByVal airTktCityID As String, _
ByVal bEdit As Boolean, ByRef newNo As String, ByRef empIDTEMP As Integer, ByVal connection As SqlConnection, ByVal trans As SqlTransaction) As Integer

        Try
            Dim pParms(70) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@EMPNO", varEmpNo)
            pParms(1) = New SqlClient.SqlParameter("@EMP_APPLNO", intAppNo)
            pParms(2) = New SqlClient.SqlParameter("@EMP_FNAME", Fname)
            pParms(3) = New SqlClient.SqlParameter("@EMP_MNAME", Mname)
            pParms(4) = New SqlClient.SqlParameter("@EMP_LNAME", Lname)
            pParms(5) = New SqlClient.SqlParameter("@EMP_DES_ID", intSD)
            pParms(6) = New SqlClient.SqlParameter("@EMP_ECT_ID", intCat)
            If intGrade = "" Then
                pParms(7) = New SqlClient.SqlParameter("@EMP_EGD_ID", intGrade)
            Else
                pParms(7) = New SqlClient.SqlParameter("@EMP_EGD_ID", DBNull.Value)
            End If
            pParms(8) = New SqlClient.SqlParameter("@EMP_BSU_ID", varBSU)
            pParms(9) = New SqlClient.SqlParameter("@EMP_TGD_ID", intTgrade)
            pParms(10) = New SqlClient.SqlParameter("@EMP_VISATYPE", intVtype)
            pParms(11) = New SqlClient.SqlParameter("@EMP_VISASATUS", intVstatus)
            pParms(12) = New SqlClient.SqlParameter("@EMP_JOINDT", dateJoin)
            pParms(13) = New SqlClient.SqlParameter("@EMP_LASTVACFROM", dateViss)
            pParms(14) = New SqlClient.SqlParameter("@EMP_LASTVACTO", dateVexp)
            pParms(15) = New SqlClient.SqlParameter("@EMP_LASTREJOINDT", dateLrejoin)
            pParms(16) = New SqlClient.SqlParameter("@EMP_STATUS", intEmpStatus)
            pParms(17) = New SqlClient.SqlParameter("@EMP_RESGDT", dateResig)
            pParms(18) = New SqlClient.SqlParameter("@EMP_PROBTILL", dateProb)
            pParms(19) = New SqlClient.SqlParameter("@EMP_ACCOMODATION", intAccom)
            pParms(20) = New SqlClient.SqlParameter("@EMP_bACTIVE", bitBactive)
            pParms(21) = New SqlClient.SqlParameter("@EMP_CUR_ID", varCur)
            pParms(22) = New SqlClient.SqlParameter("@EMP_PAY_CUR_ID", varPay)
            pParms(23) = New SqlClient.SqlParameter("@EMP_MODE", varPayMode)
            pParms(24) = New SqlClient.SqlParameter("@EMP_BANK", varBank)
            pParms(25) = New SqlClient.SqlParameter("@EMP_ACCOUNT", varAccCode)
            pParms(26) = New SqlClient.SqlParameter("@EMP_SWIFTCODE", varBankCode)
            pParms(27) = New SqlClient.SqlParameter("@EMP_MOE_DES_ID", intME)
            pParms(28) = New SqlClient.SqlParameter("@EMP_VISA_DES_ID", intML)
            pParms(29) = New SqlClient.SqlParameter("@EMP_VISA_BSU_ID", varIU)
            pParms(30) = New SqlClient.SqlParameter("@EMP_MARITALSTATUS", varMarital)
            If curGrossSal = "" Then
                curGrossSal = "0"
            End If
            pParms(31) = New SqlClient.SqlParameter("@EMP_GROSSSAL", curGrossSal)
            pParms(32) = New SqlClient.SqlParameter("@EMP_PASSPORT", varPassport)
            pParms(33) = New SqlClient.SqlParameter("@EMP_REMARKS", varRemark)
            pParms(34) = New SqlClient.SqlParameter("@EMP_CTY_ID", intCountry)
            pParms(35) = New SqlClient.SqlParameter("@EMP_SEX_bMALE", bitBmale)
            pParms(36) = New SqlClient.SqlParameter("@EMP_QLF_ID", intQlf_ID)
            If intSalGrade = "" Then
                pParms(37) = New SqlClient.SqlParameter("@EMP_SGD_ID", DBNull.Value)
            Else
                pParms(37) = New SqlClient.SqlParameter("@EMP_SGD_ID", intSalGrade)
            End If
            pParms(38) = New SqlClient.SqlParameter("@EMP_DPT_ID", intDept)
            pParms(39) = New SqlClient.SqlParameter("@EMP_BLOODGRP", varBlood)
            pParms(40) = New SqlClient.SqlParameter("@EMP_FATHER", varFather)
            pParms(41) = New SqlClient.SqlParameter("@EMP_MOTHER", varMother)
            pParms(42) = New SqlClient.SqlParameter("@EMP_REPORTTO_EMP_ID", intReportTo)

            pParms(43) = New SqlClient.SqlParameter("@EMP_STAFFNO", staffNo)
            pParms(44) = New SqlClient.SqlParameter("@EMP_ABC", cat)
            pParms(45) = New SqlClient.SqlParameter("@EMP_ID", empid)

            pParms(46) = New SqlClient.SqlParameter("@EMP_TICKETFLAG", airtktFlag)
            pParms(47) = New SqlClient.SqlParameter("@EMP_TICKETCLASS", airTktClass)
            pParms(48) = New SqlClient.SqlParameter("@EMP_TICKETCOUNT", airTktCount)
            pParms(49) = New SqlClient.SqlParameter("@EMP_TICKET_CIT_ID", airTktCityID)

            pParms(50) = New SqlClient.SqlParameter("@EMp_bTEmp", TempEMP)

            pParms(51) = New SqlClient.SqlParameter("@bEdit", bEdit)

            pParms(52) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(52).Direction = ParameterDirection.ReturnValue
            pParms(53) = New SqlClient.SqlParameter("@Emp_New_No", SqlDbType.VarChar, 15)
            pParms(53).Direction = ParameterDirection.Output
            pParms(54) = New SqlClient.SqlParameter("@NewEMP_ID", SqlDbType.Int)
            pParms(54).Direction = ParameterDirection.Output

            pParms(55) = New SqlClient.SqlParameter("@EMP_SALUTE", SqlDbType.VarChar, 5)
            pParms(55).Value = salutation

            pParms(56) = New SqlClient.SqlParameter("@EMP_DOB", SqlDbType.DateTime)
            pParms(56).Value = empDOB

            pParms(57) = New SqlClient.SqlParameter("@EMP_RLG_ID", SqlDbType.VarChar, 5)
            pParms(57).Value = empReligion

            pParms(58) = New SqlClient.SqlParameter("@EMP_CONTRACTTYPE", SqlDbType.VarChar, 30)
            pParms(58).Value = ContractType

            pParms(59) = New SqlClient.SqlParameter("@EMP_MAXCHILDCONESSION", SqlDbType.TinyInt)
            pParms(59).Value = maxChildConcession

            pParms(60) = New SqlClient.SqlParameter("@EMP_LPS_ID", SqlDbType.Int)
            If ApprovalPolicy = "" Then
                ApprovalPolicy = "0"
            End If
            pParms(60).Value = ApprovalPolicy

            pParms(61) = New SqlClient.SqlParameter("@EMP_bCompanyTransport", SqlDbType.Bit)
            pParms(61).Value = availCompanyTransportation

            pParms(62) = New SqlClient.SqlParameter("@EMP_bOVERSEAS", SqlDbType.Bit)
            pParms(62).Value = boverSeas
            pParms(63) = New SqlClient.SqlParameter("@EMP_BSU_JOINDT", dateJoinBSU)

            pParms(64) = New SqlClient.SqlParameter("@EMP_LASTATTDT", LastWorkdate)
            pParms(65) = New SqlClient.SqlParameter("@EMP_bOT", allowOT)
            If VacPolicy = "" Then
                VacPolicy = "0"
                pParms(66) = New SqlClient.SqlParameter("@EMP_BLS_ID", DBNull.Value)
            Else
                pParms(66) = New SqlClient.SqlParameter("@EMP_BLS_ID", VacPolicy)
            End If
            pParms(67) = New SqlClient.SqlParameter("@EMP_LOP_OPN", LOPOpening)
            pParms(68) = New SqlClient.SqlParameter("@EMP_FTE", FTE)
            pParms(69) = New SqlClient.SqlParameter("@user", userName)
            pParms(70) = New SqlClient.SqlParameter("@EMP_bReqPunching", bPunching)

            Dim cmd As New SqlCommand("SAVEEMPLOYEE_M", connection, trans)
            cmd.Parameters.AddRange(pParms)
            cmd.CommandType = CommandType.StoredProcedure
            ''connection.Open()
            'cmd.ExecuteNonQuery()

            cmd.ExecuteNonQuery()
            Dim EmpNewno As String


            Dim ReturnFlag As Integer = pParms(52).Value
            If ReturnFlag = 0 Then
                EmpNewno = IIf(TypeOf (pParms(53).Value) Is DBNull, String.Empty, pParms(53).Value)
                empIDTEMP = IIf(TypeOf (pParms(54).Value) Is DBNull, String.Empty, pParms(54).Value) 'pParms(50).Value
            End If
            Return ReturnFlag
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message)
            Return 1000
        End Try
    End Function
    Public Shared Function InsertCountryCityID(ByVal ID As String, ByVal Descr As String, ByVal Etype As String, ByVal trans As SqlTransaction, ByVal Cty_ID As String) As Integer

        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Dim pParms(4) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@C_id", ID)
            pParms(1) = New SqlClient.SqlParameter("@C_Descr", Descr)
            pParms(2) = New SqlClient.SqlParameter("@E_type", Etype)
            pParms(3) = New SqlClient.SqlParameter("@Cty_ID", Cty_ID)
            pParms(4) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(4).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "InsertCountryCityID", pParms)
            Dim ReturnFlag As Integer = pParms(4).Value
            Return ReturnFlag
        End Using

    End Function

    Public Shared Function InsertEmpComByID(ByVal ID As String, ByVal Descr As String, ByVal Etype As String, ByVal trans As SqlTransaction, Optional ByVal bpaid As Boolean = False, _
 Optional ByVal eob As Boolean = False, Optional ByVal C_ID As String = "", Optional ByVal M_month As String = "0", Optional ByVal MY_period As String = "0") As Integer
        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Dim pParms(8) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@E_id", ID)
            pParms(1) = New SqlClient.SqlParameter("@E_Descr", Descr)
            pParms(2) = New SqlClient.SqlParameter("@E_type", Etype)
            pParms(3) = New SqlClient.SqlParameter("@Bpaid", bpaid)
            pParms(4) = New SqlClient.SqlParameter("@EoB", eob)
            pParms(5) = New SqlClient.SqlParameter("@C_ID", C_ID)
            pParms(6) = New SqlClient.SqlParameter("@M_month", M_month)
            pParms(7) = New SqlClient.SqlParameter("@MY_period", MY_period)

            pParms(8) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(8).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "ProInsertEmpComByID", pParms)
            Dim ReturnFlag As Integer = pParms(8).Value
            Return ReturnFlag
        End Using

    End Function
    

 

    Public Shared Function InsertMissingRights(ByVal RoleId As String, ByVal BusUnit As String, ByVal modules As String, ByVal trans As SqlTransaction) As Integer

        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()

            Dim pParms(3) As SqlClient.SqlParameter

            pParms(0) = New SqlClient.SqlParameter("@mnr_rol_id", RoleId)
            pParms(1) = New SqlClient.SqlParameter("@mnr_bsu_id", BusUnit)
            pParms(2) = New SqlClient.SqlParameter("@mnu_module", modules)
            pParms(3) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(3).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "UPDATEMISSINGRIGHTS", pParms)
            Dim ReturnFlag As Integer = pParms(3).Value
            Return ReturnFlag

        End Using
    End Function


    ' 'InsertQuickMenu(RoleId, BusUnit, menucode, Usr_ID, modules, transaction)
    Public Shared Function InsertQuickMenu(ByVal RoleId As String, ByVal BusUnit As String, ByVal menucode As String, ByVal Usr_ID As String, ByVal modules As String, ByVal trans As SqlTransaction) As Integer

        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Dim pParms(5) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@MNA_ROL_ID", RoleId)
            pParms(1) = New SqlClient.SqlParameter("@MNA_BSU_ID", BusUnit)
            pParms(2) = New SqlClient.SqlParameter("@MNA_MNU_ID", menucode)
            pParms(3) = New SqlClient.SqlParameter("@MNA_USR_ID", Usr_ID)
            pParms(4) = New SqlClient.SqlParameter("@MNA_MODULE", modules)
            pParms(5) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(5).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "InsertQuickMenu", pParms)
            Dim ReturnFlag As Integer = pParms(5).Value
            Return ReturnFlag
        End Using

    End Function




    Public Shared Function InsertTabRights(ByVal RoleId As String, ByVal BusUnit As String, ByVal TAB_CODE As String, ByVal MNU_CODE As String, ByVal OperationId As Integer, ByVal modules As String, ByVal trans As SqlTransaction) As Integer

        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Dim pParms(6) As SqlClient.SqlParameter

            pParms(0) = New SqlClient.SqlParameter("@TAR_ROL_ID", RoleId)
            pParms(1) = New SqlClient.SqlParameter("@TAR_BSU_ID", BusUnit)
            pParms(2) = New SqlClient.SqlParameter("@TAB_CODE", TAB_CODE)
            pParms(3) = New SqlClient.SqlParameter("@TAR_RIGHT", OperationId)
            pParms(4) = New SqlClient.SqlParameter("@TAR_MODULE", modules)
            pParms(5) = New SqlClient.SqlParameter("@TAR_TAB_MNU_CODE", MNU_CODE)

            pParms(6) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(6).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "SaveTabRights", pParms)
            Dim ReturnFlag As Integer = pParms(6).Value
            Return ReturnFlag
        End Using

    End Function


    Public Shared Function InsertMenuRights(ByVal RoleId As String, ByVal BusUnit As String, ByVal menucode As String, ByVal OperationId As Integer, ByVal modules As String, ByVal trans As SqlTransaction) As Integer

        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Dim pParms(5) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@mnr_rol_id", RoleId)
            pParms(1) = New SqlClient.SqlParameter("@mnr_bsu_id", BusUnit)
            pParms(2) = New SqlClient.SqlParameter("@mnr_mnu_id", menucode)
            pParms(3) = New SqlClient.SqlParameter("@mnr_right", OperationId)
            pParms(4) = New SqlClient.SqlParameter("@mnu_module", modules)
            pParms(5) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(5).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "InsertMenuRights", pParms)
            Dim ReturnFlag As Integer = pParms(5).Value
            Return ReturnFlag
        End Using

    End Function

    Public Shared Function Updatepassword(ByVal password As String, ByVal username As String, ByVal trans As SqlTransaction) As Integer

        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Dim pParms(2) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@USR_PASSWORD", password)
            pParms(1) = New SqlClient.SqlParameter("@USR_NAME", username)
            pParms(2) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(2).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "Updatepassword", pParms)
            Dim ReturnFlag As Integer = pParms(2).Value
            Return ReturnFlag
        End Using

    End Function
    Public Shared Function InsertUserAccess(ByVal USA_USR_ID As String, ByVal USA_BSU_ID As String, ByVal trans As SqlTransaction) As Integer
        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Dim pParms(6) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@USA_USR_ID", USA_USR_ID)
            pParms(1) = New SqlClient.SqlParameter("@USA_BSU_ID", USA_BSU_ID)
            pParms(2) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(2).Direction = ParameterDirection.ReturnValue

            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "InsertUsersAccess", pParms)
            Dim ReturnFlag As Integer = pParms(2).Value
            Return ReturnFlag
        End Using

    End Function
    'Change in user_ID
    Public Shared Function InsertUsers(ByVal usr_id As String, ByVal usr_name As String, ByVal usr_DisplayName As String, _
    ByVal usr_password As String, ByVal usr_rol_id As Integer, ByVal usr_emp_id As String, ByVal vFCM_ID As String, _
    ByVal usr_bsu_id As String, ByVal USR_bDisable As Boolean, ByVal trans As SqlTransaction) As Integer

        Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
            Dim pParms(10) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@usr_id", usr_id)
            pParms(1) = New SqlClient.SqlParameter("@usr_name", usr_name)
            pParms(2) = New SqlClient.SqlParameter("@usr_DisplayName", usr_DisplayName)
            pParms(3) = New SqlClient.SqlParameter("@usr_password", usr_password)
            pParms(4) = New SqlClient.SqlParameter("@usr_rol_id", usr_rol_id)
            pParms(5) = New SqlClient.SqlParameter("@usr_emp_id", usr_emp_id)
            'pParms(5) = New SqlClient.SqlParameter("@usr_bsuper", usr_bsuper)
            pParms(6) = New SqlClient.SqlParameter("@usr_bsu_id", usr_bsu_id)
            pParms(7) = New SqlClient.SqlParameter("@usr_bsuper", 0)
            pParms(8) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
            pParms(8).Direction = ParameterDirection.ReturnValue
            pParms(9) = New SqlClient.SqlParameter("@USR_bDisable", USR_bDisable)
            Dim val As Object = DBNull.Value
            If vFCM_ID <> "" AndAlso vFCM_ID <> "0" Then
                val = CInt(vFCM_ID)
            End If
            pParms(10) = New SqlClient.SqlParameter("@usr_FCM_ID", val)
            'Insert User
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "ProInsertUsers", pParms)
            Dim ReturnFlag As Integer = pParms(8).Value
            Return ReturnFlag

        End Using

    End Function
    Public Shared Function EMPID_Alloc_insert(ByVal EAL_EMP_ID As String, ByVal EAL_BSU_ID As String, ByVal Sess_EAL_BSU_ID As String, ByVal EAL_REMARKS As String, ByVal EAL_FROMDT As String, ByVal trans As SqlTransaction) As Integer
        Try
            Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
                Dim pParms(6) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@EAL_EMP_ID", EAL_EMP_ID)
                pParms(1) = New SqlClient.SqlParameter("@EAL_BSU_ID", EAL_BSU_ID)
                pParms(2) = New SqlClient.SqlParameter("@Sess_EAL_BSU_ID", Sess_EAL_BSU_ID)
                pParms(3) = New SqlClient.SqlParameter("@EAL_REMARKS", EAL_REMARKS)
                pParms(4) = New SqlClient.SqlParameter("@EAL_FROMDT ", EAL_FROMDT)
                pParms(5) = New SqlClient.SqlParameter("@bNew ", 1)

                pParms(6) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                pParms(6).Direction = ParameterDirection.ReturnValue
                'Insert User
                SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "SaveEmpAllocation", pParms)
                Dim ReturnFlag As Integer = pParms(6).Value
                Return ReturnFlag

            End Using
        Catch ex As Exception
            UtilityObj.Errorlog("Error in the EMPID_Alloc_Insert class")
            Return 1000
        End Try
    End Function

    Public Shared Function EMPID_Transfer_Request(ByVal EAL_EMP_ID As String, ByVal To_BSU_ID As String, _
    ByVal From_BSU_ID As String, ByVal EAL_REMARKS As String, ByVal EAL_FROMDT As String, _
    ByVal ETF_GRATUITY As Double, ByVal ETF_LEAVESALARY As Double, ByVal ETF_AIRFARE As Double, _
    ByVal ETF_TO_bForward As Boolean, ByVal ETF_TO_bAccept As Boolean, _
    ByVal bEdit As Boolean, ByVal trans As SqlTransaction) As Integer
        Try
            Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
                Dim pParms(11) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@ETF_EMP_ID", EAL_EMP_ID)
                pParms(1) = New SqlClient.SqlParameter("@ETF_TO_BSU_ID", To_BSU_ID)
                pParms(2) = New SqlClient.SqlParameter("@ETF_FROM_BSU_ID", From_BSU_ID)
                pParms(3) = New SqlClient.SqlParameter("@ETF_REMARKS", EAL_REMARKS)
                pParms(4) = New SqlClient.SqlParameter("@ETF_FROMDT", EAL_FROMDT)
                pParms(5) = New SqlClient.SqlParameter("@ETF_GRATUITY", ETF_GRATUITY)
                pParms(6) = New SqlClient.SqlParameter("@ETF_LEAVESALARY", ETF_LEAVESALARY)
                pParms(7) = New SqlClient.SqlParameter("@ETF_AIRFARE", ETF_AIRFARE)
                pParms(8) = New SqlClient.SqlParameter("@ETF_TO_bForward", ETF_TO_bForward)
                pParms(9) = New SqlClient.SqlParameter("@ETF_TO_bAccept", ETF_TO_bAccept)
                pParms(10) = New SqlClient.SqlParameter("@bEdit", bEdit)

                pParms(11) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                pParms(11).Direction = ParameterDirection.ReturnValue
                'Insert User
                SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "SaveBookEMPTransfer", pParms)
                Dim ReturnFlag As Integer = pParms(11).Value
                Return ReturnFlag

            End Using
        Catch ex As Exception
            UtilityObj.Errorlog("Error in the EMPID_Alloc_Insert class")
            Return 1000
        End Try
    End Function

    Public Shared Function FIX_CODE_Alloc_insert(ByVal FAL_FAS_CODE As String, ByVal FAL_BSU_ID As String, ByVal Sess_FAL_BSU_ID As String, ByVal FAL_REMARKS As String, ByVal FAL_DTFROM As String, ByVal trans As SqlTransaction) As Integer
        Try
            Using connection As SqlConnection = ConnectionManger.GetOASISFinConnection()
                Dim pParms(5) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@FAL_FAS_CODE", FAL_FAS_CODE)
                pParms(1) = New SqlClient.SqlParameter("@FAL_BSU_ID", FAL_BSU_ID)
                pParms(2) = New SqlClient.SqlParameter("@Sess_FAL_BSU_ID", Sess_FAL_BSU_ID)
                pParms(3) = New SqlClient.SqlParameter("@FAL_REMARKS", FAL_REMARKS)
                pParms(4) = New SqlClient.SqlParameter("@FAL_DTFROM", FAL_DTFROM)

                pParms(5) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                pParms(5).Direction = ParameterDirection.ReturnValue
                'Insert User
                SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "SaveFixedAssetAllocation", pParms)
                Dim ReturnFlag As Integer = pParms(5).Value
                Return ReturnFlag

            End Using
        Catch ex As Exception
            UtilityObj.Errorlog("Error in the FIX_CODE_Alloc_Insert class")
            Return 1000
        End Try
    End Function
#End Region
#Region " All Utility function"
    Public Shared Function ClearLocks(ByVal JHD_SUB_ID As String, ByVal Bsuid As String, ByVal JHD_FYEAR As Integer, ByVal DOCTYPE As String, ByVal DOCNO As String, ByVal SessionCode As String, ByVal JHD_USER As String, ByVal JHD_TIMESTAMP As Byte) As Integer
        Try
            Using connection As SqlConnection = ConnectionManger.GetOASISFinConnection()
                Dim pParms(8) As SqlClient.SqlParameter
                pParms(0) = New SqlClient.SqlParameter("@JHD_SUB_ID", JHD_SUB_ID)
                pParms(1) = New SqlClient.SqlParameter("@BSUID", Bsuid)
                pParms(2) = New SqlClient.SqlParameter("@JHD_FYEAR", JHD_FYEAR)
                pParms(3) = New SqlClient.SqlParameter("@DOCTYPE", " ")
                pParms(4) = New SqlClient.SqlParameter("@DOCNO", " ")


                pParms(5) = New SqlClient.SqlParameter("@JHD_USER", JHD_USER)
                pParms(6) = New SqlClient.SqlParameter("@SESSION", " ")
                pParms(7) = New SqlClient.SqlParameter("@JHD_TIMESTAMP", JHD_TIMESTAMP)

                pParms(8) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
                pParms(8).Direction = ParameterDirection.ReturnValue
                'Insert User
                SqlHelper.ExecuteNonQuery(connection, CommandType.StoredProcedure, "ClearAllLocks", pParms)
                Dim ReturnFlag As Integer = pParms(8).Value
                Return ReturnFlag
                SqlConnection.ClearPool(connection)
            End Using
        Catch ex As Exception
            UtilityObj.Errorlog("Error in the Clear Lock class")
            Return 1000
        End Try
    End Function
#End Region

#Region " All Check function"
    Public Shared Function checkduplicatename(ByVal usr_name As String) As String
        Dim str_conn As String = ConnectionManger.GetOASISConnection().ConnectionString
        Dim str_Sql As String = "select usr_id from users_m where  usr_name='" & usr_name & "'"
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        Dim count As Integer
        count = ds.Tables(0).Rows.Count
        If count > 0 Then
            Return 0
        Else
            Return 1
        End If

        'close the connection
    End Function


    Public Shared Function checkduplicateUserID(ByVal usr_id As String) As String
        Dim str_conn As String = ConnectionManger.GetOASISConnection().ConnectionString
        Dim str_Sql As String = "select usr_id from users_m where usr_id='" & usr_id & "'"
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        Dim count As Integer
        count = ds.Tables(0).Rows.Count
        'if count is zero records then no duplicates
        If count > 0 Then
            Return 0
        Else
            Return 1
        End If

        'close the connection
    End Function
    'Changed based on the user_ID
    Public Shared Function checkduplicate(ByVal usr_name As String) As String
        Dim str_conn As String = ConnectionManger.GetOASISConnection().ConnectionString
        Dim str_Sql As String = "select usr_name from users_m where usr_name='" & usr_name & "'"
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        Dim count As Integer
        count = ds.Tables(0).Rows.Count
        If count > 0 Then
            Return 0
        Else
            Return 1
        End If

        'close the connection
    End Function

    Public Shared Function checkduplicateTab_Mod(ByVal RoleId As String, ByVal BusUnit As String) As String
        Dim str_conn As String = ConnectionManger.GetOASISConnection().ConnectionString
        Dim str_Sql As String = "select TAR_ROL_ID from dbo.TABSRIGHTS_S where  TAR_ROL_ID='" & RoleId & "' and TAR_BSU_ID= '" & BusUnit & "'"
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        Dim count As Integer
        count = ds.Tables(0).Rows.Count
        If count > 0 Then
            Return 0
        Else
            Return 1
        End If


    End Function

    Public Shared Function checkduplicateBus_Rol_Mod(ByVal RoleId As String, ByVal BusUnit As String) As String
        Dim str_conn As String = ConnectionManger.GetOASISConnection().ConnectionString
        Dim str_Sql As String = "select mnr_rol_id from dbo.menurights_s where mnr_rol_id='" & RoleId & "' and mnr_bsu_id= '" & BusUnit & "'"
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        Dim count As Integer
        count = ds.Tables(0).Rows.Count
        If count > 0 Then
            Return 0
        Else
            Return 1
        End If
    End Function
    Public Shared Function checkduplicateMenu_Access(ByVal MNA_USR_ID As String, ByVal MNA_MODULE As String, ByVal MNA_BSU_ID As String) As String
        Dim str_conn As String = ConnectionManger.GetOASISConnection().ConnectionString
        Dim str_Sql As String = "SELECT MNA_USR_ID FROM MENUACCESS_S where MNA_USR_ID='" & MNA_USR_ID & "' and MNA_MODULE='" & MNA_MODULE & "' and MNA_BSU_ID='" & MNA_BSU_ID & "'"
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_Sql)
        Dim count As Integer
        count = ds.Tables(0).Rows.Count
        If count > 0 Then
            Return 0
        Else
            Return 1
        End If
    End Function
    'Public Shared Function CheckDupBunitCopyRight(ByVal MNR_BSU_ID As String, ByVal MNR_ROL_ID As Integer) As SqlDataReader

    '    Using connection As SqlConnection = ConnectionManger.GetOASISConnection()
    '        Dim sqlDupBunitCopyRight As String = "select MNR_ROL_ID from MENURIGHTS_S where MNR_ROL_ID='" & MNR_ROL_ID & "' and MNR_BSU_ID='" & MNR_BSU_ID & "'"

    '        Dim command As SqlCommand = New SqlCommand(sqlDupBunitCopyRight, connection)
    '        command.CommandType = CommandType.Text
    '        Dim DupBunitreader As SqlDataReader = command.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
    '        Return DupBunitreader
    '    End Using
    'End Function


#End Region










End Class
