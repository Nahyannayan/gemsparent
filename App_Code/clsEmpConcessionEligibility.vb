﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Imports UtilityObj
Public Class clsEmpConcessionEligibility

#Region "Public Variables"

    Private pEMP_ID As Integer
    Public Property EMP_ID() As Integer
        Get
            Return pEMP_ID
        End Get
        Set(ByVal value As Integer)
            pEMP_ID = value
        End Set
    End Property
    Private pEMP_ECT_ID As Integer
    Public Property EMP_ECT_ID() As Integer
        Get
            Return pEMP_ECT_ID
        End Get
        Set(ByVal value As Integer)
            pEMP_ECT_ID = value
        End Set
    End Property

    Private pOTP As Integer
    Public Property OTP() As Integer
        Get
            Return pOTP
        End Get
        Set(ByVal value As Integer)
            pOTP = value
        End Set
    End Property

    Private pEMP_NO As String
    Public Property EMP_NO() As String
        Get
            Return pEMP_NO
        End Get
        Set(ByVal value As String)
            pEMP_NO = value
        End Set
    End Property

    Private pEMP_NAME As String
    Public Property EMP_NAME() As String
        Get
            Return pEMP_NAME
        End Get
        Set(ByVal value As String)
            pEMP_NAME = value
        End Set
    End Property

    Private pEMP_FNAME As String
    Public Property EMP_FNAME() As String
        Get
            Return pEMP_FNAME
        End Get
        Set(ByVal value As String)
            pEMP_FNAME = value
        End Set
    End Property

    Private pEMP_BSU_ID As String
    Public Property EMP_BSU_ID() As String
        Get
            Return pEMP_BSU_ID
        End Get
        Set(ByVal value As String)
            pEMP_BSU_ID = value
        End Set
    End Property

    Private pSTU_BSU_ID As String
    Public Property STU_BSU_ID() As String
        Get
            Return pSTU_BSU_ID
        End Get
        Set(ByVal value As String)
            pSTU_BSU_ID = value
        End Set
    End Property

    Private pEMP_DOB As String
    Public Property EMP_DOB() As String
        Get
            Return pEMP_DOB
        End Get
        Set(ByVal value As String)
            pEMP_DOB = value
        End Set
    End Property
    Private pEMP_DOJ As String
    Public Property EMP_DOJ() As String
        Get
            Return pEMP_DOJ
        End Get
        Set(ByVal value As String)
            pEMP_DOJ = value
        End Set
    End Property
    Private pEMP_MOBNO As String
    Public Property EMP_MOBNO() As String
        Get
            Return pEMP_MOBNO
        End Get
        Set(ByVal value As String)
            pEMP_MOBNO = value
        End Set
    End Property
    Private pEMP_EMAIL As String
    Public Property EMP_EMAIL() As String
        Get
            Return pEMP_EMAIL
        End Get
        Set(ByVal value As String)
            pEMP_EMAIL = value
        End Set
    End Property

    Private pEMP_NEWJOIN As Boolean
    Public Property EMP_bNEWJOIN() As Boolean
        Get
            Return pEMP_NEWJOIN
        End Get
        Set(ByVal value As Boolean)
            pEMP_NEWJOIN = value
        End Set
    End Property
    Private pSTU_NEWJOIN As Boolean
    Public Property STU_NEWJOIN() As Boolean
        Get
            Return pSTU_NEWJOIN
        End Get
        Set(ByVal value As Boolean)
            pSTU_NEWJOIN = value
        End Set
    End Property
#End Region
    Public Function SearchEmpDetails() As DataTable
        SearchEmpDetails = Nothing
        Dim dsEmp As New DataSet
        Dim qry As String = "SELECT * FROM dbo.EMPLOYEE_M WITH(NOLOCK) WHERE ISNULL(EMP_STATUS,0) IN (1,2)  AND ISNULL(EMP_bACTIVE,0)=1 "
        qry &= IIf(EMP_NO <> "", " AND EMPNO ='" & EMP_NO & "'", "")
        qry &= IIf(EMP_FNAME <> "", " AND EMP_FNAME ='" & EMP_FNAME & "'", "")
        qry &= IIf(EMP_DOB <> "", " AND EMP_DOB ='" & EMP_DOB & "'", "")
        qry &= IIf(EMP_DOJ <> "", " AND (EMP_JOINDT='" & EMP_DOJ & "' OR EMP_BSU_JOINDT='" & EMP_DOJ & "')", "")

        dsEmp = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, qry)
        If Not dsEmp Is Nothing AndAlso dsEmp.Tables(0).Rows.Count > 0 Then
            EMP_ID = dsEmp.Tables(0).Rows(0)("EMP_ID").ToString
            SearchEmpDetails = dsEmp.Tables(0)
        End If
    End Function

    Public Sub GetEmpDetails()

        Dim dsEmp As New DataSet
        Dim qry As String = "SELECT ISNULL(EMP_FNAME,'')+' '+ISNULL(EMP_MNAME,'')+' '+ISNULL(EMP_LNAME,'') AS EMP_NAME,* FROM dbo.EMPLOYEE_M WITH(NOLOCK) WHERE ISNULL(EMP_STATUS,0) IN (1,2)  AND ISNULL(EMP_bACTIVE,0)=1 "
        qry &= IIf(EMP_NO <> "", " AND EMPNO ='" & EMP_NO & "'", "")
        qry &= IIf(EMP_ID <> 0, " AND EMP_ID ='" & EMP_ID.ToString & "'", "")

        dsEmp = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, qry)
        If Not dsEmp Is Nothing AndAlso dsEmp.Tables(0).Rows.Count > 0 Then
            EMP_ID = dsEmp.Tables(0).Rows(0)("EMP_ID").ToString
            EMP_NO = dsEmp.Tables(0).Rows(0)("EMPNO").ToString
            EMP_ECT_ID = dsEmp.Tables(0).Rows(0)("EMP_ECT_ID").ToString
            EMP_BSU_ID = dsEmp.Tables(0).Rows(0)("EMP_BSU_ID").ToString
            EMP_NAME = dsEmp.Tables(0).Rows(0)("EMP_NAME").ToString
            GetEmpContact()
            EMP_EMAIL = IIf(CheckEmail(EMP_EMAIL), EMP_EMAIL, "")
            EMP_bNEWJOIN = bIsNewEmployee()
        Else
            EMP_ID = 0
        End If
    End Sub

    Public Function GetConcessions() As DataTable
        Dim str_conn As String = ConnectionManger.GetOASIS_FEESConnectionString

        Dim str_sql As String = "EXEC SP_GET_PROFILE_EMPLOYEE_CONCESSION_DETAIL '" & EMP_BSU_ID & "'," & EMP_ID & ",'T'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_sql)
        GetConcessions = ds.Tables(0)

    End Function

    Private Sub GetEmpContact()
        Dim dsEmpContact As New DataSet
        Dim qry As String = "SELECT * FROM dbo.vw_OSO_EMPLOYEECONTACT WHERE EMD_EMP_ID='" & EMP_ID & "' "

        dsEmpContact = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, qry)
        If Not dsEmpContact Is Nothing AndAlso dsEmpContact.Tables.Count > 0 Then
            EMP_MOBNO = dsEmpContact.Tables(0).Rows(0)("EMD_CUR_MOBILE").ToString
            EMP_EMAIL = dsEmpContact.Tables(0).Rows(0)("EMD_EMAIL").ToString.Trim
        End If
    End Sub

    Public Function GetEligibility() As DataTable
        GetEligibility = Nothing
        Dim ds As New DataSet
        Dim param(2) As SqlClient.SqlParameter
        param(0) = New SqlClient.SqlParameter("@EMP_ID", EMP_ID)
        param(1) = New SqlClient.SqlParameter("@STU_BSU_ID", STU_BSU_ID)
        param(2) = New SqlClient.SqlParameter("@EMP_BSU_ID", EMP_BSU_ID)
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.StoredProcedure, "OPL.GET_FEE_CONCESSION_ELIGIBLITY", param)
        If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
            GetEligibility = ds.Tables(0)
        End If
    End Function

    Private Function CheckEmail(ByVal EmailAddress As String) As Boolean
        Dim strPattern As String = "^([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$"
        If System.Text.RegularExpressions.Regex.IsMatch(EmailAddress, strPattern) Then
            Return True
        End If
        Return False
    End Function

    Public Function Genereate_Email_EMP_OTP() As String
        Genereate_Email_EMP_OTP = "1"

        Dim pParms(3) As SqlClient.SqlParameter

        pParms(0) = New SqlClient.SqlParameter("@EMP_ID", SqlDbType.Int)
        pParms(0).Value = EMP_ID
        pParms(2) = New SqlClient.SqlParameter("@OTP_OUT", SqlDbType.Int)
        pParms(2).Direction = ParameterDirection.Output

        pParms(1) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(1).Direction = ParameterDirection.ReturnValue
        Dim objConn As New SqlConnection(ConnectionManger.GetOASISConnectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try
            SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "OPL.GENERATE_EMAIL_OTP_EMP", pParms)
            If pParms(1).Value = 0 Then
                OTP = pParms(2).Value
                stTrans.Commit()
            Else
                stTrans.Rollback()
            End If
        Catch ex As Exception
            stTrans.Rollback()
            Errorlog(ex.Message)
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try

        Genereate_Email_EMP_OTP = pParms(1).Value
    End Function
    Public Function IsValidOTP(ByVal EMPID As Integer) As Boolean
        IsValidOTP = False
        Try
            Dim qry As String = "SELECT ISNULL(CASE WHEN ISNULL(EMD_OTP,0)=" & OTP & " THEN 1 ELSE 0 END,0) AS Y FROM dbo.EMPLOYEE_D WHERE EMD_EMP_ID=" & EMPID & " "
            Dim isValid As Int16 = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, qry)
            If isValid = 1 Then
                IsValidOTP = True
                EMP_NO = ""
                EMP_ID = EMPID
                GetEmpDetails()
            End If
        Catch ex As Exception
            IsValidOTP = False
        End Try
    End Function

    Public Sub FillList(ByVal str_conn As String, ByVal SqlQry As String, ByVal TableName As String, ByRef DDL As DropDownList, Optional ByVal DummyEntry As Integer = 1, Optional ByVal Defaultvalue As Integer = 0)
        Dim Ds As New DataSet
        Ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, SqlQry)
        Dim dr As DataRow
        'one dummy entry
        If DummyEntry = 1 Then
            Dim ListItem1 As New ListItem
            ListItem1.Text = "-"
            ListItem1.Value = 0
            DDL.Items.Add(ListItem1)
        End If
        For Each dr In Ds.Tables(0).Rows
            Dim ListItem As New ListItem
            ListItem.Text = dr.Item(1)
            ListItem.Value = dr.Item(0)
            DDL.Items.Add(ListItem)
        Next
        If Defaultvalue <> 0 Then DDL.SelectedValue = Defaultvalue
    End Sub

    Public Function GetChildren(ByVal OLU_ID As Integer) As DataTable
        GetChildren = Nothing
        Try
            Dim pParms(1) As SqlClient.SqlParameter

            pParms(0) = New SqlClient.SqlParameter("@OLU_ID", SqlDbType.Int)
            pParms(0).Value = OLU_ID


            'Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT * FROM dbo.vw_")
            Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.StoredProcedure, "FEES.GET_PARENTLOGIN_STUDENTS", pParms) 'Mainclass.getDataTable("FEES.GET_PARENTLOGIN_STUDENTS", pParms, ConnectionManger.GetOASISConnectionString)
            'If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
            '    GetChildren = dt
            'End If
            If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                GetChildren = ds.Tables(0)
            End If
        Catch ex As Exception

        End Try
    End Function
    Public Function ApplyForConcession(ByVal pSCR_OLU_ID As Integer, ByVal pSCR_REF_EMP_ID As Integer, ByVal pSCD_STU_ID As Int64, _
                                       ByVal pSCD_BSU_ID As String, ByVal pSCD_ACD_ID As Integer, ByVal pSCD_SSV_ID As Integer, _
                                       ByVal pUSER As String) As String
        ApplyForConcession = "1"
        Dim pParms(8) As SqlClient.SqlParameter

        pParms(0) = New SqlClient.SqlParameter("@SCR_OLU_ID", SqlDbType.Int)
        pParms(0).Value = pSCR_OLU_ID
        pParms(1) = New SqlClient.SqlParameter("@SCR_REF_EMP_ID", SqlDbType.Int)
        pParms(1).Value = pSCR_REF_EMP_ID
        pParms(2) = New SqlClient.SqlParameter("@SCD_STU_ID", SqlDbType.BigInt)
        pParms(2).Value = pSCD_STU_ID
        pParms(3) = New SqlClient.SqlParameter("@SCD_BSU_ID", SqlDbType.VarChar)
        pParms(3).Value = pSCD_BSU_ID
        pParms(4) = New SqlClient.SqlParameter("@SCD_ACD_ID", SqlDbType.Int)
        pParms(4).Value = pSCD_ACD_ID
        pParms(5) = New SqlClient.SqlParameter("@SCD_SSV_ID", SqlDbType.Int)
        pParms(5).Value = pSCD_SSV_ID
        pParms(6) = New SqlClient.SqlParameter("@USER", SqlDbType.VarChar)
        pParms(6).Value = pUSER
        pParms(7) = New SqlClient.SqlParameter("@SCR_ID", SqlDbType.Int)
        pParms(7).Direction = ParameterDirection.Output

        pParms(8) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(8).Direction = ParameterDirection.ReturnValue
        Dim objConn As New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try
            SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "FEES.SAVE_STUDENT_CONCESSION_REQUEST", pParms)
            If pParms(8).Value = 0 Then
                stTrans.Commit()
            Else
                stTrans.Rollback()
            End If
            ApplyForConcession = pParms(8).Value
        Catch ex As Exception
            stTrans.Rollback()
            Errorlog(ex.Message)
            ApplyForConcession = "1"
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try

    End Function

    Public Function bServiceExists(ByVal STU_ID As Int64, ByVal ACD_ID As Integer) As Integer
        bServiceExists = 0
        Try
            Dim qry = "SELECT ISNULL(SSV_ID,0)SSV_ID FROM dbo.STUDENT_SERVICES_D WITH(NOLOCK) WHERE SSV_STU_ID=" & STU_ID & " AND SSV_ACD_ID=" & ACD_ID & " AND ISNULL(SSV_bACTIVE,0)=1"
            Dim SSVID As Integer = Convert.ToInt32(SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, qry))
            If SSVID <> 0 Then
                bServiceExists = SSVID

            End If
        Catch ex As Exception

        End Try
    End Function

    Public Function GetConcessionRequestStatus(ByVal STU_ID As Int64, ByVal ACD_ID As Integer, ByVal SSV_ID As Integer) As String
        GetConcessionRequestStatus = ""
        Try
            Dim qry = "SELECT [dbo].fn_GetTransportServiceStatus(" & SSV_ID & "," & STU_ID & "," & ACD_ID & ") AS STATS"
            Dim STATS As String = Convert.ToString(SqlHelper.ExecuteScalar(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, qry))

            GetConcessionRequestStatus = STATS
        Catch ex As Exception
            GetConcessionRequestStatus = ""
        End Try
    End Function

    Public Function bIsNewEmployee() As Boolean
        bIsNewEmployee = False

        Dim pParms(4) As SqlClient.SqlParameter

        pParms(0) = New SqlClient.SqlParameter("@TYPE", SqlDbType.VarChar)
        pParms(0).Value = "E"
        pParms(1) = New SqlClient.SqlParameter("@ID", SqlDbType.BigInt)
        pParms(1).Value = EMP_ID
        pParms(2) = New SqlClient.SqlParameter("@bNewJoin", SqlDbType.Bit)
        pParms(2).Direction = ParameterDirection.Output

        pParms(3) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(3).Direction = ParameterDirection.ReturnValue
        Dim objConn As New SqlConnection(ConnectionManger.GetOASISConnectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try
            SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "dbo.CHECK_IS_EMPLOYEE_STUDENT_NEWJOIN", pParms)
            If pParms(3).Value = 0 Then
                bIsNewEmployee = pParms(2).Value
                stTrans.Commit()
            Else
                stTrans.Rollback()
            End If
        Catch ex As Exception
            stTrans.Rollback()
            Errorlog(ex.Message)
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try


    End Function

    Public Function bIsNewStudent(ByVal STUID As Int64) As Boolean
        bIsNewStudent = False

        Dim pParms(4) As SqlClient.SqlParameter

        pParms(0) = New SqlClient.SqlParameter("@TYPE", SqlDbType.VarChar)
        pParms(0).Value = "S"
        pParms(1) = New SqlClient.SqlParameter("@ID", SqlDbType.BigInt)
        pParms(1).Value = STUID
        pParms(2) = New SqlClient.SqlParameter("@bNewJoin", SqlDbType.Bit)
        pParms(2).Direction = ParameterDirection.Output

        pParms(3) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(3).Direction = ParameterDirection.ReturnValue
        Dim objConn As New SqlConnection(ConnectionManger.GetOASISConnectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try
            SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "dbo.CHECK_IS_EMPLOYEE_STUDENT_NEWJOIN", pParms)
            If pParms(3).Value = 0 Then
                bIsNewStudent = pParms(2).Value
                stTrans.Commit()
            Else
                stTrans.Rollback()
            End If
        Catch ex As Exception
            stTrans.Rollback()
            Errorlog(ex.Message)
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
            STU_NEWJOIN = bIsNewStudent
        End Try


    End Function

    Public Sub SetConcessionDates(ByRef txtF As String, ByRef txtT As String, ByVal EMPID As Long, ByVal SSVID As Integer, ByVal EntryDt As String)
        Try
            Dim cmd As SqlCommand
            cmd = New SqlCommand("[FEES].[SP_GET_PERIOD_FOR_STAFF_CONCESSION_AVAILING]", ConnectionManger.GetOASISTransportConnection)
            cmd.CommandType = CommandType.StoredProcedure

            Dim pParms(5) As SqlClient.SqlParameter

            pParms(0) = New SqlClient.SqlParameter("@ENTRYDT", SqlDbType.DateTime)
            pParms(0).Value = EntryDt
            cmd.Parameters.Add(pParms(0))

            pParms(1) = New SqlClient.SqlParameter("@REF_ID", SqlDbType.Int)
            pParms(1).Value = EMPID
            cmd.Parameters.Add(pParms(1))

            pParms(2) = New SqlClient.SqlParameter("@SSV_ID", SqlDbType.Int)
            pParms(2).Value = SSVID
            cmd.Parameters.Add(pParms(2))

            pParms(3) = New SqlClient.SqlParameter("@DT_FROM", SqlDbType.DateTime)
            pParms(3).Direction = ParameterDirection.Output
            cmd.Parameters.Add(pParms(3))

            pParms(4) = New SqlClient.SqlParameter("@DT_TO", SqlDbType.DateTime)
            pParms(4).Direction = ParameterDirection.Output
            cmd.Parameters.Add(pParms(4))

            cmd.CommandTimeout = 0
            cmd.ExecuteNonQuery()

            txtF = pParms(3).Value
            txtT = pParms(4).Value
        Catch
            txtF = ""
            txtT = ""
        Finally

        End Try
    End Sub
End Class
