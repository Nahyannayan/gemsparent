Imports Microsoft.VisualBasic
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.UI.WebControls
Imports System.Web.UI
Imports System

Public Class AccessRight

    Public Shared Sub setpage(ByVal oControl As Control, ByVal var_mrights As String, ByVal dataMode As String)
        On Error Resume Next



        Dim add_btn As Button = TryCast(oControl.FindControl("btnAdd"), Button)
        Dim edit_btn As Button = TryCast(oControl.FindControl("btnEdit"), Button)
        Dim save_btn As Button = TryCast(oControl.FindControl("btnSave"), Button)
        Dim print_btn As Button = TryCast(oControl.FindControl("btnPrint"), Button)
        Dim cancel_btn As Button = TryCast(oControl.FindControl("btnCancel"), Button)
        Dim delete_btn As Button = TryCast(oControl.FindControl("btndelete"), Button)
        Dim addnew_lb As LinkButton = TryCast(oControl.FindControl("lbAddNew"), LinkButton)


        Select Case Convert.ToInt16(var_mrights)
            '0 for no access 
            Case 0
                add_btn.Visible = False
                edit_btn.Visible = False
                save_btn.Visible = False
                cancel_btn.Visible = False
                delete_btn.Visible = False
                addnew_lb.Visible = False
                print_btn.Visible = False

                ' 1 for only to view records

            Case 1
                add_btn.Visible = False
                edit_btn.Visible = False
                save_btn.Visible = False
                cancel_btn.Visible = True
                delete_btn.Visible = False
                addnew_lb.Visible = False
                print_btn.Visible = False
                '2 above all rights followed by access to Print
            Case 2
                add_btn.Visible = False
                edit_btn.Visible = False
                save_btn.Visible = False
                cancel_btn.Visible = True
                delete_btn.Visible = False
                addnew_lb.Visible = False
                print_btn.Visible = True
                '3 above all rights followed by access to ADD records
            Case 3
                addnew_lb.Visible = True
                If dataMode = "add" Then
                    add_btn.Visible = False
                    edit_btn.Visible = False
                    save_btn.Visible = True
                    cancel_btn.Visible = True
                    delete_btn.Visible = False
                    print_btn.Visible = True
                ElseIf dataMode = "view" Or dataMode = "none" Then
                    'in rights of Add(3) in view datamode
                    add_btn.Visible = True
                    edit_btn.Visible = False
                    save_btn.Visible = False
                    cancel_btn.Visible = True
                    delete_btn.Visible = False
                    print_btn.Visible = True

                End If
                '4 above all rights followed by access to Edit records
            Case 4
                addnew_lb.Visible = True
                If dataMode = "add" Then
                    add_btn.Visible = False
                    edit_btn.Visible = False
                    save_btn.Visible = True
                    cancel_btn.Visible = True
                    delete_btn.Visible = False
                    print_btn.Visible = False
                ElseIf dataMode = "view" Or dataMode = "none" Then

                    add_btn.Visible = True
                    'in rights of Edit(4) in view datamode
                    edit_btn.Visible = True
                    save_btn.Visible = False
                    cancel_btn.Visible = True
                    delete_btn.Visible = False
                    print_btn.Visible = True
                ElseIf dataMode = "edit" Then

                    add_btn.Visible = False
                    'in rights of Edit(4) in view datamode
                    edit_btn.Visible = False
                    save_btn.Visible = True
                    cancel_btn.Visible = True
                    delete_btn.Visible = False
                    print_btn.Visible = True
                End If
                '5 above all rights followed by access to Delete records
            Case 5
                addnew_lb.Visible = True
                If dataMode = "add" Then
                    add_btn.Visible = False
                    edit_btn.Visible = False
                    save_btn.Visible = True
                    cancel_btn.Visible = True
                    delete_btn.Visible = False
                    print_btn.Visible = False
                ElseIf dataMode = "view" Or dataMode = "none" Then
                    'in rights of Delete(5) in view datamode
                    add_btn.Visible = True
                    edit_btn.Visible = True
                    cancel_btn.Visible = True
                    delete_btn.Visible = True
                    print_btn.Visible = True
                    save_btn.Visible = False
                ElseIf dataMode = "edit" Then
                    'in rights of Edit(5) in edit datamode
                    add_btn.Visible = False
                    edit_btn.Visible = False
                    save_btn.Visible = True
                    cancel_btn.Visible = True
                    delete_btn.Visible = False
                    print_btn.Visible = True

                End If
                '6 above all rights followed by full rights records
            Case 6
                addnew_lb.Visible = True
                If dataMode = "add" Then
                    add_btn.Visible = False
                    edit_btn.Visible = False
                    save_btn.Visible = True
                    cancel_btn.Visible = True
                    delete_btn.Visible = False
                    print_btn.Visible = False
                ElseIf dataMode = "view" Or dataMode = "none" Then
                    'in rights of Delete(5) in view datamode
                    add_btn.Visible = True
                    edit_btn.Visible = True
                    cancel_btn.Visible = True
                    delete_btn.Visible = True
                    print_btn.Visible = True
                    save_btn.Visible = False

                ElseIf dataMode = "edit" Then
                    'in rights of Edit(5) in edit datamode
                    add_btn.Visible = False
                    edit_btn.Visible = False
                    save_btn.Visible = True
                    cancel_btn.Visible = True
                    delete_btn.Visible = False
                    print_btn.Visible = True
                End If

        End Select



    End Sub
    Public Shared Function PageRightsID(ByVal USR_NAME As String, ByVal BSU_ID As String, ByVal MNU_ID As String) As Integer
        Using connection As SqlConnection = ConnectionManger.GetOASISFinConnection()
            Dim pParms(3) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@USR_NAME", USR_NAME)
            pParms(1) = New SqlClient.SqlParameter("@BSU_ID", BSU_ID)
            pParms(2) = New SqlClient.SqlParameter("@MNU_ID", MNU_ID)
            Dim ReturnFlag As Integer
            Dim User_super As Boolean = HttpContext.Current.Session("sBusper")
            If User_super = True Then
                ReturnFlag = 6
            Else
                ReturnFlag = SqlHelper.ExecuteScalar(connection, CommandType.StoredProcedure, "GetMenuAccess", pParms)

            End If

            Return ReturnFlag
        End Using

    End Function

End Class
