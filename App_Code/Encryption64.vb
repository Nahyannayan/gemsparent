Imports System
Imports System.IO
Imports System.Xml
Imports System.Text
Imports System.Security.Cryptography
Imports Microsoft.VisualBasic


Public Class Encryption64
    Private key() As Byte = {}
    Private IV() As Byte = {&H12, &H34, &H56, &H78, &H90, &HAB, &HCD, &HEF}

    Public Function Decrypt(ByVal stringToDecrypt As String, _
        Optional ByVal sEncryptionKey As String = "!#$a54?W") As String
        Dim inputByteArray(stringToDecrypt.Length) As Byte
        Try
            key = System.Text.Encoding.UTF8.GetBytes(Left(sEncryptionKey, 8))
            Dim des As New DESCryptoServiceProvider()
            inputByteArray = Convert.FromBase64String(stringToDecrypt)
            Dim ms As New MemoryStream()
            Dim cs As New CryptoStream(ms, des.CreateDecryptor(key, IV), _
                CryptoStreamMode.Write)
            cs.Write(inputByteArray, 0, inputByteArray.Length)
            cs.FlushFinalBlock()
            Dim encoding As System.Text.Encoding = System.Text.Encoding.UTF8
            Return encoding.GetString(ms.ToArray())
        Catch e As Exception
            Return e.Message
        End Try
    End Function

    Public Function Encrypt(ByVal stringToEncrypt As String, _
       Optional ByVal SEncryptionKey As String = "!#$a54?W") As String
        Try
            key = System.Text.Encoding.UTF8.GetBytes(Left(SEncryptionKey, 8))
            Dim des As New DESCryptoServiceProvider()
            Dim inputByteArray() As Byte = Encoding.UTF8.GetBytes( _
                stringToEncrypt)
            Dim ms As New MemoryStream()
            Dim cs As New CryptoStream(ms, des.CreateEncryptor(key, IV), _
                CryptoStreamMode.Write)
            cs.Write(inputByteArray, 0, inputByteArray.Length)
            cs.FlushFinalBlock()
            Return Convert.ToBase64String(ms.ToArray())
        Catch e As Exception
            Return e.Message
        End Try
    End Function

    Public Function EncryptSHA512(ByVal PlainText As String) As String
        Try
            Dim hasher As SHA512 = SHA512.Create()
            Dim HashValue As Byte() = hasher.ComputeHash(Encoding.ASCII.GetBytes(PlainText))
            Dim strHex As String = ""

            For Each b As Byte In HashValue
                strHex += b.ToString("x2")
            Next

            Return strHex
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function


    Public Function EncryptWithHash(ByVal PlainText As String) As String
        Try
            Return EncryptSHA512(PlainText)
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function

    Public Function EncryptWithSaltedHash(ByVal PlainText As String) As String
        Try
            Dim saltBytes = New Byte(15) {}

            Using provider = New RNGCryptoServiceProvider()
                provider.GetNonZeroBytes(saltBytes)
            End Using

            Return Convert.ToBase64String(saltBytes) + "."c + EncryptSHA512(PlainText)
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function

End Class

