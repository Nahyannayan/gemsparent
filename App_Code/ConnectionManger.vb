Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System
Imports System.Web.Configuration


Public Class ConnectionManger
    Public Shared Function getConnection(ByVal _nameString As String) As SqlConnection

        Dim ConnectionStrings As ConnectionStringSettingsCollection = ConfigurationManager.ConnectionStrings
        'NameValueCollection appSettings =ConfigurationManager.AppSettings; 
        Dim _connectionString As String = ConnectionStrings(_nameString).ConnectionString
        Dim _connection As New SqlConnection(_connectionString)
        _connection.Open()
        Return _connection
    End Function

    Public Shared Function GetASPNETDBConnectionString() As String
        Return WebConfigurationManager.ConnectionStrings("ASPNETDB").ConnectionString
    End Function

    Public Shared Function GetASPNETDBConnection() As SqlConnection
        Dim Connection As SqlConnection = New SqlConnection(WebConfigurationManager.ConnectionStrings("ASPNETDB").ConnectionString)
        Connection.Open()
        GetASPNETDBConnection = Connection
    End Function

    Public Shared Function GetOASISConnectionhr1() As SqlConnection
        Dim Connection As SqlConnection = New SqlConnection(WebConfigurationManager.ConnectionStrings("OASIS_HRConnectionString").ConnectionString)
        Connection.Open()
        GetOASISConnectionhr1 = Connection
    End Function

    Public Shared Function GetOASIS_CURRICULUMConnectionString() As String
        Return WebConfigurationManager.ConnectionStrings("OASIS_CURRICULUMConnectionString").ConnectionString
    End Function
    Public Shared Function GetOASIS_GLGConnectionString() As String
        Return WebConfigurationManager.ConnectionStrings("OASIS_GLGConnectionString").ConnectionString
    End Function

    Public Shared Function GetOASIS_CURRICULUMConnection() As SqlConnection
        Dim Connection As SqlConnection = New SqlConnection(WebConfigurationManager.ConnectionStrings("OASIS_CURRICULUMConnectionString").ConnectionString)
        Connection.Open()
        GetOASIS_CURRICULUMConnection = Connection
    End Function
    Public Shared Function GetOASIS_CCAConnectionString() As String
        Return WebConfigurationManager.ConnectionStrings("OASIS_CCAConnectionString").ConnectionString
    End Function
    Public Shared Function GetGLGConnectionString() As String
        Return WebConfigurationManager.ConnectionStrings("GLG_OASISConnectionString").ConnectionString
    End Function

    Public Shared Function GetGLGConnection() As SqlConnection
        Dim Connection As SqlConnection = New SqlConnection(WebConfigurationManager.ConnectionStrings("GLG_OASISConnectionString").ConnectionString)
        Connection.Open()
        GetGLGConnection = Connection
    End Function

    Public Shared Function GetOASISConnectionString() As String
        Return WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
    End Function

    Public Shared Function GetOASISConnection() As SqlConnection
        Dim Connection As SqlConnection = New SqlConnection(WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString)
        Connection.Open()
        GetOASISConnection = Connection
    End Function

    Public Shared Function GetOASISTRANSPORTConnectionString() As String
        Return WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
    End Function

    Public Shared Function GetOASISTransportConnection() As SqlConnection
        Dim Connection As SqlConnection = New SqlConnection(WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString)
        Connection.Open()
        GetOASISTransportConnection = Connection
    End Function

    Public Shared Function GetOASISSurveyConnectionString() As String
        Return WebConfigurationManager.ConnectionStrings("OASIS_SURVEYConnectionString").ConnectionString
    End Function

    Public Shared Function GetOASISSurveyConnection() As SqlConnection
        Dim Connection As SqlConnection = New SqlConnection(WebConfigurationManager.ConnectionStrings("OASIS_SURVEYConnectionString").ConnectionString)
        Connection.Open()
        GetOASISSurveyConnection = Connection
    End Function

    Public Shared Function GetOASISFINConnectionString() As String
        Return WebConfigurationManager.ConnectionStrings("MAINDB").ConnectionString
    End Function

    Public Shared Function GetOASISFinConnection() As SqlConnection
        Dim Connection As SqlConnection = New SqlConnection(WebConfigurationManager.ConnectionStrings("MainDB").ConnectionString)
        Connection.Open()
        GetOASISFinConnection = Connection
    End Function

    Public Shared Function GetOASISAuditConnection() As SqlConnection
        Dim Connection As SqlConnection = New SqlConnection(WebConfigurationManager.ConnectionStrings("OASISAuditConnectionString").ConnectionString)
        Connection.Open()
        GetOASISAuditConnection = Connection
    End Function

    Public Shared Function GetOASISAuditConnectionString() As String
        Return WebConfigurationManager.ConnectionStrings("OASISAuditConnectionString").ConnectionString
    End Function

    Public Shared Function GetOASIS_FEESConnectionString() As String
        Return WebConfigurationManager.ConnectionStrings("OASIS_FEESConnectionString").ConnectionString
    End Function

    Public Shared Function GetOASIS_FEESConnection() As SqlConnection
        Dim Connection As SqlConnection = New SqlConnection(WebConfigurationManager.ConnectionStrings("OASIS_FEESConnectionString").ConnectionString)
        Connection.Open()
        GetOASIS_FEESConnection = Connection
    End Function
    Public Shared Function GetOASIS_ALERTConnectionString() As String
        Return WebConfigurationManager.ConnectionStrings("OASIS_ALERTConnectionString").ConnectionString
    End Function

    Public Shared Function GetOASIS_ALERTConnection() As SqlConnection
        Dim Connection As SqlConnection = New SqlConnection(WebConfigurationManager.ConnectionStrings("OASIS_ALERTConnectionString").ConnectionString)
        Connection.Open()
        GetOASIS_ALERTConnection = Connection
    End Function
    Public Shared Function GetOASIS_SERVICESConnectionString() As String
        Return WebConfigurationManager.ConnectionStrings("OASIS_SERVICEConnectionString").ConnectionString
    End Function

    Public Shared Function GetOASIS_SERVICESConnection() As SqlConnection
        Dim Connection As SqlConnection = New SqlConnection(WebConfigurationManager.ConnectionStrings("OASIS_SERVICEConnectionString").ConnectionString)
        Connection.Open()
        GetOASIS_SERVICESConnection = Connection
    End Function
    Public Shared Function GetOASIS_MEDConnectionString() As String
        Return WebConfigurationManager.ConnectionStrings("OASISConnectionStringMedical").ConnectionString
    End Function


    Public Shared Function GetOASIS_MEDConnection() As SqlConnection
        Dim Connection As SqlConnection = New SqlConnection(WebConfigurationManager.ConnectionStrings("OASISConnectionStringMedical").ConnectionString)
        Connection.Open()
        GetOASIS_MEDConnection = Connection
    End Function

    Public Shared Function GetOASIS_PUR_INVConnectionString() As String
        Return ConfigurationManager.ConnectionStrings("OASIS_PUR_INVConnectionString").ConnectionString
    End Function
End Class
