Imports Microsoft.VisualBasic

Public Enum DateType
    VoucherDate = 0
    ChequeDate = 1
End Enum

Public Enum Status
    ALL = 0
    Open = 1
    Posted = 2
    Deleted = 3
End Enum

Public Enum ProcessType
    DAYEND = 0
    MONTHEND = 1
    YEAREND = 2
End Enum

Public Enum OASISPhotoType
    STUDENT_PHOTO = 1
    EMPLOYEE_PHOTO = 2
End Enum

Public Enum XMLType
    BSUName
    ACTName
    EMPName
    AMOUNT
    AIRFARE
    STUDENT
    ConcessionType
    Grade
    Bus
    CheckList
End Enum

Public Enum ReminderType
    FIRST = 1
    SECOND = 2
    THIRD = 3
End Enum

Public Enum AccountFilter
   
    BANK = 1
    CASHONLY = 2
    NORMAL = 3
    PARTY1 = 4
    PARTY2 = 5
    DEBIT = 7
    DEBIT_D = 8
    CHQISSAC = 9
    INTRAC = 10
    ACRDAC = 11
    PREPDAC = 12
    CHQISSAC_PDC = 13
    CUSTSUPP = 14
    CUSTSUPPnIJV = 15
    NOTCC = 16
    INCOME = 17
    CHARGE = 18
    CONCESSION = 19
    FEEDISC = 20
End Enum

Public Enum STUDENTTYPE
    STUDENT = 1
    ENQUIRY = 2
End Enum

Public Enum SELECTEDPROFORMADURATION
    TERMS = 1
    MONTHS = 0
End Enum

Public Enum ConcessionType
    Sibling = 1
    Staff = 2
    Management = 3
    Merit = 4
    Scholarships = 5
    Discount = 6
End Enum

Public Enum COLLECTIONTYPE
    CASH = 1
    CHEQUES = 2
    CREDIT_CARD = 3
    OTHER_MODES = 4
    INTERNET = 5
End Enum

