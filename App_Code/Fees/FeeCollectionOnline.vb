Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Imports UtilityObj

Public Class FeeCollectionOnline

#Region "Variables"

    Private vFCT_ID As Integer
    Private vSTU_ID As Long, vSTU_NO As String, vSTU_TYPE As String, vACD_ID As Integer, vBSU_ID As String
    Private vAMOUNT As Double, vSTU_NAME As String, vBSU_NAME As String, vUSER As String, vFCT_CONTENT_MESSAGE As String
    Private vFCT_TYPE As String, vFCT_DATE As Date, vFCT_FEE_ID As Integer, vFCT_LINK_EXPDT As Date
    Private vFCT_bSendAckEmail As Boolean, vFCT_bPaidStatus As Boolean, vFEE_DESCR As String, vbPaymentDateExpired As Boolean
    Private vERR_NO As String, vFCT_FCL_RECNO As String, vBSU_CURRENCY As String

    Public Property FCT_ID() As Integer
        Get
            Return vFCT_ID
        End Get
        Set(ByVal value As Integer)
            vFCT_ID = value
        End Set
    End Property

    Public Property STU_ID() As Long
        Get
            Return vSTU_ID
        End Get
        Set(ByVal value As Long)
            vSTU_ID = value
        End Set
    End Property

    Public Property STU_NO() As String
        Get
            Return vSTU_NO
        End Get
        Set(ByVal value As String)
            vSTU_NO = value
        End Set
    End Property

    Public Property ACD_ID() As Integer
        Get
            Return vACD_ID
        End Get
        Set(ByVal value As Integer)
            vACD_ID = value
        End Set
    End Property

    Public Property BSU_ID() As String
        Get
            Return vBSU_ID
        End Get
        Set(ByVal value As String)
            vBSU_ID = value
        End Set
    End Property

    Public Property AMOUNT() As Double
        Get
            Return vAMOUNT
        End Get
        Set(ByVal value As Double)
            vAMOUNT = value
        End Set
    End Property

    Public Property STU_NAME() As String
        Get
            Return vSTU_NAME
        End Get
        Set(ByVal value As String)
            vSTU_NAME = value
        End Set
    End Property

    Public Property STU_TYPE() As String
        Get
            Return vSTU_TYPE
        End Get
        Set(ByVal value As String)
            vSTU_TYPE = value
        End Set
    End Property

    Public Property BSU_NAME() As String
        Get
            Return vBSU_NAME
        End Get
        Set(ByVal value As String)
            vBSU_NAME = value
        End Set
    End Property

    Public Property USER() As String
        Get
            Return vUSER
        End Get
        Set(ByVal value As String)
            vUSER = value
        End Set
    End Property

    Public Property FCT_CONTENT_MESSAGE() As String
        Get
            Return vFCT_CONTENT_MESSAGE
        End Get
        Set(ByVal value As String)
            vFCT_CONTENT_MESSAGE = value
        End Set
    End Property

    Public Property FCT_TYPE() As String
        Get
            Return vFCT_TYPE
        End Get
        Set(ByVal value As String)
            vFCT_TYPE = value
        End Set
    End Property

    Public Property FCT_DATE() As Date
        Get
            Return vFCT_DATE
        End Get
        Set(ByVal value As Date)
            vFCT_DATE = value
        End Set
    End Property

    Public Property FCT_FEE_ID() As Integer
        Get
            Return vFCT_FEE_ID
        End Get
        Set(ByVal value As Integer)
            vFCT_FEE_ID = value
        End Set
    End Property

    Public Property FCT_LINK_EXPDT() As Date
        Get
            Return vFCT_LINK_EXPDT
        End Get
        Set(ByVal value As Date)
            vFCT_LINK_EXPDT = value
        End Set
    End Property

    Public Property FCT_bSendAckEmail() As Boolean
        Get
            Return vFCT_bSendAckEmail
        End Get
        Set(ByVal value As Boolean)
            vFCT_bSendAckEmail = value
        End Set
    End Property

    Public Property FCT_bPaidStatus() As Boolean
        Get
            Return vFCT_bPaidStatus
        End Get
        Set(ByVal value As Boolean)
            vFCT_bPaidStatus = value
        End Set
    End Property

    Public Property FEE_DESCR() As String
        Get
            Return vFEE_DESCR
        End Get
        Set(ByVal value As String)
            vFEE_DESCR = value
        End Set
    End Property

    Public Property bPaymentDateExpired() As Boolean
        Get
            Return vbPaymentDateExpired
        End Get
        Set(ByVal value As Boolean)
            vbPaymentDateExpired = value
        End Set
    End Property

    Public Property ERR_NO() As String
        Get
            Return vERR_NO
        End Get
        Set(ByVal value As String)
            vERR_NO = value
        End Set
    End Property
    Public Property FCT_FCL_RECNO() As String
        Get
            Return vFCT_FCL_RECNO
        End Get
        Set(ByVal value As String)
            vFCT_FCL_RECNO = value
        End Set
    End Property
    Public Property BSU_CURRENCY() As String
        Get
            Return vBSU_CURRENCY
        End Get
        Set(ByVal value As String)
            vBSU_CURRENCY = value
        End Set
    End Property
#End Region

    Public Shared Function F_SaveFEECOLLECTION_H_ONLINE(ByVal p_FCO_ID As Integer, ByVal p_FCO_SOURCE As String, _
        ByVal p_FCO_DATE As DateTime, ByVal p_FCO_ACD_ID As Integer, ByVal p_FCO_STU_ID As String, ByVal p_FCO_STU_TYPE As String, _
        ByVal p_FCO_AMOUNT As Decimal, ByVal p_FCO_Bposted As Boolean, ByRef p_newFCO_ID As Integer, _
        ByVal p_FCO_BSU_ID As String, ByVal p_FCO_NARRATION As String, ByVal p_FCO_DRCR As String, _
        ByVal p_FCO_REFNO As String, ByVal p_FCO_IP_ADDRESS As String, ByVal p_FCO_CPS_ID As String, _
        ByVal p_stTrans As SqlTransaction, Optional ByVal p_FCO_DISC_XML As String = "") As String
        Dim pParms(16) As SqlClient.SqlParameter
        'FEES.F_SaveFEECOLLECTION_H_ONLINE  
        ',@FCO_ID  VARCHAR(20)
        '@FCO_SOURCE VARCHAR(20)
        ',@FCO_RECNO VARCHAR(20)
        ',@FCO_DATE DATETIME 
        ',@FCO_ACD_ID BIGINT  
        pParms(0) = New SqlClient.SqlParameter("@FCO_ID", SqlDbType.BigInt)
        pParms(0).Value = p_FCO_ID
        pParms(1) = New SqlClient.SqlParameter("@FCO_SOURCE", SqlDbType.VarChar, 100)
        pParms(1).Value = p_FCO_SOURCE
        pParms(2) = New SqlClient.SqlParameter("@FCO_DRCR", SqlDbType.VarChar, 100)
        pParms(2).Value = p_FCO_DRCR
        pParms(3) = New SqlClient.SqlParameter("@FCO_DATE", SqlDbType.DateTime)
        pParms(3).Value = p_FCO_DATE
        pParms(4) = New SqlClient.SqlParameter("@FCO_ACD_ID", SqlDbType.BigInt)
        pParms(4).Value = p_FCO_ACD_ID
        ',@FCO_STU_ID BIGINT 
        ',@FCO_AMOUNT NUMERIC(18,3) 
        ',@FCO_Bposted BIT 
        pParms(5) = New SqlClient.SqlParameter("@FCO_STU_ID", SqlDbType.Int)
        pParms(5).Value = p_FCO_STU_ID
        pParms(6) = New SqlClient.SqlParameter("@FCO_AMOUNT", SqlDbType.Decimal, 21)
        pParms(6).Value = p_FCO_AMOUNT
        pParms(7) = New SqlClient.SqlParameter("@FCO_REFNO", SqlDbType.VarChar, 20)
        pParms(7).Value = p_FCO_REFNO
        pParms(8) = New SqlClient.SqlParameter("@FCO_Bposted", SqlDbType.Bit)
        pParms(8).Value = p_FCO_Bposted
        pParms(9) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(9).Direction = ParameterDirection.ReturnValue
        ',@FCO_NARRATION VARCHAR(20)
        ',@FCO_DRCR VARCHAR(20)  
        ',@FCO_BSU_ID VARCHAR(20) 
        ',@NEW_FCO_ID INT OUTPUT
        ',@FCO_REFNO VARCHAR(20)
        pParms(10) = New SqlClient.SqlParameter("@NEW_FCO_ID", SqlDbType.BigInt)
        pParms(10).Direction = ParameterDirection.Output
        pParms(11) = New SqlClient.SqlParameter("@FCO_BSU_ID", SqlDbType.VarChar, 100)
        pParms(11).Value = p_FCO_BSU_ID
        pParms(12) = New SqlClient.SqlParameter("@FCO_NARRATION", SqlDbType.VarChar, 100)
        pParms(12).Value = p_FCO_NARRATION
        pParms(13) = New SqlClient.SqlParameter("@FCO_IP_ADDRESS", SqlDbType.VarChar, 100)
        pParms(13).Value = p_FCO_IP_ADDRESS
        pParms(14) = New SqlClient.SqlParameter("@FCO_STU_TYPE", SqlDbType.VarChar, 2)
        pParms(14).Value = p_FCO_STU_TYPE
        pParms(15) = New SqlClient.SqlParameter("@FCO_CPS_ID", SqlDbType.Int)
        pParms(15).Value = p_FCO_CPS_ID
        pParms(16) = New SqlClient.SqlParameter("@FCO_DISC_XML", SqlDbType.VarChar)
        pParms(16).Value = IIf(p_FCO_DISC_XML = "", "NULL", p_FCO_DISC_XML)

        SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "FEES.F_SaveFEECOLLECTION_H_ONLINE", pParms)
        If pParms(9).Value = 0 Then
            p_newFCO_ID = pParms(10).Value
        End If
        F_SaveFEECOLLECTION_H_ONLINE = pParms(9).Value
    End Function

    Public Shared Function F_SaveFEECOLLSUB_ONLINE(ByVal p_FSO_ID As Integer, ByVal p_FSO_FCO_ID As Integer, _
        ByVal p_FSO_FEE_ID As Integer, ByVal p_FSO_AMOUNT As Decimal, ByVal p_FSO_FSH_ID As String, _
        ByVal p_FSO_ORG_AMOUNT As String, ByVal p_FCO_DISCOUNT As String, ByVal p_stTrans As SqlTransaction) As String
        Dim pParms(7) As SqlClient.SqlParameter
        '[FEES].[F_SaveFEECOLLSUB_ONLINE] 
        '@FSO_ID int, 
        '@FSO_FCO_ID int, 
        '@FSO_FEE_ID int, 
        '@FSO_AMOUNT numeric (18,3),
        '@FSO_FSH_ID  int  ,
        '@FSO_ORG_AMOUNT  numeric (18,3)
        pParms(0) = New SqlClient.SqlParameter("@FSO_ID", SqlDbType.Int)
        pParms(0).Value = p_FSO_ID
        pParms(1) = New SqlClient.SqlParameter("@FSO_FCO_ID", SqlDbType.Int)
        pParms(1).Value = p_FSO_FCO_ID
        pParms(2) = New SqlClient.SqlParameter("@FSO_FEE_ID", SqlDbType.Int)
        pParms(2).Value = p_FSO_FEE_ID
        pParms(3) = New SqlClient.SqlParameter("@FSO_AMOUNT", SqlDbType.Decimal, 21)
        pParms(3).Value = p_FSO_AMOUNT
        pParms(4) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(4).Direction = ParameterDirection.ReturnValue
        pParms(5) = New SqlClient.SqlParameter("@FSO_FSH_ID", SqlDbType.BigInt)
        pParms(5).Value = p_FSO_FSH_ID
        pParms(6) = New SqlClient.SqlParameter("@FSO_ORG_AMOUNT", SqlDbType.Decimal, 21)
        pParms(6).Value = p_FSO_ORG_AMOUNT
        pParms(7) = New SqlClient.SqlParameter("@FCO_DISCOUNT", SqlDbType.Decimal, 21)
        pParms(7).Value = p_FCO_DISCOUNT

        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "FEES.F_SaveFEECOLLSUB_ONLINE", pParms)
        F_SaveFEECOLLSUB_ONLINE = pParms(4).Value
    End Function

    Public Shared Function F_SaveFEECOLLSUB_D_ONLINE(ByVal p_FDO_ID As Integer, ByVal p_FDO_FCO_ID As String, _
        ByVal p_FDO_CLT_ID As Integer, ByVal p_FDO_AMOUNT As String, ByVal p_FDO_REFNO As String, _
         ByVal p_FDO_DATE As String, ByVal p_FDO_STATUS As String, ByVal p_FDO_VHH_DOCNO As String, _
        ByVal p_FDO_REF_ID As String, ByVal p_FDO_EMR_ID As String, ByVal p_stTrans As SqlTransaction, _
        Optional ByVal p_FDO_CHARGE_CLIENT As Decimal = 0) As String
        Dim pParms(11) As SqlClient.SqlParameter
        '@return_value = [FEES].[F_SaveFEECOLLSUB_D]
        '@FDO_ID = 0,
        '@FDO_FCO_ID = 1,
        '@FDO_CLT_ID = 23,
        '@FDO_AMOUNT = 700,
        '@FDO_REFNO = N'VOUC34',
        pParms(0) = New SqlClient.SqlParameter("@FDO_ID", SqlDbType.Int)
        pParms(0).Value = p_FDO_ID
        pParms(1) = New SqlClient.SqlParameter("@FDO_FCO_ID", SqlDbType.BigInt)
        pParms(1).Value = p_FDO_FCO_ID
        pParms(2) = New SqlClient.SqlParameter("@FDO_CLT_ID", SqlDbType.Int)
        pParms(2).Value = p_FDO_CLT_ID
        pParms(3) = New SqlClient.SqlParameter("@FDO_AMOUNT", SqlDbType.Decimal, 21)
        pParms(3).Value = p_FDO_AMOUNT
        pParms(4) = New SqlClient.SqlParameter("@FDO_REFNO", SqlDbType.VarChar, 20)
        pParms(4).Value = p_FDO_REFNO
        '@FDO_DATE = N'12-MAY-2008',
        '@FDO_STATUS = 1,
        '@FDO_VHH_DOCNO = N'VOUN5666'
        '@FDO_REF_ID = N'VOUN5666', 
        '@FDO_EMR_ID = N'VOUN5666'
        pParms(5) = New SqlClient.SqlParameter("@FDO_DATE", SqlDbType.DateTime)
        pParms(5).Value = p_FDO_DATE
        pParms(6) = New SqlClient.SqlParameter("@FDO_STATUS", SqlDbType.VarChar, SqlDbType.Int)
        pParms(6).Value = p_FDO_STATUS
        pParms(7) = New SqlClient.SqlParameter("@FDO_VHH_DOCNO", SqlDbType.VarChar, 20)
        pParms(7).Value = p_FDO_VHH_DOCNO
        pParms(8) = New SqlClient.SqlParameter("@FDO_REF_ID", SqlDbType.VarChar, 20)
        pParms(8).Value = p_FDO_REF_ID
        pParms(9) = New SqlClient.SqlParameter("@FDO_EMR_ID", SqlDbType.VarChar, 20)
        pParms(9).Value = p_FDO_EMR_ID
        pParms(11) = New SqlClient.SqlParameter("@FDO_CHARGE_CLIENT", SqlDbType.Decimal)
        pParms(11).Value = p_FDO_CHARGE_CLIENT
        pParms(10) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(10).Direction = ParameterDirection.ReturnValue
        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "FEES.F_SaveFEECOLLSUB_D_ONLINE", pParms)
        F_SaveFEECOLLSUB_D_ONLINE = pParms(10).Value
    End Function

    Public Shared Function GetUserForOnlinePayment(ByVal p_STU_NO As String, ByVal p_STU_PASSWORD As String, _
        ByVal p_AUD_HOST As String, ByVal p_AUD_WINUSER As String, ByVal p_STU_BSU_ID As String, _
        ByRef dtUserData As DataTable, ByVal p_str_conn As String) As Integer
        'DECLARE	@return_value int 
        'EXEC	@return_value = [FEES].[GetUserDetailsBB]
        '		@STU_NO = N'11100100005134',
        '		@STU_PASSWORD = N'123',
        '		@AUD_HOST = N'WWW',
        '		@AUD_WINUSER = N'WSE',
        '		@STU_BSU_ID='111001'
        Try
            Dim pParms(5) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@STU_NO", SqlDbType.VarChar, 20)
            pParms(0).Value = p_STU_NO
            pParms(1) = New SqlClient.SqlParameter("@STU_PASSWORD", SqlDbType.VarChar, 100)
            pParms(1).Value = p_STU_PASSWORD
            pParms(2) = New SqlClient.SqlParameter("@STU_BSU_ID", SqlDbType.VarChar, 20)
            pParms(2).Value = p_STU_BSU_ID
            pParms(3) = New SqlClient.SqlParameter("@AUD_HOST", SqlDbType.VarChar, 100)
            pParms(3).Value = p_AUD_HOST
            pParms(4) = New SqlClient.SqlParameter("@AUD_WINUSER", SqlDbType.VarChar, 100)
            pParms(4).Value = p_AUD_WINUSER
            pParms(5) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
            pParms(5).Direction = ParameterDirection.ReturnValue
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(p_str_conn, CommandType.StoredProcedure, "FEES.GetUserForOnlinePayment", pParms)
            'retval = SqlHelper.ExecuteNonQuery(objConn, CommandType.StoredProcedure, "GetUserDetails", pParms)
            If pParms(5).Value = "0" Then
                dtUserData = ds.Tables(0)
            End If
            GetUserForOnlinePayment = pParms(5).Value
        Catch ex As Exception
            Errorlog(ex.Message)
            GetUserForOnlinePayment = "1000"
        End Try
    End Function

    Public Shared Function UpdateStudentPassword(ByVal p_STU_OLD_PASSWORD As String, ByVal p_STU_ID As String, _
     ByVal p_STU_PASSWORD As String, ByVal trans As SqlTransaction) As Integer
        Dim pParms(3) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@STU_OLD_PASSWORD", p_STU_OLD_PASSWORD)
        pParms(1) = New SqlClient.SqlParameter("@STU_ID", p_STU_ID)
        pParms(2) = New SqlClient.SqlParameter("@STU_PASSWORD", p_STU_PASSWORD)
        pParms(3) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
        pParms(3).Direction = ParameterDirection.ReturnValue
        SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "FEES.UpdateStudentPassword", pParms)
        Dim ReturnFlag As Integer = pParms(3).Value
        Return ReturnFlag
    End Function

    Public Shared Function F_GetFeeDetailsForCollection(ByVal p_DOCDT As String, ByVal p_STU_ID As String, _
ByVal p_STU_TYPE As String, ByVal BSU_ID As String, ByVal ACD_ID As String) As DataTable
        Dim pParms(4) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@DOCDT", SqlDbType.DateTime)
        pParms(0).Value = p_DOCDT
        pParms(1) = New SqlClient.SqlParameter("@STU_ID", SqlDbType.BigInt)
        pParms(1).Value = p_STU_ID
        pParms(2) = New SqlClient.SqlParameter("@STU_TYPE", SqlDbType.VarChar, 2)
        pParms(2).Value = p_STU_TYPE
        pParms(3) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(3).Value = BSU_ID
        pParms(4) = New SqlClient.SqlParameter("@ACD_ID", SqlDbType.Int)
        pParms(4).Value = ACD_ID
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, _
          CommandType.StoredProcedure, "FEES.F_GetFeeDetailsFOrCollection", pParms)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function

    Public Shared Function F_GetFeeCollectionHistory(ByVal BSU_ID As String, ByVal p_FROMDT As String, ByVal p_TODT As String, _
  ByVal STU_ID As String) As DataTable
        Dim pParms(4) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BSUID", SqlDbType.VarChar, 20)
        pParms(0).Value = BSU_ID
        pParms(1) = New SqlClient.SqlParameter("@FRMDT", SqlDbType.Date)
        pParms(1).Value = p_FROMDT
        pParms(2) = New SqlClient.SqlParameter("@TODT", SqlDbType.Date)
        pParms(2).Value = p_TODT
        pParms(3) = New SqlClient.SqlParameter("@STUID", SqlDbType.BigInt)
        pParms(3).Value = STU_ID

        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, _
          CommandType.StoredProcedure, "[FEES].[GET_ONLINE_FEE_PAYMENT_HISTORY]", pParms)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function

    Public Shared Function GetSECURE_SECRET(ByVal BSU_ID As String) As String
        Dim str_qurey As String = "SELECT BSU_SECURE_SECRET " & _
        " FROM BUSINESSUNIT_M where BSU_ID='" & BSU_ID & "'"
        Dim str_data As String = UtilityObj.GetDataFromSQL(str_qurey, _
            ConnectionManger.GetOASISConnectionString)
        If str_data <> "--" Then
            Return str_data
        Else
            Return ""
        End If
    End Function

    Public Shared Sub GetMerchantDetails(ByVal BSU_ID As String, ByRef MerchantID As String, _
    ByRef MerchantCode As String, ByRef SECURE_SECRET As String)
        Dim ds As New DataSet
        Dim str_qurey As String = "SELECT isnull(BSU_MERCHANTID,'') as BSU_MERCHANTID, " & _
        " isnull(BSU_MERCHANTCODE,'') as BSU_MERCHANTCODE, isnull(BSU_SECURE_SECRET,'') as BSU_SECURE_SECRET " & _
        " FROM BUSINESSUNIT_M where BSU_ID='" & BSU_ID & "'"
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, _
        str_qurey)
        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            MerchantID = ds.Tables(0).Rows(0)("BSU_MERCHANTID")
            MerchantCode = ds.Tables(0).Rows(0)("BSU_MERCHANTCODE")
            SECURE_SECRET = ds.Tables(0).Rows(0)("BSU_SECURE_SECRET")
        End If
    End Sub

    Public Shared Sub GetMerchantDetails_CPS_ID(ByRef MerchantID As String, _
        ByRef MerchantCode As String, ByRef SECURE_SECRET As String, ByVal CPS_ID As String)
        Dim ds As New DataSet
        Dim str_qurey As String = "exec GetMerchantDetails_CPS_ID   " & CPS_ID
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISFINConnectionString, CommandType.Text, _
        str_qurey)
        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            MerchantID = ds.Tables(0).Rows(0)("BSU_MERCHANTID")
            MerchantCode = ds.Tables(0).Rows(0)("BSU_MERCHANTCODE")
            SECURE_SECRET = ds.Tables(0).Rows(0)("BSU_SECURE_SECRET")
        End If
    End Sub



    Public Shared Function F_GetFeeNarration(ByVal p_DOCDT As String, _
       ByVal p_ACD_ID As String, ByVal p_BSU_ID As String) As String
        '[FEES].[F_GetFeeNarration] 
        '@DOCDT varchar(12),
        '@BSU_ID varchar(20),
        '@ACD_ID bigint  
        Dim pParms(3) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@DOCDT", SqlDbType.DateTime)
        pParms(0).Value = p_DOCDT
        pParms(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(1).Value = p_BSU_ID
        pParms(2) = New SqlClient.SqlParameter("@ACD_ID", SqlDbType.BigInt)
        pParms(2).Value = p_ACD_ID
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, _
          CommandType.StoredProcedure, "FEES.F_GetFeeNarration", pParms)
        If dsData.Tables(0).Rows.Count > 0 Then
            Return dsData.Tables(0).Rows(0)(0).ToString
        Else
            Return ""
        End If
    End Function

    Public Shared Function F_SaveFEECOLLECTION_H_ONLINE_PAYMENT_FEES(ByVal p_FCO_ID As String, ByVal p_FCO_VPC_RESPONCECODE As String, _
        ByVal p_FCO_VPC_RESPONCEDESCR As String, ByVal p_FCO_VPC_MESSAGE As String, ByVal p_FCO_VPC_RECNO As String, _
        ByVal p_FCO_VPC_TRANNO As String, ByVal p_FCO_VPC_ACQRESCODE As String, ByVal p_FCO_VPC_BANKAUTID As String, _
        ByVal p_FCO_VPC_BATCHNO As String, ByVal p_FCO_VPC_CARDTYPE As String, ByVal p_FCO_VPC_HASHCODE_RESULT As String, _
        ByVal p_FCO_VPC_AMOUNT As String, ByVal p_FCO_VPC_ORDERINFO As String, ByVal p_FCO_VPC_MERCHANT_ID As String, _
        ByVal p_FCO_VPC_COMMAD As String, ByVal p_FCO_VPC_VERSION As String, ByVal p_FCO_VPC_3DS_INFO As String, _
        ByRef FCL_RECNO As String, ByRef RESPONSE As String) As String
        Dim pParms(20) As SqlClient.SqlParameter
        ''''FCO_ID 
        ''''FCO_VPC_RESPONCECODE 
        ''''FCO_VPC_RESPONCEDESCR 
        ''''FCO_VPC_MESSAGE 
        ''''FCO_VPC_RECNO 
        pParms(0) = New SqlClient.SqlParameter("@FCO_ID", SqlDbType.BigInt)
        pParms(0).Value = p_FCO_ID
        pParms(1) = New SqlClient.SqlParameter("@FCO_VPC_RESPONCECODE", SqlDbType.VarChar, 200)
        pParms(1).Value = p_FCO_VPC_RESPONCECODE
        pParms(2) = New SqlClient.SqlParameter("@FCO_VPC_RESPONCEDESCR", SqlDbType.VarChar, 200)
        pParms(2).Value = p_FCO_VPC_RESPONCEDESCR
        pParms(3) = New SqlClient.SqlParameter("@FCO_VPC_MESSAGE", SqlDbType.VarChar, 200)
        pParms(3).Value = p_FCO_VPC_MESSAGE
        pParms(4) = New SqlClient.SqlParameter("@FCO_VPC_RECNO", SqlDbType.VarChar, 200)
        pParms(4).Value = p_FCO_VPC_RECNO
        ''''FCO_VPC_TRANNO 
        ''''FCO_VPC_ACQRESCODE 
        ''''FCO_VPC_BANKAUTID 
        ''''RECNO = objCmdUpdatePayment("@FCL_RECNO")
        ''''RESP = objCmdUpdatePayment("@RESPONSE")
        pParms(5) = New SqlClient.SqlParameter("@FCO_VPC_TRANNO", SqlDbType.VarChar, 200)
        pParms(5).Value = p_FCO_VPC_TRANNO
        pParms(6) = New SqlClient.SqlParameter("@FCO_VPC_ACQRESCODE", SqlDbType.VarChar, 200)
        pParms(6).Value = p_FCO_VPC_ACQRESCODE
        pParms(7) = New SqlClient.SqlParameter("@FCO_VPC_BANKAUTID", SqlDbType.VarChar, 200)
        pParms(7).Value = p_FCO_VPC_BANKAUTID

        pParms(8) = New SqlClient.SqlParameter("@FCL_RECNO", SqlDbType.VarChar, 200)
        pParms(8).Direction = ParameterDirection.Output
        pParms(9) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(9).Direction = ParameterDirection.ReturnValue
        pParms(10) = New SqlClient.SqlParameter("@RESPONSE", SqlDbType.VarChar, 200)
        pParms(10).Direction = ParameterDirection.Output
        ''''FCO_VPC_BATCHNO 
        ''''FCO_VPC_CARDTYPE 
        ''''FCO_VPC_HASHCODE_RESULT 
        ''''FCO_VPC_AMOUNT 
        pParms(11) = New SqlClient.SqlParameter("@FCO_VPC_BATCHNO", SqlDbType.VarChar, 200)
        pParms(11).Value = p_FCO_VPC_BATCHNO
        pParms(12) = New SqlClient.SqlParameter("@FCO_VPC_CARDTYPE", SqlDbType.VarChar, 200)
        pParms(12).Value = p_FCO_VPC_CARDTYPE
        pParms(13) = New SqlClient.SqlParameter("@FCO_VPC_HASHCODE_RESULT", SqlDbType.VarChar, 200)
        pParms(13).Value = p_FCO_VPC_HASHCODE_RESULT
        pParms(14) = New SqlClient.SqlParameter("@FCO_VPC_AMOUNT", SqlDbType.VarChar, 200)
        pParms(14).Value = p_FCO_VPC_AMOUNT
        ''''FCO_VPC_ORDERINFO 
        ''''FCO_VPC_MERCHANT_ID 
        ''''FCO_VPC_COMMAD 
        ''''FCO_VPC_VERSION 
        ''''FCO_VPC_3DS_INFO 
        pParms(15) = New SqlClient.SqlParameter("@FCO_VPC_ORDERINFO", SqlDbType.VarChar, 200)
        pParms(15).Value = p_FCO_VPC_ORDERINFO
        pParms(16) = New SqlClient.SqlParameter("@FCO_VPC_MERCHANT_ID", SqlDbType.VarChar, 200)
        pParms(16).Value = p_FCO_VPC_MERCHANT_ID
        pParms(17) = New SqlClient.SqlParameter("@FCO_VPC_COMMAD", SqlDbType.VarChar, 200)
        pParms(17).Value = p_FCO_VPC_COMMAD
        pParms(18) = New SqlClient.SqlParameter("@FCO_VPC_VERSION", SqlDbType.VarChar, 200)
        pParms(18).Value = p_FCO_VPC_VERSION
        pParms(19) = New SqlClient.SqlParameter("@FCO_VPC_3DS_INFO", SqlDbType.VarChar, 200)
        pParms(19).Value = p_FCO_VPC_3DS_INFO

        Dim objConn As New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try
            SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "[FEES].[F_SaveFEECOLLECTION_H_ONLINE_PAYMENT]", pParms)
            If pParms(9).Value = 0 Then
                FCL_RECNO = pParms(8).Value
                RESPONSE = pParms(10).Value
                stTrans.Commit()
            Else
                stTrans.Rollback()
            End If
        Catch ex As Exception
            stTrans.Rollback()
            Errorlog(ex.Message)
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
        F_SaveFEECOLLECTION_H_ONLINE_PAYMENT_FEES = pParms(9).Value
    End Function

    Public Shared Function F_SaveFEECOLLECTION_H_ONLINE_PAYMENT_FEES_MULTI(ByVal p_FCO_ID As String, ByVal p_FCO_VPC_RESPONCECODE As String, _
       ByVal p_FCO_VPC_RESPONCEDESCR As String, ByVal p_FCO_VPC_MESSAGE As String, ByVal p_FCO_VPC_RECNO As String, _
       ByVal p_FCO_VPC_TRANNO As String, ByVal p_FCO_VPC_ACQRESCODE As String, ByVal p_FCO_VPC_BANKAUTID As String, _
       ByVal p_FCO_VPC_BATCHNO As String, ByVal p_FCO_VPC_CARDTYPE As String, ByVal p_FCO_VPC_HASHCODE_RESULT As String, _
       ByVal p_FCO_VPC_AMOUNT As String, ByVal p_FCO_VPC_ORDERINFO As String, ByVal p_FCO_VPC_MERCHANT_ID As String, _
       ByVal p_FCO_VPC_COMMAD As String, ByVal p_FCO_VPC_VERSION As String, ByVal p_FCO_VPC_3DS_INFO As String, _
       ByRef FCL_RECNO As String, ByRef RESPONSE As String) As String
        Dim pParms(20) As SqlClient.SqlParameter
        ''''FCO_ID 
        ''''FCO_VPC_RESPONCECODE 
        ''''FCO_VPC_RESPONCEDESCR 
        ''''FCO_VPC_MESSAGE 
        ''''FCO_VPC_RECNO 
        pParms(0) = New SqlClient.SqlParameter("@FCO_ID", SqlDbType.BigInt)
        pParms(0).Value = p_FCO_ID
        pParms(1) = New SqlClient.SqlParameter("@FCO_VPC_RESPONCECODE", SqlDbType.VarChar, 200)
        pParms(1).Value = p_FCO_VPC_RESPONCECODE
        pParms(2) = New SqlClient.SqlParameter("@FCO_VPC_RESPONCEDESCR", SqlDbType.VarChar, 200)
        pParms(2).Value = p_FCO_VPC_RESPONCEDESCR
        pParms(3) = New SqlClient.SqlParameter("@FCO_VPC_MESSAGE", SqlDbType.VarChar, 200)
        pParms(3).Value = p_FCO_VPC_MESSAGE
        pParms(4) = New SqlClient.SqlParameter("@FCO_VPC_RECNO", SqlDbType.VarChar, 200)
        pParms(4).Value = p_FCO_VPC_RECNO
        ''''FCO_VPC_TRANNO 
        ''''FCO_VPC_ACQRESCODE 
        ''''FCO_VPC_BANKAUTID 
        ''''RECNO = objCmdUpdatePayment("@FCL_RECNO")
        ''''RESP = objCmdUpdatePayment("@RESPONSE")
        pParms(5) = New SqlClient.SqlParameter("@FCO_VPC_TRANNO", SqlDbType.VarChar, 200)
        pParms(5).Value = p_FCO_VPC_TRANNO
        pParms(6) = New SqlClient.SqlParameter("@FCO_VPC_ACQRESCODE", SqlDbType.VarChar, 200)
        pParms(6).Value = p_FCO_VPC_ACQRESCODE
        pParms(7) = New SqlClient.SqlParameter("@FCO_VPC_BANKAUTID", SqlDbType.VarChar, 200)
        pParms(7).Value = p_FCO_VPC_BANKAUTID

        pParms(8) = New SqlClient.SqlParameter("@FCL_RECNO", SqlDbType.VarChar, 200)
        pParms(8).Direction = ParameterDirection.Output
        pParms(9) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(9).Direction = ParameterDirection.ReturnValue
        pParms(10) = New SqlClient.SqlParameter("@RESPONSE", SqlDbType.VarChar, 200)
        pParms(10).Direction = ParameterDirection.Output
        ''''FCO_VPC_BATCHNO 
        ''''FCO_VPC_CARDTYPE 
        ''''FCO_VPC_HASHCODE_RESULT 
        ''''FCO_VPC_AMOUNT 
        pParms(11) = New SqlClient.SqlParameter("@FCO_VPC_BATCHNO", SqlDbType.VarChar, 200)
        pParms(11).Value = p_FCO_VPC_BATCHNO
        pParms(12) = New SqlClient.SqlParameter("@FCO_VPC_CARDTYPE", SqlDbType.VarChar, 200)
        pParms(12).Value = p_FCO_VPC_CARDTYPE
        pParms(13) = New SqlClient.SqlParameter("@FCO_VPC_HASHCODE_RESULT", SqlDbType.VarChar, 200)
        pParms(13).Value = p_FCO_VPC_HASHCODE_RESULT
        pParms(14) = New SqlClient.SqlParameter("@FCO_VPC_AMOUNT", SqlDbType.VarChar, 200)
        pParms(14).Value = p_FCO_VPC_AMOUNT
        ''''FCO_VPC_ORDERINFO 
        ''''FCO_VPC_MERCHANT_ID 
        ''''FCO_VPC_COMMAD 
        ''''FCO_VPC_VERSION 
        ''''FCO_VPC_3DS_INFO 
        pParms(15) = New SqlClient.SqlParameter("@FCO_VPC_ORDERINFO", SqlDbType.VarChar, 200)
        pParms(15).Value = p_FCO_VPC_ORDERINFO
        pParms(16) = New SqlClient.SqlParameter("@FCO_VPC_MERCHANT_ID", SqlDbType.VarChar, 200)
        pParms(16).Value = p_FCO_VPC_MERCHANT_ID
        pParms(17) = New SqlClient.SqlParameter("@FCO_VPC_COMMAD", SqlDbType.VarChar, 200)
        pParms(17).Value = p_FCO_VPC_COMMAD
        pParms(18) = New SqlClient.SqlParameter("@FCO_VPC_VERSION", SqlDbType.VarChar, 200)
        pParms(18).Value = p_FCO_VPC_VERSION
        pParms(19) = New SqlClient.SqlParameter("@FCO_VPC_3DS_INFO", SqlDbType.VarChar, 200)
        pParms(19).Value = p_FCO_VPC_3DS_INFO

        Dim objConn As New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try
            SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "[FEES].[F_SaveFEECOLLECTION_H_ONLINE_PAYMENT_MULTI]", pParms)
            If pParms(9).Value = 0 Then
                FCL_RECNO = pParms(8).Value
                RESPONSE = pParms(10).Value
                stTrans.Commit()
            Else
                stTrans.Rollback()
            End If
        Catch ex As Exception
            stTrans.Rollback()
            Errorlog(ex.Message)
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
        F_SaveFEECOLLECTION_H_ONLINE_PAYMENT_FEES_MULTI = pParms(9).Value
    End Function

    Public Shared Function F_SaveFEECOLLECTION_H_ONLINE_FAILURE(ByVal p_FCO_ID As String, ByVal p_FCO_VPC_RESPONCECODE As String, _
        ByVal p_FCO_VPC_RESPONCEDESCR As String, ByVal p_FCO_VPC_MESSAGE As String, ByVal p_FCO_VPC_RECNO As String, _
        ByVal p_FCO_VPC_TRANNO As String, ByVal p_FCO_VPC_ACQRESCODE As String, ByVal p_FCO_VPC_BANKAUTID As String, _
        ByVal p_FCO_VPC_BATCHNO As String, ByVal p_FCO_VPC_CARDTYPE As String, ByVal p_FCO_VPC_HASHCODE_RESULT As String, _
        ByVal p_FCO_VPC_AMOUNT As String, ByVal p_FCO_VPC_ORDERINFO As String, ByVal p_FCO_VPC_MERCHANT_ID As String, _
        ByVal p_FCO_VPC_COMMAD As String, ByVal p_FCO_VPC_VERSION As String, ByVal p_FCO_VPC_3DS_INFO As String, _
        ByRef FCL_RECNO As String, ByRef RESPONSE As String) As String
        Dim pParms(19) As SqlClient.SqlParameter
        ''''FCO_ID 
        ''''FCO_VPC_RESPONCECODE 
        ''''FCO_VPC_RESPONCEDESCR 
        ''''FCO_VPC_MESSAGE 
        ''''FCO_VPC_RECNO 
        pParms(0) = New SqlClient.SqlParameter("@FCO_ID", SqlDbType.BigInt)
        pParms(0).Value = p_FCO_ID
        pParms(1) = New SqlClient.SqlParameter("@FCO_VPC_RESPONCECODE", SqlDbType.VarChar, 200)
        pParms(1).Value = p_FCO_VPC_RESPONCECODE
        pParms(2) = New SqlClient.SqlParameter("@FCO_VPC_RESPONCEDESCR", SqlDbType.VarChar, 200)
        pParms(2).Value = p_FCO_VPC_RESPONCEDESCR
        pParms(3) = New SqlClient.SqlParameter("@FCO_VPC_MESSAGE", SqlDbType.VarChar, 200)
        pParms(3).Value = p_FCO_VPC_MESSAGE
        pParms(4) = New SqlClient.SqlParameter("@FCO_VPC_RECNO", SqlDbType.VarChar, 200)
        pParms(4).Value = p_FCO_VPC_RECNO
        ''''FCO_VPC_TRANNO 
        ''''FCO_VPC_ACQRESCODE 
        ''''FCO_VPC_BANKAUTID 
        ''''RECNO = objCmdUpdatePayment("@FCL_RECNO")
        ''''RESP = objCmdUpdatePayment("@RESPONSE")
        pParms(5) = New SqlClient.SqlParameter("@FCO_VPC_TRANNO", SqlDbType.VarChar, 200)
        pParms(5).Value = p_FCO_VPC_TRANNO
        pParms(6) = New SqlClient.SqlParameter("@FCO_VPC_ACQRESCODE", SqlDbType.VarChar, 200)
        pParms(6).Value = p_FCO_VPC_ACQRESCODE
        pParms(7) = New SqlClient.SqlParameter("@FCO_VPC_BANKAUTID", SqlDbType.VarChar, 200)
        pParms(7).Value = p_FCO_VPC_BANKAUTID

        pParms(8) = New SqlClient.SqlParameter("@FCL_RECNO", SqlDbType.VarChar, 200)
        pParms(8).Direction = ParameterDirection.Output
        pParms(9) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(9).Direction = ParameterDirection.ReturnValue
        pParms(10) = New SqlClient.SqlParameter("@RESPONSE", SqlDbType.VarChar, 200)
        pParms(10).Direction = ParameterDirection.Output
        ''''FCO_VPC_BATCHNO 
        ''''FCO_VPC_CARDTYPE 
        ''''FCO_VPC_HASHCODE_RESULT 
        ''''FCO_VPC_AMOUNT 
        pParms(11) = New SqlClient.SqlParameter("@FCO_VPC_BATCHNO", SqlDbType.VarChar, 200)
        pParms(11).Value = p_FCO_VPC_BATCHNO
        pParms(12) = New SqlClient.SqlParameter("@FCO_VPC_CARDTYPE", SqlDbType.VarChar, 200)
        pParms(12).Value = p_FCO_VPC_CARDTYPE
        pParms(13) = New SqlClient.SqlParameter("@FCO_VPC_HASHCODE_RESULT", SqlDbType.VarChar, 200)
        pParms(13).Value = p_FCO_VPC_HASHCODE_RESULT
        pParms(14) = New SqlClient.SqlParameter("@FCO_VPC_AMOUNT", SqlDbType.VarChar, 200)
        pParms(14).Value = p_FCO_VPC_AMOUNT
        ''''FCO_VPC_ORDERINFO 
        ''''FCO_VPC_MERCHANT_ID 
        ''''FCO_VPC_COMMAD 
        ''''FCO_VPC_VERSION 
        ''''FCO_VPC_3DS_INFO 
        pParms(15) = New SqlClient.SqlParameter("@FCO_VPC_ORDERINFO", SqlDbType.VarChar, 200)
        pParms(15).Value = p_FCO_VPC_ORDERINFO
        pParms(16) = New SqlClient.SqlParameter("@FCO_VPC_MERCHANT_ID", SqlDbType.VarChar, 200)
        pParms(16).Value = p_FCO_VPC_MERCHANT_ID
        pParms(17) = New SqlClient.SqlParameter("@FCO_VPC_COMMAD", SqlDbType.VarChar, 200)
        pParms(17).Value = p_FCO_VPC_COMMAD
        pParms(18) = New SqlClient.SqlParameter("@FCO_VPC_VERSION", SqlDbType.VarChar, 200)
        pParms(18).Value = p_FCO_VPC_VERSION
        pParms(19) = New SqlClient.SqlParameter("@FCO_VPC_3DS_INFO", SqlDbType.VarChar, 200)
        pParms(19).Value = p_FCO_VPC_3DS_INFO

        Dim objConn As New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try
            SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "[FEES].[F_SaveFEECOLLECTION_H_ONLINE_FAILURE]", pParms)
            If pParms(9).Value = 0 Then
                FCL_RECNO = pParms(8).Value
                RESPONSE = pParms(10).Value
                stTrans.Commit()
            Else
                stTrans.Rollback()
            End If
        Catch ex As Exception
            stTrans.Rollback()
            Errorlog(ex.Message)
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
        F_SaveFEECOLLECTION_H_ONLINE_FAILURE = pParms(9).Value
    End Function

    Public Shared Function F_SaveFEECOLLECTION_H_ONLINE_TRYCount(ByVal p_FCO_ID As String) As Integer
        Dim pParms(2) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@FCO_ID", p_FCO_ID)
        pParms(2) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
        pParms(2).Direction = ParameterDirection.ReturnValue
        SqlHelper.ExecuteNonQuery(ConnectionManger.GetOASIS_FEESConnectionString, _
        CommandType.StoredProcedure, "FEES.F_SaveFEECOLLECTION_H_ONLINE_TRYCount", pParms)
        Return pParms(2).Value
    End Function

    Public Shared Function DIS_GetDiscount(ByVal p_FDS_ID As String, ByVal p_STU_ID As String, _
    ByVal p_STU_TYPE As String, ByVal p_DT As String) As DataTable
        '@FDS_ID int,@STU_ID bigint,@STU_TYPE  varchar(2), @DT datetime
        Dim pParms(5) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@FDS_ID", p_FDS_ID)
        pParms(1) = New SqlClient.SqlParameter("@STU_ID", p_STU_ID)
        pParms(2) = New SqlClient.SqlParameter("@STU_TYPE", p_STU_TYPE)
        pParms(3) = New SqlClient.SqlParameter("@DT", p_DT)
        pParms(4) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
        pParms(4).Direction = ParameterDirection.ReturnValue
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, _
        CommandType.StoredProcedure, "FEES.DIS_GetDiscount", pParms)
        Return ds.Tables(0)
    End Function

    Public Shared Sub SAVE_ONLINE_PAYMENT_AUDIT(ByVal p_OPA_FROM As String, ByVal p_OPA_ACTION As String, _
        ByVal p_OPA_DATA As String, ByVal p_OPA_FOC_ID As String)
        Dim pParms(3) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@OPA_FROM", p_OPA_FROM)
        pParms(1) = New SqlClient.SqlParameter("@OPA_ACTION", p_OPA_ACTION)
        pParms(2) = New SqlClient.SqlParameter("@OPA_DATA", p_OPA_DATA)
        pParms(3) = New SqlClient.SqlParameter("@OPA_FOC_ID", p_OPA_FOC_ID)
        SqlHelper.ExecuteNonQuery(ConnectionManger.GetOASISAuditConnectionString, CommandType.StoredProcedure, "FEES.SAVE_ONLINE_PAYMENT_AUDIT", pParms)
    End Sub

    Public Shared Function DIS_FindDiscount(ByVal p_STU_ID As String, ByVal p_STU_TYPE As String, _
        ByVal p_DT As String, ByVal p_BSU_ID As String, ByVal p_STM_ID As String, _
        ByVal p_SelectedAmt As Decimal, ByRef p_DiscountAmt As Decimal) As String
        Dim pParms(14) As SqlClient.SqlParameter

        pParms(0) = New SqlClient.SqlParameter("@STU_ID", SqlDbType.BigInt)
        pParms(0).Value = p_STU_ID
        pParms(1) = New SqlClient.SqlParameter("@STU_TYPE", SqlDbType.VarChar, 2)
        pParms(1).Value = p_STU_TYPE
        pParms(2) = New SqlClient.SqlParameter("@DT", SqlDbType.DateTime)
        pParms(2).Value = p_DT
        pParms(3) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(3).Value = p_BSU_ID
        pParms(4) = New SqlClient.SqlParameter("@STM_ID", SqlDbType.BigInt)
        If p_STM_ID = "" Then
            pParms(4).Value = System.DBNull.Value
        Else
            pParms(4).Value = p_STM_ID
        End If
        pParms(5) = New SqlClient.SqlParameter("@SelectedAmt", SqlDbType.Decimal, 21)
        pParms(5).Value = p_SelectedAmt
        pParms(6) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(6).Direction = ParameterDirection.ReturnValue
        pParms(7) = New SqlClient.SqlParameter("@DiscountAmt", SqlDbType.Decimal, 21)
        pParms(7).Direction = ParameterDirection.Output

        SqlHelper.ExecuteNonQuery(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.StoredProcedure, "FEES.DIS_FindDiscount", pParms)
        If pParms(6).Value = 0 Then
            If Not pParms(7).Value Is Nothing And pParms(7).Value Is System.DBNull.Value Then
                p_DiscountAmt = 0
            Else
                p_DiscountAmt = pParms(7).Value
            End If
        End If
        DIS_FindDiscount = pParms(6).Value
    End Function

    Public Shared Function F_SaveFEECOLLECTION_H_ONLINE_MULTI(ByVal p_FCO_ID As Integer, ByVal p_FCO_SOURCE As String, _
        ByVal p_FCO_DATE As DateTime, ByVal p_FCO_ACD_ID As Integer, ByVal p_FCO_STU_ID As String, ByVal p_FCO_STU_TYPE As String, _
        ByVal p_FCO_AMOUNT As Decimal, ByVal p_FCO_Bposted As Boolean, ByRef p_newFCO_ID As Integer, _
        ByVal p_FCO_BSU_ID As String, ByVal p_FCO_NARRATION As String, ByVal p_FCO_DRCR As String, _
        ByVal p_FCO_REFNO As String, ByVal p_FCO_IP_ADDRESS As String, ByVal p_FCO_CPS_ID As String, _
        ByRef p_FCO_FCO_ID As Int64, _
        ByVal p_stTrans As SqlTransaction, Optional ByVal p_FCO_DISC_XML As String = "", Optional ByVal bNextAcdPayment As Boolean = False) As String


        Dim pParms(18) As SqlClient.SqlParameter
        'FEES.F_SaveFEECOLLECTION_H_ONLINE  
        ',@FCO_ID  VARCHAR(20)
        '@FCO_SOURCE VARCHAR(20)
        ',@FCO_RECNO VARCHAR(20)
        ',@FCO_DATE DATETIME 
        ',@FCO_ACD_ID BIGINT  
        pParms(0) = New SqlClient.SqlParameter("@FCO_ID", SqlDbType.BigInt)
        pParms(0).Value = p_FCO_ID
        pParms(1) = New SqlClient.SqlParameter("@FCO_SOURCE", SqlDbType.VarChar, 100)
        pParms(1).Value = p_FCO_SOURCE
        pParms(2) = New SqlClient.SqlParameter("@FCO_DRCR", SqlDbType.VarChar, 100)
        pParms(2).Value = p_FCO_DRCR
        pParms(3) = New SqlClient.SqlParameter("@FCO_DATE", SqlDbType.DateTime)
        pParms(3).Value = p_FCO_DATE
        pParms(4) = New SqlClient.SqlParameter("@FCO_ACD_ID", SqlDbType.BigInt)
        pParms(4).Value = p_FCO_ACD_ID
        ',@FCO_STU_ID BIGINT 
        ',@FCO_AMOUNT NUMERIC(18,3) 
        ',@FCO_Bposted BIT 
        pParms(5) = New SqlClient.SqlParameter("@FCO_STU_ID", SqlDbType.Int)
        pParms(5).Value = p_FCO_STU_ID
        pParms(6) = New SqlClient.SqlParameter("@FCO_AMOUNT", SqlDbType.Decimal, 21)
        pParms(6).Value = p_FCO_AMOUNT
        pParms(7) = New SqlClient.SqlParameter("@FCO_REFNO", SqlDbType.VarChar, 20)
        pParms(7).Value = p_FCO_REFNO
        pParms(8) = New SqlClient.SqlParameter("@FCO_Bposted", SqlDbType.Bit)
        pParms(8).Value = p_FCO_Bposted
        pParms(9) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(9).Direction = ParameterDirection.ReturnValue
        ',@FCO_NARRATION VARCHAR(20)
        ',@FCO_DRCR VARCHAR(20)  
        ',@FCO_BSU_ID VARCHAR(20) 
        ',@NEW_FCO_ID INT OUTPUT
        ',@FCO_REFNO VARCHAR(20)
        pParms(10) = New SqlClient.SqlParameter("@NEW_FCO_ID", SqlDbType.BigInt)
        pParms(10).Direction = ParameterDirection.Output
        pParms(11) = New SqlClient.SqlParameter("@FCO_BSU_ID", SqlDbType.VarChar, 100)
        pParms(11).Value = p_FCO_BSU_ID
        pParms(12) = New SqlClient.SqlParameter("@FCO_NARRATION", SqlDbType.VarChar, 100)
        pParms(12).Value = p_FCO_NARRATION
        pParms(13) = New SqlClient.SqlParameter("@FCO_IP_ADDRESS", SqlDbType.VarChar, 100)
        pParms(13).Value = p_FCO_IP_ADDRESS
        pParms(14) = New SqlClient.SqlParameter("@FCO_STU_TYPE", SqlDbType.VarChar, 2)
        pParms(14).Value = p_FCO_STU_TYPE
        pParms(15) = New SqlClient.SqlParameter("@FCO_CPS_ID", SqlDbType.Int)
        pParms(15).Value = p_FCO_CPS_ID
        pParms(16) = New SqlClient.SqlParameter("@FCO_DISC_XML", SqlDbType.VarChar)
        pParms(16).Value = IIf(p_FCO_DISC_XML = "", "NULL", p_FCO_DISC_XML)
        pParms(17) = New SqlClient.SqlParameter("@FCO_FCO_ID", SqlDbType.BigInt)
        pParms(17).Value = p_FCO_FCO_ID
        pParms(17).Direction = ParameterDirection.InputOutput

        pParms(18) = New SqlClient.SqlParameter("@FCO_NEXTACDPAYMENT", SqlDbType.Bit)
        pParms(18).Value = bNextAcdPayment


        SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "FEES.F_SaveFEECOLLECTION_H_ONLINE_MULTI", pParms)
        If pParms(9).Value = 0 Then
            p_newFCO_ID = pParms(10).Value
            p_FCO_FCO_ID = pParms(17).Value
        End If
        F_SaveFEECOLLECTION_H_ONLINE_MULTI = pParms(9).Value
    End Function
    Public Shared Function UPDATE_FEECHARGE_COLLECTION(ByVal BSU_ID As String, ByVal Recno As String, ByRef strans2 As SqlTransaction) As Integer
        UPDATE_FEECHARGE_COLLECTION = 1000
        Dim act_paramS1(2) As SqlClient.SqlParameter
        Try
            act_paramS1(0) = New SqlClient.SqlParameter("@BSU_ID", BSU_ID)
            act_paramS1(1) = New SqlClient.SqlParameter("@FCL_RECNO", Recno)
            act_paramS1(2) = New SqlClient.SqlParameter("@retval", SqlDbType.Int)
            act_paramS1(2).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(strans2, CommandType.StoredProcedure, "FEES.UPDATE_FSH_ID_TO_COLLECTION", act_paramS1)
            UPDATE_FEECHARGE_COLLECTION = act_paramS1(2).Value
        Catch ex As Exception
            UtilityObj.Errorlog("From UpdateCharge_And_Post: " + ex.Message, "ACTIVITY PAYMENT RESULT")
        End Try
    End Function
    Public Shared Function CHARGE_AND_POST_FEES(ByVal FCO_ID As String, ByRef strans2 As SqlTransaction) As Integer
        CHARGE_AND_POST_FEES = 1000
        Dim act_params(1) As SqlClient.SqlParameter
        Try
            act_params(0) = New SqlClient.SqlParameter("@FCO_ID", SqlDbType.BigInt)
            act_params(0).Value = FCO_ID
            act_params(1) = New SqlClient.SqlParameter("@ReturnValue", SqlDbType.Int)
            act_params(1).Direction = ParameterDirection.ReturnValue
            SqlHelper.ExecuteNonQuery(strans2, CommandType.StoredProcedure, "FEES.CHARGE_AND_POST_FEES", act_params)
            CHARGE_AND_POST_FEES = act_params(1).Value
        Catch ex As Exception
            UtilityObj.Errorlog("From UpdateCharge_And_Post: " + ex.Message, "ACTIVITY PAYMENT RESULT")
        End Try
    End Function
    Public Shared Function Get_PaidDetails(ByVal FCO_ID As Int64, ByVal bPaymentSuccess As Boolean) As DataTable
        Try
            'Dim strPath As String = Convert.ToString(readerStudent_Detail("STU_PHOTOPATH"))
            Dim connPath As String = WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ConnectionString
            Dim noImagePath As String = "https://school.gemsoasis.com/Oasistest/Images/Home/no_image.gif"
            Dim con As String = ConnectionManger.GetOASIS_FEESConnectionString
            Dim param(4) As SqlClient.SqlParameter

            param(0) = New SqlClient.SqlParameter("@OLU_ID", HttpContext.Current.Session("OLU_ID").ToString)
            param(1) = New SqlClient.SqlParameter("@connPath", connPath)
            param(2) = New SqlClient.SqlParameter("@noImagePath", noImagePath)
            param(3) = New SqlClient.SqlParameter("@FCO_ID", FCO_ID)
            param(4) = New SqlClient.SqlParameter("@bPaymentSuccess", bPaymentSuccess)

            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "FEES.GET_SIBLINGS_PAYMENT_RESULT", param)
            If Not ds Is Nothing AndAlso ds.Tables.Count > 0 Then
                Get_PaidDetails = ds.Tables(0)
            Else
                Get_PaidDetails = Nothing
            End If
        Catch ex As Exception
            Get_PaidDetails = Nothing
        End Try
    End Function

    Public Shared Function CheckIfStudentAllowedForOnlinePayment(ByVal STU_ID As Integer) As Boolean
        Try
            Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, _
              CommandType.Text, "SELECT * FROM VW_StudentAllowedForOnlinePayment WHERE STU_ID=" & STU_ID)
            If Not dsData Is Nothing AndAlso dsData.Tables.Count > 0 AndAlso dsData.Tables(0).Rows.Count > 0 Then
                CheckIfStudentAllowedForOnlinePayment = True
            Else
                CheckIfStudentAllowedForOnlinePayment = False
            End If
        Catch ex As Exception
            CheckIfStudentAllowedForOnlinePayment = False
        End Try
    End Function

#Region "Generic Fee Payment"

    Public Function GET_PAYMENT_DETAILS() As Boolean
        GET_PAYMENT_DETAILS = False
        Try
            Dim ds As New DataSet
            Dim param(3) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@FCT_ID", SqlDbType.Int)
            param(0).Value = FCT_ID
            param(1) = New SqlClient.SqlParameter("@USER", SqlDbType.VarChar, 20)
            param(1).Value = USER
            param(2) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
            param(2).Direction = ParameterDirection.ReturnValue

            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.StoredProcedure, "FEES.GET_STUDENT_FEE_COLLECTION_TRAN", param)
            If param(2).Value = 0 AndAlso Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                FCT_TYPE = ds.Tables(0).Rows(0)("FCT_TYPE").ToString
                STU_ID = ds.Tables(0).Rows(0)("FCT_STU_ID").ToString
                BSU_ID = ds.Tables(0).Rows(0)("FCT_BSU_ID").ToString
                If ds.Tables(0).Rows(0).IsNull("FCT_DATE") Then
                    FCT_DATE = Nothing
                Else
                    FCT_DATE = Convert.ToDateTime(ds.Tables(0).Rows(0)("FCT_DATE"))
                End If
                FCT_FEE_ID = ds.Tables(0).Rows(0)("FCT_FEE_ID").ToString
                AMOUNT = Convert.ToDouble(ds.Tables(0).Rows(0)("FCT_AMOUNT"))

                FCT_CONTENT_MESSAGE = ds.Tables(0).Rows(0)("FCT_CONTENT_MESSAGE").ToString
                FCT_bSendAckEmail = Convert.ToBoolean(ds.Tables(0).Rows(0)("FCT_bSendAckEmail"))
                FCT_bPaidStatus = Convert.ToBoolean(ds.Tables(0).Rows(0)("FCT_bPaidStatus"))
                STU_NO = ds.Tables(0).Rows(0)("STU_NO").ToString
                STU_NAME = ds.Tables(0).Rows(0)("STU_NAME").ToString
                STU_TYPE = ds.Tables(0).Rows(0)("FCT_STU_TYPE").ToString
                BSU_NAME = ds.Tables(0).Rows(0)("BSU_NAME").ToString
                BSU_CURRENCY = ds.Tables(0).Rows(0)("BSU_CURRENCY").ToString
                FEE_DESCR = ds.Tables(0).Rows(0)("FEE_DESCR").ToString
                ACD_ID = ds.Tables(0).Rows(0)("ACD_ID").ToString
                bPaymentDateExpired = Convert.ToBoolean(ds.Tables(0).Rows(0)("bPaymentDateExpired"))
                ERR_NO = ds.Tables(0).Rows(0)("ERR_NO").ToString
                If ds.Tables(0).Rows(0).IsNull("FCT_LINK_EXPDT") Then
                    FCT_LINK_EXPDT = Nothing
                Else
                    FCT_LINK_EXPDT = Convert.ToDateTime(ds.Tables(0).Rows(0)("FCT_LINK_EXPDT"))
                End If
                GET_PAYMENT_DETAILS = True
            End If
        Catch ex As Exception

        End Try
    End Function

    Public Function SET_POST_PAYMENT_DETAILS() As Boolean
        SET_POST_PAYMENT_DETAILS = True
        Dim objConn As New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try
            Dim param(3) As SqlClient.SqlParameter
            param(0) = New SqlClient.SqlParameter("@FCT_ID", SqlDbType.Int)
            param(0).Value = FCT_ID
            param(1) = New SqlClient.SqlParameter("@FCT_bPaidStatus", SqlDbType.Bit)
            param(1).Value = FCT_bPaidStatus
            param(2) = New SqlClient.SqlParameter("@FCT_FCL_RECNO", SqlDbType.VarChar, 20)
            param(2).Value = FCT_FCL_RECNO
            param(3) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
            param(3).Direction = ParameterDirection.ReturnValue

            SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "[FEES].[SET_POST_GENERIC_FEE_PAYMENT_DETAILS]", param)
            If param(3).Value = 0 Then
                stTrans.Commit()
            Else
                stTrans.Rollback()
                SET_POST_PAYMENT_DETAILS = False
            End If
        Catch ex As Exception
            SET_POST_PAYMENT_DETAILS = False
            Errorlog(ex.Message, "GENERIC_FEEPAY")
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
    End Function

    Public Function GET_STUDETAIL_COLLECTION() As Boolean
        GET_STUDETAIL_COLLECTION = True
        Try
            Dim ds As New DataSet
            Dim QRY As String = " SELECT FCL_STU_ID,FCL_STU_TYPE FROM DBO.VW_FEE_COLLECTION WHERE BSU_ID='" & BSU_ID & "' AND FCL_RECNO LIKE '" & FCT_FCL_RECNO & "' "
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, CommandType.Text, QRY)
            If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                STU_TYPE = ds.Tables(0).Rows(0)("FCL_STU_TYPE").ToString
                STU_ID = ds.Tables(0).Rows(0)("FCL_STU_ID").ToString
            Else
                GET_STUDETAIL_COLLECTION = False
            End If
        Catch ex As Exception
            GET_STUDETAIL_COLLECTION = False
        End Try
    End Function
#End Region

#Region "OTHER COLLECTION ONLINE PAYMENT"

    Public Shared Function OTHER_SaveOTHERCOLLECTION_H_ONLINE_MULTI(ByVal p_OCO_ID As Integer, ByVal p_OCO_APD_ID As Integer, ByVal p_OCO_SOURCE As String, _
      ByVal p_OCO_DATE As DateTime, ByVal p_OCO_ACD_ID As Integer, ByVal p_OCO_STU_ID As String, ByVal p_OCO_STU_TYPE As String, _
      ByVal p_OCO_AMOUNT As Decimal, ByVal p_OCO_Bposted As Boolean, ByRef p_newOCO_ID As Integer, _
      ByVal p_OCO_BSU_ID As String, ByVal p_OCO_NARRATION As String, ByVal p_OCO_DRCR As String, _
      ByVal p_OCO_REFNO As String, ByVal p_OCO_IP_ADDRESS As String, ByVal p_OCO_CPS_ID As String, _
      ByRef p_OCO_OCO_ID As Int64, _
      ByVal p_stTrans As SqlTransaction, ByVal p_OCO_ACT_ID As String, ByVal p_OCO_EVT_ID As String, ByVal p_OCO_TAX_CODE As String, _
      Optional ByVal p_OCO_DISC_XML As String = "") As String
        Dim pParms(21) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@OCO_ID", SqlDbType.BigInt)
        pParms(0).Value = p_OCO_ID
        pParms(1) = New SqlClient.SqlParameter("@OCO_SOURCE", SqlDbType.VarChar, 100)
        pParms(1).Value = p_OCO_SOURCE
        pParms(2) = New SqlClient.SqlParameter("@OCO_DRCR", SqlDbType.VarChar, 100)
        pParms(2).Value = p_OCO_DRCR
        pParms(3) = New SqlClient.SqlParameter("@OCO_DATE", SqlDbType.DateTime)
        pParms(3).Value = p_OCO_DATE
        pParms(4) = New SqlClient.SqlParameter("@OCO_ACD_ID", SqlDbType.BigInt)
        pParms(4).Value = p_OCO_ACD_ID
        pParms(5) = New SqlClient.SqlParameter("@OCO_STU_ID", SqlDbType.Int)
        pParms(5).Value = p_OCO_STU_ID
        pParms(6) = New SqlClient.SqlParameter("@OCO_AMOUNT", SqlDbType.Decimal, 21)
        pParms(6).Value = p_OCO_AMOUNT
        pParms(7) = New SqlClient.SqlParameter("@OCO_REFNO", SqlDbType.VarChar, 20)
        pParms(7).Value = p_OCO_REFNO
        pParms(8) = New SqlClient.SqlParameter("@OCO_Bposted", SqlDbType.Bit)
        pParms(8).Value = p_OCO_Bposted
        pParms(9) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(9).Direction = ParameterDirection.ReturnValue
        pParms(10) = New SqlClient.SqlParameter("@NEW_OCO_ID", SqlDbType.BigInt)
        pParms(10).Direction = ParameterDirection.Output
        pParms(11) = New SqlClient.SqlParameter("@OCO_BSU_ID", SqlDbType.VarChar, 100)
        pParms(11).Value = p_OCO_BSU_ID
        pParms(12) = New SqlClient.SqlParameter("@OCO_NARRATION", SqlDbType.VarChar, 100)
        pParms(12).Value = p_OCO_NARRATION
        pParms(13) = New SqlClient.SqlParameter("@OCO_IP_ADDRESS", SqlDbType.VarChar, 100)
        pParms(13).Value = p_OCO_IP_ADDRESS
        pParms(14) = New SqlClient.SqlParameter("@OCO_STU_TYPE", SqlDbType.VarChar, 2)
        pParms(14).Value = p_OCO_STU_TYPE
        pParms(15) = New SqlClient.SqlParameter("@OCO_CPS_ID", SqlDbType.Int)
        pParms(15).Value = p_OCO_CPS_ID
        pParms(16) = New SqlClient.SqlParameter("@OCO_DISC_XML", SqlDbType.VarChar)
        pParms(16).Value = IIf(p_OCO_DISC_XML = "", "NULL", p_OCO_DISC_XML)
        pParms(17) = New SqlClient.SqlParameter("@OCO_ACT_ID", SqlDbType.VarChar)
        pParms(17).Value = p_OCO_ACT_ID
        pParms(18) = New SqlClient.SqlParameter("@OCO_EVT_ID", SqlDbType.VarChar)
        pParms(18).Value = p_OCO_EVT_ID
        pParms(19) = New SqlClient.SqlParameter("@OCO_TAX_CODE", SqlDbType.VarChar)
        pParms(19).Value = p_OCO_TAX_CODE
        pParms(20) = New SqlClient.SqlParameter("@OCO_APD_ID", SqlDbType.BigInt)
        pParms(20).Value = p_OCO_APD_ID
        pParms(21) = New SqlClient.SqlParameter("@OCO_OCO_ID", SqlDbType.BigInt)
        pParms(21).Value = p_OCO_OCO_ID
        pParms(21).Direction = ParameterDirection.InputOutput
        SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "FEES.OTHER_SaveOTHERCOLLECTION_H_ONLINE_MULTI", pParms)
        If pParms(9).Value = 0 Then
            p_newOCO_ID = pParms(10).Value
            p_OCO_OCO_ID = pParms(21).Value
        End If
        OTHER_SaveOTHERCOLLECTION_H_ONLINE_MULTI = pParms(9).Value
    End Function

    Public Shared Function F_SaveOTHERCOLLSUB_D_ONLINE(ByVal p_ODO_ID As Integer, ByVal p_ODO_OCO_ID As String, _
    ByVal p_ODO_CLT_ID As Integer, ByVal p_ODO_AMOUNT As String, ByVal p_ODO_REFNO As String, _
     ByVal p_ODO_DATE As String, ByVal p_ODO_STATUS As String, ByVal p_ODO_VHH_DOCNO As String, _
    ByVal p_ODO_REF_ID As String, ByVal p_ODO_EMR_ID As String, ByVal p_stTrans As SqlTransaction, _
    Optional ByVal p_ODO_CHARGE_CLIENT As Decimal = 0) As String
        Dim pParms(11) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@ODO_ID", SqlDbType.Int)
        pParms(0).Value = p_ODO_ID
        pParms(1) = New SqlClient.SqlParameter("@ODO_FCO_ID", SqlDbType.BigInt)
        pParms(1).Value = p_ODO_OCO_ID
        pParms(2) = New SqlClient.SqlParameter("@ODO_CLT_ID", SqlDbType.Int)
        pParms(2).Value = p_ODO_CLT_ID
        pParms(3) = New SqlClient.SqlParameter("@ODO_AMOUNT", SqlDbType.Decimal, 21)
        pParms(3).Value = p_ODO_AMOUNT
        pParms(4) = New SqlClient.SqlParameter("@ODO_REFNO", SqlDbType.VarChar, 20)
        pParms(4).Value = p_ODO_REFNO
        pParms(5) = New SqlClient.SqlParameter("@ODO_DATE", SqlDbType.DateTime)
        pParms(5).Value = p_ODO_DATE
        pParms(6) = New SqlClient.SqlParameter("@ODO_STATUS", SqlDbType.VarChar, SqlDbType.Int)
        pParms(6).Value = p_ODO_STATUS
        pParms(7) = New SqlClient.SqlParameter("@ODO_VHH_DOCNO", SqlDbType.VarChar, 20)
        pParms(7).Value = p_ODO_VHH_DOCNO
        pParms(8) = New SqlClient.SqlParameter("@ODO_REF_ID", SqlDbType.VarChar, 20)
        pParms(8).Value = p_ODO_REF_ID
        pParms(9) = New SqlClient.SqlParameter("@ODO_EMR_ID", SqlDbType.VarChar, 20)
        pParms(9).Value = p_ODO_EMR_ID
        pParms(11) = New SqlClient.SqlParameter("@ODO_CHARGE_CLIENT", SqlDbType.Decimal)
        pParms(11).Value = p_ODO_CHARGE_CLIENT
        pParms(10) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(10).Direction = ParameterDirection.ReturnValue
        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "FEES.F_SaveOTHERCOLLSUB_D_ONLINE", pParms)
        F_SaveOTHERCOLLSUB_D_ONLINE = pParms(10).Value
    End Function
    Public Shared Function F_SaveOTHERCOLLECTION_H_ONLINE_PAYMENT_FEES(ByVal p_OCO_ID As String, ByVal p_OCO_APD_ID As Integer, ByVal p_OCO_VPC_RESPONCECODE As String, _
    ByVal p_OCO_VPC_RESPONCEDESCR As String, ByVal p_OCO_VPC_MESSAGE As String, ByVal p_OCO_VPC_RECNO As String, _
    ByVal p_OCO_VPC_TRANNO As String, ByVal p_OCO_VPC_ACQRESCODE As String, ByVal p_OCO_VPC_BANKAUTID As String, _
    ByVal p_OCO_VPC_BATCHNO As String, ByVal p_OCO_VPC_CARDTYPE As String, ByVal p_OCO_VPC_HASHCODE_RESULT As String, _
    ByVal p_OCO_VPC_AMOUNT As String, ByVal p_OCO_VPC_ORDERINFO As String, ByVal p_OCO_VPC_MERCHANT_ID As String, _
    ByVal p_OCO_VPC_COMMAD As String, ByVal p_OCO_VPC_VERSION As String, ByVal p_OCO_VPC_3DS_INFO As String, _
    ByRef FCL_RECNO As String, ByRef RESPONSE As String) As String
        Dim pParms(20) As SqlClient.SqlParameter

        pParms(0) = New SqlClient.SqlParameter("@OCO_ID", SqlDbType.BigInt)
        pParms(0).Value = p_OCO_ID
        pParms(1) = New SqlClient.SqlParameter("@OCO_VPC_RESPONCECODE", SqlDbType.VarChar, 200)
        pParms(1).Value = p_OCO_VPC_RESPONCECODE
        pParms(2) = New SqlClient.SqlParameter("@OCO_VPC_RESPONCEDESCR", SqlDbType.VarChar, 200)
        pParms(2).Value = p_OCO_VPC_RESPONCEDESCR
        pParms(3) = New SqlClient.SqlParameter("@OCO_VPC_MESSAGE", SqlDbType.VarChar, 200)
        pParms(3).Value = p_OCO_VPC_MESSAGE
        pParms(4) = New SqlClient.SqlParameter("@OCO_VPC_RECNO", SqlDbType.VarChar, 200)
        pParms(4).Value = p_OCO_VPC_RECNO

        pParms(5) = New SqlClient.SqlParameter("@OCO_VPC_TRANNO", SqlDbType.VarChar, 200)
        pParms(5).Value = p_OCO_VPC_TRANNO
        pParms(6) = New SqlClient.SqlParameter("@OCO_VPC_ACQRESCODE", SqlDbType.VarChar, 200)
        pParms(6).Value = p_OCO_VPC_ACQRESCODE
        pParms(7) = New SqlClient.SqlParameter("@OCO_VPC_BANKAUTID", SqlDbType.VarChar, 200)
        pParms(7).Value = p_OCO_VPC_BANKAUTID

        pParms(8) = New SqlClient.SqlParameter("@OCL_RECNO", SqlDbType.VarChar, 200)
        pParms(8).Direction = ParameterDirection.Output
        pParms(9) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(9).Direction = ParameterDirection.ReturnValue
        pParms(10) = New SqlClient.SqlParameter("@RESPONSE", SqlDbType.VarChar, 200)
        pParms(10).Direction = ParameterDirection.Output

        pParms(11) = New SqlClient.SqlParameter("@OCO_VPC_BATCHNO", SqlDbType.VarChar, 200)
        pParms(11).Value = p_OCO_VPC_BATCHNO
        pParms(12) = New SqlClient.SqlParameter("@OCO_VPC_CARDTYPE", SqlDbType.VarChar, 200)
        pParms(12).Value = p_OCO_VPC_CARDTYPE
        pParms(13) = New SqlClient.SqlParameter("@OCO_VPC_HASHCODE_RESULT", SqlDbType.VarChar, 200)
        pParms(13).Value = p_OCO_VPC_HASHCODE_RESULT
        pParms(14) = New SqlClient.SqlParameter("@OCO_VPC_AMOUNT", SqlDbType.VarChar, 200)
        pParms(14).Value = p_OCO_VPC_AMOUNT

        pParms(15) = New SqlClient.SqlParameter("@OCO_VPC_ORDERINFO", SqlDbType.VarChar, 200)
        pParms(15).Value = p_OCO_VPC_ORDERINFO
        pParms(16) = New SqlClient.SqlParameter("@OCO_VPC_MERCHANT_ID", SqlDbType.VarChar, 200)
        pParms(16).Value = p_OCO_VPC_MERCHANT_ID
        pParms(17) = New SqlClient.SqlParameter("@OCO_VPC_COMMAD", SqlDbType.VarChar, 200)
        pParms(17).Value = p_OCO_VPC_COMMAD
        pParms(18) = New SqlClient.SqlParameter("@OCO_VPC_VERSION", SqlDbType.VarChar, 200)
        pParms(18).Value = p_OCO_VPC_VERSION
        pParms(19) = New SqlClient.SqlParameter("@OCO_VPC_3DS_INFO", SqlDbType.VarChar, 200)
        pParms(19).Value = p_OCO_VPC_3DS_INFO
        pParms(20) = New SqlClient.SqlParameter("@OCO_APD_ID", SqlDbType.VarChar, 200)
        pParms(20).Value = p_OCO_APD_ID
        'pParms(20) = New SqlClient.SqlParameter("@OCL_ACT_ID", SqlDbType.VarChar, 200)
        'pParms(20).Value = p_OCL_ACT_ID
        'pParms(21) = New SqlClient.SqlParameter("@OCL_EVT_ID", SqlDbType.VarChar, 200)
        'pParms(21).Value = p_OCL_EVT_ID
        'pParms(22) = New SqlClient.SqlParameter("@OCL_TAX_CODE", SqlDbType.VarChar, 200)
        'pParms(22).Value = p_OCL_TAX_CODE
        Dim objConn As New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try
            SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "[FEES].[F_SaveOTHERCOLLECTION_H_ONLINE_PAYMENT]", pParms)
            If pParms(9).Value = 0 Then
                FCL_RECNO = pParms(8).Value
                RESPONSE = pParms(10).Value
                stTrans.Commit()
            Else
                stTrans.Rollback()
            End If
        Catch ex As Exception
            stTrans.Rollback()
            Errorlog(ex.Message)
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
        F_SaveOTHERCOLLECTION_H_ONLINE_PAYMENT_FEES = pParms(9).Value
    End Function

    Public Shared Function Activity_SaveFEECOLLECTION_H_ONLINE_PAYMENT_FEES(ByVal p_FCO_ID As String, ByVal p_FCO_VPC_RESPONCECODE As String, _
    ByVal p_FCO_VPC_RESPONCEDESCR As String, ByVal p_FCO_VPC_MESSAGE As String, ByVal p_FCO_VPC_RECNO As String, _
    ByVal p_FCO_VPC_TRANNO As String, ByVal p_FCO_VPC_ACQRESCODE As String, ByVal p_FCO_VPC_BANKAUTID As String, _
    ByVal p_FCO_VPC_BATCHNO As String, ByVal p_FCO_VPC_CARDTYPE As String, ByVal p_FCO_VPC_HASHCODE_RESULT As String, _
    ByVal p_FCO_VPC_AMOUNT As String, ByVal p_FCO_VPC_ORDERINFO As String, ByVal p_FCO_VPC_MERCHANT_ID As String, _
    ByVal p_FCO_VPC_COMMAD As String, ByVal p_FCO_VPC_VERSION As String, ByVal p_FCO_VPC_3DS_INFO As String, _
    ByRef FCL_RECNO As String, ByRef RESPONSE As String, ByRef strans As SqlTransaction) As String
        Dim pParms(20) As SqlClient.SqlParameter
        ''''FCO_ID 
        ''''FCO_VPC_RESPONCECODE 
        ''''FCO_VPC_RESPONCEDESCR 
        ''''FCO_VPC_MESSAGE 
        ''''FCO_VPC_RECNO 
        pParms(0) = New SqlClient.SqlParameter("@FCO_ID", SqlDbType.BigInt)
        pParms(0).Value = p_FCO_ID
        pParms(1) = New SqlClient.SqlParameter("@FCO_VPC_RESPONCECODE", SqlDbType.VarChar, 200)
        pParms(1).Value = p_FCO_VPC_RESPONCECODE
        pParms(2) = New SqlClient.SqlParameter("@FCO_VPC_RESPONCEDESCR", SqlDbType.VarChar, 200)
        pParms(2).Value = p_FCO_VPC_RESPONCEDESCR
        pParms(3) = New SqlClient.SqlParameter("@FCO_VPC_MESSAGE", SqlDbType.VarChar, 200)
        pParms(3).Value = p_FCO_VPC_MESSAGE
        pParms(4) = New SqlClient.SqlParameter("@FCO_VPC_RECNO", SqlDbType.VarChar, 200)
        pParms(4).Value = p_FCO_VPC_RECNO
        ''''FCO_VPC_TRANNO 
        ''''FCO_VPC_ACQRESCODE 
        ''''FCO_VPC_BANKAUTID 
        ''''RECNO = objCmdUpdatePayment("@FCL_RECNO")
        ''''RESP = objCmdUpdatePayment("@RESPONSE")
        pParms(5) = New SqlClient.SqlParameter("@FCO_VPC_TRANNO", SqlDbType.VarChar, 200)
        pParms(5).Value = p_FCO_VPC_TRANNO
        pParms(6) = New SqlClient.SqlParameter("@FCO_VPC_ACQRESCODE", SqlDbType.VarChar, 200)
        pParms(6).Value = p_FCO_VPC_ACQRESCODE
        pParms(7) = New SqlClient.SqlParameter("@FCO_VPC_BANKAUTID", SqlDbType.VarChar, 200)
        pParms(7).Value = p_FCO_VPC_BANKAUTID

        pParms(8) = New SqlClient.SqlParameter("@FCL_RECNO", SqlDbType.VarChar, 200)
        pParms(8).Direction = ParameterDirection.Output
        pParms(9) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(9).Direction = ParameterDirection.ReturnValue
        pParms(10) = New SqlClient.SqlParameter("@RESPONSE", SqlDbType.VarChar, 200)
        pParms(10).Direction = ParameterDirection.Output
        ''''FCO_VPC_BATCHNO 
        ''''FCO_VPC_CARDTYPE 
        ''''FCO_VPC_HASHCODE_RESULT 
        ''''FCO_VPC_AMOUNT 
        pParms(11) = New SqlClient.SqlParameter("@FCO_VPC_BATCHNO", SqlDbType.VarChar, 200)
        pParms(11).Value = p_FCO_VPC_BATCHNO
        pParms(12) = New SqlClient.SqlParameter("@FCO_VPC_CARDTYPE", SqlDbType.VarChar, 200)
        pParms(12).Value = p_FCO_VPC_CARDTYPE
        pParms(13) = New SqlClient.SqlParameter("@FCO_VPC_HASHCODE_RESULT", SqlDbType.VarChar, 200)
        pParms(13).Value = p_FCO_VPC_HASHCODE_RESULT
        pParms(14) = New SqlClient.SqlParameter("@FCO_VPC_AMOUNT", SqlDbType.VarChar, 200)
        pParms(14).Value = p_FCO_VPC_AMOUNT
        ''''FCO_VPC_ORDERINFO 
        ''''FCO_VPC_MERCHANT_ID 
        ''''FCO_VPC_COMMAD 
        ''''FCO_VPC_VERSION 
        ''''FCO_VPC_3DS_INFO 
        pParms(15) = New SqlClient.SqlParameter("@FCO_VPC_ORDERINFO", SqlDbType.VarChar, 200)
        pParms(15).Value = p_FCO_VPC_ORDERINFO
        pParms(16) = New SqlClient.SqlParameter("@FCO_VPC_MERCHANT_ID", SqlDbType.VarChar, 200)
        pParms(16).Value = p_FCO_VPC_MERCHANT_ID
        pParms(17) = New SqlClient.SqlParameter("@FCO_VPC_COMMAD", SqlDbType.VarChar, 200)
        pParms(17).Value = p_FCO_VPC_COMMAD
        pParms(18) = New SqlClient.SqlParameter("@FCO_VPC_VERSION", SqlDbType.VarChar, 200)
        pParms(18).Value = p_FCO_VPC_VERSION
        pParms(19) = New SqlClient.SqlParameter("@FCO_VPC_3DS_INFO", SqlDbType.VarChar, 200)
        pParms(19).Value = p_FCO_VPC_3DS_INFO

        Dim objConn As New SqlConnection(ConnectionManger.GetOASIS_FEESConnectionString)
        objConn.Open()

        Try

            SqlHelper.ExecuteNonQuery(strans, CommandType.StoredProcedure, "[FEES].[F_SaveFEECOLLECTION_H_ONLINE_PAYMENT]", pParms)
            If pParms(9).Value = 0 Then
                FCL_RECNO = pParms(8).Value
                RESPONSE = pParms(10).Value
                ' strans.Commit()
            Else
                strans.Rollback()
            End If
        Catch ex As Exception
            strans.Rollback()
            Errorlog(ex.Message)
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
        Activity_SaveFEECOLLECTION_H_ONLINE_PAYMENT_FEES = pParms(9).Value
    End Function

#End Region
End Class
