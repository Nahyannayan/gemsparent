﻿Imports Microsoft.VisualBasic
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Imports UtilityObj
Public Class clsBookSalesOnline
    Public Shared Function SAVE_BOOK_SALE_H_ONLINE(ByRef BSAHO_ID As Integer, ByVal BSAHO_BSAHO_ID As Integer, ByVal BSAHO_NO As String, _
    ByVal BSAHO_BSU_ID As String, ByVal BSAHO_DATE As DateTime, ByVal BSAHO_TYPE As String, ByVal BSAHO_USR_NAME As String, _
    ByVal BSAHO_CUST_ID As String, ByVal BSAHO_CUST_NO As String, ByVal BSAHO_CUST_NAME As String, _
    ByVal BSAHO_GRADE As String, ByVal BSAHO_SECTION As String, ByVal BSAHO_FYEAR As String, _
    ByVal BSAHO_NARRATION As String, ByVal BSAHO_TOTAL As Decimal, ByVal BSAHO_REMARKS As String, _
    ByVal BSAHO_OLDSAL_NO As String, ByVal BSAHO_CREDITCARD_CHARGE As Decimal, ByVal BSAHO_CASH_TOTAL As Decimal, ByVal BSAHO_CC_TOTAL As Decimal, _
    ByVal BSAHO_RECVD_TOTAL As Decimal, ByVal BSAHO_BALANCE As Decimal, ByVal BSAHO_CREDIT_NO As String, ByVal BSAHO_CREDIT_CARD As Integer, _
    ByVal BSAHO_TAX_CODE As String, ByVal BSAHO_TAX_AMOUNT As Decimal, ByVal BSAHO_NET_AMOUNT As Decimal, ByVal BSAHO_CPS_ID As String, ByVal BSAHO_DELIVERY_TYPE As Integer, ByVal BSAHO_COLLECTION_MESSAGE_ID As Integer, _
    ByVal p_stTrans As SqlTransaction) As Integer
        Dim pParms(31) As SqlParameter
        pParms(1) = Mainclass.CreateSqlParameter("@BSAHO_ID", BSAHO_ID, SqlDbType.Int, True)
        pParms(2) = Mainclass.CreateSqlParameter("@BSAHO_BSAHO_ID", BSAHO_BSAHO_ID, SqlDbType.Int)
        pParms(3) = Mainclass.CreateSqlParameter("@BSAHO_NO", BSAHO_NO, SqlDbType.VarChar)
        pParms(4) = Mainclass.CreateSqlParameter("@BSAHO_BSU_ID", BSAHO_BSU_ID, SqlDbType.VarChar)
        pParms(5) = Mainclass.CreateSqlParameter("@BSAHO_DATE", BSAHO_DATE, SqlDbType.SmallDateTime)
        pParms(6) = Mainclass.CreateSqlParameter("@BSAHO_TYPE", BSAHO_TYPE, SqlDbType.VarChar)
        pParms(7) = Mainclass.CreateSqlParameter("@BSAHO_USR_NAME", BSAHO_USR_NAME, SqlDbType.VarChar)
        pParms(8) = Mainclass.CreateSqlParameter("@BSAHO_CUST_ID", BSAHO_CUST_ID, SqlDbType.VarChar)
        pParms(9) = Mainclass.CreateSqlParameter("@BSAHO_CUST_NO", BSAHO_CUST_NO, SqlDbType.VarChar)
        pParms(10) = Mainclass.CreateSqlParameter("@BSAHO_CUST_NAME", BSAHO_CUST_NAME, SqlDbType.VarChar)
        pParms(11) = Mainclass.CreateSqlParameter("@BSAHO_GRADE", BSAHO_GRADE, SqlDbType.VarChar)
        pParms(12) = Mainclass.CreateSqlParameter("@BSAHO_SECTION", BSAHO_SECTION, SqlDbType.VarChar)

        pParms(13) = Mainclass.CreateSqlParameter("@BSAHO_FYEAR", BSAHO_FYEAR, SqlDbType.VarChar)
        pParms(14) = Mainclass.CreateSqlParameter("@BSAHO_NARRATION", BSAHO_NARRATION, SqlDbType.VarChar)
        pParms(15) = Mainclass.CreateSqlParameter("@BSAHO_TOTAL", BSAHO_TOTAL, SqlDbType.Decimal)
        pParms(16) = Mainclass.CreateSqlParameter("@BSAHO_REMARKS", BSAHO_REMARKS, SqlDbType.VarChar)
        pParms(17) = Mainclass.CreateSqlParameter("@BSAHO_OLDSAL_NO", BSAHO_OLDSAL_NO, SqlDbType.VarChar)
        pParms(18) = Mainclass.CreateSqlParameter("@BSAHO_CREDITCARD_CHARGE", BSAHO_CREDITCARD_CHARGE, SqlDbType.Decimal)
        pParms(19) = Mainclass.CreateSqlParameter("@BSAHO_CASH_TOTAL", BSAHO_CASH_TOTAL, SqlDbType.Decimal)
        pParms(20) = Mainclass.CreateSqlParameter("@BSAHO_CC_TOTAL", BSAHO_CC_TOTAL, SqlDbType.Decimal)
        pParms(21) = Mainclass.CreateSqlParameter("@BSAHO_RECVD_TOTAL", BSAHO_RECVD_TOTAL, SqlDbType.Decimal)
        pParms(22) = Mainclass.CreateSqlParameter("@BSAHO_BALANCE", BSAHO_BALANCE, SqlDbType.Decimal)
        pParms(23) = Mainclass.CreateSqlParameter("@BSAHO_CREDIT_NO", BSAHO_CREDIT_NO, SqlDbType.VarChar)
        pParms(24) = Mainclass.CreateSqlParameter("@BSAHO_CREDIT_CARD", BSAHO_CREDIT_CARD, SqlDbType.BigInt)
        pParms(25) = Mainclass.CreateSqlParameter("@BSAHO_TAX_CODE", BSAHO_TAX_CODE, SqlDbType.VarChar)
        pParms(26) = Mainclass.CreateSqlParameter("@BSAHO_TAX_AMOUNT", BSAHO_TAX_AMOUNT, SqlDbType.Decimal)
        pParms(27) = Mainclass.CreateSqlParameter("@BSAHO_NET_AMOUNT", BSAHO_NET_AMOUNT, SqlDbType.Decimal)
        pParms(28) = Mainclass.CreateSqlParameter("@BSAHO_CPS_ID", BSAHO_CPS_ID, SqlDbType.BigInt)
        pParms(29) = Mainclass.CreateSqlParameter("@BSAHO_DELIVERY_TYPE", BSAHO_DELIVERY_TYPE, SqlDbType.BigInt)
        pParms(30) = Mainclass.CreateSqlParameter("@BSAHO_COLLECTION_MESSAGE_ID", BSAHO_COLLECTION_MESSAGE_ID, SqlDbType.BigInt)
        pParms(31) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(31).Direction = ParameterDirection.ReturnValue

        Dim RetVal As Integer = 0
        SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "DBO.SAVE_BOOKSALE_ONLINE_H", pParms)
        RetVal = pParms(31).Value
        BSAHO_ID = pParms(1).Value
        'If RetVal = -1 Or pParms(1).Value = 0 Then
        '    Return -1
        'End If
        Return pParms(31).Value
    End Function
    Public Shared Function CreateSqlParameter(ByVal ParamName As String, ByVal ParamValue As Object, ByVal ParamType As SqlDbType, Optional ByVal IsOutPutParam As Boolean = False, Optional ByVal MaxSize As Int16 = 0) As SqlParameter
        Try
            Dim mSqlParam As New SqlParameter
            mSqlParam.Value = ParamValue
            mSqlParam.ParameterName = ParamName
            mSqlParam.SqlDbType = ParamType
            'mSqlParam.DbType = ParamType
            If MaxSize <> 0 Then
                mSqlParam.Size = MaxSize
            End If
            If IsOutPutParam Then mSqlParam.Direction = ParameterDirection.InputOutput
            CreateSqlParameter = mSqlParam
        Catch ex As Exception

        End Try
    End Function
    Public Shared Function UPDATE_BARCODE(ByVal OPTIONS As Integer, ByVal BSAH_ID As Integer, ByVal BSAH_BSU_ID As String, ByVal b As Byte(), _
     ByVal p_stTrans As SqlTransaction) As Integer
        Try
            Dim pParms(5) As SqlParameter
            pParms(0) = Mainclass.CreateSqlParameter("@OPTIONS", 1, SqlDbType.Int)
            pParms(1) = Mainclass.CreateSqlParameter("@BSAH_ID", BSAH_ID, SqlDbType.Int)
            pParms(2) = Mainclass.CreateSqlParameter("@BSU_ID", BSAH_BSU_ID, SqlDbType.VarChar)
            pParms(3) = CreateSqlParameter("@BARCODE", b, SqlDbType.Image)
            pParms(4) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
            pParms(4).Direction = ParameterDirection.ReturnValue

            Dim RetVal As Integer = 0

            SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "[dbo].[UPDATE_BARCODE]", pParms)
            RetVal = pParms(4).Value
            'If RetVal = -1 Then
            '    Return -1
            'Else
            '    Return 1
            'End If
            Return RetVal
        Catch e As Exception
            Return -1
        End Try

    End Function
    Public Shared Function SAVE_BOOK_SALE_H(ByVal BSAH_ID As Integer, ByVal BSAH_BSAH_ID As Integer, ByVal BSAH_NO As String, _
    ByVal BSAH_BSU_ID As String, ByVal BSAH_DATE As DateTime, ByVal BSAH_TYPE As String, ByVal BSAH_USR_NAME As String, _
    ByVal BSAH_CUST_ID As String, ByVal BSAH_CUST_NO As String, ByVal BSAH_CUST_NAME As String, _
    ByVal BSAH_GRADE As String, ByVal BSAH_SECTION As String, ByVal BSAH_FYEAR As String, _
    ByVal BSAH_NARRATION As String, ByVal BSAH_TOTAL As Decimal, ByVal BSAH_REMARKS As String, _
    ByVal BSAH_OLDSAL_NO As String, ByVal BSAH_CREDITCARD_CHARGE As Decimal, ByVal BSAH_CASH_TOTAL As Decimal, ByVal BSAH_CC_TOTAL As Decimal, _
    ByVal BSAH_RECVD_TOTAL As Decimal, ByVal BSAH_BALANCE As Decimal, ByVal BSAH_CREDIT_NO As String, ByVal BSAH_CREDIT_CARD As Integer, _
    ByVal BSAH_TAX_CODE As String, ByVal BSAH_TAX_AMOUNT As Decimal, ByVal BSAH_NET_AMOUNT As Decimal, _
    ByVal p_stTrans As SqlTransaction) As Integer
        Dim pParms(27) As SqlParameter
        pParms(1) = Mainclass.CreateSqlParameter("@BSAH_ID", BSAH_ID, SqlDbType.Int, True)
        pParms(2) = Mainclass.CreateSqlParameter("@BSAH_ID_BSAH_ID", BSAH_ID, SqlDbType.Int)
        pParms(3) = Mainclass.CreateSqlParameter("@BSAH_NO", BSAH_NO, SqlDbType.VarChar)
        pParms(4) = Mainclass.CreateSqlParameter("@BSAH_BSU_ID", BSAH_BSU_ID, SqlDbType.VarChar)
        pParms(5) = Mainclass.CreateSqlParameter("@BSAH_DATE", BSAH_DATE, SqlDbType.DateTime)
        pParms(6) = Mainclass.CreateSqlParameter("@BSAH_TYPE", BSAH_TYPE, SqlDbType.VarChar)
        pParms(7) = Mainclass.CreateSqlParameter("@BSAH_USR_NAME", BSAH_USR_NAME, SqlDbType.VarChar)
        pParms(8) = Mainclass.CreateSqlParameter("@BSAH_CUST_ID", BSAH_CUST_ID, SqlDbType.VarChar)
        pParms(9) = Mainclass.CreateSqlParameter("@BSAH_CUST_NO", BSAH_CUST_NO, SqlDbType.VarChar)
        pParms(10) = Mainclass.CreateSqlParameter("@BSAH_CUST_NAME", BSAH_CUST_NAME, SqlDbType.VarChar)
        pParms(11) = Mainclass.CreateSqlParameter("@BSAH_GRADE", BSAH_GRADE, SqlDbType.VarChar)
        pParms(12) = Mainclass.CreateSqlParameter("@BSAH_SECTION", BSAH_SECTION, SqlDbType.VarChar)

        pParms(13) = Mainclass.CreateSqlParameter("@BSAH_FYEAR", BSAH_FYEAR, SqlDbType.VarChar)
        pParms(14) = Mainclass.CreateSqlParameter("@BSAH_NARRATION", BSAH_NARRATION, SqlDbType.VarChar)
        pParms(15) = Mainclass.CreateSqlParameter("@BSAH_TOTAL", BSAH_TOTAL, SqlDbType.Decimal)
        pParms(16) = Mainclass.CreateSqlParameter("@BSAH_REMARKS", BSAH_REMARKS, SqlDbType.VarChar)
        pParms(17) = Mainclass.CreateSqlParameter("@BSAH_OLDSAL_NO", BSAH_OLDSAL_NO, SqlDbType.VarChar)
        pParms(18) = Mainclass.CreateSqlParameter("@BSAH_CREDITCARD_CHARGE", BSAH_CREDITCARD_CHARGE, SqlDbType.Decimal)
        pParms(19) = Mainclass.CreateSqlParameter("@BSAH_CASH_TOTAL", BSAH_CASH_TOTAL, SqlDbType.Decimal)
        pParms(20) = Mainclass.CreateSqlParameter("@BSAH_CC_TOTAL", BSAH_CC_TOTAL, SqlDbType.Decimal)
        pParms(21) = Mainclass.CreateSqlParameter("@BSAH_RECVD_TOTAL", BSAH_RECVD_TOTAL, SqlDbType.Decimal)
        pParms(22) = Mainclass.CreateSqlParameter("@BSAH_BALANCE", BSAH_BALANCE, SqlDbType.Decimal)
        pParms(23) = Mainclass.CreateSqlParameter("@BSAH_CREDIT_NO", BSAH_CREDIT_NO, SqlDbType.VarChar)
        pParms(24) = Mainclass.CreateSqlParameter("@BSAH_CREDIT_CARD", BSAH_CREDIT_CARD, SqlDbType.BigInt)
        pParms(25) = Mainclass.CreateSqlParameter("@BSAH_TAX_CODE", BSAH_TAX_CODE, SqlDbType.Decimal)
        pParms(26) = Mainclass.CreateSqlParameter("@BSAH_TAX_AMOUNT", BSAH_TAX_AMOUNT, SqlDbType.Decimal)
        pParms(27) = Mainclass.CreateSqlParameter("@BSAH_NET_AMOUNT", BSAH_NET_AMOUNT, SqlDbType.Decimal)
        pParms(28) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(28).Direction = ParameterDirection.ReturnValue

        Dim RetVal As Integer = 0
        RetVal = pParms(28).Value
        SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "DBO.SAVE_BOOKSALE_H", pParms)
        If RetVal = -1 Or pParms(1).Value = 0 Then
            Return -1
        End If
        Return pParms(1).Value
    End Function

    Public Shared Function SAVE_BOOK_SALE_D(ByVal BSAD_ID As Integer, ByVal BSAD_BSAH_ID As Integer, _
 ByVal BSAD_ITM_ID As Integer, ByVal BSAD_DESCR As String, ByVal BSAD_DETAILS As String, ByVal BSAD_QTY As Integer, _
 ByVal BSAD_RATE As Decimal, ByVal BSAD_DISC As Decimal, ByVal BSAD_COST As Decimal, ByVal BSAD_VAT_CODE As String, _
 ByVal BSAD_TAX_AMOUNT As Decimal, _
 ByVal p_stTrans As SqlTransaction) As Integer
        Dim iParms(27) As SqlParameter
        iParms(1) = Mainclass.CreateSqlParameter("@BSAD_ID", BSAD_ID, SqlDbType.Int, True)
        iParms(2) = Mainclass.CreateSqlParameter("@BSAD_BSAH_ID", BSAD_BSAH_ID, SqlDbType.Int)
        iParms(3) = Mainclass.CreateSqlParameter("@BSAD_DESCR", BSAD_DESCR, SqlDbType.VarChar)
        iParms(4) = Mainclass.CreateSqlParameter("@BSAD_ITM_ID", BSAD_ITM_ID, SqlDbType.Int)
        iParms(5) = Mainclass.CreateSqlParameter("@BSAD_COST", BSAD_COST, SqlDbType.Decimal)
        iParms(6) = Mainclass.CreateSqlParameter("@BSAD_QTY", BSAD_QTY, SqlDbType.Decimal)
        iParms(7) = Mainclass.CreateSqlParameter("@BSAD_RATE", BSAD_RATE, SqlDbType.Decimal)
        iParms(8) = Mainclass.CreateSqlParameter("@BSAD_DETAILS", BSAD_DETAILS, SqlDbType.VarChar)
        iParms(9) = Mainclass.CreateSqlParameter("@BSAD_VAT_CODE", BSAD_VAT_CODE, SqlDbType.VarChar)
        iParms(10) = Mainclass.CreateSqlParameter("@BSAD_TAX_AMOUNT", BSAD_TAX_AMOUNT, SqlDbType.Decimal)
        iParms(11) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        iParms(11).Direction = ParameterDirection.ReturnValue

        Dim RetVal As Integer = 0
        RetVal = iParms(11).Value
        SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "DBO.SAVE_BOOKSALE_D", iParms)
        If RetVal = -1 Or iParms(1).Value = 0 Then
            Return -1
        End If
        'Return iParms(1).Value
    End Function

    Public Shared Function SAVE_BOOK_SALE_ONLINE_D(ByVal BSADO_ID As Integer, ByVal BSADO_BSAH_ID As Integer, ByVal BSADO_BSH_ID As Integer, _
ByVal BSADO_ITM_ID As Integer, ByVal BSADO_DESCR As String, ByVal BSADO_DETAILS As String, ByVal BSADO_QTY As Integer, _
ByVal BSADO_RATE As Decimal, ByVal BSADO_DISC As Decimal, ByVal BSADO_COST As Decimal, ByVal BSADO_VAT_CODE As String, _
ByVal BSADO_TAX_AMOUNT As Decimal, _
ByVal p_stTrans As SqlTransaction) As Integer
        Dim iParms(27) As SqlParameter
        iParms(1) = Mainclass.CreateSqlParameter("@BSADO_ID", BSADO_ID, SqlDbType.Int, True)
        iParms(2) = Mainclass.CreateSqlParameter("@BSADO_BSAH_ID", BSADO_BSAH_ID, SqlDbType.Int)
        iParms(3) = Mainclass.CreateSqlParameter("@BSADO_BSH_ID", BSADO_BSH_ID, SqlDbType.Int)
        iParms(4) = Mainclass.CreateSqlParameter("@BSADO_DESCR", BSADO_DESCR, SqlDbType.VarChar)
        iParms(5) = Mainclass.CreateSqlParameter("@BSADO_ITM_ID", BSADO_ITM_ID, SqlDbType.Int)
        iParms(6) = Mainclass.CreateSqlParameter("@BSADO_COST", BSADO_COST, SqlDbType.Decimal)
        iParms(7) = Mainclass.CreateSqlParameter("@BSADO_QTY", BSADO_QTY, SqlDbType.Decimal)
        iParms(8) = Mainclass.CreateSqlParameter("@BSADO_RATE", BSADO_RATE, SqlDbType.Decimal)
        iParms(9) = Mainclass.CreateSqlParameter("@BSADO_DETAILS", BSADO_DETAILS, SqlDbType.VarChar)
        iParms(10) = Mainclass.CreateSqlParameter("@BSADO_VAT_CODE", BSADO_VAT_CODE, SqlDbType.VarChar)
        iParms(11) = Mainclass.CreateSqlParameter("@BSADO_TAX_AMOUNT", BSADO_TAX_AMOUNT, SqlDbType.Decimal)
        iParms(12) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        iParms(12).Direction = ParameterDirection.ReturnValue

        Dim RetVal As Integer = 0
        RetVal = iParms(12).Value
        SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "DBO.SAVE_BOOKSALE_ONLINE_D", iParms)
        If RetVal = -1 Or iParms(1).Value = 0 Then
            Return -1
        End If
        Return iParms(1).Value
    End Function

    Public Shared Function SAVE_BOOK_SALE_ONLINE_DTLS(ByVal BSADO_BSAHO_ID As Integer, ByVal DTLS As DataTable, _
 ByVal STU_NO As String, ByVal BSU_ID As String, ByRef RETURN_MESSGAE_VALUE As String, ByVal p_stTrans As SqlTransaction) As Integer
        Dim iParms(27) As SqlParameter

        iParms(1) = Mainclass.CreateSqlParameter("@BSADO_BSAHO_ID", BSADO_BSAHO_ID, SqlDbType.Int)
        iParms(2) = New SqlClient.SqlParameter("@DT_XL", SqlDbType.Structured)
        iParms(2).Value = DTLS
        iParms(3) = Mainclass.CreateSqlParameter("@STU_NO", STU_NO, SqlDbType.VarChar)
        iParms(4) = Mainclass.CreateSqlParameter("@BSU_ID", BSU_ID, SqlDbType.VarChar)
        iParms(5) = Mainclass.CreateSqlParameter("@RETURN_MESSGAE_VALUE", "", SqlDbType.VarChar, True, 250)
        iParms(6) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        iParms(6).Direction = ParameterDirection.ReturnValue

        Dim RetVal As Integer = 0

        SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "DBO.SAVE_BOOKSALE_ONLINE_DTLS", iParms)
        RetVal = iParms(6).Value
        RETURN_MESSGAE_VALUE = iParms(5).Value

        'If RetVal = -1 Or iParms(1).Value = 0 Then
        '    Return -1
        'Else
        '    Return 0
        'End If
        Return RetVal
    End Function
    Public Shared Sub SAVE_ONLINE_PAYMENT_AUDIT(ByVal p_OPA_FROM As String, ByVal p_OPA_ACTION As String, _
      ByVal p_OPA_DATA As String, ByVal p_OPA_FOC_ID As String)
        Dim pParms(3) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@OPA_FROM", p_OPA_FROM)
        pParms(1) = New SqlClient.SqlParameter("@OPA_ACTION", p_OPA_ACTION)
        pParms(2) = New SqlClient.SqlParameter("@OPA_DATA", p_OPA_DATA)
        pParms(3) = New SqlClient.SqlParameter("@OPA_FOC_ID", p_OPA_FOC_ID)
        SqlHelper.ExecuteNonQuery(ConnectionManger.GetOASISAuditConnectionString, CommandType.StoredProcedure, "FEES.SAVE_ONLINE_PAYMENT_AUDIT", pParms)
    End Sub


    Public Shared Function BOOKSALES_ONLINE_PAYMENT(ByVal p_BSAH_BSAH_ID As String, ByVal p_BSAH_VPC_RESPONCECODE As String, _
   ByVal p_BSAH_VPC_RESPONCEDESCR As String, ByVal p_BSAH_VPC_MESSAGE As String, ByVal p_BSAH_VPC_RECNO As String, _
   ByVal p_BSAH_VPC_TRANNO As String, ByVal p_BSAH_VPC_ACQRESCODE As String, ByVal p_BSAH_VPC_BANKAUTID As String, _
   ByVal p_BSAH_VPC_BATCHNO As String, ByVal p_BSAH_VPC_CARDTYPE As String, ByVal p_BSAH_VPC_HASHCODE_RESULT As String, _
   ByVal p_BSAH_VPC_AMOUNT As String, ByVal p_BSAH_VPC_ORDERINFO As String, ByVal p_BSAH_VPC_MERCHANT_ID As String, _
   ByVal p_BSAH_VPC_COMMAD As String, ByVal p_BSAH_VPC_VERSION As String, ByVal p_BSAH_VPC_3DS_INFO As String, _
   ByRef BSAH_RECNO As String, ByRef RESPONSE As String, ByRef receiptno As String) As String
        Dim pParms(20) As SqlClient.SqlParameter
        ''''FCO_ID 
        ''''FCO_VPC_RESPONCECODE 
        ''''FCO_VPC_RESPONCEDESCR 
        ''''FCO_VPC_MESSAGE 
        ''''FCO_VPC_RECNO 
        p_BSAH_BSAH_ID = p_BSAH_BSAH_ID.Replace("BKS", "")
        pParms(0) = New SqlClient.SqlParameter("@BSAHO_ID", SqlDbType.BigInt)
        pParms(0).Value = p_BSAH_BSAH_ID
        pParms(1) = New SqlClient.SqlParameter("@BSAHO_VPC_RESPONCECODE", SqlDbType.VarChar, 200)
        pParms(1).Value = p_BSAH_VPC_RESPONCECODE
        pParms(2) = New SqlClient.SqlParameter("@BSAHO_VPC_RESPONCEDESCR", SqlDbType.VarChar, 200)
        pParms(2).Value = p_BSAH_VPC_RESPONCEDESCR
        pParms(3) = New SqlClient.SqlParameter("@BSAHO_VPC_MESSAGE", SqlDbType.VarChar, 200)
        pParms(3).Value = p_BSAH_VPC_MESSAGE
        pParms(4) = New SqlClient.SqlParameter("@BSAHO_VPC_RECNO", SqlDbType.VarChar, 200)
        pParms(4).Value = p_BSAH_VPC_RECNO
        ''''FCO_VPC_TRANNO 
        ''''FCO_VPC_ACQRESCODE 
        ''''FCO_VPC_BANKAUTID 
        ''''RECNO = objCmdUpdatePayment("@FCL_RECNO")
        ''''RESP = objCmdUpdatePayment("@RESPONSE")
        pParms(5) = New SqlClient.SqlParameter("@BSAHO_VPC_TRANNO", SqlDbType.VarChar, 200)
        pParms(5).Value = p_BSAH_VPC_TRANNO
        pParms(6) = New SqlClient.SqlParameter("@BSAHO_VPC_ACQRESCODE", SqlDbType.VarChar, 200)
        pParms(6).Value = p_BSAH_VPC_ACQRESCODE
        pParms(7) = New SqlClient.SqlParameter("@BSAHO_VPC_BANKAUTID", SqlDbType.VarChar, 200)
        pParms(7).Value = p_BSAH_VPC_BANKAUTID

        pParms(8) = New SqlClient.SqlParameter("@BSAHO_RECNO", SqlDbType.VarChar, 200)
        pParms(8).Direction = ParameterDirection.Output
        pParms(9) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(9).Direction = ParameterDirection.ReturnValue
        pParms(10) = New SqlClient.SqlParameter("@RESPONSE", SqlDbType.VarChar, 200)
        pParms(10).Direction = ParameterDirection.Output
        ''''FCO_VPC_BATCHNO 
        ''''FCO_VPC_CARDTYPE 
        ''''FCO_VPC_HASHCODE_RESULT 
        ''''FCO_VPC_AMOUNT 
        pParms(11) = New SqlClient.SqlParameter("@BSAHO_VPC_BATCHNO", SqlDbType.VarChar, 200)
        pParms(11).Value = p_BSAH_VPC_BATCHNO
        pParms(12) = New SqlClient.SqlParameter("@BSAHO_VPC_CARDTYPE", SqlDbType.VarChar, 200)
        pParms(12).Value = p_BSAH_VPC_CARDTYPE
        pParms(13) = New SqlClient.SqlParameter("@BSAHO_VPC_HASHCODE_RESULT", SqlDbType.VarChar, 200)
        pParms(13).Value = p_BSAH_VPC_HASHCODE_RESULT
        pParms(14) = New SqlClient.SqlParameter("@BSAHO_VPC_AMOUNT", SqlDbType.VarChar, 200)
        pParms(14).Value = p_BSAH_VPC_AMOUNT
        ''''FCO_VPC_ORDERINFO 
        ''''FCO_VPC_MERCHANT_ID 
        ''''FCO_VPC_COMMAD 
        ''''FCO_VPC_VERSION 
        ''''FCO_VPC_3DS_INFO 
        pParms(15) = New SqlClient.SqlParameter("@BSAHO_VPC_ORDERINFO", SqlDbType.VarChar, 200)
        pParms(15).Value = p_BSAH_VPC_ORDERINFO
        pParms(16) = New SqlClient.SqlParameter("@BSAHO_VPC_MERCHANT_ID", SqlDbType.VarChar, 200)
        pParms(16).Value = p_BSAH_VPC_MERCHANT_ID
        pParms(17) = New SqlClient.SqlParameter("@BSAHO_VPC_COMMAD", SqlDbType.VarChar, 200)
        pParms(17).Value = p_BSAH_VPC_COMMAD
        pParms(18) = New SqlClient.SqlParameter("@BSAHO_VPC_VERSION", SqlDbType.VarChar, 200)
        pParms(18).Value = p_BSAH_VPC_VERSION
        pParms(19) = New SqlClient.SqlParameter("@BSAHO_VPC_3DS_INFO", SqlDbType.VarChar, 200)
        pParms(19).Value = p_BSAH_VPC_3DS_INFO
        pParms(20) = New SqlClient.SqlParameter("@RECEIPT_NO", SqlDbType.VarChar, 100)
        pParms(20).Direction = ParameterDirection.Output
        Dim objConn As New SqlConnection(ConnectionManger.GetOASIS_PUR_INVConnectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try
            SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "[DBO].[BOOKSALES_ONLINE_PAYMENT]", pParms)
            If pParms(9).Value = 0 Then
                BSAH_RECNO = pParms(8).Value
                RESPONSE = pParms(10).Value
                receiptno = pParms(20).Value
                stTrans.Commit()
            Else
                stTrans.Rollback()
            End If
        Catch ex As Exception
            stTrans.Rollback()
            Errorlog(ex.Message)
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
        BOOKSALES_ONLINE_PAYMENT = pParms(9).Value
    End Function


    Public Shared Function SAVE_BOOK_SALE_COURIER_DETAILS(ByVal BSCD_ID As Integer, ByVal BSCD_BSAH_ID As Integer, _
 ByVal BSAH_ADDRESS As String, ByVal BSCD_CONTACTNO1 As String, ByVal BSCD_CONTACTNO2 As String, ByVal BSCD_CONTACT_NAME As String, ByVal BSCD_CITY_NAME As String, ByVal BSCD_COURIER_AMOUNT As Decimal, _
ByVal p_stTrans As SqlTransaction) As Integer
        Dim iParms(27) As SqlParameter
        iParms(1) = Mainclass.CreateSqlParameter("@BSCD_ID", BSCD_ID, SqlDbType.Int, True)
        iParms(2) = Mainclass.CreateSqlParameter("@BSCD_BSAHO_ID", BSCD_BSAH_ID, SqlDbType.Int)
        iParms(3) = Mainclass.CreateSqlParameter("@BSCD_ADDRESS", BSAH_ADDRESS, SqlDbType.VarChar)
        iParms(4) = Mainclass.CreateSqlParameter("@BSCD_CONTACTNO1", BSCD_CONTACTNO1, SqlDbType.VarChar)
        iParms(5) = Mainclass.CreateSqlParameter("@BSCD_CONTACTNO2", BSCD_CONTACTNO2, SqlDbType.VarChar)
        iParms(6) = Mainclass.CreateSqlParameter("@BSCD_CONTACT_NAME", BSCD_CONTACT_NAME, SqlDbType.VarChar)
        iParms(7) = Mainclass.CreateSqlParameter("@BSCD_CITY_NAME", BSCD_CITY_NAME, SqlDbType.VarChar)
        iParms(8) = Mainclass.CreateSqlParameter("@BSCD_COURIER_AMOUNT", BSCD_COURIER_AMOUNT, SqlDbType.Decimal)
        iParms(9) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        iParms(9).Direction = ParameterDirection.ReturnValue

        Dim RetVal As Integer = 0
        RetVal = iParms(9).Value
        SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "DBO.SAVE_BOOK_SALE_COURIER_DETAILS", iParms)
        If RetVal = -1 Or iParms(1).Value = 0 Then
            Return -1
        End If
        'Return iParms(1).Value
    End Function
End Class
