Imports Microsoft.ApplicationBlocks.Data
Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Text
Imports UtilityObj

Public Class FeeCommon

    Public Shared Function F_SaveFEE_REFUND_H(ByVal p_FRH_ID As Integer, ByVal p_FRH_BSU_ID As String, _
  ByVal p_FRH_ACD_ID As String, ByVal p_FRH_STU_TYPE As String, ByVal p_FRH_STU_ID As String, _
  ByVal p_FRH_FAR_ID As String, ByVal p_FRH_DATE As String, ByVal p_FRH_CHQDT As String, _
  ByVal p_FRH_bPosted As Boolean, ByRef NEW_FRH_ID As String, ByVal p_FRH_bDeleted As Boolean, _
  ByVal p_FRH_NARRATION As String, ByVal p_FRH_VHH_DOCNO As String, ByVal p_FRH_BANK_CASH As String, _
  ByVal p_FRH_ACT_ID As String, ByVal p_FRH_PAIDTO As String, ByVal p_FRH_TOTAL As String, _
  ByVal p_FRH_BANKCHARGE As String, ByVal p_stTrans As SqlTransaction) As String
        Dim pParms(18) As SqlClient.SqlParameter
        ' [FEES].[F_SaveFEE_REFUND_H] 
        '@FRH_ID BIGINT, 
        '@FRH_BSU_ID VARCHAR(20), 
        '@FRH_ACD_ID INT, 
        '@FRH_STU_TYPE VARCHAR(4), 
        '@FRH_STU_ID BIGINT, 
        pParms(0) = New SqlClient.SqlParameter("@FRH_ID", SqlDbType.BigInt)
        pParms(0).Value = p_FRH_ID
        pParms(1) = New SqlClient.SqlParameter("@FRH_BSU_ID", SqlDbType.VarChar, 20)
        pParms(1).Value = p_FRH_BSU_ID
        pParms(2) = New SqlClient.SqlParameter("@FRH_ACD_ID", SqlDbType.Int)
        pParms(2).Value = p_FRH_ACD_ID
        pParms(3) = New SqlClient.SqlParameter("@FRH_STU_TYPE", SqlDbType.VarChar, 4)
        pParms(3).Value = p_FRH_STU_TYPE
        pParms(4) = New SqlClient.SqlParameter("@FRH_STU_ID", SqlDbType.BigInt)
        pParms(4).Value = p_FRH_STU_ID
        '@FRH_FAR_ID INT, 
        '@FRH_DATE DATETIME, 
        '@FRH_CHQDT DATETIME, 
        '@FRH_bPosted BIT, 
        '@NEW_FRH_ID int output  
        pParms(5) = New SqlClient.SqlParameter("@FRH_FAR_ID", SqlDbType.Int)
        pParms(5).Value = p_FRH_FAR_ID
        pParms(6) = New SqlClient.SqlParameter("@FRH_DATE", SqlDbType.DateTime)
        pParms(6).Value = p_FRH_DATE
        pParms(7) = New SqlClient.SqlParameter("@FRH_CHQDT", SqlDbType.DateTime)
        pParms(7).Value = p_FRH_CHQDT
        pParms(8) = New SqlClient.SqlParameter("@FRH_bPosted", SqlDbType.Bit)
        pParms(8).Value = p_FRH_bPosted
        pParms(9) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(9).Direction = ParameterDirection.ReturnValue
        pParms(10) = New SqlClient.SqlParameter("@NEW_FRH_ID", SqlDbType.BigInt)
        pParms(10).Direction = ParameterDirection.Output
        '@FRH_bDeleted BIT, 
        '@FRH_NARRATION VARCHAR(200), 
        '@FRH_VHH_DOCNO VARCHAR(20), 
        '@FRH_BANK_CASH VARCHAR(1), 
        '@FRH_ACT_ID VARCHAR(20), 
        pParms(11) = New SqlClient.SqlParameter("@FRH_bDeleted", SqlDbType.Bit)
        pParms(11).Value = p_FRH_bDeleted
        pParms(12) = New SqlClient.SqlParameter("@FRH_NARRATION", SqlDbType.VarChar, 300)
        pParms(12).Value = p_FRH_NARRATION
        pParms(13) = New SqlClient.SqlParameter("@FRH_VHH_DOCNO", SqlDbType.VarChar, 20)
        pParms(13).Value = p_FRH_VHH_DOCNO
        pParms(14) = New SqlClient.SqlParameter("@FRH_BANK_CASH", SqlDbType.VarChar, 2)
        pParms(14).Value = p_FRH_BANK_CASH
        pParms(15) = New SqlClient.SqlParameter("@FRH_ACT_ID", SqlDbType.VarChar, 20)
        pParms(15).Value = p_FRH_ACT_ID
        '@FRH_PAIDTO VARCHAR(20), 
        '@FRH_TOTAL DECIMAL(18,3), 
        pParms(16) = New SqlClient.SqlParameter("@FRH_PAIDTO", SqlDbType.VarChar, 200)
        pParms(16).Value = p_FRH_PAIDTO
        pParms(17) = New SqlClient.SqlParameter("@FRH_TOTAL", SqlDbType.Decimal)
        pParms(17).Value = p_FRH_TOTAL
        pParms(18) = New SqlClient.SqlParameter("@FRH_BANKCHARGE", SqlDbType.Decimal)
        pParms(18).Value = p_FRH_BANKCHARGE
        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, OASISConstants.dbFees & ".[FEES].[F_SaveFEE_ONLINEREFUND_H]", pParms)
        If pParms(9).Value = 0 Then
            NEW_FRH_ID = pParms(10).Value
        End If
        F_SaveFEE_REFUND_H = pParms(9).Value
    End Function

    Public Shared Function F_SaveFEE_REFUND_D(ByVal p_FRD_ID As Integer, _
        ByVal p_FRD_FRH_ID As Integer, ByVal p_FRD_AMOUNT As String, ByVal p_FRD_FEE_ID As String, _
         ByVal p_FRD_NARRATION As String, ByVal p_FRD_STU_ID As String, ByVal p_stTrans As SqlTransaction) As String
        Dim pParms(7) As SqlClient.SqlParameter
        'ALTER   PROCEDURE [FEES].[F_SaveFEE_REFUND_D] 
        '@FRD_ID BIGINT, 
        '@FRD_FRH_ID BIGINT, 
        '@FRD_AMOUNT NUMERIC(18,3), 
        '@FRD_FEE_ID BIGINT, 
        '@FRD_NARRATION VARCHAR(100))
        pParms(0) = New SqlClient.SqlParameter("@FRD_ID", SqlDbType.BigInt)
        pParms(0).Value = p_FRD_ID
        pParms(1) = New SqlClient.SqlParameter("@FRD_FRH_ID", SqlDbType.BigInt)
        pParms(1).Value = p_FRD_FRH_ID
        pParms(2) = New SqlClient.SqlParameter("@FRD_AMOUNT", SqlDbType.Decimal, 21)
        pParms(2).Value = p_FRD_AMOUNT
        pParms(3) = New SqlClient.SqlParameter("@FRD_FEE_ID", SqlDbType.Int)
        pParms(3).Value = p_FRD_FEE_ID
        pParms(4) = New SqlClient.SqlParameter("@FRD_STU_ID", SqlDbType.Int)
        pParms(4).Value = p_FRD_STU_ID
        pParms(5) = New SqlClient.SqlParameter("@FRD_NARRATION", SqlDbType.VarChar, 100)
        pParms(5).Value = p_FRD_NARRATION
        pParms(6) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(6).Direction = ParameterDirection.ReturnValue
        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, OASISConstants.dbFees & ".FEES.F_SaveFEE_REFUND_D", pParms)
        F_SaveFEE_REFUND_D = pParms(6).Value
    End Function



    Public Shared Function GetOnlineRefundData(ByVal p_STU_ID As String, _
        ByVal p_STU_TYPE As String, ByVal BSU_ID As String) As DataTable

        Dim pParms(6) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 100)
        pParms(0).Value = BSU_ID
        pParms(1) = New SqlClient.SqlParameter("@STU_ID", SqlDbType.BigInt)
        pParms(1).Value = p_STU_ID
        pParms(2) = New SqlClient.SqlParameter("@STU_TYPE", SqlDbType.VarChar, 2)
        pParms(2).Value = p_STU_TYPE
        pParms(3) = New SqlClient.SqlParameter("@MODE", SqlDbType.VarChar, 10)
        pParms(3).Value = "HEADER"

        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, _
          CommandType.StoredProcedure, "FEES.GetOnlineRefundData", pParms)

        If Not dsData Is Nothing Then
            'If dsData.Tables(0).Columns.Contains("IsRefundallowed") Then
            '    'IsRefundallowed = dsData.Tables(0).Rows(0).Item("IsRefundallowed")
            'End If
            Return dsData.Tables(0)

        Else
            Return Nothing
        End If


    End Function
    Public Shared Function GetNotApprovedFeeRefundRequest(ByVal sBsuid As String, ByVal stu_id As String) As Integer

        Dim str_Sql As String = " SELECT	COUNT(*) AS COUNT   FROM [OASIS_FEES].FEES.FEE_REFUND_H WHERE ISNULL(FRH_bPosted, 0) = 0 AND FRH_STU_ID='" & stu_id & "' AND ISNULL(FRH_bDeleted,0)=0 " &
                                " AND [FRH_BSU_ID]='" & sBsuid & "'"

        Dim ds As New DataSet


        If str_Sql <> "" Then
            ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, _
                                          CommandType.Text, str_Sql)
        End If
        Return ds.Tables(0).Rows(0).Item("COUNT")
    End Function
    Public Shared Function F_GetFeeDetailsFOrCollection_online(ByVal p_DOCDT As String, ByVal p_STU_ID As String, _
        ByVal p_STU_TYPE As String, ByVal BSU_ID As String, Optional ByVal NACDID As Integer = 0) As DataTable
        Dim pParms(5) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@DOCDT", SqlDbType.DateTime)
        pParms(0).Value = p_DOCDT
        pParms(1) = New SqlClient.SqlParameter("@STU_ID", SqlDbType.BigInt)
        pParms(1).Value = p_STU_ID
        pParms(2) = New SqlClient.SqlParameter("@STU_TYPE", SqlDbType.VarChar, 2)
        pParms(2).Value = p_STU_TYPE
        pParms(3) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(3).Value = BSU_ID
        pParms(4) = New SqlClient.SqlParameter("@NEXTACDID", SqlDbType.Bit)
        pParms(4).Value = NACDID

        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, _
          CommandType.StoredProcedure, "FEES.F_GetFeeDetailsFOrCollection_online", pParms)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function

    Public Shared Function Resettlement(ByVal p_BSU_ID As String, ByVal p_FRD_STU_ID As String, ByVal rbLEnquiry As String) As String
        Dim RetVal As Integer = "-1"
        Dim pParms(5) As SqlParameter
        pParms(1) = Mainclass.CreateSqlParameter("@BSU_ID", p_BSU_ID, SqlDbType.Int)
        pParms(2) = Mainclass.CreateSqlParameter("@STU_ID", p_FRD_STU_ID, SqlDbType.VarChar)
        pParms(3) = Mainclass.CreateSqlParameter("@STU_TYPE", rbLEnquiry, SqlDbType.VarChar)
        pParms(4) = Mainclass.CreateSqlParameter("@ASONDT", Now(), SqlDbType.DateTime)
        pParms(5) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(5).Direction = ParameterDirection.ReturnValue

        Try

            RetVal = SqlHelper.ExecuteNonQuery(ConnectionManger.GetOASIS_FEESConnection, CommandType.StoredProcedure, OASISConstants.dbFees & ".FEES.STUDENT_RESETTLE_FEE", pParms)
            RetVal = pParms(5).Value

        Catch ex As Exception
            Errorlog(ex.Message)

        Finally

        End Try


        Return RetVal
    End Function
    Public Shared Function GetFEES_M(ByVal BSU_ID As String, ByVal ACD_ID As String) As DataTable
        Dim sql_query As String = " SELECT FEES.FEES_M.FEE_ID, FEES.FEES_M.FEE_DESCR " _
        & " FROM FEES.FEES_M    where  FEE_ID NOT IN ( " _
        & " SELECT FEES.FEES_M.FEE_ID FROM  FEES.FEES_M INNER JOIN " _
        & " SERVICES_BSU_M AS SVB ON FEES.FEES_M.FEE_SVC_ID = SVB.SVB_SVC_ID" _
        & " AND SVB.SVB_PROVIDER_BSU_ID<> SVB.SVB_BSU_ID AND SVB.SVB_BSU_ID='" & BSU_ID & "'" _
        & " AND SVB_ACD_ID='" & ACD_ID & "' ) ORDER BY FEES.FEES_M.FEE_ORDER  "
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString, _
        CommandType.Text, sql_query)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function

    Public Shared Function DIS_SelectDiscount(ByVal p_DT As String, ByVal p_STU_ID As String, _
        ByVal p_STU_TYPE As String, ByVal BSU_ID As String, ByVal STM_ID As String, ByVal bAll As Boolean) As DataTable
        Dim pParms(5) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@DT", SqlDbType.DateTime)
        pParms(0).Value = p_DT
        pParms(1) = New SqlClient.SqlParameter("@STU_ID", SqlDbType.BigInt)
        pParms(1).Value = p_STU_ID
        pParms(2) = New SqlClient.SqlParameter("@STU_TYPE", SqlDbType.VarChar, 2)
        pParms(2).Value = p_STU_TYPE
        pParms(3) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(3).Value = BSU_ID
        pParms(4) = New SqlClient.SqlParameter("@STM_ID", SqlDbType.VarChar, 20)
        If STM_ID = "" Then
            pParms(4).Value = System.DBNull.Value
        Else
            pParms(4).Value = STM_ID
        End If
        pParms(5) = New SqlClient.SqlParameter("@bAll", SqlDbType.VarChar, 20)
        pParms(5).Value = bAll
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, _
          CommandType.StoredProcedure, "FEES.DIS_SelectDiscount", pParms)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function


    Public Shared Function GETDiscountScheme(ByVal BSU_ID As String) As DataSet
        Dim sqlStr As String
        sqlStr = "SELECT ISNULL(BSU_FEE_OTH_DISC,0) AS BSU_FEE_OTH_DISC,ISNULL(BSU_bAPPLYDISCOUNTSCHEME,0) AS BSU_bAPPLYDISCOUNTSCHEME FROM dbo.BUSINESSUNIT_M WHERE BSU_ID='" & BSU_ID & "'"
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, _
         CommandType.Text, sqlStr)
        Return dsData
    End Function

    Public Shared Function CheckStudentAllowedForOnlinePayment(ByVal STU_ID As String) As DataSet
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_FEESConnectionString, _
               CommandType.Text, "SELECT * FROM VW_StudentAllowedForOnlinePayment WHERE STU_ID=" & STU_ID)
        Return dsData
    End Function

    Public Shared Function GetOtherDiscount(ByVal BSU_ID As String) As DataSet
        Dim sqlStr As String
        sqlStr = "select isnull(BSU_FEE_OTH_DISC,0) from BUSINESSUNIT_M where bsu_id='" & BSU_ID & "'"
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, _
         CommandType.Text, sqlStr)
        Return dsData
    End Function

    Public Shared Function GETAPPLYDISCOUNTSCHEME(ByVal BSU_ID As String) As DataSet
        Dim ds As New DataSet
        Dim str_discount_sql As String = "select isnull(BSU_bAPPLYDISCOUNTSCHEME,0) from businessunit_m where bsu_id='" & BSU_ID & "'"
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, str_discount_sql)
        Return ds
    End Function

    Public Shared Function GET_FEE_TAXABLE(ByVal BSU_ID As String) As Boolean
        Dim bol As Boolean = DirectCast(SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT ISNULL(BUS_bFEE_TAXABLE,0) FROM dbo.BUSINESSUNIT_SUB WHERE BUS_BSU_ID='" & BSU_ID & "'"), Boolean)
        Return bol
    End Function

    Public Shared Function GET_STUDENT_NO(ByVal STU_ID As String) As String
        Dim STU_NO As String = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT STU_NO FROM dbo.STUDENT_M WITH(NOLOCK) WHERE STU_ID='" & STU_ID & "'")
        Return STU_NO
    End Function

    Public Shared Function GET_BSU_NAME(ByVal BSU_ID As String) As String
        Dim BSUName As String = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT BSU_NAME FROM dbo.BUSINESSUNIT_M WITH(NOLOCK) WHERE BSU_ID='" & BSU_ID & "'")
        Return BSUName
    End Function

    Public Shared Function GET_TRANSPORT_BSU(ByVal BSU_ID As String, ByVal STU_ID As String) As String
        Dim str_sql As String = "SELECT  SVB_PROVIDER_BSU_ID FROM OASIS..SERVICES_BSU_M S  WHERE SVB_BSU_ID ='" & BSU_ID & "' AND SVB_ACD_ID IN ( SELECT STU_ACD_ID FROM OASIS..STUDENT_M S WHERE STU_ID='" & STU_ID & "') "
        Dim BSUName As String = SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, str_sql)
        Return BSUName

    End Function

    Public Shared Function GETTransportBusDetails(ByVal STU_NO As String) As DataSet
        Dim sqlStr As String
        sqlStr = "SELECT    ISNULL(BUSDROP.BNO_DESCR, '-') AS STU_DROPOFF_BUSNO , ISNULL(BUSPICK.BNO_DESCR, '-') AS STU_PICKUP_BUSNO,* FROM OASIS_TRANSPORT..STUDENTS AS B WITH ( NOLOCK )  " _
                            & " LEFT OUTER JOIN OASIS_TRANSPORT.TRANSPORT.VV_BUSES AS BUSDROP ON B.STU_DROPOFF_TRP_ID = BUSDROP.TRP_ID " _
                            & " LEFT OUTER JOIN OASIS_TRANSPORT.TRANSPORT.VV_BUSES AS BUSPICK ON B.STU_PICKUP_TRP_ID = BUSPICK.TRP_ID" _
                            & " WHERE B.STU_NO ='" & STU_NO & "'"
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISTRANSPORTConnectionString, _
         CommandType.Text, sqlStr)
        Return dsData
    End Function

End Class
