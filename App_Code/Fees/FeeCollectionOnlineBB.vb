Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Imports UtilityObj
Imports System

Public Class FeeCollectionOnlineBB


    Public Shared Function F_SaveFEECOLLECTION_H_ONLINE(ByVal p_FCO_ID As Integer, ByVal p_FCO_SOURCE As String, _
        ByVal p_FCO_DATE As DateTime, ByVal p_FCO_ACD_ID As Integer, ByVal p_FCO_STU_ID As String, _
        ByVal p_FCO_STU_TYPE As String, ByVal p_FCO_AMOUNT As Decimal, ByVal p_FCO_Bposted As Boolean, _
        ByRef p_newFCO_ID As Integer, ByVal p_FCO_BSU_ID As String, ByVal p_FCO_NARRATION As String, _
        ByVal p_FCO_DRCR As String, ByVal p_FCO_REFNO As String, ByVal p_FCO_IP_ADDRESS As String, _
        ByVal p_FCO_STU_BSU_ID As String, ByVal p_stTrans As SqlTransaction) As String
        Dim pParms(15) As SqlClient.SqlParameter
        'FEES.F_SaveFEECOLLECTION_H_ONLINE  
        ',@FCO_ID  VARCHAR(20)
        '@FCO_SOURCE VARCHAR(20)
        ',@FCO_RECNO VARCHAR(20)
        ',@FCO_DATE DATETIME 
        ',@FCO_ACD_ID BIGINT  
        pParms(0) = New SqlClient.SqlParameter("@FCO_ID", SqlDbType.BigInt)
        pParms(0).Value = p_FCO_ID
        pParms(1) = New SqlClient.SqlParameter("@FCO_SOURCE", SqlDbType.VarChar, 100)
        pParms(1).Value = p_FCO_SOURCE
        pParms(2) = New SqlClient.SqlParameter("@FCO_DRCR", SqlDbType.VarChar, 100)
        pParms(2).Value = p_FCO_DRCR
        pParms(3) = New SqlClient.SqlParameter("@FCO_DATE", SqlDbType.DateTime)
        pParms(3).Value = p_FCO_DATE
        pParms(4) = New SqlClient.SqlParameter("@FCO_ACD_ID", SqlDbType.BigInt)
        pParms(4).Value = p_FCO_ACD_ID
        ',@FCO_STU_ID BIGINT 
        ',@FCO_AMOUNT NUMERIC(18,3) 
        ',@FCO_Bposted BIT 
        pParms(5) = New SqlClient.SqlParameter("@FCO_STU_ID", SqlDbType.Int)
        pParms(5).Value = p_FCO_STU_ID
        pParms(6) = New SqlClient.SqlParameter("@FCO_AMOUNT", SqlDbType.Decimal, 21)
        pParms(6).Value = p_FCO_AMOUNT
        pParms(7) = New SqlClient.SqlParameter("@FCO_REFNO", SqlDbType.VarChar, 20)
        pParms(7).Value = p_FCO_REFNO
        pParms(8) = New SqlClient.SqlParameter("@FCO_Bposted", SqlDbType.Bit)
        pParms(8).Value = p_FCO_Bposted
        pParms(9) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(9).Direction = ParameterDirection.ReturnValue
        ',@FCO_NARRATION VARCHAR(20)
        ',@FCO_DRCR VARCHAR(20)  
        ',@FCO_BSU_ID VARCHAR(20) 
        ',@NEW_FCO_ID INT OUTPUT
        ',@FCO_REFNO VARCHAR(20)
        pParms(10) = New SqlClient.SqlParameter("@NEW_FCO_ID", SqlDbType.BigInt)
        pParms(10).Direction = ParameterDirection.Output
        pParms(11) = New SqlClient.SqlParameter("@FCO_BSU_ID", SqlDbType.VarChar, 100)
        pParms(11).Value = p_FCO_BSU_ID
        pParms(12) = New SqlClient.SqlParameter("@FCO_NARRATION", SqlDbType.VarChar, 100)
        pParms(12).Value = p_FCO_NARRATION
        pParms(13) = New SqlClient.SqlParameter("@FCO_IP_ADDRESS", SqlDbType.VarChar, 100)
        pParms(13).Value = p_FCO_IP_ADDRESS
        pParms(14) = New SqlClient.SqlParameter("@FCO_STU_TYPE", SqlDbType.VarChar, 2)
        pParms(14).Value = p_FCO_STU_TYPE
        pParms(15) = New SqlClient.SqlParameter("@FCO_STU_BSU_ID", SqlDbType.VarChar, 100)
        pParms(15).Value = p_FCO_STU_BSU_ID

        SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "TRANSPORT.F_SaveFEECOLLECTION_H_ONLINE", pParms)
        If pParms(9).Value = 0 Then
            p_newFCO_ID = pParms(10).Value
        End If
        F_SaveFEECOLLECTION_H_ONLINE = pParms(9).Value
    End Function
    Public Shared Function F_SaveFEECOLLSUB_D_ONLINE(ByVal p_FDO_ID As Integer, ByVal p_FDO_FCO_ID As String, _
       ByVal p_FDO_CLT_ID As Integer, ByVal p_FDO_AMOUNT As String, ByVal p_FDO_REFNO As String, _
        ByVal p_FDO_DATE As String, ByVal p_FDO_STATUS As String, ByVal p_FDO_VHH_DOCNO As String, _
       ByVal p_FDO_REF_ID As String, ByVal p_FDO_EMR_ID As String, ByVal p_stTrans As SqlTransaction, Optional ByVal p_FDO_CHARGE_CLIENT As Double = 0) As String
        Dim pParms(11) As SqlClient.SqlParameter
        '@return_value = [FEES].[F_SaveFEECOLLSUB_D]
        '@FDO_ID = 0,
        '@FDO_FCO_ID = 1,
        '@FDO_CLT_ID = 23,
        '@FDO_AMOUNT = 700,
        '@FDO_REFNO = N'VOUC34',
        pParms(0) = New SqlClient.SqlParameter("@FDO_ID", SqlDbType.Int)
        pParms(0).Value = p_FDO_ID
        pParms(1) = New SqlClient.SqlParameter("@FDO_FCO_ID", SqlDbType.BigInt)
        pParms(1).Value = p_FDO_FCO_ID
        pParms(2) = New SqlClient.SqlParameter("@FDO_CLT_ID", SqlDbType.Int)
        pParms(2).Value = p_FDO_CLT_ID
        pParms(3) = New SqlClient.SqlParameter("@FDO_AMOUNT", SqlDbType.Decimal, 21)
        pParms(3).Value = p_FDO_AMOUNT
        pParms(4) = New SqlClient.SqlParameter("@FDO_REFNO", SqlDbType.VarChar, 20)
        pParms(4).Value = p_FDO_REFNO
        '@FDO_DATE = N'12-MAY-2008',
        '@FDO_STATUS = 1,
        '@FDO_VHH_DOCNO = N'VOUN5666'
        '@FDO_REF_ID = N'VOUN5666', 
        '@FDO_EMR_ID = N'VOUN5666'
        pParms(5) = New SqlClient.SqlParameter("@FDO_DATE", SqlDbType.DateTime)
        pParms(5).Value = p_FDO_DATE
        pParms(6) = New SqlClient.SqlParameter("@FDO_STATUS", SqlDbType.VarChar, SqlDbType.Int)
        pParms(6).Value = p_FDO_STATUS
        pParms(7) = New SqlClient.SqlParameter("@FDO_VHH_DOCNO", SqlDbType.VarChar, 20)
        pParms(7).Value = p_FDO_VHH_DOCNO
        pParms(8) = New SqlClient.SqlParameter("@FDO_REF_ID", SqlDbType.VarChar, 20)
        pParms(8).Value = p_FDO_REF_ID
        pParms(9) = New SqlClient.SqlParameter("@FDO_EMR_ID", SqlDbType.VarChar, 20)
        pParms(9).Value = p_FDO_EMR_ID
        pParms(10) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(10).Direction = ParameterDirection.ReturnValue
        pParms(11) = New SqlClient.SqlParameter("@FDO_CHARGE_CLIENT", SqlDbType.Decimal)
        pParms(11).Value = p_FDO_CHARGE_CLIENT
        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "TRANSPORT.F_SaveFEECOLLSUB_D_ONLINE", pParms)
        F_SaveFEECOLLSUB_D_ONLINE = pParms(10).Value
    End Function
    Public Shared Function F_SaveFEECOLLECTION_H_ONLINE_MULTI(ByVal p_FCO_ID As Integer, ByVal p_FCO_SOURCE As String, _
   ByVal p_FCO_DATE As DateTime, ByVal p_FCO_ACD_ID As Integer, ByVal p_FCO_STU_ID As String, _
   ByVal p_FCO_STU_TYPE As String, ByVal p_FCO_AMOUNT As Decimal, ByVal p_FCO_Bposted As Boolean, _
   ByRef p_newFCO_ID As Integer, ByVal p_FCO_BSU_ID As String, ByVal p_FCO_NARRATION As String, _
   ByVal p_FCO_DRCR As String, ByVal p_FCO_REFNO As String, ByVal p_FCO_IP_ADDRESS As String, _
   ByVal p_FCO_STU_BSU_ID As String, ByVal p_FCO_SBL_ID As String, ByVal p_FCO_CPS_ID As String, _
   ByVal p_stTrans As SqlTransaction, ByRef p_FCO_FCO_ID As Int64) As String

        Dim pParms(19) As SqlClient.SqlParameter

        pParms(0) = New SqlClient.SqlParameter("@FCO_ID", SqlDbType.BigInt)
        pParms(0).Value = p_FCO_ID
        pParms(1) = New SqlClient.SqlParameter("@FCO_SOURCE", SqlDbType.VarChar, 100)
        pParms(1).Value = p_FCO_SOURCE
        pParms(2) = New SqlClient.SqlParameter("@FCO_DRCR", SqlDbType.VarChar, 100)
        pParms(2).Value = p_FCO_DRCR
        pParms(3) = New SqlClient.SqlParameter("@FCO_DATE", SqlDbType.DateTime)
        pParms(3).Value = p_FCO_DATE
        pParms(4) = New SqlClient.SqlParameter("@FCO_ACD_ID", SqlDbType.BigInt)
        pParms(4).Value = p_FCO_ACD_ID
        pParms(5) = New SqlClient.SqlParameter("@FCO_STU_ID", SqlDbType.Int)
        pParms(5).Value = p_FCO_STU_ID
        pParms(6) = New SqlClient.SqlParameter("@FCO_AMOUNT", SqlDbType.Decimal, 21)
        pParms(6).Value = p_FCO_AMOUNT
        pParms(7) = New SqlClient.SqlParameter("@FCO_REFNO", SqlDbType.VarChar, 20)
        pParms(7).Value = p_FCO_REFNO
        pParms(8) = New SqlClient.SqlParameter("@FCO_Bposted", SqlDbType.Bit)
        pParms(8).Value = p_FCO_Bposted
        pParms(9) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(9).Direction = ParameterDirection.ReturnValue
        pParms(10) = New SqlClient.SqlParameter("@NEW_FCO_ID", SqlDbType.BigInt)
        pParms(10).Direction = ParameterDirection.Output
        pParms(11) = New SqlClient.SqlParameter("@FCO_BSU_ID", SqlDbType.VarChar, 100)
        pParms(11).Value = p_FCO_BSU_ID
        pParms(12) = New SqlClient.SqlParameter("@FCO_NARRATION", SqlDbType.VarChar, 100)
        pParms(12).Value = p_FCO_NARRATION
        pParms(13) = New SqlClient.SqlParameter("@FCO_IP_ADDRESS", SqlDbType.VarChar, 100)
        pParms(13).Value = p_FCO_IP_ADDRESS
        pParms(14) = New SqlClient.SqlParameter("@FCO_STU_TYPE", SqlDbType.VarChar, 2)
        pParms(14).Value = p_FCO_STU_TYPE
        pParms(15) = New SqlClient.SqlParameter("@FCO_STU_BSU_ID", SqlDbType.VarChar, 100)
        pParms(15).Value = p_FCO_STU_BSU_ID
        pParms(16) = New SqlClient.SqlParameter("@FCO_SBL_ID", SqlDbType.Int)
        pParms(16).Value = p_FCO_SBL_ID
        pParms(17) = New SqlClient.SqlParameter("@FCO_CPS_ID", SqlDbType.Int)
        pParms(17).Value = p_FCO_CPS_ID
        pParms(18) = New SqlClient.SqlParameter("@FCO_FCO_ID", SqlDbType.BigInt)
        pParms(18).Value = p_FCO_FCO_ID
        pParms(18).Direction = ParameterDirection.InputOutput
        pParms(19) = New SqlClient.SqlParameter("@FCO_SOURCE_PORTAL", SqlDbType.VarChar, 10)
        pParms(19).Value = "GEMSPARENT"
        SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "TRANSPORT.[F_SaveFEECOLLECTION_H_ONLINE_MULTI]", pParms)
        If pParms(9).Value = 0 Then
            p_newFCO_ID = pParms(10).Value
        End If
        F_SaveFEECOLLECTION_H_ONLINE_MULTI = pParms(9).Value
    End Function

    Public Shared Function F_SaveFEECOLLSUB_ONLINE(ByVal p_FSO_ID As Integer, ByVal p_FSO_FCO_ID As Integer, _
        ByVal p_FSO_FEE_ID As Integer, ByVal p_FSO_AMOUNT As Decimal, ByVal p_FSO_FSH_ID As String, _
        ByVal p_FSO_ORG_AMOUNT As String, ByVal p_stTrans As SqlTransaction) As String
        Dim pParms(6) As SqlClient.SqlParameter
        '[FEES].[F_SaveFEECOLLSUB_ONLINE] 
        '@FSO_ID int, 
        '@FSO_FCO_ID int, 
        '@FSO_FEE_ID int, 
        '@FSO_AMOUNT numeric (18,3),
        '@FSO_FSH_ID  int  ,
        '@FSO_ORG_AMOUNT  numeric (18,3)
        pParms(0) = New SqlClient.SqlParameter("@FSO_ID", SqlDbType.Int)
        pParms(0).Value = p_FSO_ID
        pParms(1) = New SqlClient.SqlParameter("@FSO_FCO_ID", SqlDbType.Int)
        pParms(1).Value = p_FSO_FCO_ID
        pParms(2) = New SqlClient.SqlParameter("@FSO_FEE_ID", SqlDbType.Int)
        pParms(2).Value = p_FSO_FEE_ID
        pParms(3) = New SqlClient.SqlParameter("@FSO_AMOUNT", SqlDbType.Decimal, 21)
        pParms(3).Value = p_FSO_AMOUNT
        pParms(4) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(4).Direction = ParameterDirection.ReturnValue
        pParms(5) = New SqlClient.SqlParameter("@FSO_FSH_ID", SqlDbType.BigInt)
        pParms(5).Value = p_FSO_FSH_ID
        pParms(6) = New SqlClient.SqlParameter("@FSO_ORG_AMOUNT", SqlDbType.Decimal, 21)
        pParms(6).Value = p_FSO_ORG_AMOUNT
        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "TRANSPORT.F_SaveFEECOLLSUB_ONLINE", pParms)
        F_SaveFEECOLLSUB_ONLINE = pParms(4).Value
    End Function


    Public Shared Function F_SaveFEECOLLSUB_D_ONLINE(ByVal p_FDO_ID As Integer, ByVal p_FDO_FCO_ID As String, _
        ByVal p_FDO_CLT_ID As Integer, ByVal p_FDO_AMOUNT As String, ByVal p_FDO_REFNO As String, _
         ByVal p_FDO_DATE As String, ByVal p_FDO_STATUS As String, ByVal p_FDO_VHH_DOCNO As String, _
        ByVal p_FDO_REF_ID As String, ByVal p_FDO_EMR_ID As String, ByVal p_stTrans As SqlTransaction) As String
        Dim pParms(10) As SqlClient.SqlParameter
        '@return_value = [FEES].[F_SaveFEECOLLSUB_D]
        '@FDO_ID = 0,
        '@FDO_FCO_ID = 1,
        '@FDO_CLT_ID = 23,
        '@FDO_AMOUNT = 700,
        '@FDO_REFNO = N'VOUC34',
        pParms(0) = New SqlClient.SqlParameter("@FDO_ID", SqlDbType.Int)
        pParms(0).Value = p_FDO_ID
        pParms(1) = New SqlClient.SqlParameter("@FDO_FCO_ID", SqlDbType.BigInt)
        pParms(1).Value = p_FDO_FCO_ID
        pParms(2) = New SqlClient.SqlParameter("@FDO_CLT_ID", SqlDbType.Int)
        pParms(2).Value = p_FDO_CLT_ID
        pParms(3) = New SqlClient.SqlParameter("@FDO_AMOUNT", SqlDbType.Decimal, 21)
        pParms(3).Value = p_FDO_AMOUNT
        pParms(4) = New SqlClient.SqlParameter("@FDO_REFNO", SqlDbType.VarChar, 20)
        pParms(4).Value = p_FDO_REFNO
        '@FDO_DATE = N'12-MAY-2008',
        '@FDO_STATUS = 1,
        '@FDO_VHH_DOCNO = N'VOUN5666'
        '@FDO_REF_ID = N'VOUN5666', 
        '@FDO_EMR_ID = N'VOUN5666'
        pParms(5) = New SqlClient.SqlParameter("@FDO_DATE", SqlDbType.DateTime)
        pParms(5).Value = p_FDO_DATE
        pParms(6) = New SqlClient.SqlParameter("@FDO_STATUS", SqlDbType.VarChar, SqlDbType.Int)
        pParms(6).Value = p_FDO_STATUS
        pParms(7) = New SqlClient.SqlParameter("@FDO_VHH_DOCNO", SqlDbType.VarChar, 20)
        pParms(7).Value = p_FDO_VHH_DOCNO
        pParms(8) = New SqlClient.SqlParameter("@FDO_REF_ID", SqlDbType.VarChar, 20)
        pParms(8).Value = p_FDO_REF_ID
        pParms(9) = New SqlClient.SqlParameter("@FDO_EMR_ID", SqlDbType.VarChar, 20)
        pParms(9).Value = p_FDO_EMR_ID
        pParms(10) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(10).Direction = ParameterDirection.ReturnValue
        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "TRANSPORT.F_SaveFEECOLLSUB_D_ONLINE", pParms)
        F_SaveFEECOLLSUB_D_ONLINE = pParms(10).Value
    End Function

    Public Shared Function GetMinimumFeePayableStudent(ByVal p_Dt As String, ByVal p_BSU_ID As String, ByVal p_SBL_ID As String, _
  ByVal p_OustandingAmt As String, ByRef p_Amount As Decimal, ByVal p_STU_ID As String, ByVal p_STU_TYPE As String) As String
        Dim pParms(7) As SqlClient.SqlParameter
        'EXEC	@return_value	= [dbo].[GetMinimumFeePayable]
        '@Dt				= @FCO_DATE,
        '@BSU_ID			= @FCO_STU_BSU_ID,
        '@SBL_ID			= @FCO_SBL_ID,
        '@OustandingAmt	= @ActualDue,
        '@Amount			= @Amount OUTPUT
        pParms(0) = New SqlClient.SqlParameter("@Dt", SqlDbType.DateTime)
        pParms(0).Value = p_Dt
        pParms(1) = New SqlClient.SqlParameter("@STU_BSU_ID", SqlDbType.VarChar, 20)
        pParms(1).Value = p_BSU_ID
        pParms(2) = New SqlClient.SqlParameter("@SBL_ID", SqlDbType.Int)
        pParms(2).Value = p_SBL_ID
        pParms(3) = New SqlClient.SqlParameter("@OustandingAmt", SqlDbType.Decimal)
        pParms(3).Value = p_OustandingAmt
        pParms(4) = New SqlClient.SqlParameter("@Amount", SqlDbType.Decimal)
        pParms(4).Direction = ParameterDirection.Output
        pParms(5) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(5).Direction = ParameterDirection.ReturnValue
        pParms(6) = New SqlClient.SqlParameter("@STU_ID", SqlDbType.BigInt)
        pParms(6).Value = p_STU_ID
        pParms(7) = New SqlClient.SqlParameter("@STU_TYPE", SqlDbType.VarChar, 5)
        pParms(7).Value = p_STU_TYPE
        SqlHelper.ExecuteNonQuery(ConnectionManger.GetOASISTRANSPORTConnectionString, _
        CommandType.StoredProcedure, "GetMinimumFeePayableStudent", pParms)
        If pParms(5).Value = 0 AndAlso IsNumeric(pParms(4).Value) Then
            p_Amount = pParms(4).Value
        End If
        GetMinimumFeePayableStudent = pParms(5).Value
    End Function

    Public Shared Function GetUserForOnlinePayment(ByVal p_STU_NO As String, ByVal p_STU_PASSWORD As String, _
        ByVal p_AUD_HOST As String, ByVal p_AUD_WINUSER As String, ByVal p_STU_BSU_ID As String, _
        ByRef dtUserData As DataTable, ByVal p_str_conn As String) As Integer
        'DECLARE	@return_value int 
        'EXEC	@return_value = [FEES].[GetUserDetailsBB]
        '		@STU_NO = N'11100100005134',
        '		@STU_PASSWORD = N'123',
        '		@AUD_HOST = N'WWW',
        '		@AUD_WINUSER = N'WSE',
        '		@STU_BSU_ID='111001'
        Try
            Dim pParms(5) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@STU_NO", SqlDbType.VarChar, 20)
            pParms(0).Value = p_STU_NO
            pParms(1) = New SqlClient.SqlParameter("@STU_PASSWORD", SqlDbType.VarChar, 100)
            pParms(1).Value = p_STU_PASSWORD
            pParms(2) = New SqlClient.SqlParameter("@STU_BSU_ID", SqlDbType.VarChar, 20)
            pParms(2).Value = p_STU_BSU_ID
            pParms(3) = New SqlClient.SqlParameter("@AUD_HOST", SqlDbType.VarChar, 100)
            pParms(3).Value = p_AUD_HOST
            pParms(4) = New SqlClient.SqlParameter("@AUD_WINUSER", SqlDbType.VarChar, 100)
            pParms(4).Value = p_AUD_WINUSER
            pParms(5) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
            pParms(5).Direction = ParameterDirection.ReturnValue
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(p_str_conn, CommandType.StoredProcedure, "TRANSPORT.GetUserForOnlinePayment", pParms)
            If pParms(5).Value = "0" Then
                dtUserData = ds.Tables(0)
            End If
            GetUserForOnlinePayment = pParms(5).Value
        Catch ex As Exception
            Errorlog(ex.Message)
            GetUserForOnlinePayment = "1000"
        End Try
    End Function


    Public Shared Function UpdateStudentPassword(ByVal p_STU_OLD_PASSWORD As String, ByVal p_STU_ID As String, _
     ByVal p_STU_PASSWORD As String, ByVal trans As SqlTransaction) As Integer
        Dim pParms(3) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@STU_OLD_PASSWORD", p_STU_OLD_PASSWORD)
        pParms(1) = New SqlClient.SqlParameter("@STU_ID", p_STU_ID)
        pParms(2) = New SqlClient.SqlParameter("@STU_PASSWORD", p_STU_PASSWORD)
        pParms(3) = New SqlClient.SqlParameter("@RETURN_VALUE", SqlDbType.Int)
        pParms(3).Direction = ParameterDirection.ReturnValue
        SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "TRANSPORT.UpdateStudentPassword", pParms)
        Dim ReturnFlag As Integer = pParms(3).Value
        Return ReturnFlag
    End Function


    Public Shared Function F_GetFeeDetailsForCollectionTransport(ByVal p_DOCDT As String, ByVal p_STU_ID As String, _
  ByVal p_STU_TYPE As String, ByVal BSU_ID As String, ByVal ACD_ID As String) As DataTable
        Dim pParms(5) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@DOCDT", SqlDbType.DateTime)
        pParms(0).Value = p_DOCDT
        pParms(1) = New SqlClient.SqlParameter("@STU_ID", SqlDbType.BigInt)
        pParms(1).Value = p_STU_ID
        pParms(2) = New SqlClient.SqlParameter("@STU_TYPE", SqlDbType.VarChar, 2)
        pParms(2).Value = p_STU_TYPE
        pParms(3) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(3).Value = BSU_ID
        pParms(4) = New SqlClient.SqlParameter("@ACD_ID", SqlDbType.Int)
        pParms(4).Value = ACD_ID
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString, _
          CommandType.StoredProcedure, "FEES.F_GetFeeDetailsForCollectionTransport", pParms)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function

    Public Shared Function F_GetFeeDetailsForCollection(ByVal p_DOCDT As String, ByVal p_STU_ID As String, _
 ByVal p_STU_TYPE As String, ByVal BSU_ID As String, ByVal ACD_ID As String) As DataTable
        Dim pParms(4) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@DOCDT", SqlDbType.DateTime)
        pParms(0).Value = p_DOCDT
        pParms(1) = New SqlClient.SqlParameter("@STU_ID", SqlDbType.BigInt)
        pParms(1).Value = p_STU_ID
        pParms(2) = New SqlClient.SqlParameter("@STU_TYPE", SqlDbType.VarChar, 2)
        pParms(2).Value = p_STU_TYPE
        pParms(3) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(3).Value = BSU_ID
        'pParms(4) = New SqlClient.SqlParameter("@ACD_ID", SqlDbType.Int)
        'pParms(4).Value = ACD_ID
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(WebConfigurationManager.ConnectionStrings("OASIS_FEESConnectionString").ConnectionString, _
          CommandType.StoredProcedure, "FEES.F_GetFeeDetailsFOrCollection", pParms)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function

    Public Shared Function F_GetFeeCollectionHistory(ByVal BSU_ID As String, ByVal p_FROMDT As String, ByVal p_TODT As String, _
  ByVal STU_ID As String) As DataTable
        Dim pParms(4) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BSUID", SqlDbType.VarChar, 20)
        pParms(0).Value = BSU_ID
        pParms(1) = New SqlClient.SqlParameter("@FRMDT", SqlDbType.VarChar, 20)
        pParms(1).Value = p_FROMDT
        pParms(2) = New SqlClient.SqlParameter("@TODT", SqlDbType.VarChar, 20)
        pParms(2).Value = p_TODT
        pParms(3) = New SqlClient.SqlParameter("@STUID", SqlDbType.VarChar, 20)
        pParms(3).Value = STU_ID

        Dim dsData As DataSet = SqlHelper.ExecuteDataset(WebConfigurationManager.ConnectionStrings("OASIS_FEESConnectionString").ConnectionString, _
          CommandType.StoredProcedure, "[FEES].[FEE_GETPAYMENTHISTORY]", pParms)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function


    Public Shared Function F_GetFeeNarration(ByVal p_DOCDT As String, _
       ByVal p_ACD_ID As String, ByVal p_BSU_ID As String) As String
        '[FEES].[F_GetFeeNarration] 
        '@DOCDT varchar(12),
        '@BSU_ID varchar(20),
        '@ACD_ID bigint  
        Dim pParms(3) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@DOCDT", SqlDbType.DateTime)
        pParms(0).Value = p_DOCDT
        pParms(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(1).Value = p_BSU_ID
        pParms(2) = New SqlClient.SqlParameter("@ACD_ID", SqlDbType.BigInt)
        pParms(2).Value = p_ACD_ID
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString, _
          CommandType.StoredProcedure, "ONLINE.F_GetFeeNarration", pParms)
        If dsData.Tables(0).Rows.Count > 0 Then
            Return dsData.Tables(0).Rows(0)(0).ToString
        Else
            Return ""
        End If
    End Function

    Public Shared Function Get_PaidDetails(ByVal FCO_ID As Int64, ByVal bPaymentSuccess As Boolean) As DataTable
        Try
            'Dim strPath As String = Convert.ToString(readerStudent_Detail("STU_PHOTOPATH"))
            Dim connPath As String = WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ToString()
            Dim noImagePath As String = HttpContext.Current.Server.MapPath("Images\no_image.gif")
            Dim con As String = ConnectionManger.GetOASISTRANSPORTConnectionString
            Dim param(4) As SqlClient.SqlParameter

            param(0) = New SqlClient.SqlParameter("@IMAGE_PATH", connPath)
            param(1) = New SqlClient.SqlParameter("@noImagePath", noImagePath)
            param(2) = New SqlClient.SqlParameter("@FCO_ID", FCO_ID)
            param(3) = New SqlClient.SqlParameter("@bPaymentSuccess", bPaymentSuccess)

            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "TRANSPORT.[GET_SIBLINGS_PAYMENT_RESULT]", param)

            Return ds.Tables(0)
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    Public Shared Function GetSECURE_SECRET(ByVal BSU_ID As String) As String
        Dim str_qurey As String = "SELECT BSU_SECURE_SECRET " & _
        " FROM BUSINESSUNIT_M where BSU_ID='" & BSU_ID & "'"

        Dim str_data As String = UtilityObj.GetDataFromSQL(str_qurey, _
            ConnectionManger.GetOASISConnectionString)
        If str_data <> "--" Then
            Return str_data
        Else
            Return ""
        End If
    End Function

    Public Shared Function F_SaveFEECOLLECTION_H_ONLINE_PAYMENT_MULTI(ByVal p_FCO_ID As String, ByVal p_FCO_VPC_RESPONCECODE As String, _
       ByVal p_FCO_VPC_RESPONCEDESCR As String, ByVal p_FCO_VPC_MESSAGE As String, ByVal p_FCO_VPC_RECNO As String, _
       ByVal p_FCO_VPC_TRANNO As String, ByVal p_FCO_VPC_ACQRESCODE As String, ByVal p_FCO_VPC_BANKAUTID As String, _
       ByVal p_FCO_VPC_BATCHNO As String, ByVal p_FCO_VPC_CARDTYPE As String, ByVal p_FCO_VPC_HASHCODE_RESULT As String, _
       ByVal p_FCO_VPC_AMOUNT As String, ByVal p_FCO_VPC_ORDERINFO As String, ByVal p_FCO_VPC_MERCHANT_ID As String, _
       ByVal p_FCO_VPC_COMMAD As String, ByVal p_FCO_VPC_VERSION As String, ByVal p_FCO_VPC_3DS_INFO As String, _
       ByRef FCL_RECNO As String, ByRef RESPONSE As String) As String
        Dim pParms(19) As SqlClient.SqlParameter
        ''''FCO_ID 
        ''''FCO_VPC_RESPONCECODE 
        ''''FCO_VPC_RESPONCEDESCR 
        ''''FCO_VPC_MESSAGE 
        ''''FCO_VPC_RECNO 
        pParms(0) = New SqlClient.SqlParameter("@FCO_ID", SqlDbType.BigInt)
        pParms(0).Value = p_FCO_ID
        pParms(1) = New SqlClient.SqlParameter("@FCO_VPC_RESPONCECODE", SqlDbType.VarChar, 200)
        pParms(1).Value = p_FCO_VPC_RESPONCECODE
        pParms(2) = New SqlClient.SqlParameter("@FCO_VPC_RESPONCEDESCR", SqlDbType.VarChar, 200)
        pParms(2).Value = p_FCO_VPC_RESPONCEDESCR
        pParms(3) = New SqlClient.SqlParameter("@FCO_VPC_MESSAGE", SqlDbType.VarChar, 200)
        pParms(3).Value = p_FCO_VPC_MESSAGE
        pParms(4) = New SqlClient.SqlParameter("@FCO_VPC_RECNO", SqlDbType.VarChar, 200)
        pParms(4).Value = p_FCO_VPC_RECNO
        ''''FCO_VPC_TRANNO 
        ''''FCO_VPC_ACQRESCODE 
        ''''FCO_VPC_BANKAUTID 
        ''''RECNO = objCmdUpdatePayment("@FCL_RECNO")
        ''''RESP = objCmdUpdatePayment("@RESPONSE")
        pParms(5) = New SqlClient.SqlParameter("@FCO_VPC_TRANNO", SqlDbType.VarChar, 200)
        pParms(5).Value = p_FCO_VPC_TRANNO
        pParms(6) = New SqlClient.SqlParameter("@FCO_VPC_ACQRESCODE", SqlDbType.VarChar, 200)
        pParms(6).Value = p_FCO_VPC_ACQRESCODE
        pParms(7) = New SqlClient.SqlParameter("@FCO_VPC_BANKAUTID", SqlDbType.VarChar, 200)
        pParms(7).Value = p_FCO_VPC_BANKAUTID

        pParms(8) = New SqlClient.SqlParameter("@FCL_RECNO", SqlDbType.VarChar, 200)
        pParms(8).Direction = ParameterDirection.Output
        pParms(9) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(9).Direction = ParameterDirection.ReturnValue
        pParms(10) = New SqlClient.SqlParameter("@RESPONSE", SqlDbType.VarChar, 200)
        pParms(10).Direction = ParameterDirection.Output
        ''''FCO_VPC_BATCHNO 
        ''''FCO_VPC_CARDTYPE 
        ''''FCO_VPC_HASHCODE_RESULT 
        ''''FCO_VPC_AMOUNT 
        pParms(11) = New SqlClient.SqlParameter("@FCO_VPC_BATCHNO", SqlDbType.VarChar, 200)
        pParms(11).Value = p_FCO_VPC_BATCHNO
        pParms(12) = New SqlClient.SqlParameter("@FCO_VPC_CARDTYPE", SqlDbType.VarChar, 200)
        pParms(12).Value = p_FCO_VPC_CARDTYPE
        pParms(13) = New SqlClient.SqlParameter("@FCO_VPC_HASHCODE_RESULT", SqlDbType.VarChar, 200)
        pParms(13).Value = p_FCO_VPC_HASHCODE_RESULT
        pParms(14) = New SqlClient.SqlParameter("@FCO_VPC_AMOUNT", SqlDbType.VarChar, 200)
        pParms(14).Value = p_FCO_VPC_AMOUNT
        ''''FCO_VPC_ORDERINFO 
        ''''FCO_VPC_MERCHANT_ID 
        ''''FCO_VPC_COMMAD 
        ''''FCO_VPC_VERSION 
        ''''FCO_VPC_3DS_INFO 
        pParms(15) = New SqlClient.SqlParameter("@FCO_VPC_ORDERINFO", SqlDbType.VarChar, 200)
        pParms(15).Value = p_FCO_VPC_ORDERINFO
        pParms(16) = New SqlClient.SqlParameter("@FCO_VPC_MERCHANT_ID", SqlDbType.VarChar, 200)
        pParms(16).Value = p_FCO_VPC_MERCHANT_ID
        pParms(17) = New SqlClient.SqlParameter("@FCO_VPC_COMMAD", SqlDbType.VarChar, 200)
        pParms(17).Value = p_FCO_VPC_COMMAD
        pParms(18) = New SqlClient.SqlParameter("@FCO_VPC_VERSION", SqlDbType.VarChar, 200)
        pParms(18).Value = p_FCO_VPC_VERSION
        pParms(19) = New SqlClient.SqlParameter("@FCO_VPC_3DS_INFO", SqlDbType.VarChar, 200)
        pParms(19).Value = p_FCO_VPC_3DS_INFO

        Dim objConn As New SqlConnection(ConnectionManger.GetOASISTRANSPORTConnectionString) '
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try
            SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "[TRANSPORT].[F_SaveFEECOLLECTION_H_ONLINE_PAYMENT_MULTI]", pParms)
            If pParms(9).Value = 0 Then
                FCL_RECNO = pParms(8).Value
                RESPONSE = pParms(10).Value
                stTrans.Commit()
            Else
                stTrans.Rollback()
            End If
        Catch ex As Exception
            stTrans.Rollback()
            Errorlog(ex.Message)
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
        F_SaveFEECOLLECTION_H_ONLINE_PAYMENT_MULTI = pParms(9).Value
    End Function

    Public Shared Sub SAVE_ONLINE_PAYMENT_AUDIT(ByVal p_OPA_FROM As String, ByVal p_OPA_ACTION As String, _
      ByVal p_OPA_DATA As String, ByVal p_OPA_FOC_ID As String)
        Dim pParms(3) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@OPA_FROM", p_OPA_FROM)
        pParms(1) = New SqlClient.SqlParameter("@OPA_ACTION", p_OPA_ACTION)
        pParms(2) = New SqlClient.SqlParameter("@OPA_DATA", p_OPA_DATA)
        pParms(3) = New SqlClient.SqlParameter("@OPA_FOC_ID", p_OPA_FOC_ID)
        SqlHelper.ExecuteNonQuery(ConnectionManger.GetOASISAuditConnectionString, CommandType.StoredProcedure, "FEES.SAVE_ONLINE_PAYMENT_AUDIT", pParms)
    End Sub
End Class
