Imports Microsoft.VisualBasic
Imports System.Net.Mail
Imports System.Data.SqlClient
Imports System.Configuration

Public Class eMailReceipt

    Public Shared Function convertToEmbedResource(ByVal emailHtml$, ByVal BSU_ID As String) As AlternateView
        On Error Resume Next
        'This is the website where the resources are located
        Dim Encr_decrData As New Encryption64
        Dim matchesCol As System.Text.RegularExpressions.MatchCollection = System.Text.RegularExpressions.Regex.Matches(emailHtml, "url\(['|\""]+.*['|\""]\)|src=[""|'][^""']+[""|']")
        Dim normalRes As System.Text.RegularExpressions.Match
        Dim resCol As AlternateView = AlternateView.CreateAlternateViewFromString("", Nothing, "text/html")
        Dim resId% = 0
        ' Between the findings
        For Each normalRes In matchesCol
            Dim resPath$
            ' Replace it for the new content ID that will be embeded
            If normalRes.Value.Substring(0, 3) = "url" Then
                emailHtml = emailHtml.Replace(normalRes.Value, "url(cid:EmbedRes_" & resId & ")")
            Else
                emailHtml = emailHtml.Replace(normalRes.Value, "src=""cid:EmbedRes_" & resId & """")
            End If
            ' Clean the path
            resPath = System.Text.RegularExpressions.Regex.Replace(normalRes.Value, "url\(['|\""]", "")
            resPath = System.Text.RegularExpressions.Regex.Replace(resPath, "src=['|\""]", "")
            resPath = System.Text.RegularExpressions.Regex.Replace(resPath, "['|\""]\)", "").Replace("""", "")
            Dim serverpath As String = ConfigurationManager.AppSettings("ReceiptImagePath")
            ' Map it on the server
            ''resPath = Server.MapPath(resPath)
            resPath = serverpath + resPath.Replace("..", "")
            resPath = resPath.Replace("%20", " ")
            resPath = resPath.Replace("/", "\")
            ' Embed the resource
            If resPath.LastIndexOf("http://") = -1 Then
                If resPath.IndexOf("?BSU_ID=") > 0 Then
                    Dim connectionstring As String = ConnectionManger.GetOASISConnectionString
                    Dim SqlText As String = "SELECT IMG_IMAGE FROM OASIS.dbo.BSU_IMAGES WHERE IMG_BSU_ID='" & BSU_ID & "' AND IMG_TYPE='LOGO'"
                    Dim connection As New SqlConnection(connectionstring)
                    Dim command As New SqlCommand(SqlText, connection)
                    'open the database and get a datareader
                    connection.Open()
                    Dim dr As SqlDataReader = command.ExecuteReader()
                    If dr.Read() Then
                        Dim strMem As System.IO.MemoryStream = New System.IO.MemoryStream(DirectCast(dr("IMG_IMAGE"), Byte()))
                        Dim strWriter As System.IO.StreamWriter = New System.IO.StreamWriter(strMem)
                        strWriter.Flush()
                        'this is very important..wont work without it
                        strMem.Position = 0
                        Dim theResource As LinkedResource = New LinkedResource(strMem)
                        theResource.ContentId = "EmbedRes_" & resId
                        resCol.LinkedResources.Add(theResource)
                    End If
                    connection.Close()
                Else
                    Dim theResource As LinkedResource = New LinkedResource(resPath)
                    theResource.ContentId = "EmbedRes_" & resId
                    resCol.LinkedResources.Add(theResource)
                End If
            End If
            ' Next resource ID
            resId = resId + 1
        Next

        ' Create our final object
        Dim finalEmail As AlternateView = Net.Mail.AlternateView.CreateAlternateViewFromString(emailHtml, Nothing, "text/html")
        Dim transferResource As LinkedResource
        ' And transfer all the added resources to the output object
        For Each transferResource In resCol.LinkedResources
            finalEmail.LinkedResources.Add(transferResource)
        Next
        Return finalEmail
    End Function

    Public Shared Function SendNewsLetters(ByVal FromEmailId As String, ByVal ToEmailId As String, _
    ByVal Subject As String, ByVal MailBody As AlternateView, ByVal Username As String, _
    ByVal password As String, ByVal Host As String, ByVal Port As Integer) As String
        Try
            Dim msg As New System.Net.Mail.MailMessage(FromEmailId, ToEmailId)
            msg.Subject = Subject
            msg.AlternateViews.Add(MailBody)
            msg.Priority = Net.Mail.MailPriority.Normal
            msg.IsBodyHtml = True
            Dim client As New System.Net.Mail.SmtpClient(Host, Port)
            If Username <> "" And password <> "" Then
                Dim creds As New System.Net.NetworkCredential(Username, password)
                client.Credentials = creds
            End If
            ' or for other authentication with local server
            'client.Credentials= system.Net.CredentialCache.DefaultCredentials
            'client.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials
            ' Async send
            'Dim MailToken As String = My.User.Name
            'client.SendAsync(msg, emaildid)
            'AddHandler client.SendCompleted, AddressOf Me.MailSendComplete
            client.Send(msg)
            Return "Successfully sent"
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "ONLINE RECEIPT - MAIL")
            Return "Error : " & ex.Message
        End Try
    End Function
    Public Shared Function SendNewsLetters(ByVal FromEmailId As String, ByVal ToEmailId As String, _
             ByVal Subject As String, ByVal MailBody As String, ByVal Username As String, _
             ByVal password As String, ByVal Host As String, ByVal Port As Integer, Optional ByVal AttachmentFilePath As String = "") As String
        Try
            Dim msg As New System.Net.Mail.MailMessage(FromEmailId, ToEmailId)
            msg.Subject = Subject
            msg.Body = MailBody
            msg.Priority = Net.Mail.MailPriority.Normal
            msg.IsBodyHtml = True
            Dim client As New System.Net.Mail.SmtpClient(Host, Port)
            If Username <> "" And password <> "" Then
                Dim creds As New System.Net.NetworkCredential(Username, password)
                client.Credentials = creds
            End If
            If AttachmentFilePath <> "" Then
                Dim attach As New System.Net.Mail.Attachment(AttachmentFilePath)
                msg.Attachments.Add(attach)
            End If

            ' or for other authentication with local server
            'client.Credentials= system.Net.CredentialCache.DefaultCredentials
            'client.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials
            ' Async send
            'Dim MailToken As String = My.User.Name
            'client.SendAsync(msg, emaildid)
            'AddHandler client.SendCompleted, AddressOf Me.MailSendComplete
            client.Send(msg)
            Return "Successfully sent"
        Catch ex As Exception
            UtilityObj.Errorlog(ex.Message, "ONLINE RECEIPT - MAIL")
            Return "Error : " & ex.Message
        End Try
    End Function
End Class
