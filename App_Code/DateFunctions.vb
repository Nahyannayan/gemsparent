Imports Microsoft.VisualBasic
Imports System
Imports System.Web.UI.WebControls

Public Class DateFunctions

    Public Shared Function ConvertoDate(ByVal dateString As String, ByRef result As DateTime) As Boolean
        Try
            Dim supportedFormats() As String = New String() { _
                    "d/MM/yyyy", _
                    "d/MM/yy", _
                    "d/M/yyyy", _
                    "d/M/yy", _
                    "d-M-yyyy", _
                    "d M yyyy", _
                    "d-MM-yyyy", _
                    "d MM yyyy", _
                    "dd/M/yyyy", _
                    "dd/M/yy", _
                    "dd-M-yyyy", _
                    "dd M yyyy", _
                    "dd/MM/yyyy", _
                    "dd/MM/yy", _
                    "dd/M/yyyy", _
                    "dd-MM-yyyy", _
                    "dd MM yyyy", _
                    "d-MMM-yyyy", _
                    "d MMM yyyy", _
                    "d/MMM/yyyy", _
                    "d/MMM/yy", _
                    "dd-MMM-yyyy", _
                    "dd MMM yyyy", _
                    "dd/MMM/yyyy", _
                    "dd/MMM/yy", _
                    "dMMMyyyy" _
                    }
            result = Date.ParseExact(dateString, supportedFormats, System.Globalization.CultureInfo.CurrentCulture, System.Globalization.DateTimeStyles.NoCurrentDateDefault)
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Shared Function checkdates(ByRef p_date_from As String, ByRef p_date_to As String) As String
        Dim dFrom, dTo As Date
        If ConvertoDate(p_date_from, dFrom) = False Then
            Return "Invalid from date"
        End If
        If ConvertoDate(p_date_to, dTo) = False Then
            Return "Invalid to date"
        End If

        If dFrom > Now.Date Or dTo > Now.Date Then
            Return "Future dates not allowed"
        End If
        If dFrom > dTo Then
            Return "Invalid dates"
        Else
            p_date_from = String.Format("{0:dd/MMM/yyyy}", dFrom)
            p_date_to = String.Format("{0:dd/MMM/yyyy}", dTo)
            Return ""
        End If
    End Function

    Public Shared Function checkdate_nofuture(ByRef p_date As String) As String
        Dim dFrom As Date
        If ConvertoDate(p_date, dFrom) = False Then
            Return "Invalid date"
        End If
        If dFrom > Now.Date Then
            Return "Inavlid date (Future Date)"
        Else
            p_date = String.Format("{0:dd/MMM/yyyy}", dFrom)
            Return ""
        End If
    End Function

    Public Shared Function checkdate(ByRef p_date As String) As String
        Dim dFrom As Date
        If ConvertoDate(p_date, dFrom) = False Then
            Return "Invalid from date"
        End If
        If SQLDateTime(p_date) = False Then
            Return "Invalid from date"
        End If
        p_date = String.Format("{0:dd/MMM/yyyy}", dFrom)
        Return ""
    End Function
    Public Shared Function SQLDateTime(ByVal pdate As String) As Boolean
        Try

            Dim dt As System.Data.SqlTypes.SqlDateTime
            dt = pdate
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function
    Public Shared Function checkdates_canfuture(ByRef p_date_from As String, ByRef p_date_to As String) As String
        Dim dFrom, dTo As Date
        If ConvertoDate(p_date_from, dFrom) = False Then
            Return "Invalid from date"
        End If
        If ConvertoDate(p_date_to, dTo) = False Then
            Return "Invalid to date"
        End If
        If dFrom > dTo Then
            Return "Invalid dates"
        Else
            p_date_from = String.Format("{0:dd/MMM/yyyy}", dFrom)
            p_date_to = String.Format("{0:dd/MMM/yyyy}", dTo)
            Return ""
        End If
    End Function

    Public Shared Sub CorrectDateFormat(ByVal sender As Object, ByRef lblError As Label, _
    ByVal AllowsFuture As Boolean, Optional ByVal ErrorMessage As String = "Invalid Date !!!")
        If IsDate(sender.Text) Then
            Dim dFrom As Date
            If ConvertoDate(sender.Text, dFrom) = False Then
                lblError.Text = "Invalid from date"
                Exit Sub
            End If
            sender.Text = String.Format("{0:dd/MMM/yyyy}", dFrom)
            If Not AllowsFuture And dFrom > Now.Date Then
                lblError.Text = "Inavlid date (Future Date)"
            End If
        Else
            lblError.Text = ErrorMessage
        End If
    End Sub
    Public Shared Function EndDateofMonth(ByRef p_date As String) As String
        Dim ddate As DateTime
        p_date = StartDateofMonth(p_date)
        ddate = Convert.ToDateTime(p_date)
        ddate = ddate.AddMonths(1)
        ddate = ddate.AddDays(-1)
        p_date = ddate.ToString("dd/MMM/yyyy")
        Return p_date

    End Function
    Public Shared Function StartDateofMonth(ByRef p_date As String) As String
        Dim lastDate As String = "01" & "/" & MonthName(Month(Convert.ToDateTime(p_date)), True) & "/" & Year(Convert.ToDateTime(p_date)).ToString()
        p_date = Convert.ToDateTime(lastDate).ToString("dd/MMM/yyyy")
        Return p_date
    End Function
End Class
