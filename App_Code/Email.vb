Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Threading
Imports System.IO
Imports System.Net.Mail

Namespace EmailService

    Public Class Email
        Public Shared Function SendNewsLetters(ByVal FromEmailId As String, ByVal ToEmailId As String, ByVal Subject As String, ByVal MailBody As AlternateView, ByVal Username As String, ByVal password As String, ByVal Host As String, ByVal Port As Integer, ByVal Templateid As String, ByVal HasAttachments As Boolean) As String
            Dim RetutnValue = ""
            Try

                Dim msg As New System.Net.Mail.MailMessage(FromEmailId, ToEmailId)

                msg.Subject = Subject

                msg.AlternateViews.Add(MailBody)

                msg.Priority = Net.Mail.MailPriority.High

                msg.IsBodyHtml = True

                '' If Attachments.

                If HasAttachments Then

                    Dim serverpath As String = System.Configuration.ConfigurationManager.AppSettings("EmailAttachments").ToString()

                    Dim d As New DirectoryInfo(serverpath + Templateid + "/Attachments/")
                    Dim fi() As System.IO.FileInfo
                    fi = d.GetFiles("*.*", SearchOption.TopDirectoryOnly)
                    If fi.Length > 0 Then '' If Having Attachments files
                        For Each f As System.IO.FileInfo In fi
                            Dim attach As New System.Net.Mail.Attachment(serverpath + Templateid + "/Attachments/" + f.Name)
                            attach.Name = f.Name
                            msg.Attachments.Add(attach)
                        Next
                    End If
                End If

                Dim client As New System.Net.Mail.SmtpClient(Host, Port)

                If Username <> "" And password <> "" Then
                    Dim creds As New System.Net.NetworkCredential(Username, password)
                    client.Credentials = creds
                End If


                ' or for other authentication with local server

                'client.Credentials= system.Net.CredentialCache.DefaultCredentials

                'client.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials

                ' Async send

                'Dim MailToken As String = My.User.Name

                'client.SendAsync(msg, emaildid)

                'AddHandler client.SendCompleted, AddressOf Me.MailSendComplete

                client.Send(msg)

                RetutnValue = "Successfully sent"

            Catch ex As Exception
                RetutnValue = "Error : " & ex.Message
            End Try

            Return RetutnValue

        End Function
        Public Shared Function SendPlainTextEmails(ByVal FromEmailId As String, ByVal ToEmailId As String, ByVal Subject As String, ByVal MailBody As String, ByVal Username As String, ByVal password As String, ByVal Host As String, ByVal Port As Integer, ByVal Templateid As String, ByVal HasAttachments As Boolean) As String
            Dim RetutnValue = ""
            Try

                Dim msg As New System.Net.Mail.MailMessage(FromEmailId, ToEmailId)

                msg.Subject = Subject

                msg.Body = MailBody

                msg.Priority = Net.Mail.MailPriority.High

                msg.IsBodyHtml = True

                '' If Attachments.

                If HasAttachments Then
                    Dim serverpath As String = System.Configuration.ConfigurationManager.AppSettings("EmailAttachments").ToString
                    Dim d As New DirectoryInfo(serverpath + Templateid + "/Attachments/")
                    Dim fi() As System.IO.FileInfo
                    fi = d.GetFiles("*.*", SearchOption.TopDirectoryOnly)
                    If fi.Length > 0 Then '' If Having Attachments files
                        For Each f As System.IO.FileInfo In fi
                            Dim attach As New System.Net.Mail.Attachment(serverpath + Templateid + "/Attachments/" + f.Name)
                            attach.Name = f.Name
                            msg.Attachments.Add(attach)
                        Next
                    End If
                End If

                Dim client As New System.Net.Mail.SmtpClient(Host, Port)

                If Username <> "" And password <> "" Then
                    Dim creds As New System.Net.NetworkCredential(Username, password)
                    client.Credentials = creds
                End If


                ' or for other authentication with local server

                'client.Credentials= system.Net.CredentialCache.DefaultCredentials

                'client.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials

                ' Async send

                'Dim MailToken As String = My.User.Name

                'client.SendAsync(msg, emaildid)

                'AddHandler client.SendCompleted, AddressOf Me.MailSendComplete

                client.Send(msg)

                RetutnValue = "Successfully sent"

            Catch ex As Exception
                RetutnValue = "Error : " & ex.Message
            End Try

            Return RetutnValue

        End Function

    End Class

End Namespace

