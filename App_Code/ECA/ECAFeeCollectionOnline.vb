﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports System.Collections.Generic
Imports System.Xml
Imports UtilityObj
Imports System

Public Class ECAFeeCollectionOnline
    Public Shared Function F_SaveFEECOLLECTION_H_ONLINE(ByVal p_FCO_ID As Integer, ByVal p_FCO_SOURCE As String,
        ByVal p_FCO_DATE As DateTime, ByVal p_FCO_ACD_ID As Integer, ByVal p_FCO_STU_ID As String, ByVal p_FCO_STU_TYPE As String,
        ByVal p_FCO_AMOUNT As Decimal, ByVal p_FCO_Bposted As Boolean, ByRef p_newFCO_ID As Integer,
        ByVal p_FCO_BSU_ID As String, ByVal p_FCO_NARRATION As String, ByVal p_FCO_DRCR As String,
        ByVal p_FCO_REFNO As String, ByVal p_FCO_IP_ADDRESS As String, ByVal p_FCO_CPS_ID As String, ByVal p_FCO_SSR_IDS As String, ByVal p_stTrans As SqlTransaction) As String
        Dim pParms(16) As SqlClient.SqlParameter
        'FEES.F_SaveFEECOLLECTION_H_ONLINE  
        ',@FCO_ID  VARCHAR(20)
        '@FCO_SOURCE VARCHAR(20)
        ',@FCO_RECNO VARCHAR(20)
        ',@FCO_DATE DATETIME 
        ',@FCO_ACD_ID BIGINT  
        pParms(0) = New SqlClient.SqlParameter("@FCO_ID", SqlDbType.BigInt)
        pParms(0).Value = p_FCO_ID
        pParms(1) = New SqlClient.SqlParameter("@FCO_SOURCE", SqlDbType.VarChar, 100)
        pParms(1).Value = p_FCO_SOURCE
        pParms(2) = New SqlClient.SqlParameter("@FCO_DRCR", SqlDbType.VarChar, 100)
        pParms(2).Value = p_FCO_DRCR
        pParms(3) = New SqlClient.SqlParameter("@FCO_DATE", SqlDbType.DateTime)
        pParms(3).Value = p_FCO_DATE
        pParms(4) = New SqlClient.SqlParameter("@FCO_ACD_ID", SqlDbType.BigInt)
        pParms(4).Value = p_FCO_ACD_ID
        ',@FCO_STU_ID BIGINT 
        ',@FCO_AMOUNT NUMERIC(18,3) 
        ',@FCO_Bposted BIT 
        pParms(5) = New SqlClient.SqlParameter("@FCO_STU_ID", SqlDbType.Int)
        pParms(5).Value = p_FCO_STU_ID
        pParms(6) = New SqlClient.SqlParameter("@FCO_AMOUNT", SqlDbType.Decimal, 21)
        pParms(6).Value = p_FCO_AMOUNT
        pParms(7) = New SqlClient.SqlParameter("@FCO_REFNO", SqlDbType.VarChar, 20)
        pParms(7).Value = p_FCO_REFNO
        pParms(8) = New SqlClient.SqlParameter("@FCO_Bposted", SqlDbType.Bit)
        pParms(8).Value = p_FCO_Bposted
        pParms(9) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(9).Direction = ParameterDirection.ReturnValue
        ',@FCO_NARRATION VARCHAR(20)
        ',@FCO_DRCR VARCHAR(20)  
        ',@FCO_BSU_ID VARCHAR(20) 
        ',@NEW_FCO_ID INT OUTPUT
        ',@FCO_REFNO VARCHAR(20)
        pParms(10) = New SqlClient.SqlParameter("@NEW_FCO_ID", SqlDbType.BigInt)
        pParms(10).Direction = ParameterDirection.Output
        pParms(11) = New SqlClient.SqlParameter("@FCO_BSU_ID", SqlDbType.VarChar, 100)
        pParms(11).Value = p_FCO_BSU_ID
        pParms(12) = New SqlClient.SqlParameter("@FCO_NARRATION", SqlDbType.VarChar, 100)
        pParms(12).Value = p_FCO_NARRATION
        pParms(13) = New SqlClient.SqlParameter("@FCO_IP_ADDRESS", SqlDbType.VarChar, 100)
        pParms(13).Value = p_FCO_IP_ADDRESS
        pParms(14) = New SqlClient.SqlParameter("@FCO_STU_TYPE", SqlDbType.VarChar, 2)
        pParms(14).Value = p_FCO_STU_TYPE
        pParms(15) = New SqlClient.SqlParameter("@FCO_CPS_ID", SqlDbType.Int)
        pParms(15).Value = p_FCO_CPS_ID

        pParms(16) = New SqlClient.SqlParameter("@FCO_SSR_IDS", SqlDbType.VarChar, 300)
        pParms(16).Value = p_FCO_SSR_IDS


        SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "FEES.F_SaveFEECOLLECTION_H_ONLINE", pParms)
        If pParms(9).Value = 0 Then
            p_newFCO_ID = pParms(10).Value
        End If
        F_SaveFEECOLLECTION_H_ONLINE = pParms(9).Value
    End Function

    Public Shared Function F_SaveFEECOLLSUB_ONLINE(ByVal p_FSO_ID As Integer, ByVal p_FSO_FCO_ID As Integer, _
        ByVal p_FSO_FEE_ID As Integer, ByVal p_FSO_AMOUNT As Decimal, ByVal p_FSO_FSH_ID As String, _
        ByVal p_FSO_ORG_AMOUNT As String, ByVal p_FCO_DISCOUNT As String, ByVal p_stTrans As SqlTransaction) As String
        Dim pParms(7) As SqlClient.SqlParameter
        '[FEES].[F_SaveFEECOLLSUB_ONLINE] 
        '@FSO_ID int, 
        '@FSO_FCO_ID int, 
        '@FSO_FEE_ID int, 
        '@FSO_AMOUNT numeric (18,3),
        '@FSO_FSH_ID  int  ,
        '@FSO_ORG_AMOUNT  numeric (18,3)
        pParms(0) = New SqlClient.SqlParameter("@FSO_ID", SqlDbType.Int)
        pParms(0).Value = p_FSO_ID
        pParms(1) = New SqlClient.SqlParameter("@FSO_FCO_ID", SqlDbType.Int)
        pParms(1).Value = p_FSO_FCO_ID
        pParms(2) = New SqlClient.SqlParameter("@FSO_FEE_ID", SqlDbType.Int)
        pParms(2).Value = p_FSO_FEE_ID
        pParms(3) = New SqlClient.SqlParameter("@FSO_AMOUNT", SqlDbType.Decimal, 21)
        pParms(3).Value = p_FSO_AMOUNT
        pParms(4) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(4).Direction = ParameterDirection.ReturnValue
        pParms(5) = New SqlClient.SqlParameter("@FSO_FSH_ID", SqlDbType.BigInt)
        pParms(5).Value = p_FSO_FSH_ID
        pParms(6) = New SqlClient.SqlParameter("@FSO_ORG_AMOUNT", SqlDbType.Decimal, 21)
        pParms(6).Value = p_FSO_ORG_AMOUNT
        pParms(7) = New SqlClient.SqlParameter("@FCO_DISCOUNT", SqlDbType.Decimal, 21)
        pParms(7).Value = p_FCO_DISCOUNT

        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "FEES.F_SaveFEECOLLSUB_ONLINE", pParms)
        F_SaveFEECOLLSUB_ONLINE = pParms(4).Value
    End Function

    Public Shared Function F_SaveFEECOLLSUB_D_ONLINE(ByVal p_FDO_ID As Integer, ByVal p_FDO_FCO_ID As String, _
        ByVal p_FDO_CLT_ID As Integer, ByVal p_FDO_AMOUNT As String, ByVal p_FDO_REFNO As String, _
         ByVal p_FDO_DATE As String, ByVal p_FDO_STATUS As String, ByVal p_FDO_VHH_DOCNO As String, _
        ByVal p_FDO_REF_ID As String, ByVal p_FDO_EMR_ID As String, ByVal p_stTrans As SqlTransaction, _
        Optional ByVal p_FDO_CHARGE_CLIENT As Decimal = 0) As String
        Dim pParms(11) As SqlClient.SqlParameter
        '@return_value = [FEES].[F_SaveFEECOLLSUB_D]
        '@FDO_ID = 0,
        '@FDO_FCO_ID = 1,
        '@FDO_CLT_ID = 23,
        '@FDO_AMOUNT = 700,
        '@FDO_REFNO = N'VOUC34',
        pParms(0) = New SqlClient.SqlParameter("@FDO_ID", SqlDbType.Int)
        pParms(0).Value = p_FDO_ID
        pParms(1) = New SqlClient.SqlParameter("@FDO_FCO_ID", SqlDbType.BigInt)
        pParms(1).Value = p_FDO_FCO_ID
        pParms(2) = New SqlClient.SqlParameter("@FDO_CLT_ID", SqlDbType.Int)
        pParms(2).Value = p_FDO_CLT_ID
        pParms(3) = New SqlClient.SqlParameter("@FDO_AMOUNT", SqlDbType.Decimal, 21)
        pParms(3).Value = p_FDO_AMOUNT
        pParms(4) = New SqlClient.SqlParameter("@FDO_REFNO", SqlDbType.VarChar, 20)
        pParms(4).Value = p_FDO_REFNO
        '@FDO_DATE = N'12-MAY-2008',
        '@FDO_STATUS = 1,
        '@FDO_VHH_DOCNO = N'VOUN5666'
        '@FDO_REF_ID = N'VOUN5666', 
        '@FDO_EMR_ID = N'VOUN5666'
        pParms(5) = New SqlClient.SqlParameter("@FDO_DATE", SqlDbType.DateTime)
        pParms(5).Value = p_FDO_DATE
        pParms(6) = New SqlClient.SqlParameter("@FDO_STATUS", SqlDbType.VarChar, SqlDbType.Int)
        pParms(6).Value = p_FDO_STATUS
        pParms(7) = New SqlClient.SqlParameter("@FDO_VHH_DOCNO", SqlDbType.VarChar, 20)
        pParms(7).Value = p_FDO_VHH_DOCNO
        pParms(8) = New SqlClient.SqlParameter("@FDO_REF_ID", SqlDbType.VarChar, 20)
        pParms(8).Value = p_FDO_REF_ID
        pParms(9) = New SqlClient.SqlParameter("@FDO_EMR_ID", SqlDbType.VarChar, 20)
        pParms(9).Value = p_FDO_EMR_ID
        pParms(11) = New SqlClient.SqlParameter("@FDO_CHARGE_CLIENT", SqlDbType.Decimal)
        pParms(11).Value = p_FDO_CHARGE_CLIENT
        pParms(10) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(10).Direction = ParameterDirection.ReturnValue
        Dim retval As Integer
        retval = SqlHelper.ExecuteNonQuery(p_stTrans, CommandType.StoredProcedure, "FEES.F_SaveFEECOLLSUB_D_ONLINE", pParms)
        F_SaveFEECOLLSUB_D_ONLINE = pParms(10).Value
    End Function

    Public Shared Sub SAVE_ONLINE_PAYMENT_AUDIT(ByVal p_OPA_FROM As String, ByVal p_OPA_ACTION As String, _
      ByVal p_OPA_DATA As String, ByVal p_OPA_FOC_ID As String)
        Dim pParms(3) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@OPA_FROM", p_OPA_FROM)
        pParms(1) = New SqlClient.SqlParameter("@OPA_ACTION", p_OPA_ACTION)
        pParms(2) = New SqlClient.SqlParameter("@OPA_DATA", p_OPA_DATA)
        pParms(3) = New SqlClient.SqlParameter("@OPA_FOC_ID", p_OPA_FOC_ID)
        SqlHelper.ExecuteNonQuery(ConnectionManger.GetOASISAuditConnectionString, CommandType.StoredProcedure, "FEES.SAVE_ONLINE_PAYMENT_AUDIT", pParms)
    End Sub

    Public Shared Function F_SaveFEECOLLECTION_H_ONLINE_PAYMENT_FEES(ByVal p_FCO_ID As String, ByVal p_FCO_VPC_RESPONCECODE As String, _
        ByVal p_FCO_VPC_RESPONCEDESCR As String, ByVal p_FCO_VPC_MESSAGE As String, ByVal p_FCO_VPC_RECNO As String, _
        ByVal p_FCO_VPC_TRANNO As String, ByVal p_FCO_VPC_ACQRESCODE As String, ByVal p_FCO_VPC_BANKAUTID As String, _
        ByVal p_FCO_VPC_BATCHNO As String, ByVal p_FCO_VPC_CARDTYPE As String, ByVal p_FCO_VPC_HASHCODE_RESULT As String, _
        ByVal p_FCO_VPC_AMOUNT As String, ByVal p_FCO_VPC_ORDERINFO As String, ByVal p_FCO_VPC_MERCHANT_ID As String, _
        ByVal p_FCO_VPC_COMMAD As String, ByVal p_FCO_VPC_VERSION As String, ByVal p_FCO_VPC_3DS_INFO As String, _
        ByRef FCL_RECNO As String, ByRef RESPONSE As String) As String
        Dim pParms(19) As SqlClient.SqlParameter
        ''''FCO_ID 
        ''''FCO_VPC_RESPONCECODE 
        ''''FCO_VPC_RESPONCEDESCR 
        ''''FCO_VPC_MESSAGE 
        ''''FCO_VPC_RECNO 
        pParms(0) = New SqlClient.SqlParameter("@FCO_ID", SqlDbType.BigInt)
        pParms(0).Value = p_FCO_ID
        pParms(1) = New SqlClient.SqlParameter("@FCO_VPC_RESPONCECODE", SqlDbType.VarChar, 200)
        pParms(1).Value = p_FCO_VPC_RESPONCECODE
        pParms(2) = New SqlClient.SqlParameter("@FCO_VPC_RESPONCEDESCR", SqlDbType.VarChar, 200)
        pParms(2).Value = p_FCO_VPC_RESPONCEDESCR
        pParms(3) = New SqlClient.SqlParameter("@FCO_VPC_MESSAGE", SqlDbType.VarChar, 200)
        pParms(3).Value = p_FCO_VPC_MESSAGE
        pParms(4) = New SqlClient.SqlParameter("@FCO_VPC_RECNO", SqlDbType.VarChar, 200)
        pParms(4).Value = p_FCO_VPC_RECNO
        ''''FCO_VPC_TRANNO 
        ''''FCO_VPC_ACQRESCODE 
        ''''FCO_VPC_BANKAUTID 
        ''''RECNO = objCmdUpdatePayment("@FCL_RECNO")
        ''''RESP = objCmdUpdatePayment("@RESPONSE")
        pParms(5) = New SqlClient.SqlParameter("@FCO_VPC_TRANNO", SqlDbType.VarChar, 200)
        pParms(5).Value = p_FCO_VPC_TRANNO
        pParms(6) = New SqlClient.SqlParameter("@FCO_VPC_ACQRESCODE", SqlDbType.VarChar, 200)
        pParms(6).Value = p_FCO_VPC_ACQRESCODE
        pParms(7) = New SqlClient.SqlParameter("@FCO_VPC_BANKAUTID", SqlDbType.VarChar, 200)
        pParms(7).Value = p_FCO_VPC_BANKAUTID

        pParms(8) = New SqlClient.SqlParameter("@FCL_RECNO", SqlDbType.VarChar, 200)
        pParms(8).Direction = ParameterDirection.Output
        pParms(9) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        pParms(9).Direction = ParameterDirection.ReturnValue
        pParms(10) = New SqlClient.SqlParameter("@RESPONSE", SqlDbType.VarChar, 200)
        pParms(10).Direction = ParameterDirection.Output
        ''''FCO_VPC_BATCHNO 
        ''''FCO_VPC_CARDTYPE 
        ''''FCO_VPC_HASHCODE_RESULT 
        ''''FCO_VPC_AMOUNT 
        pParms(11) = New SqlClient.SqlParameter("@FCO_VPC_BATCHNO", SqlDbType.VarChar, 200)
        pParms(11).Value = p_FCO_VPC_BATCHNO
        pParms(12) = New SqlClient.SqlParameter("@FCO_VPC_CARDTYPE", SqlDbType.VarChar, 200)
        pParms(12).Value = p_FCO_VPC_CARDTYPE
        pParms(13) = New SqlClient.SqlParameter("@FCO_VPC_HASHCODE_RESULT", SqlDbType.VarChar, 200)
        pParms(13).Value = p_FCO_VPC_HASHCODE_RESULT
        pParms(14) = New SqlClient.SqlParameter("@FCO_VPC_AMOUNT", SqlDbType.VarChar, 200)
        pParms(14).Value = p_FCO_VPC_AMOUNT
        ''''FCO_VPC_ORDERINFO 
        ''''FCO_VPC_MERCHANT_ID 
        ''''FCO_VPC_COMMAD 
        ''''FCO_VPC_VERSION 
        ''''FCO_VPC_3DS_INFO 
        pParms(15) = New SqlClient.SqlParameter("@FCO_VPC_ORDERINFO", SqlDbType.VarChar, 200)
        pParms(15).Value = p_FCO_VPC_ORDERINFO
        pParms(16) = New SqlClient.SqlParameter("@FCO_VPC_MERCHANT_ID", SqlDbType.VarChar, 200)
        pParms(16).Value = p_FCO_VPC_MERCHANT_ID
        pParms(17) = New SqlClient.SqlParameter("@FCO_VPC_COMMAD", SqlDbType.VarChar, 200)
        pParms(17).Value = p_FCO_VPC_COMMAD
        pParms(18) = New SqlClient.SqlParameter("@FCO_VPC_VERSION", SqlDbType.VarChar, 200)
        pParms(18).Value = p_FCO_VPC_VERSION
        pParms(19) = New SqlClient.SqlParameter("@FCO_VPC_3DS_INFO", SqlDbType.VarChar, 200)
        pParms(19).Value = p_FCO_VPC_3DS_INFO

        Dim objConn As New SqlConnection(ConnectionManger.GetOASIS_SERVICESConnectionString)
        objConn.Open()
        Dim stTrans As SqlTransaction = objConn.BeginTransaction
        Try
            SqlHelper.ExecuteNonQuery(stTrans, CommandType.StoredProcedure, "[FEES].[F_SaveFEECOLLECTION_H_ONLINE_PAYMENT]", pParms)
            If pParms(9).Value = 0 Then
                FCL_RECNO = pParms(8).Value

                If pParms(10) IsNot Nothing Then
                    RESPONSE = pParms(10).Value.ToString()
                End If

                stTrans.Commit()
            Else
                stTrans.Rollback()
            End If
        Catch ex As Exception
            stTrans.Rollback()
            Errorlog(ex.Message)
        Finally
            If objConn.State = ConnectionState.Open Then
                objConn.Close()
            End If
        End Try
        F_SaveFEECOLLECTION_H_ONLINE_PAYMENT_FEES = pParms(9).Value
    End Function
    Public Shared Function F_GetServiceFeeCollectionHistory(ByVal BSU_ID As String, ByVal p_FROMDT As String, ByVal p_TODT As String, _
     ByVal STU_ID As String) As DataTable
        Dim pParms(4) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BSUID", SqlDbType.VarChar, 20)
        pParms(0).Value = BSU_ID
        pParms(1) = New SqlClient.SqlParameter("@FRMDT", SqlDbType.VarChar, 20)
        pParms(1).Value = p_FROMDT
        pParms(2) = New SqlClient.SqlParameter("@TODT", SqlDbType.VarChar, 20)
        pParms(2).Value = p_TODT
        pParms(3) = New SqlClient.SqlParameter("@STUID", SqlDbType.VarChar, 20)
        pParms(3).Value = STU_ID

        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_SERVICESConnectionString, _
          CommandType.StoredProcedure, "[FEES].[FEE_GETSERVICEPAYMENTHISTORY_NEW]", pParms)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function

End Class
