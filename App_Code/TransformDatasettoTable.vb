Imports Microsoft.VisualBasic
Imports System.Data.DataSet
Imports System.Data
Imports System.IO
Imports System.IO.StringWriter
Imports System.Web.Configuration
Imports System.Web.UI.HtmlTextWriter
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Text
Imports System.Collections.Generic
Imports UtilityObj
Imports System

Public Class TransformDatasettoTable

    Private Shared Function CreateDataTable() As DataTable ' made the jvdetail datatable structure
        Dim dtDt As DataTable
        dtDt = New DataTable
        Try

            Dim cId As New DataColumn("Bsuname", System.Type.GetType("System.String"))
            Dim cAccountid As New DataColumn("Date", System.Type.GetType("System.String"))
            Dim cAccountname As New DataColumn("Amount", System.Type.GetType("System.Decimal"))
            Dim cNarration As New DataColumn("Type", System.Type.GetType("System.String"))
            Dim cNarrations As New DataColumn("sort", System.Type.GetType("System.Int32"))
            Dim cNarrationc As New DataColumn("descr", System.Type.GetType("System.String"))

            dtDt.Columns.Add(cId)
            dtDt.Columns.Add(cAccountid)
            dtDt.Columns.Add(cAccountname)

            dtDt.Columns.Add(cNarration)
            dtDt.Columns.Add(cNarrations)

            dtDt.Columns.Add(cNarrationc)
            Return dtDt
        Catch ex As Exception
            Errorlog(ex.Message, "datatable")
            Return dtDt
        End Try
    End Function
    Public Shared Function TransformDataset(ByVal dtSource As DataTable, ByVal p_date As Date) As DataTable

        Try
            Dim dt As DataTable
            dt = CreateDataTable()

            For i As Integer = 0 To dtSource.Rows.Count - 1
                For j As Integer = 1 To 12
                    Dim dr As DataRow
                    dr = dt.NewRow
                    dr("Bsuname") = dtSource.Rows(i)("BSU_SHORTNAME")
                    ' p_date.AddMonths(j) 
                    dr("Date") = String.Format("{0:dd/MMM/yyyy}", p_date.AddMonths(j))
                    dr("Amount") = dtSource.Rows(i)("MONTH" & j)
                    dr("Type") = dtSource.Rows(i)("TYP")
                    dr("sort") = j
                    dr("descr") = dtSource.Rows(i)("DESCR")
                    dt.Rows.Add(dr)
                Next
            Next
            Return dt
        Catch ex As Exception
            Return Nothing
        Finally

        End Try
    End Function
End Class
