Imports Microsoft.ApplicationBlocks.Data
Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration
Imports System.IO
Imports System.Text
Imports UtilityObj
Imports System

Public Class PaymentCommon


    Public Shared Function SERVICES_BSU_M_ALL(ByVal p_Provider As String) As DataTable
        Dim sql_query As String = " SELECT distinct vw_oso_SERVICES_BSU_M.SVB_BSU_ID, BUSINESSUNIT_M.BSU_NAME" _
            & " FROM vw_oso_SERVICES_BSU_M INNER JOIN BUSINESSUNIT_M " _
            & " ON vw_oso_SERVICES_BSU_M.SVB_BSU_ID = BUSINESSUNIT_M.BSU_ID" _
            & " WHERE (vw_oso_SERVICES_BSU_M.SVB_PROVIDER_BSU_ID = '" & p_Provider & "') AND" _
            & " (vw_oso_SERVICES_BSU_M.SVB_SVC_ID = 1) "
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString, _
        CommandType.Text, sql_query)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function


    Public Shared Function Get_BSU_Currency(ByVal p_BSU_ID As String) As String
        Dim sql_query As String = " SELECT  BSU_CURRENCY  " _
        & " FROM  BUSINESSUNIT_M WHERE BSU_ID='" & p_BSU_ID & "'"
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString, _
        CommandType.Text, sql_query)
        If dsData.Tables(0).Rows.Count > 0 Then
            Return dsData.Tables(0).Rows(0)(0).ToString
        Else
            Return ""
        End If
    End Function

    Public Shared Function Get_BSU_Name(ByVal p_BSU_ID As String) As String
        Dim sql_query As String = " SELECT  BSU_Name  " _
        & " FROM  BUSINESSUNIT_M WHERE BSU_ID='" & p_BSU_ID & "'"
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString, _
        CommandType.Text, sql_query)
        If dsData.Tables(0).Rows.Count > 0 Then
            Return dsData.Tables(0).Rows(0)(0).ToString
        Else
            Return ""
        End If
    End Function


    Public Shared Function GetUserDetailsBB(ByVal p_STU_NO As String, ByVal p_STU_PASSWORD As String, _
        ByVal p_AUD_HOST As String, ByVal p_AUD_WINUSER As String, ByVal p_STU_BSU_ID As String, _
        ByRef dtUserData As DataTable, ByVal stTrans As SqlTransaction) As Integer
        Dim str_conn As String = WebConfigurationManager.ConnectionStrings("OASIS_TRANSPORTConnectionString").ConnectionString
        'DECLARE	@return_value int

        'EXEC	@return_value = [FEES].[GetUserDetailsBB]
        '		@STU_NO = N'11100100005134',
        '		@STU_PASSWORD = N'123',
        '		@AUD_HOST = N'WWW',
        '		@AUD_WINUSER = N'WSE',
        '		@STU_BSU_ID='111001'
        Try

            Dim pParms(5) As SqlClient.SqlParameter
            pParms(0) = New SqlClient.SqlParameter("@STU_NO", SqlDbType.VarChar, 20)
            pParms(0).Value = p_STU_NO
            pParms(1) = New SqlClient.SqlParameter("@STU_PASSWORD", SqlDbType.VarChar, 100)
            pParms(1).Value = p_STU_PASSWORD
            pParms(2) = New SqlClient.SqlParameter("@STU_BSU_ID", SqlDbType.VarChar, 20)
            pParms(2).Value = p_STU_BSU_ID
            pParms(3) = New SqlClient.SqlParameter("@AUD_HOST", SqlDbType.VarChar, 100)
            pParms(3).Value = p_AUD_HOST
            pParms(4) = New SqlClient.SqlParameter("@AUD_WINUSER", SqlDbType.VarChar, 100)
            pParms(4).Value = p_AUD_WINUSER
            pParms(5) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
            pParms(5).Direction = ParameterDirection.ReturnValue

            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(stTrans, CommandType.StoredProcedure, "FEES.GetUserDetailsBB", pParms)
            'retval = SqlHelper.ExecuteNonQuery(objConn, CommandType.StoredProcedure, "GetUserDetails", pParms)
            If pParms(5).Value = "0" Then
                dtUserData = ds.Tables(0)
            End If
            GetUserDetailsBB = pParms(5).Value
        Catch ex As Exception
            Errorlog(ex.Message)
            GetUserDetailsBB = "1000"
        End Try
    End Function


    Public Shared Function GetBSUPopulateAcademicYear(ByVal BSU_ID As String) As DataTable
        Dim conn As SqlConnection = ConnectionManger.GetOASISConnection
        Dim sql_query As String = "Select distinct ACADEMICYEAR_M.ACY_DESCR as ACY_DESCR, " & _
        " ACADEMICYEAR_D.ACD_ID as ACD_ID FROM ACADEMICYEAR_D INNER JOIN " & _
        " ACADEMICYEAR_M ON ACADEMICYEAR_D.ACD_ACY_ID = ACADEMICYEAR_M.ACY_ID " & _
        " where ACADEMICYEAR_D.ACD_BSU_ID='" & BSU_ID & "'"

        Dim dsData As DataSet = SqlHelper.ExecuteDataset(conn, CommandType.Text, sql_query)
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function


    Public Shared Function PopulateMonthsInAcademicYearID(ByVal ACD_ID As Integer) As DataTable
        Dim dsData As DataSet = Nothing
        Using conn As SqlConnection = ConnectionManger.GetOASISConnection
            Dim sql_query As String = "SELECT TRM_M.TRM_ID, TRM_M.TRM_DESCRIPTION,AMS_ID, " & _
            "DATENAME(month, AMS_DTFROM) + '-' + ltrim(str(datepart(yyyy,AMS_DTFROM))) " & _
            " AS MONTH_DESCR FROM ACADEMIC_MonthS_S LEFT OUTER JOIN " & _
            " TRM_M ON ACADEMIC_MonthS_S.AMS_TRM_ID = TRM_M.TRM_ID AND " & _
            " TRM_M.TRM_ACD_ID=ACADEMIC_MonthS_S.AMS_ACD_ID  WHERE AMS_ACD_ID = " & ACD_ID
            dsData = SqlHelper.ExecuteDataset(conn, CommandType.Text, sql_query)
        End Using
        If Not dsData Is Nothing Then
            Return dsData.Tables(0)
        Else
            Return Nothing
        End If
    End Function

    Public Shared Function GETFEEPAYABLE_ONLINE(ByVal p_ACD_ID As String, _
            ByVal p_BSU_ID As String, ByVal p_SBL_ID As String, ByVal str_conn As String) As DataTable
        'EXEC	@return_value =  TRANSPORT.GETFEEPAYABLE_ONLINE
        '@ACD_ID = 28,
        '@BSU_ID = N'121009' ,
        '@SBL_ID = 42 
        Dim DS As DataSet
        Dim pParms(3) As SqlClient.SqlParameter


        pParms(0) = New SqlClient.SqlParameter("@ACD_ID", SqlDbType.Int)
        pParms(0).Value = p_ACD_ID

        pParms(1) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(1).Value = p_BSU_ID

        pParms(2) = New SqlClient.SqlParameter("@SBL_ID", SqlDbType.VarChar, 20)
        pParms(2).Value = p_SBL_ID

        'pParms(5) = New SqlClient.SqlParameter("@return_value", SqlDbType.Int)
        'pParms(5).Direction = ParameterDirection.ReturnValue        

        DS = SqlHelper.ExecuteDataset(str_conn, CommandType.StoredProcedure, "TRANSPORT.GETFEEPAYABLE_ONLINE", pParms)
        Return DS.Tables(0)
    End Function


End Class
