Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.Sql
Imports System.Data.SqlClient

Public Class MyReportClass
    Public Sub New()
        MyBase.New() 
        frmCmd = True
    End Sub

    'Dim cmdType As CommandType
    'Dim cmdText As String
    'Dim cmdParams As SqlParameterCollection

    Dim cmd As SqlCommand

    Dim resName As String
    Dim voucher As String
    Dim param As Hashtable
    Dim frmCmd As Boolean
    Dim subRep As MyReportClass()

    Dim bIncludeBSUImage As Boolean
    Dim strReportUniqueName As String
    Dim strHeaderBSUID As String

    Public Property IncludeBSUImage() As Boolean
        Get
            Return bIncludeBSUImage
        End Get
        Set(ByVal value As Boolean)
            bIncludeBSUImage = value
        End Set
    End Property

    Public Property SubReport() As MyReportClass()
        Get
            Return subRep
        End Get
        Set(ByVal value As MyReportClass())
            subRep = value
        End Set
    End Property
    Public Property Parameter() As Hashtable
        Get
            Return param
        End Get
        Set(ByVal value As Hashtable)
            If param Is Nothing Then
                param = New Hashtable()
            End If
            param = value
        End Set
    End Property
    Public Property ResourceName() As String
        Get
            Return resName
        End Get
        Set(ByVal value As [String])
            resName = value
        End Set
    End Property

    Public Property VoucherName() As String
        Get
            Return voucher
        End Get
        Set(ByVal value As [String])
            voucher = value
        End Set
    End Property

    Public Property Command() As SqlCommand
        Get
            Return cmd
        End Get
        Set(ByVal value As SqlCommand)
            cmd = value
        End Set
    End Property

    Public Property GetDataSourceFromCommand() As Boolean
        Get
            Return frmCmd
        End Get
        Set(ByVal value As Boolean)
            frmCmd = value
        End Set
    End Property

    Public Property HeaderBSUID() As String
        Get
            Return strHeaderBSUID
        End Get
        Set(ByVal value As [String])
            strHeaderBSUID = value
        End Set
    End Property

    Public Property ReportUniqueName() As String
        Get
            Return strReportUniqueName
        End Get
        Set(ByVal value As String)
            strReportUniqueName = value
        End Set
    End Property
    'Public WriteOnly Property CommandType() As CommandType
    '    Set(ByVal value As CommandType)
    '        cmdType = value
    '    End Set
    'End Property

    'Public WriteOnly Property CommandText() As String
    '    Set(ByVal value As String)
    '        cmdText = value
    '    End Set
    'End Property

    'Public WriteOnly Property Parameter() As SqlParameterCollection
    '    Set(ByVal value As SqlParameterCollection)
    '        cmdParams = value
    '    End Set
    'End Property

End Class

Public Class RepClass
    Inherits CrystalDecisions.CrystalReports.Engine.ReportClass
    Public Sub New()
        MyBase.New()
    End Sub

    Dim resName As String

    Public Overrides Property ResourceName() As String
        Get
            Return resName
        End Get
        Set(ByVal value As [String])
            resName = value
        End Set
    End Property
End Class
