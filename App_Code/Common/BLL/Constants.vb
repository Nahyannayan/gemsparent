Imports Microsoft.VisualBasic

Public NotInheritable Class OASISConstants
    Private Sub New()

    End Sub

    Public Const SEPARATOR_PIPE As String = "|"
    Public Const DateFormat As String = "dd/MMM/yyyy"
    Public Const DataBaseDateFormat As String = "MM-dd-yyyy"
    Public Const Gemstitle As String = "::::GEMS PARENT PORTAL::::"
    'Public Const Transporttitle As String = ">>>Bright Bus Transport - Online Transport Management System<<<"
    Public Const dbFinance As String = "OASISFIN"
    Public Const dbPayroll As String = "OASIS"
    Public Const dbFee As String = "OASIS"
    Public Const dbFees As String = "OASIS_FEES"
    Public Const MAX_PASSWORDTRY As Integer = 3
    Public Const TransportBSU_BrightBus As String = "900500"
    Public Const TransportBSU_Sts As String = "900501"
    Public Const TransportFEEID As Integer = 6
    Public Const ERRORMSG_UNEXPECTED As Integer = 1000
    Public Const ERRORMSG_NOERRORS As Integer = 0

#Region "Menu Accounts"
    Public Const MNU_BB_PAYFEE As String = "FF00010"
    Public Const MNU_BB_CHANGEPASSWORD As String = "FF00015"
    Public Const MNU_BB_PAYMENTHISTORY As String = "FF00020"
    Public Const MNU_BB_CHARGEDETAILS As String = "FF00025"
    Public Const MNU_BB_FEEAGING As String = "FF00030"

 
    'Reports
    'Public Const ReportMISDailyCashFlow As String = "A753035" 
    'Public Const ReportMISPnL As String = "A753005"
    'Public Const ReportMISBalanceSheet As String = "A753015"
    'Public Const ReportMISBalanceSheetSchedule As String = "A753020"
    'Public Const ReportMISCashFlow As String = "A753030"
    'Public Const ReportMISBudgetvsActual As String = "A753040"
    'Public Const MNU_ACC_REP_PROCESSCHECKLIST As String = "A350045"
    'Public Const ReportMISPnLColumnar As String = "A753045"
    'Public Const ReportMISBalanceSheetColumnar As String = "A753050"
    'Public Const ReportAccountBalanceColumnar As String = "A753060"
#End Region


 


 


 

 

 


End Class
