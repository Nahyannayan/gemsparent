Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.UI.WebControls
Imports System

Namespace CURRICULUM

    ''' <summary>
    ''' The class Created for handling Reports 
    ''' 
    ''' Author : SHIJIN C A
    ''' Date : 25-Mar-2009
    ''' </summary>
    ''' <remarks></remarks>
    ''' 
    Public Class ReportFunctions

        Public Shared Function GetSelectedSubjects(Optional ByVal vSBM_IDs As String = "")
            Dim str_sql As String = " SELECT GRD_DISPLAY SBG_ID , " & _
            " SBG_DESCR FROM SUBJECTS_GRADE_S " & _
            " INNER JOIN VW_GRADE_M ON SBG_GRD_ID = GRD_ID "
            If vSBM_IDs <> "" Then
                str_sql += "WHERE SBG_ID IN ('" & vSBM_IDs.Replace("___", "','") & "')"
            End If
            Return SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_CURRICULUMConnectionString, CommandType.Text, str_sql)
        End Function


        'code added by dhanya
        Public Shared Function GetGradeSelectedSubjects(Optional ByVal vSBM_IDs As String = "")
            Dim str_sql As String = " SELECT DISTINCT GRM_DISPLAY ,SBG_ID , " & _
            " SBG_DESCR FROM SUBJECTS_GRADE_S " & _
            " INNER JOIN VW_GRADE_BSU_M ON SBG_GRD_ID = GRM_GRD_ID AND SBG_STM_ID=GRM_STM_ID AND GRM_ACD_ID=SBG_ACD_ID  "
            If vSBM_IDs <> "" Then
                str_sql += "WHERE SBG_ID IN ('" & vSBM_IDs.Replace("___", "','") & "')"
            End If
            str_sql += " ORDER BY GRM_DISPLAY,SBG_DESCR"
            Return SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_CURRICULUMConnectionString, CommandType.Text, str_sql)
        End Function


        Public Shared Function GetSelectedCoreSubjects(Optional ByVal vSBM_IDs As String = "")
            Dim str_sql As String = " SELECT SBM_ID, SBM_DESCR, SBM_SHORTCODE FROM SUBJECT_M "
            If vSBM_IDs <> "" Then
                str_sql += "WHERE SBM_ID IN ('" & vSBM_IDs.Replace("___", "','") & "')"
            End If
            Return SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_CURRICULUMConnectionString, CommandType.Text, str_sql)
        End Function

        Public Shared Function GetSelectedSubjectGroups(Optional ByVal vSGR_IDs As String = "")
            Dim str_sql As String = "SELECT  SBM_DESCR ID, SGR_DESCR DESCR FROM GROUPS_M " & _
            " INNER JOIN SUBJECT_M ON GROUPS_M.SGR_SBM_ID = SUBJECT_M.SBM_ID "
            If vSGR_IDs <> "" Then
                str_sql += "WHERE SGR_ID IN ('" & vSGR_IDs.Replace("___", "','") & "')"
            End If
            Return SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_CURRICULUMConnectionString, CommandType.Text, str_sql)
        End Function

        Public Shared Function GetSectionForGrade(ByVal vBSU_ID As String, ByVal vACD_ID As String, ByVal vGRD_ID As String, Optional ByVal vEMP_ID As String = "", Optional ByVal bSuperUser As Boolean = True)
            Dim str_Sql As String = String.Empty
            str_Sql = " SELECT DISTINCT SECTION_M.SCT_ID, SECTION_M.SCT_DESCR FROM  GRADE_BSU_M INNER JOIN " & _
                " SECTION_M ON GRADE_BSU_M.GRM_GRD_ID = SECTION_M.SCT_GRD_ID AND GRADE_BSU_M.GRM_ACD_ID = SECTION_M.SCT_ACD_ID  AND GRADE_BSU_M.GRM_ID=SECTION_M.SCT_GRM_ID " & _
                " WHERE (GRADE_BSU_M.GRM_BSU_ID = '" & vBSU_ID & "') AND (GRADE_BSU_M.GRM_ACD_ID = '" & vACD_ID & "') AND " & _
                " (GRADE_BSU_M.GRM_GRD_ID = '" & vGRD_ID & "')  order by SECTION_M.SCT_DESCR "

            Return SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, str_Sql)
        End Function

        Public Shared Function GetSelectedStudents(Optional ByVal vSTU_IDs As String = "")
            Dim str_sql As String = "SELECT DISTINCT STU_NO ID, STU_NAME DESCR " & _
             " FROM  vw_STUDENT_DETAILS "
            If vSTU_IDs <> "" Then
                str_sql += "WHERE STU_ID IN ('" & vSTU_IDs.Replace("___", "','") & "')"
            End If
            Return SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_CURRICULUMConnectionString, CommandType.Text, str_sql)
        End Function

        Public Shared Function GetReportType(ByVal vBSU_ID As String, ByVal vACD_ID As String) As DataSet
            Dim str_sql As String = "SELECT RSM_ID, RSM_DESCR FROM RPT.REPORT_SETUP_M " & _
            " WHERE RSM_BSU_ID = '" & vBSU_ID & "' AND RSM_ACD_ID =" & vACD_ID & " ORDER BY RSM_DISPLAYORDER"
            Return SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_CURRICULUMConnectionString, CommandType.Text, str_sql)
        End Function

        Public Shared Function GetReportPrintedFor(ByVal RSM_ID As String) As String
            Dim DsTemp As DataSet
            Dim str_sql As String = "SELECT RPF_ID, RPF_DESCR FROM RPT.REPORT_PRINTEDFOR_M " & _
            " WHERE RPF_RSM_ID = '" & RSM_ID & "'"
            DsTemp = SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_CURRICULUMConnectionString, CommandType.Text, str_sql)
            If DsTemp.Tables(0).Rows.Count >= 1 Then
                Return DsTemp.Tables(0).Rows(0).Item("RPF_ID").ToString()
            Else
                Return ""
            End If
        End Function

        Public Shared Function GetGradeWithReportType(ByVal vRSM_ID As String, ByVal vRPF_ID As String, ByVal vACD_ID As String, ByVal vBSU_ID As String, Optional ByVal vEMP_ID As String = "", Optional ByVal bSuperUser As Boolean = True) As DataSet
            Dim str_Sql As String
            str_Sql = " SELECT DISTINCT VW_GRADE_BSU_M.GRM_GRD_ID AS GRD_ID, VW_GRADE_BSU_M.GRM_DISPLAY, " & _
                " VW_GRADE_M.GRD_DISPLAYORDER FROM VW_GRADE_BSU_M INNER JOIN " & _
                " VW_GRADE_M ON VW_GRADE_BSU_M.GRM_GRD_ID = VW_GRADE_M.GRD_ID INNER JOIN " & _
                " RPT.REPORTSETUP_GRADE_S ON VW_GRADE_BSU_M.GRM_GRD_ID = RPT.REPORTSETUP_GRADE_S.RSG_GRD_ID " & _
                " INNER JOIN  RPT.REPORT_PRINTEDFOR_M ON " & _
                " RPT.REPORTSETUP_GRADE_S.RSG_RSM_ID = RPT.REPORT_PRINTEDFOR_M.RPF_RSM_ID " & _
                " WHERE  (VW_GRADE_BSU_M.GRM_ACD_ID = '" & vACD_ID & "') " & _
                " and RPT.REPORTSETUP_GRADE_S.RSG_RSM_ID = '" & vRSM_ID & "' and RPT.REPORT_PRINTEDFOR_M.RPF_ID  = '" & vRPF_ID & "'" & _
                " ORDER BY VW_GRADE_M.GRD_DISPLAYORDER"
            Return SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_CURRICULUMConnectionString, CommandType.Text, str_Sql)
        End Function

        Public Shared Function GetReportPrintedFor_ALL(ByVal vREP_TYPE As String, Optional ByVal bFINAL_REPORT As Boolean = False) As DataSet
            Dim str_sql As String = "SELECT RPF_ID, RPF_DESCR FROM " & _
            "RPT.REPORT_PRINTEDFOR_M WHERE RPF_RSM_ID =" & vREP_TYPE & _
            " AND ISNULL(RPF_bFINAL_REPORT ,0) = '" & bFINAL_REPORT & _
            "' ORDER BY RPF_DISPLAYORDER"
            Return SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_CURRICULUMConnectionString, CommandType.Text, str_sql)
        End Function

        Public Shared Function GetReportPrinetdID(ByVal vBSU_ID As String, ByVal vACD_ID As String) As DataSet
            Dim str_sql As String = "SELECT RSM_ID, RSM_DESCR FROM RPT.REPORT_SETUP_M " & _
            " WHERE RSM_BSU_ID = '" & vBSU_ID & "' AND RSM_ACD_ID =" & vACD_ID
            Return SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_CURRICULUMConnectionString, CommandType.Text, str_sql)
        End Function

        Public Shared Function PopulateSubjectLevel(ByVal ddlSubjectLevel As DropDownList, ByVal SBGID As String)
            ddlSubjectLevel.Items.Clear()
            Dim str_conn As String = ConnectionManger.GetOASIS_CURRICULUMConnectionString

            Dim str_query As String = " SELECT SBG_bCOREEXT FROM SUBJECTS_GRADE_S WHERE SBG_ID=" & SBGID & ""
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(str_conn, CommandType.Text, str_query)

            If ds.Tables(0).Rows.Count >= 1 Then
                If IsDBNull(ds.Tables(0).Rows(0).Item(0)) = True Then
                    Dim LstNormal As New ListItem("NORMAL", "NORMAL")
                    ddlSubjectLevel.Items.Add(LstNormal)
                Else
                    If ds.Tables(0).Rows(0).Item(0) = 1 Then
                        Dim LstCore As New ListItem("CORE", "CORE")
                        Dim LstEXT As New ListItem("EXTENDED", "EXTENDED")
                        ddlSubjectLevel.Items.Add(LstCore)
                        ddlSubjectLevel.Items.Add(LstEXT)
                    Else
                        Dim LstNormal As New ListItem("NORMAL", "NORMAL")
                        ddlSubjectLevel.Items.Add(LstNormal)
                    End If
                End If

            Else
                Dim LstNormal As New ListItem("NORMAL", "NORMAL")
                ddlSubjectLevel.Items.Add(LstNormal)
            End If
            Return ddlSubjectLevel
        End Function

        Public Shared Function CreateTableStuMarks() As DataTable
            Dim dtStuMark As DataTable
            dtStuMark = New DataTable
            Try
                Dim Id As New DataColumn("ID", System.Type.GetType("System.String"))
                Dim RSD_ID As New DataColumn("RSD_ID", System.Type.GetType("System.String"))
                Dim AcdId As New DataColumn("AcdId", System.Type.GetType("System.String"))
                Dim AcdYear As New DataColumn("AcdYear", System.Type.GetType("System.String"))
                Dim GrdId As New DataColumn("GrdId", System.Type.GetType("System.String"))
                Dim Grade As New DataColumn("Grade", System.Type.GetType("System.String"))
                Dim RPfID As New DataColumn("RpfId", System.Type.GetType("System.String"))
                Dim Report As New DataColumn("Report", System.Type.GetType("System.String"))
                Dim GrdSbjId As New DataColumn("GrdSbjId", System.Type.GetType("System.String"))
                Dim GrdSbj As New DataColumn("GrdSbj", System.Type.GetType("System.String"))
                Dim GroupId As New DataColumn("GroupId", System.Type.GetType("System.String"))
                Dim Group As New DataColumn("Group", System.Type.GetType("System.String"))
                Dim TypeLevel As New DataColumn("TypeLevel", System.Type.GetType("System.String"))
                Dim SBJID As New DataColumn("SBJID", System.Type.GetType("System.String"))
                Dim Subject As New DataColumn("Subject", System.Type.GetType("System.String"))
                Dim RptSchId As New DataColumn("RptSchId", System.Type.GetType("System.String"))
                Dim rptSchedule As New DataColumn("rptSchedule", System.Type.GetType("System.String"))
                Dim studId As New DataColumn("studId", System.Type.GetType("System.String"))
                Dim StudName As New DataColumn("StudName", System.Type.GetType("System.String"))
                Dim Mark As New DataColumn("Mark", System.Type.GetType("System.String"))
                Dim MGrade As New DataColumn("MGrade", System.Type.GetType("System.String"))
                Dim Comments As New DataColumn("Comments", System.Type.GetType("System.String"))

                dtStuMark.Columns.Add(Id)
                dtStuMark.Columns.Add(RSD_ID)
                dtStuMark.Columns.Add(AcdId)
                dtStuMark.Columns.Add(AcdYear)
                dtStuMark.Columns.Add(GrdId)
                dtStuMark.Columns.Add(Grade)
                dtStuMark.Columns.Add(RPfID)
                dtStuMark.Columns.Add(Report)
                dtStuMark.Columns.Add(GrdSbjId)
                dtStuMark.Columns.Add(GrdSbj)
                dtStuMark.Columns.Add(GroupId)
                dtStuMark.Columns.Add(Group)
                dtStuMark.Columns.Add(TypeLevel)
                dtStuMark.Columns.Add(SBJID)
                dtStuMark.Columns.Add(Subject)
                dtStuMark.Columns.Add(RptSchId)
                dtStuMark.Columns.Add(rptSchedule)
                dtStuMark.Columns.Add(studId)
                dtStuMark.Columns.Add(StudName)
                dtStuMark.Columns.Add(Mark)
                dtStuMark.Columns.Add(MGrade)
                dtStuMark.Columns.Add(Comments)

                Return dtStuMark
            Catch ex As Exception
                Return dtStuMark
            End Try
        End Function
        Public Shared Function CreateTableStuComments() As DataTable
            Dim dtStuComments As DataTable
            dtStuComments = New DataTable

            Try

                Dim Id As New DataColumn("ID", System.Type.GetType("System.String"))
                Dim RST_RSM_ID As New DataColumn("RST_RSM_ID", System.Type.GetType("System.String"))
                Dim RST_RPF_ID As New DataColumn("RST_RPF_ID", System.Type.GetType("System.String"))
                Dim RST_RSD_ID As New DataColumn("RST_RSD_ID", System.Type.GetType("System.String"))
                Dim RST_ACD_ID As New DataColumn("RST_ACD_ID", System.Type.GetType("System.String"))
                Dim RST_GRD_ID As New DataColumn("RST_GRD_ID", System.Type.GetType("System.String"))
                Dim RST_STU_ID As New DataColumn("RST_STU_ID", System.Type.GetType("System.String"))
                Dim RST_COMMENTS As New DataColumn("RST_COMMENTS", System.Type.GetType("System.String"))

                dtStuComments.Columns.Add(Id)
                dtStuComments.Columns.Add(RST_RSM_ID)
                dtStuComments.Columns.Add(RST_RPF_ID)
                dtStuComments.Columns.Add(RST_RSD_ID)
                dtStuComments.Columns.Add(RST_ACD_ID)
                dtStuComments.Columns.Add(RST_GRD_ID)
                dtStuComments.Columns.Add(RST_STU_ID)
                dtStuComments.Columns.Add(RST_COMMENTS)

                Return dtStuComments
            Catch ex As Exception
                Return dtStuComments
            End Try
        End Function
        Public Shared Function GetSectionGrade(ByVal vBSU_ID As String, ByVal vACD_ID As String, ByVal vGRD_ID As String, Optional ByVal vEMP_ID As String = "", Optional ByVal bSuperUser As Boolean = True)
            Dim str_Sql As String = String.Empty
            If bSuperUser Then

                str_Sql = " SELECT DISTINCT vw_SECTION_M.SCT_ID, vw_SECTION_M.SCT_DESCR " & _
                            " FROM vw_SECTION_M INNER JOIN " & _
                            " VW_GRADE_BSU_M ON vw_SECTION_M.SCT_GRD_ID = VW_GRADE_BSU_M.GRM_GRD_ID AND " & _
                            " vw_SECTION_M.SCT_ACD_ID = VW_GRADE_BSU_M.GRM_ACD_ID And vw_SECTION_M.SCT_GRM_ID = VW_GRADE_BSU_M.GRM_ID " & _
                            " WHERE (VW_GRADE_BSU_M.GRM_BSU_ID = '" & vBSU_ID & "') AND (VW_GRADE_BSU_M.GRM_ACD_ID = '" & vACD_ID & "') AND " & _
                " (VW_GRADE_BSU_M.GRM_GRD_ID = '" & vGRD_ID & "')  order by vw_SECTION_M.SCT_DESCR "


            Else

                str_Sql = " SELECT DISTINCT vw_SECTION_M.SCT_ID, vw_SECTION_M.SCT_DESCR " & _
                            " FROM vw_SECTION_M INNER JOIN " & _
                            " VW_GRADE_BSU_M ON vw_SECTION_M.SCT_GRD_ID = VW_GRADE_BSU_M.GRM_GRD_ID AND " & _
                            " vw_SECTION_M.SCT_ACD_ID = VW_GRADE_BSU_M.GRM_ACD_ID And vw_SECTION_M.SCT_GRM_ID = VW_GRADE_BSU_M.GRM_ID " & _
                            " WHERE (VW_GRADE_BSU_M.GRM_BSU_ID = '" & vBSU_ID & "') AND (VW_GRADE_BSU_M.GRM_ACD_ID = '" & vACD_ID & "') AND " & _
                            " (VW_GRADE_BSU_M.GRM_GRD_ID = '" & vGRD_ID & "') AND vw_SECTION_M.SCT_EMP_ID='" & vEMP_ID & "' order by vw_SECTION_M.SCT_DESCR "
            End If

            Return SqlHelper.ExecuteDataset(ConnectionManger.GetOASIS_CURRICULUMConnectionString, CommandType.Text, str_Sql)
        End Function

    End Class

End Namespace
