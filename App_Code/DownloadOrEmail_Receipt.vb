﻿Imports Microsoft.VisualBasic
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Web.Configuration
Imports System.Data
Imports System.IO
Imports System.Text
Imports UtilityObj
Imports CrystalDecisions.CrystalReports
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Shared
Imports CrystalDecisions.Web

Public Class DownloadOrEmail_Receipt
#Region "Public Variables"
    Private pBSU_ID As String
    Public Property BSU_ID() As String
        Get
            Return pBSU_ID
        End Get
        Set(ByVal value As String)
            pBSU_ID = value
        End Set
    End Property
    Private pFCL_FCL_ID As Long
    Public Property FCL_FCL_ID() As Long
        Get
            Return pFCL_FCL_ID
        End Get
        Set(ByVal value As Long)
            pFCL_FCL_ID = value
        End Set
    End Property
    Private pFCO_ID As Long
    Public Property FCO_ID() As Long
        Get
            Return pFCO_ID
        End Get
        Set(ByVal value As Long)
            pFCO_ID = value
        End Set
    End Property
    Private pLogoPath As String
    Public Property LogoPath() As String
        Get
            Return pLogoPath
        End Get
        Set(ByVal value As String)
            pLogoPath = value
        End Set
    End Property
    Private pEmailStatus As String
    Public Property EmailStatusMsg() As String
        Get
            Return pEmailStatus
        End Get
        Set(ByVal value As String)
            pEmailStatus = value
        End Set
    End Property

    Private boolEmailed As Boolean
    Public Property bEmailSuccess() As Boolean
        Get
            Return boolEmailed
        End Get
        Set(ByVal value As Boolean)
            boolEmailed = value
        End Set
    End Property
    Private boolMobileApp As Boolean
    Public Property bMobileApp() As Boolean
        Get
            Return boolMobileApp
        End Get
        Set(ByVal value As Boolean)
            boolMobileApp = value
        End Set
    End Property
#End Region
    Shared reportHeader As String
    Dim rs As New ReportDocument
    Public Shared Function DownloadOrEMailReceipt(ByVal Action As String, ByVal p_Receiptno As String, ByVal BSUID As String, ByVal ForceEmail As Boolean) As String
        Dim str_Sql, strFilter As String
        strFilter = " FCL_RECNO='" & p_Receiptno & "' AND FCL_BSU_ID='" & BSUID & "' "
        str_Sql = "select * FROM [FEES].[VW_OSO_FEES_FEERECEIPT] WHERE " + strFilter
        Dim cmd As New SqlCommand
        cmd.CommandText = str_Sql
        cmd.CommandType = Data.CommandType.Text
        ' check whether Data Exits
        Dim str_conn As String = ConnectionManger.GetOASISTRANSPORTConnectionString
        Dim ds As New DataSet
        Dim repSource As New MyReportClass
        SqlHelper.FillDataset(str_conn, CommandType.Text, cmd.CommandText, ds, Nothing)
        repSource.ReportUniqueName = BSUID & "_" & p_Receiptno
        If Not ds Is Nothing Or ds.Tables(0).Rows.Count > 0 Then
            cmd.Connection = New SqlConnection(str_conn)

            Dim params As New Hashtable
            params("UserName") = "SYSTEM"
            repSource.Parameter = params
            repSource.Command = cmd
            repSource.IncludeBSUImage = False
            '''''''''''''''
            Dim repSourceSubRep(1) As MyReportClass
            repSourceSubRep(0) = New MyReportClass
            repSourceSubRep(1) = New MyReportClass

            Dim cmdHeader As New SqlCommand("getBsuInFoWithImage", New SqlConnection(ConnectionManger.GetOASISConnectionString))
            cmdHeader.CommandType = CommandType.StoredProcedure
            cmdHeader.Parameters.AddWithValue("@IMG_BSU_ID", BSUID)
            cmdHeader.Parameters.AddWithValue("@IMG_TYPE", "LOGO")
            repSourceSubRep(1).Command = cmdHeader

            Dim cmdSubEarn As New SqlCommand
            cmdSubEarn.CommandText = "EXEC FEES.GetReceiptPrint_Online @FCL_RECNO ='" & p_Receiptno & "', @FCL_BSU_ID  = '" & BSUID & "'"
            cmdSubEarn.Connection = New SqlConnection(str_conn)
            cmdSubEarn.CommandType = CommandType.Text
            repSourceSubRep(0).Command = cmdSubEarn
            repSource.SubReport = repSourceSubRep
            'repSource.ResourceName = "~/Reports/RPT/rptFeeReceipt.rpt"
            Dim bBSUTaxable As Boolean, RPTName As String

            bBSUTaxable = DirectCast(SqlHelper.ExecuteScalar(ConnectionManger.GetOASISConnectionString, CommandType.Text, "SELECT ISNULL(BUS_bFEE_TAXABLE,0) FROM dbo.BUSINESSUNIT_SUB WHERE BUS_BSU_ID='" & BSUID & "'"), Boolean)
            If bBSUTaxable Then
                ' repSource.ResourceName = HttpContext.Current.Server.MapPath("../Reports/FeeReceipt/rptFeeReceipt_Tax.rpt")
                RPTName = "rptFeeReceipt_Tax.rpt"
            Else
                RPTName = "rptFeeReceipt.rpt"
                ' repSource.ResourceName = HttpContext.Current.Server.MapPath("../Reports/FeeReceipt/rptFeeReceipt.rpt")
            End If
            Dim RootBackArr As String(), AppPath As String, i As Int16, RPTPath As String
            AppPath = HttpContext.Current.Request.Url.AbsolutePath
            RootBackArr = AppPath.ToString.Split("/")
            RPTPath = ""
            For i = 1 To RootBackArr.Length - 3
                RPTPath = RPTPath & "../"
            Next

            repSource.ResourceName = RPTPath & "Reports/FeeReceipt/" & RPTName
        End If
        If Not repSource Is Nothing Then
            Dim repClassVal As New RepClass

            repClassVal = New RepClass
            repClassVal.ResourceName = repSource.ResourceName
            repClassVal.SetDataSource(ds.Tables(0))
            If repSource.IncludeBSUImage Then
                If repSource.HeaderBSUID Is Nothing Then
                    IncludeBSUmage(repClassVal, BSUID)
                Else
                    IncludeBSUmage(repClassVal, repSource.HeaderBSUID)
                End If

            End If

            SubReport(repSource, repSource, repClassVal)

            Dim pdfFilePath As String
            Dim pdfFileName As String
            If Action.ToUpper = "DOWNLOAD" Or Action.ToUpper = "" Then
                pdfFileName = "Fee Receipt-" & repSource.ReportUniqueName & ".pdf"
            Else
                pdfFileName = "Fee Receipt-" & repSource.ReportUniqueName & ".pdf"
            End If
            pdfFilePath = WebConfigurationManager.AppSettings.Item("OnlineFeeReceipt")
            pdfFilePath += pdfFileName

            repClassVal.PrintOptions.PaperOrientation = PaperOrientation.Portrait
            Dim ienum As IDictionaryEnumerator = repSource.Parameter.GetEnumerator()
            While ienum.MoveNext()
                repClassVal.SetParameterValue(ienum.Key, ienum.Value)
            End While
            Try
                If System.IO.File.Exists(pdfFilePath) Then
                    System.IO.File.Delete(pdfFilePath)
                End If
            Catch ex As Exception

            End Try

            repClassVal.ExportToDisk(ExportFormatType.PortableDocFormat, pdfFilePath)
            If Action.ToUpper = "DOWNLOAD" Or Action.ToUpper = "" Then
                Dim BytesData As Byte()
                BytesData = ConvertFiletoBytes(pdfFilePath)
                Dim ContentType, Extension As String
                ContentType = "application/pdf"
                Extension = GetFileExtension(ContentType)
                Dim Title As String = "Receipt"
                If repSource.ReportUniqueName.ToString.Split("_").Length > 1 Then
                    Title = repSource.ReportUniqueName.ToString.Split("_")(1)
                End If
                DownloadFile(System.Web.HttpContext.Current, BytesData, ContentType, Extension, Title)
                repClassVal.Close()
                repClassVal.Dispose()
                repSource = Nothing
                repClassVal = Nothing
            ElseIf Action.ToUpper = "EMAIL" Then
                ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, _
                  "EXEC  [ONLINE].[GetFeeReceiptEmailDetails] '" & BSUID & "','" & p_Receiptno & "'")
                If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                    Dim subject As String = "Online Payment Receipt"
                    Dim ContactName, BSU_Name, StudName, EMailID As String, Emailed As Boolean
                    ContactName = ds.Tables(0).Rows(0).Item("TO_EMAIL_NAME").ToString
                    BSU_Name = ds.Tables(0).Rows(0).Item("BSU_Name").ToString
                    StudName = ds.Tables(0).Rows(0).Item("STU_NAME").ToString
                    EMailID = ds.Tables(0).Rows(0).Item("TO_EMAIL_ID").ToString
                    Emailed = ds.Tables(0).Rows(0).Item("Emailed")
                    If ForceEmail Or Not Emailed Then
                        Dim sb As New StringBuilder
                        sb.Append("<table border='0' style='border-top-style: none; font-family: Verdana; border-right-style: none; border-left-style: none; background-color: beige; border-bottom-style: none;font-size: 10pt;'>")
                        sb.Append("<tr><td  style='font-weight: bold; font-size: 12pt; color: white; background-color: gray'>Dear " & ContactName & ", </td></tr>")
                        sb.Append("<tr><td ><br />GREETINGS from the " & BSU_Name & "<br /></td></tr>")
                        sb.Append("<tr><td ><br />Please find the attached fee receipt for your child " & StudName & ".<br /></td></tr>")
                        sb.Append("<tr><td ><br />Thanks & regards </td></tr>")
                        sb.Append("<tr><td >Accounts </td></tr>")
                        sb.Append("<tr><td >" & BSU_Name & "<br /></td></tr>")
                        sb.Append("<tr><td></td></tr>")
                        sb.Append("<tr></tr><tr></tr>")
                        sb.Append("</table>")
                        Dim EmailStatus As String
                        EmailStatus = eMailReceipt.SendNewsLetters(ds.Tables(0).Rows(0)("SYS_EMAIL_FEEONLINE").ToString, EMailID, _
                                     subject, sb.ToString, ds.Tables(0).Rows(0)("SYS_EMAIL_FEEONLINE").ToString, ds.Tables(0).Rows(0)("SYS_PASSWORD_FEEONLINE").ToString, _
                                            ds.Tables(0).Rows(0)("SYS_EMAIL_HOST").ToString, ds.Tables(0).Rows(0)("SYS_EMAIL_PORT").ToString, pdfFilePath)
                        If EmailStatus.ToString.ToUpper.Contains("SUCCESS") Then
                            DownloadOrEMailReceipt = "Receipt successfully emailed to " & EMailID
                            Dim sqlStr As String
                            sqlStr = "UPDATE FEES.FEECOLLECTION_H SET FCL_bEMAILED=1 WHERE FCL_BSU_ID='" & BSUID & "' and FCL_RECNO='" & p_Receiptno & "'"
                            SqlHelper.ExecuteNonQuery(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.Text, sqlStr)
                        Else
                            DownloadOrEMailReceipt = "Email Sending Failed"
                        End If
                        repClassVal.Close()
                        repClassVal.Dispose()
                        repSource = Nothing
                        repClassVal = Nothing
                    End If
                    Try
                        If System.IO.File.Exists(pdfFilePath) Then
                            System.IO.File.Delete(pdfFilePath)
                        End If
                    Catch ex As Exception

                    End Try
                End If

            End If
        End If
    End Function
    Public Shared Function ConvertFiletoBytes(ByVal FilePath As String) As Byte()
        Dim _tempByte() As Byte = Nothing
        If String.IsNullOrEmpty(FilePath) = True Then
            Throw New ArgumentNullException("File Name Cannot be Null or Empty", "FilePath")
            Return Nothing
        End If
        Try
            Dim _fileInfo As New IO.FileInfo(FilePath)
            Dim _NumBytes As Long = _fileInfo.Length
            Dim _FStream As New IO.FileStream(FilePath, IO.FileMode.Open, IO.FileAccess.Read)
            Dim _BinaryReader As New IO.BinaryReader(_FStream)
            _tempByte = _BinaryReader.ReadBytes(Convert.ToInt32(_NumBytes))
            _fileInfo = Nothing
            _NumBytes = 0
            _FStream.Close()
            _FStream.Dispose()
            _BinaryReader.Close()
            Return _tempByte
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
    Private Shared Sub DownloadFile(ByVal context As HttpContext, ByVal BytesData As Byte(), ByVal contentType As String, ByVal Extension As String, ByVal Title As String)
        context.Response.Clear()
        context.Response.AddHeader("Content-Length", BytesData.Length.ToString())
        context.Response.ContentType = contentType
        context.Response.AddHeader("Expires", "0")
        context.Response.AddHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0")
        context.Response.AddHeader("Pragma", "public")
        context.Response.AddHeader("Content-Disposition", "attachment; filename=" & Title & Extension)
        context.Response.BinaryWrite(BytesData)
        context.Response.Flush()
        context.Response.End()
    End Sub
    Private Shared Function GetFileExtension(ByVal ContentType As String) As String
        ' Dim filePath As String = fuUpload.PostedFile.FileName
        Select Case ContentType
            Case "application/vnd.ms-word"
                GetFileExtension = ".doc"
                Exit Select
            Case "application/vnd.ms-word"
                GetFileExtension = ".docx"
                Exit Select
            Case "application/vnd.ms-excel"
                GetFileExtension = ".xls"
                Exit Select
            Case "image/jpg"
                GetFileExtension = ".jpg"
                Exit Select
            Case "image/png"
                GetFileExtension = ".png"
                Exit Select
            Case "image/gif"
                GetFileExtension = ".gif"
                Exit Select
            Case "application/pdf"
                GetFileExtension = ".pdf"
                Exit Select
        End Select
    End Function
    Private Shared Sub SubReport(ByVal repSource As MyReportClass, ByVal subRep As MyReportClass, ByRef repClassVal As RepClass)
        If subRep.SubReport IsNot Nothing Then
            Dim ii As Integer = 0
            If subRep.IncludeBSUImage Then
                ii = 1
            End If
            For i As Integer = 0 To subRep.SubReport.Length - 1
                Dim myrep As MyReportClass = subRep.SubReport(i)
                If myrep IsNot Nothing Then
                    If myrep.SubReport IsNot Nothing Then
                        SubReport(repSource, subRep, repClassVal)
                    Else
                        Dim objConn As SqlConnection = repSource.Command.Connection
                        'objConn.Close()
                        objConn.Open()
                        Dim ds As New DataSet
                        Dim adpt As New SqlDataAdapter
                        adpt.SelectCommand = myrep.Command
                        adpt.Fill(ds)
                        objConn.Close()
                        If String.Compare(repClassVal.Subreports(i + ii).Name, "rptHeader.rpt", True) <> 0 Or _
                        String.Compare(repClassVal.Subreports(i + ii).Name, "rptHeader_Transport.rpt", True) <> 0 Or _
                        String.Compare(repClassVal.Subreports(i + ii).Name, "rptHeader_Portrait.rpt", True) <> 0 Then
                            repClassVal.Subreports(i + ii).SetDataSource(ds.Tables(0))
                        Else
                            reportHeader = repClassVal.Subreports(i + ii).Name
                        End If
                    End If
                End If
            Next
        End If
    End Sub

    Private Shared Sub IncludeBSUmage(ByRef repClassVal As RepClass, ByVal BSUID As String)
        Dim lstrConn As String = WebConfigurationManager.ConnectionStrings("OASISConnectionString").ConnectionString
        Dim objConn As New SqlConnection(lstrConn)
        Dim adpt As New SqlDataAdapter
        Dim ds As New DataSet

        Dim cmd As New SqlCommand("getBsuInFoWithImage", objConn)
        cmd.CommandType = CommandType.StoredProcedure

        Dim sqlpIMG_BSU_ID As New SqlParameter("@IMG_BSU_ID", SqlDbType.VarChar, 100)
        If BSUID = "" Then
            sqlpIMG_BSU_ID.Value = BSUID
        Else
            sqlpIMG_BSU_ID.Value = BSUID
        End If

        cmd.Parameters.Add(sqlpIMG_BSU_ID)

        Dim sqlpIMG_TYPE As New SqlParameter("@IMG_TYPE", SqlDbType.VarChar, 10)
        sqlpIMG_TYPE.Value = "LOGO"
        cmd.Parameters.Add(sqlpIMG_TYPE)

        adpt.SelectCommand = cmd
        objConn.Open()
        adpt.Fill(ds)
        objConn.Close()
        If Not repClassVal.Subreports("rptHeader.rpt") Is Nothing Then
            repClassVal.Subreports("rptHeader.rpt").SetDataSource(ds.Tables(0))
        ElseIf Not repClassVal.Subreports("rptHeader_Transport.rpt") Is Nothing Then
            repClassVal.Subreports("rptHeader_Transport.rpt").SetDataSource(ds.Tables(0))
        ElseIf Not repClassVal.Subreports("rptHeader_Portrait.rpt") Is Nothing Then
            repClassVal.Subreports("rptHeader_Portrait.rpt").SetDataSource(ds.Tables(0))
        End If
        'repClassVal.Subreports("bsuimage").SetDataSource(dtDt)

        'repClassVal.Subreports("BSUIMAGE").SetDataSource(dtDt)
    End Sub

    Sub LoadReports(ByVal rptClass As rptClass)
        Try
            Dim iRpt As New DictionaryEntry
            Dim rptStr As String = ""
            Dim crParameterDiscreteValue As ParameterDiscreteValue
            Dim crParameterFieldDefinitions As ParameterFieldDefinitions
            Dim crParameterFieldLocation As ParameterFieldDefinition
            Dim crParameterValues As ParameterValues

            With rptClass
                rs.Load(.reportPath)

                Dim myConnectionInfo As ConnectionInfo = New ConnectionInfo()
                myConnectionInfo.ServerName = .crInstanceName
                myConnectionInfo.DatabaseName = .crDatabase
                myConnectionInfo.UserID = .crUser
                myConnectionInfo.Password = .crPassword
                'Logger.LogInfo("Calling SetDBLogonForSubreports")
                'UtilityObj.Errorlog("Calling SetDBLogonForSubreports", "BBT_TEST")
                SetDBLogonForSubreports(myConnectionInfo, rs, .reportParameters)
                SetDBLogonForReport(myConnectionInfo, rs, .reportParameters)
                'UtilityObj.Errorlog("setting report parameters", "BBT_TEST")
                'Logger.LogInfo("Setting report parameters")
                crParameterFieldDefinitions = rs.DataDefinition.ParameterFields
                If .reportParameters.Count <> 0 Then
                    For Each iRpt In .reportParameters
                        crParameterFieldLocation = crParameterFieldDefinitions.Item(iRpt.Key.ToString)
                        crParameterValues = crParameterFieldLocation.CurrentValues
                        crParameterDiscreteValue = New CrystalDecisions.Shared.ParameterDiscreteValue
                        crParameterDiscreteValue.Value = iRpt.Value
                        crParameterValues.Add(crParameterDiscreteValue)
                        crParameterFieldLocation.ApplyCurrentValues(crParameterValues)
                    Next
                End If

                If .selectionFormula <> "" Then
                    rs.RecordSelectionFormula = .selectionFormula
                End If
                'Logger.LogInfo("Calling exportReport")
                'UtilityObj.Errorlog("Calling exportreport", "BBT_TEST")
                exportReport(rs, CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat)
                rs.Close()
                rs.Dispose()
            End With
        Catch ex As Exception
            rs.Close()
            rs.Dispose()
        End Try
    End Sub

    Protected Sub exportReport(ByVal selectedReport As CrystalDecisions.CrystalReports.Engine.ReportDocument, ByVal eft As CrystalDecisions.Shared.ExportFormatType)
        selectedReport.ExportOptions.ExportFormatType = eft

        Dim contentType As String = ""
        ' Make sure asp.net has create and delete permissions in the directory
        Dim tempDir As String = Web.Configuration.WebConfigurationManager.AppSettings.Item("OnlineFeeReceipt")
        Dim tempFileName As String = HttpContext.Current.Session("sBsuId") & "_" & FCL_FCL_ID & "."
        Select Case eft
            Case CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat
                tempFileName += "pdf"
                contentType = "application/pdf"
                Exit Select
            Case CrystalDecisions.[Shared].ExportFormatType.WordForWindows
                tempFileName += "doc"
                contentType = "application/msword"
                Exit Select
            Case CrystalDecisions.[Shared].ExportFormatType.Excel
                tempFileName += "xls"
                contentType = "application/vnd.ms-excel"
                Exit Select
            Case CrystalDecisions.[Shared].ExportFormatType.HTML32, CrystalDecisions.[Shared].ExportFormatType.HTML40
                tempFileName += "htm"
                contentType = "text/html"
                Dim hop As New CrystalDecisions.Shared.HTMLFormatOptions()
                hop.HTMLBaseFolderName = tempDir
                hop.HTMLFileName = tempFileName
                selectedReport.ExportOptions.FormatOptions = hop
                Exit Select
        End Select
        'Logger.LogInfo("inside exportReport")
        'UtilityObj.Errorlog("inside export report", "BBT_TEST")
        Dim dfo As New CrystalDecisions.Shared.DiskFileDestinationOptions()
        dfo.DiskFileName = tempDir + tempFileName
        selectedReport.ExportOptions.DestinationOptions = dfo
        selectedReport.ExportOptions.ExportDestinationType = CrystalDecisions.[Shared].ExportDestinationType.DiskFile
        'Logger.LogInfo("inside exportReport, DiskFileName = " & dfo.DiskFileName)
        'UtilityObj.Errorlog("before export file", "BBT_TEST")
        selectedReport.Export()
        selectedReport.Close()

        Dim tempFileNameUsed As String
        If eft = CrystalDecisions.[Shared].ExportFormatType.HTML32 OrElse eft = CrystalDecisions.[Shared].ExportFormatType.HTML40 Then
            Dim fp As String() = selectedReport.FilePath.Split("\".ToCharArray())
            Dim leafDir As String = fp(fp.Length - 1)
            ' strip .rpt extension
            leafDir = leafDir.Substring(0, leafDir.Length - 4)
            tempFileNameUsed = String.Format("{0}{1}\{2}", tempDir, leafDir, tempFileName)
        Else
            tempFileNameUsed = tempDir + tempFileName
        End If
        EmailFeeReceipt(tempFileNameUsed)
    End Sub

    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument, ByVal reportParameters As Hashtable)
        Dim myTables As Tables = myReportDocument.Database.Tables
        Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
        For Each myTable In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
            myTable.Location = myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
        Next
        myReportDocument.VerifyDatabase()
    End Sub

    Private Sub SetDBLogonForSubreports(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument, ByVal reportParameters As Hashtable)
        Dim mySections As Sections = myReportDocument.ReportDefinition.Sections
        Dim mySection As Section
        For Each mySection In mySections
            Dim myReportObjects As ReportObjects = mySection.ReportObjects
            Dim myReportObject As ReportObject
            For Each myReportObject In myReportObjects
                If myReportObject.Kind = ReportObjectKind.SubreportObject Then
                    Dim mySubreportObject As SubreportObject = CType(myReportObject, SubreportObject)
                    Dim subReportDocument As ReportDocument = mySubreportObject.OpenSubreport(mySubreportObject.SubreportName)

                    Select Case subReportDocument.Name

                        Case Else
                            SetDBLogonForReport(myConnectionInfo, subReportDocument, reportParameters)
                    End Select

                    ' subReportDocument.VerifyDatabase()
                End If
            Next
        Next

    End Sub

    Public Sub EmailFeeReceipt(ByVal pdfFilePath As String)
        'UtilityObj.Errorlog("inside emailfee receipt", "BBT_TEST")
        Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.Text, _
                  "EXEC [ONLINE].[GET_FEERECEIPT_EMAILINFO] @FCL_FCL_ID=" & FCL_FCL_ID & "")
        Dim dt As DataTable = FeeCollectionOnlineBB.Get_PaidDetails(FCO_ID, True)
        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            Dim subject As String = IIf(bMobileApp, "Payment Receipt", "Online Payment Receipt")
            Dim ContactName, BSU_Name, StudName, EMailID As String, Emailed As Boolean
            ContactName = ds.Tables(0).Rows(0).Item("TO_EMAIL_NAME").ToString
            BSU_Name = ds.Tables(0).Rows(0).Item("BSU_Name").ToString
            StudName = ds.Tables(0).Rows(0).Item("STU_NAME").ToString
            EMailID = ds.Tables(0).Rows(0).Item("TO_EMAIL_ID").ToString
            Emailed = ds.Tables(0).Rows(0).Item("Emailed")
            Dim DummyImagePath As String = WebConfigurationManager.ConnectionStrings("EmpFilepathvirtual").ToString() & "/DUMMYIMAGE/dummy_profile.png"
            If Not Emailed Or 1 = 1 Then
                Dim sb As New StringBuilder '
                sb.Append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>")
                sb.Append("<html xmlns='http://www.w3.org/1999/xhtml'>")
                sb.Append("<head id='Head1' runat='server'><title>Untitled Page</title></head><body>")
                sb.Append("<form id='form1' runat='server'><div style='width: 100%;'><center>")
                sb.Append("<table border='0' style='width:50%; border-top-style: none; font-family: Verdana; border-right-style: none; border-left-style: none; background-color: white; border-bottom-style: none;font-size: 10pt;'>")
                sb.Append("<tr><td align='left'><div style='width:80px; height:60px;'><img src='" & LogoPath & "' alt='' style='width:80px; height:60px;'/></div></td></tr>")
                sb.Append("<tr><td><hr /></td></tr>")
                sb.Append("<tr><td align='left' style='font-weight: bold; font-size: 12pt; color: white; background-color: gray'>Dear " & ContactName & ", </td></tr>")
                sb.Append("<tr><td align='left' style='color:#00476B;'><br />GREETINGS from the <strong>" & BSU_Name & "</strong><br /><br /></td></tr>")

                If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                    sb.Append("<tr><td align='left' style='color:#00476B; font-weight:bold;'>Please find the attached fee receipt for your " & IIf(dt.Rows.Count = 1, "Child", "Children") & "<br /><br /></td></tr>")
                    sb.Append("<br /><tr><td align='center'><div style=''border:1; border-color :#D1D1D1; display:block;''>")
                    Dim RowCount As Int16 = 1
                    For Each dr As DataRow In dt.Rows
                        sb.Append("<div style='display:block; background-color :#F0F0F0; color:#80B2CC;' >")
                        sb.Append("<table style='width:100%;'>")
                        'sb.Append("<td align='center' valign='middle'><img width='60' height='70' src='" & DummyImagePath & "' alt='" & dr("STU_NO") & "' /></td>")
                        'sb.Append("<td style='border-left: solid 1pt #00476B !important; width: 1pt;'></td>")
                        sb.Append("<tr><td align='left' style='color:#80B2CC;width:100px;'>School</td><td style='color:#80B2CC;width:20px;'>:</td><td align='left' style='color:#00476B;'>" & dr("BSUNAME") & "</td></tr>")
                        sb.Append("<tr><td align='left' style='color:#80B2CC;width:100px;'>Student ID</td><td style='color:#80B2CC;width:20px;'>:</td><td align='left' style='color:#00476B;'>" & dr("STU_NO") & "</td></tr>")
                        sb.Append("<tr><td align='left' style='color:#80B2CC;width:100px;'>Student Name</td><td style='color:#80B2CC;'>:</td><td align='left' style='color:#00476B;'>" & dr("STU_NAME") & "</td></tr>")
                        sb.Append("<tr><td align='left' style='color:#80B2CC;width:100px;'>Grade</td><td style='color:#80B2CC;'>:</td><td align='left' style='color:#00476B;'>" & dr("GRD_DISPLAY") & "</td></tr>")
                        sb.Append("<tr><td align='left' style='color:#80B2CC;width:100px;'>Receipt.No</td><td style='color:#80B2CC;'>:</td><td align='left' style='color:#00476B;'>" & dr("RECNO") & "</td></tr>")
                        sb.Append("<tr><td align='left' style='color:#80B2CC;width:100px;'>Paid (" & dr("BSUCURRENCY").ToString & ")</td><td style='color:#80B2CC;'>:</td><td align='left' style='color:#00476B;'>" & Format(dr("AMOUNT"), "#,##0.00") & "</td></tr>")
                        sb.Append("</table></div>")
                        If dt.Rows.Count > RowCount Then
                            sb.Append("<hr /><br />")
                        End If
                        RowCount += 1
                    Next
                    sb.Append("</div></td></tr>")
                End If
                sb.Append("<tr><td align='left'><br />Thanks & regards </td></tr>")
                sb.Append("<tr><td align='left'>Accounts </td></tr>")
                sb.Append("<tr><td align='left'><strong>" & BSU_Name & "</strong><br /></td></tr>")
                sb.Append("<tr><td></td></tr>")
                sb.Append("<tr></tr><tr></tr>")
                sb.Append("</table></center></div></form></body></html>")
                'UtilityObj.Errorlog("Calling sendNewsletters", "BBT_TEST")
                EMailID = "shakeel.shakkeer@gemseducation.com"
                If EMailID <> "" Then
                    EmailStatusMsg = eMailReceipt.SendNewsLetters(ds.Tables(0).Rows(0)("SYS_EMAIL_FEEONLINE").ToString, EMailID, _
                                                 subject, sb.ToString, ds.Tables(0).Rows(0)("SYS_EMAIL_FEEONLINE").ToString, ds.Tables(0).Rows(0)("SYS_PASSWORD_FEEONLINE").ToString, _
                                                        ds.Tables(0).Rows(0)("SYS_EMAIL_HOST").ToString, ds.Tables(0).Rows(0)("SYS_EMAIL_PORT").ToString, pdfFilePath)
                Else
                    EmailStatusMsg = eMailReceipt.SendNewsLetters(ds.Tables(0).Rows(0)("SYS_EMAIL_FEEONLINE").ToString, "jacob.chacko@gemseducation.com", _
                                 subject, sb.ToString, ds.Tables(0).Rows(0)("SYS_EMAIL_FEEONLINE").ToString, ds.Tables(0).Rows(0)("SYS_PASSWORD_FEEONLINE").ToString, _
                                        ds.Tables(0).Rows(0)("SYS_EMAIL_HOST").ToString, ds.Tables(0).Rows(0)("SYS_EMAIL_PORT").ToString, pdfFilePath)
                End If

                If EmailStatusMsg.ToString.ToUpper.Contains("SUCCESS") Then
                    EmailStatusMsg = IIf(EMailID <> "", " Receipt successfully emailed to " & EMailID, "")
                    bEmailSuccess = True
                    Dim sqlStr As String = "UPDATE FEES.FEECOLLECTION_H SET FCL_bEMAILED=1 WHERE FCL_FCL_ID='" & FCL_FCL_ID & "' "
                    SqlHelper.ExecuteNonQuery(ConnectionManger.GetOASISTRANSPORTConnectionString, CommandType.Text, sqlStr)
                Else
                    EmailStatusMsg = "Email Sending Failed"
                    'UtilityObj.Errorlog("Email status" & EmailStatus, "BBT_TEST")
                End If

            End If
            Try
                If System.IO.File.Exists(pdfFilePath) Then
                    System.IO.File.Delete(pdfFilePath)
                End If
            Catch ex As Exception

            End Try
        End If
    End Sub


End Class
