﻿Imports Microsoft.VisualBasic
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web.Security
Imports System.Data
Imports System.Net
Imports System.Web
Imports System.Collections.Generic
Imports System.Security.Cryptography

Public Class PaymentGatewayClass

#Region "Properties And Variable Declaration"
    Public Property vpc_Amount() As String
        Get
            Return null2unknown(_vpc_Amount)
        End Get
        Set(ByVal Value As String)
            _vpc_Amount = Value
        End Set
    End Property
    Public Property vpc_ActualAmount() As String
        Get
            Return null2unknown(_vpc_ActualAmount)
        End Get
        Set(ByVal Value As String)
            _vpc_ActualAmount = Value
        End Set
    End Property
    Public Property vpc_Locale() As String
        Get
            Return null2unknown(_vpc_Locale)
        End Get
        Set(ByVal Value As String)
            _vpc_Locale = Value
        End Set
    End Property
    Public Property vpc_BatchNo() As String
        Get
            Return null2unknown(_vpc_BatchNo)
        End Get
        Set(ByVal Value As String)
            _vpc_BatchNo = Value
        End Set
    End Property
    Public Property vpc_Command() As String
        Get
            Return null2unknown(_vpc_Command)
        End Get
        Set(ByVal Value As String)
            _vpc_Command = Value
        End Set
    End Property
    Public Property vpc_Version() As String
        Get
            Return null2unknown(_vpc_Version)
        End Get
        Set(ByVal Value As String)
            _vpc_Version = Value
        End Set
    End Property
    Public Property vpc_CardType() As String
        Get
            Return null2unknown(_vpc_CardType)
        End Get
        Set(ByVal Value As String)
            _vpc_CardType = Value
        End Set
    End Property
    Public Property vpc_OrderInfo() As String
        Get
            Return null2unknown(_vpc_OrderInfo)
        End Get
        Set(ByVal Value As String)
            _vpc_OrderInfo = Value
        End Set
    End Property
    Public Property vpc_ReceiptNo() As String
        Get
            Return null2unknown(_vpc_ReceiptNo)
        End Get
        Set(ByVal Value As String)
            _vpc_ReceiptNo = Value
        End Set
    End Property
    Public Property vpc_MerchantID() As String
        Get
            Return null2unknown(_vpc_MerchantID)
        End Get
        Set(ByVal Value As String)
            _vpc_MerchantID = Value
        End Set
    End Property
    Public Property vpc_AuthorizeID() As String
        Get
            Return null2unknown(_vpc_AuthorizeID)
        End Get
        Set(ByVal Value As String)
            _vpc_AuthorizeID = Value
        End Set
    End Property
    Public Property vpc_MerchTxnRef() As String
        Get
            Return null2unknown(_vpc_MerchTxnRef)
        End Get
        Set(ByVal Value As String)
            _vpc_MerchTxnRef = Value
        End Set
    End Property
    Public Property vpc_TransactionNo() As String
        Get
            Return null2unknown(_vpc_TransactionNo)
        End Get
        Set(ByVal Value As String)
            _vpc_TransactionNo = Value
        End Set
    End Property
    Public Property vpc_AcqResponseCode() As String
        Get
            Return null2unknown(_vpc_AcqResponseCode)
        End Get
        Set(ByVal Value As String)
            _vpc_AcqResponseCode = Value
        End Set
    End Property
    Public Property vpc_VerType() As String
        Get
            Return null2unknown(_vpc_VerType)
        End Get
        Set(ByVal Value As String)
            _vpc_VerType = Value
        End Set
    End Property
    Public Property vpc_VerStatus() As String
        Get
            Return null2unknown(_vpc_VerStatus)
        End Get
        Set(ByVal Value As String)
            _vpc_VerStatus = Value
        End Set
    End Property
    Public Property vpc_Token() As String
        Get
            Return null2unknown(_vpc_Token)
        End Get
        Set(ByVal Value As String)
            _vpc_Token = Value
        End Set
    End Property
    Public Property vpc_VerSecurLevel() As String
        Get
            Return null2unknown(_vpc_VerSecurLevel)
        End Get
        Set(ByVal Value As String)
            _vpc_VerSecurLevel = Value
        End Set
    End Property
    Public Property vpc_Enrolled() As String
        Get
            Return null2unknown(_vpc_Enrolled)
        End Get
        Set(ByVal Value As String)
            _vpc_Enrolled = Value
        End Set
    End Property
    Public Property vpc_Xid() As String
        Get
            Return null2unknown(_vpc_Xid)
        End Get
        Set(ByVal Value As String)
            _vpc_Xid = Value
        End Set
    End Property
    Public Property vpc_AcqECI() As String
        Get
            Return null2unknown(_vpc_AcqECI)
        End Get
        Set(ByVal Value As String)
            _vpc_AcqECI = Value
        End Set
    End Property
    Public Property vpc_AuthStatus() As String
        Get
            Return null2unknown(_vpc_AuthStatus)
        End Get
        Set(ByVal Value As String)
            _vpc_AuthStatus = Value
        End Set
    End Property
    Public Property vpc_SecureHashCode() As String
        Get
            Return null2unknown(_vpc_SecureHashCode)
        End Get
        Set(ByVal Value As String)
            _vpc_SecureHashCode = Value
        End Set
    End Property

    Public Property vpc_SecureHashType() As String
        Get
            Return null2unknown(_vpc_SecureHashType)
        End Get
        Set(value As String)
            _vpc_SecureHashType = value
        End Set
    End Property
    Public Property vpc_TxnResponseCode() As String
        Get
            Return null2unknown(_vpc_TxnResponseCode)
        End Get
        Set(ByVal Value As String)
            _vpc_TxnResponseCode = Value
        End Set
    End Property
    Public Property vpc_TxnResponseCodeDescr() As String
        Get
            Return null2unknown(_vpc_TxnResponseCodeDescr)
        End Get
        Set(ByVal Value As String)
            _vpc_TxnResponseCodeDescr = Value
        End Set
    End Property
    Public Property vpc_Message() As String
        Get
            Return null2unknown(_vpc_Message)
        End Get
        Set(ByVal Value As String)
            _vpc_Message = Value
        End Set
    End Property
    Public Property AgainLink() As String
        Get
            Return null2unknown(_AgainLink)
        End Get
        Set(ByVal Value As String)
            _AgainLink = Value
        End Set
    End Property
    Public Property CPM_ID() As String
        Get
            Return null2unknown(_CPM_ID)
        End Get
        Set(ByVal Value As String)
            _CPM_ID = Value
        End Set
    End Property
    Public Property MySecueHashCodeName() As String
        Get
            Return null2unknown(_MySecueHashCodeName)
        End Get
        Set(ByVal Value As String)
            _MySecueHashCodeName = Value
        End Set
    End Property
    Public Property CPS_ID() As String
        Get
            Return null2unknown(_CPS_ID)
        End Get
        Set(ByVal Value As String)
            _CPS_ID = Value
        End Set
    End Property
    Public Property ResponseType() As String
        Get
            Return null2unknown(_ResponseType)
        End Get
        Set(ByVal Value As String)
            _ResponseType = Value
        End Set
    End Property
    Public Property vpc_virtualPaymentClientURL() As String
        Get
            Return null2unknown(_vpc_virtualPaymentClientURL)
        End Get
        Set(ByVal Value As String)
            _vpc_virtualPaymentClientURL = Value
        End Set
    End Property
    Public Property CPM_GATEWAY_TYPE() As String
        Get
            Return null2unknown(_CPM_GATEWAY_TYPE)
        End Get
        Set(ByVal Value As String)
            _CPM_GATEWAY_TYPE = Value
        End Set
    End Property
    Public Property CPM_bIFrameEnabled() As Boolean
        Get
            Return null2unknown(_CPM_bIFrameEnabled)
        End Get
        Set(ByVal Value As Boolean)
            _CPM_bIFrameEnabled = Value
        End Set
    End Property
    Public Property CPM_PaymentClientURL() As String
        Get
            Return null2unknown(_CPM_PaymentClientURL)
        End Get
        Set(ByVal Value As String)
            _CPM_PaymentClientURL = Value
        End Set
    End Property
    Public Property IsHashValidated() As Boolean
        Get
            Return null2unknown(_IsHashValidated)
        End Get
        Set(ByVal Value As Boolean)
            _IsHashValidated = Value
        End Set
    End Property
    Public Property CurrencyCode() As String
        Get
            Return null2unknown(_CurrencyCode)
        End Get
        Set(ByVal Value As String)
            _CurrencyCode = Value
        End Set
    End Property
    Public Property vpc_CustIP() As String
        Get
            Return null2unknown(_CustIP)
        End Get
        Set(ByVal Value As String)
            _CustIP = Value
        End Set
    End Property
    Public Property CustName() As String
        Get
            Return null2unknown(_CustName)
        End Get
        Set(ByVal Value As String)
            _CustName = Value
        End Set
    End Property
    Public Property CustEmail() As String
        Get
            Return null2unknown(_CustEmail)
        End Get
        Set(ByVal Value As String)
            _CustEmail = Value
        End Set
    End Property
    Public Property CustPhone() As String
        Get
            Return null2unknown(_CustPhone)
        End Get
        Set(ByVal Value As String)
            _CustPhone = Value
        End Set
    End Property
    Public Property vpc_MerchantCode() As String
        Get
            Return null2unknown(_MerchantCode)
        End Get
        Set(ByVal Value As String)
            _MerchantCode = Value
        End Set
    End Property
    Public Property vpc_MerchantReturnURL() As String
        Get
            Return null2unknown(_MerchantReturnURL)
        End Get
        Set(ByVal Value As String)
            _MerchantReturnURL = Value
        End Set
    End Property
    Public Property vpc_PageTimeout() As Integer
        Get
            Return _PageTimeout
        End Get
        Set(ByVal Value As Integer)
            _PageTimeout = Value
        End Set
    End Property
    Public Shared Property CardNo() As String
        Get
            Return null2unknown(_CardNo)
        End Get
        Set(ByVal Value As String)
            _CardNo = Value
        End Set
    End Property
    Public Property Title() As String
        Get
            Return null2unknown(_Title)
        End Get
        Set(ByVal Value As String)
            _Title = Value
        End Set
    End Property

    Public Property NEW_GATEWAY_TYPE() As String
        Get
            Return null2unknown(_NEW_GATEWAY_TYPE)
        End Get
        Set(ByVal Value As String)
            _NEW_GATEWAY_TYPE = Value
        End Set
    End Property

    Public _PAYMENT_GATEWAY_URL As String
    Public Property PAYMENT_GATEWAY_URL() As String
        Get
            Return null2unknown(_PAYMENT_GATEWAY_URL)
        End Get
        Set(ByVal Value As String)
            _PAYMENT_GATEWAY_URL = Value
        End Set
    End Property

    Public _vpc_Amount As String
    Public _vpc_ActualAmount As String
    Public _vpc_Locale As String
    Public _vpc_BatchNo As String
    Public _vpc_Command As String
    Public _vpc_Version As String
    Public _vpc_CardType As String
    Public _vpc_OrderInfo As String
    Public _vpc_ReceiptNo As String
    Public _vpc_MerchantID As String
    Public _vpc_AuthorizeID As String
    Public _vpc_MerchTxnRef As String
    Public _vpc_TransactionNo As String
    Public _vpc_AcqResponseCode As String
    Public _vpc_VerType As String
    Public _vpc_VerStatus As String
    Public _vpc_Token As String
    Public _vpc_VerSecurLevel As String
    Public _vpc_Enrolled As String
    Public _vpc_Xid As String
    Public _vpc_AcqECI As String
    Public _vpc_AuthStatus As String
    Public _vpc_SecureHashCode As String
    Public _vpc_SecureHashType As String
    Public _vpc_TxnResponseCode As String
    Public _vpc_TxnResponseCodeDescr As String
    Public _vpc_Message As String
    Public _AgainLink As String
    Public _CPM_ID As Int16
    Public _MySecueHashCodeName As String
    Public _CPS_ID As Int16
    Public _ResponseType As String
    Public _vpc_virtualPaymentClientURL As String
    Public Shared _CPM_PaymentClientURL As String
    Public Shared _CPM_GATEWAY_TYPE As String
    Public Shared _CPM_bIFrameEnabled As Boolean
    Public Shared _IsHashValidated As Boolean
    Private Shared _CurrencyCode As String
    Private Shared _CustIP As String
    Private _CustName As String
    Private _CustEmail As String
    Private _CustPhone As String
    Private _MerchantCode As String
    Private _MerchantReturnURL As String
    Private Shared _PageTimeout As Integer
    Private Shared _CardNo As String
    Public _Title As String
    Public _NEW_GATEWAY_TYPE As String
#End Region
    Public Sub New(ByVal CPS_ID_NEW As String)
        Try
            InitialiseVariables()
            CPS_ID = CPS_ID_NEW
            CPM_ID = GetPaymentProvider(CPS_ID, NEW_GATEWAY_TYPE, PAYMENT_GATEWAY_URL)
            If Not HttpContext.Current.Session("CPM_GATEWAY_TYPE") Is Nothing AndAlso HttpContext.Current.Session("CPM_GATEWAY_TYPE").ToString() <> "" Then
                NEW_GATEWAY_TYPE = HttpContext.Current.Session("CPM_GATEWAY_TYPE").ToString()
            End If
            SetGatewayParameterDetail(CPM_ID)
            vpc_virtualPaymentClientURL = CPM_PaymentClientURL
            vpc_CustIP = "10.0.10.15" 'GetIPAddress()
            vpc_PageTimeout = 780
            'CardNo = "4444333322221111"
            If CPM_GATEWAY_TYPE = "OCBC" Then
                ResponseType = "POST"
                '   vpc_virtualPaymentClientURL = "https://epayment.ocbc.com/BPG/admin/payment/PaymentInterface.jsp"
                MySecueHashCodeName = "TXN_SIGNATURE"
            ElseIf CPM_GATEWAY_TYPE = "MIGS" Then
                ResponseType = "GET"
                MySecueHashCodeName = "vpc_SecureHash"
            ElseIf CPM_GATEWAY_TYPE = "EGHL" Then
                ResponseType = "POST"
                MySecueHashCodeName = "HashValue"
                SetCustomerDetails()
            End If
        Catch ex As Exception

        End Try
    End Sub
    Private Sub InitialiseVariables()
        vpc_Amount = ""
        vpc_ActualAmount = ""
        vpc_Locale = ""
        vpc_BatchNo = ""
        vpc_Command = ""
        vpc_Version = ""
        vpc_CardType = ""
        vpc_OrderInfo = ""
        vpc_ReceiptNo = ""
        vpc_MerchantID = ""
        vpc_AuthorizeID = ""
        vpc_MerchTxnRef = ""
        vpc_TransactionNo = ""
        vpc_AcqResponseCode = ""
        vpc_VerType = ""
        vpc_VerStatus = ""
        vpc_Token = ""
        vpc_VerSecurLevel = ""
        vpc_Enrolled = ""
        vpc_Xid = ""
        vpc_AcqECI = ""
        vpc_AuthStatus = ""
        vpc_SecureHashCode = ""
        vpc_SecureHashType = ""
        vpc_TxnResponseCode = ""
        vpc_TxnResponseCodeDescr = ""
        vpc_Message = ""
        AgainLink = ""
        vpc_TxnResponseCodeDescr = ""
        MySecueHashCodeName = ""
        CurrencyCode = ""
        CustEmail = ""
        CardNo = ""
        vpc_CustIP = ""
        CustName = ""
        CustPhone = ""
    End Sub

    Public Function SetRequestDataForProvider(ByVal MerchTxnRef As String, ByVal Amount As String, ByVal ReturnURL As String, ByVal OrderInfo As String, ByRef transactiondata As System.Collections.SortedList, ByRef rawHashData As String, ByVal QueryStringCollection As System.Collections.Specialized.NameValueCollection, ByVal HTTP_REFERER As String, ByVal Title As String) As String
        Dim MerchantID, MerchantCode, SECURE_SECRET As String, MerchantName As String
        MerchantID = "" : MerchantCode = "" : SECURE_SECRET = "" : MerchantName = ""
        GetMerchantDetails_CPS_ID(MerchantID, MerchantCode, SECURE_SECRET, CPS_ID, CurrencyCode, MerchantName)
        Dim seperator As String = "?"
        rawHashData = ""
        Dim Signature As String
        Dim tranItem As System.Collections.DictionaryEntry
        Dim queryString As String
        queryString = vpc_virtualPaymentClientURL

        If CPM_GATEWAY_TYPE = "OCBC" Then
            transactiondata.Add("MERCHANT_ACC_NO", MerchantID)
            transactiondata.Add("MERCHANT_TRANID", MerchTxnRef)
            transactiondata.Add("AMOUNT", Amount)
            transactiondata.Add("TRANSACTION_TYPE", 2)
            transactiondata.Add("RESPONSE_TYPE", "HTTP")
            ReturnURL = ReturnURL.ToString().ToUpper.Replace("HTTP://", "https://")
            transactiondata.Add("RETURN_URL", ReturnURL.ToString())
            transactiondata.Add("TXN_DESC", OrderInfo.ToString())
            If SECURE_SECRET.Length > 0 Then
                rawHashData &= SECURE_SECRET
                rawHashData &= MerchantID
                rawHashData &= MerchTxnRef.ToString()
                rawHashData &= Amount.ToString()
            End If
        ElseIf CPM_GATEWAY_TYPE = "MIGS" Then
            For Each item As String In QueryStringCollection
                If (QueryStringCollection(item) <> "") AndAlso (item <> "virtualPaymentClientURL") AndAlso (item <> "SubButL") AndAlso (item <> "Title") AndAlso Not (item.StartsWith("__")) Then
                    If CPM_GATEWAY_TYPE = "MIGS" Then
                        transactiondata.Add(item, QueryStringCollection(item))
                    End If
                End If
            Next
            transactiondata.Add("vpc_AccessCode", MerchantCode)
            transactiondata.Add("vpc_Merchant", MerchantID)
            transactiondata.Add("vpc_MerchTxnRef", MerchTxnRef)
            transactiondata.Add("vpc_Amount", Amount)
            transactiondata.Add("vpc_Currency", CurrencyCode)
            ' add the AgainLink & Title fields
            transactiondata.Add("AgainLink", HTTP_REFERER)
            transactiondata.Add("Title", Title)
            '
            'rawHashData = SECURE_SECRET
            seperator = ""
            For Each tranItem In transactiondata
                If tranItem.Key.StartsWith("vpc_") Or tranItem.Key.StartsWith("user_") Then
                    rawHashData &= seperator & tranItem.Key & "=" & tranItem.Value
                    seperator = "&"
                End If
            Next
            Signature = CreateMIGS_SHA256Signature(rawHashData, SECURE_SECRET)
            transactiondata.Add("vpc_SecureHash", Signature)
            transactiondata.Add("vpc_SecureHashType", "SHA256")
        ElseIf CPM_GATEWAY_TYPE = "EGHL" Then 'for Malaysia
            transactiondata.Add("TransactionType", "SALE")
            transactiondata.Add("PymtMethod", "ANY")
            transactiondata.Add("LanguageCode", "en")
            transactiondata.Add("ServiceID", MerchantID)
            transactiondata.Add("PaymentID", MerchTxnRef)
            transactiondata.Add("OrderNumber", MerchTxnRef)
            transactiondata.Add("PaymentDesc", OrderInfo.ToString())
            ReturnURL = ReturnURL.ToString().ToUpper.Replace("HTTP://", "HTTP://")
            transactiondata.Add("MerchantReturnURL", ReturnURL)
            transactiondata.Add("MerchantName", MerchantName) ''to be changed to bsu name
            transactiondata.Add("Amount", Amount)
            transactiondata.Add("CurrencyCode", CurrencyCode)
            transactiondata.Add("CustIP", vpc_CustIP)
            rawHashData = MerchantCode & MerchantID & MerchTxnRef & ReturnURL & Amount & CurrencyCode & vpc_CustIP & vpc_PageTimeout ' & CardNo
            Signature = CreateMD5Signature(rawHashData)
            transactiondata.Add("HashValue", Signature)
            transactiondata.Add("CustName", CustName)
            transactiondata.Add("CustEmail", CustEmail)
            transactiondata.Add("CustPhone", CustPhone)
            transactiondata.Add("PageTimeout", vpc_PageTimeout)
        End If
        seperator = "?"
        For Each tranItem In transactiondata
            queryString &= seperator & System.Web.HttpUtility.UrlEncode(tranItem.Key.ToString()) & "=" & System.Web.HttpUtility.UrlEncode(tranItem.Value.ToString())
            seperator = "&"
        Next

        If SECURE_SECRET.Length > 0 And CPM_GATEWAY_TYPE <> "MIGS" Then
            Signature = CreateMD5Signature(rawHashData)
            queryString &= seperator & MySecueHashCodeName & "=" & Signature
            'queryString &= seperator & "vpc_SecureHashType=SHA256"
        End If
        SetRequestDataForProvider = queryString
    End Function
    Public Shared Function GetIPAddress() As String
        Dim context As HttpContext = HttpContext.Current
        Dim sIPAddress As String = context.Request.ServerVariables("HTTP_X_FORWARDED_FOR")
        If String.IsNullOrEmpty(sIPAddress) Then
            Return context.Request.ServerVariables("REMOTE_ADDR")
        Else
            Dim ipArray As String() = sIPAddress.Split(New [Char]() {","c})
            Return ipArray(0)
        End If
    End Function
    Public Shared Function GetIPAddress(ByVal request As System.Web.HttpRequest) As String
        Dim ip As String
        Try
            ip = request.ServerVariables("HTTP_X_FORWARDED_FOR")
            If Not String.IsNullOrEmpty(ip) Then
                If ip.IndexOf(",") > 0 Then
                    Dim ipRange() As String = ip.Split(","c)
                    Dim le As Integer = ipRange.Length - 1
                    ip = ipRange(le)
                End If
            Else
                ip = request.UserHostAddress
            End If
        Catch
            ip = Nothing
        End Try

        Return ip
    End Function
    Private Sub SetCustomerDetails()
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISConnectionString, CommandType.Text, _
        "EXEC ONLINE.GetMailDetals_OASIS '" & HttpContext.Current.Session("username") & "', '" & HttpContext.Current.Session("sBsuId") & "',''")
        If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
            CustName = ds.Tables(0).Rows(0)("OLU_DISPLAYNAME").ToString
            CustEmail = ds.Tables(0).Rows(0)("OLU_Email").ToString
            CustPhone = ds.Tables(0).Rows(0)("OLU_PHONE").ToString
        End If
    End Sub
    Public Shared Function GetPaymentProvider(ByVal CPS_ID As String, ByRef pGATEWAY_TYPE As String, ByRef pPAYMENT_GATEWAY_URL As String) As Int16
        Try
            Dim sqlStr, CPM_ID As String
            CPM_ID = 0
            sqlStr = "SELECT CPS_CPM_ID, ISNULL(CPS_PAYMENT_GATEWAY_TYPE ,'') NEW_GATEWAY_TYPE FROM dbo.CREDITCARD_PROVD_S WITH(NOLOCK) WHERE CPS_ID='" & CPS_ID & "'"
            Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISFINConnectionString, CommandType.Text, sqlStr)
            If dsData.Tables.Count > 0 Then
                If dsData.Tables(0).Rows.Count > 0 Then
                    CPM_ID = dsData.Tables(0).Rows(0).Item("CPS_CPM_ID")
                    pGATEWAY_TYPE = dsData.Tables(0).Rows(0).Item("NEW_GATEWAY_TYPE").ToString().Trim
                    pPAYMENT_GATEWAY_URL = "" 'dsData.Tables(0).Rows(0).Item("PAYMENT_GATEWAY_URL").ToString().Trim
                End If
            End If
            Return CPM_ID
        Catch ex As Exception
            Return 0
        End Try
    End Function
    Public Shared Function GetDefaultPaymentProvider(ByVal BSU_ID As String) As Int16
        Dim pParms(1) As SqlClient.SqlParameter
        pParms(0) = New SqlClient.SqlParameter("@BSU_ID", SqlDbType.VarChar, 20)
        pParms(0).Value = BSU_ID
        Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISFINConnectionString, _
          CommandType.StoredProcedure, "Get_Default_MerchantDetails_BSU_ID", pParms)
        If Not dsData Is Nothing AndAlso dsData.Tables.Count > 0 Then
            For Each row As DataRow In dsData.Tables(0).Rows
                ' If row("CPS_CPM_ID") = CPM_ID Then
                Return row("CPS_ID")
                ' End If
            Next
            Return 0
        End If
    End Function
    Public Function SetGatewayParameterDetail(ByVal CPM_ID As String) As Int16
        Try
            Dim sqlStr As String
            sqlStr = "SELECT CPM_GATEWAY_TYPE,CPM_bIFrameEnabled,CPM_PaymentClientURL, ISNULL(CPM_PAYMENT_GATEWAY_URL,'') PAYMENT_GATEWAY_URL FROM dbo.CREDITCARD_PROVD_M WITH(NOLOCK) WHERE CPM_ID='" & CPM_ID & "'"
            Dim dsData As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISFINConnectionString, CommandType.Text, sqlStr)
            If dsData.Tables.Count > 0 Then
                If dsData.Tables(0).Rows.Count > 0 Then
                    CPM_GATEWAY_TYPE = dsData.Tables(0).Rows(0).Item("CPM_GATEWAY_TYPE")
                    If NEW_GATEWAY_TYPE <> "" Then
                        CPM_GATEWAY_TYPE = NEW_GATEWAY_TYPE
                        PAYMENT_GATEWAY_URL = dsData.Tables(0).Rows(0).Item("PAYMENT_GATEWAY_URL").ToString
                    End If
                    CPM_bIFrameEnabled = dsData.Tables(0).Rows(0).Item("CPM_bIFrameEnabled")
                    CPM_PaymentClientURL = dsData.Tables(0).Rows(0).Item("CPM_PaymentClientURL")
                    HttpContext.Current.Session("CPM_GATEWAY_TYPE") = CPM_GATEWAY_TYPE
                End If
            End If
            Return CPM_ID
        Catch ex As Exception
            Return 0
        End Try
    End Function

    Public Sub SetResponseCodeFromPaymentGateway(ByVal QueryStringCollection As System.Collections.Specialized.NameValueCollection)
        If CPM_ID <> "0" Then
            If CPM_GATEWAY_TYPE = "MIGS" Then  'NBAD from MIGS Gateway
                vpc_Amount = null2unknown(QueryStringCollection("vpc_Amount"))
                vpc_ActualAmount = Format(vpc_Amount / 100, "0.00")
                vpc_Locale = null2unknown(QueryStringCollection("vpc_Locale"))
                vpc_BatchNo = null2unknown(QueryStringCollection("vpc_BatchNo"))
                vpc_Command = null2unknown(QueryStringCollection("vpc_Command"))
                vpc_Version = null2unknown(QueryStringCollection("vpc_Version"))
                vpc_CardType = null2unknown(QueryStringCollection("vpc_Card"))
                vpc_OrderInfo = null2unknown(QueryStringCollection("vpc_OrderInfo"))
                vpc_ReceiptNo = null2unknown(QueryStringCollection("vpc_ReceiptNo"))
                vpc_MerchantID = null2unknown(QueryStringCollection("vpc_Merchant"))
                vpc_AuthorizeID = null2unknown(QueryStringCollection("vpc_AuthorizeId"))
                vpc_MerchTxnRef = null2unknown(QueryStringCollection("vpc_MerchTxnRef"))
                vpc_TransactionNo = null2unknown(QueryStringCollection("vpc_TransactionNo"))
                vpc_AcqResponseCode = null2unknown(QueryStringCollection("vpc_AcqResponseCode"))
                vpc_VerType = null2unknown(QueryStringCollection("vpc_VerType"))
                vpc_VerStatus = null2unknown(QueryStringCollection("vpc_VerStatus"))
                vpc_Token = null2unknown(QueryStringCollection("vpc_VerToken"))
                vpc_VerSecurLevel = null2unknown(QueryStringCollection("vpc_VerSecurityLevel"))
                vpc_Enrolled = null2unknown(QueryStringCollection("vpc_3DSenrolled"))
                vpc_Xid = null2unknown(QueryStringCollection("vpc_3DSXID"))
                vpc_AcqECI = null2unknown(QueryStringCollection("vpc_3DSECI"))
                vpc_AuthStatus = null2unknown(QueryStringCollection("vpc_3DSstatus"))
                vpc_SecureHashCode = null2unknown(QueryStringCollection("vpc_SecureHash"))
                vpc_TxnResponseCode = null2unknown(QueryStringCollection("vpc_TxnResponseCode"))
                vpc_TxnResponseCodeDescr = getResponseDescription(vpc_TxnResponseCode)
                vpc_Message = null2unknown(QueryStringCollection(vpc_Message))
                AgainLink = null2unknown(QueryStringCollection(AgainLink))
                vpc_TxnResponseCodeDescr = getResponseDescription(vpc_TxnResponseCode)
                vpc_SecureHashType = null2unknown(QueryStringCollection("vpc_SecureHashType"))
                Title = null2unknown(QueryStringCollection("Title"))
            ElseIf CPM_GATEWAY_TYPE = "OCBC" Then  ' OCBC from Infintilium
                'OCBC starts here
                vpc_Command = ""
                vpc_Version = ""
                vpc_SecureHashCode = null2unknown(QueryStringCollection("TXN_SIGNATURE"))
                vpc_ActualAmount = vpc_Amount
                vpc_TxnResponseCode = null2unknown(QueryStringCollection("RESPONSE_CODE"))
                vpc_TxnResponseCodeDescr = null2unknown(QueryStringCollection("RESPONSE_DESC"))
                vpc_MerchTxnRef = null2unknown(QueryStringCollection("MERCHANT_TRANID"))
                vpc_TransactionNo = null2unknown(QueryStringCollection("TRANSACTION_ID"))
                vpc_AuthorizeID = null2unknown(QueryStringCollection("AUTH_ID"))
                vpc_VerSecurLevel = null2unknown(QueryStringCollection("FR_LEVEL"))
                vpc_ReceiptNo = null2unknown(QueryStringCollection("TRANSACTION_ID"))
                vpc_CardType = "VC"
                vpc_AcqResponseCode = null2unknown(QueryStringCollection("TXN_STATUS"))
                vpc_AuthStatus = null2unknown(QueryStringCollection("TXN_STATUS"))

                Dim MerchantID, MerchantCode, SECURE_SECRET As String
                MerchantID = "" : MerchantCode = "" : SECURE_SECRET = ""
                GetMerchantDetails_CPS_ID(MerchantID, MerchantCode, SECURE_SECRET, CPS_ID)
                vpc_MerchantID = MerchantID

                vpc_OrderInfo = null2unknown(QueryStringCollection("vpc_OrderInfo"))

                vpc_Message = null2unknown(QueryStringCollection(vpc_Message))
                AgainLink = null2unknown(QueryStringCollection(AgainLink))
                Title = null2unknown(QueryStringCollection("Title"))
            ElseIf CPM_GATEWAY_TYPE = "EGHL" Then 'Payment gateway for Malaysia
                vpc_Command = ""
                vpc_Version = ""
                vpc_Amount = null2unknown(QueryStringCollection("Amount"))
                vpc_SecureHashCode = null2unknown(QueryStringCollection("HashValue"))
                vpc_ActualAmount = vpc_Amount
                vpc_TxnResponseCode = null2unknown(QueryStringCollection("TxnStatus"))
                vpc_TxnResponseCodeDescr = HttpUtility.UrlDecode(null2unknown(QueryStringCollection("TxnMessage")))
                vpc_MerchTxnRef = null2unknown(QueryStringCollection("PaymentID"))
                vpc_TransactionNo = null2unknown(QueryStringCollection("TxnID"))
                vpc_AuthorizeID = null2unknown(QueryStringCollection("AuthCode"))
                vpc_ReceiptNo = null2unknown(QueryStringCollection("OrderNumber"))
                vpc_CardType = null2unknown(QueryStringCollection("PymtMethod"))
                vpc_AcqResponseCode = null2unknown(QueryStringCollection("TxnStatus"))
                vpc_AuthStatus = null2unknown(QueryStringCollection("TxnStatus"))
                vpc_MerchantReturnURL = null2unknown(QueryStringCollection("MerchantReturnURL"))
                Dim MerchantID, MerchantCode, SECURE_SECRET As String
                MerchantID = "" : MerchantCode = "" : SECURE_SECRET = ""
                GetMerchantDetails_CPS_ID(MerchantID, MerchantCode, SECURE_SECRET, CPS_ID, CurrencyCode)
                vpc_MerchantID = MerchantID
                vpc_MerchantCode = MerchantCode
                vpc_OrderInfo = null2unknown(QueryStringCollection("PaymentDesc"))
                Title = null2unknown(QueryStringCollection("Title"))
            End If
        End If
    End Sub
    Public Shared Sub GetMerchantDetails_CPS_ID(ByRef MerchantID As String, _
       ByRef MerchantCode As String, ByRef SECURE_SECRET As String, ByVal CPS_ID As String, Optional ByRef Currency As String = "", Optional ByRef BSU_NAME As String = "")
        Dim ds As New DataSet
        Dim str_qurey As String = "exec GetMerchantDetails_CPS_ID   " & CPS_ID
        ds = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISFINConnectionString, CommandType.Text, _
        str_qurey)
        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            MerchantID = ds.Tables(0).Rows(0)("BSU_MERCHANTID")
            MerchantCode = ds.Tables(0).Rows(0)("BSU_MERCHANTCODE")
            SECURE_SECRET = ds.Tables(0).Rows(0)("BSU_SECURE_SECRET").ToString
            Currency = ds.Tables(0).Rows(0)("BSU_CURRENCY").ToString
            BSU_NAME = ds.Tables(0).Rows(0)("BSU_NAME").ToString
        End If
    End Sub
    Private Shared Function null2unknown(ByVal req As Object) As String
        Try
            If req Is Nothing Then
                Return String.Empty
            Else
                Return Microsoft.Security.Application.Encoder.HtmlEncode(req.ToString())
            End If
        Catch ex As Exception
            Return String.Empty
        End Try
    End Function
    Public Shared Function GetSECURE_SECRET(ByVal BSU_ID As String) As String
        Dim str_qurey As String = "SELECT BSU_SECURE_SECRET " & _
        " FROM dbo.BUSINESSUNIT_M WITH(NOLOCK) where BSU_ID='" & BSU_ID & "'"
        Dim str_data As String = UtilityObj.GetDataFromSQL(str_qurey, _
            ConnectionManger.GetOASISConnectionString)
        If str_data <> "--" Then
            Return str_data
        Else
            Return ""
        End If
    End Function
    Public Function getResponseDescription(ByVal vResponseCode As String) As String
        Dim result As String = "Unknown"
        If CPM_GATEWAY_TYPE = "MIGS" Then 'NBAD from MIGS Gateway
            If vResponseCode.Length > 0 Then
                Select Case vResponseCode
                    Case "0"
                        result = "Transaction Successful"
                        Exit Select
                    Case "1"
                        result = "Transaction Declined"
                        Exit Select
                    Case "2"
                        result = "Bank Declined Transaction"
                        Exit Select
                    Case "3"
                        result = "No Reply from Bank"
                        Exit Select
                    Case "4"
                        result = "Expired Card"
                        Exit Select
                    Case "5"
                        result = "Insufficient Funds"
                        Exit Select
                    Case "6"
                        result = "Error Communicating with Bank"
                        Exit Select
                    Case "7"
                        result = "Payment Server detected an error"
                        Exit Select
                    Case "8"
                        result = "Transaction Type Not Supported"
                        Exit Select
                    Case "9"
                        result = "Bank declined transaction (Do not contact Bank)"
                        Exit Select
                    Case "A"
                        result = "Transaction Aborted"
                        Exit Select
                    Case "B"
                        result = "Transaction Declined - Contact the Bank"
                        Exit Select
                    Case "C"
                        result = "Transaction Cancelled"
                        Exit Select
                    Case "D"
                        result = "Deferred transaction has been received and is awaiting processing"
                        Exit Select
                    Case "F"
                        result = "3-D Secure Authentication failed"
                        Exit Select
                    Case "I"
                        result = "Card Security Code verification failed"
                        Exit Select
                    Case "L"
                        result = "Shopping Transaction Locked (Please try the transaction again later)"
                        Exit Select
                    Case "N"
                        result = "Cardholder is not enrolled in Authentication scheme"
                        Exit Select
                    Case "P"
                        result = "Transaction has been received by the Payment Adaptor and is being processed"
                        Exit Select
                    Case "R"
                        result = "Transaction was not processed - Reached limit of retry attempts allowed"
                        Exit Select
                    Case "S"
                        result = "Duplicate SessionID"
                        Exit Select
                    Case "T"
                        result = "Address Verification Failed"
                        Exit Select
                    Case "U"
                        result = "Card Security Code Failed"
                        Exit Select
                    Case "V"
                        result = "Address Verification and Card Security Code Failed"
                        Exit Select
                    Case Else
                        result = "Unable to be determined"
                        Exit Select
                End Select
            End If
        ElseIf CPM_GATEWAY_TYPE = "OCBC" Then  ' OCBC from Infintilium
            If vResponseCode.Length > 0 Then
                Select Case vResponseCode
                    Case "N"
                        result = "Pending/Not authorized"
                    Case "A"
                        result = "Authorized"
                    Case "C"
                        result = "Captured"
                    Case "S"
                        result = "Sales Completed"
                    Case "V"
                        result = "Void"
                    Case "E"
                        result = "Error/Exception Occurred"
                    Case "F"
                        result = "Not Approved"
                    Case "CB"
                        result = "Charge back"
                    Case "FBL"
                        result = "Blacklisted"
                    Case "FB"
                        result = "Blocked"
                    Case Else
                        result = "Unable to be determined"
                        Exit Select
                End Select
            End If
        End If
        Return result
    End Function
    Public Function CreateMD5Signature(ByVal RawData As String) As String
        '
        '    <summary>Creates a MD5 Signature</summary>
        '    <param name="RawData">The string used to create the MD5 signautre.</param>
        '    <returns>A string containing the MD5 signature.</returns>
        '    
        If CPM_GATEWAY_TYPE = "EGHL" Then ''included MIGS also for SHA256 from oct2016 by Jacob.
            Return CreateSHA256Signature(RawData)
        End If
        Dim hasher As System.Security.Cryptography.MD5 = System.Security.Cryptography.MD5CryptoServiceProvider.Create()
        Dim HashValue As Byte() = hasher.ComputeHash(Encoding.ASCII.GetBytes(RawData))

        Dim strHex As String = ""
        For Each b As Byte In HashValue
            strHex &= b.ToString("x2")
        Next

        If CPM_GATEWAY_TYPE = "OCBC" Then
            Return strHex
        Else
            Return strHex.ToUpper()
        End If
    End Function
    Public Function CreateSHA256Signature(ByVal RawData As String) As String
        '
        '    <summary>Creates a SHA256 Signature</summary>
        '    <param name="RawData">The string used to create the MD5 signautre.</param>
        '    <returns>A string containing the MD5 signature.</returns>
        '    

        Dim hasher As System.Security.Cryptography.SHA256 = System.Security.Cryptography.SHA256CryptoServiceProvider.Create()
        Dim HashValue As Byte() = hasher.ComputeHash(Encoding.ASCII.GetBytes(RawData))

        Dim strHex As String = ""
        For Each b As Byte In HashValue
            strHex &= b.ToString("x2")
        Next

        If CPM_GATEWAY_TYPE = "EGHL" Then
            Return strHex
        Else
            Return strHex.ToUpper()
        End If
    End Function

    Public Function CreateMIGS_SHA256Signature(ByVal RawData As String, ByVal SECURE_SECRET As String) As String
        Dim convertedHash((SECURE_SECRET.Length \ 2) - 1) As Byte
        For i As Integer = 0 To (SECURE_SECRET.Length \ 2) - 1
            convertedHash(i) = CByte(Int32.Parse(SECURE_SECRET.Substring(i * 2, 2), System.Globalization.NumberStyles.HexNumber))
        Next i

        Dim hexHash As String = ""
        Using hasher As New HMACSHA256(convertedHash)
            Dim hashValue() As Byte = hasher.ComputeHash(Encoding.UTF8.GetBytes(RawData))
            For Each b As Byte In hashValue
                hexHash &= b.ToString("X2")
            Next b
        End Using
        Return hexHash
    End Function
    Public Function getParameterValues() As String
        Dim myString As String
        myString = "vpc_Amount -" & vpc_Amount
        myString &= ",vpc_ActualAmount -" & vpc_ActualAmount
        myString &= ",vpc_Locale-" & vpc_Locale
        myString &= ",vpc_BatchNo-" & vpc_BatchNo
        myString &= ",vpc_Command-" & vpc_Command
        myString &= ",vpc_Version-" & vpc_Version
        myString &= ",vpc_CardType-" & vpc_CardType
        myString &= ",vpc_OrderInfo-" & vpc_OrderInfo
        myString &= ",vpc_ReceiptNo-" & vpc_ReceiptNo
        myString &= ",vpc_MerchantID-" & vpc_MerchantID
        myString &= ",vpc_AuthorizeID-" & vpc_AuthorizeID
        myString &= ",vpc_MerchTxnRef -" & vpc_MerchTxnRef
        myString &= ",vpc_TransactionNo-" & vpc_TransactionNo
        myString &= ",vpc_AcqResponseCode -" & vpc_AcqResponseCode
        myString &= ",vpc_VerType -" & vpc_VerType
        myString &= ",vpc_VerStatus-" & vpc_VerStatus
        myString &= ",vpc_Token-" & vpc_Token
        myString &= ",vpc_VerSecurLevel-" & vpc_VerSecurLevel
        myString &= ",vpc_Enrolled -" & vpc_Enrolled
        myString &= ",vpc_Xid-" & vpc_Xid
        myString &= ",vpc_AcqECI-" & vpc_AcqECI
        myString &= ",vpc_AuthStatus -" & vpc_AuthStatus
        myString &= ",vpc_SecureHashCode -" & vpc_SecureHashCode
        myString &= ",vpc_TxnResponseCode -" & vpc_TxnResponseCode
        myString &= ",vpc_TxnResponseCodeDescr-" & vpc_TxnResponseCodeDescr
        myString &= ",vpc_Message-" & vpc_Message
        myString &= ",AgainLink-" & AgainLink
        myString &= ",vpc_TxnResponseCodeDescr -" & vpc_TxnResponseCodeDescr
        myString &= ",MySecueHashCodeName-" & MySecueHashCodeName
        Return myString

    End Function
    Public Function GetAmountInPaymentGatewayFormat(ByVal Amount As String) As String
        Dim mAmount As String
        mAmount = Amount

        If CPM_GATEWAY_TYPE = "OCBC" Or CPM_GATEWAY_TYPE = "EGHL" Then
            mAmount = Amount / 100
            mAmount = TwoDecimalString(mAmount)
        End If
        GetAmountInPaymentGatewayFormat = mAmount
    End Function
    Public Shared Function GetAmountInPaymentGatewayFormat(ByVal pGATEWAY_TYPE As String, ByVal Amount As String) As String
        Dim mAmount As String
        mAmount = Amount

        If pGATEWAY_TYPE = "OCBC" Or pGATEWAY_TYPE = "EGHL" Then
            mAmount = Amount / 100
            mAmount = TwoDecimalString(mAmount)
        End If
        GetAmountInPaymentGatewayFormat = mAmount
    End Function
    Public Shared Function TwoDecimalString(ByVal input As String) As String
        Dim output As String
        Dim splittedInput As String() = input.Split("."c)
        If splittedInput.Length = 1 Then
            output = input & ".00"
        Else
            output = splittedInput(0) & "." & splittedInput(1).PadRight(2, "0"c)
        End If
        Return output
    End Function
    Public Function IsPaymentSuccessful() As Boolean
        Dim result As String = "Unknown"
        If CPM_GATEWAY_TYPE = "MIGS" Or CPM_GATEWAY_TYPE = "EGHL" Then 'NBAD from MIGS Gateway
            If vpc_TxnResponseCode.ToString = "0" Then
                Return True
            Else
                Return False
            End If
        ElseIf CPM_GATEWAY_TYPE = "OCBC" Then  ' OCBC from Infintilium
            If vpc_TxnResponseCode.ToString = "A" Or vpc_TxnResponseCode.ToString = "S" Then
                Return True
            Else
                Return False
            End If
        End If
        Return False
    End Function

    Public Function GetPageResponseValues() As String
        GetPageResponseValues = ""
        Try
            GetPageResponseValues = vpc_MerchantCode & vpc_MerchantID & vpc_MerchTxnRef & HttpContext.Current.Request.Url.AbsoluteUri.ToUpper _
                        & vpc_Amount & CurrencyCode & vpc_CustIP & vpc_PageTimeout
        Catch ex As Exception
            GetPageResponseValues = ""
        End Try
    End Function

    Public Shared Sub GetGatewayParameters(ByVal pBSU_ID As String, ByVal pCPS_ID As Integer, ByRef pGatewayType As String, ByRef pGatewayURL As String)
        pGatewayType = "" : pGatewayURL = ""
        Dim Qry As String = "SELECT ISNULL(CPS_PAYMENT_GATEWAY_TYPE, '') GATEWAY_TYPE, ISNULL(CPS_PAYMENT_GATEWAY_URL, ISNULL(CPM_PAYMENT_GATEWAY_URL, '')) GATEWAY_URL " & _
            "FROM OASISFIN.dbo.CREDITCARD_PROVD_S AS A WITH(NOLOCK) INNER JOIN OASISFIN.dbo.CREDITCARD_PROVD_M AS B WITH(NOLOCK) " & _
            "ON A.CPS_CPM_ID = B.CPM_ID WHERE CPS_BSU_ID = '" & pBSU_ID & "' AND CPS_ID = " & pCPS_ID
        Dim ds As DataSet = SqlHelper.ExecuteDataset(ConnectionManger.GetOASISFINConnectionString, CommandType.Text, Qry)
        If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
            pGatewayType = ds.Tables(0).Rows(0)("GATEWAY_TYPE").ToString()
            pGatewayURL = ds.Tables(0).Rows(0)("GATEWAY_URL").ToString()
        End If
    End Sub
End Class
